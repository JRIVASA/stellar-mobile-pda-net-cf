﻿Imports System.Data.SqlClient
Imports System.Data

Public Class Form1

    Dim mCn As New SqlConnection

    Private Sub cmdConnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdConnect.Click


        Try

            Dim mCnString As String = "Data Source=" + txtServerInstance.Text + ";Initial Catalog=" + txtServerBD.Text + ";" + _
            "User ID=" + txtServerUser.Text + ";Password=" + txtServerPassword.Text + ";Persist Security Info=True"

            If mCn.State <> ConnectionState.Open Then
                mCn.ConnectionString = mCnString
                mCn.Open()
            End If

            Dim Resp As DialogResult = MessageBox.Show( _
            "Connection Success. Select Yes to Open a table preview " + _
            "or No to simply attempt to recover server date " + _
            "(Check connection effect)", "Test", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)

            Dim Tbl As String

            If Resp = Windows.Forms.DialogResult.Yes Then
                Tbl = InputBox("Enter table name to make simple select")
            End If

            If String.IsNullOrEmpty(Tbl) Then
                Tbl = "SELECT GETDATE() AS ServerTime"
            Else
                Tbl = "SELECT TOP 500 * FROM " + Tbl.Replace("'", "").Replace(";", "") + ";"
            End If

            Dim QRes As New DataSet

            Dim Adapter_Servidor As New SqlDataAdapter
            Dim CmdDB As New SqlCommand

            Try
                CmdDB.CommandText = Tbl
                CmdDB.Connection = mCn
                'el metodo SelectCommand de un dataAdapter contiene los datos o información del origen de datos
                Adapter_Servidor.SelectCommand = (CmdDB)
                'Adapter_Servidor.SelectCommand.Transaction = Nothing
                Adapter_Servidor.Fill(QRes)
                Adapter_Servidor.Dispose()
            Catch sqlEx As SqlException
                MessageBox.Show("Query Error:" + sqlEx.Message + "")
            Catch AnyEx As Exception
                MessageBox.Show("Query Error:" + AnyEx.Message + "")
            Finally
                CmdDB.Parameters.Clear()
            End Try

            DataPreview.Visible = True
            DataPreview.BringToFront()
            DataPreview.DataSource = QRes.Tables(0)

            'En caso que se produzca un error en las líneas de código del Try, 
            'el Catch contiene el código para controlar cualquier error que tenga lugar
            'y se produce una execpción'
        Catch sqlEx As SqlException
            MessageBox.Show("Connection Error:" + sqlEx.Message + "")
        Catch Any As Exception
            MessageBox.Show("Connection Error:" + Any.Message)
        End Try

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub txtServerBD_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtServerBD.KeyDown
        If e.KeyCode = Keys.Enter Then
            cmdConnect_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub txtServerBD_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtServerBD.TextChanged

    End Sub
End Class
