﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    private mainMenu1 As System.Windows.Forms.MainMenu

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.mainMenu1 = New System.Windows.Forms.MainMenu
        Me.txtServerInstance = New System.Windows.Forms.TextBox
        Me.lblServerInstance = New System.Windows.Forms.Label
        Me.lblServerUser = New System.Windows.Forms.Label
        Me.txtServerUser = New System.Windows.Forms.TextBox
        Me.lblServerPassword = New System.Windows.Forms.Label
        Me.txtServerPassword = New System.Windows.Forms.TextBox
        Me.lblServerBD = New System.Windows.Forms.Label
        Me.txtServerBD = New System.Windows.Forms.TextBox
        Me.cmdConnect = New System.Windows.Forms.Button
        Me.DataPreview = New System.Windows.Forms.DataGrid
        Me.SuspendLayout()
        '
        'txtServerInstance
        '
        Me.txtServerInstance.Location = New System.Drawing.Point(17, 24)
        Me.txtServerInstance.Name = "txtServerInstance"
        Me.txtServerInstance.Size = New System.Drawing.Size(167, 23)
        Me.txtServerInstance.TabIndex = 0
        '
        'lblServerInstance
        '
        Me.lblServerInstance.Location = New System.Drawing.Point(17, 6)
        Me.lblServerInstance.Name = "lblServerInstance"
        Me.lblServerInstance.Size = New System.Drawing.Size(161, 15)
        Me.lblServerInstance.Text = "Server Instance"
        Me.lblServerInstance.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblServerUser
        '
        Me.lblServerUser.Location = New System.Drawing.Point(16, 50)
        Me.lblServerUser.Name = "lblServerUser"
        Me.lblServerUser.Size = New System.Drawing.Size(161, 15)
        Me.lblServerUser.Text = "User"
        Me.lblServerUser.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txtServerUser
        '
        Me.txtServerUser.Location = New System.Drawing.Point(16, 68)
        Me.txtServerUser.Name = "txtServerUser"
        Me.txtServerUser.Size = New System.Drawing.Size(167, 23)
        Me.txtServerUser.TabIndex = 3
        Me.txtServerUser.Text = "SA"
        '
        'lblServerPassword
        '
        Me.lblServerPassword.Location = New System.Drawing.Point(16, 94)
        Me.lblServerPassword.Name = "lblServerPassword"
        Me.lblServerPassword.Size = New System.Drawing.Size(161, 15)
        Me.lblServerPassword.Text = "Password"
        Me.lblServerPassword.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txtServerPassword
        '
        Me.txtServerPassword.Location = New System.Drawing.Point(16, 112)
        Me.txtServerPassword.Name = "txtServerPassword"
        Me.txtServerPassword.Size = New System.Drawing.Size(167, 23)
        Me.txtServerPassword.TabIndex = 6
        '
        'lblServerBD
        '
        Me.lblServerBD.Location = New System.Drawing.Point(16, 138)
        Me.lblServerBD.Name = "lblServerBD"
        Me.lblServerBD.Size = New System.Drawing.Size(161, 15)
        Me.lblServerBD.Text = "Database"
        Me.lblServerBD.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txtServerBD
        '
        Me.txtServerBD.Location = New System.Drawing.Point(16, 156)
        Me.txtServerBD.Name = "txtServerBD"
        Me.txtServerBD.Size = New System.Drawing.Size(167, 23)
        Me.txtServerBD.TabIndex = 9
        Me.txtServerBD.Text = "VAD10"
        '
        'cmdConnect
        '
        Me.cmdConnect.Location = New System.Drawing.Point(16, 200)
        Me.cmdConnect.Name = "cmdConnect"
        Me.cmdConnect.Size = New System.Drawing.Size(166, 29)
        Me.cmdConnect.TabIndex = 11
        Me.cmdConnect.Text = "Connect"
        '
        'DataPreview
        '
        Me.DataPreview.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.DataPreview.Location = New System.Drawing.Point(17, 6)
        Me.DataPreview.Name = "DataPreview"
        Me.DataPreview.Size = New System.Drawing.Size(165, 171)
        Me.DataPreview.TabIndex = 16
        Me.DataPreview.Visible = False
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(198, 247)
        Me.Controls.Add(Me.cmdConnect)
        Me.Controls.Add(Me.lblServerBD)
        Me.Controls.Add(Me.txtServerBD)
        Me.Controls.Add(Me.lblServerPassword)
        Me.Controls.Add(Me.txtServerPassword)
        Me.Controls.Add(Me.lblServerUser)
        Me.Controls.Add(Me.txtServerUser)
        Me.Controls.Add(Me.lblServerInstance)
        Me.Controls.Add(Me.txtServerInstance)
        Me.Controls.Add(Me.DataPreview)
        Me.Menu = Me.mainMenu1
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtServerInstance As System.Windows.Forms.TextBox
    Friend WithEvents lblServerInstance As System.Windows.Forms.Label
    Friend WithEvents lblServerUser As System.Windows.Forms.Label
    Friend WithEvents txtServerUser As System.Windows.Forms.TextBox
    Friend WithEvents lblServerPassword As System.Windows.Forms.Label
    Friend WithEvents txtServerPassword As System.Windows.Forms.TextBox
    Friend WithEvents lblServerBD As System.Windows.Forms.Label
    Friend WithEvents txtServerBD As System.Windows.Forms.TextBox
    Friend WithEvents cmdConnect As System.Windows.Forms.Button
    Friend WithEvents DataPreview As System.Windows.Forms.DataGrid

End Class
