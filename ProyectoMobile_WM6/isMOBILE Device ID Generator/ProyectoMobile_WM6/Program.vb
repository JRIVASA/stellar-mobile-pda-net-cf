﻿Module Program

    Public nombreArchivoExe As String
    Public rutaArchivoExe As String
    Public NewID As String

    Sub main()

        ConfiguracionInicial()

        Call AsignarPDA()

        Application.Exit()

    End Sub

    Function AsignarPDA() As Boolean

        Try
            Dim OldIdDispositivo As String
            OldIdDispositivo = Registry.GetValue("HKEY_LOCAL_MACHINE\Ident", "OrigName", "Unknown")

            Registry.SetValue("HKEY_LOCAL_MACHINE\Ident", "OrigName", NewID)

            AsignarPDA = True

        Catch ex As Exception
            'Application.DoEvents()
            'MsgBox("Ha ocurrido un error, reporte lo siguiente: " & vbNewLine & ex.Message)
        End Try

    End Function

    Sub ConfiguracionInicial()

        'Obtengo por Reflexion la ruta completa del archivo .exe
        nombreArchivoExe = System.Reflection.Assembly.GetExecutingAssembly.GetName.CodeBase

        rutaArchivoExe = String.Empty
        'Obtengo la ruta del archivo .exe             
        rutaArchivoExe = nombreArchivoExe.Substring(0, nombreArchivoExe.LastIndexOf("\") + 1)

        Call Leer_SETUP(rutaArchivoExe)

    End Sub

    Function Leer_SETUP(ByVal rutaArchivoExe) As Boolean

        Dim SETUP As New XmlDocument
        Dim nodo_SETUP As XmlNodeList
        Dim IDGenerator As New Random
        Dim Value As Integer

        Dim Create As FileStream

        'For i = 1 To 1000
        Value = CInt(IDGenerator.Next(1, 999999999))
        'Next i

        If Not (File.Exists(rutaArchivoExe + "DeviceID.xml")) Then

            NewID = Format(Value, "ID000000000")
            File.Create(rutaArchivoExe + "DeviceID.xml").Close()
            Dim Write As New System.IO.StreamWriter(CStr(rutaArchivoExe + "DeviceID.xml"))
            Dim FileText As String
            FileText = "<?xml version=" & Chr(34) & "1.0" & Chr(34) & " encoding=" & Chr(34) & "utf-8" & Chr(34) & " standalone=" & Chr(34) & "yes" & Chr(34) & " ?>"
            FileText = FileText & vbNewLine & vbNewLine & "<Table>"
            FileText = FileText & vbNewLine & vbTab & "<ID>"
            FileText = FileText & vbNewLine & vbTab & vbTab & "<Valor>" & NewID & "</Valor>"
            FileText = FileText & vbNewLine & vbTab & "</ID>"
            FileText = FileText & vbNewLine & "</Table>"

            Write.Write(FileText)
            Write.Flush()
            Write.Close()

            Return False

        Else

            Try
                Dim rutaSETUP As New FileStream(rutaArchivoExe + "DeviceID.xml", FileMode.Open, FileAccess.Read)
                SETUP.Load(rutaSETUP)

                nodo_SETUP = SETUP.GetElementsByTagName("ID")

                NewID = ValorSetup(nodo_SETUP, 0, Format(Value, "ID000000000"))

                Return True

            Catch ex As Exception
                'MsgBox("Verifique su archivo SETUP. " + ex.Message)
                Return False
            End Try
        End If

    End Function

    Private Function ValorSetup(ByVal Setup As XmlNodeList, ByVal index As Integer, Optional ByVal ValorDefault As String = "") As String

        Dim Nodo As String

        Try
            Nodo = Setup(0).ChildNodes.Item(index).InnerText
        Catch ex As Exception
            Nodo = ValorDefault
        End Try

        Return Nodo

    End Function

End Module
