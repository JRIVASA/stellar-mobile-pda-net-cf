
SELECT 'DELETE FROM MA_USUARIOS;' + CHAR(13)
UNION
select 
'INSERT INTO MA_USUARIOS (codusuario, password, login_name, descripcion, clave, Nivel) ' + 
'VALUES (''' + CODUSUARIO + ''', ''' + password + ''', ''' + login_name + ''', ''' + descripcion + ''', ''' + clave + ''', ' + CAST(Nivel AS NVARCHAR(20)) + '' + 
');' FROM MA_USUARIOS

SELECT 'DELETE FROM MA_PRODUCTOS;' + CHAR(13)
UNION
select 

'INSERT INTO MA_PRODUCTOS (C_CODIGO, C_DESCRI, C_DEPARTAMENTO, C_GRUPO, C_SUBGRUPO, N_COSTOACT, N_COSTOANT, N_COSTOPRO, N_CANTIBUL, N_TIPOPESO, CANT_DECIMALES) ' + 
'VALUES (''' + C_CODIGO + ''', ''' + REPLACE(C_DESCRI, '''', '')  + ''', ''' + C_DEPARTAMENTO + ''', ''' + C_GRUPO + ''', ''' + C_SUBGRUPO + ''', ' + CAST(N_COSTOACT AS NVARCHAR(20)) + ', ' + CAST(N_COSTOANT AS NVARCHAR(20)) + ', ' + CAST(N_COSTOPRO AS NVARCHAR(20)) + ', ' + CAST(N_CANTIBUL AS NVARCHAR(20)) + ', ' + CAST(N_TIPOPESO AS NVARCHAR(20)) + ', ' + CAST(CANT_DECIMALES AS NVARCHAR(20)) + '' + 
');' FROM MA_PRODUCTOS

SELECT 'DELETE FROM MA_CODIGOS;' + CHAR(13)
UNION
select 

'INSERT INTO MA_CODIGOS (C_CODNASA, C_CODIGO, C_DESCRIPCION, N_CANTIDAD) ' + 
'VALUES (''' + C_CODNASA + ''', ''' + C_CODIGO  + ''', ''' + C_DESCRIPCION + ''', ' + CAST(N_CANTIDAD AS NVARCHAR(20)) + '' + 
');' FROM MA_CODIGOS

SELECT 'DELETE FROM CONF_MENU_USER;' + CHAR(13)
UNION
select 

'INSERT INTO CONF_MENU_USER (CLAVE_MENU, CLAVE_USER, TEXTO, ACTIVADO, ICONO, FORMA, RELACION, RESOURCEID) ' + 
'VALUES (''' + CLAVE_MENU + ''', ''' + CLAVE_USER  + ''', ''' + TEXTO + ''', ' + CAST(ACTIVADO AS NVARCHAR(20)) + ', ''' + ICONO  + ''', ''' + FORMA  + ''', ''' + RELACION  + ''', ' + CAST(RESOURCEID AS NVARCHAR(20))  + '' + 
');' FROM CONF_MENU_USER ORDER BY 1


SELECT 'DELETE FROM MA_DEPOSITO;' + CHAR(13)
UNION
select 

'INSERT INTO MA_DEPOSITO (C_CODDEPOSITO, C_DESCRIPCION, C_CODLOCALIDAD) ' + 
'VALUES (''' + C_CODDEPOSITO + ''', ''' + C_DESCRIPCION  + ''', ''' + C_CODLOCALIDAD + '''' + 
');' FROM MA_DEPOSITO

SELECT 'DELETE FROM MA_MONEDAS;' + CHAR(13)
UNION
select 

'INSERT INTO MA_MONEDAS (C_CODMONEDA, N_FACTOR, N_DECIMALES) ' + 
'VALUES (''' + C_CODMONEDA + ''', ' + CAST(N_FACTOR AS NVARCHAR(20)) + ', ' + CAST(N_DECIMALES AS NVARCHAR(20)) + '' + 
');' FROM MA_MONEDAS

SELECT 'DELETE FROM MA_PROCESOS;' + CHAR(13)
UNION
select 

'INSERT INTO MA_PROCESOS (IDPROCESO, PROCESO) ' + 
'VALUES (' + CAST(IDPROCESO AS NVARCHAR(20)) + ', ''' + PROCESO + '''' + 
');' FROM MA_PROCESOS

SELECT 'DELETE FROM MA_CONCEPTOS;' + CHAR(13)
UNION
select 

'INSERT INTO MA_CONCEPTOS (IDCONCEPTO, DESCRIPCION, IDPROCESO) ' + 
'VALUES (' + CAST(IDCONCEPTO AS NVARCHAR(20)) + ', ''' + DESCRIPCION + ''', ' + CAST(IDPROCESO AS NVARCHAR(20)) + '' + 
');' FROM MA_CONCEPTOS

SELECT 'DELETE FROM MA_REGLASDENEGOCIO;' + CHAR(13)
UNION
select 

'INSERT INTO MA_REGLASDENEGOCIO (CAMPO, VALOR) ' + 
'VALUES (''' + CAMPO + ''', ''' + VALOR  + '''' + 
');' FROM MA_REGLASDENEGOCIO

SELECT 'DELETE FROM MA_PROVEEDORES;' + CHAR(13)
UNION
select 

'INSERT INTO MA_PROVEEDORES (C_CODPROVEED, C_DESCRIPCIO, C_RAZON) ' + 
'VALUES (''' + C_CODPROVEED + ''', ''' + REPLACE(C_DESCRIPCIO, '''', '')  + ''', ''' + REPLACE(C_RAZON, '''', '') + '''' + 
');' FROM MA_PROVEEDORES
