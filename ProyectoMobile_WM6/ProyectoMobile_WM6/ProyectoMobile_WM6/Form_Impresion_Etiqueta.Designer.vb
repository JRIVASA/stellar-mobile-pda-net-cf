﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class Form_Impresion_Etiquetas
    Inherits System.Windows.Forms.Form

    'Form invalida a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar con el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form_Impresion_Etiquetas))
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.cmbSalir_etiqueta = New System.Windows.Forms.PictureBox
        Me.lblUsuarioEnSesion = New System.Windows.Forms.Label
        Me.PictureBox2 = New System.Windows.Forms.PictureBox
        Me.picBarraMorada = New System.Windows.Forms.PictureBox
        Me.cmbImprimir = New System.Windows.Forms.Button
        Me.Datagrid_impresion = New System.Windows.Forms.DataGrid
        Me.cmbBuscarImprimir = New System.Windows.Forms.Button
        Me.numero_impresiones = New System.Windows.Forms.NumericUpDown
        Me.lblStatus = New System.Windows.Forms.Label
        Me.cmbVerificarCodigo = New System.Windows.Forms.PictureBox
        Me.txtCod = New System.Windows.Forms.TextBox
        Me.CmbSeleccionarImpresora = New System.Windows.Forms.PictureBox
        Me.SuspendLayout()
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(42, 0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(198, 38)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'cmbSalir_etiqueta
        '
        Me.cmbSalir_etiqueta.Image = CType(resources.GetObject("cmbSalir_etiqueta.Image"), System.Drawing.Image)
        Me.cmbSalir_etiqueta.Location = New System.Drawing.Point(0, 0)
        Me.cmbSalir_etiqueta.Name = "cmbSalir_etiqueta"
        Me.cmbSalir_etiqueta.Size = New System.Drawing.Size(41, 38)
        Me.cmbSalir_etiqueta.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'lblUsuarioEnSesion
        '
        Me.lblUsuarioEnSesion.BackColor = System.Drawing.Color.DarkGray
        Me.lblUsuarioEnSesion.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblUsuarioEnSesion.Location = New System.Drawing.Point(0, 39)
        Me.lblUsuarioEnSesion.Name = "lblUsuarioEnSesion"
        Me.lblUsuarioEnSesion.Size = New System.Drawing.Size(240, 15)
        Me.lblUsuarioEnSesion.Text = "Usuario:"
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.Gainsboro
        Me.PictureBox2.Location = New System.Drawing.Point(0, 54)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(240, 205)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'picBarraMorada
        '
        Me.picBarraMorada.Image = CType(resources.GetObject("picBarraMorada.Image"), System.Drawing.Image)
        Me.picBarraMorada.Location = New System.Drawing.Point(0, 260)
        Me.picBarraMorada.Name = "picBarraMorada"
        Me.picBarraMorada.Size = New System.Drawing.Size(240, 40)
        Me.picBarraMorada.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'cmbImprimir
        '
        Me.cmbImprimir.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.cmbImprimir.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.cmbImprimir.ForeColor = System.Drawing.Color.Black
        Me.cmbImprimir.Location = New System.Drawing.Point(72, 268)
        Me.cmbImprimir.Name = "cmbImprimir"
        Me.cmbImprimir.Size = New System.Drawing.Size(87, 24)
        Me.cmbImprimir.TabIndex = 115
        Me.cmbImprimir.Text = "Imprimir"
        '
        'Datagrid_impresion
        '
        Me.Datagrid_impresion.BackgroundColor = System.Drawing.Color.Gainsboro
        Me.Datagrid_impresion.GridLineColor = System.Drawing.Color.Gainsboro
        Me.Datagrid_impresion.HeaderBackColor = System.Drawing.Color.Gainsboro
        Me.Datagrid_impresion.Location = New System.Drawing.Point(19, 154)
        Me.Datagrid_impresion.Name = "Datagrid_impresion"
        Me.Datagrid_impresion.Size = New System.Drawing.Size(202, 84)
        Me.Datagrid_impresion.TabIndex = 116
        '
        'cmbBuscarImprimir
        '
        Me.cmbBuscarImprimir.BackColor = System.Drawing.Color.White
        Me.cmbBuscarImprimir.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.cmbBuscarImprimir.ForeColor = System.Drawing.Color.Black
        Me.cmbBuscarImprimir.Location = New System.Drawing.Point(19, 92)
        Me.cmbBuscarImprimir.Name = "cmbBuscarImprimir"
        Me.cmbBuscarImprimir.Size = New System.Drawing.Size(108, 24)
        Me.cmbBuscarImprimir.TabIndex = 117
        Me.cmbBuscarImprimir.Text = "Buscar Producto"
        '
        'numero_impresiones
        '
        Me.numero_impresiones.Location = New System.Drawing.Point(133, 92)
        Me.numero_impresiones.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.numero_impresiones.Name = "numero_impresiones"
        Me.numero_impresiones.Size = New System.Drawing.Size(77, 24)
        Me.numero_impresiones.TabIndex = 200
        Me.numero_impresiones.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'lblStatus
        '
        Me.lblStatus.BackColor = System.Drawing.Color.Gainsboro
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.lblStatus.Location = New System.Drawing.Point(19, 65)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(202, 14)
        Me.lblStatus.Text = "Etiquetas"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'cmbVerificarCodigo
        '
        Me.cmbVerificarCodigo.Image = CType(resources.GetObject("cmbVerificarCodigo.Image"), System.Drawing.Image)
        Me.cmbVerificarCodigo.Location = New System.Drawing.Point(181, 125)
        Me.cmbVerificarCodigo.Name = "cmbVerificarCodigo"
        Me.cmbVerificarCodigo.Size = New System.Drawing.Size(29, 23)
        Me.cmbVerificarCodigo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'txtCod
        '
        Me.txtCod.Location = New System.Drawing.Point(19, 125)
        Me.txtCod.Name = "txtCod"
        Me.txtCod.Size = New System.Drawing.Size(156, 23)
        Me.txtCod.TabIndex = 209
        '
        'CmbSeleccionarImpresora
        '
        Me.CmbSeleccionarImpresora.Image = CType(resources.GetObject("CmbSeleccionarImpresora.Image"), System.Drawing.Image)
        Me.CmbSeleccionarImpresora.Location = New System.Drawing.Point(22, 268)
        Me.CmbSeleccionarImpresora.Name = "CmbSeleccionarImpresora"
        Me.CmbSeleccionarImpresora.Size = New System.Drawing.Size(29, 23)
        Me.CmbSeleccionarImpresora.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'Form_Impresion_Etiquetas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.ClientSize = New System.Drawing.Size(240, 300)
        Me.ControlBox = False
        Me.Controls.Add(Me.CmbSeleccionarImpresora)
        Me.Controls.Add(Me.cmbVerificarCodigo)
        Me.Controls.Add(Me.txtCod)
        Me.Controls.Add(Me.lblStatus)
        Me.Controls.Add(Me.numero_impresiones)
        Me.Controls.Add(Me.cmbBuscarImprimir)
        Me.Controls.Add(Me.Datagrid_impresion)
        Me.Controls.Add(Me.cmbImprimir)
        Me.Controls.Add(Me.picBarraMorada)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.lblUsuarioEnSesion)
        Me.Controls.Add(Me.cmbSalir_etiqueta)
        Me.Controls.Add(Me.PictureBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Form_Impresion_Etiquetas"
        Me.Text = "isMOBILE - Etiquetas"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents cmbSalir_etiqueta As System.Windows.Forms.PictureBox
    Friend WithEvents lblUsuarioEnSesion As System.Windows.Forms.Label
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents picBarraMorada As System.Windows.Forms.PictureBox
    Friend WithEvents cmbImprimir As System.Windows.Forms.Button
    Friend WithEvents Datagrid_impresion As System.Windows.Forms.DataGrid
    Friend WithEvents cmbBuscarImprimir As System.Windows.Forms.Button
    Friend WithEvents numero_impresiones As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents cmbVerificarCodigo As System.Windows.Forms.PictureBox
    Friend WithEvents txtCod As System.Windows.Forms.TextBox
    Friend WithEvents CmbSeleccionarImpresora As System.Windows.Forms.PictureBox
End Class
