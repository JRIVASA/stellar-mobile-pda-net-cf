﻿Public Class FormLecturaProducto_Traslado

    Dim empaques, unidades As Double
    Dim dataset_Codigos, dataset_Productos As Data.DataSet
    Public Cancelando_O_Entrando As Boolean

    Private Sub FormLecturaProducto_Traslado_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Activated

        TimerReload.Interval = 100
        TimerReload.Enabled = True

    End Sub

    'Private CantDecimalesProducto As Integer

    Private Sub FormLecturaProducto_Traslado_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If CompatibilidadFueraDeLinea Then
            cmbActualizar.Visible = True
        Else
            cmbActualizar.Visible = False
        End If

        AjustarFormularios(Me)

        Cancelando_O_Entrando = True

    End Sub

    'Private Sub txtCodigoProducto_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCodigoProducto.GotFocus
    ' cmbTeclado.Visible = True
    'End Sub

    Private Sub txtCodigoProducto_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCodigoProducto.KeyDown

        Select Case e.KeyValue

            Case Is = 13

                ProgressBar_FormLecturaProducto_Traslado.Visible = True
                ProgressBar_FormLecturaProducto_Traslado.Value = 10

                'Cuando el txtCodigoProducto pierde el foco y no esta vacio; se verifica que el codigo existe
                If Not (txtCodigoProducto.Text = "") Then

                    If txtCodigoProducto.TextLength <= 15 Then

                        dataset_Codigos = buscarCodigoProducto(txtCodigoProducto.Text)

                        If (dataset_Codigos Is Nothing) Then
                            MsgBox("La información del producto no se ha cargado. Ingrese el producto nuevamente.", MsgBoxStyle.Information, "isMOBILE")
                            txtCodigoProducto.Focus()
                            Return
                        End If

                        ProgressBar_FormLecturaProducto_Traslado.Value = 30

                        'Se verifica que la consulta trajo resultados o no
                        If dataset_Codigos.Tables(0).Rows.Count() = 0 Then

                            MsgBox("El código de producto no existe", MsgBoxStyle.Information, "isMOBILE")

                            txtCodigoProducto.Text = ""
                            lblDescripcion.Text = "Descripción: "
                            txtCodigoProducto.Focus()

                            ProgressBar_FormLecturaProducto_Traslado.Value = 100
                            ProgressBar_FormLecturaProducto_Traslado.Visible = False

                            Exit Sub

                        Else

                            txtCodigoProducto.Text = dataset_Codigos.Tables(0).Rows(0)("C_CODNASA")
                            dataset_Productos = detallesProducto(txtCodigoProducto.Text)

                            If (dataset_Productos Is Nothing) Then
                                MsgBox("La información del producto no se ha cargado. Ingrese el producto nuevamente.", MsgBoxStyle.Information, "isMOBILE")
                                txtCodigoProducto.Focus()
                                Return
                            ElseIf (dataset_Productos.Tables(0).Rows.Count <= 0) Then
                                MsgBox("La información del producto no se ha cargado. Ingrese el producto nuevamente.", MsgBoxStyle.Information, "isMOBILE")
                                txtCodigoProducto.Focus()
                                Return
                            End If

                            lblDescripcion.Text = "Descripción: " + dataset_Productos.Tables(0).Rows(0)("C_DESCRI")

                            ProgressBar_FormLecturaProducto_Traslado.Value = 50
                            UnidadesxEmpaque = determinarUnidadesxEmpaque(dataset_Codigos, dataset_Productos)
                            lblUnidadesxEmpaque.Text = "Uni.x Emp.: " + CStr(UnidadesxEmpaque)

                            ProgressBar_FormLecturaProducto_Traslado.Value = 70

                            If txtEmpaques.Visible = True Then
                                txtEmpaques.Focus()
                            Else
                                txtUnidades.Focus()
                            End If

                            ProgressBar_FormLecturaProducto_Traslado.Value = 100
                            ProgressBar_FormLecturaProducto_Traslado.Visible = False

                        End If

                    Else
                        MsgBox("Código inválido.", MsgBoxStyle.Information, "isMOBILE")
                        txtCodigoProducto.Text = ""
                        txtCodigoProducto.Focus()
                    End If

                    ProgressBar_FormLecturaProducto_Traslado.Value = 100
                    ProgressBar_FormLecturaProducto_Traslado.Visible = False

                End If

        End Select

    End Sub

    'Private Sub txtCodigoProducto_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCodigoProducto.LostFocus

    '    'cmbTeclado.Visible = False

    '    ProgressBar_FormLecturaProducto_Traslado.Visible = True
    '    ProgressBar_FormLecturaProducto_Traslado.Value = 10

    '    'Cuando el txtCodigoProducto pierde el foco y no esta vacio; se verifica que el codigo existe
    '    If Not (txtCodigoProducto.Text = "") Then

    '        If txtCodigoProducto.TextLength <= 15 Then

    '            dataset_Codigos = buscarCodigoProducto(txtCodigoProducto.Text)

    '            ProgressBar_FormLecturaProducto_Traslado.Value = 30

    '            'Se verifica que la consulta trajo resultados o no
    '            If dataset_Codigos.Tables(0).Rows.Count() = 0 Then

    '                MsgBox("El código de producto no existe.", MsgBoxStyle.Information, "isMOBILE")
    '                txtCodigoProducto.Text = ""
    '                lblDescripcion.Text = "Descripción: "
    '                txtCodigoProducto.Focus()

    '                ProgressBar_FormLecturaProducto_Traslado.Value = 100
    '                ProgressBar_FormLecturaProducto_Traslado.Visible = False
    '                Exit Sub
    '            Else
    '                txtCodigoProducto.Text = dataset_Codigos.Tables(0).Rows(0)("C_CODNASA")
    '                dataset_Productos = detallesProducto(txtCodigoProducto.Text)
    '                lblDescripcion.Text = "Descripción: " + dataset_Productos.Tables(0).Rows(0)("C_DESCRI")

    '                ProgressBar_FormLecturaProducto_Traslado.Value = 50
    '                UnidadesxEmpaque = determinarUnidadesxEmpaque(dataset_Codigos, dataset_Productos)
    '                lblUnidadesxEmpaque.Text = "Uni.x Emp.: " + CStr(UnidadesxEmpaque)

    '                ProgressBar_FormLecturaProducto_Traslado.Value = 70

    '                If txtEmpaques.Visible = True Then
    '                    txtEmpaques.Focus()
    '                Else
    '                    txtUnidades.Focus()
    '                End If

    '                ProgressBar_FormLecturaProducto_Traslado.Value = 100
    '                ProgressBar_FormLecturaProducto_Traslado.Visible = False
    '            End If
    '        Else
    '            MsgBox("Código inválido.", MsgBoxStyle.Information, "isMOBILE")
    '            txtCodigoProducto.Text = ""
    '            txtCodigoProducto.Focus()
    '        End If

    '        ProgressBar_FormLecturaProducto_Traslado.Value = 100
    '        ProgressBar_FormLecturaProducto_Traslado.Visible = False

    '    End If

    'End Sub

    Private Sub cmbAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbAgregar.Click

        If txtCodigoProducto.Text = "" Then
            MsgBox("No ha ingresado ningún producto.", MsgBoxStyle.Information, "isMOBILE")
            txtCodigoProducto.Focus()
            Exit Sub
        End If

        If (dataset_Productos Is Nothing Or dataset_Codigos Is Nothing) Then
            MsgBox("La información del producto no se ha cargado. Ingrese el producto nuevamente.", MsgBoxStyle.Information, "isMOBILE")
            txtCodigoProducto.Focus()
            Return
        ElseIf (dataset_Productos.Tables(0).Rows.Count <= 0 Or dataset_Codigos.Tables(0).Rows.Count <= 0) Then
            MsgBox("La información del producto no se ha cargado. Ingrese el producto nuevamente.", MsgBoxStyle.Information, "isMOBILE")
            txtCodigoProducto.Focus()
            Return
        End If

        'RegladeNegocio = "Trs_FormadeTrabajo"
        'dataset_RegladeNegocio = consultarRegladeNegocio(RegladeNegocio)
        'obtenerRegladeNegocio_Servidor("Trs_FormadeTrabajo", "2")
        valorRegladeNegocio = obtenerRegladeNegocio_Servidor("Trs_FormadeTrabajo", "2") 'Val(dataset_RegladeNegocio.Tables(0).Rows(0)("valor"))

        If valorRegladeNegocio = "0" Then 'Trabaja solo con unidades

            If txtUnidades.Text = "" Then
                MsgBox("Debe ingresar la cantidad de unidades.", MsgBoxStyle.Information, "isMOBILE")
                txtUnidades.Focus()
            Else
                If Not IsNumeric(txtUnidades.Text) Then
                    MsgBox("Los datos deben ser numéricos.", MsgBoxStyle.Information, "isMOBILE")
                    txtUnidades.Text = ""
                    txtUnidades.Focus()
                Else

                    txtEmpaques.Text = 0 'A pesar de que no está visible, esto es para usar esta variable en el form de GuardarDatos
                    unidades = CDbl(txtUnidades.Text)
                    empaques = CDbl(txtEmpaques.Text)

                    '************* AÑADE EL PRODUCTO A LAS TABLAS CORRESPONDIENTES ***************

                    ReglasdeNegocioAgregarProducto_Traslados(unidades, empaques)

                    '*************************************************************************************

                End If

            End If

        Else 'Si el valor de la regla de negocio no es cero (0)

            If valorRegladeNegocio = "1" Then 'Trabaja solo con Empaques

                If txtEmpaques.Text = "" Then
                    MsgBox("Debe ingresar la cantidad de empaques.", MsgBoxStyle.Information, "isMOBILE")
                    txtEmpaques.Focus()
                Else
                    If Not IsNumeric(txtEmpaques.Text) Then
                        MsgBox("Los datos deben ser numéricos.", MsgBoxStyle.Information, "isMOBILE")
                        txtEmpaques.Text = ""
                        txtEmpaques.Focus()
                    Else

                        txtUnidades.Text = 0
                        empaques = CDbl(txtEmpaques.Text)
                        unidades = CDbl(txtUnidades.Text)

                        '************* AÑADE EL PRODUCTO A LAS TABLAS CORRESPONDIENTES ***************

                        ReglasdeNegocioAgregarProducto_Traslados(unidades, empaques)

                        '*************************************************************************************

                    End If

                End If

            Else 'si el valor de la regla de negocio es igual a 2 (Trabaja con unidades y/o empaques)

                'hay 4 opciones:
                '1.- Que tanto empaques como unidades estén vacíos
                If txtEmpaques.Text = "" And txtUnidades.Text = "" Then
                    MsgBox("Debe ingresar la cantidad de empaques y/o unidades.", MsgBoxStyle.Information, "isMOBILE")
                    txtEmpaques.Focus()
                Else
                    '2.- Que Empaques esté vacío y Unidades esté lleno
                    If (txtEmpaques.Text = "" And txtUnidades.Text <> "") Then

                        If Not IsNumeric(txtUnidades.Text) Then
                            MsgBox("Los datos deben ser numéricos.", MsgBoxStyle.Information, "isMOBILE")
                            txtUnidades.Text = ""
                            txtUnidades.Focus()
                        Else

                            txtEmpaques.Text = 0
                            unidades = CDbl(txtUnidades.Text)
                            empaques = CDbl(txtEmpaques.Text)

                            '************* AÑADE EL PRODUCTO A LAS TABLAS CORRESPONDIENTES ***************

                            ReglasdeNegocioAgregarProducto_Traslados(unidades, empaques)

                            '*************************************************************************************

                        End If

                    Else

                        '3.- Que Empaques esté lleno y Unidades esté vacío 
                        If (txtEmpaques.Text <> "" And txtUnidades.Text = "") Then

                            If Not IsNumeric(txtEmpaques.Text) Then
                                MsgBox("Los datos deben ser numéricos.", MsgBoxStyle.Information, "isMOBILE")
                                txtEmpaques.Text = ""
                                txtEmpaques.Focus()
                            Else

                                txtUnidades.Text = 0
                                empaques = CDbl(txtEmpaques.Text)
                                unidades = CDbl(txtUnidades.Text)

                                '************* AÑADE EL PRODUCTO A LAS TABLAS CORRESPONDIENTES ***************

                                ReglasdeNegocioAgregarProducto_Traslados(unidades, empaques)

                                '*************************************************************************************

                            End If
                        Else

                            '4.- Que tanto Empaques como Unidades estén llenos 

                            If (txtEmpaques.Text <> "" And txtUnidades.Text <> "") Then

                                If IsNumeric(txtUnidades.Text) Then

                                    If IsNumeric(txtEmpaques.Text) Then

                                        empaques = CDbl(txtEmpaques.Text)
                                        unidades = CDbl(txtUnidades.Text)

                                        '************* AÑADE EL PRODUCTO A LAS TABLAS CORRESPONDIENTES ***************

                                        ReglasdeNegocioAgregarProducto_Traslados(unidades, empaques)

                                        '*************************************************************************************
                                    Else
                                        MsgBox("Los datos deben ser numéricos.", MsgBoxStyle.Information, "isMOBILE")
                                        txtEmpaques.Text = ""
                                        txtEmpaques.Focus()
                                    End If

                                Else
                                    MsgBox("Los datos deben ser numéricos.", MsgBoxStyle.Information, "isMOBILE")
                                    txtUnidades.Text = ""
                                    txtUnidades.Focus()
                                End If

                            End If

                        End If

                    End If

                End If

            End If

        End If

    End Sub

    Private Sub cmbSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSalir.Click
        respuesta = MsgBox("¿Está seguro que desea regresar? Si lo hace el traslado actual será cancelado.", MsgBoxStyle.YesNo, "Finalizar Traslado")

        If respuesta = 6 Then
            Cancelando_O_Entrando = True 'comandBD_PDA.CommandText = "DELETE TR_TRASLADOS_PDA" : ejecutaQuery_PDA(comandBD_PDA)
            FormEjecutorMotivo_Traslado.Show()
            Me.Visible = False
            'Me.Close()
        End If
    End Sub

    Private Sub cmbFinalizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbFinalizar.Click

        If verificaExistenTraslados(numeroTraslado_SERVIDOR) = 0 Then
            'Limpia el formulario y no genera documento de inventario
            limpiarFormLecturaProducto_Traslado()
            txtCodigoProducto.Focus()
            MsgBox("Aún no ha realizado el traslado de de ningún producto.", MsgBoxStyle.Information, "isMOBILE")
        Else
            respuesta = MsgBox("¿Está seguro que desea finalizar el traslado?", MsgBoxStyle.YesNo, "Finalizar Traslado")
            If respuesta = 6 Then
                FormObservacion_Traslado.Show()
                Me.Visible = False
            End If
        End If

    End Sub

    Private Sub txtEmpaques_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtEmpaques.KeyPress
        'If Char.IsNumber(e.KeyChar) Then 'Comentado, no permitía ingresar . para valores decimales
        'MsgBox("-!" & txtEmpaques.Text & e.KeyChar.ToString & "!-") Nueva forma de Validar Aprobada.
        If IsNumeric(txtEmpaques.Text & e.KeyChar.ToString) Then
            e.Handled = False
        Else
            If Char.IsControl(e.KeyChar) Then
                e.Handled = False
            Else
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txtUnidades_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtUnidades.KeyPress
        'If Char.IsNumber(e.KeyChar) Then 'Comentado, no permitía ingresar . para valores decimales
        'MsgBox("-!" & txtUnidades.Text & e.KeyChar.ToString & "!-") Nueva forma de Validar Aprobada.
        If IsNumeric(txtUnidades.Text & e.KeyChar.ToString) Then
            e.Handled = False
        Else
            If Char.IsControl(e.KeyChar) Then
                e.Handled = False
            Else
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txtEmpaques_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEmpaques.LostFocus
        Dim TmpCantDec As Short
        Dim TmpText As String
        If IsNumeric(txtEmpaques.Text) And Not (dataset_Productos Is Nothing) Then
            If dataset_Productos.Tables(0).Rows.Count <> 0 Then
                TmpText = txtEmpaques.Text
                TmpCantDec = CShort(Val(dataset_Productos.Tables(0).Rows(0)("CANT_DECIMALES")))
                txtEmpaques.Text = FormatNumber(txtEmpaques.Text, TmpCantDec, TriState.True, TriState.False, TriState.False)
                If TmpText <> txtEmpaques.Text Then txtEmpaques.Focus()
            End If
        End If
    End Sub

    Private Sub txtUnidades_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUnidades.LostFocus
        Dim TmpCantDec As Short
        Dim TmpText As String
        If IsNumeric(txtUnidades.Text) And Not (dataset_Productos Is Nothing) Then
            If dataset_Productos.Tables(0).Rows.Count <> 0 Then
                TmpText = txtUnidades.Text
                TmpCantDec = CShort(Val(dataset_Productos.Tables(0).Rows(0)("CANT_DECIMALES")))
                txtUnidades.Text = FormatNumber(txtUnidades.Text, TmpCantDec, TriState.True, TriState.False, TriState.False)
                If TmpText <> txtUnidades.Text Then txtUnidades.Focus()
            End If
        End If
    End Sub
    
    Private Sub cmbActualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbActualizar.Click
        If ActualizacionParcial("Productos") Then
            MsgBox("Actualización parcial de Productos completada.", MsgBoxStyle.Information, "isMOBILE")
        Else
            MsgBox("No se pudo realizar la Actualización parcial de Productos.", MsgBoxStyle.Information, "isMOBILE")
        End If
    End Sub

    Private Sub Activation()

        If Cancelando_O_Entrando Then

            comandBD_PDA.CommandText = "DELETE TR_TRASLADOS_PDA"
            ejecutaQuery_PDA(comandBD_PDA)

            Dim numeroTraslado_PDA As String

            If Not construirNumeroTraslado_PDA() = "0" Then
                numeroTraslado_PDA = construirNumeroTraslado_PDA()
                Me.lblNºTrs.Text = numeroTraslado_PDA
                FormObservacion_Traslado.lblNºTrs.Text = numeroTraslado_PDA

                Me.lblUsuarioEnSesion.Text = "Usuario: " + descripcionUsuario
                Me.Refresh()
                Me.Update()

                txtCodigoProducto.Focus()

                numeroTraslado_SERVIDORsinFormato = determinarNumeroTraslado_SERVIDOR()
                numeroTraslado_SERVIDOR = Format(numeroTraslado_SERVIDORsinFormato, "00000000#")

                lineaTraslado = verificaProductosTraslados(numeroTraslado_SERVIDOR)

                RegladeNegocio = "Trs_FormadeTrabajo"

                dataset_RegladeNegocio = consultarRegladeNegocio(RegladeNegocio)

                If (dataset_RegladeNegocio Is Nothing) Then
                    MsgBox("Ha ocurrido un error al cargar las reglas de negocio.", MsgBoxStyle.Information, "isMOBILE")
                    Cancelando_O_Entrando = True 'comandBD_PDA.CommandText = "DELETE TR_TRASLADOS_PDA" : ejecutaQuery_PDA(comandBD_PDA)
                    FormEjecutorMotivo_Traslado.Show()
                    Me.Visible = False
                End If

                If dataset_RegladeNegocio.Tables(0).Rows.Count = 0 Then
                    MsgBox("No existe la Regla de Negocio Trs_FormadeTrabajo.", MsgBoxStyle.Information, "isMOBILE")
                Else
                    valorRegladeNegocio = CInt(dataset_RegladeNegocio.Tables(0).Rows(0)("valor"))
                    If valorRegladeNegocio = 0 Then 'Trabaja solo con unidades
                        txtEmpaques.Visible = False
                        lblEmpaques.Visible = False
                        lblUnidadesxEmpaque.Visible = False
                    Else
                        If valorRegladeNegocio = 1 Then 'Trabaja solo con empaques
                            txtUnidades.Visible = False
                            lblUnidades.Visible = False
                            'txtEmpaques.Focus()
                        Else 'Trabaja con empaques y unidades
                        End If

                    End If
                End If

                Cancelando_O_Entrando = False
            Else
                Cancelando_O_Entrando = True
                MsgBox("No se puede establecer el número del traslado, no hay conexión con el servidor.", MsgBoxStyle.Exclamation, "Número de Traslado")
                FormEjecutorMotivo_Traslado.Show()
                Me.Visible = False
                'Me.Close()
            End If

        Else
            'Console.WriteLine("Evento activado por volver a obtener el fóco.")
            Me.txtCodigoProducto.Focus()
        End If

    End Sub

    Private Sub TimerReload_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TimerReload.Tick
        TimerReload.Enabled = False
        Activation()
    End Sub

    Private Sub txtCodigoProducto_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCodigoProducto.TextChanged

    End Sub

End Class