﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class FormDepositoOrigenDestino_Traslado
    Inherits System.Windows.Forms.Form

    'Form invalida a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer
    Private mainMenu1 As System.Windows.Forms.MainMenu

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar con el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormDepositoOrigenDestino_Traslado))
        Me.mainMenu1 = New System.Windows.Forms.MainMenu
        Me.lblUsuarioEnSesion = New System.Windows.Forms.Label
        Me.cmbVerificarDepositoDestino = New System.Windows.Forms.PictureBox
        Me.cmbBuscarDepositoDestino = New System.Windows.Forms.PictureBox
        Me.txtDepositoDestino = New System.Windows.Forms.TextBox
        Me.lblDepositoDestino = New System.Windows.Forms.Label
        Me.lblDescripcionDepositoDestino = New System.Windows.Forms.Label
        Me.cmbVerificarDepositoOrigen = New System.Windows.Forms.PictureBox
        Me.cmbBuscarDepositoOrigen = New System.Windows.Forms.PictureBox
        Me.txtDepositoOrigen = New System.Windows.Forms.TextBox
        Me.lblDepositoOrigen = New System.Windows.Forms.Label
        Me.lblDescripcionDepositoOrigen = New System.Windows.Forms.Label
        Me.cmbSiguiente = New System.Windows.Forms.Button
        Me.cmbCancelar = New System.Windows.Forms.Button
        Me.cmbSalir = New System.Windows.Forms.PictureBox
        Me.PictureFondoProveedorDeposito_Recepcion = New System.Windows.Forms.PictureBox
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.picBarraMorada = New System.Windows.Forms.PictureBox
        Me.SuspendLayout()
        '
        'lblUsuarioEnSesion
        '
        Me.lblUsuarioEnSesion.BackColor = System.Drawing.Color.DarkGray
        Me.lblUsuarioEnSesion.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblUsuarioEnSesion.Location = New System.Drawing.Point(0, 39)
        Me.lblUsuarioEnSesion.Name = "lblUsuarioEnSesion"
        Me.lblUsuarioEnSesion.Size = New System.Drawing.Size(240, 15)
        Me.lblUsuarioEnSesion.Text = "Usuario:"
        '
        'cmbVerificarDepositoDestino
        '
        Me.cmbVerificarDepositoDestino.Image = CType(resources.GetObject("cmbVerificarDepositoDestino.Image"), System.Drawing.Image)
        Me.cmbVerificarDepositoDestino.Location = New System.Drawing.Point(157, 183)
        Me.cmbVerificarDepositoDestino.Name = "cmbVerificarDepositoDestino"
        Me.cmbVerificarDepositoDestino.Size = New System.Drawing.Size(29, 29)
        Me.cmbVerificarDepositoDestino.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'cmbBuscarDepositoDestino
        '
        Me.cmbBuscarDepositoDestino.Image = CType(resources.GetObject("cmbBuscarDepositoDestino.Image"), System.Drawing.Image)
        Me.cmbBuscarDepositoDestino.Location = New System.Drawing.Point(192, 183)
        Me.cmbBuscarDepositoDestino.Name = "cmbBuscarDepositoDestino"
        Me.cmbBuscarDepositoDestino.Size = New System.Drawing.Size(29, 29)
        Me.cmbBuscarDepositoDestino.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'txtDepositoDestino
        '
        Me.txtDepositoDestino.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.txtDepositoDestino.Font = New System.Drawing.Font("Verdana", 14.0!, System.Drawing.FontStyle.Bold)
        Me.txtDepositoDestino.Location = New System.Drawing.Point(29, 183)
        Me.txtDepositoDestino.MaxLength = 50
        Me.txtDepositoDestino.Name = "txtDepositoDestino"
        Me.txtDepositoDestino.Size = New System.Drawing.Size(122, 29)
        Me.txtDepositoDestino.TabIndex = 46
        '
        'lblDepositoDestino
        '
        Me.lblDepositoDestino.BackColor = System.Drawing.Color.Gainsboro
        Me.lblDepositoDestino.Font = New System.Drawing.Font("Tahoma", 14.0!, System.Drawing.FontStyle.Bold)
        Me.lblDepositoDestino.Location = New System.Drawing.Point(27, 155)
        Me.lblDepositoDestino.Name = "lblDepositoDestino"
        Me.lblDepositoDestino.Size = New System.Drawing.Size(195, 25)
        Me.lblDepositoDestino.Text = "Depósito Destino"
        '
        'lblDescripcionDepositoDestino
        '
        Me.lblDescripcionDepositoDestino.BackColor = System.Drawing.Color.Gainsboro
        Me.lblDescripcionDepositoDestino.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Regular)
        Me.lblDescripcionDepositoDestino.Location = New System.Drawing.Point(27, 216)
        Me.lblDescripcionDepositoDestino.Name = "lblDescripcionDepositoDestino"
        Me.lblDescripcionDepositoDestino.Size = New System.Drawing.Size(186, 31)
        Me.lblDescripcionDepositoDestino.Text = "Descripción:"
        '
        'cmbVerificarDepositoOrigen
        '
        Me.cmbVerificarDepositoOrigen.Image = CType(resources.GetObject("cmbVerificarDepositoOrigen.Image"), System.Drawing.Image)
        Me.cmbVerificarDepositoOrigen.Location = New System.Drawing.Point(157, 95)
        Me.cmbVerificarDepositoOrigen.Name = "cmbVerificarDepositoOrigen"
        Me.cmbVerificarDepositoOrigen.Size = New System.Drawing.Size(29, 29)
        Me.cmbVerificarDepositoOrigen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'cmbBuscarDepositoOrigen
        '
        Me.cmbBuscarDepositoOrigen.Image = CType(resources.GetObject("cmbBuscarDepositoOrigen.Image"), System.Drawing.Image)
        Me.cmbBuscarDepositoOrigen.Location = New System.Drawing.Point(192, 95)
        Me.cmbBuscarDepositoOrigen.Name = "cmbBuscarDepositoOrigen"
        Me.cmbBuscarDepositoOrigen.Size = New System.Drawing.Size(29, 29)
        Me.cmbBuscarDepositoOrigen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.cmbBuscarDepositoOrigen.Tag = "Buscar"
        '
        'txtDepositoOrigen
        '
        Me.txtDepositoOrigen.Font = New System.Drawing.Font("Verdana", 14.0!, System.Drawing.FontStyle.Bold)
        Me.txtDepositoOrigen.Location = New System.Drawing.Point(29, 95)
        Me.txtDepositoOrigen.MaxLength = 50
        Me.txtDepositoOrigen.Name = "txtDepositoOrigen"
        Me.txtDepositoOrigen.Size = New System.Drawing.Size(122, 29)
        Me.txtDepositoOrigen.TabIndex = 43
        '
        'lblDepositoOrigen
        '
        Me.lblDepositoOrigen.BackColor = System.Drawing.Color.Gainsboro
        Me.lblDepositoOrigen.Font = New System.Drawing.Font("Tahoma", 14.0!, System.Drawing.FontStyle.Bold)
        Me.lblDepositoOrigen.Location = New System.Drawing.Point(27, 68)
        Me.lblDepositoOrigen.Name = "lblDepositoOrigen"
        Me.lblDepositoOrigen.Size = New System.Drawing.Size(166, 24)
        Me.lblDepositoOrigen.Text = "Depósito Origen"
        '
        'lblDescripcionDepositoOrigen
        '
        Me.lblDescripcionDepositoOrigen.BackColor = System.Drawing.Color.Gainsboro
        Me.lblDescripcionDepositoOrigen.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Regular)
        Me.lblDescripcionDepositoOrigen.Location = New System.Drawing.Point(27, 128)
        Me.lblDescripcionDepositoOrigen.Name = "lblDescripcionDepositoOrigen"
        Me.lblDescripcionDepositoOrigen.Size = New System.Drawing.Size(186, 27)
        Me.lblDescripcionDepositoOrigen.Text = "Descripción:"
        '
        'cmbSiguiente
        '
        Me.cmbSiguiente.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.cmbSiguiente.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.cmbSiguiente.ForeColor = System.Drawing.Color.Black
        Me.cmbSiguiente.Location = New System.Drawing.Point(128, 266)
        Me.cmbSiguiente.Name = "cmbSiguiente"
        Me.cmbSiguiente.Size = New System.Drawing.Size(78, 25)
        Me.cmbSiguiente.TabIndex = 44
        Me.cmbSiguiente.Text = "Siguiente"
        '
        'cmbCancelar
        '
        Me.cmbCancelar.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.cmbCancelar.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.cmbCancelar.ForeColor = System.Drawing.Color.Black
        Me.cmbCancelar.Location = New System.Drawing.Point(29, 266)
        Me.cmbCancelar.Name = "cmbCancelar"
        Me.cmbCancelar.Size = New System.Drawing.Size(78, 25)
        Me.cmbCancelar.TabIndex = 45
        Me.cmbCancelar.Text = "Cancelar"
        '
        'cmbSalir
        '
        Me.cmbSalir.Image = CType(resources.GetObject("cmbSalir.Image"), System.Drawing.Image)
        Me.cmbSalir.Location = New System.Drawing.Point(0, 0)
        Me.cmbSalir.Name = "cmbSalir"
        Me.cmbSalir.Size = New System.Drawing.Size(41, 38)
        Me.cmbSalir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'PictureFondoProveedorDeposito_Recepcion
        '
        Me.PictureFondoProveedorDeposito_Recepcion.Image = CType(resources.GetObject("PictureFondoProveedorDeposito_Recepcion.Image"), System.Drawing.Image)
        Me.PictureFondoProveedorDeposito_Recepcion.Location = New System.Drawing.Point(42, 0)
        Me.PictureFondoProveedorDeposito_Recepcion.Name = "PictureFondoProveedorDeposito_Recepcion"
        Me.PictureFondoProveedorDeposito_Recepcion.Size = New System.Drawing.Size(198, 38)
        Me.PictureFondoProveedorDeposito_Recepcion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Gainsboro
        Me.PictureBox1.Location = New System.Drawing.Point(0, 57)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(240, 203)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'picBarraMorada
        '
        Me.picBarraMorada.Image = CType(resources.GetObject("picBarraMorada.Image"), System.Drawing.Image)
        Me.picBarraMorada.Location = New System.Drawing.Point(0, 260)
        Me.picBarraMorada.Name = "picBarraMorada"
        Me.picBarraMorada.Size = New System.Drawing.Size(240, 40)
        Me.picBarraMorada.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'FormDepositoOrigenDestino_Traslado
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(240, 300)
        Me.ControlBox = False
        Me.Controls.Add(Me.cmbSiguiente)
        Me.Controls.Add(Me.cmbCancelar)
        Me.Controls.Add(Me.picBarraMorada)
        Me.Controls.Add(Me.cmbVerificarDepositoDestino)
        Me.Controls.Add(Me.cmbBuscarDepositoDestino)
        Me.Controls.Add(Me.cmbVerificarDepositoOrigen)
        Me.Controls.Add(Me.cmbBuscarDepositoOrigen)
        Me.Controls.Add(Me.txtDepositoDestino)
        Me.Controls.Add(Me.lblDepositoDestino)
        Me.Controls.Add(Me.lblDescripcionDepositoDestino)
        Me.Controls.Add(Me.txtDepositoOrigen)
        Me.Controls.Add(Me.lblDepositoOrigen)
        Me.Controls.Add(Me.lblDescripcionDepositoOrigen)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.lblUsuarioEnSesion)
        Me.Controls.Add(Me.cmbSalir)
        Me.Controls.Add(Me.PictureFondoProveedorDeposito_Recepcion)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormDepositoOrigenDestino_Traslado"
        Me.Text = "isMOBILE - Traslados"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblUsuarioEnSesion As System.Windows.Forms.Label
    Friend WithEvents cmbVerificarDepositoDestino As System.Windows.Forms.PictureBox
    Friend WithEvents cmbBuscarDepositoDestino As System.Windows.Forms.PictureBox
    Friend WithEvents txtDepositoDestino As System.Windows.Forms.TextBox
    Friend WithEvents lblDepositoDestino As System.Windows.Forms.Label
    Friend WithEvents lblDescripcionDepositoDestino As System.Windows.Forms.Label
    Friend WithEvents cmbVerificarDepositoOrigen As System.Windows.Forms.PictureBox
    Friend WithEvents cmbBuscarDepositoOrigen As System.Windows.Forms.PictureBox
    Friend WithEvents txtDepositoOrigen As System.Windows.Forms.TextBox
    Friend WithEvents lblDepositoOrigen As System.Windows.Forms.Label
    Friend WithEvents lblDescripcionDepositoOrigen As System.Windows.Forms.Label
    Friend WithEvents cmbSiguiente As System.Windows.Forms.Button
    Friend WithEvents cmbCancelar As System.Windows.Forms.Button
    Friend WithEvents cmbSalir As System.Windows.Forms.PictureBox
    Friend WithEvents PictureFondoProveedorDeposito_Recepcion As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents picBarraMorada As System.Windows.Forms.PictureBox
End Class
