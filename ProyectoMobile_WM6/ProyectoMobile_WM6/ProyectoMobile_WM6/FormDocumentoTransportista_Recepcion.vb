﻿Public Class FormDocumentoTransportista_Recepcion

    Private Sub txtDocumento_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDocumento.LostFocus
        If Not txtDocumento.Text = "" Then
            ProgressBar_FormDocumentoTransportista.Visible = True
            lblProgressBar_FormDocumentoTransportista.Visible = True
            Me.Refresh()
            Me.Update()
            verificarExisteDocumento(txtDocumento.Text, FormProveedorDeposito_Recepcion.txtProveedor.Text)
        End If
    End Sub

    Private Sub FormDocumentoTransportista_Recepcion_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblUsuarioEnSesion.Text = "Usuario: " + descripcionUsuario
        Me.Refresh()
        Me.Update()
        AjustarFormularios(Me)
        txtDocumento.Text = ""
        txtTransportista.Text = ""
    End Sub

    Private Sub cmbSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSalir.Click
        txtDocumento.Text = ""
        txtTransportista.Text = ""
        FormProveedorDeposito_Recepcion.Show()
        Me.Visible = False
    End Sub

    Private Sub cmbSiguiente_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSiguiente.Click
        If txtDocumento.Text = "" Then
            MsgBox("Debe ingresar el número de Documento.", MsgBoxStyle.Information, "isMOBILE")
            txtDocumento.Focus()
        Else
            If txtTransportista.Text = "" Then
                MsgBox("Debe ingresar el nombre del Transportista.", MsgBoxStyle.Information, "isMOBILE")
                txtTransportista.Focus()
            Else
                FormLecturaProducto_Recepcion.Show()
                Me.Visible = False
            End If
        End If
    End Sub

    Private Sub cmbCancelar_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbCancelar.Click

        If Not (txtDocumento.Text = "") Then
            respuesta = MsgBox("¿Está seguro que desea cancelar?", MsgBoxStyle.YesNo, "Cancelar")
            If respuesta = "6" Then
                txtDocumento.Text = ""
                txtTransportista.Text = ""
                txtDocumento.Focus()
            End If
        Else 'el txtdocumento esta vacio
            If Not (txtTransportista.Text = "") Then
                respuesta = MsgBox("¿Está seguro que desea cancelar?", MsgBoxStyle.YesNo, "Cancelar")
                If respuesta = "6" Then
                    txtDocumento.Text = ""
                    txtTransportista.Text = ""
                    txtDocumento.Focus()
                End If
            Else 'ambos estan vacios
                txtDocumento.Text = ""
                txtTransportista.Text = ""
                txtDocumento.Focus()
            End If
        End If

    End Sub

End Class