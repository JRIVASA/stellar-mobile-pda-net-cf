﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class FormBuscarUbicacion
    Inherits System.Windows.Forms.Form

    'Form invalida a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer
    Private mainMenu1 As System.Windows.Forms.MainMenu

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar con el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormBuscarUbicacion))
        Me.mainMenu1 = New System.Windows.Forms.MainMenu
        Me.DataGridUbicaciones = New System.Windows.Forms.DataGrid
        Me.txtUbicacion = New System.Windows.Forms.TextBox
        Me.lblDescUbicacion = New System.Windows.Forms.Label
        Me.PictureFondoBuscarProveedor = New System.Windows.Forms.PictureBox
        Me.CmbBuscarUbicacion = New System.Windows.Forms.Button
        Me.cmbSalir = New System.Windows.Forms.PictureBox
        Me.picBarraMorada = New System.Windows.Forms.PictureBox
        Me.CmbAceptar = New System.Windows.Forms.Button
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.lblUsuarioEnSesion = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'DataGridUbicaciones
        '
        Me.DataGridUbicaciones.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.DataGridUbicaciones.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold)
        Me.DataGridUbicaciones.Location = New System.Drawing.Point(9, 119)
        Me.DataGridUbicaciones.Name = "DataGridUbicaciones"
        Me.DataGridUbicaciones.Size = New System.Drawing.Size(220, 134)
        Me.DataGridUbicaciones.TabIndex = 26
        '
        'txtUbicacion
        '
        Me.txtUbicacion.Font = New System.Drawing.Font("Verdana", 14.0!, System.Drawing.FontStyle.Bold)
        Me.txtUbicacion.Location = New System.Drawing.Point(9, 84)
        Me.txtUbicacion.MaxLength = 50
        Me.txtUbicacion.Name = "txtUbicacion"
        Me.txtUbicacion.Size = New System.Drawing.Size(135, 29)
        Me.txtUbicacion.TabIndex = 24
        '
        'lblDescUbicacion
        '
        Me.lblDescUbicacion.BackColor = System.Drawing.Color.Gainsboro
        Me.lblDescUbicacion.Font = New System.Drawing.Font("Verdana", 14.0!, System.Drawing.FontStyle.Bold)
        Me.lblDescUbicacion.Location = New System.Drawing.Point(48, 58)
        Me.lblDescUbicacion.Name = "lblDescUbicacion"
        Me.lblDescUbicacion.Size = New System.Drawing.Size(135, 25)
        Me.lblDescUbicacion.Text = "Ubicación"
        Me.lblDescUbicacion.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'PictureFondoBuscarProveedor
        '
        Me.PictureFondoBuscarProveedor.Image = CType(resources.GetObject("PictureFondoBuscarProveedor.Image"), System.Drawing.Image)
        Me.PictureFondoBuscarProveedor.Location = New System.Drawing.Point(42, 0)
        Me.PictureFondoBuscarProveedor.Name = "PictureFondoBuscarProveedor"
        Me.PictureFondoBuscarProveedor.Size = New System.Drawing.Size(198, 38)
        Me.PictureFondoBuscarProveedor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'CmbBuscarUbicacion
        '
        Me.CmbBuscarUbicacion.BackColor = System.Drawing.Color.White
        Me.CmbBuscarUbicacion.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.CmbBuscarUbicacion.ForeColor = System.Drawing.Color.Black
        Me.CmbBuscarUbicacion.Location = New System.Drawing.Point(151, 84)
        Me.CmbBuscarUbicacion.Name = "CmbBuscarUbicacion"
        Me.CmbBuscarUbicacion.Size = New System.Drawing.Size(78, 29)
        Me.CmbBuscarUbicacion.TabIndex = 31
        Me.CmbBuscarUbicacion.Text = "Buscar"
        '
        'cmbSalir
        '
        Me.cmbSalir.Image = CType(resources.GetObject("cmbSalir.Image"), System.Drawing.Image)
        Me.cmbSalir.Location = New System.Drawing.Point(0, 0)
        Me.cmbSalir.Name = "cmbSalir"
        Me.cmbSalir.Size = New System.Drawing.Size(41, 38)
        Me.cmbSalir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'picBarraMorada
        '
        Me.picBarraMorada.Image = CType(resources.GetObject("picBarraMorada.Image"), System.Drawing.Image)
        Me.picBarraMorada.Location = New System.Drawing.Point(0, 260)
        Me.picBarraMorada.Name = "picBarraMorada"
        Me.picBarraMorada.Size = New System.Drawing.Size(240, 40)
        Me.picBarraMorada.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'CmbAceptar
        '
        Me.CmbAceptar.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.CmbAceptar.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.CmbAceptar.ForeColor = System.Drawing.Color.Black
        Me.CmbAceptar.Location = New System.Drawing.Point(78, 265)
        Me.CmbAceptar.Name = "CmbAceptar"
        Me.CmbAceptar.Size = New System.Drawing.Size(88, 25)
        Me.CmbAceptar.TabIndex = 115
        Me.CmbAceptar.Text = "Aceptar"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Gainsboro
        Me.PictureBox1.Location = New System.Drawing.Point(0, 54)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(240, 205)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'lblUsuarioEnSesion
        '
        Me.lblUsuarioEnSesion.BackColor = System.Drawing.Color.DarkGray
        Me.lblUsuarioEnSesion.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblUsuarioEnSesion.Location = New System.Drawing.Point(0, 39)
        Me.lblUsuarioEnSesion.Name = "lblUsuarioEnSesion"
        Me.lblUsuarioEnSesion.Size = New System.Drawing.Size(240, 15)
        Me.lblUsuarioEnSesion.Text = "Usuario:"
        '
        'FormBuscarUbicacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(240, 300)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblUsuarioEnSesion)
        Me.Controls.Add(Me.CmbBuscarUbicacion)
        Me.Controls.Add(Me.DataGridUbicaciones)
        Me.Controls.Add(Me.txtUbicacion)
        Me.Controls.Add(Me.lblDescUbicacion)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.CmbAceptar)
        Me.Controls.Add(Me.picBarraMorada)
        Me.Controls.Add(Me.cmbSalir)
        Me.Controls.Add(Me.PictureFondoBuscarProveedor)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormBuscarUbicacion"
        Me.Text = "isMOBILE - Buscar Depósito"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridUbicaciones As System.Windows.Forms.DataGrid
    Friend WithEvents txtUbicacion As System.Windows.Forms.TextBox
    Friend WithEvents lblDescUbicacion As System.Windows.Forms.Label
    Friend WithEvents PictureFondoBuscarProveedor As System.Windows.Forms.PictureBox
    Friend WithEvents CmbBuscarUbicacion As System.Windows.Forms.Button
    Friend WithEvents cmbSalir As System.Windows.Forms.PictureBox
    Friend WithEvents picBarraMorada As System.Windows.Forms.PictureBox
    Friend WithEvents CmbAceptar As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents lblUsuarioEnSesion As System.Windows.Forms.Label
End Class
