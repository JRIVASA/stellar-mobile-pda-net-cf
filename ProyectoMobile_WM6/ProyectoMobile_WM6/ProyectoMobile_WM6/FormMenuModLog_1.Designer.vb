﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class FormMenuModLog_1
    Inherits System.Windows.Forms.Form

    'Form invalida a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar con el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormMenuModLog_1))
        Me.lblRecepcion = New System.Windows.Forms.Label
        Me.lblTraslado = New System.Windows.Forms.Label
        Me.lblStatus_MenuPrincipal = New System.Windows.Forms.Label
        Me.lblPicking = New System.Windows.Forms.Label
        Me.lblPacking = New System.Windows.Forms.Label
        Me.CmdPacking = New System.Windows.Forms.PictureBox
        Me.CmdPicking = New System.Windows.Forms.PictureBox
        Me.cmbTraslado = New System.Windows.Forms.PictureBox
        Me.CmbRecepcion = New System.Windows.Forms.PictureBox
        Me.CmbSalir = New System.Windows.Forms.PictureBox
        Me.PicTopBar = New System.Windows.Forms.PictureBox
        Me.TimerReload = New System.Windows.Forms.Timer
        Me.SuspendLayout()
        '
        'lblRecepcion
        '
        Me.lblRecepcion.BackColor = System.Drawing.Color.Gainsboro
        Me.lblRecepcion.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular)
        Me.lblRecepcion.Location = New System.Drawing.Point(20, 113)
        Me.lblRecepcion.Name = "lblRecepcion"
        Me.lblRecepcion.Size = New System.Drawing.Size(63, 13)
        Me.lblRecepcion.Text = "Recepción"
        Me.lblRecepcion.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblRecepcion.Visible = False
        '
        'lblTraslado
        '
        Me.lblTraslado.BackColor = System.Drawing.Color.Gainsboro
        Me.lblTraslado.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular)
        Me.lblTraslado.Location = New System.Drawing.Point(90, 113)
        Me.lblTraslado.Name = "lblTraslado"
        Me.lblTraslado.Size = New System.Drawing.Size(63, 13)
        Me.lblTraslado.Text = "Traslado"
        Me.lblTraslado.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblTraslado.Visible = False
        '
        'lblStatus_MenuPrincipal
        '
        Me.lblStatus_MenuPrincipal.BackColor = System.Drawing.Color.DarkGray
        Me.lblStatus_MenuPrincipal.Location = New System.Drawing.Point(0, 39)
        Me.lblStatus_MenuPrincipal.Name = "lblStatus_MenuPrincipal"
        Me.lblStatus_MenuPrincipal.Size = New System.Drawing.Size(240, 15)
        Me.lblStatus_MenuPrincipal.Visible = False
        '
        'lblPicking
        '
        Me.lblPicking.BackColor = System.Drawing.Color.Gainsboro
        Me.lblPicking.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular)
        Me.lblPicking.Location = New System.Drawing.Point(159, 113)
        Me.lblPicking.Name = "lblPicking"
        Me.lblPicking.Size = New System.Drawing.Size(54, 13)
        Me.lblPicking.Text = "Picking"
        Me.lblPicking.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblPicking.Visible = False
        '
        'lblPacking
        '
        Me.lblPacking.BackColor = System.Drawing.Color.Gainsboro
        Me.lblPacking.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular)
        Me.lblPacking.Location = New System.Drawing.Point(22, 188)
        Me.lblPacking.Name = "lblPacking"
        Me.lblPacking.Size = New System.Drawing.Size(54, 13)
        Me.lblPacking.Text = "Packing"
        Me.lblPacking.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblPacking.Visible = False
        '
        'CmdPacking
        '
        Me.CmdPacking.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.CmdPacking.Image = CType(resources.GetObject("CmdPacking.Image"), System.Drawing.Image)
        Me.CmdPacking.Location = New System.Drawing.Point(28, 138)
        Me.CmdPacking.Name = "CmdPacking"
        Me.CmdPacking.Size = New System.Drawing.Size(48, 47)
        Me.CmdPacking.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.CmdPacking.Visible = False
        '
        'CmdPicking
        '
        Me.CmdPicking.Image = CType(resources.GetObject("CmdPicking.Image"), System.Drawing.Image)
        Me.CmdPicking.Location = New System.Drawing.Point(165, 63)
        Me.CmdPicking.Name = "CmdPicking"
        Me.CmdPicking.Size = New System.Drawing.Size(48, 48)
        Me.CmdPicking.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'cmbTraslado
        '
        Me.cmbTraslado.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.cmbTraslado.Image = CType(resources.GetObject("cmbTraslado.Image"), System.Drawing.Image)
        Me.cmbTraslado.Location = New System.Drawing.Point(96, 63)
        Me.cmbTraslado.Name = "cmbTraslado"
        Me.cmbTraslado.Size = New System.Drawing.Size(48, 47)
        Me.cmbTraslado.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.cmbTraslado.Visible = False
        '
        'CmbRecepcion
        '
        Me.CmbRecepcion.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.CmbRecepcion.Image = CType(resources.GetObject("CmbRecepcion.Image"), System.Drawing.Image)
        Me.CmbRecepcion.Location = New System.Drawing.Point(28, 63)
        Me.CmbRecepcion.Name = "CmbRecepcion"
        Me.CmbRecepcion.Size = New System.Drawing.Size(48, 47)
        Me.CmbRecepcion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.CmbRecepcion.Visible = False
        '
        'CmbSalir
        '
        Me.CmbSalir.Image = CType(resources.GetObject("CmbSalir.Image"), System.Drawing.Image)
        Me.CmbSalir.Location = New System.Drawing.Point(0, 0)
        Me.CmbSalir.Name = "CmbSalir"
        Me.CmbSalir.Size = New System.Drawing.Size(41, 38)
        Me.CmbSalir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'PicTopBar
        '
        Me.PicTopBar.Image = CType(resources.GetObject("PicTopBar.Image"), System.Drawing.Image)
        Me.PicTopBar.Location = New System.Drawing.Point(42, 0)
        Me.PicTopBar.Name = "PicTopBar"
        Me.PicTopBar.Size = New System.Drawing.Size(198, 38)
        Me.PicTopBar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'TimerReload
        '
        '
        'FormMenuModLog_1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.Gainsboro
        Me.ClientSize = New System.Drawing.Size(240, 300)
        Me.ControlBox = False
        Me.Controls.Add(Me.CmdPacking)
        Me.Controls.Add(Me.CmdPicking)
        Me.Controls.Add(Me.lblPacking)
        Me.Controls.Add(Me.lblPicking)
        Me.Controls.Add(Me.lblStatus_MenuPrincipal)
        Me.Controls.Add(Me.lblTraslado)
        Me.Controls.Add(Me.cmbTraslado)
        Me.Controls.Add(Me.lblRecepcion)
        Me.Controls.Add(Me.CmbRecepcion)
        Me.Controls.Add(Me.CmbSalir)
        Me.Controls.Add(Me.PicTopBar)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FormMenuModLog_1"
        Me.Text = "isMOBILE - Menú Logística y Almacenaje"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PicTopBar As System.Windows.Forms.PictureBox
    Friend WithEvents CmbSalir As System.Windows.Forms.PictureBox
    Friend WithEvents lblRecepcion As System.Windows.Forms.Label
    Friend WithEvents CmbRecepcion As System.Windows.Forms.PictureBox
    Friend WithEvents lblTraslado As System.Windows.Forms.Label
    Friend WithEvents cmbTraslado As System.Windows.Forms.PictureBox
    Friend WithEvents lblStatus_MenuPrincipal As System.Windows.Forms.Label
    Friend WithEvents lblPicking As System.Windows.Forms.Label
    Friend WithEvents lblPacking As System.Windows.Forms.Label
    Friend WithEvents CmdPicking As System.Windows.Forms.PictureBox
    Friend WithEvents CmdPacking As System.Windows.Forms.PictureBox
    Friend WithEvents TimerReload As System.Windows.Forms.Timer
End Class
