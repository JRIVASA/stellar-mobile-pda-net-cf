﻿Public Class FormMenuInventarioFisico

    Private Sub cmbActualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbActualizar.Click
        FormActualizar_InventarioFisico.CallerForm = Me
        FormActualizar_InventarioFisico.Show()
        Me.Visible = False
    End Sub

    Private Sub FormMenuInventarioFisico_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        lblUsuarioEnSesion.Text = "Usuario: " + descripcionUsuario

        If CompatibilidadFueraDeLinea Then
            avisoDeActualizarProductos()
        Else
            cmbActualizar.Visible = False
            Label1.Visible = False
        End If

        AjustarFormularios(Me)
        AjustarFormularios(Me.PanelMenu_Principal)

    End Sub

    Private Sub cmbSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSalir.Click
        salirProceso(Me)
    End Sub

    Private Sub cmbInventario_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbInventario.Click
        If Not CompatibilidadFueraDeLinea Then
            If conectarDB_SERVIDOR(rutaDB_SERVIDOR) = 0 Then
                MessageBox.Show(MsjSinConexionGenerico)
                Exit Sub
            End If
        End If
        FormInv_Activo_InventarioFisico.Show()
        Me.Visible = False
    End Sub

    Private Sub cmbEnviarPendientes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbEnviarPendientes.Click
        FormPendientes_InventarioFisico.Show()
        Me.Visible = False
    End Sub

End Class