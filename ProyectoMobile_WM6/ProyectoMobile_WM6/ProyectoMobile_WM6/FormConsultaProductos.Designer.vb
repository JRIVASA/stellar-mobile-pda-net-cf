﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class FormConsultaProductos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormConsultaProductos))
        Me.picBarraMorada = New System.Windows.Forms.PictureBox
        Me.pictureFondo = New System.Windows.Forms.PictureBox
        Me.cmbSalir = New System.Windows.Forms.PictureBox
        Me.lblUsuarioEnSesion = New System.Windows.Forms.Label
        Me.codigoBox = New System.Windows.Forms.TextBox
        Me.codigoLabel = New System.Windows.Forms.Label
        Me.descripcionLabel = New System.Windows.Forms.Label
        Me.cdescriLabel = New System.Windows.Forms.Label
        Me.departamentoLabel = New System.Windows.Forms.Label
        Me.cdepartamentoLabel = New System.Windows.Forms.Label
        Me.cgrupoLabel = New System.Windows.Forms.Label
        Me.grupoLabel = New System.Windows.Forms.Label
        Me.csubgrupoLabel = New System.Windows.Forms.Label
        Me.subgrupoLabel = New System.Windows.Forms.Label
        Me.codigosButton = New System.Windows.Forms.Button
        Me.existenciaButton = New System.Windows.Forms.Button
        Me.preciosButton = New System.Windows.Forms.Button
        Me.cmbActualizar = New System.Windows.Forms.PictureBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.precioLabel = New System.Windows.Forms.Label
        Me.lugarButton = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'picBarraMorada
        '
        Me.picBarraMorada.Image = CType(resources.GetObject("picBarraMorada.Image"), System.Drawing.Image)
        Me.picBarraMorada.Location = New System.Drawing.Point(0, 260)
        Me.picBarraMorada.Name = "picBarraMorada"
        Me.picBarraMorada.Size = New System.Drawing.Size(240, 40)
        Me.picBarraMorada.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'pictureFondo
        '
        Me.pictureFondo.Image = CType(resources.GetObject("pictureFondo.Image"), System.Drawing.Image)
        Me.pictureFondo.Location = New System.Drawing.Point(42, 0)
        Me.pictureFondo.Name = "pictureFondo"
        Me.pictureFondo.Size = New System.Drawing.Size(198, 38)
        Me.pictureFondo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'cmbSalir
        '
        Me.cmbSalir.Image = CType(resources.GetObject("cmbSalir.Image"), System.Drawing.Image)
        Me.cmbSalir.Location = New System.Drawing.Point(0, 0)
        Me.cmbSalir.Name = "cmbSalir"
        Me.cmbSalir.Size = New System.Drawing.Size(41, 38)
        Me.cmbSalir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'lblUsuarioEnSesion
        '
        Me.lblUsuarioEnSesion.BackColor = System.Drawing.Color.DarkGray
        Me.lblUsuarioEnSesion.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblUsuarioEnSesion.Location = New System.Drawing.Point(0, 39)
        Me.lblUsuarioEnSesion.Name = "lblUsuarioEnSesion"
        Me.lblUsuarioEnSesion.Size = New System.Drawing.Size(240, 15)
        Me.lblUsuarioEnSesion.Text = "Usuario:"
        '
        'codigoBox
        '
        Me.codigoBox.BackColor = System.Drawing.Color.White
        Me.codigoBox.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.codigoBox.Location = New System.Drawing.Point(97, 59)
        Me.codigoBox.MaxLength = 20
        Me.codigoBox.Name = "codigoBox"
        Me.codigoBox.Size = New System.Drawing.Size(140, 23)
        Me.codigoBox.TabIndex = 73
        '
        'codigoLabel
        '
        Me.codigoLabel.BackColor = System.Drawing.Color.Gainsboro
        Me.codigoLabel.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.codigoLabel.Location = New System.Drawing.Point(37, 63)
        Me.codigoLabel.Name = "codigoLabel"
        Me.codigoLabel.Size = New System.Drawing.Size(58, 19)
        Me.codigoLabel.Text = "Código:"
        '
        'descripcionLabel
        '
        Me.descripcionLabel.BackColor = System.Drawing.Color.Gainsboro
        Me.descripcionLabel.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.descripcionLabel.Location = New System.Drawing.Point(6, 85)
        Me.descripcionLabel.Name = "descripcionLabel"
        Me.descripcionLabel.Size = New System.Drawing.Size(102, 16)
        Me.descripcionLabel.Text = "Descripción:"
        '
        'cdescriLabel
        '
        Me.cdescriLabel.BackColor = System.Drawing.Color.Gainsboro
        Me.cdescriLabel.Location = New System.Drawing.Point(6, 101)
        Me.cdescriLabel.Name = "cdescriLabel"
        Me.cdescriLabel.Size = New System.Drawing.Size(231, 30)
        '
        'departamentoLabel
        '
        Me.departamentoLabel.BackColor = System.Drawing.Color.Gainsboro
        Me.departamentoLabel.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold)
        Me.departamentoLabel.Location = New System.Drawing.Point(6, 155)
        Me.departamentoLabel.Name = "departamentoLabel"
        Me.departamentoLabel.Size = New System.Drawing.Size(102, 16)
        Me.departamentoLabel.Text = "Departamento:"
        '
        'cdepartamentoLabel
        '
        Me.cdepartamentoLabel.BackColor = System.Drawing.Color.Gainsboro
        Me.cdepartamentoLabel.Location = New System.Drawing.Point(6, 171)
        Me.cdepartamentoLabel.Name = "cdepartamentoLabel"
        Me.cdepartamentoLabel.Size = New System.Drawing.Size(215, 19)
        '
        'cgrupoLabel
        '
        Me.cgrupoLabel.BackColor = System.Drawing.Color.Gainsboro
        Me.cgrupoLabel.Location = New System.Drawing.Point(7, 205)
        Me.cgrupoLabel.Name = "cgrupoLabel"
        Me.cgrupoLabel.Size = New System.Drawing.Size(214, 19)
        '
        'grupoLabel
        '
        Me.grupoLabel.BackColor = System.Drawing.Color.Gainsboro
        Me.grupoLabel.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold)
        Me.grupoLabel.Location = New System.Drawing.Point(7, 189)
        Me.grupoLabel.Name = "grupoLabel"
        Me.grupoLabel.Size = New System.Drawing.Size(49, 16)
        Me.grupoLabel.Text = "Grupo:"
        '
        'csubgrupoLabel
        '
        Me.csubgrupoLabel.BackColor = System.Drawing.Color.Gainsboro
        Me.csubgrupoLabel.Location = New System.Drawing.Point(6, 238)
        Me.csubgrupoLabel.Name = "csubgrupoLabel"
        Me.csubgrupoLabel.Size = New System.Drawing.Size(215, 19)
        '
        'subgrupoLabel
        '
        Me.subgrupoLabel.BackColor = System.Drawing.Color.Gainsboro
        Me.subgrupoLabel.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold)
        Me.subgrupoLabel.Location = New System.Drawing.Point(6, 222)
        Me.subgrupoLabel.Name = "subgrupoLabel"
        Me.subgrupoLabel.Size = New System.Drawing.Size(73, 16)
        Me.subgrupoLabel.Text = "Subgrupo:"
        '
        'codigosButton
        '
        Me.codigosButton.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.codigosButton.Location = New System.Drawing.Point(5, 267)
        Me.codigosButton.Name = "codigosButton"
        Me.codigosButton.Size = New System.Drawing.Size(57, 25)
        Me.codigosButton.TabIndex = 89
        Me.codigosButton.Text = "Códigos"
        '
        'existenciaButton
        '
        Me.existenciaButton.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.existenciaButton.Location = New System.Drawing.Point(118, 267)
        Me.existenciaButton.Name = "existenciaButton"
        Me.existenciaButton.Size = New System.Drawing.Size(69, 25)
        Me.existenciaButton.TabIndex = 90
        Me.existenciaButton.Text = "Existencia"
        '
        'preciosButton
        '
        Me.preciosButton.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.preciosButton.Location = New System.Drawing.Point(64, 267)
        Me.preciosButton.Name = "preciosButton"
        Me.preciosButton.Size = New System.Drawing.Size(52, 25)
        Me.preciosButton.TabIndex = 104
        Me.preciosButton.Text = "Precios"
        '
        'cmbActualizar
        '
        Me.cmbActualizar.Image = CType(resources.GetObject("cmbActualizar.Image"), System.Drawing.Image)
        Me.cmbActualizar.Location = New System.Drawing.Point(10, 58)
        Me.cmbActualizar.Name = "cmbActualizar"
        Me.cmbActualizar.Size = New System.Drawing.Size(24, 24)
        Me.cmbActualizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Gainsboro
        Me.Label1.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold)
        Me.Label1.Location = New System.Drawing.Point(7, 131)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(54, 16)
        Me.Label1.Text = "Precio:"
        '
        'precioLabel
        '
        Me.precioLabel.BackColor = System.Drawing.Color.Gainsboro
        Me.precioLabel.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.precioLabel.Location = New System.Drawing.Point(86, 136)
        Me.precioLabel.Name = "precioLabel"
        Me.precioLabel.Size = New System.Drawing.Size(147, 19)
        Me.precioLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lugarButton
        '
        Me.lugarButton.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.lugarButton.Location = New System.Drawing.Point(189, 267)
        Me.lugarButton.Name = "lugarButton"
        Me.lugarButton.Size = New System.Drawing.Size(47, 25)
        Me.lugarButton.TabIndex = 118
        Me.lugarButton.Text = "Lugar"
        '
        'FormConsultaProductos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.Gainsboro
        Me.ClientSize = New System.Drawing.Size(240, 300)
        Me.ControlBox = False
        Me.Controls.Add(Me.lugarButton)
        Me.Controls.Add(Me.precioLabel)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cmbActualizar)
        Me.Controls.Add(Me.preciosButton)
        Me.Controls.Add(Me.existenciaButton)
        Me.Controls.Add(Me.codigosButton)
        Me.Controls.Add(Me.csubgrupoLabel)
        Me.Controls.Add(Me.subgrupoLabel)
        Me.Controls.Add(Me.cgrupoLabel)
        Me.Controls.Add(Me.grupoLabel)
        Me.Controls.Add(Me.cdepartamentoLabel)
        Me.Controls.Add(Me.departamentoLabel)
        Me.Controls.Add(Me.cdescriLabel)
        Me.Controls.Add(Me.descripcionLabel)
        Me.Controls.Add(Me.codigoBox)
        Me.Controls.Add(Me.codigoLabel)
        Me.Controls.Add(Me.lblUsuarioEnSesion)
        Me.Controls.Add(Me.cmbSalir)
        Me.Controls.Add(Me.picBarraMorada)
        Me.Controls.Add(Me.pictureFondo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormConsultaProductos"
        Me.Text = "isMOBILE - Consulta"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents picBarraMorada As System.Windows.Forms.PictureBox
    Friend WithEvents pictureFondo As System.Windows.Forms.PictureBox
    Friend WithEvents cmbSalir As System.Windows.Forms.PictureBox
    Friend WithEvents lblUsuarioEnSesion As System.Windows.Forms.Label
    Friend WithEvents codigoBox As System.Windows.Forms.TextBox
    Friend WithEvents codigoLabel As System.Windows.Forms.Label
    Friend WithEvents descripcionLabel As System.Windows.Forms.Label
    Friend WithEvents cdescriLabel As System.Windows.Forms.Label
    Friend WithEvents departamentoLabel As System.Windows.Forms.Label
    Friend WithEvents cdepartamentoLabel As System.Windows.Forms.Label
    Friend WithEvents cgrupoLabel As System.Windows.Forms.Label
    Friend WithEvents grupoLabel As System.Windows.Forms.Label
    Friend WithEvents csubgrupoLabel As System.Windows.Forms.Label
    Friend WithEvents subgrupoLabel As System.Windows.Forms.Label
    Friend WithEvents codigosButton As System.Windows.Forms.Button
    Friend WithEvents existenciaButton As System.Windows.Forms.Button
    Friend WithEvents preciosButton As System.Windows.Forms.Button
    Friend WithEvents cmbActualizar As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents precioLabel As System.Windows.Forms.Label
    Friend WithEvents lugarButton As System.Windows.Forms.Button
End Class
