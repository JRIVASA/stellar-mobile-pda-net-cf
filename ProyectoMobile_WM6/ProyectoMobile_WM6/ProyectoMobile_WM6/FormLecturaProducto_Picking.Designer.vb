﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class FormLecturaProducto_Picking
    Inherits System.Windows.Forms.Form

    'Form invalida a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer
    Private mainMenu1 As System.Windows.Forms.MainMenu

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar con el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormLecturaProducto_Picking))
        Me.mainMenu1 = New System.Windows.Forms.MainMenu
        Me.lblUsuarioEnSesion = New System.Windows.Forms.Label
        Me.txtCantSolicitada = New System.Windows.Forms.TextBox
        Me.lblCantSolicitada = New System.Windows.Forms.Label
        Me.lblCantRecolectada = New System.Windows.Forms.Label
        Me.txtCantRecolectada = New System.Windows.Forms.TextBox
        Me.lblCantidades = New System.Windows.Forms.Label
        Me.txtUbicaciones = New System.Windows.Forms.TextBox
        Me.lblDescripcion = New System.Windows.Forms.Label
        Me.cmbAgregar = New System.Windows.Forms.Button
        Me.cmbFinalizar = New System.Windows.Forms.Button
        Me.lblProducto = New System.Windows.Forms.Label
        Me.lblUbicaciones = New System.Windows.Forms.Label
        Me.picBarraMorada = New System.Windows.Forms.PictureBox
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.cmbSalir = New System.Windows.Forms.PictureBox
        Me.PictureFondoProveedorDeposito_Recepcion = New System.Windows.Forms.PictureBox
        Me.CmbRecTodo = New System.Windows.Forms.PictureBox
        Me.TimerReload = New System.Windows.Forms.Timer
        Me.SuspendLayout()
        '
        'lblUsuarioEnSesion
        '
        Me.lblUsuarioEnSesion.BackColor = System.Drawing.Color.DarkGray
        Me.lblUsuarioEnSesion.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblUsuarioEnSesion.Location = New System.Drawing.Point(0, 39)
        Me.lblUsuarioEnSesion.Name = "lblUsuarioEnSesion"
        Me.lblUsuarioEnSesion.Size = New System.Drawing.Size(240, 15)
        Me.lblUsuarioEnSesion.Text = "Usuario:"
        '
        'txtCantSolicitada
        '
        Me.txtCantSolicitada.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtCantSolicitada.Location = New System.Drawing.Point(119, 208)
        Me.txtCantSolicitada.MaxLength = 10
        Me.txtCantSolicitada.Name = "txtCantSolicitada"
        Me.txtCantSolicitada.Size = New System.Drawing.Size(82, 21)
        Me.txtCantSolicitada.TabIndex = 2
        '
        'lblCantSolicitada
        '
        Me.lblCantSolicitada.BackColor = System.Drawing.Color.Gainsboro
        Me.lblCantSolicitada.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.lblCantSolicitada.Location = New System.Drawing.Point(15, 208)
        Me.lblCantSolicitada.Name = "lblCantSolicitada"
        Me.lblCantSolicitada.Size = New System.Drawing.Size(96, 23)
        Me.lblCantSolicitada.Text = "Solicitada"
        Me.lblCantSolicitada.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblCantRecolectada
        '
        Me.lblCantRecolectada.BackColor = System.Drawing.Color.Gainsboro
        Me.lblCantRecolectada.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.lblCantRecolectada.Location = New System.Drawing.Point(15, 235)
        Me.lblCantRecolectada.Name = "lblCantRecolectada"
        Me.lblCantRecolectada.Size = New System.Drawing.Size(93, 24)
        Me.lblCantRecolectada.Text = "Recolectada"
        Me.lblCantRecolectada.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtCantRecolectada
        '
        Me.txtCantRecolectada.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtCantRecolectada.Location = New System.Drawing.Point(119, 235)
        Me.txtCantRecolectada.MaxLength = 10
        Me.txtCantRecolectada.Name = "txtCantRecolectada"
        Me.txtCantRecolectada.Size = New System.Drawing.Size(82, 21)
        Me.txtCantRecolectada.TabIndex = 3
        '
        'lblCantidades
        '
        Me.lblCantidades.BackColor = System.Drawing.Color.Gainsboro
        Me.lblCantidades.Font = New System.Drawing.Font("Verdana", 11.0!, System.Drawing.FontStyle.Bold)
        Me.lblCantidades.Location = New System.Drawing.Point(27, 186)
        Me.lblCantidades.Name = "lblCantidades"
        Me.lblCantidades.Size = New System.Drawing.Size(186, 20)
        Me.lblCantidades.Text = "Cantidades"
        Me.lblCantidades.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txtUbicaciones
        '
        Me.txtUbicaciones.BackColor = System.Drawing.Color.White
        Me.txtUbicaciones.Font = New System.Drawing.Font("Verdana", 11.0!, System.Drawing.FontStyle.Bold)
        Me.txtUbicaciones.Location = New System.Drawing.Point(27, 133)
        Me.txtUbicaciones.MaxLength = 0
        Me.txtUbicaciones.Multiline = True
        Me.txtUbicaciones.Name = "txtUbicaciones"
        Me.txtUbicaciones.ReadOnly = True
        Me.txtUbicaciones.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtUbicaciones.Size = New System.Drawing.Size(186, 50)
        Me.txtUbicaciones.TabIndex = 1
        '
        'lblDescripcion
        '
        Me.lblDescripcion.BackColor = System.Drawing.Color.Gainsboro
        Me.lblDescripcion.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Regular)
        Me.lblDescripcion.Location = New System.Drawing.Point(38, 83)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(153, 31)
        '
        'cmbAgregar
        '
        Me.cmbAgregar.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.cmbAgregar.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.cmbAgregar.ForeColor = System.Drawing.Color.Black
        Me.cmbAgregar.Location = New System.Drawing.Point(77, 266)
        Me.cmbAgregar.Name = "cmbAgregar"
        Me.cmbAgregar.Size = New System.Drawing.Size(155, 25)
        Me.cmbAgregar.TabIndex = 4
        Me.cmbAgregar.Text = "Guardar Cantidad"
        '
        'cmbFinalizar
        '
        Me.cmbFinalizar.BackColor = System.Drawing.Color.LightGreen
        Me.cmbFinalizar.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.cmbFinalizar.ForeColor = System.Drawing.Color.Black
        Me.cmbFinalizar.Location = New System.Drawing.Point(5, 265)
        Me.cmbFinalizar.Name = "cmbFinalizar"
        Me.cmbFinalizar.Size = New System.Drawing.Size(66, 25)
        Me.cmbFinalizar.TabIndex = 5
        Me.cmbFinalizar.Text = "Finalizar"
        '
        'lblProducto
        '
        Me.lblProducto.BackColor = System.Drawing.Color.Gainsboro
        Me.lblProducto.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold)
        Me.lblProducto.Location = New System.Drawing.Point(86, 65)
        Me.lblProducto.Name = "lblProducto"
        Me.lblProducto.Size = New System.Drawing.Size(61, 15)
        Me.lblProducto.Text = "Producto:"
        '
        'lblUbicaciones
        '
        Me.lblUbicaciones.BackColor = System.Drawing.Color.Gainsboro
        Me.lblUbicaciones.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold)
        Me.lblUbicaciones.Location = New System.Drawing.Point(86, 114)
        Me.lblUbicaciones.Name = "lblUbicaciones"
        Me.lblUbicaciones.Size = New System.Drawing.Size(87, 15)
        Me.lblUbicaciones.Text = "Ubicación:"
        '
        'picBarraMorada
        '
        Me.picBarraMorada.Image = CType(resources.GetObject("picBarraMorada.Image"), System.Drawing.Image)
        Me.picBarraMorada.Location = New System.Drawing.Point(0, 259)
        Me.picBarraMorada.Name = "picBarraMorada"
        Me.picBarraMorada.Size = New System.Drawing.Size(240, 40)
        Me.picBarraMorada.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Gainsboro
        Me.PictureBox1.Location = New System.Drawing.Point(0, 57)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(240, 203)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'cmbSalir
        '
        Me.cmbSalir.Image = CType(resources.GetObject("cmbSalir.Image"), System.Drawing.Image)
        Me.cmbSalir.Location = New System.Drawing.Point(0, 0)
        Me.cmbSalir.Name = "cmbSalir"
        Me.cmbSalir.Size = New System.Drawing.Size(41, 38)
        Me.cmbSalir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'PictureFondoProveedorDeposito_Recepcion
        '
        Me.PictureFondoProveedorDeposito_Recepcion.Image = CType(resources.GetObject("PictureFondoProveedorDeposito_Recepcion.Image"), System.Drawing.Image)
        Me.PictureFondoProveedorDeposito_Recepcion.Location = New System.Drawing.Point(42, 0)
        Me.PictureFondoProveedorDeposito_Recepcion.Name = "PictureFondoProveedorDeposito_Recepcion"
        Me.PictureFondoProveedorDeposito_Recepcion.Size = New System.Drawing.Size(198, 38)
        Me.PictureFondoProveedorDeposito_Recepcion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'CmbRecTodo
        '
        Me.CmbRecTodo.Image = CType(resources.GetObject("CmbRecTodo.Image"), System.Drawing.Image)
        Me.CmbRecTodo.Location = New System.Drawing.Point(205, 210)
        Me.CmbRecTodo.Name = "CmbRecTodo"
        Me.CmbRecTodo.Size = New System.Drawing.Size(19, 17)
        Me.CmbRecTodo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'TimerReload
        '
        '
        'FormLecturaProducto_Picking
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(240, 300)
        Me.ControlBox = False
        Me.Controls.Add(Me.CmbRecTodo)
        Me.Controls.Add(Me.lblUbicaciones)
        Me.Controls.Add(Me.lblProducto)
        Me.Controls.Add(Me.cmbAgregar)
        Me.Controls.Add(Me.cmbFinalizar)
        Me.Controls.Add(Me.lblCantidades)
        Me.Controls.Add(Me.txtUbicaciones)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.txtCantSolicitada)
        Me.Controls.Add(Me.lblCantSolicitada)
        Me.Controls.Add(Me.lblCantRecolectada)
        Me.Controls.Add(Me.txtCantRecolectada)
        Me.Controls.Add(Me.picBarraMorada)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.lblUsuarioEnSesion)
        Me.Controls.Add(Me.cmbSalir)
        Me.Controls.Add(Me.PictureFondoProveedorDeposito_Recepcion)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.Name = "FormLecturaProducto_Picking"
        Me.Text = "isMOBILE - Traslados"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents picBarraMorada As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents lblUsuarioEnSesion As System.Windows.Forms.Label
    Friend WithEvents cmbSalir As System.Windows.Forms.PictureBox
    Friend WithEvents PictureFondoProveedorDeposito_Recepcion As System.Windows.Forms.PictureBox
    Friend WithEvents txtCantSolicitada As System.Windows.Forms.TextBox
    Friend WithEvents lblCantSolicitada As System.Windows.Forms.Label
    Friend WithEvents lblCantRecolectada As System.Windows.Forms.Label
    Friend WithEvents txtCantRecolectada As System.Windows.Forms.TextBox
    Friend WithEvents lblCantidades As System.Windows.Forms.Label
    Friend WithEvents txtUbicaciones As System.Windows.Forms.TextBox
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents cmbAgregar As System.Windows.Forms.Button
    Friend WithEvents cmbFinalizar As System.Windows.Forms.Button
    Friend WithEvents lblProducto As System.Windows.Forms.Label
    Friend WithEvents cmbTeclado As System.Windows.Forms.PictureBox
    Friend WithEvents lblUbicaciones As System.Windows.Forms.Label
    Friend WithEvents CmbRecTodo As System.Windows.Forms.PictureBox
    Friend WithEvents TimerReload As System.Windows.Forms.Timer
End Class
