﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class FormNumPadGeneral
    Inherits System.Windows.Forms.Form

    'Form invalida a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer
    Private mainMenu1 As System.Windows.Forms.MainMenu

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar con el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormNumPadGeneral))
        Me.mainMenu1 = New System.Windows.Forms.MainMenu
        Me.CmdNumero1 = New System.Windows.Forms.Button
        Me.CmdNumero2 = New System.Windows.Forms.Button
        Me.CmdNumero3 = New System.Windows.Forms.Button
        Me.CmdNumero6 = New System.Windows.Forms.Button
        Me.CmdNumero5 = New System.Windows.Forms.Button
        Me.CmdNumero4 = New System.Windows.Forms.Button
        Me.CmdNumero9 = New System.Windows.Forms.Button
        Me.CmdNumero8 = New System.Windows.Forms.Button
        Me.CmdNumero7 = New System.Windows.Forms.Button
        Me.CmdDecimal = New System.Windows.Forms.Button
        Me.CmdNumero0 = New System.Windows.Forms.Button
        Me.CmdEnter = New System.Windows.Forms.Button
        Me.CmdBack = New System.Windows.Forms.Button
        Me.CmdClear = New System.Windows.Forms.Button
        Me.txtNumero = New System.Windows.Forms.TextBox
        Me.picBarraMorada = New System.Windows.Forms.PictureBox
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.cmbSalir = New System.Windows.Forms.PictureBox
        Me.PictureFondoProveedorDeposito_Recepcion = New System.Windows.Forms.PictureBox
        Me.SuspendLayout()
        '
        'CmdNumero1
        '
        Me.CmdNumero1.Location = New System.Drawing.Point(5, 93)
        Me.CmdNumero1.Name = "CmdNumero1"
        Me.CmdNumero1.Size = New System.Drawing.Size(56, 38)
        Me.CmdNumero1.TabIndex = 4
        Me.CmdNumero1.Text = "1"
        '
        'CmdNumero2
        '
        Me.CmdNumero2.Location = New System.Drawing.Point(62, 93)
        Me.CmdNumero2.Name = "CmdNumero2"
        Me.CmdNumero2.Size = New System.Drawing.Size(56, 38)
        Me.CmdNumero2.TabIndex = 5
        Me.CmdNumero2.Text = "2"
        '
        'CmdNumero3
        '
        Me.CmdNumero3.Location = New System.Drawing.Point(119, 93)
        Me.CmdNumero3.Name = "CmdNumero3"
        Me.CmdNumero3.Size = New System.Drawing.Size(56, 38)
        Me.CmdNumero3.TabIndex = 6
        Me.CmdNumero3.Text = "3"
        '
        'CmdNumero6
        '
        Me.CmdNumero6.Location = New System.Drawing.Point(119, 137)
        Me.CmdNumero6.Name = "CmdNumero6"
        Me.CmdNumero6.Size = New System.Drawing.Size(56, 35)
        Me.CmdNumero6.TabIndex = 9
        Me.CmdNumero6.Text = "6"
        '
        'CmdNumero5
        '
        Me.CmdNumero5.Location = New System.Drawing.Point(62, 137)
        Me.CmdNumero5.Name = "CmdNumero5"
        Me.CmdNumero5.Size = New System.Drawing.Size(56, 35)
        Me.CmdNumero5.TabIndex = 8
        Me.CmdNumero5.Text = "5"
        '
        'CmdNumero4
        '
        Me.CmdNumero4.Location = New System.Drawing.Point(5, 137)
        Me.CmdNumero4.Name = "CmdNumero4"
        Me.CmdNumero4.Size = New System.Drawing.Size(56, 35)
        Me.CmdNumero4.TabIndex = 7
        Me.CmdNumero4.Text = "4"
        '
        'CmdNumero9
        '
        Me.CmdNumero9.Location = New System.Drawing.Point(119, 178)
        Me.CmdNumero9.Name = "CmdNumero9"
        Me.CmdNumero9.Size = New System.Drawing.Size(56, 33)
        Me.CmdNumero9.TabIndex = 12
        Me.CmdNumero9.Text = "9"
        '
        'CmdNumero8
        '
        Me.CmdNumero8.Location = New System.Drawing.Point(62, 178)
        Me.CmdNumero8.Name = "CmdNumero8"
        Me.CmdNumero8.Size = New System.Drawing.Size(56, 33)
        Me.CmdNumero8.TabIndex = 11
        Me.CmdNumero8.Text = "8"
        '
        'CmdNumero7
        '
        Me.CmdNumero7.Location = New System.Drawing.Point(5, 178)
        Me.CmdNumero7.Name = "CmdNumero7"
        Me.CmdNumero7.Size = New System.Drawing.Size(56, 33)
        Me.CmdNumero7.TabIndex = 10
        Me.CmdNumero7.Text = "7"
        '
        'CmdDecimal
        '
        Me.CmdDecimal.Location = New System.Drawing.Point(119, 217)
        Me.CmdDecimal.Name = "CmdDecimal"
        Me.CmdDecimal.Size = New System.Drawing.Size(56, 36)
        Me.CmdDecimal.TabIndex = 14
        Me.CmdDecimal.Text = "."
        '
        'CmdNumero0
        '
        Me.CmdNumero0.Location = New System.Drawing.Point(5, 217)
        Me.CmdNumero0.Name = "CmdNumero0"
        Me.CmdNumero0.Size = New System.Drawing.Size(113, 36)
        Me.CmdNumero0.TabIndex = 13
        Me.CmdNumero0.Text = "0"
        '
        'CmdEnter
        '
        Me.CmdEnter.Location = New System.Drawing.Point(181, 93)
        Me.CmdEnter.Name = "CmdEnter"
        Me.CmdEnter.Size = New System.Drawing.Size(54, 160)
        Me.CmdEnter.TabIndex = 15
        Me.CmdEnter.Text = "Enter"
        '
        'CmdBack
        '
        Me.CmdBack.Location = New System.Drawing.Point(179, 49)
        Me.CmdBack.Name = "CmdBack"
        Me.CmdBack.Size = New System.Drawing.Size(56, 38)
        Me.CmdBack.TabIndex = 3
        Me.CmdBack.Text = "<--"
        '
        'CmdClear
        '
        Me.CmdClear.Location = New System.Drawing.Point(5, 49)
        Me.CmdClear.Name = "CmdClear"
        Me.CmdClear.Size = New System.Drawing.Size(56, 38)
        Me.CmdClear.TabIndex = 2
        Me.CmdClear.Text = "Clear"
        '
        'txtNumero
        '
        Me.txtNumero.AcceptsReturn = True
        Me.txtNumero.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Regular)
        Me.txtNumero.HideSelection = False
        Me.txtNumero.Location = New System.Drawing.Point(67, 49)
        Me.txtNumero.MaxLength = 999999
        Me.txtNumero.Multiline = True
        Me.txtNumero.Name = "txtNumero"
        Me.txtNumero.Size = New System.Drawing.Size(108, 38)
        Me.txtNumero.TabIndex = 1
        Me.txtNumero.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtNumero.WordWrap = False
        '
        'picBarraMorada
        '
        Me.picBarraMorada.Image = CType(resources.GetObject("picBarraMorada.Image"), System.Drawing.Image)
        Me.picBarraMorada.Location = New System.Drawing.Point(0, 259)
        Me.picBarraMorada.Name = "picBarraMorada"
        Me.picBarraMorada.Size = New System.Drawing.Size(240, 40)
        Me.picBarraMorada.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Gainsboro
        Me.PictureBox1.Location = New System.Drawing.Point(0, 37)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(240, 223)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'cmbSalir
        '
        Me.cmbSalir.Image = CType(resources.GetObject("cmbSalir.Image"), System.Drawing.Image)
        Me.cmbSalir.Location = New System.Drawing.Point(0, 0)
        Me.cmbSalir.Name = "cmbSalir"
        Me.cmbSalir.Size = New System.Drawing.Size(41, 38)
        Me.cmbSalir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'PictureFondoProveedorDeposito_Recepcion
        '
        Me.PictureFondoProveedorDeposito_Recepcion.Image = CType(resources.GetObject("PictureFondoProveedorDeposito_Recepcion.Image"), System.Drawing.Image)
        Me.PictureFondoProveedorDeposito_Recepcion.Location = New System.Drawing.Point(42, 0)
        Me.PictureFondoProveedorDeposito_Recepcion.Name = "PictureFondoProveedorDeposito_Recepcion"
        Me.PictureFondoProveedorDeposito_Recepcion.Size = New System.Drawing.Size(198, 38)
        Me.PictureFondoProveedorDeposito_Recepcion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'FormNumPadGeneral
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(240, 300)
        Me.ControlBox = False
        Me.Controls.Add(Me.txtNumero)
        Me.Controls.Add(Me.CmdClear)
        Me.Controls.Add(Me.CmdBack)
        Me.Controls.Add(Me.CmdEnter)
        Me.Controls.Add(Me.CmdNumero0)
        Me.Controls.Add(Me.CmdDecimal)
        Me.Controls.Add(Me.CmdNumero9)
        Me.Controls.Add(Me.CmdNumero8)
        Me.Controls.Add(Me.CmdNumero7)
        Me.Controls.Add(Me.CmdNumero6)
        Me.Controls.Add(Me.CmdNumero5)
        Me.Controls.Add(Me.CmdNumero4)
        Me.Controls.Add(Me.CmdNumero3)
        Me.Controls.Add(Me.CmdNumero2)
        Me.Controls.Add(Me.CmdNumero1)
        Me.Controls.Add(Me.picBarraMorada)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.cmbSalir)
        Me.Controls.Add(Me.PictureFondoProveedorDeposito_Recepcion)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.Name = "FormNumPadGeneral"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents picBarraMorada As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents cmbSalir As System.Windows.Forms.PictureBox
    Friend WithEvents PictureFondoProveedorDeposito_Recepcion As System.Windows.Forms.PictureBox
    Friend WithEvents cmbTeclado As System.Windows.Forms.PictureBox
    Friend WithEvents CmdNumero1 As System.Windows.Forms.Button
    Friend WithEvents CmdNumero2 As System.Windows.Forms.Button
    Friend WithEvents CmdNumero3 As System.Windows.Forms.Button
    Friend WithEvents CmdNumero6 As System.Windows.Forms.Button
    Friend WithEvents CmdNumero5 As System.Windows.Forms.Button
    Friend WithEvents CmdNumero4 As System.Windows.Forms.Button
    Friend WithEvents CmdNumero9 As System.Windows.Forms.Button
    Friend WithEvents CmdNumero8 As System.Windows.Forms.Button
    Friend WithEvents CmdNumero7 As System.Windows.Forms.Button
    Friend WithEvents CmdDecimal As System.Windows.Forms.Button
    Friend WithEvents CmdNumero0 As System.Windows.Forms.Button
    Friend WithEvents CmdEnter As System.Windows.Forms.Button
    Friend WithEvents CmdBack As System.Windows.Forms.Button
    Friend WithEvents CmdClear As System.Windows.Forms.Button
    Friend WithEvents txtNumero As System.Windows.Forms.TextBox
End Class
