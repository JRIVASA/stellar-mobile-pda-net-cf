﻿Public Class FormGuardarDatos_Recepcion

    Private Sub cmbGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbGuardar.Click

        respuesta = MsgBox("¿Está seguro que la información es correcta?", MsgBoxStyle.YesNo, "Guardar Datos")

        If respuesta = "6" Then

            'Verifica si existen recepciones pendientes por enviar y en caso de que existan las envia automaticamente
            enviarRecepcionesPendientes()

            insertaryActualizarRegistrosMA_RECEPCIONESMOVIL(FormCantidadRecibida_Recepcion.lblNºRec.Text)
            respuesta = MsgBox("La información ha sido guardada en la base de datos, ¿desea recibir otro producto?", MsgBoxStyle.YesNo, "isMOBILE")

            If respuesta = "6" Then

                limpiarFormCantidadFacturada()
                limpiarFormCantidadRecibida()
                limpiarFormLecturaProducto()
                limpiarFormGuardarDatos()

                'En caso de que se haga una recepción y no se haya procesado en stellar, la aplicación permite usar el numero de documento nuevamente
                'ya que todavia no se ha validado eso. Para validarlo tendría que buscar en la tabla MA_RECEPCIONESMOVILES tanto del PDA como del SERVIDOR,
                'si ya el numero de documento fue utilizado anteriormente para ese proveedor.

                'Actualizo TR_ODC para que la cantidad recibida y la solicitada sean las mas actualizadas en caso de que se quiera recibir un producto de una misma
                'orden de compra cuya recepcion ya fue procesada en STELLAR

                If conectarDB_SERVIDOR(rutaDB_SERVIDOR) = 1 Then

                    If codigoODC <> "" Then

                        'comandoBD_PDA.CommandText = "DELETE TR_ODC"
                        'ejecutaQuery_PDA(comandoBD_PDA)
                        'actualizarTR_ODC()

                        If CompatibilidadFueraDeLinea Then

                            Try

                                comandBD_SERVIDOR.CommandText = "INSERT INTO TR_PEND_PDA_ODC (C_DOCUMENTO, C_STATUS, C_LOCALIDAD, ID_PDA) VALUES (@DOC, @STAT, @LOC, @PDA)"
                                comandBD_SERVIDOR.Parameters.Add("@DOC", codigoODC)
                                comandBD_SERVIDOR.Parameters.Add("@STAT", "DPE")
                                comandBD_SERVIDOR.Parameters.Add("@LOC", Localidad)
                                comandBD_SERVIDOR.Parameters.Add("@PDA", Id_PDA)
                                comandBD_SERVIDOR.ExecuteNonQuery()
                                comandBD_SERVIDOR.Parameters.Clear()

                                ActualizacionParcial("ODC")

                            Catch ex As Exception

                                MsgBox("Se ha producido un error al actualizar la orden de compra en el dispositivo." & vbNewLine & "A continuacion se intentara actualizar la orden de compra en conexion directa al servidor.", MsgBoxStyle.Information, "isMOBILE")

                                Try
                                    ActualizacionParcial("ODC", codigoODC)
                                    MsgBox("Actualización exitosa.")
                                Catch ex2 As Exception
                                    MsgBox("No se pudo completar la operación. Por favor, contacte al departamento de soporte técnico.")
                                End Try

                            End Try

                            cerrarConexion_SERVIDOR()

                        End If

                    End If

                    FormLecturaProducto_Recepcion.Show()
                    Me.Visible = False

                Else
                    'No es recomendable recibir otro producto sin antes actualizar la tabla TR_ODC ya que la cantidad recibida puede haber cambiado en la tabla 
                    MsgBox("No se pueden actualizar las ordenes de compra, no hay conexión con el servidor, comuniquese con el departamento de sistemas.", MsgBoxStyle.Information, "isMOBILE")
                    MsgBox("No se recomienda recibir un nuevo producto sin antes haber establecido la conexión con el servidor.", MsgBoxStyle.Exclamation, "isMOBILE")
                    FormLecturaProducto_Recepcion.Show()
                    Me.Visible = False
                End If

                'Limpio las variables globales necesarias
                verFormCantidadFacturada = True
                verFormCantidadFacturadaSinODC = True

            Else 'No desea recibir otro producto

                limpiarFormCantidadFacturada()
                FormCantidadFacturada_Recepcion.lblNºRec.Text = ""
                FormCantidadFacturada_Recepcion.lblFechaRecepcion.Text = ""

                limpiarFormCantidadRecibida()
                FormCantidadRecibida_Recepcion.lblNºRec.Text = ""
                FormCantidadRecibida_Recepcion.lblFechaRecepcion.Text = ""

                limpiarFormLecturaProducto()
                FormLecturaProducto_Recepcion.lblNºRec.Text = ""
                FormLecturaProducto_Recepcion.lblFechaRecepcion.Text = ""

                limpiarFormDocumentoTransportista()
                limpiarFormProveedorDeposito()

                limpiarFormGuardarDatos()

                If conectarDB_SERVIDOR(rutaDB_SERVIDOR) = 1 Then

                    If codigoODC <> "" Then

                        'comandoBD_PDA.CommandText = "DELETE TR_ODC"
                        'ejecutaQuery_PDA(comandoBD_PDA)
                        'actualizarTR_ODC()

                        If CompatibilidadFueraDeLinea Then

                            Try

                                comandBD_SERVIDOR.CommandText = "INSERT INTO TR_PEND_PDA_ODC (C_DOCUMENTO, C_STATUS, C_LOCALIDAD, ID_PDA) VALUES (@DOC, @STAT, @LOC, @PDA)"
                                comandBD_SERVIDOR.Parameters.Add("@DOC", codigoODC)
                                comandBD_SERVIDOR.Parameters.Add("@STAT", "DPE")
                                comandBD_SERVIDOR.Parameters.Add("@LOC", Localidad)
                                comandBD_SERVIDOR.Parameters.Add("@PDA", Id_PDA)
                                comandBD_SERVIDOR.ExecuteNonQuery()
                                comandBD_SERVIDOR.Parameters.Clear()

                                ActualizacionParcial("ODC")

                            Catch ex As Exception
                                MsgBox("Se ha producido un error al actualizar la orden de compra en el dispositivo." & vbNewLine & "A continuacion se intentara actualizar la orden de compra en conexion directa al servidor.", MsgBoxStyle.Information, "isMOBILE")

                                Try
                                    ActualizacionParcial("ODC", codigoODC)
                                    MsgBox("Actualización exitosa.")
                                Catch ex2 As Exception
                                    MsgBox("No se pudo completar la operación. Por favor, contacte al departamento de soporte técnico.")
                                End Try

                            End Try

                        End If

                    End If

                    cerrarConexion_SERVIDOR()

                Else

                    'No es recomendable recibir otro producto sin antes actualizar la tabla TR_ODC ya que la cantidad recibida puede haber cambiado en la tabla 
                    MsgBox("No se pueden actualizar las ordenes de compra, no hay conexión con el servidor, comuniquese con el departamento de sistemas.", MsgBoxStyle.Information, "isMOBILE")
                    MsgBox("No se recomienda recibir un nuevo producto sin antes haber establecido la conexión con el servidor.", MsgBoxStyle.Exclamation, "isMOBILE")

                End If

                'Limpio las variables globales necesarias
                codigoODC = ""
                dataset_Productos = Nothing
                dataset_ODC = Nothing
                cantidadSolicitada = 0
                verFormCantidadFacturada = True
                verFormCantidadFacturadaSinODC = True

                'Actualizo el numero de ultima recepción
                ActualizarNumUltimaRecepcion()

                formMenuPrincipal.Show()
                Me.Visible = False

            End If

        Else

            respuesta = MsgBox("¿Desea cambiar las cantidades recibidas?", MsgBoxStyle.YesNo, "isMOBILE")

            If respuesta = "6" Then
                FormCantidadRecibida_Recepcion.Show()
                Me.Visible = False
            Else
                If Not verFormCantidadFacturada = False Then
                    respuesta = MsgBox("¿Desea cambiar las cantidades facturadas?", MsgBoxStyle.YesNo, "isMOBILE")
                    If respuesta = "6" Then
                        FormCantidadFacturada_Recepcion.Show()
                        Me.Visible = False
                    End If
                End If
            End If

        End If

    End Sub

    Private Sub FormGuardarDatos_Recepcion_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblUsuarioEnSesion.Text = "Usuario: " + descripcionUsuario
        Me.Refresh()
        Me.Update()
        AjustarFormularios(Me)
    End Sub

    Private Sub cmbSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSalir.Click
        FormCantidadRecibida_Recepcion.Show()
        Me.Visible = False
    End Sub
End Class