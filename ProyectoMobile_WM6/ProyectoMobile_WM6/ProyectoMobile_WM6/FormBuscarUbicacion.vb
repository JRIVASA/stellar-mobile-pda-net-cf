﻿Public Class FormBuscarUbicacion

    Public Deposito As String
    Public Selecciono As Boolean
    Public Ubicacion As String

    Public FormaCargada As Boolean

    Private Sub DataGridBuscarDeposito_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridUbicaciones.DoubleClick
        seleccionarDepositoDataGrid()
    End Sub

    Private Sub txtDescripcionDeposito_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtUbicacion.KeyDown
        If e.KeyValue = 13 Then
            If txtUbicacion.Text = "" Then
                MsgBox("Debe colocar en Descripción al menos un carácter", MsgBoxStyle.Information, "isMOBILE")
                txtUbicacion.Focus()
            Else
                LlenaDataGridBuscarUbicacion(txtUbicacion.Text)
            End If
        End If
    End Sub

    Private Sub cmbBuscarDeposito_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmbBuscarUbicacion.Click
        'If txtDescripcionDeposito.Text = "" Then
        'MsgBox("Debe colocar al menos un carácter en Descripción", MsgBoxStyle.Information, "isMOBILE")
        'txtDescripcionDeposito.Focus()
        'Else
        LlenaDataGridBuscarUbicacion(txtUbicacion.Text)
        'End If
    End Sub

    Private Sub cmbSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSalir.Click
        txtUbicacion.Text = ""
        DataGridUbicaciones.DataSource = Nothing
        Selecciono = False
        Ubicacion = String.Empty
        Me.DialogResult = Forms.DialogResult.OK
        Me.Hide()
    End Sub

    Private Sub cmbAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmbAceptar.Click
        SeleccionarUbicacion()
    End Sub

    Private Sub FormBuscarDeposito_Recepcion_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not FormaCargada Then
            FormaCargada = True
            AjustarFormularios(Me)
        End If

        lblUsuarioEnSesion.Text = "Usuario: " + descripcionUsuario
        Me.Refresh()

    End Sub

    Sub SeleccionarUbicacion()

        Dim FilaDataGrid As Long = DataGridUbicaciones.CurrentRowIndex

        If FilaDataGrid = -1 Then
            MsgBox("No seleccionó ninguna ubicación.", MsgBoxStyle.Information, "isMOBILE")
        Else
            txtUbicacion.Text = ""
            DataGridUbicaciones.DataSource = Nothing
            Selecciono = True
            Ubicacion = DataGridUbicaciones.Item(FilaDataGrid, 0)
            Me.DialogResult = Forms.DialogResult.OK
            Me.Hide()
        End If

    End Sub

End Class