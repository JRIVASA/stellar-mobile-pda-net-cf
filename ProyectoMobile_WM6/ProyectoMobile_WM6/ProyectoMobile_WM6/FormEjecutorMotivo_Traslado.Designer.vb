﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class FormEjecutorMotivo_Traslado
    Inherits System.Windows.Forms.Form

    'Form invalida a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer
    Private mainMenu1 As System.Windows.Forms.MainMenu

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar con el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormEjecutorMotivo_Traslado))
        Me.mainMenu1 = New System.Windows.Forms.MainMenu
        Me.txtEjecutor = New System.Windows.Forms.TextBox
        Me.lblEjecutor = New System.Windows.Forms.Label
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.lblUsuarioEnSesion = New System.Windows.Forms.Label
        Me.cmbSiguiente = New System.Windows.Forms.Button
        Me.cmbSalir = New System.Windows.Forms.PictureBox
        Me.PictureFondoProveedorDeposito_Recepcion = New System.Windows.Forms.PictureBox
        Me.picBarraMorada = New System.Windows.Forms.PictureBox
        Me.lblMotivo = New System.Windows.Forms.Label
        Me.cboConceptosTraslados = New System.Windows.Forms.ComboBox
        Me.SuspendLayout()
        '
        'txtEjecutor
        '
        Me.txtEjecutor.Font = New System.Drawing.Font("Verdana", 14.0!, System.Drawing.FontStyle.Bold)
        Me.txtEjecutor.Location = New System.Drawing.Point(17, 98)
        Me.txtEjecutor.MaxLength = 50
        Me.txtEjecutor.Name = "txtEjecutor"
        Me.txtEjecutor.Size = New System.Drawing.Size(208, 29)
        Me.txtEjecutor.TabIndex = 49
        '
        'lblEjecutor
        '
        Me.lblEjecutor.BackColor = System.Drawing.Color.Gainsboro
        Me.lblEjecutor.Font = New System.Drawing.Font("Tahoma", 14.0!, System.Drawing.FontStyle.Bold)
        Me.lblEjecutor.Location = New System.Drawing.Point(28, 65)
        Me.lblEjecutor.Name = "lblEjecutor"
        Me.lblEjecutor.Size = New System.Drawing.Size(178, 24)
        Me.lblEjecutor.Text = "Ejecutor"
        Me.lblEjecutor.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Gainsboro
        Me.PictureBox1.Location = New System.Drawing.Point(0, 57)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(240, 203)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'lblUsuarioEnSesion
        '
        Me.lblUsuarioEnSesion.BackColor = System.Drawing.Color.DarkGray
        Me.lblUsuarioEnSesion.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblUsuarioEnSesion.Location = New System.Drawing.Point(0, 39)
        Me.lblUsuarioEnSesion.Name = "lblUsuarioEnSesion"
        Me.lblUsuarioEnSesion.Size = New System.Drawing.Size(240, 15)
        Me.lblUsuarioEnSesion.Text = "Usuario:"
        '
        'cmbSiguiente
        '
        Me.cmbSiguiente.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.cmbSiguiente.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.cmbSiguiente.ForeColor = System.Drawing.Color.Black
        Me.cmbSiguiente.Location = New System.Drawing.Point(78, 265)
        Me.cmbSiguiente.Name = "cmbSiguiente"
        Me.cmbSiguiente.Size = New System.Drawing.Size(78, 25)
        Me.cmbSiguiente.TabIndex = 50
        Me.cmbSiguiente.Text = "Siguiente"
        '
        'cmbSalir
        '
        Me.cmbSalir.Image = CType(resources.GetObject("cmbSalir.Image"), System.Drawing.Image)
        Me.cmbSalir.Location = New System.Drawing.Point(0, 0)
        Me.cmbSalir.Name = "cmbSalir"
        Me.cmbSalir.Size = New System.Drawing.Size(41, 38)
        Me.cmbSalir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'PictureFondoProveedorDeposito_Recepcion
        '
        Me.PictureFondoProveedorDeposito_Recepcion.Image = CType(resources.GetObject("PictureFondoProveedorDeposito_Recepcion.Image"), System.Drawing.Image)
        Me.PictureFondoProveedorDeposito_Recepcion.Location = New System.Drawing.Point(42, 0)
        Me.PictureFondoProveedorDeposito_Recepcion.Name = "PictureFondoProveedorDeposito_Recepcion"
        Me.PictureFondoProveedorDeposito_Recepcion.Size = New System.Drawing.Size(198, 38)
        Me.PictureFondoProveedorDeposito_Recepcion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'picBarraMorada
        '
        Me.picBarraMorada.Image = CType(resources.GetObject("picBarraMorada.Image"), System.Drawing.Image)
        Me.picBarraMorada.Location = New System.Drawing.Point(0, 259)
        Me.picBarraMorada.Name = "picBarraMorada"
        Me.picBarraMorada.Size = New System.Drawing.Size(240, 40)
        Me.picBarraMorada.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'lblMotivo
        '
        Me.lblMotivo.BackColor = System.Drawing.Color.Gainsboro
        Me.lblMotivo.Font = New System.Drawing.Font("Tahoma", 14.0!, System.Drawing.FontStyle.Bold)
        Me.lblMotivo.Location = New System.Drawing.Point(28, 149)
        Me.lblMotivo.Name = "lblMotivo"
        Me.lblMotivo.Size = New System.Drawing.Size(178, 24)
        Me.lblMotivo.Text = "Motivo"
        Me.lblMotivo.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'cboConceptosTraslados
        '
        Me.cboConceptosTraslados.Font = New System.Drawing.Font("Verdana", 11.0!, System.Drawing.FontStyle.Regular)
        Me.cboConceptosTraslados.Location = New System.Drawing.Point(17, 182)
        Me.cboConceptosTraslados.Name = "cboConceptosTraslados"
        Me.cboConceptosTraslados.Size = New System.Drawing.Size(208, 24)
        Me.cboConceptosTraslados.TabIndex = 61
        '
        'FormEjecutorMotivo_Traslado
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(240, 300)
        Me.ControlBox = False
        Me.Controls.Add(Me.cboConceptosTraslados)
        Me.Controls.Add(Me.lblMotivo)
        Me.Controls.Add(Me.cmbSiguiente)
        Me.Controls.Add(Me.picBarraMorada)
        Me.Controls.Add(Me.txtEjecutor)
        Me.Controls.Add(Me.lblEjecutor)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.lblUsuarioEnSesion)
        Me.Controls.Add(Me.cmbSalir)
        Me.Controls.Add(Me.PictureFondoProveedorDeposito_Recepcion)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormEjecutorMotivo_Traslado"
        Me.Text = "isMOBILE - Traslados"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtEjecutor As System.Windows.Forms.TextBox
    Friend WithEvents lblEjecutor As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents lblUsuarioEnSesion As System.Windows.Forms.Label
    Friend WithEvents cmbSiguiente As System.Windows.Forms.Button
    Friend WithEvents cmbSalir As System.Windows.Forms.PictureBox
    Friend WithEvents PictureFondoProveedorDeposito_Recepcion As System.Windows.Forms.PictureBox
    Friend WithEvents picBarraMorada As System.Windows.Forms.PictureBox
    Friend WithEvents lblMotivo As System.Windows.Forms.Label
    Friend WithEvents cboConceptosTraslados As System.Windows.Forms.ComboBox
End Class
