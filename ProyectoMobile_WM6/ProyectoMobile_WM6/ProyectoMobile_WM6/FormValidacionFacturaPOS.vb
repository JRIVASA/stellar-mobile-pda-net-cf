﻿Imports System.Data.SqlServerCe
Imports System.Net
Imports System.Data.SqlClient
Imports System.Data
Imports System.Data.Common.DbConnection
Imports System.Data.Common.DbCommand
Imports System.Data.Common.DbDataAdapter

Public Class FormValidacionFacturaPOS

    Private NroDocumento As String
    Private TimeStamp As DateTime
    'Private Escribiendo As Boolean

    Private FormaCargada As Boolean

    Private Sub FormValidacionFacturaPOS_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Refresh()
        Me.Update()

        AjustarFormularios(Me)

    End Sub

    Private Sub cmbSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSalir.Click
        LimpiarDatos()
        formMenuPrincipal.Show()
        Me.Visible = False
    End Sub

    Private Sub txtDocumento_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDocumento.KeyDown
        If e.KeyValue = Keys.Enter Then cmbValidar_Click(sender, Nothing)
    End Sub

    Private Sub LimpiarDatos()

        txtDocumento.Text = ""
        txtDocumento.Enabled = True

        NroDocumento = ""

        lblResultInfoL1.Text = ""
        lblResultInfoL2.Text = ""

        PicValidacion.Visible = False

        Try : txtDocumento.Focus() : Catch : End Try

        ResultShowTime.Enabled = False
        ResultShowTime.Interval = 1500

        cerrarConexion_SERVIDORPOS()

    End Sub

    Private Function EnviarDocumentosPendientes(ByVal Connection As SqlConnection) As Boolean

        Dim Pendientes As New DataSet
        Dim Adapter As New SqlCeDataAdapter
        Dim Builder As SqlCeCommandBuilder = Nothing
        Dim SRVTrans As SqlTransaction = Nothing
        Dim PDATrans As SqlCeTransaction = Nothing

        Try

            'SRVTrans = Connection.BeginTransaction(IsolationLevel.ReadUncommitted)
            PDATrans = conexionBD_PDA.BeginTransaction()

            comandoBD_PDA.Parameters.Clear()
            comandoBD_PDA.CommandText = "SELECT * FROM MA_DOCUMENTOS_VERIFICADOS"
            comandoBD_PDA.Connection = conexionBD_PDA

            Adapter.SelectCommand = comandoBD_PDA
            Adapter.Fill(Pendientes)

            For Each Row As DataRow In Pendientes.Tables(0).Rows

                Try

                    comandBD_SERVIDOR.Parameters.Clear()

                    comandBD_SERVIDOR.Parameters.Add("@Doc", Convert.ToString(Row("cu_DocumentoStellar")))
                    comandBD_SERVIDOR.Parameters.Add("@Tipo", Convert.ToString(Row("cu_DocumentoTipo")))
                    comandBD_SERVIDOR.Connection = conexionBD_SERVIDORPOS

                    comandBD_SERVIDOR.CommandText = "SELECT COUNT(C_NUMERO) FROM MA_PAGOS WHERE C_NUMERO = @Doc AND C_CONCEPTO = @Tipo"

                    Dim Existe As Boolean = CBool(comandBD_SERVIDOR.ExecuteScalar())

                    If (Existe) Then
                        comandBD_SERVIDOR.CommandText = "INSERT INTO MA_DOCUMENTOS_VERIFICADOS (cu_DocumentoStellar, cu_DocumentoTipo, cu_Localidad)" + _
                        vbNewLine + "VALUES (@Doc, @Tipo, @Loc)"

                        comandBD_SERVIDOR.Parameters.Add("@Loc", Convert.ToString(Row("cu_Localidad")))

                        comandBD_SERVIDOR.ExecuteNonQuery()
                    End If

                Catch AnyInner As Exception

                End Try

                Row.Delete()

            Next

            Builder = New SqlCeCommandBuilder(Adapter)
            Adapter.DeleteCommand = Builder.GetDeleteCommand(False)
            Adapter.ContinueUpdateOnError = True
            Adapter.Update(Pendientes)

            'SRVTrans.Commit()
            PDATrans.Commit()

        Catch Any As Exception

            If (Not PDATrans Is Nothing) Then PDATrans.Rollback()
            'If (Not SRVTrans Is Nothing) Then SRVTrans.Rollback()
            Return False

        Finally

            comandoBD_PDA.Parameters.Clear()
            comandBD_SERVIDOR.Parameters.Clear()

            If (Not Builder Is Nothing) Then Builder.Dispose()
            If (Not Adapter Is Nothing) Then Adapter.Dispose()
            If (Not Pendientes Is Nothing) Then Pendientes.Dispose()
            If (Not PDATrans Is Nothing) Then PDATrans.Dispose()
            'If (Not SRVTrans Is Nothing) Then SRVTrans.Dispose()

        End Try

        EnviarDocumentosPendientes = True

    End Function

    Private Sub cmbValidar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbValidar.Click

        If txtDocumento.Text = "" Then
            MsgBox("Debe introducir un número de documento para realizar la consulta.", MsgBoxStyle.Critical, "isMOBILE")
        Else

            NroDocumento = txtDocumento.Text.Trim()

            If conectarDB_SERVIDORPOS(rutaDB_SERVIDOR) = 1 Then

                EnviarDocumentosPendientes(conexionBD_SERVIDORPOS)

                comandBD_SERVIDOR.Parameters.Clear()

                comandBD_SERVIDOR.Parameters.AddWithValue("@Doc", NroDocumento)
                comandBD_SERVIDOR.Parameters.AddWithValue("@Tipo", "VEN")

                comandBD_SERVIDOR.CommandText = "SELECT COUNT(C_NUMERO) AS Documento FROM MA_PAGOS WHERE C_NUMERO = @Doc AND C_CONCEPTO = @Tipo"

                Dim Resp As Integer = CInt(GetScalar_SRV(comandBD_SERVIDOR, conexionBD_SERVIDORPOS, -1, -1))

                If (Resp = (-1)) Then
                    MsgBox("Ha ocurrido un error al hacer la consulta. Verifique la conexión o intente mas tarde.", MsgBoxStyle.Critical, "isMOBILE")
                    LimpiarDatos()
                    Return
                ElseIf (Not CBool(Resp)) Then
                    MsgBox("La factura no existe.", MsgBoxStyle.Critical, "isMOBILE")
                    LimpiarDatos()
                    Return
                End If

                comandBD_SERVIDOR.CommandText = "SELECT COUNT(cu_DocumentoStellar) AS Documento FROM MA_DOCUMENTOS_VERIFICADOS" _
                + vbNewLine + "WHERE cu_DocumentoStellar = @Doc AND cu_DocumentoTipo = @Tipo"

                Resp = CInt(GetScalar_SRV(comandBD_SERVIDOR, conexionBD_SERVIDORPOS, -1, -1))

                Dim Existe As Boolean

                If (Resp = (-1)) Then
                    MsgBox("Ha ocurrido un error al hacer la consulta. Verifique la conexión o intente mas tarde.", MsgBoxStyle.Critical, "isMOBILE")
                    LimpiarDatos()
                    Return
                Else
                    Existe = CBool(Resp)
                End If

                If (Existe) Then
                    PicValidacion.Image = My.Resources.Wrong
                    PicValidacion.Visible = True
                    lblResultInfoL1.Text = ValidacionFacturaPOS_MensajeIncorrectoLinea1
                    lblResultInfoL2.Text = ValidacionFacturaPOS_MensajeIncorrectoLinea2
                Else

                    Dim mGrabo As Boolean

                    Try

                        comandBD_SERVIDOR.Parameters.AddWithValue("@Loc", Localidad)
                        comandBD_SERVIDOR.CommandText = "INSERT INTO MA_DOCUMENTOS_VERIFICADOS VALUES (@Doc, @Tipo, @Loc)"
                        comandBD_SERVIDOR.Connection = conexionBD_SERVIDORPOS
                        comandBD_SERVIDOR.ExecuteNonQuery()
                        mGrabo = True

                    Catch Any As Exception
                        MessageBox.Show("Error al registrar documento: " + Any.Message)
                    End Try

                    If mGrabo Then

                        PicValidacion.Image = My.Resources.Valid
                        PicValidacion.Visible = True
                        lblResultInfoL1.Text = ValidacionFacturaPOS_MensajeCorrectoLinea1
                        lblResultInfoL2.Text = ValidacionFacturaPOS_MensajeCorrectoLinea2

                    End If

                End If

                txtDocumento.Enabled = False

                ResultShowTime.Enabled = True

            Else

                If CompatibilidadFueraDeLinea Then

                    comandoBD_PDA.Parameters.Clear()
                    comandoBD_PDA.CommandText = "SELECT COUNT(cu_DocumentoStellar) FROM MA_DOCUMENTOS_VERIFICADOS WHERE cu_DocumentoStellar = @Doc AND cu_DocumentoTipo = @Tipo"
                    comandoBD_PDA.Connection = conexionBD_PDA

                    comandoBD_PDA.Parameters.Add("@Doc", NroDocumento)
                    comandoBD_PDA.Parameters.Add("@Tipo", "VEN")

                    Dim Resp As Integer = CInt(GetScalar_PDA(comandoBD_PDA, -1, -1))
                    Dim Existe As Boolean

                    If (Resp = (-1)) Then
                        MsgBox("Ha ocurrido un error al hacer la consulta. Verifique la conexión o intente mas tarde.", MsgBoxStyle.Critical, "isMOBILE")
                        LimpiarDatos()
                        Return
                    Else
                        Existe = CBool(Resp)
                    End If

                    If (Existe) Then
                        PicValidacion.Image = My.Resources.Wrong
                        PicValidacion.Visible = True
                        lblResultInfoL1.Text = ValidacionFacturaPOS_MensajeIncorrectoLinea1
                        lblResultInfoL2.Text = ValidacionFacturaPOS_MensajeIncorrectoLinea2
                    Else
                        comandoBD_PDA.Parameters.Add("@Loc", Localidad)
                        comandoBD_PDA.CommandText = "INSERT INTO MA_DOCUMENTOS_VERIFICADOS VALUES (@Doc, @Tipo, @Loc)"
                        ejecutaQuery_PDA(comandoBD_PDA, False)

                        PicValidacion.Image = My.Resources.Valid
                        PicValidacion.Visible = True
                        lblResultInfoL1.Text = ValidacionFacturaPOS_MensajeCorrectoLinea1
                        lblResultInfoL2.Text = ValidacionFacturaPOS_MensajeCorrectoLinea2
                    End If

                    txtDocumento.Enabled = False

                    ResultShowTime.Enabled = True

                Else

                    MessageBox.Show(MsjSinConexionGenerico)

                End If

            End If

        End If

    End Sub

    Private Sub ResultShowTime_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ResultShowTime.Tick
        LimpiarDatos()
    End Sub

    Private Sub FormValidacionFacturaPOS_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated

        TimerReload.Interval = 100
        TimerReload.Enabled = True

    End Sub

    Private Sub txtDocumento_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDocumento.KeyPress
        If (ValidacionFacturaPOS_TiempoEscritura > 0) Then
            Dim CurrentTime As DateTime = DateTime.Now

            Dim Diferencia As TimeSpan = (CurrentTime - TimeStamp)

            If (Diferencia.TotalMilliseconds > ValidacionFacturaPOS_TiempoEscritura And txtDocumento.Text <> "") Then
                TimeStamp = CurrentTime
                txtDocumento.Text = ""
                e.Handled = True
            Else
                TimeStamp = DateTime.Now
                e.Handled = False
            End If
        Else
            e.Handled = False
        End If
    End Sub

    Private Sub Activation()

        If Not FormaCargada Then

            FormaCargada = True

            lblUsuarioEnSesion.Text = "Usuario: " + descripcionUsuario

            LimpiarDatos()

        End If

    End Sub

    Private Sub TimerReload_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TimerReload.Tick
        TimerReload.Enabled = False
        Activation()
    End Sub

End Class