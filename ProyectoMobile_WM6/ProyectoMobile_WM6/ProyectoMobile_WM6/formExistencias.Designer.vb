﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class FormExistencias
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormExistencias))
        Me.PictureFondo_FormBarraProgreso = New System.Windows.Forms.PictureBox
        Me.salirButton = New System.Windows.Forms.PictureBox
        Me.gridExistencia = New System.Windows.Forms.DataGrid
        Me.hideButton = New System.Windows.Forms.Button
        Me.lblStatus_MenuPrincipal = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'PictureFondo_FormBarraProgreso
        '
        Me.PictureFondo_FormBarraProgreso.BackColor = System.Drawing.Color.Gainsboro
        Me.PictureFondo_FormBarraProgreso.Image = CType(resources.GetObject("PictureFondo_FormBarraProgreso.Image"), System.Drawing.Image)
        Me.PictureFondo_FormBarraProgreso.Location = New System.Drawing.Point(42, 0)
        Me.PictureFondo_FormBarraProgreso.Name = "PictureFondo_FormBarraProgreso"
        Me.PictureFondo_FormBarraProgreso.Size = New System.Drawing.Size(198, 38)
        Me.PictureFondo_FormBarraProgreso.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'salirButton
        '
        Me.salirButton.Enabled = False
        Me.salirButton.Image = CType(resources.GetObject("salirButton.Image"), System.Drawing.Image)
        Me.salirButton.Location = New System.Drawing.Point(0, 0)
        Me.salirButton.Name = "salirButton"
        Me.salirButton.Size = New System.Drawing.Size(41, 38)
        Me.salirButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'gridExistencia
        '
        Me.gridExistencia.BackgroundColor = System.Drawing.Color.Gainsboro
        Me.gridExistencia.GridLineColor = System.Drawing.Color.Gainsboro
        Me.gridExistencia.HeaderBackColor = System.Drawing.Color.Gainsboro
        Me.gridExistencia.Location = New System.Drawing.Point(0, 56)
        Me.gridExistencia.Name = "gridExistencia"
        Me.gridExistencia.Size = New System.Drawing.Size(240, 132)
        Me.gridExistencia.TabIndex = 4
        '
        'hideButton
        '
        Me.hideButton.BackColor = System.Drawing.Color.Gainsboro
        Me.hideButton.Location = New System.Drawing.Point(68, 194)
        Me.hideButton.Name = "hideButton"
        Me.hideButton.Size = New System.Drawing.Size(96, 21)
        Me.hideButton.TabIndex = 5
        Me.hideButton.Text = "Cerrar"
        '
        'lblStatus_MenuPrincipal
        '
        Me.lblStatus_MenuPrincipal.BackColor = System.Drawing.Color.DarkGray
        Me.lblStatus_MenuPrincipal.Location = New System.Drawing.Point(0, 39)
        Me.lblStatus_MenuPrincipal.Name = "lblStatus_MenuPrincipal"
        Me.lblStatus_MenuPrincipal.Size = New System.Drawing.Size(240, 16)
        Me.lblStatus_MenuPrincipal.Visible = False
        '
        'FormExistencias
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.Gainsboro
        Me.ClientSize = New System.Drawing.Size(240, 300)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblStatus_MenuPrincipal)
        Me.Controls.Add(Me.hideButton)
        Me.Controls.Add(Me.salirButton)
        Me.Controls.Add(Me.gridExistencia)
        Me.Controls.Add(Me.PictureFondo_FormBarraProgreso)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormExistencias"
        Me.Text = "isMOBILE - Consulta"
        Me.TopMost = True
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PictureFondo_FormBarraProgreso As System.Windows.Forms.PictureBox
    Friend WithEvents salirButton As System.Windows.Forms.PictureBox
    Friend WithEvents gridExistencia As System.Windows.Forms.DataGrid
    Friend WithEvents hideButton As System.Windows.Forms.Button
    Friend WithEvents lblStatus_MenuPrincipal As System.Windows.Forms.Label
End Class
