﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class FormElab_Inv_InventarioFisico
    Inherits System.Windows.Forms.Form

    'Form invalida a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar con el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormElab_Inv_InventarioFisico))
        Me.txtCodigoProducto = New System.Windows.Forms.TextBox
        Me.lblDescripcion = New System.Windows.Forms.Label
        Me.txtEmpaque = New System.Windows.Forms.TextBox
        Me.lblEmpaque = New System.Windows.Forms.Label
        Me.lblUnidad = New System.Windows.Forms.Label
        Me.lblNumDeposito = New System.Windows.Forms.Label
        Me.lblNumDoc = New System.Windows.Forms.Label
        Me.lblDeposito = New System.Windows.Forms.Label
        Me.lblNºDoc = New System.Windows.Forms.Label
        Me.txtUnidad = New System.Windows.Forms.TextBox
        Me.cmbAgregar = New System.Windows.Forms.Button
        Me.lblCantidadProducto = New System.Windows.Forms.Label
        Me.lblCodigoProducto = New System.Windows.Forms.Label
        Me.lblUsuarioEnSesion = New System.Windows.Forms.Label
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.cmbSalir = New System.Windows.Forms.PictureBox
        Me.picBarraMorada = New System.Windows.Forms.PictureBox
        Me.PictureFondoINVENTARIO_ELAB_INV = New System.Windows.Forms.PictureBox
        Me.cmbFinalizar = New System.Windows.Forms.Button
        Me.cmbActualizar = New System.Windows.Forms.PictureBox
        Me.chkQuickScan = New System.Windows.Forms.CheckBox
        Me.ChkRevisar = New System.Windows.Forms.CheckBox
        Me.SuspendLayout()
        '
        'txtCodigoProducto
        '
        Me.txtCodigoProducto.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.txtCodigoProducto.Location = New System.Drawing.Point(25, 116)
        Me.txtCodigoProducto.MaxLength = 30
        Me.txtCodigoProducto.Name = "txtCodigoProducto"
        Me.txtCodigoProducto.Size = New System.Drawing.Size(187, 23)
        Me.txtCodigoProducto.TabIndex = 84
        '
        'lblDescripcion
        '
        Me.lblDescripcion.BackColor = System.Drawing.Color.Gainsboro
        Me.lblDescripcion.Location = New System.Drawing.Point(26, 146)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(186, 38)
        Me.lblDescripcion.Text = "Descripción:"
        '
        'txtEmpaque
        '
        Me.txtEmpaque.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.txtEmpaque.Location = New System.Drawing.Point(132, 204)
        Me.txtEmpaque.MaxLength = 10
        Me.txtEmpaque.Name = "txtEmpaque"
        Me.txtEmpaque.Size = New System.Drawing.Size(57, 23)
        Me.txtEmpaque.TabIndex = 87
        '
        'lblEmpaque
        '
        Me.lblEmpaque.BackColor = System.Drawing.Color.Gainsboro
        Me.lblEmpaque.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.lblEmpaque.Location = New System.Drawing.Point(26, 204)
        Me.lblEmpaque.Name = "lblEmpaque"
        Me.lblEmpaque.Size = New System.Drawing.Size(101, 23)
        Me.lblEmpaque.Text = "Empaques"
        Me.lblEmpaque.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblUnidad
        '
        Me.lblUnidad.BackColor = System.Drawing.Color.Gainsboro
        Me.lblUnidad.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.lblUnidad.Location = New System.Drawing.Point(63, 232)
        Me.lblUnidad.Name = "lblUnidad"
        Me.lblUnidad.Size = New System.Drawing.Size(63, 24)
        Me.lblUnidad.Text = "Unidades"
        Me.lblUnidad.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblNumDeposito
        '
        Me.lblNumDeposito.BackColor = System.Drawing.Color.Gainsboro
        Me.lblNumDeposito.Font = New System.Drawing.Font("Tahoma", 7.0!, System.Drawing.FontStyle.Bold)
        Me.lblNumDeposito.Location = New System.Drawing.Point(64, 74)
        Me.lblNumDeposito.Name = "lblNumDeposito"
        Me.lblNumDeposito.Size = New System.Drawing.Size(55, 14)
        '
        'lblNumDoc
        '
        Me.lblNumDoc.BackColor = System.Drawing.Color.Gainsboro
        Me.lblNumDoc.Font = New System.Drawing.Font("Tahoma", 7.0!, System.Drawing.FontStyle.Bold)
        Me.lblNumDoc.Location = New System.Drawing.Point(64, 60)
        Me.lblNumDoc.Name = "lblNumDoc"
        Me.lblNumDoc.Size = New System.Drawing.Size(55, 14)
        '
        'lblDeposito
        '
        Me.lblDeposito.BackColor = System.Drawing.Color.Gainsboro
        Me.lblDeposito.Font = New System.Drawing.Font("Tahoma", 7.0!, System.Drawing.FontStyle.Bold)
        Me.lblDeposito.Location = New System.Drawing.Point(3, 74)
        Me.lblDeposito.Name = "lblDeposito"
        Me.lblDeposito.Size = New System.Drawing.Size(55, 14)
        Me.lblDeposito.Text = "Deposito:"
        '
        'lblNºDoc
        '
        Me.lblNºDoc.BackColor = System.Drawing.Color.Gainsboro
        Me.lblNºDoc.Font = New System.Drawing.Font("Tahoma", 7.0!, System.Drawing.FontStyle.Bold)
        Me.lblNºDoc.Location = New System.Drawing.Point(3, 60)
        Me.lblNºDoc.Name = "lblNºDoc"
        Me.lblNºDoc.Size = New System.Drawing.Size(55, 14)
        Me.lblNºDoc.Text = "Nº Doc.:"
        '
        'txtUnidad
        '
        Me.txtUnidad.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.txtUnidad.Location = New System.Drawing.Point(132, 233)
        Me.txtUnidad.MaxLength = 10
        Me.txtUnidad.Name = "txtUnidad"
        Me.txtUnidad.Size = New System.Drawing.Size(57, 23)
        Me.txtUnidad.TabIndex = 85
        '
        'cmbAgregar
        '
        Me.cmbAgregar.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.cmbAgregar.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.cmbAgregar.ForeColor = System.Drawing.Color.Black
        Me.cmbAgregar.Location = New System.Drawing.Point(149, 268)
        Me.cmbAgregar.Name = "cmbAgregar"
        Me.cmbAgregar.Size = New System.Drawing.Size(78, 25)
        Me.cmbAgregar.TabIndex = 94
        Me.cmbAgregar.Text = "Agregar"
        '
        'lblCantidadProducto
        '
        Me.lblCantidadProducto.BackColor = System.Drawing.Color.Gainsboro
        Me.lblCantidadProducto.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblCantidadProducto.Location = New System.Drawing.Point(42, 184)
        Me.lblCantidadProducto.Name = "lblCantidadProducto"
        Me.lblCantidadProducto.Size = New System.Drawing.Size(160, 17)
        Me.lblCantidadProducto.Text = "Cantidad a Registrar"
        Me.lblCantidadProducto.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblCodigoProducto
        '
        Me.lblCodigoProducto.BackColor = System.Drawing.Color.Gainsboro
        Me.lblCodigoProducto.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblCodigoProducto.Location = New System.Drawing.Point(54, 97)
        Me.lblCodigoProducto.Name = "lblCodigoProducto"
        Me.lblCodigoProducto.Size = New System.Drawing.Size(135, 16)
        Me.lblCodigoProducto.Text = "Codigo Producto"
        '
        'lblUsuarioEnSesion
        '
        Me.lblUsuarioEnSesion.BackColor = System.Drawing.Color.DarkGray
        Me.lblUsuarioEnSesion.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblUsuarioEnSesion.Location = New System.Drawing.Point(0, 40)
        Me.lblUsuarioEnSesion.Name = "lblUsuarioEnSesion"
        Me.lblUsuarioEnSesion.Size = New System.Drawing.Size(240, 16)
        Me.lblUsuarioEnSesion.Text = "Usuario:"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Gainsboro
        Me.PictureBox1.Location = New System.Drawing.Point(0, 57)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(240, 205)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'cmbSalir
        '
        Me.cmbSalir.Image = CType(resources.GetObject("cmbSalir.Image"), System.Drawing.Image)
        Me.cmbSalir.Location = New System.Drawing.Point(0, 0)
        Me.cmbSalir.Name = "cmbSalir"
        Me.cmbSalir.Size = New System.Drawing.Size(41, 38)
        Me.cmbSalir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'picBarraMorada
        '
        Me.picBarraMorada.Image = CType(resources.GetObject("picBarraMorada.Image"), System.Drawing.Image)
        Me.picBarraMorada.Location = New System.Drawing.Point(0, 260)
        Me.picBarraMorada.Name = "picBarraMorada"
        Me.picBarraMorada.Size = New System.Drawing.Size(240, 40)
        Me.picBarraMorada.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'PictureFondoINVENTARIO_ELAB_INV
        '
        Me.PictureFondoINVENTARIO_ELAB_INV.Image = CType(resources.GetObject("PictureFondoINVENTARIO_ELAB_INV.Image"), System.Drawing.Image)
        Me.PictureFondoINVENTARIO_ELAB_INV.Location = New System.Drawing.Point(42, 0)
        Me.PictureFondoINVENTARIO_ELAB_INV.Name = "PictureFondoINVENTARIO_ELAB_INV"
        Me.PictureFondoINVENTARIO_ELAB_INV.Size = New System.Drawing.Size(198, 38)
        Me.PictureFondoINVENTARIO_ELAB_INV.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'cmbFinalizar
        '
        Me.cmbFinalizar.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.cmbFinalizar.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.cmbFinalizar.ForeColor = System.Drawing.Color.Black
        Me.cmbFinalizar.Location = New System.Drawing.Point(16, 268)
        Me.cmbFinalizar.Name = "cmbFinalizar"
        Me.cmbFinalizar.Size = New System.Drawing.Size(78, 25)
        Me.cmbFinalizar.TabIndex = 95
        Me.cmbFinalizar.Text = "Finalizar"
        '
        'cmbActualizar
        '
        Me.cmbActualizar.Image = CType(resources.GetObject("cmbActualizar.Image"), System.Drawing.Image)
        Me.cmbActualizar.Location = New System.Drawing.Point(108, 268)
        Me.cmbActualizar.Name = "cmbActualizar"
        Me.cmbActualizar.Size = New System.Drawing.Size(24, 24)
        Me.cmbActualizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'chkQuickScan
        '
        Me.chkQuickScan.BackColor = System.Drawing.Color.Gainsboro
        Me.chkQuickScan.Font = New System.Drawing.Font("Tahoma", 6.0!, System.Drawing.FontStyle.Regular)
        Me.chkQuickScan.Location = New System.Drawing.Point(118, 60)
        Me.chkQuickScan.Name = "chkQuickScan"
        Me.chkQuickScan.Size = New System.Drawing.Size(65, 23)
        Me.chkQuickScan.TabIndex = 102
        Me.chkQuickScan.Text = "MultiScan"
        '
        'ChkRevisar
        '
        Me.ChkRevisar.BackColor = System.Drawing.Color.Gainsboro
        Me.ChkRevisar.Font = New System.Drawing.Font("Tahoma", 6.0!, System.Drawing.FontStyle.Regular)
        Me.ChkRevisar.Location = New System.Drawing.Point(179, 60)
        Me.ChkRevisar.Name = "ChkRevisar"
        Me.ChkRevisar.Size = New System.Drawing.Size(57, 23)
        Me.ChkRevisar.TabIndex = 103
        Me.ChkRevisar.Text = "Consulta"
        '
        'FormElab_Inv_InventarioFisico
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(240, 300)
        Me.ControlBox = False
        Me.Controls.Add(Me.ChkRevisar)
        Me.Controls.Add(Me.chkQuickScan)
        Me.Controls.Add(Me.cmbActualizar)
        Me.Controls.Add(Me.txtCodigoProducto)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.txtEmpaque)
        Me.Controls.Add(Me.lblEmpaque)
        Me.Controls.Add(Me.lblUnidad)
        Me.Controls.Add(Me.lblNumDeposito)
        Me.Controls.Add(Me.lblNumDoc)
        Me.Controls.Add(Me.lblDeposito)
        Me.Controls.Add(Me.lblNºDoc)
        Me.Controls.Add(Me.txtUnidad)
        Me.Controls.Add(Me.cmbAgregar)
        Me.Controls.Add(Me.lblCantidadProducto)
        Me.Controls.Add(Me.lblCodigoProducto)
        Me.Controls.Add(Me.lblUsuarioEnSesion)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.cmbSalir)
        Me.Controls.Add(Me.PictureFondoINVENTARIO_ELAB_INV)
        Me.Controls.Add(Me.cmbFinalizar)
        Me.Controls.Add(Me.picBarraMorada)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormElab_Inv_InventarioFisico"
        Me.Text = "isMOBILE - Inventario"
        Me.ResumeLayout(False)

    End Sub

    Public Sub New()

        ' Llamada necesaria para el Diseñador de Windows Forms.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub
    Friend WithEvents txtCodigoProducto As System.Windows.Forms.TextBox
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents txtEmpaque As System.Windows.Forms.TextBox
    Friend WithEvents lblEmpaque As System.Windows.Forms.Label
    Friend WithEvents lblUnidad As System.Windows.Forms.Label
    Friend WithEvents lblNumDeposito As System.Windows.Forms.Label
    Friend WithEvents lblNumDoc As System.Windows.Forms.Label
    Friend WithEvents lblDeposito As System.Windows.Forms.Label
    Friend WithEvents lblNºDoc As System.Windows.Forms.Label
    Friend WithEvents txtUnidad As System.Windows.Forms.TextBox
    Friend WithEvents cmbAgregar As System.Windows.Forms.Button
    Friend WithEvents lblCantidadProducto As System.Windows.Forms.Label
    Friend WithEvents lblCodigoProducto As System.Windows.Forms.Label
    Friend WithEvents lblUsuarioEnSesion As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents cmbSalir As System.Windows.Forms.PictureBox
    Friend WithEvents picBarraMorada As System.Windows.Forms.PictureBox
    Friend WithEvents PictureFondoINVENTARIO_ELAB_INV As System.Windows.Forms.PictureBox
    Friend WithEvents cmbFinalizar As System.Windows.Forms.Button
    Friend WithEvents cmbActualizar As System.Windows.Forms.PictureBox
    Friend WithEvents chkQuickScan As System.Windows.Forms.CheckBox
    Friend WithEvents ChkRevisar As System.Windows.Forms.CheckBox
End Class
