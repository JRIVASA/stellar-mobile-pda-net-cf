﻿Public Class FormBuscarDeposito

    Public IsDialog As Boolean
    Public DialogProp_Code As String
    Public DialogProp_Desc As String
    Public Dialog_Selected As Boolean
    Public FormaCargada As Boolean

    Private Sub DataGridBuscarDeposito_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridBuscarDeposito.DoubleClick
        Seleccionar()
    End Sub

    Private Sub Seleccionar()

        Dim filaDataGrid = DataGridBuscarDeposito.CurrentRowIndex

        If filaDataGrid = -1 Then
            MsgBox("No seleccionó ningún deposito.", MsgBoxStyle.Information, "isMOBILE")
            Exit Sub
        End If

        FormaCargada = False

        If IsDialog Then
            Dialog_Selected = True
            IsDialog = False
            DialogProp_Code = DataGridBuscarDeposito.Item(filaDataGrid, 0)
            DialogProp_Desc = DataGridBuscarDeposito.Item(filaDataGrid, 1)
            txtDescripcionDeposito.Text = ""
            DataGridBuscarDeposito.DataSource = Nothing
            Me.DialogResult = Forms.DialogResult.OK
            Me.Hide()
            Exit Sub
        Else
            seleccionarDepositoDataGrid()
        End If

    End Sub

    Private Sub txtDescripcionDeposito_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDescripcionDeposito.KeyDown
        If e.KeyValue = 13 Then
            'If txtDescripcionDeposito.Text = String.Empty Then
            'MsgBox("Debe colocar en Descripción al menos un carácter", MsgBoxStyle.Information, "isMOBILE")
            'txtDescripcionDeposito.Focus()
            'Else
            llenaDataGridBuscarDeposito(txtDescripcionDeposito.Text)
            'End If
        End If
    End Sub

    Private Sub cmbBuscarDeposito_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbBuscarDeposito.Click
        'If txtDescripcionDeposito.Text = "" Then
        'MsgBox("Debe colocar al menos un carácter en Descripción", MsgBoxStyle.Information, "isMOBILE")
        'txtDescripcionDeposito.Focus()
        'Else
        llenaDataGridBuscarDeposito(txtDescripcionDeposito.Text)
        'End If
    End Sub

    Private Sub cmbSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSalir.Click

        txtDescripcionDeposito.Text = ""
        DataGridBuscarDeposito.DataSource = Nothing
        FormaCargada = False

        If IsDialog Then
            Dialog_Selected = False
            IsDialog = False
            Me.DialogResult = Forms.DialogResult.OK
            Me.Hide()
            Exit Sub
        End If

        If Me.Owner.Equals(FormProveedorDeposito_Recepcion) Then
            'Me.Hide()
            Me.Close()
            FormProveedorDeposito_Recepcion.Show()
            FormDepositoOrigenDestino_Traslado.Visible = True
            FormProveedorDeposito_Recepcion.txtDeposito.Focus()
        Else
            If Me.Owner.Equals(FormDepositoOrigenDestino_Traslado) Then
                If Deposito = "origen" Then
                    FormDepositoOrigenDestino_Traslado.txtDepositoOrigen.Focus()
                ElseIf Deposito = "destino" Then
                    FormDepositoOrigenDestino_Traslado.txtDepositoDestino.Focus()
                End If
                'Me.Hide()
                Me.Close()
                FormDepositoOrigenDestino_Traslado.Show()
                FormDepositoOrigenDestino_Traslado.Visible = True
            End If
        End If

    End Sub

    Private Sub cmbAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbAceptar.Click
        Seleccionar()
    End Sub

    Private Sub FormBuscarDeposito_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Activated

        If Not FormaCargada Then

            FormaCargada = True

            TimerReload.Interval = 100
            TimerReload.Enabled = True

        End If

    End Sub

    Private Sub FormBuscarDeposito_Recepcion_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not PantallaAjustada_FrmBuscarDeposito Then
            AjustarFormularios(Me)
            PantallaAjustada_FrmBuscarDeposito = True
        End If

    End Sub

    Private Sub Activation()

        lblUsuarioEnSesion.Text = "Usuario: " + descripcionUsuario

        Me.Refresh()
        Me.Update()

        If Not IsDialog Then
            llenaDataGridBuscarDeposito(txtDescripcionDeposito.Text)
        End If

    End Sub

    Private Sub TimerReload_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TimerReload.Tick
        TimerReload.Enabled = False
        Activation()
    End Sub

End Class