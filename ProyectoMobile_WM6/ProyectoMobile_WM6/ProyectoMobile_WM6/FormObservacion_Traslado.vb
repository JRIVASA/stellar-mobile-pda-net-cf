﻿Public Class FormObservacion_Traslado

    Private Sub FormObservacion_Traslado_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Activated

        TimerReload.Interval = 100
        TimerReload.Enabled = True

    End Sub

    Private Sub FormObservacion_Traslado_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblUsuarioEnSesion.Text = "Usuario: " + descripcionUsuario
        Me.Refresh()
        Me.Update()
        txtObservacion.Text = "Desde el módulo de Traslado móvil " + lblNºTrs.Text + "."
        AjustarFormularios(Me)
    End Sub

    Private Sub cmbAceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbAceptar.Click

        If conectarDB_SERVIDOR(rutaDB_SERVIDOR) = 1 Then
            Call finalizarTraslado() 'Guarda el traslado que se acaba de hacer en la tabla MA_INVENTARIO del servidor
            cerrarConexion_SERVIDOR()
        Else
            'Guardar en base de datos local
            'guardarTraslado() 'Guarda en la tabla MA_TRASLADOS_PDA, para luego ser enviado cuando haya conexión con el servidor
            MsgBox("No hay conexión con el servidor. No se puede realizar el traslado.", MsgBoxStyle.Exclamation, "isMOBILE")

            limpiarFormLecturaProducto_Traslado()
            FormLecturaProducto_Traslado.Cancelando_O_Entrando = True
            Me.txtObservacion.Text = ""
            Me.lblNºTrs.Text = ""
            formMenuPrincipal.Show()
            Me.Visible = False
        End If

    End Sub

    Private Sub cmbSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSalir.Click
        respuesta = MsgBox("¿Está seguro que desea regresar? Si lo hace el traslado actual será cancelado.", MsgBoxStyle.YesNo, "Finalizar Traslado")

        If respuesta = 6 Then
            FormLecturaProducto_Traslado.Cancelando_O_Entrando = True 'comandBD_PDA.CommandText = "DELETE TR_TRASLADOS_PDA" : ejecutaQuery_PDA(comandBD_PDA)
            limpiarFormLecturaProducto_Traslado()
            Me.txtObservacion.Text = ""
            Me.lblNºTrs.Text = ""
            formMenuPrincipal.Show()
            Me.Visible = False
        End If
    End Sub

    Private Sub Activation()

        lblUsuarioEnSesion.Text = "Usuario: " + descripcionUsuario

        txtObservacion.Text = "Desde el módulo de Traslado móvil " + lblNºTrs.Text + "."

        Me.Refresh()
        Me.Update()

        txtObservacion.Focus()

    End Sub

    Private Sub TimerReload_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TimerReload.Tick
        TimerReload.Enabled = False
        Activation()
    End Sub

End Class