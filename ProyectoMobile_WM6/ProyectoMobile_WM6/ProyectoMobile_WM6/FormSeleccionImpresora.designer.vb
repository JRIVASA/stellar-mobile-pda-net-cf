﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class FormSeleccionImpresora
    Inherits System.Windows.Forms.Form

    'Form invalida a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer
    Private mainMenu1 As System.Windows.Forms.MainMenu

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar con el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormSeleccionImpresora))
        Me.mainMenu1 = New System.Windows.Forms.MainMenu
        Me.CmbBluetooth = New System.Windows.Forms.PictureBox
        Me.PanelMenu_Principal = New System.Windows.Forms.Panel
        Me.lblUsuarioEnSesion = New System.Windows.Forms.Label
        Me.cmbSalir = New System.Windows.Forms.PictureBox
        Me.CmbWLAN = New System.Windows.Forms.PictureBox
        Me.PictureFondoMenuInventarioFisico = New System.Windows.Forms.PictureBox
        Me.PanelWLANSelected = New System.Windows.Forms.Panel
        Me.PanelBluetoothSelected = New System.Windows.Forms.Panel
        Me.TimerReload = New System.Windows.Forms.Timer
        Me.PanelMenu_Principal.SuspendLayout()
        Me.SuspendLayout()
        '
        'CmbBluetooth
        '
        Me.CmbBluetooth.Image = CType(resources.GetObject("CmbBluetooth.Image"), System.Drawing.Image)
        Me.CmbBluetooth.Location = New System.Drawing.Point(68, 183)
        Me.CmbBluetooth.Name = "CmbBluetooth"
        Me.CmbBluetooth.Size = New System.Drawing.Size(96, 96)
        Me.CmbBluetooth.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'PanelMenu_Principal
        '
        Me.PanelMenu_Principal.BackColor = System.Drawing.Color.Gainsboro
        Me.PanelMenu_Principal.Controls.Add(Me.lblUsuarioEnSesion)
        Me.PanelMenu_Principal.Controls.Add(Me.cmbSalir)
        Me.PanelMenu_Principal.Controls.Add(Me.CmbBluetooth)
        Me.PanelMenu_Principal.Controls.Add(Me.CmbWLAN)
        Me.PanelMenu_Principal.Controls.Add(Me.PictureFondoMenuInventarioFisico)
        Me.PanelMenu_Principal.Controls.Add(Me.PanelWLANSelected)
        Me.PanelMenu_Principal.Controls.Add(Me.PanelBluetoothSelected)
        Me.PanelMenu_Principal.Location = New System.Drawing.Point(0, 0)
        Me.PanelMenu_Principal.Name = "PanelMenu_Principal"
        Me.PanelMenu_Principal.Size = New System.Drawing.Size(240, 300)
        '
        'lblUsuarioEnSesion
        '
        Me.lblUsuarioEnSesion.BackColor = System.Drawing.Color.DarkGray
        Me.lblUsuarioEnSesion.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblUsuarioEnSesion.Location = New System.Drawing.Point(0, 39)
        Me.lblUsuarioEnSesion.Name = "lblUsuarioEnSesion"
        Me.lblUsuarioEnSesion.Size = New System.Drawing.Size(240, 15)
        Me.lblUsuarioEnSesion.Text = "Usuario:"
        '
        'cmbSalir
        '
        Me.cmbSalir.Image = CType(resources.GetObject("cmbSalir.Image"), System.Drawing.Image)
        Me.cmbSalir.Location = New System.Drawing.Point(0, 0)
        Me.cmbSalir.Name = "cmbSalir"
        Me.cmbSalir.Size = New System.Drawing.Size(41, 38)
        Me.cmbSalir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'CmbWLAN
        '
        Me.CmbWLAN.Image = CType(resources.GetObject("CmbWLAN.Image"), System.Drawing.Image)
        Me.CmbWLAN.Location = New System.Drawing.Point(68, 72)
        Me.CmbWLAN.Name = "CmbWLAN"
        Me.CmbWLAN.Size = New System.Drawing.Size(96, 96)
        Me.CmbWLAN.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'PictureFondoMenuInventarioFisico
        '
        Me.PictureFondoMenuInventarioFisico.Image = CType(resources.GetObject("PictureFondoMenuInventarioFisico.Image"), System.Drawing.Image)
        Me.PictureFondoMenuInventarioFisico.Location = New System.Drawing.Point(42, 0)
        Me.PictureFondoMenuInventarioFisico.Name = "PictureFondoMenuInventarioFisico"
        Me.PictureFondoMenuInventarioFisico.Size = New System.Drawing.Size(198, 38)
        Me.PictureFondoMenuInventarioFisico.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'PanelWLANSelected
        '
        Me.PanelWLANSelected.BackColor = System.Drawing.Color.CornflowerBlue
        Me.PanelWLANSelected.Location = New System.Drawing.Point(66, 70)
        Me.PanelWLANSelected.Name = "PanelWLANSelected"
        Me.PanelWLANSelected.Size = New System.Drawing.Size(100, 100)
        '
        'PanelBluetoothSelected
        '
        Me.PanelBluetoothSelected.BackColor = System.Drawing.Color.CornflowerBlue
        Me.PanelBluetoothSelected.Location = New System.Drawing.Point(66, 181)
        Me.PanelBluetoothSelected.Name = "PanelBluetoothSelected"
        Me.PanelBluetoothSelected.Size = New System.Drawing.Size(100, 100)
        '
        'TimerReload
        '
        Me.TimerReload.Interval = 1250
        '
        'FormSeleccionImpresora
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(240, 300)
        Me.ControlBox = False
        Me.Controls.Add(Me.PanelMenu_Principal)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormSeleccionImpresora"
        Me.Text = "isMOBILE - Inventario Físico"
        Me.PanelMenu_Principal.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents CmbBluetooth As System.Windows.Forms.PictureBox
    Friend WithEvents PanelMenu_Principal As System.Windows.Forms.Panel
    Friend WithEvents CmbWLAN As System.Windows.Forms.PictureBox
    Friend WithEvents PictureFondoMenuInventarioFisico As System.Windows.Forms.PictureBox
    Friend WithEvents cmbSalir As System.Windows.Forms.PictureBox
    Friend WithEvents lblUsuarioEnSesion As System.Windows.Forms.Label
    Friend WithEvents PanelWLANSelected As System.Windows.Forms.Panel
    Friend WithEvents PanelBluetoothSelected As System.Windows.Forms.Panel
    Friend WithEvents TimerReload As System.Windows.Forms.Timer
End Class
