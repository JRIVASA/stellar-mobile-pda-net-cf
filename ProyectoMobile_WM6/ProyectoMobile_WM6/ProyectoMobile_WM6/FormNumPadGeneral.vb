﻿Public Class FormNumPadGeneral

    Public ModoAlfanumerico As Boolean
    Public ValorNumerico As Double
    Public ValorAlfanumerico As String
    Public ValorOriginal As Object
    Public CantDec As Integer

    Public FormaCargada As Boolean

    Private EventoProgramado As Boolean

    Private Sub cmbSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSalir.Click
        Me.DialogResult = Forms.DialogResult.OK
        Me.Hide()
    End Sub

    Private Sub CmdBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdBack.Click

        Dim PosAct As Integer

        PosAct = txtNumero.SelectionStart

        If txtNumero.SelectionLength > 0 Then
            txtNumero.Text = String.Empty
        Else
            If txtNumero.SelectionStart > 0 Then
                EventoProgramado = True
                txtNumero.Text = Mid(txtNumero.Text, 1, txtNumero.SelectionStart - 1) & Mid(txtNumero.Text, txtNumero.SelectionStart + 1)
                If txtNumero.Text = String.Empty Then
                    txtNumero_TextChanged(txtNumero, Nothing)
                Else
                    PosAct = PosAct - 1
                    txtNumero.SelectionStart = PosAct
                End If
            End If
        End If

    End Sub

    Private Sub txtNumero_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNumero.GotFocus
        SeleccionarTexto(txtNumero)
    End Sub

    Private Sub txtNumero_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNumero.KeyPress
        If Val(txtNumero.Text) < 0 Then
            e.Handled = True
            txtNumero.Text = String.Empty
        End If
        If e.KeyChar = Chr(Keys.Enter) Then
            CmdEnter_Click(Nothing, Nothing)
        Else
            If txtNumero.SelectionLength = txtNumero.Text.Length Then
                EventoProgramado = True
            End If
        End If
    End Sub

    Private Sub txtNumero_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNumero.TextChanged
        If Not EventoProgramado Then
            If IsNumeric(txtNumero.Text) Then
                If CDbl(txtNumero.Text) < 0 Then
                    txtNumero.Text = "0" & CmdDecimal.Text & "00"
                    txtNumero_GotFocus(Nothing, Nothing)
                End If
            Else
                txtNumero.Text = "0" & CmdDecimal.Text & "00"
                txtNumero_GotFocus(Nothing, Nothing)
            End If
        Else
            EventoProgramado = False ' Ignorar
        End If
    End Sub

    Private Sub CmdClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdClear.Click
        txtNumero.Text = String.Empty
    End Sub

    Private Sub CmdDecimal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdDecimal.Click

        Dim PosAct As Integer, CarDecimal As String

        PosAct = txtNumero.SelectionStart
        CarDecimal = NumberFormatInfo.CurrentInfo.CurrencyDecimalSeparator 'CmdDecimal.Text

        'If Decimales = 0 Then Exit Sub

        If InStr(1, txtNumero.Text, CmdDecimal.Text) <= 0 Then
            txtNumero.Text = Mid(txtNumero.Text, 1, PosAct) & CarDecimal & Mid(txtNumero.Text, PosAct + 2)
            txtNumero.SelectionStart = PosAct + 1
        End If

    End Sub

    Private Sub Numeros_Click(ByVal Index As Integer)

        Dim PosAct As Integer

        If txtNumero.SelectionLength = Len(txtNumero.Text) Then
            EventoProgramado = True
            txtNumero.Text = String.Empty
        End If

        PosAct = txtNumero.SelectionStart

        txtNumero.Text = Mid(txtNumero.Text, 1, PosAct) & Index & Mid(txtNumero.Text, PosAct + 1)
        txtNumero.SelectionStart = PosAct + 1

    End Sub

    Private Sub CmdEnter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdEnter.Click

        On Error Resume Next

        If Not ModoAlfanumerico And Trim(txtNumero.Text) = String.Empty Then Exit Sub

        ValorAlfanumerico = Trim(txtNumero.Text)

        If IsNumeric(ValorAlfanumerico) Then
            ValorNumerico = CDbl(ValorAlfanumerico)
        Else
            ValorNumerico = Val(ValorAlfanumerico)
        End If

        cmbSalir_Click(Nothing, Nothing)

    End Sub

    Private Sub CmdNumero0_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdNumero0.Click
        Numeros_Click(0)
    End Sub

    Private Sub CmdNumero1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdNumero1.Click
        Numeros_Click(1)
    End Sub

    Private Sub CmdNumero2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdNumero2.Click
        Numeros_Click(2)
    End Sub

    Private Sub CmdNumero3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdNumero3.Click
        Numeros_Click(3)
    End Sub

    Private Sub CmdNumero4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdNumero4.Click
        Numeros_Click(4)
    End Sub

    Private Sub CmdNumero5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdNumero5.Click
        Numeros_Click(5)
    End Sub

    Private Sub CmdNumero6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdNumero6.Click
        Numeros_Click(6)
    End Sub

    Private Sub CmdNumero7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdNumero7.Click
        Numeros_Click(7)
    End Sub

    Private Sub CmdNumero8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdNumero8.Click
        Numeros_Click(8)
    End Sub

    Private Sub CmdNumero9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdNumero9.Click
        Numeros_Click(9)
    End Sub

    Private Sub FormNumPadGeneral_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        If ValorOriginal Is Nothing Then ValorOriginal = String.Empty
        txtNumero.Text = ValorOriginal
        SeleccionarTexto(txtNumero)
    End Sub

    Private Sub FormNumPadGeneral_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not FormaCargada Then
            FormaCargada = True
            AjustarFormularios(Me)
        End If
        Me.CmdDecimal.Text = NumberFormatInfo.CurrentInfo.CurrencyDecimalSeparator
    End Sub
End Class