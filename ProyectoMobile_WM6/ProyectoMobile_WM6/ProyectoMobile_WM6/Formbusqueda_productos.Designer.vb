﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class Formbusqueda_productos
    Inherits System.Windows.Forms.Form

    'Form invalida a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar con el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Formbusqueda_productos))
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.cmbSalir = New System.Windows.Forms.PictureBox
        Me.lblUsuarioEnSesion = New System.Windows.Forms.Label
        Me.picBarraMorada = New System.Windows.Forms.PictureBox
        Me.PictureBox2 = New System.Windows.Forms.PictureBox
        Me.DataGridbusqueda_productos = New System.Windows.Forms.DataGrid
        Me.txtproductos = New System.Windows.Forms.TextBox
        Me.cmbBuscarProducto = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.cmbAceptar = New System.Windows.Forms.Button
        Me.TimerReload = New System.Windows.Forms.Timer
        Me.SuspendLayout()
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(42, 0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(198, 38)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'cmbSalir
        '
        Me.cmbSalir.Image = CType(resources.GetObject("cmbSalir.Image"), System.Drawing.Image)
        Me.cmbSalir.Location = New System.Drawing.Point(0, 0)
        Me.cmbSalir.Name = "cmbSalir"
        Me.cmbSalir.Size = New System.Drawing.Size(41, 38)
        Me.cmbSalir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'lblUsuarioEnSesion
        '
        Me.lblUsuarioEnSesion.BackColor = System.Drawing.Color.DarkGray
        Me.lblUsuarioEnSesion.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblUsuarioEnSesion.Location = New System.Drawing.Point(0, 39)
        Me.lblUsuarioEnSesion.Name = "lblUsuarioEnSesion"
        Me.lblUsuarioEnSesion.Size = New System.Drawing.Size(240, 15)
        Me.lblUsuarioEnSesion.Text = "Usuario:"
        '
        'picBarraMorada
        '
        Me.picBarraMorada.Image = CType(resources.GetObject("picBarraMorada.Image"), System.Drawing.Image)
        Me.picBarraMorada.Location = New System.Drawing.Point(0, 260)
        Me.picBarraMorada.Name = "picBarraMorada"
        Me.picBarraMorada.Size = New System.Drawing.Size(240, 40)
        Me.picBarraMorada.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.Gainsboro
        Me.PictureBox2.Location = New System.Drawing.Point(0, 54)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(240, 205)
        '
        'DataGridbusqueda_productos
        '
        Me.DataGridbusqueda_productos.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.DataGridbusqueda_productos.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold)
        Me.DataGridbusqueda_productos.Location = New System.Drawing.Point(9, 119)
        Me.DataGridbusqueda_productos.Name = "DataGridbusqueda_productos"
        Me.DataGridbusqueda_productos.Size = New System.Drawing.Size(220, 134)
        Me.DataGridbusqueda_productos.TabIndex = 117
        '
        'txtproductos
        '
        Me.txtproductos.Font = New System.Drawing.Font("Tahoma", 14.0!, System.Drawing.FontStyle.Regular)
        Me.txtproductos.Location = New System.Drawing.Point(9, 84)
        Me.txtproductos.Name = "txtproductos"
        Me.txtproductos.Size = New System.Drawing.Size(135, 29)
        Me.txtproductos.TabIndex = 118
        '
        'cmbBuscarProducto
        '
        Me.cmbBuscarProducto.BackColor = System.Drawing.Color.White
        Me.cmbBuscarProducto.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.cmbBuscarProducto.ForeColor = System.Drawing.Color.Black
        Me.cmbBuscarProducto.Location = New System.Drawing.Point(151, 84)
        Me.cmbBuscarProducto.Name = "cmbBuscarProducto"
        Me.cmbBuscarProducto.Size = New System.Drawing.Size(78, 29)
        Me.cmbBuscarProducto.TabIndex = 119
        Me.cmbBuscarProducto.Text = "Buscar"
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Gainsboro
        Me.Label1.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold)
        Me.Label1.Location = New System.Drawing.Point(3, 63)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(234, 13)
        Me.Label1.Text = "Búsqueda de Productos"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'cmbAceptar
        '
        Me.cmbAceptar.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.cmbAceptar.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.cmbAceptar.ForeColor = System.Drawing.Color.Black
        Me.cmbAceptar.Location = New System.Drawing.Point(78, 265)
        Me.cmbAceptar.Name = "cmbAceptar"
        Me.cmbAceptar.Size = New System.Drawing.Size(78, 25)
        Me.cmbAceptar.TabIndex = 125
        Me.cmbAceptar.Text = "Aceptar"
        '
        'TimerReload
        '
        Me.TimerReload.Interval = 1250
        '
        'Formbusqueda_productos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.ClientSize = New System.Drawing.Size(240, 300)
        Me.ControlBox = False
        Me.Controls.Add(Me.cmbAceptar)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cmbBuscarProducto)
        Me.Controls.Add(Me.txtproductos)
        Me.Controls.Add(Me.DataGridbusqueda_productos)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.picBarraMorada)
        Me.Controls.Add(Me.lblUsuarioEnSesion)
        Me.Controls.Add(Me.cmbSalir)
        Me.Controls.Add(Me.PictureBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Formbusqueda_productos"
        Me.Text = "isMOBILE - Búsqueda de Productos"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents cmbSalir As System.Windows.Forms.PictureBox
    Friend WithEvents lblUsuarioEnSesion As System.Windows.Forms.Label
    Friend WithEvents picBarraMorada As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents DataGridbusqueda_productos As System.Windows.Forms.DataGrid
    Friend WithEvents txtproductos As System.Windows.Forms.TextBox
    Friend WithEvents cmbBuscarProducto As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmbAceptar As System.Windows.Forms.Button
    Friend WithEvents TimerReload As System.Windows.Forms.Timer
End Class
