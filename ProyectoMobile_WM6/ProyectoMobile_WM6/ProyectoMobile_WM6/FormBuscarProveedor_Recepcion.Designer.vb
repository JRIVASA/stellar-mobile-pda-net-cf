﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class FormBuscarProveedor_Recepcion
    Inherits System.Windows.Forms.Form

    'Form invalida a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer
    Private mainMenu1 As System.Windows.Forms.MainMenu

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar con el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormBuscarProveedor_Recepcion))
        Me.mainMenu1 = New System.Windows.Forms.MainMenu
        Me.lblUsuarioEnSesion = New System.Windows.Forms.Label
        Me.lblDescripcionProveedor = New System.Windows.Forms.Label
        Me.cmbBuscarProveedor = New System.Windows.Forms.Button
        Me.txtDescripcionProveedor = New System.Windows.Forms.TextBox
        Me.DataGridBuscarProveedor = New System.Windows.Forms.DataGrid
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.cmbAceptar = New System.Windows.Forms.Button
        Me.picBarraMorada = New System.Windows.Forms.PictureBox
        Me.PictureFondoProveedorDeposito_Recepcion = New System.Windows.Forms.PictureBox
        Me.cmbSalir = New System.Windows.Forms.PictureBox
        Me.TimerReload = New System.Windows.Forms.Timer
        Me.SuspendLayout()
        '
        'lblUsuarioEnSesion
        '
        Me.lblUsuarioEnSesion.BackColor = System.Drawing.Color.DarkGray
        Me.lblUsuarioEnSesion.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblUsuarioEnSesion.Location = New System.Drawing.Point(0, 39)
        Me.lblUsuarioEnSesion.Name = "lblUsuarioEnSesion"
        Me.lblUsuarioEnSesion.Size = New System.Drawing.Size(240, 15)
        Me.lblUsuarioEnSesion.Text = "Usuario:"
        '
        'lblDescripcionProveedor
        '
        Me.lblDescripcionProveedor.BackColor = System.Drawing.Color.Gainsboro
        Me.lblDescripcionProveedor.Font = New System.Drawing.Font("Verdana", 14.0!, System.Drawing.FontStyle.Bold)
        Me.lblDescripcionProveedor.Location = New System.Drawing.Point(9, 58)
        Me.lblDescripcionProveedor.Name = "lblDescripcionProveedor"
        Me.lblDescripcionProveedor.Size = New System.Drawing.Size(220, 25)
        Me.lblDescripcionProveedor.Text = "Proveedor"
        Me.lblDescripcionProveedor.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'cmbBuscarProveedor
        '
        Me.cmbBuscarProveedor.BackColor = System.Drawing.Color.White
        Me.cmbBuscarProveedor.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.cmbBuscarProveedor.ForeColor = System.Drawing.Color.Black
        Me.cmbBuscarProveedor.Location = New System.Drawing.Point(151, 84)
        Me.cmbBuscarProveedor.Name = "cmbBuscarProveedor"
        Me.cmbBuscarProveedor.Size = New System.Drawing.Size(78, 29)
        Me.cmbBuscarProveedor.TabIndex = 125
        Me.cmbBuscarProveedor.Text = "Buscar"
        '
        'txtDescripcionProveedor
        '
        Me.txtDescripcionProveedor.Font = New System.Drawing.Font("Verdana", 14.0!, System.Drawing.FontStyle.Bold)
        Me.txtDescripcionProveedor.Location = New System.Drawing.Point(9, 84)
        Me.txtDescripcionProveedor.MaxLength = 50
        Me.txtDescripcionProveedor.Name = "txtDescripcionProveedor"
        Me.txtDescripcionProveedor.Size = New System.Drawing.Size(135, 29)
        Me.txtDescripcionProveedor.TabIndex = 124
        '
        'DataGridBuscarProveedor
        '
        Me.DataGridBuscarProveedor.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.DataGridBuscarProveedor.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold)
        Me.DataGridBuscarProveedor.Location = New System.Drawing.Point(9, 119)
        Me.DataGridBuscarProveedor.Name = "DataGridBuscarProveedor"
        Me.DataGridBuscarProveedor.Size = New System.Drawing.Size(220, 134)
        Me.DataGridBuscarProveedor.TabIndex = 127
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Gainsboro
        Me.PictureBox1.Location = New System.Drawing.Point(0, 54)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(240, 205)
        '
        'cmbAceptar
        '
        Me.cmbAceptar.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.cmbAceptar.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.cmbAceptar.ForeColor = System.Drawing.Color.Black
        Me.cmbAceptar.Location = New System.Drawing.Point(78, 265)
        Me.cmbAceptar.Name = "cmbAceptar"
        Me.cmbAceptar.Size = New System.Drawing.Size(78, 25)
        Me.cmbAceptar.TabIndex = 126
        Me.cmbAceptar.Text = "Aceptar"
        '
        'picBarraMorada
        '
        Me.picBarraMorada.Image = CType(resources.GetObject("picBarraMorada.Image"), System.Drawing.Image)
        Me.picBarraMorada.Location = New System.Drawing.Point(0, 260)
        Me.picBarraMorada.Name = "picBarraMorada"
        Me.picBarraMorada.Size = New System.Drawing.Size(240, 40)
        Me.picBarraMorada.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'PictureFondoProveedorDeposito_Recepcion
        '
        Me.PictureFondoProveedorDeposito_Recepcion.Image = CType(resources.GetObject("PictureFondoProveedorDeposito_Recepcion.Image"), System.Drawing.Image)
        Me.PictureFondoProveedorDeposito_Recepcion.Location = New System.Drawing.Point(42, 0)
        Me.PictureFondoProveedorDeposito_Recepcion.Name = "PictureFondoProveedorDeposito_Recepcion"
        Me.PictureFondoProveedorDeposito_Recepcion.Size = New System.Drawing.Size(198, 38)
        Me.PictureFondoProveedorDeposito_Recepcion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'cmbSalir
        '
        Me.cmbSalir.Image = CType(resources.GetObject("cmbSalir.Image"), System.Drawing.Image)
        Me.cmbSalir.Location = New System.Drawing.Point(0, 0)
        Me.cmbSalir.Name = "cmbSalir"
        Me.cmbSalir.Size = New System.Drawing.Size(41, 38)
        Me.cmbSalir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'TimerReload
        '
        Me.TimerReload.Interval = 1250
        '
        'FormBuscarProveedor_Recepcion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(240, 300)
        Me.ControlBox = False
        Me.Controls.Add(Me.cmbSalir)
        Me.Controls.Add(Me.PictureFondoProveedorDeposito_Recepcion)
        Me.Controls.Add(Me.lblUsuarioEnSesion)
        Me.Controls.Add(Me.lblDescripcionProveedor)
        Me.Controls.Add(Me.cmbBuscarProveedor)
        Me.Controls.Add(Me.txtDescripcionProveedor)
        Me.Controls.Add(Me.DataGridBuscarProveedor)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.cmbAceptar)
        Me.Controls.Add(Me.picBarraMorada)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MinimizeBox = False
        Me.Name = "FormBuscarProveedor_Recepcion"
        Me.Text = "isMOBILE - Recepción"
        Me.TopMost = True
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblUsuarioEnSesion As System.Windows.Forms.Label
    Friend WithEvents lblDescripcionProveedor As System.Windows.Forms.Label
    Friend WithEvents cmbBuscarProveedor As System.Windows.Forms.Button
    Friend WithEvents txtDescripcionProveedor As System.Windows.Forms.TextBox
    Friend WithEvents DataGridBuscarProveedor As System.Windows.Forms.DataGrid
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents cmbAceptar As System.Windows.Forms.Button
    Friend WithEvents picBarraMorada As System.Windows.Forms.PictureBox
    Friend WithEvents PictureFondoProveedorDeposito_Recepcion As System.Windows.Forms.PictureBox
    Friend WithEvents cmbSalir As System.Windows.Forms.PictureBox
    Friend WithEvents TimerReload As System.Windows.Forms.Timer
End Class
