﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class FormCantidadRecibida_Recepcion
    Inherits System.Windows.Forms.Form

    'Form invalida a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer
    Private mainMenu1 As System.Windows.Forms.MainMenu

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar con el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormCantidadRecibida_Recepcion))
        Me.mainMenu1 = New System.Windows.Forms.MainMenu
        Me.PictureFondoBuscarProveedor = New System.Windows.Forms.PictureBox
        Me.txtEmpaquesRecibidos = New System.Windows.Forms.TextBox
        Me.lblEmpaquesRecibidos = New System.Windows.Forms.Label
        Me.lblUnidadesRecibidas = New System.Windows.Forms.Label
        Me.txtUnidadesRecibidas = New System.Windows.Forms.TextBox
        Me.lblUnidadesSolicitadas = New System.Windows.Forms.Label
        Me.lblCantidadRecibida = New System.Windows.Forms.Label
        Me.lblUnidadesxEmpaque = New System.Windows.Forms.Label
        Me.lblFechaRecepcion = New System.Windows.Forms.Label
        Me.lblNºRec = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.lblRec = New System.Windows.Forms.Label
        Me.lblCantidadFaltante = New System.Windows.Forms.Label
        Me.picBarraMorada = New System.Windows.Forms.PictureBox
        Me.cmbSiguiente = New System.Windows.Forms.Button
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.cmbSalir = New System.Windows.Forms.PictureBox
        Me.lblEmpaquesSolicitados = New System.Windows.Forms.Label
        Me.lblUsuarioEnSesion = New System.Windows.Forms.Label
        Me.TimerReload = New System.Windows.Forms.Timer
        Me.SuspendLayout()
        '
        'PictureFondoBuscarProveedor
        '
        Me.PictureFondoBuscarProveedor.Image = CType(resources.GetObject("PictureFondoBuscarProveedor.Image"), System.Drawing.Image)
        Me.PictureFondoBuscarProveedor.Location = New System.Drawing.Point(42, 0)
        Me.PictureFondoBuscarProveedor.Name = "PictureFondoBuscarProveedor"
        Me.PictureFondoBuscarProveedor.Size = New System.Drawing.Size(198, 38)
        Me.PictureFondoBuscarProveedor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'txtEmpaquesRecibidos
        '
        Me.txtEmpaquesRecibidos.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold)
        Me.txtEmpaquesRecibidos.Location = New System.Drawing.Point(126, 153)
        Me.txtEmpaquesRecibidos.MaxLength = 10
        Me.txtEmpaquesRecibidos.Name = "txtEmpaquesRecibidos"
        Me.txtEmpaquesRecibidos.Size = New System.Drawing.Size(75, 23)
        Me.txtEmpaquesRecibidos.TabIndex = 48
        '
        'lblEmpaquesRecibidos
        '
        Me.lblEmpaquesRecibidos.BackColor = System.Drawing.Color.Gainsboro
        Me.lblEmpaquesRecibidos.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblEmpaquesRecibidos.Location = New System.Drawing.Point(23, 153)
        Me.lblEmpaquesRecibidos.Name = "lblEmpaquesRecibidos"
        Me.lblEmpaquesRecibidos.Size = New System.Drawing.Size(97, 23)
        Me.lblEmpaquesRecibidos.Text = "Empaques"
        Me.lblEmpaquesRecibidos.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblUnidadesRecibidas
        '
        Me.lblUnidadesRecibidas.BackColor = System.Drawing.Color.Gainsboro
        Me.lblUnidadesRecibidas.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblUnidadesRecibidas.Location = New System.Drawing.Point(41, 181)
        Me.lblUnidadesRecibidas.Name = "lblUnidadesRecibidas"
        Me.lblUnidadesRecibidas.Size = New System.Drawing.Size(79, 24)
        Me.lblUnidadesRecibidas.Text = "Unidades"
        Me.lblUnidadesRecibidas.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtUnidadesRecibidas
        '
        Me.txtUnidadesRecibidas.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold)
        Me.txtUnidadesRecibidas.Location = New System.Drawing.Point(126, 182)
        Me.txtUnidadesRecibidas.MaxLength = 10
        Me.txtUnidadesRecibidas.Name = "txtUnidadesRecibidas"
        Me.txtUnidadesRecibidas.Size = New System.Drawing.Size(75, 23)
        Me.txtUnidadesRecibidas.TabIndex = 47
        '
        'lblUnidadesSolicitadas
        '
        Me.lblUnidadesSolicitadas.BackColor = System.Drawing.Color.Gainsboro
        Me.lblUnidadesSolicitadas.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold)
        Me.lblUnidadesSolicitadas.Location = New System.Drawing.Point(137, 215)
        Me.lblUnidadesSolicitadas.Name = "lblUnidadesSolicitadas"
        Me.lblUnidadesSolicitadas.Size = New System.Drawing.Size(100, 15)
        Me.lblUnidadesSolicitadas.Text = "Uni. Sol.:"
        '
        'lblCantidadRecibida
        '
        Me.lblCantidadRecibida.BackColor = System.Drawing.Color.Gainsboro
        Me.lblCantidadRecibida.Font = New System.Drawing.Font("Verdana", 14.0!, System.Drawing.FontStyle.Bold)
        Me.lblCantidadRecibida.Location = New System.Drawing.Point(15, 125)
        Me.lblCantidadRecibida.Name = "lblCantidadRecibida"
        Me.lblCantidadRecibida.Size = New System.Drawing.Size(207, 25)
        Me.lblCantidadRecibida.Text = "Cantidad Recibida"
        Me.lblCantidadRecibida.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblUnidadesxEmpaque
        '
        Me.lblUnidadesxEmpaque.BackColor = System.Drawing.Color.Gainsboro
        Me.lblUnidadesxEmpaque.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblUnidadesxEmpaque.Location = New System.Drawing.Point(52, 105)
        Me.lblUnidadesxEmpaque.Name = "lblUnidadesxEmpaque"
        Me.lblUnidadesxEmpaque.Size = New System.Drawing.Size(163, 16)
        Me.lblUnidadesxEmpaque.Text = "Uni. x Emp.:"
        '
        'lblFechaRecepcion
        '
        Me.lblFechaRecepcion.BackColor = System.Drawing.Color.Gainsboro
        Me.lblFechaRecepcion.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold)
        Me.lblFechaRecepcion.Location = New System.Drawing.Point(63, 75)
        Me.lblFechaRecepcion.Name = "lblFechaRecepcion"
        Me.lblFechaRecepcion.Size = New System.Drawing.Size(132, 15)
        '
        'lblNºRec
        '
        Me.lblNºRec.BackColor = System.Drawing.Color.Gainsboro
        Me.lblNºRec.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold)
        Me.lblNºRec.Location = New System.Drawing.Point(63, 61)
        Me.lblNºRec.Name = "lblNºRec"
        Me.lblNºRec.Size = New System.Drawing.Size(132, 15)
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Gainsboro
        Me.Label2.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold)
        Me.Label2.Location = New System.Drawing.Point(5, 75)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(61, 15)
        Me.Label2.Text = "Fecha:"
        '
        'lblRec
        '
        Me.lblRec.BackColor = System.Drawing.Color.Gainsboro
        Me.lblRec.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold)
        Me.lblRec.Location = New System.Drawing.Point(5, 61)
        Me.lblRec.Name = "lblRec"
        Me.lblRec.Size = New System.Drawing.Size(61, 15)
        Me.lblRec.Text = "Nº Rec.:"
        '
        'lblCantidadFaltante
        '
        Me.lblCantidadFaltante.BackColor = System.Drawing.Color.Gainsboro
        Me.lblCantidadFaltante.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold)
        Me.lblCantidadFaltante.Location = New System.Drawing.Point(25, 239)
        Me.lblCantidadFaltante.Name = "lblCantidadFaltante"
        Me.lblCantidadFaltante.Size = New System.Drawing.Size(146, 15)
        Me.lblCantidadFaltante.Text = "Cant. Faltante:"
        '
        'picBarraMorada
        '
        Me.picBarraMorada.Image = CType(resources.GetObject("picBarraMorada.Image"), System.Drawing.Image)
        Me.picBarraMorada.Location = New System.Drawing.Point(0, 260)
        Me.picBarraMorada.Name = "picBarraMorada"
        Me.picBarraMorada.Size = New System.Drawing.Size(240, 40)
        Me.picBarraMorada.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'cmbSiguiente
        '
        Me.cmbSiguiente.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.cmbSiguiente.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.cmbSiguiente.ForeColor = System.Drawing.Color.Black
        Me.cmbSiguiente.Location = New System.Drawing.Point(78, 265)
        Me.cmbSiguiente.Name = "cmbSiguiente"
        Me.cmbSiguiente.Size = New System.Drawing.Size(78, 25)
        Me.cmbSiguiente.TabIndex = 87
        Me.cmbSiguiente.Text = "Siguiente"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Gainsboro
        Me.PictureBox1.Location = New System.Drawing.Point(0, 54)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(240, 203)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'cmbSalir
        '
        Me.cmbSalir.Image = CType(resources.GetObject("cmbSalir.Image"), System.Drawing.Image)
        Me.cmbSalir.Location = New System.Drawing.Point(0, 0)
        Me.cmbSalir.Name = "cmbSalir"
        Me.cmbSalir.Size = New System.Drawing.Size(41, 38)
        Me.cmbSalir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'lblEmpaquesSolicitados
        '
        Me.lblEmpaquesSolicitados.BackColor = System.Drawing.Color.Gainsboro
        Me.lblEmpaquesSolicitados.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold)
        Me.lblEmpaquesSolicitados.Location = New System.Drawing.Point(25, 215)
        Me.lblEmpaquesSolicitados.Name = "lblEmpaquesSolicitados"
        Me.lblEmpaquesSolicitados.Size = New System.Drawing.Size(108, 15)
        Me.lblEmpaquesSolicitados.Text = "Emp. Sol.:"
        '
        'lblUsuarioEnSesion
        '
        Me.lblUsuarioEnSesion.BackColor = System.Drawing.Color.DarkGray
        Me.lblUsuarioEnSesion.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblUsuarioEnSesion.Location = New System.Drawing.Point(0, 39)
        Me.lblUsuarioEnSesion.Name = "lblUsuarioEnSesion"
        Me.lblUsuarioEnSesion.Size = New System.Drawing.Size(240, 15)
        Me.lblUsuarioEnSesion.Text = "Usuario:"
        '
        'TimerReload
        '
        Me.TimerReload.Interval = 1250
        '
        'FormCantidadRecibida_Recepcion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(240, 300)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblUsuarioEnSesion)
        Me.Controls.Add(Me.lblEmpaquesSolicitados)
        Me.Controls.Add(Me.cmbSalir)
        Me.Controls.Add(Me.txtEmpaquesRecibidos)
        Me.Controls.Add(Me.txtUnidadesRecibidas)
        Me.Controls.Add(Me.lblCantidadFaltante)
        Me.Controls.Add(Me.lblFechaRecepcion)
        Me.Controls.Add(Me.lblNºRec)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblRec)
        Me.Controls.Add(Me.lblCantidadRecibida)
        Me.Controls.Add(Me.lblUnidadesxEmpaque)
        Me.Controls.Add(Me.lblEmpaquesRecibidos)
        Me.Controls.Add(Me.lblUnidadesRecibidas)
        Me.Controls.Add(Me.lblUnidadesSolicitadas)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.cmbSiguiente)
        Me.Controls.Add(Me.picBarraMorada)
        Me.Controls.Add(Me.PictureFondoBuscarProveedor)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormCantidadRecibida_Recepcion"
        Me.Text = "isMOBILE - Recepción"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PictureFondoBuscarProveedor As System.Windows.Forms.PictureBox
    Friend WithEvents txtEmpaquesRecibidos As System.Windows.Forms.TextBox
    Friend WithEvents lblEmpaquesRecibidos As System.Windows.Forms.Label
    Friend WithEvents lblUnidadesRecibidas As System.Windows.Forms.Label
    Friend WithEvents txtUnidadesRecibidas As System.Windows.Forms.TextBox
    Friend WithEvents lblUnidadesSolicitadas As System.Windows.Forms.Label
    Friend WithEvents lblCantidadRecibida As System.Windows.Forms.Label
    Friend WithEvents lblUnidadesxEmpaque As System.Windows.Forms.Label
    Friend WithEvents lblFechaRecepcion As System.Windows.Forms.Label
    Friend WithEvents lblNºRec As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblRec As System.Windows.Forms.Label
    Friend WithEvents lblCantidadFaltante As System.Windows.Forms.Label
    Friend WithEvents picBarraMorada As System.Windows.Forms.PictureBox
    Friend WithEvents cmbSiguiente As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents cmbSalir As System.Windows.Forms.PictureBox
    Friend WithEvents lblEmpaquesSolicitados As System.Windows.Forms.Label
    Friend WithEvents lblUsuarioEnSesion As System.Windows.Forms.Label
    Friend WithEvents TimerReload As System.Windows.Forms.Timer
End Class
