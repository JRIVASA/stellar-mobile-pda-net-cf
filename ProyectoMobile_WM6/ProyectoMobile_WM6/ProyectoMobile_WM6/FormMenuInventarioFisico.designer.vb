﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class FormMenuInventarioFisico
    Inherits System.Windows.Forms.Form

    'Form invalida a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer
    Private mainMenu1 As System.Windows.Forms.MainMenu

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar con el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormMenuInventarioFisico))
        Me.mainMenu1 = New System.Windows.Forms.MainMenu
        Me.cmbEnviarPendientes = New System.Windows.Forms.PictureBox
        Me.cmbActualizar = New System.Windows.Forms.PictureBox
        Me.PanelMenu_Principal = New System.Windows.Forms.Panel
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.lblRecepcion = New System.Windows.Forms.Label
        Me.lblUsuarioEnSesion = New System.Windows.Forms.Label
        Me.cmbSalir = New System.Windows.Forms.PictureBox
        Me.cmbInventario = New System.Windows.Forms.PictureBox
        Me.PictureFondoMenuInventarioFisico = New System.Windows.Forms.PictureBox
        Me.PanelMenu_Principal.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmbEnviarPendientes
        '
        Me.cmbEnviarPendientes.Image = CType(resources.GetObject("cmbEnviarPendientes.Image"), System.Drawing.Image)
        Me.cmbEnviarPendientes.Location = New System.Drawing.Point(170, 128)
        Me.cmbEnviarPendientes.Name = "cmbEnviarPendientes"
        Me.cmbEnviarPendientes.Size = New System.Drawing.Size(48, 48)
        Me.cmbEnviarPendientes.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'cmbActualizar
        '
        Me.cmbActualizar.Image = CType(resources.GetObject("cmbActualizar.Image"), System.Drawing.Image)
        Me.cmbActualizar.Location = New System.Drawing.Point(98, 128)
        Me.cmbActualizar.Name = "cmbActualizar"
        Me.cmbActualizar.Size = New System.Drawing.Size(48, 48)
        Me.cmbActualizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'PanelMenu_Principal
        '
        Me.PanelMenu_Principal.BackColor = System.Drawing.Color.Gainsboro
        Me.PanelMenu_Principal.Controls.Add(Me.Label2)
        Me.PanelMenu_Principal.Controls.Add(Me.Label1)
        Me.PanelMenu_Principal.Controls.Add(Me.lblRecepcion)
        Me.PanelMenu_Principal.Controls.Add(Me.lblUsuarioEnSesion)
        Me.PanelMenu_Principal.Controls.Add(Me.cmbSalir)
        Me.PanelMenu_Principal.Controls.Add(Me.cmbEnviarPendientes)
        Me.PanelMenu_Principal.Controls.Add(Me.cmbActualizar)
        Me.PanelMenu_Principal.Controls.Add(Me.cmbInventario)
        Me.PanelMenu_Principal.Controls.Add(Me.PictureFondoMenuInventarioFisico)
        Me.PanelMenu_Principal.Location = New System.Drawing.Point(0, 0)
        Me.PanelMenu_Principal.Name = "PanelMenu_Principal"
        Me.PanelMenu_Principal.Size = New System.Drawing.Size(240, 300)
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label2.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular)
        Me.Label2.Location = New System.Drawing.Point(161, 178)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(63, 14)
        Me.Label2.Text = "Pendiente"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label1.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular)
        Me.Label1.Location = New System.Drawing.Point(92, 178)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(63, 14)
        Me.Label1.Text = "Actualizar"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblRecepcion
        '
        Me.lblRecepcion.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.lblRecepcion.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular)
        Me.lblRecepcion.Location = New System.Drawing.Point(15, 178)
        Me.lblRecepcion.Name = "lblRecepcion"
        Me.lblRecepcion.Size = New System.Drawing.Size(70, 14)
        Me.lblRecepcion.Text = "Inventario"
        Me.lblRecepcion.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblUsuarioEnSesion
        '
        Me.lblUsuarioEnSesion.BackColor = System.Drawing.Color.DarkGray
        Me.lblUsuarioEnSesion.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblUsuarioEnSesion.Location = New System.Drawing.Point(0, 39)
        Me.lblUsuarioEnSesion.Name = "lblUsuarioEnSesion"
        Me.lblUsuarioEnSesion.Size = New System.Drawing.Size(240, 15)
        Me.lblUsuarioEnSesion.Text = "Usuario:"
        '
        'cmbSalir
        '
        Me.cmbSalir.Image = CType(resources.GetObject("cmbSalir.Image"), System.Drawing.Image)
        Me.cmbSalir.Location = New System.Drawing.Point(0, 0)
        Me.cmbSalir.Name = "cmbSalir"
        Me.cmbSalir.Size = New System.Drawing.Size(41, 38)
        Me.cmbSalir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'cmbInventario
        '
        Me.cmbInventario.Image = CType(resources.GetObject("cmbInventario.Image"), System.Drawing.Image)
        Me.cmbInventario.Location = New System.Drawing.Point(25, 128)
        Me.cmbInventario.Name = "cmbInventario"
        Me.cmbInventario.Size = New System.Drawing.Size(48, 48)
        Me.cmbInventario.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'PictureFondoMenuInventarioFisico
        '
        Me.PictureFondoMenuInventarioFisico.Image = CType(resources.GetObject("PictureFondoMenuInventarioFisico.Image"), System.Drawing.Image)
        Me.PictureFondoMenuInventarioFisico.Location = New System.Drawing.Point(42, 0)
        Me.PictureFondoMenuInventarioFisico.Name = "PictureFondoMenuInventarioFisico"
        Me.PictureFondoMenuInventarioFisico.Size = New System.Drawing.Size(198, 38)
        Me.PictureFondoMenuInventarioFisico.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'FormMenuInventarioFisico
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(240, 300)
        Me.ControlBox = False
        Me.Controls.Add(Me.PanelMenu_Principal)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormMenuInventarioFisico"
        Me.Text = "isMOBILE - Inventario Físico"
        Me.PanelMenu_Principal.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cmbEnviarPendientes As System.Windows.Forms.PictureBox
    Friend WithEvents cmbActualizar As System.Windows.Forms.PictureBox
    Friend WithEvents PanelMenu_Principal As System.Windows.Forms.Panel
    Friend WithEvents cmbInventario As System.Windows.Forms.PictureBox
    Friend WithEvents PictureFondoMenuInventarioFisico As System.Windows.Forms.PictureBox
    Friend WithEvents cmbSalir As System.Windows.Forms.PictureBox
    Friend WithEvents lblUsuarioEnSesion As System.Windows.Forms.Label
    Friend WithEvents lblRecepcion As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
End Class
