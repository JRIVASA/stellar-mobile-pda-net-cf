﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class FormUnidadesxEmpaque_Recepcion
    Inherits System.Windows.Forms.Form

    'Form invalida a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer
    Private mainMenu1 As System.Windows.Forms.MainMenu

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar con el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormUnidadesxEmpaque_Recepcion))
        Me.mainMenu1 = New System.Windows.Forms.MainMenu
        Me.PictureFondo = New System.Windows.Forms.PictureBox
        Me.cmbSalir = New System.Windows.Forms.PictureBox
        Me.picBarraMorada = New System.Windows.Forms.PictureBox
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.lblUnidadesxEmpaque = New System.Windows.Forms.Label
        Me.txtUnidadesxEmpaque = New System.Windows.Forms.TextBox
        Me.lblConfirmar = New System.Windows.Forms.Label
        Me.cmbAceptar = New System.Windows.Forms.Button
        Me.cmbModificar = New System.Windows.Forms.Button
        Me.lblUsuarioEnSesion = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'PictureFondo
        '
        Me.PictureFondo.Image = CType(resources.GetObject("PictureFondo.Image"), System.Drawing.Image)
        Me.PictureFondo.Location = New System.Drawing.Point(42, 0)
        Me.PictureFondo.Name = "PictureFondo"
        Me.PictureFondo.Size = New System.Drawing.Size(198, 38)
        Me.PictureFondo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'cmbSalir
        '
        Me.cmbSalir.Image = CType(resources.GetObject("cmbSalir.Image"), System.Drawing.Image)
        Me.cmbSalir.Location = New System.Drawing.Point(0, 0)
        Me.cmbSalir.Name = "cmbSalir"
        Me.cmbSalir.Size = New System.Drawing.Size(41, 38)
        Me.cmbSalir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'picBarraMorada
        '
        Me.picBarraMorada.Image = CType(resources.GetObject("picBarraMorada.Image"), System.Drawing.Image)
        Me.picBarraMorada.Location = New System.Drawing.Point(0, 260)
        Me.picBarraMorada.Name = "picBarraMorada"
        Me.picBarraMorada.Size = New System.Drawing.Size(240, 40)
        Me.picBarraMorada.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Gainsboro
        Me.PictureBox1.Location = New System.Drawing.Point(0, 57)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(240, 205)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'lblUnidadesxEmpaque
        '
        Me.lblUnidadesxEmpaque.BackColor = System.Drawing.Color.Gainsboro
        Me.lblUnidadesxEmpaque.Font = New System.Drawing.Font("Verdana", 14.0!, System.Drawing.FontStyle.Bold)
        Me.lblUnidadesxEmpaque.Location = New System.Drawing.Point(1, 99)
        Me.lblUnidadesxEmpaque.Name = "lblUnidadesxEmpaque"
        Me.lblUnidadesxEmpaque.Size = New System.Drawing.Size(237, 40)
        Me.lblUnidadesxEmpaque.Text = "Unidades x Empaque"
        Me.lblUnidadesxEmpaque.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txtUnidadesxEmpaque
        '
        Me.txtUnidadesxEmpaque.Enabled = False
        Me.txtUnidadesxEmpaque.Font = New System.Drawing.Font("Verdana", 14.0!, System.Drawing.FontStyle.Bold)
        Me.txtUnidadesxEmpaque.Location = New System.Drawing.Point(75, 143)
        Me.txtUnidadesxEmpaque.MaxLength = 10
        Me.txtUnidadesxEmpaque.Name = "txtUnidadesxEmpaque"
        Me.txtUnidadesxEmpaque.Size = New System.Drawing.Size(90, 29)
        Me.txtUnidadesxEmpaque.TabIndex = 49
        '
        'lblConfirmar
        '
        Me.lblConfirmar.BackColor = System.Drawing.Color.Gainsboro
        Me.lblConfirmar.Font = New System.Drawing.Font("Verdana", 14.0!, System.Drawing.FontStyle.Bold)
        Me.lblConfirmar.Location = New System.Drawing.Point(15, 72)
        Me.lblConfirmar.Name = "lblConfirmar"
        Me.lblConfirmar.Size = New System.Drawing.Size(206, 26)
        Me.lblConfirmar.Text = "Confirmar"
        Me.lblConfirmar.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'cmbAceptar
        '
        Me.cmbAceptar.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.cmbAceptar.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.cmbAceptar.ForeColor = System.Drawing.Color.Black
        Me.cmbAceptar.Location = New System.Drawing.Point(129, 205)
        Me.cmbAceptar.Name = "cmbAceptar"
        Me.cmbAceptar.Size = New System.Drawing.Size(80, 30)
        Me.cmbAceptar.TabIndex = 114
        Me.cmbAceptar.Text = "Aceptar"
        '
        'cmbModificar
        '
        Me.cmbModificar.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.cmbModificar.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.cmbModificar.ForeColor = System.Drawing.Color.Black
        Me.cmbModificar.Location = New System.Drawing.Point(33, 205)
        Me.cmbModificar.Name = "cmbModificar"
        Me.cmbModificar.Size = New System.Drawing.Size(80, 30)
        Me.cmbModificar.TabIndex = 115
        Me.cmbModificar.Text = "Modificar"
        '
        'lblUsuarioEnSesion
        '
        Me.lblUsuarioEnSesion.BackColor = System.Drawing.Color.DarkGray
        Me.lblUsuarioEnSesion.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblUsuarioEnSesion.Location = New System.Drawing.Point(0, 39)
        Me.lblUsuarioEnSesion.Name = "lblUsuarioEnSesion"
        Me.lblUsuarioEnSesion.Size = New System.Drawing.Size(240, 15)
        Me.lblUsuarioEnSesion.Text = "Usuario:"
        '
        'FormUnidadesxEmpaque_Recepcion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(240, 300)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblUsuarioEnSesion)
        Me.Controls.Add(Me.cmbModificar)
        Me.Controls.Add(Me.cmbAceptar)
        Me.Controls.Add(Me.lblConfirmar)
        Me.Controls.Add(Me.txtUnidadesxEmpaque)
        Me.Controls.Add(Me.lblUnidadesxEmpaque)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.picBarraMorada)
        Me.Controls.Add(Me.cmbSalir)
        Me.Controls.Add(Me.PictureFondo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormUnidadesxEmpaque_Recepcion"
        Me.Text = "isMOBILE - Recepción"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PictureFondo As System.Windows.Forms.PictureBox
    Friend WithEvents cmbSalir As System.Windows.Forms.PictureBox
    Friend WithEvents picBarraMorada As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents lblUnidadesxEmpaque As System.Windows.Forms.Label
    Friend WithEvents txtUnidadesxEmpaque As System.Windows.Forms.TextBox
    Friend WithEvents lblConfirmar As System.Windows.Forms.Label
    Friend WithEvents cmbAceptar As System.Windows.Forms.Button
    Friend WithEvents cmbModificar As System.Windows.Forms.Button
    Friend WithEvents lblUsuarioEnSesion As System.Windows.Forms.Label
End Class
