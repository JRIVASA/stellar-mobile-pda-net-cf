﻿Public Class FormSeleccionImpresora

    Public Tipo_Impresion As String

    Private FormaCargada As Boolean

    Private Sub CmbBluetooth_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmbBluetooth.Click
        ImpresoraSeleccionada = ConexionImpresora.Impresora_Bluetooth
        HideAndReturn()
    End Sub

    Private Sub FormSeleccionImpresora_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Refresh()
        Me.Update()

        AjustarFormularios(Me)
        AjustarFormularios(Me.PanelMenu_Principal)

    End Sub

    Private Sub cmbSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSalir.Click
        HideAndReturn()
    End Sub

    Private Sub CmbWLAN_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmbWLAN.Click
        ImpresoraSeleccionada = ConexionImpresora.Impresora_WLAN
        HideAndReturn()
    End Sub

    Private Sub HideAndReturn()

        Select Case (Tipo_Impresion)
            Case Is = "Etiqueta"
                Form_Impresion_Etiquetas.Show()
            Case Is = "Hablador"
                Form_Impresion_Hablador.Show()
            Case Else
        End Select

        FormaCargada = False
        Me.Visible = False

    End Sub

    Private Sub Switch()

        If (ImpresoraSeleccionada = ConexionImpresora.Impresora_WLAN) Then
            PanelWLANSelected.BackColor = Color.CornflowerBlue
            PanelBluetoothSelected.BackColor = Color.Transparent
        ElseIf (ImpresoraSeleccionada = ConexionImpresora.Impresora_Bluetooth) Then
            PanelWLANSelected.BackColor = Color.Transparent
            PanelBluetoothSelected.BackColor = Color.CornflowerBlue
        End If

    End Sub

    Private Sub FormSeleccionImpresora_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated

        If Not FormaCargada Then

            FormaCargada = True

            TimerReload.Interval = 100
            TimerReload.Enabled = True

        End If

    End Sub

    Private Sub Activation()

        lblUsuarioEnSesion.Text = "Usuario: " + descripcionUsuario

        Switch()

    End Sub

    Private Sub TimerReload_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TimerReload.Tick
        TimerReload.Enabled = False
        Activation()
    End Sub

End Class