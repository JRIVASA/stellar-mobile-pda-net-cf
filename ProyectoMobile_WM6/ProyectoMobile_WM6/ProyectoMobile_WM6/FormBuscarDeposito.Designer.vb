﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class FormBuscarDeposito
    Inherits System.Windows.Forms.Form

    'Form invalida a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer
    Private mainMenu1 As System.Windows.Forms.MainMenu

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar con el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormBuscarDeposito))
        Me.mainMenu1 = New System.Windows.Forms.MainMenu
        Me.DataGridBuscarDeposito = New System.Windows.Forms.DataGrid
        Me.txtDescripcionDeposito = New System.Windows.Forms.TextBox
        Me.lblDescripcionDeposito = New System.Windows.Forms.Label
        Me.PictureFondoBuscarProveedor = New System.Windows.Forms.PictureBox
        Me.cmbBuscarDeposito = New System.Windows.Forms.Button
        Me.cmbSalir = New System.Windows.Forms.PictureBox
        Me.picBarraMorada = New System.Windows.Forms.PictureBox
        Me.cmbAceptar = New System.Windows.Forms.Button
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.lblUsuarioEnSesion = New System.Windows.Forms.Label
        Me.TimerReload = New System.Windows.Forms.Timer
        Me.SuspendLayout()
        '
        'DataGridBuscarDeposito
        '
        Me.DataGridBuscarDeposito.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.DataGridBuscarDeposito.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold)
        Me.DataGridBuscarDeposito.Location = New System.Drawing.Point(9, 119)
        Me.DataGridBuscarDeposito.Name = "DataGridBuscarDeposito"
        Me.DataGridBuscarDeposito.Size = New System.Drawing.Size(220, 134)
        Me.DataGridBuscarDeposito.TabIndex = 26
        '
        'txtDescripcionDeposito
        '
        Me.txtDescripcionDeposito.Font = New System.Drawing.Font("Verdana", 14.0!, System.Drawing.FontStyle.Bold)
        Me.txtDescripcionDeposito.Location = New System.Drawing.Point(9, 84)
        Me.txtDescripcionDeposito.MaxLength = 50
        Me.txtDescripcionDeposito.Name = "txtDescripcionDeposito"
        Me.txtDescripcionDeposito.Size = New System.Drawing.Size(135, 29)
        Me.txtDescripcionDeposito.TabIndex = 24
        '
        'lblDescripcionDeposito
        '
        Me.lblDescripcionDeposito.BackColor = System.Drawing.Color.Gainsboro
        Me.lblDescripcionDeposito.Font = New System.Drawing.Font("Verdana", 14.0!, System.Drawing.FontStyle.Bold)
        Me.lblDescripcionDeposito.Location = New System.Drawing.Point(48, 58)
        Me.lblDescripcionDeposito.Name = "lblDescripcionDeposito"
        Me.lblDescripcionDeposito.Size = New System.Drawing.Size(135, 25)
        Me.lblDescripcionDeposito.Text = "Deposito"
        Me.lblDescripcionDeposito.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'PictureFondoBuscarProveedor
        '
        Me.PictureFondoBuscarProveedor.Image = CType(resources.GetObject("PictureFondoBuscarProveedor.Image"), System.Drawing.Image)
        Me.PictureFondoBuscarProveedor.Location = New System.Drawing.Point(42, 0)
        Me.PictureFondoBuscarProveedor.Name = "PictureFondoBuscarProveedor"
        Me.PictureFondoBuscarProveedor.Size = New System.Drawing.Size(198, 38)
        Me.PictureFondoBuscarProveedor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'cmbBuscarDeposito
        '
        Me.cmbBuscarDeposito.BackColor = System.Drawing.Color.White
        Me.cmbBuscarDeposito.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.cmbBuscarDeposito.ForeColor = System.Drawing.Color.Black
        Me.cmbBuscarDeposito.Location = New System.Drawing.Point(151, 84)
        Me.cmbBuscarDeposito.Name = "cmbBuscarDeposito"
        Me.cmbBuscarDeposito.Size = New System.Drawing.Size(78, 29)
        Me.cmbBuscarDeposito.TabIndex = 31
        Me.cmbBuscarDeposito.Text = "Buscar"
        '
        'cmbSalir
        '
        Me.cmbSalir.Image = CType(resources.GetObject("cmbSalir.Image"), System.Drawing.Image)
        Me.cmbSalir.Location = New System.Drawing.Point(0, 0)
        Me.cmbSalir.Name = "cmbSalir"
        Me.cmbSalir.Size = New System.Drawing.Size(41, 38)
        Me.cmbSalir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'picBarraMorada
        '
        Me.picBarraMorada.Image = CType(resources.GetObject("picBarraMorada.Image"), System.Drawing.Image)
        Me.picBarraMorada.Location = New System.Drawing.Point(0, 260)
        Me.picBarraMorada.Name = "picBarraMorada"
        Me.picBarraMorada.Size = New System.Drawing.Size(240, 40)
        Me.picBarraMorada.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'cmbAceptar
        '
        Me.cmbAceptar.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.cmbAceptar.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.cmbAceptar.ForeColor = System.Drawing.Color.Black
        Me.cmbAceptar.Location = New System.Drawing.Point(78, 265)
        Me.cmbAceptar.Name = "cmbAceptar"
        Me.cmbAceptar.Size = New System.Drawing.Size(78, 25)
        Me.cmbAceptar.TabIndex = 115
        Me.cmbAceptar.Text = "Aceptar"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Gainsboro
        Me.PictureBox1.Location = New System.Drawing.Point(0, 54)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(240, 205)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'lblUsuarioEnSesion
        '
        Me.lblUsuarioEnSesion.BackColor = System.Drawing.Color.DarkGray
        Me.lblUsuarioEnSesion.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblUsuarioEnSesion.Location = New System.Drawing.Point(0, 39)
        Me.lblUsuarioEnSesion.Name = "lblUsuarioEnSesion"
        Me.lblUsuarioEnSesion.Size = New System.Drawing.Size(240, 15)
        Me.lblUsuarioEnSesion.Text = "Usuario:"
        '
        'TimerReload
        '
        Me.TimerReload.Interval = 1250
        '
        'FormBuscarDeposito
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(240, 300)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblUsuarioEnSesion)
        Me.Controls.Add(Me.cmbBuscarDeposito)
        Me.Controls.Add(Me.DataGridBuscarDeposito)
        Me.Controls.Add(Me.txtDescripcionDeposito)
        Me.Controls.Add(Me.lblDescripcionDeposito)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.cmbAceptar)
        Me.Controls.Add(Me.picBarraMorada)
        Me.Controls.Add(Me.cmbSalir)
        Me.Controls.Add(Me.PictureFondoBuscarProveedor)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormBuscarDeposito"
        Me.Text = "isMOBILE - Buscar Depósito"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridBuscarDeposito As System.Windows.Forms.DataGrid
    Friend WithEvents txtDescripcionDeposito As System.Windows.Forms.TextBox
    Friend WithEvents lblDescripcionDeposito As System.Windows.Forms.Label
    Friend WithEvents PictureFondoBuscarProveedor As System.Windows.Forms.PictureBox
    Friend WithEvents cmbBuscarDeposito As System.Windows.Forms.Button
    Friend WithEvents cmbSalir As System.Windows.Forms.PictureBox
    Friend WithEvents picBarraMorada As System.Windows.Forms.PictureBox
    Friend WithEvents cmbAceptar As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents lblUsuarioEnSesion As System.Windows.Forms.Label
    Friend WithEvents TimerReload As System.Windows.Forms.Timer
End Class
