﻿Public Class FormLecturaProducto_Recepcion

    Dim dataset_ProductoenODC, dataset_Codigos, dataset_Productos As Data.DataSet

    Private Sub FormRecepcion_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        lblUsuarioEnSesion.Text = "Usuario: " + descripcionUsuario
        lblFechaRecepcion.Text = Date.Now.ToString

        If CompatibilidadFueraDeLinea Then
            cmbActualizar.Visible = True
        Else
            cmbActualizar.Visible = False
        End If

        Me.Refresh()
        Me.Update()

        AjustarFormularios(Me)

        txtCodigoProducto.Focus()
        verFormCantidadFacturada = True

    End Sub

    Private Sub txtCodigoProducto_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCodigoProducto.KeyDown

        'Cuando se da enter luego de ingresar el código del producto y no esta vacio; se verifica que el código existe
        Select Case e.KeyValue

            Case Is = 13

                If txtCodigoProducto.Text <> "" Then

                    If txtCodigoProducto.TextLength <= 15 Then

                        lblProgressBar_FormLecturaProducto.Visible = True
                        ProgressBar_FormLecturaProducto.Visible = True
                        Me.Refresh()
                        Me.Update()
                        ProgressBar_FormLecturaProducto.Value = 5

                        dataset_Codigos = buscarCodigoProducto(txtCodigoProducto.Text)

                        If (dataset_Codigos Is Nothing) Then
                            OcultarBarra()
                            MsgBox("La información del producto no se ha cargado. Ingrese el producto nuevamente.", MsgBoxStyle.Information, "isMOBILE")
                            lblDescripcion.Text = "Descripción: "
                            txtCodigoProducto.Focus()
                            Return
                        End If

                        ProgressBar_FormLecturaProducto.Value = 50

                        'Se verifica que la consulta trajo resultados o no
                        If dataset_Codigos.Tables(0).Rows.Count() = 0 Then

                            OcultarBarra()

                            MsgBox("El código del producto no existe.", MsgBoxStyle.Information, "isMOBILE")
                            txtCodigoProducto.Text = ""
                            lblDescripcion.Text = "Descripción: "
                            txtCodigoProducto.Focus()

                        Else 'El producto existe 
                            'cambio el texto del txtCodigoProducto para que sea el c_codnasa de MA_CODIGOS que es el que se usa en todas las consultas

                            txtCodigoProducto.Text = dataset_Codigos.Tables(0).Rows(0)("c_codnasa")

                            dataset_Productos = detallesProducto(txtCodigoProducto.Text)

                            If (dataset_Productos Is Nothing) Then
                                OcultarBarra()
                                MsgBox("La información del producto no se ha cargado. Ingrese el producto nuevamente.", MsgBoxStyle.Information, "isMOBILE")
                                lblDescripcion.Text = "Descripción: "
                                txtCodigoProducto.Focus()
                                Return
                            ElseIf (dataset_Productos.Tables(0).Rows.Count <= 0) Then
                                OcultarBarra()
                                MsgBox("El producto no existe en la base de datos." & vbNewLine _
                                & "Es recomendable que actualice los productos antes de continuar" & vbNewLine _
                                & "con la recepción, a través del modulo de Inventario.", MsgBoxStyle.Information, "isMOBILE")
                                lblDescripcion.Text = "Descripción: "
                                txtCodigoProducto.Focus()
                                Return
                            End If

                            RegladeNegocio = "Recepcion_PermiteRecibirsoloconODC"
                            dataset_RegladeNegocio = consultarRegladeNegocio(RegladeNegocio)

                            If (dataset_RegladeNegocio Is Nothing) Then
                                OcultarBarra()
                                MsgBox("Ha ocurrido un error al cargar las reglas de negocio.", MsgBoxStyle.Information, "isMOBILE")
                                lblDescripcion.Text = "Descripción: "
                                txtCodigoProducto.Focus()
                                Return
                            ElseIf (dataset_RegladeNegocio.Tables(0).Rows.Count <= 0) Then
                                OcultarBarra()
                                MsgBox("No existe la regla de negocio Recepcion_PermiteRecibirsoloconODC.", MsgBoxStyle.Information, "isMOBILE")
                                lblDescripcion.Text = "Descripción: "
                                txtCodigoProducto.Focus()
                                Return
                            End If

                            valorRegladeNegocio = dataset_RegladeNegocio.Tables(0).Rows(0)("valor")

                            ProgressBar_FormLecturaProducto.Value = 60

                            If valorRegladeNegocio = 1 Then 'Permite recibir el producto sólo si encuentra en la orden de compra
                                'verifico que el producto se encuentre en la ODC
                                dataset_ProductoenODC = BuscarProductoenODC(txtCodigoProducto.Text) 'busca en la tabla  TR_ODC

                                If (dataset_ProductoenODC Is Nothing) Then
                                    OcultarBarra()
                                    MsgBox("La información del producto en orden de compra no se ha cargado. Ingrese el producto nuevamente.", MsgBoxStyle.Information, "isMOBILE")
                                    lblDescripcion.Text = "Descripción:"
                                    txtCodigoProducto.Focus()
                                    Return
                                End If

                                If dataset_ProductoenODC.Tables(0).Rows.Count = 0 Then 'No se encuentra el producto en la ODC, o no se importó una ODC al inicio de la recepción
                                    'Aquí el valor de la línea va a ser 1, que es el valor que se le da cada vez que se inicia una recepción en el formDocumentoTransportista

                                    ProgressBar_FormLecturaProducto.Value = 80

                                    If codigoODC = "" Then 'No se importó una ODC al inicio de la recepción

                                        OcultarBarra()

                                        MsgBox("No se importó una orden de compra, no está permitido recibir un producto sin orden de compra.", MsgBoxStyle.Information, "isMOBILE")
                                        lblDescripcion.Text = "Descripción:"
                                        txtCodigoProducto.Focus()

                                    Else

                                        OcultarBarra()

                                        MsgBox("El producto no se encuentra en la orden de compra.", MsgBoxStyle.Information, "isMOBILE")
                                        lblDescripcion.Text = "Descripción:"
                                        txtCodigoProducto.Focus()

                                    End If

                                Else 'si existen registros en el dataset, pueden ser mas de uno, en caso de que el mismo producto esté varias veces en la ODC en lineas diferentes

                                    If dataset_ProductoenODC.Tables(0).Rows.Count = 1 Then 'si hay una sola linea en la ODC con ese producto 

                                        cantidadSolicitada = dataset_ProductoenODC.Tables(0).Rows(0)("n_cantidad")
                                        cantidadRecibida = dataset_ProductoenODC.Tables(0).Rows(0)("n_cant_recibida")

                                        UnidadesxEmpaque = determinarUnidadesxEmpaque(dataset_Codigos, dataset_Productos)
                                        FormCantidadRecibida_Recepcion.lblUnidadesxEmpaque.Text = "Uni.x Emp.: " + CStr(UnidadesxEmpaque)
                                        FormUnidadesxEmpaque_Recepcion.txtUnidadesxEmpaque.Text = CStr(UnidadesxEmpaque)

                                        determinarCantidadFaltanteFormulariosconODC()

                                        ProgressBar_FormLecturaProducto.Value = 80

                                    Else ' si el producto esta en varias lineas de la ODC

                                        'con este for siempre va a tomar los valores de la ultima linea en la que se encuentre el producto, donde la cantidad solicitada
                                        'es menor que la cantidad recibida

                                        For numeroFilas = 0 To dataset_ProductoenODC.Tables(0).Rows.Count - 1
                                            cantidadSolicitada = dataset_ProductoenODC.Tables(0).Rows(numeroFilas)("n_cantidad")
                                            cantidadRecibida = dataset_ProductoenODC.Tables(0).Rows(numeroFilas)("n_cant_recibida")
                                        Next

                                        UnidadesxEmpaque = determinarUnidadesxEmpaque(dataset_Codigos, dataset_Productos)
                                        FormCantidadRecibida_Recepcion.lblUnidadesxEmpaque.Text = "Uni.x Emp.: " + CStr(UnidadesxEmpaque)
                                        FormUnidadesxEmpaque_Recepcion.txtUnidadesxEmpaque.Text = CStr(UnidadesxEmpaque)

                                        determinarCantidadFaltanteFormulariosconODC()

                                        ProgressBar_FormLecturaProducto.Value = 80

                                    End If

                                    OcultarBarra()

                                End If

                            Else 'Permite recibir productos que no esten en la ODC

                                dataset_ProductoenODC = BuscarProductoenODC(txtCodigoProducto.Text)

                                If (dataset_ProductoenODC Is Nothing) Then
                                    OcultarBarra()
                                    MsgBox("La información del producto en orden de compra no se ha cargado. Ingrese el producto nuevamente.", MsgBoxStyle.Information, "isMOBILE")
                                    lblDescripcion.Text = "Descripción:"
                                    txtCodigoProducto.Focus()
                                    Return
                                End If

                                If dataset_ProductoenODC.Tables(0).Rows.Count = 0 Then 'No se encuentra el producto en la ODC, o no se importó una ODC al inicio de la recepción

                                    ' debo consultar el nivel del usuario ingresado

                                    ProgressBar_FormLecturaProducto.Value = 80

                                    consultarNivelUsuario_PermiteRecibirSinODC(dataset_Codigos)

                                    'Ya oculta barra en ese procedimiento anterior

                                Else 'si existen registros en el dataset, pueden ser mas de uno, en caso de que el mismo producto esté varias veces en la ODC en lineas diferentes

                                    If dataset_ProductoenODC.Tables(0).Rows.Count = 1 Then ' si hay una sola linea en la ODC con ese producto 

                                        dataset_Productos = detallesProducto(txtCodigoProducto.Text)

                                        If (dataset_Productos Is Nothing) Then
                                            OcultarBarra()
                                            MsgBox("La información del producto no se ha cargado. Ingrese el producto nuevamente.", MsgBoxStyle.Information, "isMOBILE")
                                            lblDescripcion.Text = "Descripción:"
                                            txtCodigoProducto.Focus()
                                            Return
                                        End If

                                        If dataset_Productos.Tables(0).Rows.Count = 0 Then

                                            OcultarBarra()

                                            MsgBox("El producto no existe en la base de datos." & vbNewLine _
                                            & "Es recomendable que actualice los productos antes de continuar" & vbNewLine _
                                            & "con la recepción, a través del modulo de Inventario.", MsgBoxStyle.Information, "isMOBILE")
                                            txtCodigoProducto.Text = ""
                                            lblDescripcion.Text = "Descripción: "
                                            txtCodigoProducto.Focus()

                                            Exit Sub

                                        End If

                                        lblDescripcion.Text = "Descripción: " + CStr(dataset_Productos.Tables(0).Rows(0)("C_DESCRI"))
                                        txtCodigoProducto.Enabled = False

                                        cantidadSolicitada = dataset_ProductoenODC.Tables(0).Rows(0)("n_cantidad")
                                        cantidadRecibida = dataset_ProductoenODC.Tables(0).Rows(0)("n_cant_recibida")

                                        ProgressBar_FormLecturaProducto.Value = 80

                                        UnidadesxEmpaque = determinarUnidadesxEmpaque(dataset_Codigos, dataset_Productos)

                                        FormCantidadRecibida_Recepcion.lblUnidadesxEmpaque.Text = "Uni.x Emp.: " + CStr(UnidadesxEmpaque)
                                        FormUnidadesxEmpaque_Recepcion.txtUnidadesxEmpaque.Text = CStr(UnidadesxEmpaque)

                                        determinarCantidadFaltanteFormulariosconODC()

                                    Else ' si el producto esta en varias lineas de la ODC

                                        'con este for siempre va a tomar los valores de la ultima linea en la que se encuentre el producto, donde la cantidad solicitada
                                        'es menor que la cantidad recibida

                                        dataset_Productos = detallesProducto(txtCodigoProducto.Text)

                                        If (dataset_Productos Is Nothing) Then
                                            OcultarBarra()
                                            MsgBox("La información del producto no se ha cargado. Ingrese el producto nuevamente.", MsgBoxStyle.Information, "isMOBILE")
                                            lblDescripcion.Text = "Descripción:"
                                            txtCodigoProducto.Focus()
                                            Return
                                        End If

                                        If dataset_Productos.Tables(0).Rows.Count = 0 Then

                                            OcultarBarra()

                                            MsgBox("El producto no existe en la base de datos." & vbNewLine _
                                            & "Es recomendable que actualice los productos antes de continuar" & vbNewLine _
                                            & "con la recepción, a través del modulo de Inventario.", MsgBoxStyle.Information, "isMOBILE")
                                            txtCodigoProducto.Text = ""
                                            lblDescripcion.Text = "Descripción: "
                                            txtCodigoProducto.Focus()

                                            Exit Sub

                                        End If

                                        lblDescripcion.Text = "Descripción: " + CStr(dataset_Productos.Tables(0).Rows(0)("C_DESCRI"))
                                        txtCodigoProducto.Enabled = False

                                        For numeroFilas = 0 To dataset_ProductoenODC.Tables(0).Rows.Count - 1
                                            cantidadSolicitada = dataset_ProductoenODC.Tables(0).Rows(numeroFilas)("n_cantidad")
                                            cantidadRecibida = dataset_ProductoenODC.Tables(0).Rows(numeroFilas)("n_cant_recibida")
                                        Next

                                        ProgressBar_FormLecturaProducto.Value = 80

                                        UnidadesxEmpaque = determinarUnidadesxEmpaque(dataset_Codigos, dataset_Productos)
                                        FormCantidadRecibida_Recepcion.lblUnidadesxEmpaque.Text = "Uni.x Emp.: " + CStr(UnidadesxEmpaque)
                                        FormUnidadesxEmpaque_Recepcion.txtUnidadesxEmpaque.Text = CStr(UnidadesxEmpaque)

                                        determinarCantidadFaltanteFormulariosconODC()

                                    End If

                                    OcultarBarra()

                                End If

                            End If

                        End If

                    Else

                        OcultarBarra()

                        MsgBox("Código inválido.", MsgBoxStyle.Information, "isMOBILE")
                        txtCodigoProducto.Text = ""
                        txtCodigoProducto.Enabled = True
                        txtCodigoProducto.Focus()

                    End If

                Else
                    MsgBox("Debe ingresar el código del producto.", MsgBoxStyle.Information, "isMOBILE")
                    txtCodigoProducto.Enabled = True
                    txtCodigoProducto.Focus()
                End If

        End Select

    End Sub

    Private Sub cmbLimpiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbLimpiar.Click
        If txtCodigoProducto.Text <> "" Then
            respuesta = MsgBox("¿Está seguro que desea borrar la información del producto?", MsgBoxStyle.YesNo, "isMOBILE")
            If respuesta = "6" Then
                limpiarFormLecturaProducto()
            End If
        Else
            MsgBox("No ha ingresado ningún código de producto.", MsgBoxStyle.OkOnly, "isMOBILE")
            txtCodigoProducto.Enabled = True
            txtCodigoProducto.Focus()
        End If
    End Sub

    Private Sub cmbSiguiente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSiguiente.Click
        If txtCodigoProducto.Text = "" Then
            MsgBox("Debe ingresar el código del producto.", MsgBoxStyle.Information, "isMOBILE")
            txtCodigoProducto.Focus()
        Else
            If txtCodigoProducto.Enabled = True Then
                MsgBox("Debe ingresar el código del producto y presionar el botón Enter" & vbNewLine & "o escanear el código de barras del producto.", MsgBoxStyle.Information, "isMOBILE")
                txtCodigoProducto.Focus()
            Else
                FormCantidadRecibida_Recepcion.Show()
                Me.Visible = False
            End If
        End If
    End Sub

    Private Sub OcultarBarra()
        ProgressBar_FormLecturaProducto.Value = 100
        ProgressBar_FormLecturaProducto.Visible = False
        lblProgressBar_FormLecturaProducto.Visible = False
    End Sub

    Private Sub cmbSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSalir.Click
        FormDocumentoTransportista_Recepcion.Show()
        Me.Visible = False
    End Sub

    Private Sub cmbActualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbActualizar.Click
        If ActualizacionParcial("Productos") Then
            MsgBox("Actualización parcial de Productos completada.", MsgBoxStyle.Information, "isMOBILE")
        Else
            MsgBox("No se pudo realizar la Actualización parcial de Productos.", MsgBoxStyle.Information, "isMOBILE")
        End If
    End Sub

    Private Sub txtCodigoProducto_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCodigoProducto.TextChanged

    End Sub

End Class


