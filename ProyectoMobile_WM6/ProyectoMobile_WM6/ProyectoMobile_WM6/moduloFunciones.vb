﻿Imports System
Imports System.Data
Imports System.Data.SqlServerCe
Imports System.Data.SqlClient
Imports System.Xml
Imports System.IO

Module moduloFunciones

    Dim respuesta, nombreTabla, RegladeNegocio As String
    Dim fecha_Actualizacion, contador, descripcionDeposito, codigoDeposito, descripcionProveedor, codigoProveedorCompleto, codigo_productocompleto As String
    Dim comandoBD_PDA As New SqlCeCommand
    Dim comandoBD_SERVIDOR As New SqlCommand
    Dim dataSet_PDA As New Data.DataSet
    Dim num_UltimoDocumento, filaDataGrid, valorRegladeNegocio, nivelUsuario, numUltimaRecepcion, numUltimoTraslado As Integer

    Public ImpresoraDefault As ConexionImpresora

    Public Impresora_WLAN_IP_Input As String
    Public Impresora_WLAN_IP As System.Net.IPAddress
    Public Impresora_WLAN_Port As Integer

    Public Impresora_Bluetooth_NeedPortRegister As Boolean
    Public Impresora_Bluetooth_Address As String
    Public Impresora_Bluetooth_Port As String
    Public Impresora_Bluetooth_BaudRate As String

    Public ValidacionFacturaPOS_MensajeCorrectoLinea1 As String
    Public ValidacionFacturaPOS_MensajeCorrectoLinea2 As String
    Public ValidacionFacturaPOS_MensajeIncorrectoLinea1 As String
    Public ValidacionFacturaPOS_MensajeIncorrectoLinea2 As String
    Public ValidacionFacturaPOS_TiempoEscritura As Integer

    Public Etiqueta_Setup As String
    Public Hablador_Setup As String
    Public Etiqueta_Oferta_Setup As String
    Public Hablador_Oferta_Setup As String

    Dim dataSet_Servidor, dataset_RegladeNegocio, dataset_NivelUsuario, dataset_RecepcionesMovil_PDA As New Data.DataSet
    Dim nuevoAncho As Integer
    Dim numeroRecepcion_SERVIDORsinFormato, numeroFacturasinFormato As Integer
    Dim cantidadUnidadesRecibidasAnterior, cantidadTotalUnidadesRecibidas, cantidadEmpaquesRecibidosAnterior, cantidadTotalEmpaquesRecibidos, cantidadTotalRecibida As Double

    Dim dataset_Codigos, dataset_Productos As New Data.DataSet

    Dim UnidadesSolicitadas As Double
    Dim EmpaquesSolicitados As Double

    Public ImpHblEti_BusquedaActiva As Boolean
    Public FactorizarPrecios As Boolean

    Public FormDesignHeight As String
    Public FormDesignWidth As String
    Public CurrentScreenWidth As String
    Public CurrentScreenHeight As String

    Public CurrentHeightFactor As Double
    Public CurrentWidthFactor As Double
    Public CurrentFontFactor As Double

    Public RuntimeVSDesignRatioX As Double
    Public RuntimeVSDesignRatioY As Double

    Public DebugMode As Boolean

    Public UserMenuConfig As DataSet

    Function VerificarPDA() As Boolean

        Try

            Dim Serial As String
            Dim CantidadPDA As Integer
            Dim Adapter_Servidor As New SqlDataAdapter

            If conectarDB_SERVIDOR(rutaDB_SERVIDOR) = 0 Then
                MsgBox("No se pudo conectar con el servidor. Verifique la configuración e intente de nuevo más tarde.")
                Return False
            End If

            dataSet_Servidor = New DataSet

            comandoBD_SERVIDOR.CommandText = "SELECT TOP(1) SER_SOF FROM ESTRUC_SIS"
            comandoBD_SERVIDOR.Connection = conexionBD_SERVIDOR
            Adapter_Servidor.SelectCommand = comandoBD_SERVIDOR
            Adapter_Servidor.Fill(dataSet_Servidor)
            comandoBD_SERVIDOR.Parameters.Clear()

            Serial = dataSet_Servidor.Tables(0).Rows(0).Item("Ser_Sof")

            'CantidadPDA = Val(Mid(Serial, 7, 1))

            'comandoBD_SERVIDOR.CommandText = "DELETE FROM MA_PDA WHERE IDROW > ( " _
            '& "SELECT MAX(IDROW) FROM(" _
            '& "SELECT TOP (" & CantidadPDA & ") * FROM MA_PDA ORDER BY IDROW ASC) TB)"

            'dataSet_Servidor = executeQuery_Servidor(comandoBD_SERVIDOR)

            'comandoBD_SERVIDOR.CommandText = "SELECT COUNT(*) as Cuenta FROM MA_PDA"
            'dataSet_Servidor = executeQuery_Servidor(comandoBD_SERVIDOR)

            Dim NumPDA_Activos As Integer
            'NumPDA_Activos = dataSet_Servidor.Tables(0).Rows(0).Item("Cuenta")

            Dim idDispositivo As String
            idDispositivo = Registry.GetValue("HKEY_LOCAL_MACHINE\Ident", "OrigName", "Unknown")

            comandBD_SERVIDOR.CommandText = "SELECT CodigoPDA, ID_Dispositivo FROM MA_PDA WHERE ID_Dispositivo = @Disp"
            comandBD_SERVIDOR.Parameters.Add("@Disp", idDispositivo)

            dataSet_Servidor = executeQuery_Servidor(comandBD_SERVIDOR)

            If dataSet_Servidor.Tables(0).Rows.Count = 0 Then
                'El dispositivo es nuevo
            Else
                If dataSet_Servidor.Tables(0).Rows(0).Item("CodigoPDA") <> Id_PDA Then
                    MsgBox("Este dispositivo ya se encuentra asignado a otro ID_PDA." & vbNewLine & _
                           "Debe cambiar la configuración y reiniciar la aplicación.")
                    Return False
                End If
            End If

            dataSet_Servidor = New DataSet

            comandoBD_SERVIDOR.CommandText = "SELECT CodigoPDA, ID_Dispositivo, Localidad FROM MA_PDA Where CodigoPDA = @Dispositivo"
            comandoBD_SERVIDOR.Parameters.Add("@Dispositivo", Id_PDA)
            comandoBD_SERVIDOR.Connection = conexionBD_SERVIDOR
            Adapter_Servidor.SelectCommand = comandoBD_SERVIDOR
            Dim AutoUpdateDB As SqlCommandBuilder
            AutoUpdateDB = New SqlCommandBuilder(Adapter_Servidor)
            Adapter_Servidor.InsertCommand = AutoUpdateDB.GetInsertCommand
            Adapter_Servidor.UpdateCommand = AutoUpdateDB.GetUpdateCommand
            Adapter_Servidor.Fill(dataSet_Servidor)
            comandoBD_SERVIDOR.Parameters.Clear()

            If dataSet_Servidor.Tables(0).Rows.Count = 0 Then

                'If NumPDA_Activos < CantidadPDA Then

                Dim TempRow As DataRow = dataSet_Servidor.Tables(0).NewRow()
                TempRow.Item("CodigoPDA") = Id_PDA
                TempRow.Item("ID_Dispositivo") = idDispositivo
                TempRow.Item("Localidad") = Localidad
                dataSet_Servidor.Tables(0).Rows.Add(TempRow)

                Adapter_Servidor.Update(dataSet_Servidor)

                'Else

                'MsgBox("Usted ha excedido la Cantidad de PDA activos en su licencia." & vbNewLine _
                '& "Contacte a las oficinas de BIGWISE, si desea adquirir una nueva licencia.")
                'Return False

                'End If

            Else

                If dataSet_Servidor.Tables(0).Rows(0).Item("ID_Dispositivo") <> idDispositivo Then
                    MsgBox("El ID_PDA definido en el setup, no corresponde con el identificador del Dispositivo inicial. debe cambiar la configuración y reiniciar la aplicación.")
                    Return False
                End If

            End If

            VerificarPDA = True
            conexionBD_SERVIDOR.Close()

        Catch ex As Exception
            VerificarPDA = False
            MsgBox("Ha ocurrido un error, reporte lo siguiente: " & vbNewLine & ex.Message)
        End Try

    End Function

    Function Leer_Setup(ByVal rutaArchivoExe) As Boolean

        Dim Setup As New XmlDocument
        Dim Nodo_Setup As XmlNodeList
        Dim Servidor As String
        Dim Login As String
        Dim Password As String

        If Not (File.Exists(rutaArchivoExe + "SETUP.xml")) Then

            MsgBox("No se encontró el archivo setup.xml dentro de la ruta de la aplicación.", MsgBoxStyle.Information, "Setup")
            Return False

        Else

            Try

                Dim Ruta_Setup As New FileStream(rutaArchivoExe + "SETUP.xml", FileMode.Open, FileAccess.Read)
                Setup.Load(Ruta_Setup)

                Nodo_Setup = Setup.GetElementsByTagName("SERVER")
                Servidor = Nodo_Setup(0).ChildNodes.Item(0).InnerText.Trim()

                Nodo_Setup = Setup.GetElementsByTagName("LOGIN")
                Login = ValorSetup(Nodo_Setup, 0, "SA")

                Nodo_Setup = Setup.GetElementsByTagName("PASSWORD")
                Password = ValorSetup(Nodo_Setup, 0, String.Empty)

                rutaDB_SERVIDOR = "Data Source=" + Servidor + ";Initial Catalog=VAD10;" + _
                "User ID=" + Login + ";Password=" + Password + ";Persist Security Info=True"

                Nodo_Setup = Setup.GetElementsByTagName("MODO_OFFLINE")
                mFueraLineaSetup = ValorSetup(Nodo_Setup, 0, "-1")

                CompatibilidadFueraDeLinea = (Val(mFueraLineaSetup) = 1)

                Nodo_Setup = Setup.GetElementsByTagName("LOCALIDAD")
                Localidad = Nodo_Setup(0).ChildNodes.Item(0).InnerText.Trim()

                Nodo_Setup = Setup.GetElementsByTagName("PDA")
                Id_PDA = Nodo_Setup(0).ChildNodes.Item(0).InnerText.Trim()

                Nodo_Setup = Setup.GetElementsByTagName("ULTIMO_DOCUMENTO")
                num_UltimoDocumento = Nodo_Setup(0).ChildNodes.Item(0).InnerText.Trim()

                Nodo_Setup = Setup.GetElementsByTagName("ULTIMA_RECEPCION")
                numUltimaRecepcion = Nodo_Setup(0).ChildNodes.Item(0).InnerText.Trim()

                Nodo_Setup = Setup.GetElementsByTagName("ULTIMO_TRASLADO")
                numUltimoTraslado = Nodo_Setup(0).ChildNodes.Item(0).InnerText.Trim()

                Nodo_Setup = Setup.GetElementsByTagName("IP_DE_IMPRESORA_PORTATIL")
                Impresora_WLAN_IP_Input = ValorSetup(Nodo_Setup, 0, "").Trim()
                Impresora_WLAN_IP = ParseIP(Impresora_WLAN_IP_Input)

                Nodo_Setup = Setup.GetElementsByTagName("PUERTO_IMPRESORA")
                Impresora_WLAN_Port = CInt(Val(ValorSetup(Nodo_Setup, 0, "")))

                Nodo_Setup = Setup.GetElementsByTagName("ETIQUETA")
                Etiqueta_Setup = ValorSetup(Nodo_Setup, 0, "").Trim()

                Nodo_Setup = Setup.GetElementsByTagName("HABLADOR")
                Hablador_Setup = ValorSetup(Nodo_Setup, 0, "").Trim()

                Nodo_Setup = Setup.GetElementsByTagName("ETIQUETA_OFERTA")
                Etiqueta_Oferta_Setup = ValorSetup(Nodo_Setup, 0, "").Trim()

                If (String.IsNullOrEmpty(Etiqueta_Oferta_Setup)) Then
                    Etiqueta_Oferta_Setup = Etiqueta_Setup
                End If

                Nodo_Setup = Setup.GetElementsByTagName("HABLADOR_OFERTA")
                Hablador_Oferta_Setup = ValorSetup(Nodo_Setup, 0, "").Trim()

                If (String.IsNullOrEmpty(Hablador_Oferta_Setup)) Then
                    Hablador_Oferta_Setup = Hablador_Setup
                End If

                Nodo_Setup = Setup.GetElementsByTagName("IMPRESORA_BLUETOOTH_REGISTRAR_PUERTO")
                Impresora_Bluetooth_NeedPortRegister = CBool(Val(ValorSetup(Nodo_Setup, 0, "1")))

                Nodo_Setup = Setup.GetElementsByTagName("IMPRESORA_BLUETOOTH_ADDRESS")
                Impresora_Bluetooth_Address = ValorSetup(Nodo_Setup, 0, "").Trim()

                Nodo_Setup = Setup.GetElementsByTagName("PUERTO_IMPRESORA_BLUETOOTH")
                Impresora_Bluetooth_Port = ValorSetup(Nodo_Setup, 0, "").Trim()

                Nodo_Setup = Setup.GetElementsByTagName("BAUDRATE_IMPRESORA_BLUETOOTH")
                Impresora_Bluetooth_BaudRate = ValorSetup(Nodo_Setup, 0, "").Trim()

                Nodo_Setup = Setup.GetElementsByTagName("IMPRESORA_DEFAULT")
                ImpresoraDefault = CInt(ValidarNumeroIntervalo(Val(ValorSetup(Nodo_Setup, 0, "")), Val("1"), Val("0")))

                EstablecerImpresoraSeleccionada()

                Nodo_Setup = Setup.GetElementsByTagName("ValidacionFacturaPOS_TiempoEscritura")
                ValidacionFacturaPOS_TiempoEscritura = CInt(Val(ValorSetup(Nodo_Setup, 0, "0")))

                ValidacionFacturaPOS_TiempoEscritura = 150

                Nodo_Setup = Setup.GetElementsByTagName("ValidacionFacturaPOS_MensajeCorrectoLinea1")
                ValidacionFacturaPOS_MensajeCorrectoLinea1 = ValorSetup(Nodo_Setup, 0, "El documento").Trim()

                Nodo_Setup = Setup.GetElementsByTagName("ValidacionFacturaPOS_MensajeCorrectoLinea2")
                ValidacionFacturaPOS_MensajeCorrectoLinea2 = ValorSetup(Nodo_Setup, 0, "es válido").Trim()

                Nodo_Setup = Setup.GetElementsByTagName("ValidacionFacturaPOS_MensajeIncorrectoLinea1")
                ValidacionFacturaPOS_MensajeIncorrectoLinea1 = ValorSetup(Nodo_Setup, 0, "El documento").Trim()

                Nodo_Setup = Setup.GetElementsByTagName("ValidacionFacturaPOS_MensajeIncorrectoLinea2")
                ValidacionFacturaPOS_MensajeIncorrectoLinea2 = ValorSetup(Nodo_Setup, 0, "ya existe").Trim()

                ' Las variables a continuación son para acomodar el tamaño de la aplicación a la pantalla
                ' del dispositivo. Basicamente, se creará un ratio de los valores de tamaño del form
                ' a nivel de aplicación contra los valores de tamaño de la pantalla del dispositivo
                ' para crear ratios proporcionales.

                ' Para acomodar la pantalla a un dispositivo lo primero que se debe hacer es:

                ' Iniciar la aplicación sin definir estos valores:

                '-<Configuracion_Pantalla>
                '   <A></A> ' CurrentScreenHeight
                '   <B></B> ' CurrentScreenWidth
                '   <C></C> ' FormDesignHeight
                '   <D></D> ' FormDesignWidth
                '-</Configuracion_Pantalla>

                ' Determinar si la aplicación se ve bien o necesita mas o menos ancho y alto para ajustarse a la pantalla
                ' Por ejemplo caso de los Motorola MC3XXX

                ' La aplicación tiene todos sus formularios hechos en 240x300 (AnchoxAltura)

                ' Pero cuando se arranca isMobile a nivel de código en estos dispositivos
                ' Por alguna razon los formularios terminan como 320x400 (Si se fijan, tanto ancho como alto 
                ' se incrementaron en un 33% (1/3)

                ' Ahora, cuando nos vamos a System Info -> Display observamos que las resoluciones son:
                ' 320x320. Y efectivamente cuando se arranca a aplicación en el dispositivo con los valores nativos
                ' Se puede visualizar que a nivel de ancho esta perfecto, pero no se alcanza a ver todo el tamaño
                ' de la altura de la aplicación.

                ' Para solucionar esto colocamos la siguiente configuración:

                '-<Configuracion_Pantalla>
                '   <A>400</A>
                '   <B>320</B>
                '   <C>320</C>
                '   <D>320</D>
                '-</Configuracion_Pantalla>

                ' Esto creara un ratio de C / A (Altura) y D / B (Ancho).
                ' Ratio Altura = (320 / 400) = (4 / 5) = 0.8
                ' Ratio Ancho = (320 / 320) = (1 / 1) = 1.0

                ' Por consiguiente la rutina AjustarFormularios
                ' en este caso a cada formulario y sus controles
                ' le bajara la altura en un 80% para que Stellar
                ' MOBILE se muestre correctamente en ese dispositivo.

                ' Si se trata de instalar un PDA remoto no integrado
                ' Va a ser dificil adivinar los valores que tomó para
                ' FormDesignHeight y FormDesignWidth.
                ' Pero en ese caso la solución será ensayo y error 
                ' con los valores del setup que se indican a continuación:

                Nodo_Setup = Setup.GetElementsByTagName("Configuracion_Pantalla")

                FormDesignHeight = ValorSetup(Nodo_Setup, 0, "")
                FormDesignWidth = ValorSetup(Nodo_Setup, 1, "")
                CurrentScreenHeight = ValorSetup(Nodo_Setup, 2, "")
                CurrentScreenWidth = ValorSetup(Nodo_Setup, 3, "")

                If Not (IsNumeric(FormDesignHeight) Or IsNumeric(FormDesignWidth) Or IsNumeric(CurrentScreenHeight) Or IsNumeric(CurrentScreenWidth)) Then
                    RuntimeVSDesignRatioX = 1 : RuntimeVSDesignRatioY = 1
                Else
                    RuntimeVSDesignRatioX = (FormDesignWidth / 240) ' 240 = Px de Ancho en los que esta hecho el MOBILE.
                    RuntimeVSDesignRatioY = (FormDesignHeight / 300) ' 300 = Px de Altura en los que esta hecho el MOBILE.
                End If

                Return True

            Catch ex As XmlException
                MsgBox("Verifique su archivo Setup. " + ex.Message)
                Return False
            Catch Any As Exception
                MsgBox("Verifique su archivo Setup. " + Any.Message)
                Return False
            End Try

        End If

    End Function

    Public Function ValidarNumeroIntervalo(ByVal pValor As Object, Optional ByVal pMax As Object = Nothing, Optional ByVal pMin As Object = Nothing) As Object

        Try

            If Not (pMax Is Nothing) And Not (pMin Is Nothing) Then
                If pValor > pMax Then
                    pValor = pMax
                ElseIf pValor < pMin Then
                    pValor = pMin
                End If
            ElseIf Not (pMax Is Nothing) And (pMin Is Nothing) Then
                If pValor > pMax Then pValor = pMax
            ElseIf (pMax Is Nothing) And Not (pMin Is Nothing) Then
                If pValor < pMin Then pValor = pMin
            End If

            ValidarNumeroIntervalo = pValor

            Exit Function

        Catch
            ValidarNumeroIntervalo = pValor
        End Try

    End Function

    Sub EstablecerImpresoraSeleccionada()

        If (Impresora_WLAN_IP Is Nothing And Impresora_Bluetooth_Port = "") Then
            ImpresoraSeleccionada = ConexionImpresora.Impresora_WLAN
        ElseIf (Not Impresora_WLAN_IP Is Nothing And Impresora_Bluetooth_Port = "") Then
            ImpresoraSeleccionada = ConexionImpresora.Impresora_WLAN
        ElseIf (Impresora_WLAN_IP Is Nothing And Not Impresora_Bluetooth_Port = "") Then
            ImpresoraSeleccionada = ConexionImpresora.Impresora_Bluetooth
        ElseIf (Not Impresora_WLAN_IP Is Nothing And Not Impresora_Bluetooth_Port = "") Then
            ImpresoraSeleccionada = ImpresoraDefault
        End If

    End Sub

    Public Function ParseIP(ByVal pIPString) As System.Net.IPAddress
        Try
            ParseIP = System.Net.IPAddress.Parse(pIPString)
        Catch ex As Exception
            ParseIP = Nothing
        End Try
    End Function

    Sub inicializarPDA(ByVal rutaArchivoExe As String)

        FormBarraProgreso_Recepcion.lblBarraProgreso.Text = "Verificando conexión con el servidor..."
        FormBarraProgreso_Recepcion.Refresh()
        FormBarraProgreso_Recepcion.Update()

        Dim DB As Integer
        'Llama al procedimiento que crea la BD en el PDA
        DB = crearDB_PDA("Data Source= " + rutaArchivoExe + "isMOBILE.sdf")
        conectarDB_PDA("Data Source= " + rutaArchivoExe + "isMOBILE.sdf")
        'Verifico que tenga conexión con el servidor

        If conectarDB_SERVIDOR(rutaDB_SERVIDOR) = 0 Then
            MessageBox.Show("La aplicación no pudo inicializar, asegurese que el dispositivo tiene conexión con el servidor.")
            'Global.System.Windows.Forms.Application.Exit()
            ApplicationExit = True
            'FormBarraProgreso_Recepcion.Close()
            'formUsuario.Close()
        Else
            'Verifico que se creo la BD
            If (DB = 1) Then

                FormBarraProgreso_Recepcion.lblBarraProgreso.Text = "Creando tablas de la base de datos del dispositivo..."
                FormBarraProgreso_Recepcion.Refresh()
                FormBarraProgreso_Recepcion.Update()

                'Creación de tablas base de datos PDA
                comandoBD_PDA.CommandText = "CREATE TABLE MA_ELAB_INVENTARIO(F_INiCIO datetime NOT NULL, c_coddeposito nvarchar(10) NOT NULL, CU_CODDEPARTAMENTO nvarchar(100) NOT NULL, CU_CODGRUPO nvarchar(100) NOT NULL, CU_CODSUBGRUPO nvarchar(100) NOT NULL, CU_CODPROVEEDOR nvarchar(100) NOT NULL, no_elab int NOT NULL, CONSTRAINT PK_MA_ELAB_INVENTARIO PRIMARY KEY (c_coddeposito, no_elab))"
                ejecutaQuery_PDA(comandoBD_PDA)
                comandoBD_PDA.CommandText = "CREATE TABLE MA_DEPOSITO (c_coddeposito nvarchar(10) NOT NULL, c_descripcion nvarchar(40) NOT NULL, c_codlocalidad nvarchar(10) NOT NULL, CONSTRAINT PK_MA_DEPOSITO PRIMARY KEY (c_coddeposito))"
                ejecutaQuery_PDA(comandoBD_PDA)
                comandoBD_PDA.CommandText = "CREATE TABLE MA_USUARIOS (codusuario nvarchar(15) NOT NULL, password nvarchar(50) NOT NULL, login_name nvarchar(50) NOT NULL, nivel decimal (18,0) NOT NULL, clave nvarchar(20) NOT NULL, descripcion nvarchar(50) NOT NULL, CONSTRAINT PK_MA_USUARIOS PRIMARY KEY (codusuario))"
                ejecutaQuery_PDA(comandoBD_PDA)
                comandoBD_PDA.CommandText = "CREATE TABLE MA_CODIGOS (c_codnasa nvarchar(15) NOT NULL,c_codigo nvarchar(15) NOT NULL,c_descripcion nvarchar(20) NOT NULL, n_cantidad int NOT NULL, CONSTRAINT PK_MA_CODIGOS PRIMARY KEY (c_codnasa,c_codigo))"
                ejecutaQuery_PDA(comandoBD_PDA)
                comandoBD_PDA.CommandText = "CREATE TABLE MA_PRODUCTOS (C_CODIGO nvarchar(15) NOT NULL, C_DESCRI nvarchar(255) NOT NULL, c_departamento nvarchar(10) NOT NULL, c_grupo nvarchar(10) NOT NULL, c_subgrupo nvarchar(10) NOT NULL, n_costoact float NOT NULL, n_costoant float NOT NULL, n_costopro float NOT NULL, n_cantibul float NOT NULL, n_tipopeso int NOT NULL, cant_DECIMALES int NOT NULL, CONSTRAINT PK_MA_PRODUCTOS PRIMARY KEY (C_CODIGO))"
                ejecutaQuery_PDA(comandoBD_PDA)
                comandoBD_PDA.CommandText = "CREATE TABLE MA_CONFIGURACION (ID_PDA NVARCHAR(10) NOT NULL, ESTIMACION_INV INT NOT NULL, FECHAult_ACTUALIZACION NVARCHAR(50) NOT NULL, NUMultDOC INT NOT NULL, numUltimaRecepcion INT NOT NULL, numUltimoTraslado INT NOT NULL, CONSTRAINT PK_MA_CONFIGURACION PRIMARY KEY (ID_PDA))"
                ejecutaQuery_PDA(comandoBD_PDA)
                comandoBD_PDA.CommandText = "CREATE TABLE MA_INV_FISICO(c_DOCUMENTO nvarchar(15) NOT NULL, d_FECHA datetime NOT NULL, c_STATUS nvarchar(10) NOT NULL, c_CODLOCALIDAD nvarchar(10) NOT NULL, c_codMONEDA nvarchar(10) NOT NULL, n_FACTORCAMBIO float NOT NULL, c_OBSERVACION ntext NULL, c_CODCOMPRADOR nvarchar(15) NOT NULL, c_DEP_ORIG nvarchar(50) NOT NULL, c_MOTIVO nvarchar(50) NOT NULL, NO_ELAB int NOT NULL, CU_CODDEPARTAMENTO nvarchar(50) NOT NULL, CU_CODGRUPO nvarchar(50) NOT NULL, CU_CODSUBGRUPO nvarchar(50) NOT NULL, CU_CODPROVEEDOR nvarchar(50) NOT NULL, CONSTRAINT PK_MA_INV_FISICO PRIMARY KEY (c_DOCUMENTO, c_CODLOCALIDAD))"
                ejecutaQuery_PDA(comandoBD_PDA)
                comandoBD_PDA.CommandText = "CREATE TABLE TR_INV_FISICO(c_LINEA int NOT NULL, c_DOCUMENTO nvarchar(15) NOT NULL, c_DEPOSITO nvarchar(10) NOT NULL, c_CODARTICULO nvarchar(15) NOT NULL, n_CANTIDAD float NOT NULL, n_COSTO float NOT NULL, N_SUBTOTAL float NOT NULL, cs_codlocalidad nvarchar(50) NOT NULL, ns_CantidadEmpaque float NOT NULL, CONSTRAINT PK_TR_INV_FISICO PRIMARY KEY (c_LINEA, c_DOCUMENTO, c_CODARTICULO))"
                ejecutaQuery_PDA(comandoBD_PDA)
                comandoBD_PDA.CommandText = "CREATE TABLE MA_ENVIO_PENDIENTES(C_CONCEPTO nvarchar(10) NOT NULL, c_DOCUMENTO nvarchar(15) NOT NULL, c_DEPOSITO nvarchar(10) NOT NULL, CONSTRAINT PK_MA_ENVIO_PENDIENTES PRIMARY KEY (C_CONCEPTO, C_DOCUMENTO))"
                ejecutaQuery_PDA(comandoBD_PDA)
                comandoBD_PDA.CommandText = "CREATE TABLE MA_MONEDAS(c_codmoneda nvarchar(10) NOT NULL, n_factor float NOT NULL, n_decimales int NOT NULL, CONSTRAINT PK_MA_MONEDAS PRIMARY KEY (c_codmoneda))"
                ejecutaQuery_PDA(comandoBD_PDA)
                comandoBD_PDA.CommandText = "CREATE TABLE MA_PROVEEDORES (c_codproveed nvarchar(50) NOT NULL, c_descripcio nvarchar(255) NOT NULL, c_razon nvarchar(255) NOT NULL, CONSTRAINT PK_MA_PROVEEDORES PRIMARY KEY (c_codproveed))"
                ejecutaQuery_PDA(comandoBD_PDA)
                comandoBD_PDA.CommandText = "CREATE TABLE MA_ODC (c_documento nvarchar(15) NOT NULL, d_fecha datetime NOT NULL, d_fecha_recepcion datetime NOT NULL, c_status nvarchar(10) NOT NULL, c_codproveedor nvarchar(20) NOT NULL, c_codlocalidad nvarchar(10) NOT NULL, c_despachar nvarchar(10) NOT NULL, c_codmoneda nvarchar(10) NOT NULL, n_factorcambio float NOT NULL, CONSTRAINT PK_MA_ODC PRIMARY KEY (c_documento))"
                ejecutaQuery_PDA(comandoBD_PDA)
                comandoBD_PDA.CommandText = "CREATE TABLE MA_CORRELATIVOS (cu_campo nvarchar(50) NOT NULL, nu_valor numeric(18,0) NOT NULL, CONSTRAINT PK_MA_CORRELATIVOS PRIMARY KEY (cu_campo))"
                ejecutaQuery_PDA(comandoBD_PDA)
                comandoBD_PDA.CommandText = "CREATE TABLE MA_CORRELATIVOS_PDA (cu_campo nvarchar(50) NOT NULL, nu_valor numeric(18,0) NOT NULL, CONSTRAINT PK_MA_CORRELATIVOS_PDA PRIMARY KEY (cu_campo))"
                ejecutaQuery_PDA(comandoBD_PDA)
                comandoBD_PDA.CommandText = "CREATE TABLE MA_INVENTARIO (c_documento nvarchar(15) NOT NULL, c_codproveedor nvarchar(10) NOT NULL, c_factura nvarchar(15) NOT NULL, c_concepto nvarchar (10) NOT NULL, c_status nvarchar(10) NOT NULL, c_codlocalidad nvarchar(10) NOT NULL)"
                ejecutaQuery_PDA(comandoBD_PDA)
                comandoBD_PDA.CommandText = "CREATE TABLE TR_ODC (c_documento nvarchar(15) NOT NULL, c_codarticulo nvarchar(15) NOT NULL, c_linea int NOT NULL, n_cantidad float NOT NULL, ns_cantidadEmpaque float NOT NULL, cs_codlocalidad nvarchar (50) NOT NULL, n_cant_recibida float NOT NULL, n_costo float NOT NULL, n_subtotal float NOT NULL, n_impuesto float NOT NULL, n_total float NOT NULL, CONSTRAINT PK_TR_ODC PRIMARY KEY (C_DOCUMENTO, C_CODARTICULO, C_LINEA, CS_CODLOCALIDAD))"
                ejecutaQuery_PDA(comandoBD_PDA)
                comandoBD_PDA.CommandText = "CREATE TABLE MA_REGLASDENEGOCIO (campo nvarchar(50) NOT NULL, valor nvarchar (1024) NOT NULL, CONSTRAINT PK_MA_REGLASDENEGOCIO PRIMARY KEY (campo))"
                ejecutaQuery_PDA(comandoBD_PDA)
                comandoBD_PDA.CommandText = "CREATE TABLE MA_RECEPCIONESMOVIL (codigoProveedor nvarchar(50) NOT NULL, codigoODC nvarchar(50) NOT NULL, codigoProducto nvarchar(50) NOT NULL, cantidadUnidades float NOT NULL, cantidadEmpaques float NOT NULL, facturaUnidades float NOT NULL, facturaEmpaques float NOT NULL, transportista nvarchar(50) NOT NULL, codigoLocalidad nvarchar(50) NOT NULL, codigoDeposito nvarchar(50) NOT NULL, documento nvarchar(50) NOT NULL, numeroRecepcion_PDA nvarchar(50) NOT NULL, UnidadesxEmpaque float NOT NULL)"
                ejecutaQuery_PDA(comandoBD_PDA)
                comandoBD_PDA.CommandText = "CREATE TABLE TR_TRASLADOS_PDA (c_linea int NOT NULL, c_concepto nvarchar(10) NOT NULL, c_documento nvarchar(15) NOT NULL, c_deposito nvarchar(10) NOt NULL, c_codarticulo nvarchar(15) NOT NULL, n_cantidad Float NOT NULL, n_costo float NOT NULL, n_subtotal float NOT NULL, n_total float NOT NULL, c_tipomov nvarchar(10) NOT NULL, f_fecha datetime NOT NULL, c_codlocalidad nvarchar(10) NOT NULL, c_descripcion nvarchar(255) NOT NULL, cs_codlocalidad nvarchar(10) NOT NULL, ns_cantidadEmpaque float NOT NULL, CONSTRAINT PK_TR_TRASLADOS PRIMARY KEY (c_linea, c_documento, c_tipomov))"
                ejecutaQuery_PDA(comandoBD_PDA)
                comandoBD_PDA.CommandText = "CREATE TABLE MA_TRASLADOS_PDA (c_concepto nvarchar (10) NOT NULL, c_documento nvarchar(15) NOT NULL, d_fecha datetime NOT NULL, c_status nvarchar(10) NOT NULL, c_codlocalidad nvarchar(10) NOT NULL, c_codmoneda nvarchar(10) NOT NULL, n_factorcambio float NOT NULL, c_observacion ntext NOT NULL, c_codcomprador nvarchar(15) NOT NULL, c_dep_orig nvarchar(10) NOT NULL, c_dep_dest nvarchar(10), c_motivo nvarchar (50) NOT NULL, c_ejecutor nvarchar(255) NOT NULL, codconcepto numeric(18,0) NOT NULL)"
                ejecutaQuery_PDA(comandoBD_PDA)
                comandoBD_PDA.CommandText = "CREATE TABLE MA_CONCEPTOS (idconcepto numeric(18,0) NOT NULL, descripcion nvarchar(255)  NOT NULL, idproceso numeric(18,0) NOT NULL, CONSTRAINT PK_MA_CONCEPTOS PRIMARY KEY (idconcepto))"
                ejecutaQuery_PDA(comandoBD_PDA)
                comandoBD_PDA.CommandText = "CREATE TABLE MA_PROCESOS (idproceso numeric(18,0) NOT NULL, proceso nvarchar(255)  NOT NULL, CONSTRAINT PK_MA_CONCEPTOS PRIMARY KEY (idproceso))"
                ejecutaQuery_PDA(comandoBD_PDA)
                comandoBD_PDA.CommandText = "CREATE TABLE MA_DOCUMENTOS_VERIFICADOS (cu_DocumentoStellar nvarchar(20) NOT NULL, cu_DocumentoTipo nvarchar(20) NOT NULL, cu_Localidad nvarchar(20) NOT NULL, CONSTRAINT PK_MA_CONCEPTOS PRIMARY KEY (cu_DocumentoStellar, cu_DocumentoTipo))"
                ejecutaQuery_PDA(comandoBD_PDA)

                Dim TmpSQL As String = "CREATE TABLE CONF_MENU_USER (Clave_User nvarchar(50) NOT NULL, Clave_Menu nvarchar(50) NOT NULL, Texto nvarchar(50), Activado INT, Icono nvarchar(50), Forma nvarchar(255), Relacion nvarchar(50), ID INT Identity (1,1) NOT NULL, ResourceId nvarchar(50), CONSTRAINT PK_CONF_MENU_USER PRIMARY KEY (Clave_User, Clave_Menu))"
                comandoBD_PDA.CommandText = TmpSQL
                ejecutaQuery_PDA(comandoBD_PDA)

                FormBarraProgreso_Recepcion.ProgressBar_FormBarraProgreso.Value = 10

                fecha_Actualizacion = DateTime.Now.ToString

                Dim ModoOfflineSrv As Boolean = CBool(Val(obtenerRegladeNegocio_Servidor( _
                "PDA_ManejaDatosOffLine", "0")))

                If String.IsNullOrEmpty(mFueraLineaSetup) Or Val(mFueraLineaSetup) = -1 Then
                    CompatibilidadFueraDeLinea = ModoOfflineSrv
                Else
                    CompatibilidadFueraDeLinea = (Val(mFueraLineaSetup) = 1)
                End If

                'Llenado de tablas del PDA requeridas para iniciar aplicación

                FormBarraProgreso_Recepcion.lblBarraProgreso.Text = "Insertando registros de CONFIGURACIÓN."
                FormBarraProgreso_Recepcion.Refresh()
                FormBarraProgreso_Recepcion.Update()

                If IsDBNull(fecha_Actualizacion) Then fecha_Actualizacion = DateTime.Now.ToString
                If IsDBNull(num_UltimoDocumento) Then num_UltimoDocumento = 1
                If IsDBNull(numUltimaRecepcion) Then numUltimaRecepcion = 1
                If IsDBNull(numUltimoTraslado) Then numUltimoTraslado = 1

                actualizarMA_CONFIGURACION(fecha_Actualizacion, num_UltimoDocumento, numUltimaRecepcion, numUltimoTraslado)
                FormBarraProgreso_Recepcion.ProgressBar_FormBarraProgreso.Value = 5

                If CompatibilidadFueraDeLinea Then

                    FormBarraProgreso_Recepcion.lblBarraProgreso.Text = "Insertando registros de ODC."
                    FormBarraProgreso_Recepcion.Refresh()
                    FormBarraProgreso_Recepcion.Update()
                    actualizarMA_ODC()
                    FormBarraProgreso_Recepcion.ProgressBar_FormBarraProgreso.Value = 20

                    FormBarraProgreso_Recepcion.lblBarraProgreso.Text = "Insertando registros de INVENTARIO."
                    FormBarraProgreso_Recepcion.Refresh()
                    FormBarraProgreso_Recepcion.Update()
                    actualizarMA_ELAB_INVENTARIO()
                    FormBarraProgreso_Recepcion.ProgressBar_FormBarraProgreso.Value = 25

                    FormBarraProgreso_Recepcion.lblBarraProgreso.Text = "Insertando registros de DEPOSITOS."
                    FormBarraProgreso_Recepcion.Refresh()
                    FormBarraProgreso_Recepcion.Update()
                    actualizarMA_DEPOSITO()
                    FormBarraProgreso_Recepcion.ProgressBar_FormBarraProgreso.Value = 30

                    FormBarraProgreso_Recepcion.lblBarraProgreso.Text = "Insertando registros de USUARIOS."
                    FormBarraProgreso_Recepcion.Refresh()
                    FormBarraProgreso_Recepcion.Update()
                    actualizarMA_USUARIOS()
                    FormBarraProgreso_Recepcion.ProgressBar_FormBarraProgreso.Value = 35

                    FormBarraProgreso_Recepcion.lblBarraProgreso.Text = "Insertando registros de CODIGOS."
                    FormBarraProgreso_Recepcion.Refresh()
                    FormBarraProgreso_Recepcion.Update()
                    actualizarMA_CODIGOS()
                    FormBarraProgreso_Recepcion.ProgressBar_FormBarraProgreso.Value = 40

                    FormBarraProgreso_Recepcion.lblBarraProgreso.Text = "Insertando registros de PRODUCTOS."
                    FormBarraProgreso_Recepcion.Refresh()
                    FormBarraProgreso_Recepcion.Update()
                    actualizarMA_PRODUCTOS()
                    FormBarraProgreso_Recepcion.ProgressBar_FormBarraProgreso.Value = 60

                    FormBarraProgreso_Recepcion.lblBarraProgreso.Text = "Insertando registros de MONEDAS."
                    FormBarraProgreso_Recepcion.Refresh()
                    FormBarraProgreso_Recepcion.Update()
                    actualizarMA_MONEDAS()
                    FormBarraProgreso_Recepcion.ProgressBar_FormBarraProgreso.Value = 65

                    FormBarraProgreso_Recepcion.lblBarraProgreso.Text = "Insertando registros de PROVEEDORES."
                    FormBarraProgreso_Recepcion.Refresh()
                    FormBarraProgreso_Recepcion.Update()
                    actualizarMA_PROVEEDORES()
                    FormBarraProgreso_Recepcion.ProgressBar_FormBarraProgreso.Value = 70

                    FormBarraProgreso_Recepcion.lblBarraProgreso.Text = "Insertando registros de CORRELATIVOS."
                    FormBarraProgreso_Recepcion.Refresh()
                    FormBarraProgreso_Recepcion.Update()
                    actualizarMA_CORRELATIVOS()
                    FormBarraProgreso_Recepcion.ProgressBar_FormBarraProgreso.Value = 75

                    FormBarraProgreso_Recepcion.lblBarraProgreso.Text = "Insertando registros de ODC."
                    FormBarraProgreso_Recepcion.Refresh()
                    FormBarraProgreso_Recepcion.Update()
                    ActualizarTR_ODC()
                    FormBarraProgreso_Recepcion.ProgressBar_FormBarraProgreso.Value = 80

                    FormBarraProgreso_Recepcion.lblBarraProgreso.Text = "Insertando registros de REGLAS DE NEGOCIO."
                    FormBarraProgreso_Recepcion.Refresh()
                    FormBarraProgreso_Recepcion.Update()
                    actualizarMA_REGLASDENEGOCIO()
                    FormBarraProgreso_Recepcion.ProgressBar_FormBarraProgreso.Value = 85

                    'FormBarraProgreso_Recepcion.lblBarraProgreso.Text = "Insertando registros de INVENTARIO."
                    'FormBarraProgreso_Recepcion.Refresh()
                    'FormBarraProgreso_Recepcion.Update()
                    'actualizarMA_INVENTARIO()
                    'FormBarraProgreso_Recepcion.ProgressBar_FormBarraProgreso.Value = 90
                    ' Esto no es viable... tranca el inicio del PDA. Demasiados registros. Puesto a consultar al servidor.

                    actualizarMA_CONCEPTOS()

                    actualizarMA_PROCESOS()

                End If

                dataset_RegladeNegocio = consultarRegladeNegocio("VEN_FactorizarPrecios")

                If dataset_RegladeNegocio.Tables(0).Rows.Count > 0 Then
                    FactorizarPrecios = CBool(Val(dataset_RegladeNegocio.Tables(0).Rows(0)("Valor")))
                Else
                    FactorizarPrecios = False
                End If

                dataset_RegladeNegocio = consultarRegladeNegocio("Hbl_FactorizarPrecios")

                If dataset_RegladeNegocio.Tables(0).Rows.Count > 0 Then
                    Hablador_FactorizarPreciosMultiMoneda = CBool(Val(dataset_RegladeNegocio.Tables(0).Rows(0)("Valor")))
                Else
                    Hablador_FactorizarPreciosMultiMoneda = True
                End If

                cerrarConexion_SERVIDOR()

                FormBarraProgreso_Recepcion.ProgressBar_FormBarraProgreso.Value = 100

            End If

            End If

    End Sub

    Sub actualizarMA_CONFIGURACION(ByVal fecha_Actualizacion As String, ByVal num_UltimoDocumento As Integer, ByVal numUltimaRecepcion As Integer, ByVal numUltimoTraslado As Integer)

        'Se busca la información en el servidor y se inserta en el PDA
        Dim estimacion_INV As Integer
        SentenciaSQL = "SELECT ESTIMACION_INV FROM REGLAS_COMERCIALES"
        estimacion_INV = returnInteger_SERVIDOR(SentenciaSQL)
        comandoBD_PDA.CommandText = "INSERT INTO MA_CONFIGURACION  VALUES (@Id_PDA, @ESTIMACION_INV, @FECHAult_ACTUALIZACION, @NUMultDOC, @numUltimaRecepcion, @numUltimoTraslado)"
        comandoBD_PDA.Parameters.Add("@Id_PDA", Id_PDA)
        comandoBD_PDA.Parameters.Add("@ESTIMACION_INV", estimacion_INV)
        comandoBD_PDA.Parameters.Add("@FECHAult_ACTUALIZACION", fecha_Actualizacion)
        comandoBD_PDA.Parameters.Add("@NUMultDOC", num_UltimoDocumento)
        comandoBD_PDA.Parameters.Add("@numUltimaRecepcion", numUltimaRecepcion)
        comandoBD_PDA.Parameters.Add("@numUltimoTraslado", numUltimoTraslado)
        ejecutaQuery_PDA(comandoBD_PDA)

    End Sub

    Sub actualizarMA_ELAB_INVENTARIO()

        nombreTabla = "MA_ELAB_INVENTARIO"
        comandoBD_SERVIDOR.CommandText = "SELECT F_INiCIO, c_coddeposito, CU_CODDEPARTAMENTO, CU_CODGRUPO, CU_CODSUBGRUPO, CU_CODPROVEEDOR, no_elab FROM MA_ELAB_INVENTARIO where B_COMENZADO=1 and B_FINALIZADO=0"
        dataSet_Servidor = ejecutaQuery_Servidor(comandoBD_SERVIDOR, nombreTabla)
        insertarRegistros_MA_ELAB_INVENTARIO_PDA(dataSet_Servidor)
        FormActualizar_InventarioFisico.ProgressBar_FormActualizarInventarioFisico.Value = 60
    End Sub

    Sub actualizarMA_DEPOSITO()

        comandoBD_SERVIDOR.CommandText = "SELECT c_coddeposito, c_descripcion, c_codlocalidad FROM MA_DEPOSITO where c_codlocalidad= @localidad"
        comandoBD_SERVIDOR.Parameters.Add("@localidad", Localidad)
        nombreTabla = "MA_DEPOSITO"
        dataSet_Servidor = ejecutaQuery_Servidor(comandoBD_SERVIDOR, nombreTabla)
        insertarRegistros_MA_DEPOSITO_PDA(dataSet_Servidor)

    End Sub

    Sub actualizarMA_USUARIOS()

        comandoBD_SERVIDOR.CommandText = "SELECT codusuario, password, login_name, nivel, descripcion, clave FROM MA_USUARIOS WHERE localidad= @c_codlocalidad OR tipo_usuario=1"
        comandoBD_SERVIDOR.Parameters.Add("@c_codlocalidad", Localidad)
        nombreTabla = "MA_USUARIOS"
        dataSet_Servidor = ejecutaQuery_Servidor(comandoBD_SERVIDOR, nombreTabla)
        insertarRegistros_MA_USUARIOS_PDA(dataSet_Servidor)

    End Sub

    Sub actualizarCONF_MENU_USER(Optional ByVal Clave_User As String = "")

        comandoBD_SERVIDOR.Parameters.Clear()
        comandoBD_SERVIDOR.CommandText = "SELECT Clave_User, Clave_Menu, Texto, Activado, Icono, Forma, Relacion, ResourceId FROM CONF_MENU_USER"

        If Not String.IsNullOrEmpty(Clave_User) Then
            comandoBD_SERVIDOR.CommandText += " WHERE Clave_User = @Clave_User"
            comandoBD_SERVIDOR.Parameters.Add("@Clave_User", Clave_User)
        End If

        nombreTabla = "CONF_MENU_USER"

        dataSet_Servidor = ejecutaQuery_Servidor(comandoBD_SERVIDOR, nombreTabla)

        If DebugMode Then
            If dataSet_Servidor Is Nothing Then MsgBox("Error al obtener CONF_MENU_USER del servidor - Usuario -> " & Clave_User)
        End If

        insertarRegistros_CONF_MENU_USER_PDA(dataSet_Servidor, Clave_User)

    End Sub

    Sub actualizarMA_CODIGOS()

        'Debo crear tablas temporales por la gran cantidad de registros de MA_CODIGOS
        'Aseguro que no existe dicha tabla temporal
        nombreTabla = "TEMP_" + Id_PDA + "_MA_CODIGOS"
        verificarExisteTabla(nombreTabla)
        SentenciaSQL = "CREATE TABLE TEMP_" + Id_PDA + "_MA_CODIGOS (c_codnasa nvarchar(15) NOT NULL,c_codigo nvarchar(15) NOT NULL,c_descripcion nvarchar(20) NOT NULL, n_cantidad int NOT NULL, CONSTRAINT PK_TEMP_" + Id_PDA + "_MA_CODIGOS PRIMARY KEY (c_codnasa,c_codigo))"
        AccionesTablaTemporal(SentenciaSQL)
        'INSERTAR EN LA TABLA TEMPORAL TODOS LOS REGISTROS EN SU TOTALIDAD
        SentenciaSQL = "INSERT INTO TEMP_" + Id_PDA + "_MA_CODIGOS SELECT COD.c_codnasa, COD.c_codigo, COD.c_descripcion, COD.n_cantidad FROM MA_CODIGOS AS COD INNER JOIN MA_PRODUCTOS AS PRD ON PRD.C_CODIGO=COD.c_codnasa WHERE PRD.n_tipopeso NOT IN ('3','4') AND PRD.n_Activo = 1"
        AccionesTablaTemporal(SentenciaSQL)
        'Verificar la totalidad de registros para hacer el insert segmentado
        SentenciaSQL = "SELECT COUNT(*) FROM TEMP_" + Id_PDA + "_MA_CODIGOS"
        contador = (returnInteger_SERVIDOR(SentenciaSQL) / 1000) + 1
        'Bucle para hacer el insert segmentado
        For index As Integer = 1 To contador
            'INSERTANDO EN EL PDA 4000 REGISTROS
            comandoBD_SERVIDOR.CommandText = "SELECT TOP 1000 * FROM TEMP_" + Id_PDA + "_MA_CODIGOS ORDER BY c_codnasa"
            nombreTabla = "MA_CODIGOS"
            dataSet_Servidor = ejecutaQuery_Servidor(comandoBD_SERVIDOR, nombreTabla)
            insertarRegistros_MA_CODIGOS_PDA(dataSet_Servidor)
            'ELIMINANDO LOS REGISTRO YA INSERTADOS EN EL PDA
            SentenciaSQL = "DELETE TEMP FROM (SELECT TOP 1000 * FROM TEMP_" + Id_PDA + "_MA_CODIGOS ORDER BY c_codnasa) TEMP"
            AccionesTablaTemporal(SentenciaSQL)
        Next
        'Borrar del servidor la tabla temporal 
        SentenciaSQL = "DROP TABLE TEMP_" + Id_PDA + "_MA_CODIGOS"
        AccionesTablaTemporal(SentenciaSQL)

    End Sub

    Sub verificarExisteTabla(ByVal nombreTabla As String)

        comandoBD_SERVIDOR.CommandText = "SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE' AND TABLE_NAME = @nombreTabla"
        comandoBD_SERVIDOR.Parameters.Add("@nombreTabla", nombreTabla)
        dataSet_Servidor = ejecutaQuery_Servidor(comandoBD_SERVIDOR, nombreTabla)
        'Si existe la tabla procedo a eliminarla
        If dataSet_Servidor.Tables(nombreTabla).Rows.Count() = 1 Then
            SentenciaSQL = "drop table " + nombreTabla
            AccionesTablaTemporal(SentenciaSQL)
        End If

    End Sub

    Sub actualizarMA_PRODUCTOS()

        nombreTabla = "TEMP_" + Id_PDA + "_MA_PRODUCTOS"
        verificarExisteTabla(nombreTabla)
        SentenciaSQL = "CREATE TABLE TEMP_" + Id_PDA + "_MA_PRODUCTOS (C_CODIGO nvarchar(15) NOT NULL, C_DESCRI nvarchar(255) NOT NULL, c_departamento nvarchar(10) NOT NULL, c_grupo nvarchar(10) NOT NULL,	c_subgrupo nvarchar(10) NOT NULL, n_costoact float NOT NULL, n_costoant float NOT NULL, n_costopro float NOT NULL, n_cantibul float NOT NULL, n_tipopeso int NOT NULL, cant_DECIMALES int NOT NULL, CONSTRAINT PK_TEMP_" + Id_PDA + "_MA_PRODUCTOS PRIMARY KEY (C_CODIGO))"
        AccionesTablaTemporal(SentenciaSQL)
        SentenciaSQL = "INSERT INTO TEMP_" + Id_PDA + "_MA_PRODUCTOS SELECT C_CODIGO, C_DESCRI, c_departamento, c_grupo, c_subgrupo, n_costoact, n_costoant, n_costopro, n_cantibul, n_tipopeso, cant_DECIMALES FROM MA_PRODUCTOS WHERE n_tipopeso NOT IN ('3','4') AND n_Activo = 1"
        AccionesTablaTemporal(SentenciaSQL)
        SentenciaSQL = "SELECT COUNT(*) FROM TEMP_" + Id_PDA + "_MA_PRODUCTOS"
        contador = (returnInteger_SERVIDOR(SentenciaSQL) / 1000) + 1
        For index As Integer = 1 To contador
            comandoBD_SERVIDOR.CommandText = "SELECT TOP 1000 * FROM TEMP_" + Id_PDA + "_MA_PRODUCTOS ORDER BY C_CODIGO"
            nombreTabla = "MA_PRODUCTOS"
            dataSet_Servidor = ejecutaQuery_Servidor(comandoBD_SERVIDOR, nombreTabla)
            insertarRegistros_MA_PRODUCTOS_PDA(dataSet_Servidor)
            SentenciaSQL = "DELETE TEMP FROM (SELECT TOP 1000 * FROM TEMP_" + Id_PDA + "_MA_PRODUCTOS ORDER BY C_CODIGO) TEMP"
            AccionesTablaTemporal(SentenciaSQL)
        Next
        SentenciaSQL = "DROP TABLE TEMP_" + Id_PDA + "_MA_PRODUCTOS"
        AccionesTablaTemporal(SentenciaSQL)

    End Sub

    Sub actualizarMA_MONEDAS()

        comandoBD_SERVIDOR.CommandText = "SELECT c_codmoneda, n_factor, n_decimales FROM MA_MONEDAS WHERE b_preferencia=1"
        nombreTabla = "MA_MONEDAS"
        dataSet_Servidor = ejecutaQuery_Servidor(comandoBD_SERVIDOR, nombreTabla)
        insertarRegistros_MA_MONEDAS_PDA(dataSet_Servidor)

    End Sub

    Sub actualizarMA_PROVEEDORES()

        comandoBD_SERVIDOR.CommandText = "SELECT c_codproveed, c_descripcio, c_razon FROM MA_PROVEEDORES"
        nombreTabla = "MA_PROVEEDORES"
        dataSet_Servidor = ejecutaQuery_Servidor(comandoBD_SERVIDOR, nombreTabla)
        insertarRegistros_MA_PROVEEDORES_PDA(dataSet_Servidor)

    End Sub

    Sub actualizarMA_ODC()

        comandoBD_SERVIDOR.CommandText = "SELECT c_documento, d_fecha, d_fecha_recepcion, c_status, c_codproveedor, c_codlocalidad, c_despachar, c_codmoneda, n_factorcambio FROM MA_ODC WHERE c_codlocalidad = @localidad and c_status IN ('DPE')"
        comandoBD_SERVIDOR.Parameters.Add("@localidad", Localidad)
        nombreTabla = "MA_ODC"
        dataSet_Servidor = ejecutaQuery_Servidor(comandoBD_SERVIDOR, nombreTabla)
        insertarRegistros_MA_ODC_PDA(dataSet_Servidor)

    End Sub

    Public Function ActualizacionParcial(ByVal Proceso_Tabla As String, _
    Optional ByVal Codigo As String = "" _
    ) As Boolean

        ActualizacionParcial = False

        Try

            Select Case Proceso_Tabla

                Case Is = "ODC"

                    If Codigo = "" Then
                        comandoBD_SERVIDOR.CommandText = "SELECT ODC.c_documento, ODC.d_fecha, ODC.d_fecha_recepcion, ODC.c_status, ODC.c_codproveedor, ODC.c_codlocalidad, ODC.c_despachar, ODC.c_codmoneda, ODC.n_factorcambio FROM MA_ODC ODC INNER JOIN TR_PEND_PDA_ODC PEND ON ODC.C_DOCUMENTO = PEND.C_DOCUMENTO AND ODC.C_CODLOCALIDAD = PEND.C_LOCALIDAD WHERE PEND.C_LOCALIDAD = @LOCALIDAD AND PEND.ID_PDA = @dispositivo"
                        comandoBD_SERVIDOR.Parameters.Add("@localidad", Localidad)
                        comandoBD_SERVIDOR.Parameters.Add("@dispositivo", Id_PDA)
                    Else
                        comandoBD_SERVIDOR.CommandText = "SELECT ODC.c_documento, ODC.d_fecha, ODC.d_fecha_recepcion, ODC.c_status, ODC.c_codproveedor, ODC.c_codlocalidad, ODC.c_despachar, ODC.c_codmoneda, ODC.n_factorcambio FROM MA_ODC ODC WHERE ODC.C_DOCUMENTO = @DOC AND ODC.C_CODLOCALIDAD = @LOC"
                        comandoBD_SERVIDOR.Parameters.Add("@DOC", Codigo)
                        comandoBD_SERVIDOR.Parameters.Add("@LOC", Localidad)
                    End If

                    dataSet_Servidor = New DataSet

                    Dim Adapter_Servidor As New SqlDataAdapter

                    Try
                        comandoBD_SERVIDOR.Connection = conexionBD_SERVIDOR
                        Adapter_Servidor.SelectCommand = comandoBD_SERVIDOR

                        Adapter_Servidor.Fill(dataSet_Servidor)

                    Catch sqlEx As SqlException
                    Finally
                        comandoBD_SERVIDOR.Parameters.Clear()
                    End Try

                    For Each Record As Data.DataRow In dataSet_Servidor.Tables(0).Rows

                        comandoBD_PDA.CommandText = "SELECT * FROM MA_ODC WHERE C_DOCUMENTO = @DOC AND C_CODLOCALIDAD = @LOC"
                        comandoBD_PDA.Parameters.Add("@DOC", Record("C_DOCUMENTO"))
                        comandoBD_PDA.Parameters.Add("@LOC", Record("C_CODLOCALIDAD"))
                        dataSet_PDA = New DataSet
                        Dim Adapter_PDA As New SqlCeDataAdapter
                        Dim AutoUpdateDB As SqlCeCommandBuilder
                        Try

                            comandoBD_PDA.Connection = conexionBD_PDA
                            'el metodo SelectCommand de un dataAdapter contiene los datos o información del origen de datos
                            Adapter_PDA.SelectCommand = comandoBD_PDA
                            AutoUpdateDB = New SqlCeCommandBuilder(Adapter_PDA)
                            Adapter_PDA.InsertCommand = AutoUpdateDB.GetInsertCommand
                            Adapter_PDA.UpdateCommand = AutoUpdateDB.GetUpdateCommand
                            'el metodo fill se usa para rellenar un objeto DataSet con los resultados del elemento SelectCommand
                            Adapter_PDA.Fill(dataSet_PDA)


                        Catch sqlEx As SqlCeException
                        Finally
                            comandoBD_PDA.Parameters.Clear()
                        End Try

                        If dataSet_PDA.Tables(0).Rows.Count = 0 Then
                            Dim TempRow As DataRow = dataSet_PDA.Tables(0).NewRow()
                            For Each TempCol As DataColumn In dataSet_PDA.Tables(0).Columns
                                TempRow(TempCol.ColumnName) = Record(TempCol.ColumnName)
                            Next
                            dataSet_PDA.Tables(0).Rows.Add(TempRow)

                        Else
                            For Each TempCol As DataColumn In dataSet_PDA.Tables(0).Columns
                                dataSet_PDA.Tables(0).Rows(0)(TempCol.ColumnName) = Record(TempCol.ColumnName)
                            Next

                        End If

                        Adapter_PDA.Update(dataSet_PDA)

                    Next

                    'LISTO MA_ODC

                    dataSet_Servidor = New DataSet

                    If Codigo = "" Then
                        comandoBD_SERVIDOR.CommandText = "SELECT ODC.c_documento, ODC.c_codarticulo, ODC.c_linea, ODC.n_cantidad, ODC.ns_cantidadEmpaque, ODC.cs_codlocalidad, ODC.n_cant_recibida, ODC.n_costo, ODC.n_subtotal, ODC.n_impuesto, ODC.n_total FROM TR_ODC ODC INNER JOIN TR_PEND_PDA_ODC PEND ON ODC.C_DOCUMENTO = PEND.C_DOCUMENTO WHERE PEND.ID_PDA = @dispositivo"
                        comandoBD_SERVIDOR.Parameters.Add("@dispositivo", Id_PDA)
                    Else
                        comandoBD_SERVIDOR.CommandText = "SELECT ODC.c_documento, ODC.c_codarticulo, ODC.c_linea, ODC.n_cantidad, ODC.ns_cantidadEmpaque, ODC.cs_codlocalidad, ODC.n_cant_recibida, ODC.n_costo, ODC.n_subtotal, ODC.n_impuesto, ODC.n_total FROM TR_ODC ODC WHERE ODC.C_DOCUMENTO = @DOC"
                        comandoBD_SERVIDOR.Parameters.Add("@DOC", Codigo)
                    End If

                    Adapter_Servidor = New SqlDataAdapter

                    Try
                        comandoBD_SERVIDOR.Connection = conexionBD_SERVIDOR
                        Adapter_Servidor.SelectCommand = (comandoBD_SERVIDOR)
                        Adapter_Servidor.Fill(dataSet_Servidor)

                    Catch sqlEx As SqlException
                    Finally
                        comandoBD_SERVIDOR.Parameters.Clear()
                    End Try

                    For Each Record As Data.DataRow In dataSet_Servidor.Tables(0).Rows

                        comandoBD_PDA.CommandText = "SELECT * FROM TR_ODC WHERE C_DOCUMENTO = @DOC AND CS_CODLOCALIDAD = @LOC AND C_CODARTICULO = @PROD AND C_LINEA = @LINE"
                        comandoBD_PDA.Parameters.Add("@DOC", Record("C_DOCUMENTO"))
                        comandoBD_PDA.Parameters.Add("@LOC", Record("CS_CODLOCALIDAD"))
                        comandoBD_PDA.Parameters.Add("@PROD", Record("C_CODARTICULO"))
                        comandoBD_PDA.Parameters.Add("@LINE", Record("C_LINEA"))

                        dataSet_PDA = New DataSet
                        Dim Adapter_PDA As New SqlCeDataAdapter
                        Dim AutoUpdateDB As SqlCeCommandBuilder
                        Try

                            comandoBD_PDA.Connection = conexionBD_PDA
                            'el metodo SelectCommand de un dataAdapter contiene los datos o información del origen de datos
                            Adapter_PDA.SelectCommand = comandoBD_PDA
                            AutoUpdateDB = New SqlCeCommandBuilder(Adapter_PDA)
                            Adapter_PDA.InsertCommand = AutoUpdateDB.GetInsertCommand
                            Adapter_PDA.UpdateCommand = AutoUpdateDB.GetUpdateCommand
                            'el metodo fill se usa para rellenar un objeto DataSet con los resultados del elemento SelectCommand
                            Adapter_PDA.Fill(dataSet_PDA)


                        Catch sqlEx As SqlCeException
                        Finally
                            comandoBD_PDA.Parameters.Clear()
                        End Try

                        If dataSet_PDA.Tables(0).Rows.Count = 0 Then
                            Dim TempRow As DataRow = dataSet_PDA.Tables(0).NewRow()
                            For Each TempCol As DataColumn In dataSet_PDA.Tables(0).Columns
                                TempRow(TempCol.ColumnName) = Record(TempCol.ColumnName)
                            Next
                            dataSet_PDA.Tables(0).Rows.Add(TempRow)

                        Else
                            For Each TempCol As DataColumn In dataSet_PDA.Tables(0).Columns
                                dataSet_PDA.Tables(0).Rows(0)(TempCol.ColumnName) = Record(TempCol.ColumnName)
                            Next

                        End If

                        Adapter_PDA.Update(dataSet_PDA)

                        If Codigo = "" Then

                            comandoBD_SERVIDOR.CommandText = "DELETE FROM TR_PEND_PDA_ODC WHERE C_DOCUMENTO = @DOC AND ID_PDA = @DISP"
                            comandoBD_SERVIDOR.Parameters.Add("@DOC", Record("C_DOCUMENTO"))
                            comandoBD_SERVIDOR.Parameters.Add("@DISP", Id_PDA)

                            executeQuery_Servidor(comandoBD_SERVIDOR)

                        End If

                    Next

                    'LISTO TR_ODC

                    ActualizacionParcial = True

                Case Is = "Productos"

                    comandoBD_SERVIDOR.CommandText = "SELECT C_CODIGO, TIPO_CAMBIO FROM" _
                    & " TR_PEND_PDA_PRODUCTOS Pend WHERE Pend.ID_PDA = @Dispositivo" _
                    & " ORDER BY Pend.ID ASC"
                    comandoBD_SERVIDOR.Parameters.Add("@Dispositivo", Id_PDA)

                    dataSet_Servidor = New DataSet

                    Dim Adapter_Servidor As New SqlDataAdapter

                    Try
                        comandoBD_SERVIDOR.Connection = conexionBD_SERVIDOR
                        Adapter_Servidor.SelectCommand = comandoBD_SERVIDOR

                        Adapter_Servidor.Fill(dataSet_Servidor)

                    Catch sqlEx As SqlException
                    Finally
                        comandoBD_SERVIDOR.Parameters.Clear()
                    End Try

                    For Each Record As Data.DataRow In dataSet_Servidor.Tables(0).Rows

                        If Record("TIPO_CAMBIO") = 1 Or Record("TIPO_CAMBIO") = 10 Then

                            comandoBD_PDA.CommandText = "DELETE FROM MA_PRODUCTOS WHERE" _
                            & " C_CODIGO = @COD"
                            comandoBD_PDA.Parameters.Add("@COD", Record("C_CODIGO"))
                            ejecutaQuery_PDA(comandoBD_PDA)

                        ElseIf Record("TIPO_CAMBIO") = 0 Then

                            Dim Dataset_Temp As New DataSet

                            comandBD_SERVIDOR.CommandText = "SELECT * FROM MA_PRODUCTOS WHERE C_CODIGO = @COD"
                            comandBD_SERVIDOR.Parameters.Add("@COD", Record("C_CODIGO"))

                            Dataset_Temp = executeQuery_Servidor(comandBD_SERVIDOR)

                            comandoBD_PDA.CommandText = "SELECT * FROM MA_PRODUCTOS WHERE C_CODIGO = @COD"
                            comandoBD_PDA.Parameters.Add("@COD", Record("C_CODIGO"))

                            dataSet_PDA = New DataSet
                            Dim Adapter_PDA As New SqlCeDataAdapter
                            Dim AutoUpdateDB As SqlCeCommandBuilder
                            Try

                                comandoBD_PDA.Connection = conexionBD_PDA
                                'el metodo SelectCommand de un dataAdapter contiene los datos o información del origen de datos
                                Adapter_PDA.SelectCommand = comandoBD_PDA
                                AutoUpdateDB = New SqlCeCommandBuilder(Adapter_PDA)
                                Adapter_PDA.InsertCommand = AutoUpdateDB.GetInsertCommand
                                Adapter_PDA.UpdateCommand = AutoUpdateDB.GetUpdateCommand
                                'el metodo fill se usa para rellenar un objeto DataSet con los resultados del elemento SelectCommand
                                Adapter_PDA.Fill(dataSet_PDA)


                            Catch sqlEx As SqlCeException
                            Finally
                                comandoBD_PDA.Parameters.Clear()
                            End Try

                            If Dataset_Temp.Tables(0).Rows.Count <> 0 Then

                                If dataSet_PDA.Tables(0).Rows.Count = 0 Then

                                    Dim TempRow As DataRow = dataSet_PDA.Tables(0).NewRow()

                                    For Each TempCol As DataColumn In dataSet_PDA.Tables(0).Columns

                                        'If TempCol.ColumnName <> "TIPO_CAMBIO" Then _
                                        TempRow(TempCol.ColumnName) = Dataset_Temp.Tables(0).Rows(0)(TempCol.ColumnName)

                                    Next

                                    dataSet_PDA.Tables(0).Rows.Add(TempRow)

                                Else

                                    For Each TempCol As DataColumn In dataSet_PDA.Tables(0).Columns
                                        dataSet_PDA.Tables(0).Rows(0)(TempCol.ColumnName) = Dataset_Temp.Tables(0).Rows(0)(TempCol.ColumnName)
                                    Next

                                End If

                                Adapter_PDA.Update(dataSet_PDA)

                            End If

                        End If

                        '______ Eliminar registro en Pendientes ______'

                        comandoBD_SERVIDOR.CommandText = "DELETE FROM TR_PEND_PDA_PRODUCTOS" _
                        & " WHERE C_CODIGO = @Producto AND ID_PDA = @Dispositivo"
                        comandoBD_SERVIDOR.Parameters.Add("@Producto", Record("C_CODIGO"))
                        comandoBD_SERVIDOR.Parameters.Add("@Dispositivo", Id_PDA)

                        executeQuery_Servidor(comandoBD_SERVIDOR)

                    Next

                    '_____ LISTO MA_PRODUCTOS _____'

                    dataSet_Servidor = New DataSet

                    comandoBD_SERVIDOR.CommandText = "SELECT C_CODNASA, C_CODIGO, TIPO_CAMBIO" _
                    & " FROM TR_PEND_PDA_CODIGOS Pend WHERE Pend.ID_PDA = @Dispositivo" _
                    & " ORDER BY Pend.ID ASC"
                    comandoBD_SERVIDOR.Parameters.Add("@Dispositivo", Id_PDA)

                    Adapter_Servidor = New SqlDataAdapter

                    Try
                        comandoBD_SERVIDOR.Connection = conexionBD_SERVIDOR
                        Adapter_Servidor.SelectCommand = (comandoBD_SERVIDOR)
                        Adapter_Servidor.Fill(dataSet_Servidor)

                    Catch sqlEx As SqlException
                    Finally
                        comandoBD_SERVIDOR.Parameters.Clear()
                    End Try

                    For Each Record As Data.DataRow In dataSet_Servidor.Tables(0).Rows

                        If Record("TIPO_CAMBIO") = 1 Or Record("TIPO_CAMBIO") = 10 Then

                            comandBD_PDA.CommandText = "DELETE FROM MA_CODIGOS" _
                                    & " WHERE C_CODNASA = @CodPrincipal AND C_CODIGO = @CodAlterno"
                            comandBD_PDA.Parameters.Add("@CodPrincipal", Record("C_CODNASA"))
                            comandBD_PDA.Parameters.Add("@CodAlterno", Record("C_CODIGO"))
                            ejecutaQuery_PDA(comandBD_PDA)

                        ElseIf Record("TIPO_CAMBIO") = 0 Then

                            Dim Dataset_Temp As New DataSet

                            comandBD_SERVIDOR.CommandText = "SELECT * FROM MA_CODIGOS" _
                            & " WHERE C_CODNASA = @CodPrincipal AND C_CODIGO = @CodAlterno"
                            comandBD_SERVIDOR.Parameters.Add("@COD", Record("C_CODIGO"))
                            comandBD_SERVIDOR.Parameters.Add("@CodPrincipal", Record("C_CODNASA"))
                            comandBD_SERVIDOR.Parameters.Add("@CodAlterno", Record("C_CODIGO"))

                            Dataset_Temp = executeQuery_Servidor(comandBD_SERVIDOR)

                            comandoBD_PDA.CommandText = "SELECT * FROM MA_CODIGOS" _
                            & " WHERE C_CODNASA = @CodPrincipal AND C_CODIGO = @CodAlterno"
                            comandoBD_PDA.Parameters.Add("@CodPrincipal", Record("C_CODNASA"))
                            comandoBD_PDA.Parameters.Add("@CodAlterno", Record("C_CODIGO"))

                            dataSet_PDA = New DataSet
                            Dim Adapter_PDA As New SqlCeDataAdapter
                            Dim AutoUpdateDB As SqlCeCommandBuilder
                            Try

                                comandoBD_PDA.Connection = conexionBD_PDA
                                'el metodo SelectCommand de un dataAdapter contiene los datos o información del origen de datos
                                Adapter_PDA.SelectCommand = comandoBD_PDA
                                AutoUpdateDB = New SqlCeCommandBuilder(Adapter_PDA)
                                Adapter_PDA.InsertCommand = AutoUpdateDB.GetInsertCommand
                                Adapter_PDA.UpdateCommand = AutoUpdateDB.GetUpdateCommand
                                'el metodo fill se usa para rellenar un objeto DataSet con los resultados del elemento SelectCommand
                                Adapter_PDA.Fill(dataSet_PDA)


                            Catch sqlEx As SqlCeException
                            Finally
                                comandoBD_PDA.Parameters.Clear()
                            End Try

                            If Dataset_Temp.Tables(0).Rows.Count <> 0 Then

                                If dataSet_PDA.Tables(0).Rows.Count = 0 Then

                                    Dim TempRow As DataRow = dataSet_PDA.Tables(0).NewRow()

                                    For Each TempCol As DataColumn In dataSet_PDA.Tables(0).Columns
                                        TempRow(TempCol.ColumnName) = Dataset_Temp.Tables(0).Rows(0)(TempCol.ColumnName)
                                    Next

                                    dataSet_PDA.Tables(0).Rows.Add(TempRow)

                                Else

                                    For Each TempCol As DataColumn In dataSet_PDA.Tables(0).Columns
                                        'If TempCol.ColumnName <> "TIPO_CAMBIO" Then _
                                        dataSet_PDA.Tables(0).Rows(0)(TempCol.ColumnName) = Dataset_Temp.Tables(0).Rows(0)(TempCol.ColumnName)
                                    Next

                                End If

                                Adapter_PDA.Update(dataSet_PDA)

                            End If

                        End If

                        '______ Eliminar registro en Pendientes ______'

                        comandoBD_SERVIDOR.CommandText = "DELETE FROM TR_PEND_PDA_CODIGOS" _
                        & " WHERE C_CODNASA = @CodPrincipal AND C_CODIGO = @CodAlterno AND" _
                        & " ID_PDA = @Dispositivo"
                        comandoBD_SERVIDOR.Parameters.Add("@CodPrincipal", Record("C_CODNASA"))
                        comandoBD_SERVIDOR.Parameters.Add("@CodAlterno", Record("C_CODIGO"))
                        comandoBD_SERVIDOR.Parameters.Add("@Dispositivo", Id_PDA)

                        executeQuery_Servidor(comandoBD_SERVIDOR)

                    Next

                    'LISTO MA_CODIGOS

                    ActualizacionParcial = True

                Case Else

                    ActualizacionParcial = True

            End Select

        Catch ex As Exception
            ActualizacionParcial = False
        End Try

    End Function

    Sub actualizarMA_CORRELATIVOS()

        comandoBD_SERVIDOR.CommandText = "SELECT cu_campo, nu_valor FROM MA_CORRELATIVOS"
        nombreTabla = "MA_CORRELATIVOS"
        dataSet_Servidor = ejecutaQuery_Servidor(comandoBD_SERVIDOR, nombreTabla)
        insertarRegistros_MA_CORRELATIVOS_PDA(dataSet_Servidor)

    End Sub

    Sub actualizarMA_INVENTARIO()

        comandoBD_SERVIDOR.CommandText = "SELECT c_documento, c_codproveedor, c_factura, c_concepto, c_status, c_codlocalidad FROM MA_INVENTARIO WHERE c_codlocalidad = @localidad AND c_concepto = 'REC'"
        comandoBD_SERVIDOR.Parameters.Add("@localidad", Localidad)
        nombreTabla = "MA_INVENTARIO"
        dataSet_Servidor = ejecutaQuery_Servidor(comandoBD_SERVIDOR, nombreTabla)
        insertarRegistros_MA_INVENTARIO_PDA(dataSet_Servidor)

    End Sub

    Sub actualizarMA_CONCEPTOS()

        comandoBD_SERVIDOR.CommandText = "SELECT idconcepto, descripcion, idproceso FROM MA_CONCEPTOS"
        nombreTabla = "MA_CONCEPTOS"
        dataSet_Servidor = ejecutaQuery_Servidor(comandoBD_SERVIDOR, nombreTabla)
        insertarRegistros_MA_CONCEPTOS(dataSet_Servidor)

    End Sub

    Sub actualizarMA_PROCESOS()

        comandoBD_SERVIDOR.CommandText = "SELECT idproceso, proceso FROM MA_PROCESOS"
        nombreTabla = "MA_PROCESOS"
        dataSet_Servidor = ejecutaQuery_Servidor(comandoBD_SERVIDOR, nombreTabla)
        insertarRegistros_MA_PROCESOS(dataSet_Servidor)

    End Sub

    Sub ActualizarTR_ODC()

        'comandoBD_SERVIDOR.CommandText = "SELECT c_documento, c_codarticulo, c_linea, n_cantidad, ns_cantidadEmpaque, cs_codlocalidad, n_cant_recibida, n_costo, n_subtotal, n_impuesto, n_total FROM TR_ODC WHERE cs_codlocalidad = @localidad"
        comandoBD_SERVIDOR.CommandText = "SELECT TR_ODC.c_DOCUMENTO, TR_ODC.c_codarticulo, TR_ODC.c_linea, " _
        & "TR_ODC.n_cantidad, TR_ODC.ns_cantidadEmpaque, TR_ODC.cs_codlocalidad, " _
        & "TR_ODC.n_cant_recibida, TR_ODC.n_costo, TR_ODC.n_subtotal, " _
        & "TR_ODC.n_impuesto, TR_ODC.n_total " _
        & "FROM TR_ODC INNER JOIN MA_ODC ON TR_ODC.c_DOCUMENTO = MA_ODC.c_DOCUMENTO " _
        & "WHERE MA_ODC.c_status IN ('DPE') AND MA_ODC.c_CODLOCALIDAD = @localidad"
        comandoBD_SERVIDOR.Parameters.Add("@localidad", Localidad)
        dataSet_Servidor = executeQuery_Servidor(comandoBD_SERVIDOR)
        insertarRegistros_TR_ODC(dataSet_Servidor)

    End Sub

    Sub actualizarMA_REGLASDENEGOCIO()

        comandoBD_SERVIDOR.CommandText = "SELECT campo, valor FROM MA_REGLASDENEGOCIO"
        nombreTabla = "MA_REGLASDENEGOCIO"
        dataSet_Servidor = ejecutaQuery_Servidor(comandoBD_SERVIDOR, nombreTabla)
        insertarRegistros_MA_REGLASDENEGOCIO_PDA(dataSet_Servidor)

    End Sub

    Sub ActualizarEsquema_BD_PDA()

        comandBD_PDA.Parameters.Clear()
        comandoBD_PDA.CommandText = "SELECT Count(*) FROM MA_DOCUMENTOS_VERIFICADOS"
        If (CInt(GetScalar_PDA(comandoBD_PDA, (-1), (-1))) = (-1)) Then
            comandoBD_PDA.CommandText = "CREATE TABLE MA_DOCUMENTOS_VERIFICADOS (cu_DocumentoStellar nvarchar(20) NOT NULL, cu_DocumentoTipo nvarchar(20) NOT NULL, cu_Localidad nvarchar(20) NOT NULL, CONSTRAINT PK_MA_CONCEPTOS PRIMARY KEY (cu_DocumentoStellar, cu_DocumentoTipo))"
            ejecutaQuery_PDA(comandoBD_PDA, False)
        End If

        comandBD_PDA.Parameters.Clear()
        comandoBD_PDA.CommandText = "SELECT Count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'MA_INV_FISICO' AND COLUMN_NAME = 'CU_CODDEPARTAMENTO'"
        If (CInt(GetScalar_PDA(comandoBD_PDA, (-1), (-1))) = (-1)) Then
            comandoBD_PDA.CommandText = "ALTER TABLE MA_INV_FISICO ADD CU_CODDEPARTAMENTO nvarchar(50) NOT NULL DEFAULT ('')"
            ejecutaQuery_PDA(comandoBD_PDA, False)
        End If

        comandBD_PDA.Parameters.Clear()
        comandoBD_PDA.CommandText = "SELECT Count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'MA_INV_FISICO' AND COLUMN_NAME = 'CU_CODGRUPO'"
        If (CInt(GetScalar_PDA(comandoBD_PDA, (-1), (-1))) = (-1)) Then
            comandoBD_PDA.CommandText = "ALTER TABLE MA_INV_FISICO ADD CU_CODGRUPO nvarchar(50) NOT NULL DEFAULT ('')"
            ejecutaQuery_PDA(comandoBD_PDA, False)
        End If

        comandBD_PDA.Parameters.Clear()
        comandoBD_PDA.CommandText = "SELECT Count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'MA_INV_FISICO' AND COLUMN_NAME = 'CU_CODSUBGRUPO'"
        If (CInt(GetScalar_PDA(comandoBD_PDA, (-1), (-1))) = (-1)) Then
            comandoBD_PDA.CommandText = "ALTER TABLE MA_INV_FISICO ADD CU_CODSUBGRUPO nvarchar(50) NOT NULL DEFAULT ('')"
            ejecutaQuery_PDA(comandoBD_PDA, False)
        End If

        comandBD_PDA.Parameters.Clear()
        comandoBD_PDA.CommandText = "SELECT Count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'MA_INV_FISICO' AND COLUMN_NAME = 'CU_CODPROVEEDOR'"
        If (CInt(GetScalar_PDA(comandoBD_PDA, (-1), (-1))) = (-1)) Then
            comandoBD_PDA.CommandText = "ALTER TABLE MA_INV_FISICO ADD CU_CODPROVEEDOR nvarchar(50) NOT NULL DEFAULT ('')"
            ejecutaQuery_PDA(comandoBD_PDA, False)
        End If

        comandBD_PDA.Parameters.Clear()
        comandoBD_PDA.CommandText = "SELECT CONSTRAINT_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE TABLE_NAME = 'MA_ENVIO_PENDIENTES' AND CONSTRAINT_TYPE = 'PRIMARY KEY'"
        Dim MA_ENVIO_PENDIENTES_PKEY_CONSTRAINT As String = CStr(GetScalar_PDA(comandoBD_PDA, "", ""))
        If (MA_ENVIO_PENDIENTES_PKEY_CONSTRAINT = "") Then
            comandoBD_PDA.CommandText = "ALTER TABLE MA_ENVIO_PENDIENTES ADD PRIMARY KEY (C_CONCEPTO, C_DOCUMENTO)"
            ejecutaQuery_PDA(comandoBD_PDA, False)
        Else
            comandoBD_PDA.CommandText = "SELECT COUNT(*) FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE TABLE_NAME = 'MA_ENVIO_PENDIENTES' AND CONSTRAINT_NAME = '" + MA_ENVIO_PENDIENTES_PKEY_CONSTRAINT + "'"
            Dim MA_ENVIO_PENDIENTES_PKEY_MEMBERS As Integer = CInt(GetScalar_PDA(comandoBD_PDA, 0, 0))
            If (MA_ENVIO_PENDIENTES_PKEY_MEMBERS <> 2) Then
                comandoBD_PDA.CommandText = "ALTER TABLE MA_ENVIO_PENDIENTES DROP CONSTRAINT " + MA_ENVIO_PENDIENTES_PKEY_CONSTRAINT
                ejecutaQuery_PDA(comandoBD_PDA, False)
                comandoBD_PDA.CommandText = "ALTER TABLE MA_ENVIO_PENDIENTES ADD PRIMARY KEY (C_CONCEPTO, C_DOCUMENTO)"
                ejecutaQuery_PDA(comandoBD_PDA, False)
            End If
        End If

        Dim TmpSQL As String

        TmpSQL = "SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'CONF_MENU_USER'"

        comandoBD_PDA.Parameters.Clear()
        comandoBD_PDA.CommandText = TmpSQL

        If GetScalar_PDA(comandoBD_PDA, 0, 0) = 0 Then
            TmpSQL = "CREATE TABLE CONF_MENU_USER (Clave_User nvarchar(50) NOT NULL, Clave_Menu nvarchar(50) NOT NULL, Texto nvarchar(50), Activado INT, Icono nvarchar(50), Forma nvarchar(255), Relacion nvarchar(50), ID INT Identity (1,1) NOT NULL, ResourceId nvarchar(50), CONSTRAINT PK_CONF_MENU_USER PRIMARY KEY (Clave_User, Clave_Menu))"
            comandoBD_PDA.Parameters.Clear()
            comandoBD_PDA.CommandText = TmpSQL
            ejecutaQuery_PDA(comandoBD_PDA)
        End If

        TmpSQL = "SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'MA_CORRELATIVOS_PDA'"

        comandoBD_PDA.Parameters.Clear()
        comandoBD_PDA.CommandText = TmpSQL

        If GetScalar_PDA(comandoBD_PDA, 0, 0) = 0 Then
            TmpSQL = "CREATE TABLE MA_CORRELATIVOS_PDA (cu_campo nvarchar(50) NOT NULL, nu_valor numeric(18,0) NOT NULL, CONSTRAINT PK_MA_CORRELATIVOS_PDA PRIMARY KEY (cu_campo))"
            comandoBD_PDA.Parameters.Clear()
            comandoBD_PDA.CommandText = TmpSQL
            ejecutaQuery_PDA(comandoBD_PDA)
        End If

    End Sub

    Sub actualizacionAutomatica(ByVal rutaArchivoExe As String)

        FormBarraProgreso_Recepcion.lblBarraProgreso.Text = "Verificando conexión con el servidor..."
        FormBarraProgreso_Recepcion.lblBarraProgreso.Refresh()
        FormBarraProgreso_Recepcion.lblBarraProgreso.Update()

        conectarDB_PDA("Data Source= " + rutaArchivoExe + "isMOBILE.sdf")

        ActualizarEsquema_BD_PDA()

        'Verifico que tenga conexión con el servidor

        If conectarDB_SERVIDOR(rutaDB_SERVIDOR) = 0 Then

            If CompatibilidadFueraDeLinea Then
                MessageBox.Show("La aplicación trabajará sin conexión para todos los procesos, asegurese de establecer conexión con el servidor si desea actualizar y / o enviar información a la base de datos del mismo.")
                Inicio_Conectado = True
            Else
                MessageBox.Show("No hay conexión con el servidor. no se puede iniciar la aplicación.")
            End If

        Else

            Inicio_Conectado = True

            SentenciaSQL = "SELECT FECHAult_ACTUALIZACION FROM MA_CONFIGURACION"
            fecha_Actualizacion = returnString_PDA(SentenciaSQL)
            SentenciaSQL = "SELECT NUMultDOC FROM MA_CONFIGURACION"
            num_UltimoDocumento = returnInteger_PDA(SentenciaSQL)

            Dim ModoOfflineSrv As Boolean = CBool(Val(obtenerRegladeNegocio_Servidor("PDA_ManejaDatosOffLine", "0")))

            If String.IsNullOrEmpty(mFueraLineaSetup) Or Val(mFueraLineaSetup) = -1 Then
                CompatibilidadFueraDeLinea = ModoOfflineSrv
            Else
                CompatibilidadFueraDeLinea = (Val(mFueraLineaSetup) = 1)
            End If

            'La actualizacion automatica solo aplicara para las tablas: Usuarios, Depositos, Monedas, Configuación, Proveedores, ODC, Correlativos, Inventario
            'y consistira en borrar los registros existentes y llenarlas nuevamente 

            'NO DEBERIA ACTUALIZAR MA_CONFIGURACION para que no me coloque en el numero de la ultima recepcion siempre el mismo que se coloco la primera vez que se 
            'ejecuto la aplicacion. Recordar actualizar el campo numUltimaRecepcion cada vez que se finalice una recepcion con el PDA. La unica forma de actualizar 
            'la tabla MA_CONFIGURACION cada vez que se inicia la aplicacion es que se encuentre una forma de modificar el valor de numUltimaRecepcion en el setup y en lugar
            'de actualizar la tabla cada vez que se finaliza la recepcion, se actualizaria el setup.

            'Por los momentos no voy a actualizar la tabla MA_CONFIGURACION

            'comandoBD_PDA.CommandText = "DELETE MA_CONFIGURACION"
            'ejecutaQuery_PDA(comandoBD_PDA)
            'actualizarMA_CONFIGURACION(fecha_Actualizacion, num_UltimoDocumento, numUltimaRecepcion)

            If CompatibilidadFueraDeLinea Then

                FormBarraProgreso_Recepcion.ProgressBar_FormBarraProgreso.Value = 0

                FormBarraProgreso_Recepcion.lblBarraProgreso.Text = "Actualizando DEPOSITOS..."
                FormBarraProgreso_Recepcion.lblBarraProgreso.Refresh()
                FormBarraProgreso_Recepcion.lblBarraProgreso.Update()
                comandoBD_PDA.CommandText = "DELETE MA_DEPOSITO"
                ejecutaQuery_PDA(comandoBD_PDA)
                actualizarMA_DEPOSITO()
                FormBarraProgreso_Recepcion.ProgressBar_FormBarraProgreso.Value = 10

                FormBarraProgreso_Recepcion.lblBarraProgreso.Text = "Actualizando USUARIOS..."
                FormBarraProgreso_Recepcion.lblBarraProgreso.Refresh()
                FormBarraProgreso_Recepcion.lblBarraProgreso.Update()
                comandoBD_PDA.CommandText = "DELETE MA_USUARIOS"
                ejecutaQuery_PDA(comandoBD_PDA)
                actualizarMA_USUARIOS()
                FormBarraProgreso_Recepcion.ProgressBar_FormBarraProgreso.Value = 20

                'actualizarMA_CONFIGURACION(DateTime.Now.ToString, 0, 0, 0)

                FormBarraProgreso_Recepcion.lblBarraProgreso.Text = "Actualizando MONEDAS..."
                FormBarraProgreso_Recepcion.lblBarraProgreso.Refresh()
                FormBarraProgreso_Recepcion.lblBarraProgreso.Update()
                comandoBD_PDA.CommandText = "DELETE MA_MONEDAS"
                ejecutaQuery_PDA(comandoBD_PDA)
                actualizarMA_MONEDAS()
                FormBarraProgreso_Recepcion.ProgressBar_FormBarraProgreso.Value = 30

                FormBarraProgreso_Recepcion.lblBarraProgreso.Text = "Actualizando PROVEEDORES..."
                FormBarraProgreso_Recepcion.lblBarraProgreso.Refresh()
                FormBarraProgreso_Recepcion.lblBarraProgreso.Update()
                comandoBD_PDA.CommandText = "DELETE MA_PROVEEDORES"
                ejecutaQuery_PDA(comandoBD_PDA)
                actualizarMA_PROVEEDORES()
                FormBarraProgreso_Recepcion.ProgressBar_FormBarraProgreso.Value = 40

                'FormBarraProgreso_Recepcion.lblBarraProgreso.Text = "Actualizando ODC..."
                'FormBarraProgreso_Recepcion.lblBarraProgreso.Refresh()
                'FormBarraProgreso_Recepcion.lblBarraProgreso.Update()
                'If debeActualizar("MA_ODC") Then
                'comandoBD_PDA.CommandText = "DELETE MA_ODC"
                'ejecutaQuery_PDA(comandoBD_PDA)
                'actualizarMA_ODC()
                'Else
                'ActualizacionParcial("ODC")
                'End If

                FormBarraProgreso_Recepcion.ProgressBar_FormBarraProgreso.Value = 50

                FormBarraProgreso_Recepcion.lblBarraProgreso.Text = "Actualizando CORRELATIVOS..."
                FormBarraProgreso_Recepcion.lblBarraProgreso.Refresh()
                FormBarraProgreso_Recepcion.lblBarraProgreso.Update()
                comandoBD_PDA.CommandText = "DELETE MA_CORRELATIVOS"
                ejecutaQuery_PDA(comandoBD_PDA)
                actualizarMA_CORRELATIVOS()
                FormBarraProgreso_Recepcion.ProgressBar_FormBarraProgreso.Value = 60

                'FormBarraProgreso_Recepcion.lblBarraProgreso.Text = "Actualizando INVENTARIO..."
                'FormBarraProgreso_Recepcion.lblBarraProgreso.Refresh()
                'FormBarraProgreso_Recepcion.lblBarraProgreso.Update()
                'If debeActualizar("MA_INVENTARIO") Then
                'comandoBD_PDA.CommandText = "DELETE MA_INVENTARIO"
                'ejecutaQuery_PDA(comandoBD_PDA)
                'actualizarMA_INVENTARIO()
                'End If

                FormBarraProgreso_Recepcion.ProgressBar_FormBarraProgreso.Value = 70

                'FormBarraProgreso_Recepcion.lblBarraProgreso.Text = "Actualizando ODC..."
                'FormBarraProgreso_Recepcion.lblBarraProgreso.Refresh()
                'FormBarraProgreso_Recepcion.lblBarraProgreso.Update()
                'If debeActualizar("TR_ODC") Then
                'comandoBD_PDA.CommandText = "DELETE TR_ODC"
                'ejecutaQuery_PDA(comandoBD_PDA)
                'ActualizarTR_ODC()
                'Else
                'ActualizacionParcial("ODC")
                'End If

                FormBarraProgreso_Recepcion.ProgressBar_FormBarraProgreso.Value = 80

                FormBarraProgreso_Recepcion.lblBarraProgreso.Text = "Actualizando PRODUCTOS..."
                FormBarraProgreso_Recepcion.lblBarraProgreso.Refresh()
                FormBarraProgreso_Recepcion.lblBarraProgreso.Update()
                ActualizacionParcial("Productos")
                FormBarraProgreso_Recepcion.ProgressBar_FormBarraProgreso.Value = 90

                FormBarraProgreso_Recepcion.lblBarraProgreso.Text = "Actualizando REGLAS DE NEGOCIO..."
                FormBarraProgreso_Recepcion.lblBarraProgreso.Refresh()
                FormBarraProgreso_Recepcion.lblBarraProgreso.Update()
                comandoBD_PDA.CommandText = "DELETE MA_REGLASDENEGOCIO"
                ejecutaQuery_PDA(comandoBD_PDA)
                actualizarMA_REGLASDENEGOCIO()
                FormBarraProgreso_Recepcion.ProgressBar_FormBarraProgreso.Value = 95

                comandoBD_PDA.CommandText = "DELETE MA_CONCEPTOS"
                ejecutaQuery_PDA(comandoBD_PDA)
                actualizarMA_CONCEPTOS()

                comandoBD_PDA.CommandText = "DELETE MA_PROCESOS"
                ejecutaQuery_PDA(comandoBD_PDA)
                actualizarMA_PROCESOS()

            Else

                dataset_RegladeNegocio = consultarRegladeNegocio("VEN_FactorizarPrecios")

                If dataset_RegladeNegocio.Tables(0).Rows.Count > 0 Then
                    FactorizarPrecios = CBool(Val(dataset_RegladeNegocio.Tables(0).Rows(0)("Valor")))
                Else
                    FactorizarPrecios = False
                End If

                dataset_RegladeNegocio = consultarRegladeNegocio("Hbl_FactorizarPrecios")

                If dataset_RegladeNegocio.Tables(0).Rows.Count > 0 Then
                    Hablador_FactorizarPreciosMultiMoneda = CBool(Val(dataset_RegladeNegocio.Tables(0).Rows(0)("Valor")))
                Else
                    Hablador_FactorizarPreciosMultiMoneda = True
                End If

            End If

            cerrarConexion_SERVIDOR()
            FormBarraProgreso_Recepcion.ProgressBar_FormBarraProgreso.Value = 100

        End If

    End Sub

    Sub Salir()
        respuesta = MsgBox("¿Desea cerrar la aplicación?", MsgBoxStyle.YesNo, "Salir")
        If respuesta = "6" Then
            'Cierro todos los formularios
            'Application.Exit()
            ApplicationExit = True
            formUsuario.Close()
        End If
    End Sub

    Sub salirProceso(ByVal formulario As Form)
        Application.DoEvents()
        respuesta = MsgBox("¿Seguro que desea salir de este proceso?", MsgBoxStyle.YesNo, "Salir")
        Application.DoEvents()
        If respuesta = "6" Then
            formMenuPrincipal.Show()
            formulario.Visible = False
        End If
    End Sub

    Sub salirProcesoModLog(ByVal formulario As Form)
        SalioDelProceso = False
        Application.DoEvents()
        respuesta = MsgBox("¿Seguro que desea salir de este proceso?", MsgBoxStyle.YesNo, "Salir")
        Application.DoEvents()
        If respuesta = "6" Then
            SalioDelProceso = True
            FormMenuModLog_1.Show()
            formulario.Visible = False
        End If
    End Sub

    Function validarUsuarioContraseña(ByVal usuario As String, ByVal contraseña As String)

        Dim Query As String = "SELECT * FROM MA_USUARIOS WHERE Login_Name = @login_name AND Password = @password"

        If CompatibilidadFueraDeLinea Then

            comandoBD_PDA.Parameters.Clear()
            comandoBD_PDA.CommandText = Query
            comandoBD_PDA.Parameters.Add("@login_name", usuario)
            comandoBD_PDA.Parameters.Add("@password", contraseña)
            dataSet_PDA = consultaQuery_PDA(comandoBD_PDA)

        Else

            comandBD_SERVIDOR.Parameters.Clear()
            comandBD_SERVIDOR.CommandText = Query
            comandBD_SERVIDOR.Parameters.Add("@login_name", usuario)
            comandBD_SERVIDOR.Parameters.Add("@password", contraseña)
            dataSet_PDA = executeQuery_Servidor(comandBD_SERVIDOR)

        End If

        Return dataSet_PDA

    End Function

    Function RecuperarCONF_MENU_USER(ByVal Usuario As String)

        Dim Clave_User As String

        If CompatibilidadFueraDeLinea Then

            comandoBD_PDA.Parameters.Clear()
            comandoBD_PDA.CommandText = "SELECT TOP (1) ('org' + Clave) AS Clave_User FROM MA_USUARIOS WHERE Login_Name = @Login_Name"
            comandoBD_PDA.Parameters.Add("@Login_Name", Usuario)
            Clave_User = GetScalar_PDA(comandoBD_PDA, String.Empty, String.Empty)

            If Not String.IsNullOrEmpty(Clave_User) Then

                'If DebugMode Then MsgBox("Actualizando Datos User: " & Clave_User)

                actualizarCONF_MENU_USER(Clave_User)

                'If DebugMode Then MsgBox("Buscando opciones User: " & Clave_User)

                comandoBD_PDA.Parameters.Clear()
                comandoBD_PDA.CommandText = "SELECT * FROM CONF_MENU_USER WHERE Clave_User = @Clave_User"
                comandoBD_PDA.Parameters.Add("@Clave_User", Clave_User)
                dataSet_PDA = consultaQuery_PDA(comandoBD_PDA)

            End If

        Else

            comandBD_SERVIDOR.Parameters.Clear()
            comandBD_SERVIDOR.CommandText = "SELECT TOP (1) ('org' + Clave) AS Clave_User FROM MA_USUARIOS WHERE Login_Name = @Login_Name"
            comandBD_SERVIDOR.Parameters.Add("@Login_Name", Usuario)
            Clave_User = GetScalar_SRV(comandBD_SERVIDOR, conexionBD_SERVIDOR, String.Empty, String.Empty)

            If Not String.IsNullOrEmpty(Clave_User) Then

                'If DebugMode Then MsgBox("Buscando opciones User: " & Clave_User)

                comandBD_SERVIDOR.Parameters.Clear()
                comandBD_SERVIDOR.CommandText = "SELECT * FROM CONF_MENU_USER WHERE Clave_User = @Clave_User"
                comandBD_SERVIDOR.Parameters.Add("@Clave_User", Clave_User)
                dataSet_PDA = executeQuery_Servidor(comandBD_SERVIDOR)

            End If

        End If

        Return dataSet_PDA

    End Function

    Sub actualizacionManual()

        'Verifico que tenga conexión con el servidor

        If conectarDB_SERVIDOR(rutaDB_SERVIDOR) = 0 Then
            MsgBox("No hay conexión con el Servidor. No se puede actualizar en este momento.", MsgBoxStyle.Information, "isMOBILE")
        Else
            'Verifico cada Check
            If FormActualizar_InventarioFisico.CheckConfiguración.CheckState = 1 Then

                FormActualizar_InventarioFisico.ProgressBar_FormActualizarInventarioFisico.Value = 20
                SentenciaSQL = "SELECT FECHAult_ACTUALIZACION FROM MA_CONFIGURACION"
                fecha_Actualizacion = returnString_PDA(SentenciaSQL)

                FormActualizar_InventarioFisico.ProgressBar_FormActualizarInventarioFisico.Value = 40
                'SentenciaSQL = "SELECT NUMultDOC FROM MA_CONFIGURACION"
                'num_UltimoDocumento = returnInteger_PDA(SentenciaSQL)

                SentenciaSQL = "SELECT TOP (1) SUBSTRING(C_DOCUMENTO, LEN('" & Replace(Id_PDA, "'", String.Empty) & "') + 1, 9999) AS NUMultDOC, C_DOCUMENTO FROM MA_INV_FISICO WHERE LEFT(C_DOCUMENTO, LEN('" & Replace(Id_PDA, "'", String.Empty) & "')) = '" & Replace(Id_PDA, "'", String.Empty) & "' ORDER BY c_Documento DESC"
                comandoBD_SERVIDOR.Parameters.Clear()
                comandoBD_SERVIDOR.CommandText = SentenciaSQL
                num_UltimoDocumento = CLng(Val(GetScalar_SRV(comandoBD_SERVIDOR, conexionBD_SERVIDOR, 0, 0)))

                FormActualizar_InventarioFisico.ProgressBar_FormActualizarInventarioFisico.Value = 60
                comandoBD_PDA.CommandText = "DELETE MA_CONFIGURACION"
                ejecutaQuery_PDA(comandoBD_PDA)
                FormActualizar_InventarioFisico.ProgressBar_FormActualizarInventarioFisico.Value = 70

                If IsDBNull(fecha_Actualizacion) Or fecha_Actualizacion Is Nothing Then fecha_Actualizacion = DateTime.Now.ToString
                If IsDBNull(num_UltimoDocumento) Then num_UltimoDocumento = 1
                If IsDBNull(numUltimaRecepcion) Then numUltimaRecepcion = 1
                If IsDBNull(numUltimoTraslado) Then numUltimoTraslado = 1

                actualizarMA_CONFIGURACION(fecha_Actualizacion, num_UltimoDocumento, numUltimaRecepcion, numUltimoTraslado)

                comandoBD_PDA.CommandText = "DELETE MA_MONEDAS"
                ejecutaQuery_PDA(comandoBD_PDA)

                actualizarMA_MONEDAS()
                FormActualizar_InventarioFisico.ProgressBar_FormActualizarInventarioFisico.Value = 100

                MsgBox("Actualización de Configuración completada.", MsgBoxStyle.Information, "isMOBILE")
                FormActualizar_InventarioFisico.ProgressBar_FormActualizarInventarioFisico.Value = 0

            End If

            If FormActualizar_InventarioFisico.CheckDepositos.CheckState = 1 Then

                comandoBD_PDA.CommandText = "DELETE MA_DEPOSITO"
                ejecutaQuery_PDA(comandoBD_PDA)
                FormActualizar_InventarioFisico.ProgressBar_FormActualizarInventarioFisico.Value = 50
                actualizarMA_DEPOSITO()
                FormActualizar_InventarioFisico.ProgressBar_FormActualizarInventarioFisico.Value = 100
                MsgBox("Actualización de Depositos completada.", MsgBoxStyle.Information, "isMOBILE")
                FormActualizar_InventarioFisico.ProgressBar_FormActualizarInventarioFisico.Value = 0

            End If

            If FormActualizar_InventarioFisico.CheckUsuarios.CheckState = 1 Then

                comandoBD_PDA.CommandText = "DELETE MA_USUARIOS"
                ejecutaQuery_PDA(comandoBD_PDA)
                FormActualizar_InventarioFisico.ProgressBar_FormActualizarInventarioFisico.Value = 50
                actualizarMA_USUARIOS()
                FormActualizar_InventarioFisico.ProgressBar_FormActualizarInventarioFisico.Value = 100
                MsgBox("Actualización de Usuarios completada.", MsgBoxStyle.Information, "isMOBILE")
                FormActualizar_InventarioFisico.ProgressBar_FormActualizarInventarioFisico.Value = 0

            End If

            If FormActualizar_InventarioFisico.CheckInventarios.CheckState = 1 Then

                comandoBD_PDA.CommandText = "DELETE MA_ELAB_INVENTARIO"
                ejecutaQuery_PDA(comandoBD_PDA)
                FormActualizar_InventarioFisico.ProgressBar_FormActualizarInventarioFisico.Value = 20
                actualizarMA_ELAB_INVENTARIO()
                FormActualizar_InventarioFisico.ProgressBar_FormActualizarInventarioFisico.Value = 100
                MsgBox("Actualización de Inventarios completada.", MsgBoxStyle.Information, "isMOBILE")
                FormActualizar_InventarioFisico.ProgressBar_FormActualizarInventarioFisico.Value = 0

            End If

            If FormActualizar_InventarioFisico.CheckProductos.CheckState = 1 Then

                comandoBD_PDA.CommandText = "DELETE MA_CODIGOS"
                ejecutaQuery_PDA(comandoBD_PDA)
                FormActualizar_InventarioFisico.ProgressBar_FormActualizarInventarioFisico.Value = 20
                actualizarMA_CODIGOS()
                FormActualizar_InventarioFisico.ProgressBar_FormActualizarInventarioFisico.Value = 40
                comandoBD_PDA.CommandText = "DELETE MA_PRODUCTOS"
                ejecutaQuery_PDA(comandoBD_PDA)
                FormActualizar_InventarioFisico.ProgressBar_FormActualizarInventarioFisico.Value = 60
                actualizarMA_PRODUCTOS()
                FormActualizar_InventarioFisico.ProgressBar_FormActualizarInventarioFisico.Value = 100
                MsgBox("Actualización de Productos completada.", MsgBoxStyle.Information, "isMOBILE")
                FormActualizar_InventarioFisico.ProgressBar_FormActualizarInventarioFisico.Value = 0
                fecha_Actualizacion = DateTime.Now.ToString

                comandoBD_PDA.CommandText = "UPDATE MA_CONFIGURACION SET FECHAult_ACTUALIZACION= @fecha"
                comandoBD_PDA.Parameters.Add("@fecha", fecha_Actualizacion)
                ejecutaQuery_PDA(comandoBD_PDA)

            End If

            If FormActualizar_InventarioFisico.CheckODC.CheckState = 1 Then

                comandoBD_PDA.CommandText = "DELETE MA_ODC"
                ejecutaQuery_PDA(comandoBD_PDA)

                FormActualizar_InventarioFisico.ProgressBar_FormActualizarInventarioFisico.Value = 25

                actualizarMA_ODC()

                FormActualizar_InventarioFisico.ProgressBar_FormActualizarInventarioFisico.Value = 50

                comandoBD_PDA.CommandText = "DELETE TR_ODC"
                ejecutaQuery_PDA(comandoBD_PDA)

                FormActualizar_InventarioFisico.ProgressBar_FormActualizarInventarioFisico.Value = 75

                ActualizarTR_ODC()

                FormActualizar_InventarioFisico.ProgressBar_FormActualizarInventarioFisico.Value = 100

                MsgBox("Actualización de Ordenes de Compra completada.", MsgBoxStyle.Information, "isMOBILE")
                FormActualizar_InventarioFisico.ProgressBar_FormActualizarInventarioFisico.Value = 0

            End If

            cerrarConexion_SERVIDOR()
        End If

    End Sub

    Sub avisoDeActualizarProductos()

        SentenciaSQL = "SELECT FECHAult_ACTUALIZACION FROM MA_CONFIGURACION"
        fecha_Actualizacion = returnString_PDA(SentenciaSQL)
        MsgBox("La ultima vez que actualizo los productos fue: " + fecha_Actualizacion + ".", MsgBoxStyle.Exclamation, "isMOBILE")

    End Sub

    '******************************** R E C E P C I Ó N **************************************************************

    Sub buscarProveedorporCodigo(ByVal CodigoProveedor As String)

        'Cuando el txtProveedor no esta vacio; se verifica que el proveedor existe
        If Not (FormProveedorDeposito_Recepcion.txtProveedor.Text = "") Then

            If FormProveedorDeposito_Recepcion.txtProveedor.TextLength <= 15 Then

                Dim Query As String = "SELECT c_Descripcio " + _
                "FROM MA_Proveedores " + _
                "WHERE c_codproveed = @c_codproveed"

                If CompatibilidadFueraDeLinea Then
                    comandoBD_PDA.CommandText = Query
                    comandoBD_PDA.Parameters.Add("@c_codproveed", CodigoProveedor)
                    dataSet_PDA = consultaQuery_PDA(comandoBD_PDA)
                Else
                    comandBD_SERVIDOR.Parameters.Clear()
                    comandBD_SERVIDOR.CommandText = Query
                    comandBD_SERVIDOR.Parameters.Add("@c_codproveed", CodigoProveedor)
                    dataSet_PDA = executeQuery_Servidor(comandBD_SERVIDOR)
                End If

                'Se verifica que la consulta trajo resultados o no
                If dataSet_PDA.Tables(0).Rows.Count() = 0 Then
                    MsgBox("El código de proveedor no existe.", MsgBoxStyle.Information, "isMOBILE")
                    FormProveedorDeposito_Recepcion.txtProveedor.Text = ""
                    FormProveedorDeposito_Recepcion.lblDescripcionProveedor.Text = "Descripción: "
                    FormProveedorDeposito_Recepcion.txtProveedor.Focus()
                Else
                    FormProveedorDeposito_Recepcion.lblDescripcionProveedor.Text = "Descripción: " + dataSet_PDA.Tables(0).Rows(0)("c_descripcio")
                    FormProveedorDeposito_Recepcion.txtProveedor.Enabled = False

                    If (verificarProveedorPoseeODC(FormProveedorDeposito_Recepcion.txtProveedor.Text) = 1) Then
                        RegladeNegocio = "Recepcion_PermiteRecibirsoloconODC"
                        dataset_RegladeNegocio = consultarRegladeNegocio(RegladeNegocio)
                        valorRegladeNegocio = CInt(dataset_RegladeNegocio.Tables(0).Rows(0)("valor"))
                        If valorRegladeNegocio = 1 Then
                            MsgBox("El proveedor posee órdenes de compra pendientes, presione Ok para importarlas.", MsgBoxStyle.OkOnly, "isMOBILE")
                            FormBuscarODC_Recepcion.Show()
                            FormProveedorDeposito_Recepcion.Visible = False
                            FormBuscarODC_Recepcion.txtCodigoProveedor.Text = FormProveedorDeposito_Recepcion.txtProveedor.Text
                            FormBuscarODC_Recepcion.txtCodigoProveedor.Enabled = False
                            FormBuscarODC_Recepcion.cmbBuscarODC.Enabled = False
                            llenaDataGridBuscarODC(FormBuscarODC_Recepcion.txtCodigoProveedor.Text)
                        Else 'el valor de la Regla de Negocio es cero (0) Permite recibir sin ODC
                            respuesta = MsgBox("El proveedor posee órdenes de compra pendientes, ¿desea importarlas?", MsgBoxStyle.YesNo, "isMOBILE")
                            If respuesta = "6" Then ' Si la respuesta es yes
                                FormBuscarODC_Recepcion.Show()
                                FormProveedorDeposito_Recepcion.Visible = False
                                FormBuscarODC_Recepcion.txtCodigoProveedor.Text = FormProveedorDeposito_Recepcion.txtProveedor.Text
                                FormBuscarODC_Recepcion.txtCodigoProveedor.Enabled = False
                                FormBuscarODC_Recepcion.cmbBuscarODC.Enabled = False
                                llenaDataGridBuscarODC(FormBuscarODC_Recepcion.txtCodigoProveedor.Text)
                            Else 'No desea importar la ODC
                                'Consulto el nivel de usuario necesario para recibir sin ODC
                                consultarNivelUsuarioRecibirSinODC_FormProveedorDeposito()
                            End If
                        End If
                    Else 'el proveedor No posee ODC
                        RegladeNegocio = "Recepcion_PermiteRecibirsoloconODC"
                        dataset_RegladeNegocio = consultarRegladeNegocio(RegladeNegocio)
                        valorRegladeNegocio = CInt(dataset_RegladeNegocio.Tables(0).Rows(0)("valor"))

                        If valorRegladeNegocio = 1 Then
                            MsgBox("Este proveedor no posee órdenes de compra pendientes, no se puede realizar la recepción.", MsgBoxStyle.Information, "isMOBILE")
                            FormProveedorDeposito_Recepcion.txtProveedor.Text = ""
                            FormProveedorDeposito_Recepcion.lblDescripcionProveedor.Text = "Descripción: "
                            FormProveedorDeposito_Recepcion.txtProveedor.Focus()
                            FormProveedorDeposito_Recepcion.txtDeposito.Enabled = False
                            FormProveedorDeposito_Recepcion.cmbBuscarDeposito.Enabled = False
                            FormProveedorDeposito_Recepcion.cmbVerificarDeposito.Enabled = False
                        Else 'Se puede recibir sin ODC
                            'Consulto el nivel de usuario necesario para recibir sin ODC
                            consultarNivelUsuarioRecibirSinODC_FormProveedorDeposito()
                        End If
                    End If
                End If
            Else
                MsgBox("Código inválido.", MsgBoxStyle.Information, "isMOBILE")
                FormProveedorDeposito_Recepcion.txtProveedor.Text = ""
                FormProveedorDeposito_Recepcion.txtProveedor.Focus()
            End If
        Else
            MsgBox("Debe ingresar el código del proveedor o presionar el botón signo de interrogación para realizar la búsqueda.", MsgBoxStyle.Information, "isMOBILE")
            FormProveedorDeposito_Recepcion.txtProveedor.Enabled = True
            FormProveedorDeposito_Recepcion.txtProveedor.Focus()
        End If

    End Sub

    Sub llenaDataGridBuscarProveedor(ByVal descripcionProveedor)

        Dim Query As String = "SELECT c_codproveed, c_razon " + _
        "FROM MA_PROVEEDORES " + _
        "WHERE c_descripcio LIKE @descripcionProveedor " + _
        "ORDER BY c_descripcio"

        If CompatibilidadFueraDeLinea Then

            comandoBD_PDA.Parameters.Clear()
            comandoBD_PDA.CommandText = Query
            comandoBD_PDA.Parameters.Add("@descripcionProveedor", "%" & descripcionProveedor & "%")
            dataSet_PDA = consultaQuery_PDA(comandoBD_PDA)

        Else

            comandBD_SERVIDOR.Parameters.Clear()
            comandBD_SERVIDOR.CommandText = Query
            comandBD_SERVIDOR.Parameters.Add("@descripcionProveedor", "%" & descripcionProveedor & "%")
            dataSet_PDA = executeQuery_Servidor(comandBD_SERVIDOR)

        End If

        'En la propiedad DataSource es donde puedo llenar el dataGrid con la informacion de la BD.
        FormBuscarProveedor_Recepcion.DataGridBuscarProveedor.DataSource = dataSet_PDA.Tables(0)

        'se cambia el nombre de las columnas del dataSet al nombre que se desea que aparezcan en el dataGrid, para después poder utilizarlos en la funcion de obtener el campo mas largo
        dataSet_PDA.Tables(0).Columns(0).ColumnName = "CÓDIGO"
        dataSet_PDA.Tables(0).Columns(1).ColumnName = "RAZÓN SOCIAL"

        Dim Estilos As New DataGridTableStyle
        Dim columna1, columna2 As New DataGridTextBoxColumn
        Estilos.MappingName = dataSet_PDA.Tables(0).TableName

        With columna1
            .MappingName = "CÓDIGO"
            .HeaderText = dataSet_PDA.Tables(0).Columns(0).ColumnName
        End With

        With columna2
            .MappingName = "RAZÓN SOCIAL"
            .HeaderText = dataSet_PDA.Tables(0).Columns(1).ColumnName
        End With

        Estilos.GridColumnStyles.Add(columna1)
        Estilos.GridColumnStyles.Add(columna2)

        FormBuscarProveedor_Recepcion.DataGridBuscarProveedor.TableStyles.Clear()
        FormBuscarProveedor_Recepcion.DataGridBuscarProveedor.TableStyles.Add(Estilos)


        ' For para ajustar el tamaño de las columnas contenidas en el dataSet_PDA al campo mas largo
        Dim numeroColumnas As Integer
        For numeroColumnas = 0 To (dataSet_PDA.Tables(0).Columns.Count - 1)
            nuevoAncho = obtenerCampoMasLargo(dataSet_PDA, 0, numeroColumnas)
            FormBuscarProveedor_Recepcion.DataGridBuscarProveedor.TableStyles(dataSet_PDA.Tables(0).TableName).GridColumnStyles(numeroColumnas).Width = nuevoAncho
        Next

        If dataSet_PDA.Tables(0).Rows.Count() = 0 Then
            MsgBox("Búsqueda sin éxito", MsgBoxStyle.Information, "isMOBILE")
            FormBuscarProveedor_Recepcion.txtDescripcionProveedor.Text = ""
            FormBuscarProveedor_Recepcion.txtDescripcionProveedor.Focus()
        End If

    End Sub

    Sub llenaDataGridBuscarProductos(ByVal descripcionProducto)

        Dim Query As String = "SELECT C_CODIGO, C_DESCRI " + _
        "FROM MA_PRODUCTOS " + _
        "WHERE C_DESCRI LIKE @descripcionProducto " + _
        "ORDER BY C_codigo"

        If CompatibilidadFueraDeLinea Then

            comandBD_SERVIDOR.Parameters.Clear()
            comandoBD_PDA.CommandText = Query
            comandoBD_PDA.Parameters.Add("@descripcionProducto", "%" & descripcionProducto & "%")
            dataSet_PDA = consultaQuery_PDA(comandoBD_PDA)

        Else

            comandBD_SERVIDOR.Parameters.Clear()
            comandBD_SERVIDOR.CommandText = Query
            comandBD_SERVIDOR.Parameters.Add("@descripcionProducto", "%" & descripcionProducto & "%")

            If Val(TmpBDCheck)=1 then
                dataSet_PDA = executeQuery_Servidor(comandBD_SERVIDOR, conexionBD_SERVIDORPOS)
            Else
                dataSet_PDA = executeQuery_Servidor(comandBD_SERVIDOR, conexionBD_SERVIDOR)
            End If

        End If

        If dataSet_PDA.Tables(0).Rows.Count() = 0 Then
            MsgBox("Búsqueda sin éxito.", MsgBoxStyle.Information, "isMOBILE")
            Formbusqueda_productos.txtproductos.Text = ""
            Formbusqueda_productos.txtproductos.Focus()
            Exit Sub
        End If

        'En la propiedad DataSource es donde puedo llenar el dataGrid con la informacion de la BD.
        Formbusqueda_productos.DataGridbusqueda_productos.DataSource = dataSet_PDA.Tables(0)

        'se cambia el nombre de las columnas del dataSet al nombre que se desea que aparezcan en el dataGrid,
        'para después poder utilizarlos en la funcion de obtener el campo mas largo

        dataSet_PDA.Tables(0).Columns(0).ColumnName = "CÓDIGO"
        dataSet_PDA.Tables(0).Columns(1).ColumnName = "DESCRIPCIÓN"

        Dim Estilos As New DataGridTableStyle
        Dim columna1, columna2 As New DataGridTextBoxColumn
        Estilos.MappingName = dataSet_PDA.Tables(0).TableName

        With columna1
            .MappingName = "CÓDIGO"
            .HeaderText = dataSet_PDA.Tables(0).Columns(0).ColumnName
        End With

        With columna2
            .MappingName = "DESCRIPCIÓN"
            .HeaderText = dataSet_PDA.Tables(0).Columns(1).ColumnName
        End With

        Estilos.GridColumnStyles.Add(columna1)
        Estilos.GridColumnStyles.Add(columna2)

        Formbusqueda_productos.DataGridbusqueda_productos.TableStyles.Clear()
        Formbusqueda_productos.DataGridbusqueda_productos.TableStyles.Add(Estilos)

        ' For para ajustar el tamaño de las columnas contenidas en el dataSet_PDA al campo mas largo
        Dim numeroColumnas As Integer

        For numeroColumnas = 0 To (dataSet_PDA.Tables(0).Columns.Count - 1)
            nuevoAncho = obtenerCampoMasLargo(dataSet_PDA, 0, numeroColumnas)
            Formbusqueda_productos.DataGridbusqueda_productos.TableStyles(dataSet_PDA.Tables(0).TableName).GridColumnStyles(numeroColumnas).Width = nuevoAncho
        Next

        Formbusqueda_productos.DataGridbusqueda_productos.Focus()

    End Sub


    Public Function obtenerCampoMasLargo(ByVal dataSet As DataSet, ByVal IndiceTabla As Integer, ByVal IndiceColumna As Integer) As Integer

        Dim grafico As Graphics = FormBuscarProveedor_Recepcion.DataGridBuscarProveedor.CreateGraphics()
        'Variable Encabezado para obtener el nombre de la columa del dataSet indicado en el indiceColumna 
        Dim Encabezado As String = dataSet.Tables(IndiceTabla).Columns(IndiceColumna).ColumnName
        'En la variable LongitudMaxima se guarda el valor de la longitud del Encabezado como valor inicial, para que en caso que no exista un campo con una longitud mayor, se vea el nombre de la columna completo
        Dim longitudMaxima As Integer = Convert.ToInt32(Math.Ceiling(grafico.MeasureString(Encabezado, FormBuscarProveedor_Recepcion.DataGridBuscarProveedor.Font).Width))

        'Toma el ancho de un espacio en blanco para agregarlo al nuevo ancho de la columna
        Dim Espacio As Integer = Convert.ToInt32(Math.Ceiling(grafico.MeasureString(" ", FormBuscarProveedor_Recepcion.DataGridBuscarProveedor.Font).Width))
        Dim numeroFilas As Integer = 0
        Dim integerAuxiliar As Integer
        Dim stringAuxiliar As String
        Dim total As Integer = dataSet.Tables(0).Rows.Count

        For numeroFilas = 0 To (total - 1)
            'Obtiene como string el valor contenido en la celda indicada 
            stringAuxiliar = dataSet.Tables(IndiceTabla).Rows(numeroFilas)(IndiceColumna).ToString()
            'Obtiene el ancho del campo string actual de acuerdo a la fuente
            integerAuxiliar = Convert.ToInt32(Math.Ceiling(grafico.MeasureString(stringAuxiliar, FormBuscarProveedor_Recepcion.DataGridBuscarProveedor.Font).Width))
            If (integerAuxiliar > longitudMaxima) Then
                longitudMaxima = integerAuxiliar
            End If
        Next

        Return longitudMaxima + Espacio

    End Function

    Public Function obtenerCampoMasLargo(ByVal dataSet As DataSet, ByVal IndiceTabla As Integer, _
    ByVal IndiceColumna As Integer, ByRef DataGridObj As DataGrid) As Integer

        Dim grafico As Graphics = DataGridObj.CreateGraphics()
        'Variable Encabezado para obtener el nombre de la columa del dataSet indicado en el indiceColumna 
        Dim Encabezado As String = dataSet.Tables(IndiceTabla).Columns(IndiceColumna).ColumnName
        'En la variable LongitudMaxima se guarda el valor de la longitud del Encabezado como valor inicial, para que en caso que no exista un campo con una longitud mayor, se vea el nombre de la columna completo
        Dim longitudMaxima As Integer = Convert.ToInt32(Math.Ceiling(grafico.MeasureString(Encabezado, DataGridObj.Font).Width))

        'Toma el ancho de un espacio en blanco para agregarlo al nuevo ancho de la columna
        Dim Espacio As Integer = Convert.ToInt32(Math.Ceiling(grafico.MeasureString(" ", DataGridObj.Font).Width))
        Dim numeroFilas As Integer = 0
        Dim integerAuxiliar As Integer
        Dim stringAuxiliar As String
        Dim total As Integer = dataSet.Tables(0).Rows.Count

        For numeroFilas = 0 To (total - 1)
            'Obtiene como string el valor contenido en la celda indicada 
            stringAuxiliar = dataSet.Tables(IndiceTabla).Rows(numeroFilas)(IndiceColumna).ToString()
            'Obtiene el ancho del campo string actual de acuerdo a la fuente
            integerAuxiliar = Convert.ToInt32(Math.Ceiling(grafico.MeasureString(stringAuxiliar, DataGridObj.Font).Width))
            If (integerAuxiliar > longitudMaxima) Then
                longitudMaxima = integerAuxiliar
            End If
        Next

        Return longitudMaxima + Espacio

    End Function

    Sub seleccionarProveedorDataGrid()

        'Obtengo la fila del DataGridBuscarProveedor que el usuario selecciono 
        'Asi podre obtener el codigo y descripcion del proveedor para colocarlo en el formProveedorDeposito
        filaDataGrid = FormBuscarProveedor_Recepcion.DataGridBuscarProveedor.CurrentRowIndex

        If filaDataGrid = -1 Then
            MsgBox("No seleccionó ningún proveedor", MsgBoxStyle.Information, "isMOBILE")
        Else

            'descripcionProveedor
            codigoProveedorCompleto = FormBuscarProveedor_Recepcion.DataGridBuscarProveedor.Item(filaDataGrid, 0)

            Dim Query As String = "SELECT c_descripcio " + _
            "from MA_PROVEEDORES " + _
            "WHERE c_codproveed = @codigoProveedorCompleto "

            If CompatibilidadFueraDeLinea Then

                comandoBD_PDA.Parameters.Clear()
                comandoBD_PDA.CommandText = Query
                comandoBD_PDA.Parameters.Add("@codigoProveedorCompleto", codigoProveedorCompleto)
                descripcionProveedor = CStr(comandoBD_PDA.ExecuteScalar())
                comandoBD_PDA.Parameters.Clear()

            Else

                comandBD_SERVIDOR.Parameters.Clear()
                comandBD_SERVIDOR.CommandText = Query
                comandBD_SERVIDOR.Connection = conexionBD_SERVIDOR
                comandBD_SERVIDOR.Parameters.Add("@codigoProveedorCompleto", codigoProveedorCompleto)
                descripcionProveedor = CStr(comandBD_SERVIDOR.ExecuteScalar())
                comandBD_SERVIDOR.Parameters.Clear()

            End If

            FormProveedorDeposito_Recepcion.txtProveedor.Text = FormBuscarProveedor_Recepcion.DataGridBuscarProveedor.Item(filaDataGrid, 0)
            FormProveedorDeposito_Recepcion.lblDescripcionProveedor.Text = "Descripción: " + descripcionProveedor
            FormBuscarProveedor_Recepcion.txtDescripcionProveedor.Text = ""
            FormBuscarProveedor_Recepcion.DataGridBuscarProveedor.DataSource = Nothing
            FormProveedorDeposito_Recepcion.Show()
            FormBuscarProveedor_Recepcion.Visible = False

            If (verificarProveedorPoseeODC(FormProveedorDeposito_Recepcion.txtProveedor.Text) = 1) Then
                RegladeNegocio = "Recepcion_PermiteRecibirsoloconODC"
                dataset_RegladeNegocio = consultarRegladeNegocio(RegladeNegocio)
                valorRegladeNegocio = Val(dataset_RegladeNegocio.Tables(0).Rows(0)("valor"))
                If valorRegladeNegocio = 1 Then
                    MsgBox("El Proveedor posee Órdenes de Compra Pendientes, Presione Ok para importarlas", MsgBoxStyle.OkOnly, "isMOBILE")
                    FormBuscarODC_Recepcion.Show()
                    FormProveedorDeposito_Recepcion.Visible = False
                    FormBuscarODC_Recepcion.txtCodigoProveedor.Text = FormProveedorDeposito_Recepcion.txtProveedor.Text
                    FormBuscarODC_Recepcion.txtCodigoProveedor.Enabled = False
                    FormBuscarODC_Recepcion.cmbBuscarODC.Enabled = False
                    llenaDataGridBuscarODC(FormBuscarODC_Recepcion.txtCodigoProveedor.Text)
                Else 'el valor de la Regla de Negocio es cero (0) Permite recibir sin ODC
                    respuesta = MsgBox("El Proveedor posee Órdenes de Compra Pendientes, desea importarlas", MsgBoxStyle.YesNo, "isMOBILE")
                    If respuesta = "6" Then ' Si la respuesta es yes
                        FormBuscarODC_Recepcion.Show()
                        FormProveedorDeposito_Recepcion.Visible = False
                        FormBuscarODC_Recepcion.txtCodigoProveedor.Text = FormProveedorDeposito_Recepcion.txtProveedor.Text
                        FormBuscarODC_Recepcion.txtCodigoProveedor.Enabled = False
                        FormBuscarODC_Recepcion.cmbBuscarODC.Enabled = False
                        llenaDataGridBuscarODC(FormBuscarODC_Recepcion.txtCodigoProveedor.Text)
                    Else 'No desea importar la ODC
                        'Consulto el nivel de usuario necesario para recibir sin ODC
                        consultarNivelUsuarioRecibirSinODC_FormProveedorDeposito()
                    End If
                End If
            Else 'el proveedor No posee ODC
                RegladeNegocio = "Recepcion_PermiteRecibirsoloconODC"
                dataset_RegladeNegocio = consultarRegladeNegocio(RegladeNegocio)
                valorRegladeNegocio = Val(dataset_RegladeNegocio.Tables(0).Rows(0)("valor"))
                If valorRegladeNegocio = 1 Then
                    MsgBox("Este proveedor no posee órdenes de compra pendientes, no se puede realizar la recepción", MsgBoxStyle.Information, "isMOBILE")
                    FormProveedorDeposito_Recepcion.txtProveedor.Text = ""
                    FormProveedorDeposito_Recepcion.lblDescripcionProveedor.Text = "Descripción: "
                    FormProveedorDeposito_Recepcion.txtProveedor.Focus()
                    FormProveedorDeposito_Recepcion.txtDeposito.Enabled = False
                    FormProveedorDeposito_Recepcion.cmbBuscarDeposito.Enabled = False
                    FormProveedorDeposito_Recepcion.cmbVerificarDeposito.Enabled = False
                Else
                    'Consulto el nivel de usuario necesario para recibir sin ODC
                    consultarNivelUsuarioRecibirSinODC_FormProveedorDeposito()
                End If
            End If
        End If

    End Sub

    Sub SeleccionarProductos_datagrid()

        Dim codigo_productocompleto As String

        filaDataGrid = Formbusqueda_productos.DataGridbusqueda_productos.CurrentRowIndex

        If filaDataGrid = -1 Then
            MsgBox("No seleccionó ningún producto.", MsgBoxStyle.Information, "isMOBILE")
        Else

            codigo_productocompleto = Formbusqueda_productos.DataGridbusqueda_productos.Item(filaDataGrid, 0)

            Dim Query As String = "SELECT C_CODIGO, C_DESCRI " + _
            "FROM MA_PRODUCTOS " + _
            "WHERE C_codigo LIKE @codigo_productocompleto " + _
            "ORDER BY C_DESCRI"

            If CompatibilidadFueraDeLinea Then

                comandoBD_PDA.CommandText = Query
                comandoBD_PDA.Parameters.Add("@codigo_productocompleto", codigo_productocompleto)
                dataSet_PDA = consultaQuery_PDA(comandoBD_PDA)

            Else

                comandBD_SERVIDOR.CommandText = Query
                comandBD_SERVIDOR.Parameters.Add("@codigo_productocompleto", codigo_productocompleto)

                If Val(TmpBDCheck) = 1 Then
                    dataSet_PDA = executeQuery_Servidor(comandBD_SERVIDOR, conexionBD_SERVIDORPOS)
                Else
                    dataSet_PDA = executeQuery_Servidor(comandBD_SERVIDOR, conexionBD_SERVIDOR)
                End If

            End If

            'En la propiedad DataSource es donde puedo llenar el dataGrid con la informacion de la BD.

            'se cambia el nombre de las columnas del dataSet al nombre que se desea que aparezcan en el dataGrid,
            'para después poder utilizarlos en la funcion de obtener el campo mas largo

            dataSet_PDA.Tables(0).Columns(0).ColumnName = "CÓDIGO"
            dataSet_PDA.Tables(0).Columns(1).ColumnName = "DESCRIPCIÓN"

            'dataSet_PDA.Tables(0).Columns(2).ColumnName = "Nº DE IMPRESIONES"

            Dim Estilos As New DataGridTableStyle
            Dim columna1, columna2, columna3 As New DataGridTextBoxColumn
            Estilos.MappingName = dataSet_PDA.Tables(0).TableName

            With columna1
                .MappingName = "CÓDIGO"
                .HeaderText = dataSet_PDA.Tables(0).Columns(0).ColumnName
            End With

            With columna2
                .MappingName = "DESCRIPCIÓN"
                .HeaderText = dataSet_PDA.Tables(0).Columns(1).ColumnName
            End With

            Estilos.GridColumnStyles.Add(columna1)
            Estilos.GridColumnStyles.Add(columna2)
            'Estilos.GridColumnStyles.Add(columna3)

            Dim numeroColumnas As Integer

            Select Case (Formbusqueda_productos.Tipo_Impresion)
                Case Is = "Etiqueta"

                    Form_Impresion_Etiquetas.Datagrid_impresion.DataSource = dataSet_PDA.Tables(0)

                    Form_Impresion_Etiquetas.Datagrid_impresion.TableStyles.Clear()
                    Form_Impresion_Etiquetas.Datagrid_impresion.TableStyles.Add(Estilos)

                    For numeroColumnas = 0 To (dataSet_PDA.Tables(0).Columns.Count - 1)
                        nuevoAncho = obtenerCampoMasLargo(dataSet_PDA, 0, numeroColumnas)
                        Form_Impresion_Etiquetas.Datagrid_impresion.TableStyles(dataSet_PDA.Tables(0).TableName).GridColumnStyles(numeroColumnas).Width = nuevoAncho
                    Next

                    Formbusqueda_productos.txtproductos.Text = ""
                    Formbusqueda_productos.DataGridbusqueda_productos.DataSource = Nothing

                    Form_Impresion_Etiquetas.Show()

                    Formbusqueda_productos.Visible = False

                Case Is = "Hablador"

                    Form_Impresion_Hablador.Datagrid_Impresion.DataSource = dataSet_PDA.Tables(0)

                    'se cambia el nombre de las columnas del dataSet al nombre que se desea que aparezcan en el dataGrid,
                    ' para después poder utilizarlos en la funcion de obtener el campo mas largo

                    'dataSet_PDA.Tables(0).Columns(0).ColumnName = "CÓDIGO"
                    'dataSet_PDA.Tables(0).Columns(1).ColumnName = "DESCRIPCIÓN"

                    ''dataSet_PDA.Tables(0).Columns(2).ColumnName = "Nº DE IMPRESIONES"
                    'Dim Estilos As New DataGridTableStyle
                    'Dim columna1, columna2, columna3 As New DataGridTextBoxColumn
                    'Estilos.MappingName = dataSet_PDA.Tables(0).TableName

                    'With columna1
                    '    .MappingName = "CÓDIGO"
                    '    .HeaderText = dataSet_PDA.Tables(0).Columns(0).ColumnName
                    'End With

                    'With columna2
                    '    .MappingName = "DESCRIPCIÓN"
                    '    .HeaderText = dataSet_PDA.Tables(0).Columns(1).ColumnName
                    'End With

                    'Estilos.GridColumnStyles.Add(columna1)
                    'Estilos.GridColumnStyles.Add(columna2)
                    ''Estilos.GridColumnStyles.Add(columna3)

                    Form_Impresion_Hablador.Datagrid_Impresion.TableStyles.Clear()
                    Form_Impresion_Hablador.Datagrid_Impresion.TableStyles.Add(Estilos)

                    For numeroColumnas = 0 To (dataSet_PDA.Tables(0).Columns.Count - 1)
                        nuevoAncho = obtenerCampoMasLargo(dataSet_PDA, 0, numeroColumnas)
                        Form_Impresion_Hablador.Datagrid_Impresion.TableStyles(dataSet_PDA.Tables(0).TableName).GridColumnStyles(numeroColumnas).Width = nuevoAncho
                    Next

                    Formbusqueda_productos.txtproductos.Text = ""
                    Formbusqueda_productos.DataGridbusqueda_productos.DataSource = Nothing

                    Form_Impresion_Hablador.Show()

                    Formbusqueda_productos.Visible = False

            End Select

        End If

    End Sub

    Function buscarDepositoporCodigo(ByVal CodigoDeposito As String)

        Dim Query As String = "SELECT c_Descripcion " + _
        "FROM MA_DEPOSITO " + _
        "WHERE c_CodDeposito = @c_coddeposito " + _
        "AND c_CodLocalidad = @localidad"

        If CompatibilidadFueraDeLinea Then

            comandoBD_PDA.Parameters.Clear()
            comandoBD_PDA.CommandText = Query
            comandoBD_PDA.Parameters.Add("@c_coddeposito", CodigoDeposito)
            comandoBD_PDA.Parameters.Add("@localidad", Localidad)
            dataSet_PDA = consultaQuery_PDA(comandoBD_PDA)

        Else

            comandBD_SERVIDOR.Parameters.Clear()
            comandBD_SERVIDOR.CommandText = Query
            comandBD_SERVIDOR.Parameters.Add("@c_coddeposito", CodigoDeposito)
            comandBD_SERVIDOR.Parameters.Add("@localidad", Localidad)
            dataSet_PDA = executeQuery_Servidor(comandBD_SERVIDOR)

        End If

        Return dataSet_PDA

    End Function

    Sub llenaDataGridBuscarDeposito(ByVal descripcionDeposito)

        Dim Query As String = "SELECT c_CodDeposito, c_Descripcion FROM MA_DEPOSITO " + _
        "WHERE c_Descripcion LIKE @descripcionDeposito " + _
        "AND c_CodLocalidad = @localidad " + _
        "ORDER BY c_Descripcion"

        If CompatibilidadFueraDeLinea Then

            comandoBD_PDA.Parameters.Clear()
            comandoBD_PDA.CommandText = Query
            comandoBD_PDA.Parameters.Add("@descripcionDeposito", "%" & descripcionDeposito & "%")
            comandoBD_PDA.Parameters.Add("@localidad", Localidad)

            dataSet_PDA = consultaQuery_PDA(comandoBD_PDA)

        Else

            comandBD_SERVIDOR.Parameters.Clear()
            comandBD_SERVIDOR.CommandText = Query
            comandBD_SERVIDOR.Parameters.Add("@descripcionDeposito", "%" & descripcionDeposito & "%")
            comandBD_SERVIDOR.Parameters.Add("@localidad", Localidad)

            dataSet_PDA = executeQuery_Servidor(comandBD_SERVIDOR)

        End If

        'En la propiedad DataSource es donde puedo llenar el dataGrid con la informacion de la BD.
        FormBuscarDeposito.DataGridBuscarDeposito.DataSource = dataSet_PDA.Tables(0)

        dataSet_PDA.Tables(0).Columns(0).ColumnName = "CÓDIGO"
        dataSet_PDA.Tables(0).Columns(1).ColumnName = "DESCRIPCIÓN"

        Dim Estilos As New DataGridTableStyle
        Dim columna1, columna2 As New DataGridTextBoxColumn
        Estilos.MappingName = dataSet_PDA.Tables(0).TableName

        With columna1
            .MappingName = dataSet_PDA.Tables(0).Columns(0).ColumnName
            .HeaderText = "CÓDIGO"
        End With

        With columna2
            .MappingName = dataSet_PDA.Tables(0).Columns(1).ColumnName
            .HeaderText = "DESCRIPCIÓN"
        End With

        Estilos.GridColumnStyles.Add(columna1)
        Estilos.GridColumnStyles.Add(columna2)

        FormBuscarDeposito.DataGridBuscarDeposito.TableStyles.Clear()
        FormBuscarDeposito.DataGridBuscarDeposito.TableStyles.Add(Estilos)

        ' For para ajustar el tamaño de las columnas contenidas en el dataSet_PDA al campo mas largo
        Dim i As Integer

        For i = 0 To (dataSet_PDA.Tables(0).Columns.Count - 1)
            nuevoAncho = obtenerCampoMasLargo(dataSet_PDA, 0, i)
            FormBuscarDeposito.DataGridBuscarDeposito.TableStyles(dataSet_PDA.Tables(0).TableName).GridColumnStyles(i).Width = nuevoAncho
        Next i

        If dataSet_PDA.Tables(0).Rows.Count() = 0 Then
            MsgBox("Búsqueda sin éxito", MsgBoxStyle.Information, "isMOBILE")
            FormBuscarDeposito.txtDescripcionDeposito.Text = ""
            FormBuscarDeposito.txtDescripcionDeposito.Focus()
        End If

    End Sub

    Sub seleccionarDepositoDataGrid()

        On Error Resume Next 'ErrSel

        'Obtengo la fila del DataGridBuscarDeposito que el usuario selecciono 
        'Asi podre obtener el codigo y descripcion del deposito para colocarlo en el formProveedorDeposito
        filaDataGrid = FormBuscarDeposito.DataGridBuscarDeposito.CurrentRowIndex

        If filaDataGrid = -1 Then
            MsgBox("No seleccionó ningún deposito.", MsgBoxStyle.Information, "isMOBILE")
        Else
            If FormBuscarDeposito.Owner.Equals(FormProveedorDeposito_Recepcion) Then
                FormProveedorDeposito_Recepcion.txtDeposito.Text = FormBuscarDeposito.DataGridBuscarDeposito.Item(filaDataGrid, 0)
                FormProveedorDeposito_Recepcion.lblDescripcionDeposito.Text = "Descripción: " + FormBuscarDeposito.DataGridBuscarDeposito.Item(filaDataGrid, 1)
                FormBuscarDeposito.txtDescripcionDeposito.Text = ""
                FormBuscarDeposito.DataGridBuscarDeposito.DataSource = Nothing
                FormBuscarDeposito.Close()
                FormProveedorDeposito_Recepcion.Enabled = True
                FormProveedorDeposito_Recepcion.cmbSiguiente.Focus()
                FormProveedorDeposito_Recepcion.Show()
            Else
                If FormBuscarDeposito.Owner.Equals(FormDepositoOrigenDestino_Traslado) Then
                    If Deposito = "origen" Then
                        FormDepositoOrigenDestino_Traslado.txtDepositoOrigen.Text = FormBuscarDeposito.DataGridBuscarDeposito.Item(filaDataGrid, 0)
                        FormDepositoOrigenDestino_Traslado.lblDescripcionDepositoOrigen.Text = "Descripción: " + FormBuscarDeposito.DataGridBuscarDeposito.Item(filaDataGrid, 1)
                        FormBuscarDeposito.txtDescripcionDeposito.Text = ""
                        FormBuscarDeposito.DataGridBuscarDeposito.DataSource = Nothing
                        FormBuscarDeposito.Close()
                        FormDepositoOrigenDestino_Traslado.Enabled = True
                        FormDepositoOrigenDestino_Traslado.txtDepositoDestino.Focus()
                        FormDepositoOrigenDestino_Traslado.Show()
                    ElseIf Deposito = "destino" Then
                        FormDepositoOrigenDestino_Traslado.txtDepositoDestino.Text = FormBuscarDeposito.DataGridBuscarDeposito.Item(filaDataGrid, 0)
                        FormDepositoOrigenDestino_Traslado.lblDescripcionDepositoDestino.Text = "Descripción: " + FormBuscarDeposito.DataGridBuscarDeposito.Item(filaDataGrid, 1)
                        FormBuscarDeposito.txtDescripcionDeposito.Text = ""
                        FormBuscarDeposito.DataGridBuscarDeposito.DataSource = Nothing
                        FormBuscarDeposito.Close()
                        FormDepositoOrigenDestino_Traslado.Enabled = True
                        FormDepositoOrigenDestino_Traslado.cmbSiguiente.Focus()
                        FormDepositoOrigenDestino_Traslado.Show()
                    End If
                End If
            End If
        End If

        Exit Sub

        'ErrSel:

        'Err.Clear()

    End Sub

    Sub LlenaDataGridBuscarUbicacion(Optional ByVal pCodDeposito As String = "")

        If conectarDB_SERVIDOR(rutaDB_SERVIDOR) = 1 Then

            Dim pFiltroDeposito As String = String.Empty

            If pCodDeposito.Trim.Length > 0 Then
                pFiltroDeposito = " AND cu_Deposito = @Deposito"
            End If

            comandoBD_SERVIDOR.Parameters.Clear()

            Dim TmpQuery As String = "SELECT DISTINCT cu_Mascara AS Ubicacion FROM MA_UBICACIONxPRODUCTO WHERE 1 = 1"

            If pCodDeposito.Trim.Length > 0 Then
                TmpQuery += vbNewLine & pFiltroDeposito
                comandoBD_SERVIDOR.Parameters.Add("@Deposito", pCodDeposito)
            End If

            TmpQuery += " ORDER BY cu_Mascara"

            dataSet_Servidor = ejecutaQuery_Servidor(comandoBD_SERVIDOR, "MA_UBICACIONxPRODUCTO")

            FormBuscarUbicacion.DataGridUbicaciones.DataSource = dataSet_Servidor.Tables(0)

            Dim Estilos As New DataGridTableStyle

            Dim Columna1 As New DataGridTextBoxColumn

            Estilos.MappingName = dataSet_Servidor.Tables(0).TableName

            With Columna1
                .MappingName = dataSet_Servidor.Tables(0).Columns(0).ColumnName
                .HeaderText = "Ubicacion"
                .Width = FormBuscarUbicacion.DataGridUbicaciones.Width * 0.9
            End With

            Estilos.GridColumnStyles.Add(Columna1)

            FormBuscarUbicacion.DataGridUbicaciones.TableStyles.Clear()
            FormBuscarUbicacion.DataGridUbicaciones.TableStyles.Add(Estilos)

            ' For para ajustar el tamaño de las columnas contenidas en el dataSet_PDA al campo mas largo
            'Dim i As Integer

            'For i = 0 To (dataSet_Servidor.Tables(0).Columns.Count - 1)
            'nuevoAncho = obtenerCampoMasLargo(dataSet_Servidor, 0, i, FormBuscarUbicacion.DataGridUbicaciones)
            'FormBuscarUbicacion.DataGridUbicaciones.TableStyles(dataSet_Servidor.Tables(0).TableName).GridColumnStyles(i).Width = nuevoAncho
            'Next i

            If dataSet_Servidor.Tables(0).Rows.Count() = 0 Then
                MsgBox("Búsqueda sin éxito.", MsgBoxStyle.Information, "isMOBILE")
                SeleccionarTexto(FormBuscarUbicacion.txtUbicacion)
                FormBuscarUbicacion.txtUbicacion.Focus()
            End If

        Else
            MsgBox("No se puede realizar la búsqueda, no hay conexión con el servidor.", MsgBoxStyle.Information, "isMOBILE")
            SeleccionarTexto(FormBuscarUbicacion.txtUbicacion)
        End If

    End Sub

    Sub llenaDataGridBuscarODC(ByVal codigoProveedor)

        Dim Query As String = "SELECT d_fecha, c_documento, MA_PROVEEDORES.c_descripcio, d_fecha_recepcion, " + _
        "MA_DEPOSITO.c_Descripcion, MA_DEPOSITO.c_CodDeposito, c_codproveedor " + _
        "FROM MA_ODC, MA_PROVEEDORES, MA_DEPOSITO " + _
        "WHERE c_Status = 'DPE' AND c_CodProveedor LIKE @c_codproveed " + _
        "AND MA_ODC.c_codproveedor = MA_PROVEEDORES.c_codproveed " + _
        "AND MA_DEPOSITO.c_coddeposito = MA_ODC.c_despachar " + _
        "AND MA_ODC.c_codlocalidad = @localidad " + _
        "ORDER BY c_codproveedor"

        If CompatibilidadFueraDeLinea Then

            comandoBD_PDA.Parameters.Clear()
            comandoBD_PDA.CommandText = Query
            comandoBD_PDA.Parameters.Add("@c_codproveed", codigoProveedor)
            comandoBD_PDA.Parameters.Add("@localidad", Localidad)
            dataSet_PDA = consultaQuery_PDA(comandoBD_PDA)

        Else

            comandBD_SERVIDOR.Parameters.Clear()
            comandBD_SERVIDOR.CommandText = Query
            comandBD_SERVIDOR.Parameters.Add("@c_codproveed", codigoProveedor)
            comandBD_SERVIDOR.Parameters.Add("@localidad", Localidad)
            dataSet_PDA = executeQuery_Servidor(comandBD_SERVIDOR)

        End If

        ''Para guardar el resultado de una consulta en una variable tipo string empleando la funcion CStr, pero solo el valor de la primera fila y columna empleando la funcion ExecuteScalar
        'comandoBD_PDA.CommandText = "SELECT c_descripcion FROM MA_DEPOSITO, MA_PROVEEDORES, MA_ODC WHERE c_codproveed LIKE @c_codproveedor AND c_status='DPE' AND c_codproveed=c_codproveedor AND c_coddeposito=c_despachar AND MA_ODC.c_codlocalidad= @localidad"
        'comandoBD_PDA.Parameters.Add("@c_codproveedor", codigoProveedor)
        'comandoBD_PDA.Parameters.Add("@localidad", Localidad)
        'descripcionDeposito = CStr(comandoBD_PDA.ExecuteScalar())
        'comandoBD_PDA.Parameters.Clear()

        'comandoBD_PDA.CommandText = "SELECT c_coddeposito FROM MA_DEPOSITO, MA_PROVEEDORES, MA_ODC WHERE c_codproveed LIKE @c_codproveedor AND c_status='DPE' AND c_codproveed=c_codproveedor AND c_coddeposito=c_despachar AND MA_ODC.c_codlocalidad= @localidad"
        'comandoBD_PDA.Parameters.Add("@c_codproveedor", codigoProveedor)
        'comandoBD_PDA.Parameters.Add("@localidad", Localidad)
        'codigoDeposito = CStr(comandoBD_PDA.ExecuteScalar())
        'comandoBD_PDA.Parameters.Clear()

        'comandoBD_PDA.CommandText = "SELECT c_codproveedor FROM MA_DEPOSITO, MA_PROVEEDORES, MA_ODC WHERE c_codproveed LIKE @c_codproveedor AND c_status='DPE' AND c_codproveed=c_codproveedor AND c_coddeposito=c_despachar AND MA_ODC.c_codlocalidad= @localidad"
        'comandoBD_PDA.Parameters.Add("@c_codproveedor", codigoProveedor)
        'comandoBD_PDA.Parameters.Add("@localidad", Localidad)
        'codigoProveedorCompleto = CStr(comandoBD_PDA.ExecuteScalar())
        'comandoBD_PDA.Parameters.Clear()

        ' Que desperdicio de recursos ejecutar la misma consulta 4 veces -_-

        'En la propiedad DataSource es donde puedo llenar el dataGrid con la informacion de la BD.
        FormBuscarODC_Recepcion.DataGridBuscarODC.DataSource = dataSet_PDA.Tables(0)

        dataSet_PDA.Tables(0).Columns(0).ColumnName = "FECHA"
        dataSet_PDA.Tables(0).Columns(1).ColumnName = "DOCUMENTO"
        dataSet_PDA.Tables(0).Columns(2).ColumnName = "PROVEEDOR"
        dataSet_PDA.Tables(0).Columns(3).ColumnName = "FECHA RECEPCIÓN"
        dataSet_PDA.Tables(0).Columns(4).ColumnName = "DEPOSITO"
        dataSet_PDA.Tables(0).Columns(5).ColumnName = "COD_DEPOSITO"
        dataSet_PDA.Tables(0).Columns(6).ColumnName = "COD_PROVEEDOR"

        Dim Estilos As New DataGridTableStyle
        Dim columna1, columna2, columna3, columna4, columna5, columna6, columna7 As New DataGridTextBoxColumn
        Estilos.MappingName = dataSet_PDA.Tables(0).TableName

        With columna1
            .MappingName = dataSet_PDA.Tables(0).Columns(0).ColumnName
            .HeaderText = "FECHA"
        End With

        With columna2
            .MappingName = dataSet_PDA.Tables(0).Columns(1).ColumnName
            .HeaderText = "DOCUMENTO"
        End With

        With columna3
            .MappingName = dataSet_PDA.Tables(0).Columns(2).ColumnName
            .HeaderText = "PROVEEDOR"
        End With

        With columna4
            .MappingName = dataSet_PDA.Tables(0).Columns(3).ColumnName
            .HeaderText = "FECHA_RECEPCIÓN"
        End With

        With columna5
            .MappingName = dataSet_PDA.Tables(0).Columns(4).ColumnName
            .HeaderText = "DEPOSITO"
            .Width = 0
        End With

        With columna6
            .MappingName = dataSet_PDA.Tables(0).Columns(5).ColumnName
            .HeaderText = "COD_DEPOSITO"
        End With

        With columna7
            .MappingName = dataSet_PDA.Tables(0).Columns(6).ColumnName
            .HeaderText = "COD_PROVEEDOR"
        End With

        'Sólo se agregan al DataGrid las columnas que se quiere que aparezcan en pantalla, las demás columnas fueron creadas para utilizar su informacioón mas adelante
        Estilos.GridColumnStyles.Add(columna1)
        Estilos.GridColumnStyles.Add(columna2)
        Estilos.GridColumnStyles.Add(columna3)
        Estilos.GridColumnStyles.Add(columna4)
        Estilos.GridColumnStyles.Add(columna5)
        Estilos.GridColumnStyles.Add(columna6)
        Estilos.GridColumnStyles.Add(columna7)

        FormBuscarODC_Recepcion.DataGridBuscarODC.TableStyles.Clear()
        FormBuscarODC_Recepcion.DataGridBuscarODC.TableStyles.Add(Estilos)

        Dim i As Integer

        For i = 0 To 3 ' No considerar las ocultas.
            nuevoAncho = obtenerCampoMasLargo(dataSet_PDA, 0, i)
            FormBuscarODC_Recepcion.DataGridBuscarODC.TableStyles(dataSet_PDA.Tables(0).TableName).GridColumnStyles(i).Width = nuevoAncho
        Next i

        If dataSet_PDA.Tables(0).Rows.Count() = 0 Then
            MsgBox("Búsqueda sin éxito", MsgBoxStyle.Information, "isMOBILE")
            FormProveedorDeposito_Recepcion.Show()
        End If

    End Sub

    Function verificarProveedorPoseeODC(ByVal codigoProveedor) As Integer

        'Verifico si el proveedor posee ODC

        Dim Query As String = "SELECT c_documento from MA_ODC where c_codproveedor = @c_codproveed and c_status = 'DPE' AND c_codlocalidad = @localidad"

        If CompatibilidadFueraDeLinea Then
            comandoBD_PDA.Parameters.Clear()
            comandoBD_PDA.CommandText = Query
            comandoBD_PDA.Parameters.Add("@c_codproveed", codigoProveedor)
            comandoBD_PDA.Parameters.Add("@localidad", Localidad)
            dataSet_PDA = consultaQuery_PDA(comandoBD_PDA)
        Else
            comandBD_SERVIDOR.Parameters.Clear()
            comandBD_SERVIDOR.CommandText = Query
            comandBD_SERVIDOR.Parameters.Add("@c_codproveed", codigoProveedor)
            comandBD_SERVIDOR.Parameters.Add("@localidad", Localidad)
            dataSet_PDA = executeQuery_Servidor(comandBD_SERVIDOR)
        End If

        'si la consulta trajo resultados, le pregunto al usuario si desea importar las ODC
        If Not (dataSet_PDA.Tables(0).Rows.Count() = 0) Then
            Return 1
        Else
            Return 0
            FormProveedorDeposito_Recepcion.Show()
        End If

    End Function

    Sub seleccionarODCDataGrid()

        'Obtengo la fila del DataGridBuscarODC que el usuario selecciono 
        'Asi podre obtener los datos de la ODC para colocarlo en el formProveedorDeposito
        filaDataGrid = FormBuscarODC_Recepcion.DataGridBuscarODC.CurrentRowIndex

        If filaDataGrid = -1 Then
            MsgBox("No seleccionó ninguna ODC.", MsgBoxStyle.Information, "isMOBILE")
        Else
            'FormProveedorDeposito_Recepcion.txtProveedor.Text = codigoProveedorCompleto
            FormProveedorDeposito_Recepcion.txtProveedor.Text = FormBuscarODC_Recepcion.DataGridBuscarODC.Item(filaDataGrid, 6)
            FormProveedorDeposito_Recepcion.lblDescripcionProveedor.Text = "Descripción: " + FormBuscarODC_Recepcion.DataGridBuscarODC.Item(filaDataGrid, 2)
            'FormProveedorDeposito_Recepcion.lblDescripcionDeposito.Text = "Descripción: " + descripcionDeposito
            FormProveedorDeposito_Recepcion.lblDescripcionDeposito.Text = "Descripción: " + FormBuscarODC_Recepcion.DataGridBuscarODC.Item(filaDataGrid, 4)
            'FormProveedorDeposito_Recepcion.txtDeposito.Text = codigoDeposito
            FormProveedorDeposito_Recepcion.txtDeposito.Text = FormBuscarODC_Recepcion.DataGridBuscarODC.Item(filaDataGrid, 5)

            'la variable codigoODC es para usarla mas adelante en el formulario lecturaProducto
            codigoODC = FormBuscarODC_Recepcion.DataGridBuscarODC.Item(filaDataGrid, 1)

            FormBuscarODC_Recepcion.DataGridBuscarODC.DataSource = Nothing
            FormProveedorDeposito_Recepcion.Show()
            FormBuscarODC_Recepcion.Visible = False
            FormProveedorDeposito_Recepcion.txtProveedor.Enabled = False
            FormProveedorDeposito_Recepcion.txtDeposito.Enabled = False
            FormProveedorDeposito_Recepcion.cmbBuscarDeposito.Enabled = False
            FormProveedorDeposito_Recepcion.cmbVerificarDeposito.Enabled = False
            FormProveedorDeposito_Recepcion.cmbSiguiente.Focus()
        End If

    End Sub

    Sub verificarExisteDocumento(ByVal codigoDocumento As String, ByVal codigoProveedor As String) 'Esta funcion es con los correlativos del servidor

        'cada vez que se ejecuta la funcion de verificar documento, actualizo la tabla con el correlativo que tenga la recepcion en el servidor
        'para mantener como valor de recepcion actual el mas actualizado

        If conectarDB_SERVIDOR(rutaDB_SERVIDOR) = 1 Then

            Dim Query As String = "SELECT nu_Valor FROM MA_CORRELATIVOS WHERE cu_Campo = 'Recepcion'"

            If CompatibilidadFueraDeLinea Then

                'comandoBD_PDA.CommandText = "DELETE MA_INVENTARIO"
                'ejecutaQuery_PDA(comandoBD_PDA)
                'actualizarMA_INVENTARIO() 'Para tener los numeros de documentos ya utilizados para cada proveedor, lo mas actualizado posible
                ' NOP. Tranca el programa.

                comandoBD_PDA.CommandText = "DELETE MA_CORRELATIVOS"
                ejecutaQuery_PDA(comandoBD_PDA)
                actualizarMA_CORRELATIVOS()

            End If

            FormDocumentoTransportista_Recepcion.ProgressBar_FormDocumentoTransportista.Value = 30

            If CompatibilidadFueraDeLinea Then

                comandoBD_PDA.Parameters.Clear()
                comandoBD_PDA.CommandText = Query
                dataSet_PDA = consultaQuery_PDA(comandoBD_PDA)

            Else

                comandBD_SERVIDOR.Parameters.Clear()
                comandBD_SERVIDOR.CommandText = Query
                dataSet_PDA = executeQuery_Servidor(comandBD_SERVIDOR)

            End If

            numeroRecepcion_SERVIDORsinFormato = (dataSet_PDA.Tables(0).Rows(0)("nu_valor")) + 1
            numeroRecepcion_SERVIDOR = Format(numeroRecepcion_SERVIDORsinFormato, "00000000#")

            FormDocumentoTransportista_Recepcion.ProgressBar_FormDocumentoTransportista.Value = 60

            comandoBD_SERVIDOR.CommandText = _
            "SELECT C_DOCUMENTO FROM MA_INVENTARIO " + _
            "WHERE C_CODPROVEEDOR = @codigoProveedor " + _
            "AND C_FACTURA = @codigoDocumento " + _
            "AND C_STATUS NOT IN ('ANU', 'DWT') " + _
            "AND C_DOCUMENTO <> @numeroRecepcion_SERVIDOR " + _
            "AND C_CONCEPTO = 'REC' AND C_CODLOCALIDAD = @localidad"

            comandoBD_SERVIDOR.Parameters.Add("@codigoProveedor", codigoProveedor)
            comandoBD_SERVIDOR.Parameters.Add("@codigoDocumento", codigoDocumento) 'Es el codigo que ingresa el usuario en el txtdocumento
            comandoBD_SERVIDOR.Parameters.Add("@numeroRecepcion_SERVIDOR", numeroRecepcion_SERVIDOR)
            comandoBD_SERVIDOR.Parameters.Add("@localidad", Localidad)

            dataSet_Servidor = executeQuery_Servidor(comandoBD_SERVIDOR)

            If Not (dataSet_Servidor.Tables(0).Rows.Count = 0) Then

                FormDocumentoTransportista_Recepcion.ProgressBar_FormDocumentoTransportista.Value = 100
                FormDocumentoTransportista_Recepcion.ProgressBar_FormDocumentoTransportista.Visible = False
                FormDocumentoTransportista_Recepcion.ProgressBar_FormDocumentoTransportista.Value = 0
                FormDocumentoTransportista_Recepcion.lblProgressBar_FormDocumentoTransportista.Visible = False

                MsgBox("El número de documento ya existe para este proveedor, por favor verifique sus datos.", MsgBoxStyle.Information, "isMOBILE")

                FormDocumentoTransportista_Recepcion.txtDocumento.Text = ""
                FormDocumentoTransportista_Recepcion.txtDocumento.Focus()

            Else
                'llamo a la funcion que construye el numero de recepcion en el pda para que quede guardado en la variable numeroRecepcion_PDA y sea utilizada
                'en los formularios posteriores

                FormDocumentoTransportista_Recepcion.ProgressBar_FormDocumentoTransportista.Value = 80

                Dim numeroRecepcion_PDA As String = CStr(construirNumeroRecepcion_PDA())

                FormLecturaProducto_Recepcion.lblNºRec.Text = CStr(numeroRecepcion_PDA)
                FormCantidadRecibida_Recepcion.lblNºRec.Text = CStr(numeroRecepcion_PDA)
                FormCantidadFacturada_Recepcion.lblNºRec.Text = CStr(numeroRecepcion_PDA)

                FormDocumentoTransportista_Recepcion.ProgressBar_FormDocumentoTransportista.Value = 100
                FormDocumentoTransportista_Recepcion.ProgressBar_FormDocumentoTransportista.Visible = False
                FormDocumentoTransportista_Recepcion.ProgressBar_FormDocumentoTransportista.Value = 0
                FormDocumentoTransportista_Recepcion.lblProgressBar_FormDocumentoTransportista.Visible = False

            End If

            cerrarConexion_SERVIDOR()

        Else

            MsgBox("No se puede actualizar el inventario ni los correlativos, no hay conexión con el servidor. Comuníquese con el departamento de sistemas.", MsgBoxStyle.Information, "isMOBILE")
            limpiarFormDocumentoTransportista()
            limpiarFormProveedorDeposito()
            FormDocumentoTransportista_Recepcion.Visible = False
            FormProveedorDeposito_Recepcion.Visible = False
            formMenuPrincipal.Show()

        End If

    End Sub

    Function buscarCodigoProducto(ByVal CodigoProducto As String) As Data.DataSet

        Dim Query As String = "SELECT c_CodNasa, n_Cantidad, c_Codigo FROM MA_CODIGOS WHERE c_Codigo = @c_codigo"

        If CompatibilidadFueraDeLinea Then
            comandoBD_PDA.Parameters.Clear()
            comandoBD_PDA.CommandText = Query
            comandoBD_PDA.Parameters.Add("@c_codigo", CodigoProducto)
            dataSet_PDA = consultaQuery_PDA(comandoBD_PDA)
        Else
            comandBD_SERVIDOR.Parameters.Clear()
            comandBD_SERVIDOR.CommandText = Query
            comandBD_SERVIDOR.Parameters.Add("@c_codigo", CodigoProducto)
            dataSet_PDA = executeQuery_Servidor(comandBD_SERVIDOR)
        End If

        Return dataSet_PDA

    End Function

    Function detallesProducto(ByVal CodigoProducto As String) As Data.DataSet

        Dim Query As String = "SELECT n_TipoPeso, c_Descri, n_Cantibul, n_CostoAct, n_CostoAnt, n_CostoPro, Cant_Decimales FROM MA_PRODUCTOS WHERE c_Codigo = @c_codigo"

        If CompatibilidadFueraDeLinea Then
            comandoBD_PDA.Parameters.Clear()
            comandoBD_PDA.CommandText = Query
            comandoBD_PDA.Parameters.Add("@c_codigo", CodigoProducto)
            dataSet_PDA = consultaQuery_PDA(comandoBD_PDA)
        Else
            comandBD_SERVIDOR.Parameters.Clear()
            comandBD_SERVIDOR.CommandText = Query
            comandBD_SERVIDOR.Parameters.Add("@c_codigo", CodigoProducto)
            dataSet_PDA = executeQuery_Servidor(comandBD_SERVIDOR)
        End If

        Return dataSet_PDA

    End Function

    Function detallesProducto_Servidor(ByVal CodigoProducto As String) As Data.DataSet
        comandBD_SERVIDOR.CommandText = "SELECT n_tipopeso, C_DESCRI,n_cantibul, n_costoact, n_costoant, n_costopro, cant_DECIMALES FROM MA_PRODUCTOS WHERE c_codigo = @c_codigo"
        comandBD_SERVIDOR.Parameters.Clear()
        comandBD_SERVIDOR.Parameters.Add("@c_codigo", CodigoProducto)
        dataSet_Servidor = executeQuery_Servidor(comandBD_SERVIDOR)
        Return dataSet_Servidor
    End Function

    Function detallesODC() As Data.DataSet ' ?? NO SE ESTA USANDO.

        If codigoODC = "" Then
            codigoODC = "0"
        End If

        comandoBD_PDA.CommandText = "SELECT TR_ODC.c_documento, MA_INVENTARIO.c_documento, c_codarticulo, c_linea, TR_ODC.cs_codlocalidad, n_cantidad, n_cant_recibida, ns_cantidadEmpaque, ma_productos.c_descri, ma_productos.cant_decimales FROM TR_ODC, MA_INVENTARIO, MA_PRODUCTOS WHERE MA_INVENTARIO.c_documento= @numeroFactura AND TR_ODC.cs_codlocalidad= @localidad AND TR_ODC.c_documento=@codigoODC AND TR_ODC.c_codarticulo= MA_PRODUCTOS.c_codigo ORDER BY c_linea"
        comandoBD_PDA.Parameters.Add("@numeroFactura", numeroRecepcion_SERVIDOR)
        comandoBD_PDA.Parameters.Add("@codigoODC", codigoODC)
        comandoBD_PDA.Parameters.Add("@localidad", Localidad)
        dataSet_PDA = consultaQuery_PDA(comandoBD_PDA)

        If codigoODC = "0" Then
            codigoODC = ""
        End If

        Return dataSet_PDA

    End Function

    Function consultarRegladeNegocio(ByVal RegladeNegocio As String)

        Dim Query As String = "SELECT Valor FROM MA_REGLASDENEGOCIO WHERE campo = @RegladeNegocio"

        If CompatibilidadFueraDeLinea Then
            comandoBD_PDA.Parameters.Clear()
            comandoBD_PDA.CommandText = Query
            comandoBD_PDA.Parameters.Add("@RegladeNegocio", RegladeNegocio)
            dataSet_PDA = consultaQuery_PDA(comandoBD_PDA)
        Else
            comandBD_SERVIDOR.Parameters.Clear()
            comandBD_SERVIDOR.CommandText = Query
            comandBD_SERVIDOR.Parameters.Add("@RegladeNegocio", RegladeNegocio)
            dataSet_PDA = executeQuery_Servidor(comandBD_SERVIDOR)
        End If

        Return dataSet_PDA

    End Function

    Public Function Redondear_Precio(ByVal Valor As Double, ByVal Imp1 As Double, ByVal Imp2 As Double, ByVal Imp3 As Double) As Double

        Dim ConsideraImp As Boolean = obtenerRegladeNegocio_Servidor("PDA_ReglasComerciales_ConsideraImpuesto", "0") = "1"

        Dim valor1, valor2, valor3 As Integer

        If ConsideraImp Then
            valor1 = (Valor * Imp1) / 100
            valor2 = (Valor * Imp2) / 100
            valor3 = (Valor * Imp3) / 100
            Valor = FormatNumber((Valor + valor1 + valor2 + valor3), 2, TriState.True, TriState.False, TriState.False)
        End If

        Dim TipoRedondeo As Integer = Val(obtenerRegladeNegocio_Servidor("PDA_ReglasComerciales_TipoRedondeo", "0"))
        Dim RedondearMitad As Boolean = obtenerRegladeNegocio_Servidor("PDA_ReglasComerciales_RedondearMitad", "0") = "1"
        Dim Variar9 As Boolean = obtenerRegladeNegocio_Servidor("PDA_ReglasComerciales_VariarA9", "0") = "1"

        Select Case TipoRedondeo

            '*******************************************
            '***  DECIMALES
            '*******************************************

            Case Is = 0

                Valor = FormatNumber(Valor, 1, TriState.True, TriState.False, TriState.False)

                '*******************************************
                '***  UNIDADES
                '*******************************************
            Case Is = 1

                Valor = FormatNumber(Valor, 0, TriState.True, TriState.False, TriState.False)

                If Right(Valor, 1) >= 5 Or Len(Valor) < 2 Then
                    If RedondearMitad = True And Right(Valor, 1) <= 5 Then
                        Valor = Valor + (5 - (Right(Valor, 1)))
                    Else
                        Valor = Valor + (10 - (Right(Valor, 1)))
                    End If
                Else
                    If RedondearMitad = True And Right(Valor, 1) > 0 Then
                        Valor = Valor + (5 - (Right(Valor, 1)))
                    Else
                        Valor = Valor - (Right(Valor, 1))
                    End If
                End If

                '*******************************************
                '***  DECENAS
                '*******************************************
            Case Is = 2

                Valor = FormatNumber(Valor, 0, TriState.True, TriState.False, TriState.False)

                If Right(Valor, 2) >= 50 Or Len(Valor) < 3 Then

                    If RedondearMitad = True And Right(Valor, 2) <= 50 Then
                        Valor = Valor + (50 - (Right(Valor, 2)))
                    Else
                        Valor = Valor + (100 - (Right(Valor, 2)))
                    End If

                Else
                    If RedondearMitad And Right(Valor, 2) <= 50 Then
                        Valor = Valor + (50 - (Right(Valor, 2)))
                    Else
                        Valor = Valor - (Right(Valor, 2))
                    End If
                End If

                '*******************************************
                '***  CENTENAS
                '*******************************************
            Case Is = 3

                Valor = FormatNumber(Valor, 0, TriState.True, TriState.False, TriState.False)

                If Right(Valor, 3) >= 500 Or Len(Valor) < 4 Then

                    If RedondearMitad = True And Right(Valor, 3) <= 500 Then
                        Valor = Valor + (500 - (Right(Valor, 3)))
                    Else
                        Valor = Valor + (1000 - (Right(Valor, 3)))
                    End If

                Else
                    If RedondearMitad = True And Right(Valor, 3) <= 500 Then
                        Valor = Valor + (500 - (Right(Valor, 3)))
                    Else
                        Valor = Valor - (Right(Valor, 3))
                    End If
                End If

        End Select

        '***** PARA VARIAR A 9
        If Variar9 = True Then
            Valor = Valor - 1
        End If

        If ConsideraImp Then
            Dim rim As Double = FormatNumber((Valor * (Imp1 + Imp2 + Imp3)) / (100 + (Imp1 + Imp2 + Imp3)), 2, TriState.True, TriState.False, TriState.False)
            Valor = Valor - rim
            Redondear_Precio = FormatNumber(Valor, 2, TriState.True, TriState.False, TriState.False)
        Else
            Redondear_Precio = FormatNumber(Valor, 2, TriState.True, TriState.False, TriState.False)
        End If

    End Function

    Function BuscarProductoenODC(ByVal codigoProducto)

        Dim Query As String = "SELECT c_CodArticulo, c_Linea, n_Cantidad, n_Cant_Recibida " + _
        "FROM TR_ODC " + _
        "WHERE c_CodArticulo = @codigoProducto " + _
        "AND c_Documento = @codigoODC"

        'Con esta consulta se pueden tener mas de un registro en caso de que el mismo producto esté en mas de una linea

        If codigoODC = "" Then
            codigoODC = "0"
        End If

        If CompatibilidadFueraDeLinea Then
            comandoBD_PDA.CommandText = Query
            comandoBD_PDA.Parameters.Add("@codigoProducto", codigoProducto)
            comandoBD_PDA.Parameters.Add("@codigoODC", codigoODC)
            dataSet_PDA = consultaQuery_PDA(comandoBD_PDA)
        Else
            comandBD_SERVIDOR.CommandText = Query
            comandBD_SERVIDOR.Parameters.Add("@codigoProducto", codigoProducto)
            comandBD_SERVIDOR.Parameters.Add("@codigoODC", codigoODC)
            dataSet_PDA = executeQuery_Servidor(comandBD_SERVIDOR)
        End If

        If codigoODC = "0" Then
            codigoODC = ""
        End If

        Return dataSet_PDA

    End Function

    Function detallesProductoenODC(ByVal linea, ByVal codigoProducto)

        If codigoODC = "" Then
            codigoODC = "0"
        End If

        comandoBD_PDA.CommandText = "SELECT n_cantidad, n_cant_recibida, c_documento FROM TR_ODC WHERE c_codarticulo= @codigoProducto AND c_documento= @codigoODC AND c_linea = @linea"
        comandoBD_PDA.Parameters.Add("@codigoProducto", codigoProducto)
        comandoBD_PDA.Parameters.Add("@codigoODC", codigoODC)
        comandoBD_PDA.Parameters.Add("@linea", linea)
        dataSet_PDA = consultaQuery_PDA(comandoBD_PDA)

        If codigoODC = "0" Then
            codigoODC = ""
        End If
        Return dataSet_PDA

    End Function

    Function consultarNivelUsuario(ByVal Usuario, ByVal Contraseña)

        Dim Query As String = "SELECT nivel FROM MA_USUARIOS WHERE login_name = @Usuario AND password = @Contraseña"

        If CompatibilidadFueraDeLinea Then
            comandoBD_PDA.Parameters.Clear()
            comandoBD_PDA.CommandText = Query
            comandoBD_PDA.Parameters.Add("@Usuario", Usuario)
            comandoBD_PDA.Parameters.Add("@Contraseña", Contraseña)
            dataSet_PDA = consultaQuery_PDA(comandoBD_PDA)
        Else
            comandBD_SERVIDOR.Parameters.Clear()
            comandBD_SERVIDOR.CommandText = Query
            comandBD_SERVIDOR.Parameters.Add("@Usuario", Usuario)
            comandBD_SERVIDOR.Parameters.Add("@Contraseña", Contraseña)
            dataSet_PDA = executeQuery_Servidor(comandBD_SERVIDOR)
        End If

        Return dataSet_PDA

    End Function

    Function consultarNivelUsuario_Servidor(ByVal Usuario, ByVal Contraseña)

        comandBD_SERVIDOR.Parameters.Clear()
        comandBD_SERVIDOR.CommandText = "SELECT nivel FROM MA_USUARIOS WHERE login_name = @Usuario AND password = @Contraseña"
        comandBD_SERVIDOR.Parameters.Add("@Usuario", Usuario)
        comandBD_SERVIDOR.Parameters.Add("@Contraseña", Contraseña)

        dataSet_Servidor = executeQuery_Servidor(comandBD_SERVIDOR)

        Return dataSet_Servidor

    End Function

    Sub limpiarFormLecturaProducto()

        FormLecturaProducto_Recepcion.txtCodigoProducto.Text = ""
        If FormLecturaProducto_Recepcion.txtCodigoProducto.Enabled = False Then
            FormLecturaProducto_Recepcion.txtCodigoProducto.Enabled = True
        End If
        FormLecturaProducto_Recepcion.txtCodigoProducto.Focus()
        FormLecturaProducto_Recepcion.lblDescripcion.Text = "Descripción:"
    End Sub

    Sub limpiarFormCantidadFacturada()
        FormCantidadFacturada_Recepcion.txtEmpaquesFacturados.Text = ""
        FormCantidadFacturada_Recepcion.txtUnidadesFacturadas.Text = ""
        FormCantidadFacturada_Recepcion.lblUnidadesSolicitadas.Text = "Uni. Sol.:"
        FormCantidadFacturada_Recepcion.lblEmpaquesSolicitados.Text = "Emp. Sol.:"
        FormCantidadFacturada_Recepcion.lblCantidadFaltante.Text = "Cant. Faltante:"
    End Sub

    Sub limpiarFormCantidadRecibida()
        FormCantidadRecibida_Recepcion.txtEmpaquesRecibidos.Text = ""
        FormCantidadRecibida_Recepcion.txtUnidadesRecibidas.Text = ""
        FormCantidadRecibida_Recepcion.lblUnidadesSolicitadas.Text = "Uni. Sol.:"
        FormCantidadRecibida_Recepcion.lblEmpaquesSolicitados.Text = "Emp. Sol.:"
        FormCantidadRecibida_Recepcion.lblCantidadFaltante.Text = "Cant. Faltante:"
    End Sub

    Sub limpiarFormUsuario()
        formUsuario.txtUsuario.Text = ""
        formUsuario.txtContraseña.Text = ""
    End Sub

    Sub limpiarFormGuardarDatos()
        FormGuardarDatos_Recepcion.lblCantidadEmpaquesRecibidos.Text = ""
        FormGuardarDatos_Recepcion.lblCantidadUnidadesRecibidas.Text = ""
        FormGuardarDatos_Recepcion.lblCantidadUnidadesFacturadas.Text = ""
        FormGuardarDatos_Recepcion.lblCantidadEmpaquesFacturados.Text = ""
        FormGuardarDatos_Recepcion.lblDescripcionProducto.Text = ""
        FormGuardarDatos_Recepcion.lblUnidadesxEmpaque.Text = "Uni.x Emp.:"
    End Sub

    Sub limpiarFormProveedorDeposito()
        FormProveedorDeposito_Recepcion.txtProveedor.Text = ""
        FormProveedorDeposito_Recepcion.lblDescripcionProveedor.Text = "Descripción:"
        FormProveedorDeposito_Recepcion.txtDeposito.Text = ""
        FormProveedorDeposito_Recepcion.lblDescripcionDeposito.Text = "Descripción:"
    End Sub

    Sub limpiarFormDocumentoTransportista()
        FormDocumentoTransportista_Recepcion.txtDocumento.Text = ""
        FormDocumentoTransportista_Recepcion.txtTransportista.Text = ""
        FormDocumentoTransportista_Recepcion.lblProgressBar_FormDocumentoTransportista.Visible = False
        FormDocumentoTransportista_Recepcion.ProgressBar_FormDocumentoTransportista.Value = 0
        FormDocumentoTransportista_Recepcion.ProgressBar_FormDocumentoTransportista.Visible = False
    End Sub

    Function obtenerRegistrosMA_RECEPCIONESMOVIL_PDA(ByVal numeroRecepcion_PDA)

        comandoBD_PDA.CommandText = "SELECT * FROM MA_RECEPCIONESMOVIL " + _
        "WHERE numeroRecepcion_PDA = @numeroRecepcion_PDA " + _
        "AND codigoProveedor = @codigoProveedor AND codigoProducto = @codigoProducto"
        comandoBD_PDA.Parameters.Add("@numeroRecepcion_PDA", numeroRecepcion_PDA)
        comandoBD_PDA.Parameters.Add("@codigoProveedor", FormProveedorDeposito_Recepcion.txtProveedor.Text)
        comandoBD_PDA.Parameters.Add("@codigoProducto", FormLecturaProducto_Recepcion.txtCodigoProducto.Text)
        dataSet_PDA = consultaQuery_PDA(comandoBD_PDA)

        Return dataSet_PDA

    End Function

    Sub insertarRegistrosMA_RECEPCIONESMOVIL_PDA(ByVal cantidadTotalUnidadesRecibidas, ByVal cantidadTotalEmpaquesRecibidos, ByVal numeroRecepcion_PDA)

        comandoBD_PDA.CommandText = "INSERT INTO MA_RECEPCIONESMOVIL VALUES (@codigoProveedor, @codigoODC, @codigoProducto, @cantidadUnidades, @cantidadEmpaques, @facturaUnidades, @facturaEmpaques, @transportista, @codigoLocalidad, @codigoDeposito, @documento, @numeroRecepcion_PDA, @UnidadesxEmpaque)"
        comandoBD_PDA.Parameters.Add("@codigoProveedor", FormProveedorDeposito_Recepcion.txtProveedor.Text)
        comandoBD_PDA.Parameters.Add("@codigoODC", codigoODC)
        comandoBD_PDA.Parameters.Add("@codigoProducto", FormLecturaProducto_Recepcion.txtCodigoProducto.Text)
        comandoBD_PDA.Parameters.Add("@cantidadUnidades", cantidadTotalUnidadesRecibidas)
        comandoBD_PDA.Parameters.Add("@cantidadEmpaques", cantidadTotalEmpaquesRecibidos)
        comandoBD_PDA.Parameters.Add("@facturaUnidades", FormCantidadFacturada_Recepcion.txtUnidadesFacturadas.Text)
        comandoBD_PDA.Parameters.Add("@facturaEmpaques", FormCantidadFacturada_Recepcion.txtEmpaquesFacturados.Text)
        comandoBD_PDA.Parameters.Add("@transportista", FormDocumentoTransportista_Recepcion.txtTransportista.Text)
        comandoBD_PDA.Parameters.Add("@codigoLocalidad", Localidad)
        comandoBD_PDA.Parameters.Add("@codigoDeposito", FormProveedorDeposito_Recepcion.txtDeposito.Text)
        comandoBD_PDA.Parameters.Add("@documento", FormDocumentoTransportista_Recepcion.txtDocumento.Text)
        comandoBD_PDA.Parameters.Add("@numeroRecepcion_PDA", numeroRecepcion_PDA)
        comandoBD_PDA.Parameters.Add("@UnidadesxEmpaque", UnidadesxEmpaque)
        ejecutaQuery_PDA(comandoBD_PDA)

    End Sub

    Sub ActualizarMA_RECEPCIONESMOVIL_PDA(ByVal cantidadTotalUnidadesRecibidas, ByVal cantidadTotalEmpaquesRecibidos, ByVal numeroRecepcion_PDA)

        comandoBD_PDA.CommandText = "UPDATE MA_RECEPCIONESMOVIL SET cantidadUnidades = @cantidadUnidades, cantidadEmpaques = @cantidadEmpaques WHERE codigoProveedor = @codigoProveedor AND numeroRecepcion_PDA = @numeroRecepcion_PDA AND codigoProducto = @codigoProducto"
        comandoBD_PDA.Parameters.Add("@cantidadUnidades", cantidadTotalUnidadesRecibidas)
        comandoBD_PDA.Parameters.Add("@cantidadEmpaques", cantidadTotalEmpaquesRecibidos)
        comandoBD_PDA.Parameters.Add("@codigoProveedor", FormProveedorDeposito_Recepcion.txtProveedor.Text)
        comandoBD_PDA.Parameters.Add("@codigoProducto", FormLecturaProducto_Recepcion.txtCodigoProducto.Text)
        comandoBD_PDA.Parameters.Add("@numeroRecepcion_PDA", numeroRecepcion_PDA)
        ejecutaQuery_PDA(comandoBD_PDA)

    End Sub

    Sub insertaryActualizarRegistrosMA_RECEPCIONESMOVIL(ByVal numeroRecepcion_PDA)

        Dim numeroRecepcion_PDAaEnviar As String = numeroRecepcion_PDA

        If conectarDB_SERVIDOR(rutaDB_SERVIDOR) = 1 Then
            'If conectarDB_SERVIDOR(rutaDB_SERVIDOR) = 0 Then 'prueba
            dataset_MA_RECEPCIONESMOVIL_SERVIDOR = obtenerRegistrosMA_RECEPCIONESMOVIL_SERVIDOR(numeroRecepcion_PDAaEnviar)

            If dataset_MA_RECEPCIONESMOVIL_SERVIDOR.Tables(0).Rows.Count = 0 Then
                insertarRegistrosMA_RECEPCIONESMOVIL_SERVIDOR(numeroRecepcion_PDA)
            Else 'Ya el producto fue recibido anteriormente y no se ha procesado la recepcion en STELLAR
                cantidadEmpaquesRecibidosAnterior = dataset_MA_RECEPCIONESMOVIL_SERVIDOR.Tables(0).Rows(0)("cantidadEmpaques")
                cantidadUnidadesRecibidasAnterior = dataset_MA_RECEPCIONESMOVIL_SERVIDOR.Tables(0).Rows(0)("cantidadUnidades")
                cantidadTotalEmpaquesRecibidos = CDbl(FormGuardarDatos_Recepcion.lblCantidadEmpaquesRecibidos.Text) + CDbl(cantidadEmpaquesRecibidosAnterior)
                cantidadTotalUnidadesRecibidas = CDbl(FormGuardarDatos_Recepcion.lblCantidadUnidadesRecibidas.Text) + CDbl(cantidadUnidadesRecibidasAnterior)
                actualizarMA_RECEPCIONESMOVIL_SERVIDOR(cantidadTotalUnidadesRecibidas, cantidadTotalEmpaquesRecibidos, cantidadUnidadesRecibidasAnterior, cantidadEmpaquesRecibidosAnterior, numeroRecepcion_PDAaEnviar)
            End If

        Else 'No hay conexión con el servidor
            MsgBox("No se tiene conexión con el servidor, la información de este producto será guardado temporalmente de forma local y quedará pendiente por enviar", MsgBoxStyle.Information, "isMOBILE")

            'conectarDB_PDA("Data Source= " + rutaArchivoExe + "isMOBILE.sdf")

            dataset_RecepcionesMovil_PDA = obtenerRegistrosMA_RECEPCIONESMOVIL_PDA(numeroRecepcion_PDAaEnviar)

            If dataset_RecepcionesMovil_PDA.Tables(0).Rows.Count = 0 Then
                insertarRegistrosMA_RECEPCIONESMOVIL_PDA(cantidadTotalUnidadesRecibidas, cantidadTotalEmpaquesRecibidos, numeroRecepcion_PDAaEnviar)
            Else
                cantidadEmpaquesRecibidosAnterior = dataset_MA_RECEPCIONESMOVIL_PDA.Tables(0).Rows(0)("cantidadEmpaques")
                cantidadUnidadesRecibidasAnterior = dataset_MA_RECEPCIONESMOVIL_PDA.Tables(0).Rows(0)("cantidadUnidades") 'En caso de que ya se haya recibido unidades de ese producto
                cantidadTotalEmpaquesRecibidos = CDbl(FormGuardarDatos_Recepcion.lblCantidadEmpaquesRecibidos.Text) + CDbl(cantidadEmpaquesRecibidosAnterior)
                cantidadTotalUnidadesRecibidas = CDbl(FormGuardarDatos_Recepcion.lblCantidadUnidadesRecibidas.Text) + CDbl(cantidadUnidadesRecibidasAnterior)
                ActualizarMA_RECEPCIONESMOVIL_PDA(cantidadTotalUnidadesRecibidas, cantidadTotalEmpaquesRecibidos, numeroRecepcion_PDAaEnviar)
            End If
        End If

        cerrarConexion_SERVIDOR()

    End Sub

    Function determinarUnidadesxEmpaque(ByVal dataset_Codigos As Data.DataSet, ByVal dataset_Productos As Data.DataSet)

        If dataset_Codigos.Tables(0).Rows(0)("n_cantidad") > 1 Then
            UnidadesxEmpaque = dataset_Codigos.Tables(0).Rows(0)("n_cantidad")
        Else
            UnidadesxEmpaque = dataset_Productos.Tables(0).Rows(0)("n_cantibul")
        End If

        Return UnidadesxEmpaque

    End Function

    'Sub determinarCantidadFaltanteFormulariosconODC()
    '    'Este procedimiento es en caso de que se haga la recepcion nuevamente de un mismo producto para el mismo numero de factura 
    '    'y que todavia no se haya procesado en STELLAR, entonces se debe tomar en cuenta la cantidad recibida anteriormente y restarsela
    '    'al resultado de la resta de la cantidadsolicitada - cantidadrecibida (ya procesada)

    '    Dim cantidadRecibidaNoProcesada_SERVIDOR As Double
    '    Dim cantidadRecibidaNoProcesada_PDA As Double

    '    verFormCantidadFacturada = True

    '    'Verifico que no se haya recibido anteriormete el producto en la tabla del servidor

    '    If conectarDB_SERVIDOR(rutaDB_SERVIDOR) = 1 Then
    '        dataset_MA_RECEPCIONESMOVIL_SERVIDOR = obtenerRegistrosMA_RECEPCIONESMOVIL_SERVIDOR(FormCantidadRecibida_Recepcion.lblNºRec.Text) 'Es para un numero de recepción 
    '        'específico, no toma en cuenta el código de orden de compra. Por lo tanto si se recibe un producto con la misma orden de compra pero en otra recepcion, y la recepcion 
    '        'anterior no ha sido procesada en stellar, no se estaría restando la cantidad recibida anterior (Esto no es bueno creo) Deberia restar la cantidad recibida en la recepcion anterior
    '        'que no ha sido procesada para esa orden de compra (Pero esto ya es mucho pedir!)

    '        If dataset_MA_RECEPCIONESMOVIL_SERVIDOR.Tables(0).Rows.Count = 0 Then
    '            cantidadRecibidaNoProcesada_SERVIDOR = 0
    '            UnidadesFacturadasNoProcesadas_SERVIDOR = 0
    '            EmpaquesFacturadosNoProcesados_SERVIDOR = 0
    '        Else
    '            cantidadRecibidaNoProcesada_SERVIDOR = dataset_MA_RECEPCIONESMOVIL_SERVIDOR.Tables(0).Rows(0)("cantidadUnidades") + (dataset_MA_RECEPCIONESMOVIL_SERVIDOR.Tables(0).Rows(0)("cantidadEmpaques") * UnidadesxEmpaque)
    '            UnidadesFacturadasNoProcesadas_SERVIDOR = dataset_MA_RECEPCIONESMOVIL_SERVIDOR.Tables(0).Rows(0)("facturaUnidades")
    '            EmpaquesFacturadosNoProcesados_SERVIDOR = dataset_MA_RECEPCIONESMOVIL_SERVIDOR.Tables(0).Rows(0)("facturaEmpaques")
    '        End If

    '        'verifico que no se haya recibido anteriormente el producto en el pda
    '        dataset_MA_RECEPCIONESMOVIL_PDA = obtenerRegistrosMA_RECEPCIONESMOVIL_PDA(FormCantidadRecibida_Recepcion.lblNºRec.Text)

    '        If dataset_MA_RECEPCIONESMOVIL_PDA.Tables(0).Rows.Count = 0 Then
    '            cantidadRecibidaNoProcesada_PDA = 0
    '            UnidadesFacturadasNoProcesadas_PDA = 0
    '            EmpaquesFacturadosNoProcesados_PDA = 0
    '        Else
    '            cantidadRecibidaNoProcesada_PDA = dataset_MA_RECEPCIONESMOVIL_PDA.Tables(0).Rows(0)("cantidadUnidades") + (dataset_MA_RECEPCIONESMOVIL_PDA.Tables(0).Rows(0)("cantidadEmpaques") * UnidadesxEmpaque)
    '            UnidadesFacturadasNoProcesadas_PDA = dataset_MA_RECEPCIONESMOVIL_PDA.Tables(0).Rows(0)("facturaUnidades")
    '            EmpaquesFacturadosNoProcesados_PDA = dataset_MA_RECEPCIONESMOVIL_PDA.Tables(0).Rows(0)("facturaEmpaques")
    '        End If

    '        If cantidadRecibidaNoProcesada_SERVIDOR = 0 And cantidadRecibidaNoProcesada_PDA = 0 Then 'No se ha recibido el producto anteriormente para esa factura
    '            cantidadFaltanteFormularios = cantidadSolicitada - cantidadRecibida

    '            verificarCantidadRecibidaOtraRecepcion()
    '        Else
    '            cantidadFaltanteFormularios = cantidadSolicitada - cantidadRecibida - cantidadRecibidaNoProcesada_SERVIDOR - cantidadRecibidaNoProcesada_PDA
    '            verFormCantidadFacturada = False

    '            verificarCantidadRecibidaOtraRecepcion()
    '        End If

    '        configurarFormulariosFormadeTrabajoconODC()

    '        FormCantidadRecibida_Recepcion.lblUnidadesSolicitadas.Text = "Uni. Solc.: " + CStr(UnidadesSolicitadas)
    '        FormCantidadFacturada_Recepcion.lblUnidadesSolicitadas.Text = "Uni. Solc. :" + CStr(UnidadesSolicitadas)
    '        FormCantidadRecibida_Recepcion.lblEmpaquesSolicitados.Text = "Emp. Solc.: " + CStr(EmpaquesSolicitados)
    '        FormCantidadFacturada_Recepcion.lblEmpaquesSolicitados.Text = "Emp. Solc.: " + CStr(EmpaquesSolicitados)
    '        FormCantidadRecibida_Recepcion.lblCantidadFaltante.Text = "Cant. Faltante: " + CStr(cantidadFaltanteFormularios)
    '        FormCantidadFacturada_Recepcion.lblCantidadFaltante.Text = "Cant. Faltante: " + CStr(cantidadFaltanteFormularios)

    '        dataset_Productos = detallesProducto(FormLecturaProducto_Recepcion.txtCodigoProducto.Text)
    '        FormLecturaProducto_Recepcion.lblDescripcion.Text = "Descripción: " + CStr(dataset_Productos.Tables(0).Rows(0)("C_DESCRI"))
    '        FormLecturaProducto_Recepcion.txtCodigoProducto.Enabled = False
    '    Else
    '        MsgBox("No hay conexión con el servidor, no se puede determinar la cantidad faltante por recibir de este producto", MsgBoxStyle.Exclamation, "isMOBILE")
    '        dataset_Productos = detallesProducto(FormLecturaProducto_Recepcion.txtCodigoProducto.Text)
    '        FormLecturaProducto_Recepcion.txtCodigoProducto.Text = ""
    '        FormLecturaProducto_Recepcion.txtCodigoProducto.Focus()
    '    End If
    '    cerrarConexion_SERVIDOR()
    'End Sub

    Sub determinarCantidadFaltanteFormulariosconODC()

        verFormCantidadFacturada = True

        If conectarDB_SERVIDOR(rutaDB_SERVIDOR) = 1 Then

            cantidadFaltanteFormularios = cantidadSolicitada - cantidadRecibida

            verificarCantidadRecibidaOtraRecepcion()

            configurarFormulariosFormadeTrabajoconODC()

            FormCantidadRecibida_Recepcion.lblUnidadesSolicitadas.Text = "Uni. Solc.: " + CStr(UnidadesSolicitadas)
            FormCantidadFacturada_Recepcion.lblUnidadesSolicitadas.Text = "Uni. Solc. :" + CStr(UnidadesSolicitadas)
            FormCantidadRecibida_Recepcion.lblEmpaquesSolicitados.Text = "Emp. Solc.: " + CStr(EmpaquesSolicitados)
            FormCantidadFacturada_Recepcion.lblEmpaquesSolicitados.Text = "Emp. Solc.: " + CStr(EmpaquesSolicitados)
            FormCantidadRecibida_Recepcion.lblCantidadFaltante.Text = "Cant. Faltante: " + CStr(cantidadFaltanteFormularios)
            FormCantidadFacturada_Recepcion.lblCantidadFaltante.Text = "Cant. Faltante: " + CStr(cantidadFaltanteFormularios)

            dataset_Productos = detallesProducto(FormLecturaProducto_Recepcion.txtCodigoProducto.Text)
            FormLecturaProducto_Recepcion.lblDescripcion.Text = "Descripción: " + CStr(dataset_Productos.Tables(0).Rows(0)("C_DESCRI"))
            FormLecturaProducto_Recepcion.txtCodigoProducto.Enabled = False

        Else

            MsgBox("No hay conexión con el servidor, no se puede determinar la cantidad faltante por recibir de este producto", MsgBoxStyle.Exclamation, "isMOBILE")
            dataset_Productos = detallesProducto(FormLecturaProducto_Recepcion.txtCodigoProducto.Text)
            FormLecturaProducto_Recepcion.txtCodigoProducto.Text = ""
            FormLecturaProducto_Recepcion.txtCodigoProducto.Focus()

        End If

        cerrarConexion_SERVIDOR()

    End Sub


    Sub consultarNivelUsuario_PermiteRecibirSinODC(ByVal dataset_Codigos As Data.DataSet)

        'El parametro dataset_codigos es para usarlo en la funcion DeterminarUnidadesxEmpaque

        'consulto el nivel de usuario necesario para recibir un producto que no este en la orden de compra
        RegladeNegocio = "Recepcion_PermiteRecibirsoloconODCNivel"
        dataset_RegladeNegocio = consultarRegladeNegocio(RegladeNegocio)
        valorRegladeNegocio = dataset_RegladeNegocio.Tables(0).Rows(0)("valor")

        'consulto el nivel del usuario ingresado en el formulario principal (formUsuario)

        If CompatibilidadFueraDeLinea Then
            dataset_NivelUsuario = consultarNivelUsuario(formUsuario.txtUsuario.Text, formUsuario.txtContraseña.Text)
        Else
            dataset_NivelUsuario = consultarNivelUsuario_Servidor(formUsuario.txtUsuario.Text, formUsuario.txtContraseña.Text)
        End If

        nivelUsuario = CInt(dataset_NivelUsuario.Tables(0).Rows(0)("nivel"))

        If nivelUsuario < valorRegladeNegocio Then

            MsgBox("El producto no se encuentra en la orden de compra, debe solicitar una clave de autorización", MsgBoxStyle.Information, "isMOBILE")
            FormConfirmarUsuarioyContraseña.ShowDialog()

            If FormConfirmarUsuarioyContraseña.DialogResult = Windows.Forms.DialogResult.OK Then

                'consulto el nivel del usuario ingresado

                If CompatibilidadFueraDeLinea Then
                    dataset_NivelUsuario = consultarNivelUsuario(FormConfirmarUsuarioyContraseña.txtUsuario.Text, FormConfirmarUsuarioyContraseña.txtContraseña.Text)
                Else
                    dataset_NivelUsuario = consultarNivelUsuario_Servidor(FormConfirmarUsuarioyContraseña.txtUsuario.Text, FormConfirmarUsuarioyContraseña.txtContraseña.Text)
                End If

                nivelUsuario = CInt(dataset_NivelUsuario.Tables(0).Rows(0)("nivel"))

                If nivelUsuario < valorRegladeNegocio Then

                    FormLecturaProducto_Recepcion.ProgressBar_FormLecturaProducto.Value = 100
                    FormLecturaProducto_Recepcion.ProgressBar_FormLecturaProducto.Visible = False
                    FormLecturaProducto_Recepcion.lblProgressBar_FormLecturaProducto.Visible = False

                    MsgBox("No posee el nivel necesario para realizar la recepción de este producto", MsgBoxStyle.Information, "isMOBILE")
                    FormLecturaProducto_Recepcion.txtCodigoProducto.Text = ""
                    FormLecturaProducto_Recepcion.txtCodigoProducto.Focus()
                    FormLecturaProducto_Recepcion.lblDescripcion.Text = "Descripción:"

                Else

                    MsgBox("Usted posee el nivel necesario para realizar la recepción de este producto", MsgBoxStyle.Information, "isMOBILE")

                    dataset_Productos = detallesProducto(FormLecturaProducto_Recepcion.txtCodigoProducto.Text)
                    FormLecturaProducto_Recepcion.lblDescripcion.Text = "Descripción: " + CStr(dataset_Productos.Tables(0).Rows(0)("C_DESCRI"))
                    FormLecturaProducto_Recepcion.txtCodigoProducto.Enabled = False

                    UnidadesxEmpaque = determinarUnidadesxEmpaque(dataset_Codigos, dataset_Productos)
                    FormCantidadRecibida_Recepcion.lblUnidadesxEmpaque.Text = "Uni.x Emp.: " + CStr(UnidadesxEmpaque)
                    FormUnidadesxEmpaque_Recepcion.txtUnidadesxEmpaque.Text = CStr(UnidadesxEmpaque)

                    configurarFormulariosFormadeTrabajoSinODC()
                    'Consulto que el producto no se ha recibido anteriormente para esa recepcion

                    cantidadFaltanteFormularios = 0 'Porque el producto no se tiene en la ODC
                    ' y no se va a tomar en cuenta el valor recibido anteriormente procesado en TR_ODC para restarlo 

                    FormCantidadRecibida_Recepcion.lblUnidadesSolicitadas.Text = "Uni. Solc.:" + CStr(cantidadFaltanteFormularios)
                    FormCantidadFacturada_Recepcion.lblUnidadesSolicitadas.Text = "Uni. Solc.:" + CStr(cantidadFaltanteFormularios)
                    FormCantidadRecibida_Recepcion.lblEmpaquesSolicitados.Text = "Emp. Solc.:" + CStr(cantidadFaltanteFormularios)
                    FormCantidadFacturada_Recepcion.lblEmpaquesSolicitados.Text = "Emp. Solc.:" + CStr(cantidadFaltanteFormularios)
                    FormCantidadRecibida_Recepcion.lblCantidadFaltante.Text = "Cant. Faltante:" + CStr(cantidadFaltanteFormularios)
                    FormCantidadFacturada_Recepcion.lblCantidadFaltante.Text = "Cant. Faltante:" + CStr(cantidadFaltanteFormularios)

                    FormLecturaProducto_Recepcion.ProgressBar_FormLecturaProducto.Value = 100
                    FormLecturaProducto_Recepcion.ProgressBar_FormLecturaProducto.Visible = False
                    FormLecturaProducto_Recepcion.lblProgressBar_FormLecturaProducto.Visible = False

                End If

            Else 'Si el usuario no ingresó ningun usuario y contraseña (Le dio al botón Cancelar)

                If FormConfirmarUsuarioyContraseña.DialogResult = Windows.Forms.DialogResult.Cancel Then
                    FormLecturaProducto_Recepcion.txtCodigoProducto.Text = ""
                    FormLecturaProducto_Recepcion.txtCodigoProducto.Focus()
                    FormLecturaProducto_Recepcion.lblDescripcion.Text = "Descripción:"

                    FormLecturaProducto_Recepcion.ProgressBar_FormLecturaProducto.Value = 100
                    FormLecturaProducto_Recepcion.ProgressBar_FormLecturaProducto.Visible = False
                    FormLecturaProducto_Recepcion.lblProgressBar_FormLecturaProducto.Visible = False

                End If

            End If

        Else 'El usuario ingresado inicialmente posee el nivel necesario para recibir un producto que no está en la ODC

            dataset_Productos = detallesProducto(FormLecturaProducto_Recepcion.txtCodigoProducto.Text)
            FormLecturaProducto_Recepcion.lblDescripcion.Text = "Descripción: " + CStr(dataset_Productos.Tables(0).Rows(0)("C_DESCRI"))
            FormLecturaProducto_Recepcion.txtCodigoProducto.Enabled = False

            UnidadesxEmpaque = determinarUnidadesxEmpaque(dataset_Codigos, dataset_Productos)
            FormCantidadRecibida_Recepcion.lblUnidadesxEmpaque.Text = "Uni.x Emp.: " + CStr(UnidadesxEmpaque)
            FormUnidadesxEmpaque_Recepcion.txtUnidadesxEmpaque.Text = CStr(UnidadesxEmpaque)

            configurarFormulariosFormadeTrabajoSinODC()

            cantidadFaltanteFormularios = 0 'Porque el producto no se tiene en la ODC y no se va a tomar en cuenta el valor recibido anteriormente para restarlo
            FormCantidadRecibida_Recepcion.lblUnidadesSolicitadas.Text = "Uni. Solc.: " + CStr(cantidadFaltanteFormularios)
            FormCantidadFacturada_Recepcion.lblUnidadesSolicitadas.Text = "Uni. Solc.: " + CStr(cantidadFaltanteFormularios)
            FormCantidadRecibida_Recepcion.lblEmpaquesSolicitados.Text = "Emp. Solc.: " + CStr(cantidadFaltanteFormularios)
            FormCantidadFacturada_Recepcion.lblEmpaquesSolicitados.Text = "Emp. Solc.: " + CStr(cantidadFaltanteFormularios)
            FormCantidadRecibida_Recepcion.lblCantidadFaltante.Text = "Cant. Faltante: " + CStr(cantidadFaltanteFormularios)
            FormCantidadFacturada_Recepcion.lblCantidadFaltante.Text = "Cant. Faltante: " + CStr(cantidadFaltanteFormularios)

            FormLecturaProducto_Recepcion.ProgressBar_FormLecturaProducto.Value = 100
            FormLecturaProducto_Recepcion.ProgressBar_FormLecturaProducto.Visible = False
            FormLecturaProducto_Recepcion.lblProgressBar_FormLecturaProducto.Visible = False

        End If

    End Sub

    Sub permiteRecibirporEncimadeODC()

        RegladeNegocio = "Recepcion_PermiteRecibirporEncimadeODC"
        dataset_RegladeNegocio = consultarRegladeNegocio(RegladeNegocio)
        valorRegladeNegocio = CInt(dataset_RegladeNegocio.Tables(0).Rows(0)("valor"))

        If valorRegladeNegocio = 1 Then 'Si permite recibir una cantidad mayor a la solicitada, debo consultar el nivel 
            'del usuario ingresado inicialmente y si el nivel es inferior al requerido se pide una clave de autorizacion
            RegladeNegocio = "Recepcion_PermiteRecibirporEncimadeNivel"
            dataset_RegladeNegocio = consultarRegladeNegocio(RegladeNegocio)
            valorRegladeNegocio = CInt(dataset_RegladeNegocio.Tables(0).Rows(0)("valor")) 'Este es el nivel requerido para recibir por encima de ODC

            'consulto el nivel del usuario ingresado en el formulario principal (formUsuario)
            dataset_NivelUsuario = consultarNivelUsuario(formUsuario.txtUsuario.Text, formUsuario.txtContraseña.Text)
            nivelUsuario = CInt(dataset_NivelUsuario.Tables(0).Rows(0)("nivel"))

            If nivelUsuario < valorRegladeNegocio Then
                MsgBox("La cantidad ingresada es mayor que la solicitada, debe solicitar una clave de autorización.", MsgBoxStyle.Information, "isMOBILE")

                FormConfirmarUsuarioyContraseña.ShowDialog()
                If FormConfirmarUsuarioyContraseña.DialogResult = Windows.Forms.DialogResult.OK Then
                    'consulto el nivel del usuario ingresado
                    dataset_NivelUsuario = consultarNivelUsuario(FormConfirmarUsuarioyContraseña.txtUsuario.Text, FormConfirmarUsuarioyContraseña.txtContraseña.Text)
                    nivelUsuario = CInt(dataset_NivelUsuario.Tables(0).Rows(0)("nivel"))

                    If nivelUsuario < valorRegladeNegocio Then
                        MsgBox("No posee el nivel necesario para recibir una cantidad mayor a la solicitada.", MsgBoxStyle.Information, "isMOBILE")
                        EstablecercantidadRecibidaPermitida()
                    Else
                        MsgBox("Usted posee el nivel necesario para recibir una cantidad mayor a la solicitada.", MsgBoxStyle.Information, "isMOBILE")
                        FormCantidadFacturada_Recepcion.Show()
                        FormCantidadRecibida_Recepcion.Visible = False
                    End If
                Else 'Si el usuario no ingreso ningun usuario y contraseña (Le dio al botón Cancelar)
                    If FormConfirmarUsuarioyContraseña.DialogResult = Windows.Forms.DialogResult.Cancel Then
                        EstablecercantidadRecibidaPermitida()
                    End If
                End If
            Else 'si el usuario ingresado inicialmente posee el nivel para recibir por encima de ODC
                respuesta = MsgBox("La cantidad ingresada es mayor que la solicitada, ¿Está seguro de recibir esta cantidad?", MsgBoxStyle.YesNo, "isMOBILE")
                If respuesta = "6" Then
                    FormCantidadFacturada_Recepcion.Show()
                    FormCantidadRecibida_Recepcion.Visible = False
                Else
                    EstablecercantidadRecibidaPermitida()
                End If
            End If
        Else 'Si no permite recibir una cantidad mayor a la solicitada
            MsgBox("La cantidad ingresada es mayor que la solicitada, comuníquese con el Gerente.", MsgBoxStyle.Information, "isMOBILE")
            EstablecercantidadRecibidaPermitida()
        End If

    End Sub

    Sub EstablecercantidadRecibidaPermitida()

        If FormCantidadRecibida_Recepcion.txtEmpaquesRecibidos.Visible = True And FormCantidadRecibida_Recepcion.txtUnidadesRecibidas.Visible = True Then
            If FormCantidadRecibida_Recepcion.txtEmpaquesRecibidos.Text = 0 And FormCantidadRecibida_Recepcion.txtUnidadesRecibidas.Text <> "" Then
                FormCantidadRecibida_Recepcion.txtUnidadesRecibidas.Text = cantidadUnidadesPermitidas
                FormCantidadRecibida_Recepcion.txtUnidadesRecibidas.Focus()
            Else
                If FormCantidadRecibida_Recepcion.txtEmpaquesRecibidos.Text <> "" And FormCantidadRecibida_Recepcion.txtUnidadesRecibidas.Text = "0" Then
                    FormCantidadRecibida_Recepcion.txtEmpaquesRecibidos.Text = cantidadEmpaquesPermitidos
                    FormCantidadRecibida_Recepcion.txtEmpaquesRecibidos.Focus()
                Else 'Ambos tienen informacion
                    If FormCantidadRecibida_Recepcion.txtEmpaquesRecibidos.Text <> "" And FormCantidadRecibida_Recepcion.txtUnidadesRecibidas.Text <> "" Then
                        FormCantidadRecibida_Recepcion.txtEmpaquesRecibidos.Text = cantidadEmpaquesPermitidos
                        FormCantidadRecibida_Recepcion.txtUnidadesRecibidas.Text = 0
                        FormCantidadRecibida_Recepcion.txtEmpaquesRecibidos.Focus()
                    End If
                End If
            End If
        Else
            If FormCantidadRecibida_Recepcion.txtEmpaquesRecibidos.Visible = True And FormCantidadRecibida_Recepcion.txtUnidadesRecibidas.Visible = False Then
                FormCantidadRecibida_Recepcion.txtEmpaquesRecibidos.Text = cantidadEmpaquesPermitidos
                FormCantidadRecibida_Recepcion.txtEmpaquesRecibidos.Focus()
            Else
                If FormCantidadRecibida_Recepcion.txtEmpaquesRecibidos.Visible = False And FormCantidadRecibida_Recepcion.txtUnidadesRecibidas.Visible = False Then
                    FormCantidadRecibida_Recepcion.txtUnidadesRecibidas.Text = cantidadUnidadesPermitidas
                    FormCantidadRecibida_Recepcion.txtUnidadesRecibidas.Focus()
                End If
            End If
        End If
    End Sub

    Sub consultarNivelUsuarioRecibirSinODC_FormProveedorDeposito()

        RegladeNegocio = "Recepcion_PermiteRecibirsoloconODCNivel"
        dataset_RegladeNegocio = consultarRegladeNegocio(RegladeNegocio)
        valorRegladeNegocio = Val(dataset_RegladeNegocio.Tables(0).Rows(0)("valor"))

        'consulto el nivel del usuario ingresado en el formulario principal (formUsuario)

        If CompatibilidadFueraDeLinea Then
            dataset_NivelUsuario = consultarNivelUsuario(formUsuario.txtUsuario.Text, formUsuario.txtContraseña.Text)
        Else
            dataset_NivelUsuario = consultarNivelUsuario_Servidor(formUsuario.txtUsuario.Text, formUsuario.txtContraseña.Text)
        End If

        nivelUsuario = CInt(dataset_NivelUsuario.Tables(0).Rows(0)("nivel"))

        If nivelUsuario < Val(valorRegladeNegocio) Then

            MsgBox("No posee el nivel necesario para realizar la recepción sin ODC, debe solicitar una clave de autorización", MsgBoxStyle.Information, "isMOBILE")

            FormConfirmarUsuarioyContraseña.ShowDialog()

            If FormConfirmarUsuarioyContraseña.DialogResult = Windows.Forms.DialogResult.OK Then

                'consulto el nivel del usuario ingresado

                If CompatibilidadFueraDeLinea Then
                    dataset_NivelUsuario = consultarNivelUsuario(FormConfirmarUsuarioyContraseña.txtUsuario.Text, FormConfirmarUsuarioyContraseña.txtContraseña.Text)
                Else
                    dataset_NivelUsuario = consultarNivelUsuario_Servidor(FormConfirmarUsuarioyContraseña.txtUsuario.Text, FormConfirmarUsuarioyContraseña.txtContraseña.Text)
                End If

                nivelUsuario = CInt(dataset_NivelUsuario.Tables(0).Rows(0)("nivel"))

                If nivelUsuario < Val(valorRegladeNegocio) Then
                    MsgBox("No posee el nivel necesario para realizar la recepción sin ODC", MsgBoxStyle.Information, "isMOBILE")
                    FormProveedorDeposito_Recepcion.txtProveedor.Text = ""
                    FormProveedorDeposito_Recepcion.lblDescripcionProveedor.Text = "Descripción: "
                    FormProveedorDeposito_Recepcion.txtProveedor.Focus()
                    FormProveedorDeposito_Recepcion.txtDeposito.Enabled = False
                    FormProveedorDeposito_Recepcion.cmbBuscarDeposito.Enabled = False
                    FormProveedorDeposito_Recepcion.cmbVerificarDeposito.Enabled = False
                Else
                    MsgBox("Usted posee el nivel necesario para realizar la recepción sin ODC", MsgBoxStyle.Information, "isMOBILE")
                    FormProveedorDeposito_Recepcion.txtProveedor.Enabled = False
                    FormProveedorDeposito_Recepcion.txtDeposito.Enabled = True
                    FormProveedorDeposito_Recepcion.txtDeposito.Focus()
                    FormProveedorDeposito_Recepcion.cmbBuscarDeposito.Enabled = True
                    FormProveedorDeposito_Recepcion.cmbVerificarDeposito.Enabled = True
                End If

            Else 'Si el usuario no ingreso ningun usuario y contraseña (Le dio al botón Cancelar)
                If FormConfirmarUsuarioyContraseña.DialogResult = Windows.Forms.DialogResult.Cancel Then
                    FormProveedorDeposito_Recepcion.txtProveedor.Text = ""
                    FormProveedorDeposito_Recepcion.lblDescripcionProveedor.Text = "Descripción: "
                    FormProveedorDeposito_Recepcion.txtProveedor.Focus()
                    FormProveedorDeposito_Recepcion.txtDeposito.Enabled = False
                    FormProveedorDeposito_Recepcion.cmbBuscarDeposito.Enabled = False
                    FormProveedorDeposito_Recepcion.cmbVerificarDeposito.Enabled = False
                End If
            End If

        Else 'El usuario ingresado incialmente si posee el nivel para recibir sin ODC
            FormProveedorDeposito_Recepcion.txtProveedor.Enabled = False
            FormProveedorDeposito_Recepcion.txtDeposito.Enabled = True
            FormProveedorDeposito_Recepcion.txtDeposito.Focus()
            FormProveedorDeposito_Recepcion.cmbBuscarDeposito.Enabled = True
            FormProveedorDeposito_Recepcion.cmbVerificarDeposito.Enabled = True
        End If

    End Sub

    Sub ActualizarNumUltimaRecepcion()

        comandoBD_PDA.CommandText = "SELECT numUltimaRecepcion FROM MA_CONFIGURACION"
        dataSet_PDA = consultaQuery_PDA(comandoBD_PDA)

        Dim numUltimaRecepcionProcesada As Double = dataSet_PDA.Tables(0).Rows(0)("numUltimaRecepcion") + 1

        comandoBD_PDA.CommandText = "UPDATE MA_CONFIGURACION SET numUltimaRecepcion = @numUltimaRecepcionProcesada"
        comandoBD_PDA.Parameters.Add("@numUltimaRecepcionProcesada", numUltimaRecepcionProcesada)
        ejecutaQuery_PDA(comandoBD_PDA)

    End Sub

    Sub enviarRecepcionesPendientes()

        comandoBD_PDA.CommandText = "SELECT * FROM MA_RECEPCIONESMOVIL"
        dataSet_PDA = consultaQuery_PDA(comandoBD_PDA)

        If dataSet_PDA.Tables(0).Rows.Count = 0 Then
            'MsgBox("No existen recepciones pendientes por enviar", MsgBoxStyle.Information, "Recepciones Pendientes")
        Else
            For i = 0 To dataSet_PDA.Tables(0).Rows.Count - 1
                Dim numeroRecepcionPendiente As String = dataSet_PDA.Tables(0).Rows(i)("numeroRecepcion_PDA")
                insertaryActualizarRegistrosMA_RECEPCIONESMOVIL(numeroRecepcionPendiente)
            Next
            'Borro la tabla para que no intente enviar información que ya ha sido enviada anteriormente
            comandoBD_PDA.CommandText = "DELETE MA_RECEPCIONESMOVIL"
            ejecutaQuery_PDA(comandoBD_PDA)
            'MsgBox("Las recepciones pendientes por enviar han sido guardadas en la base de datos del servidor", MsgBoxStyle.Information, "Recepciones Pendientes")
        End If

    End Sub

    Sub verificarCantidadRecibidaOtraRecepcion()

        comandoBD_PDA.CommandText = "SELECT SUM (cantidadUnidades) AS cantidadUnidades, CASE WHEN cantidadUnidades is Null THEN 0 ELSE cantidadUnidades END AS Unidades, SUM (cantidadEmpaques) AS cantidadEmpaques, CASE WHEN cantidadEmpaques is Null THEN 0 ELSE cantidadEmpaques END AS Empaques FROM MA_RECEPCIONESMOVIL WHERE codigoODC = @codigoODC AND codigoProveedor = @codigoProveedor AND  codigoProducto = @codigoProducto GROUP BY cantidadUnidades, cantidadEmpaques"
        comandoBD_PDA.Parameters.Add("@codigoODC", codigoODC)
        comandoBD_PDA.Parameters.Add("@codigoProveedor", FormProveedorDeposito_Recepcion.txtProveedor.Text)
        comandoBD_PDA.Parameters.Add("@codigoProducto", FormLecturaProducto_Recepcion.txtCodigoProducto.Text)
        dataSet_PDA = consultaQuery_PDA(comandoBD_PDA)

        nombreTabla = "MA_RECEPCIONESMOVIL"
        comandoBD_SERVIDOR.CommandText = "SELECT SUM (cantidadUnidades) AS cantidadUnidades, CASE WHEN cantidadUnidades is Null THEN 0 ELSE cantidadUnidades END AS Unidades, SUM (cantidadEmpaques) AS cantidadEmpaques, CASE WHEN cantidadEmpaques is Null THEN 0 ELSE cantidadEmpaques END AS Empaques FROM MA_RECEPCIONESMOVIL WHERE codigoODC = @codigoODC AND codigoProveedor = @codigoProveedor AND  codigoProducto = @codigoProducto GROUP BY cantidadUnidades, cantidadEmpaques"
        comandoBD_SERVIDOR.Parameters.Add("@codigoODC", codigoODC)
        comandoBD_SERVIDOR.Parameters.Add("@codigoProveedor", FormProveedorDeposito_Recepcion.txtProveedor.Text)
        comandoBD_SERVIDOR.Parameters.Add("@codigoProducto", FormLecturaProducto_Recepcion.txtCodigoProducto.Text)
        dataSet_Servidor = ejecutaQuery_Servidor(comandoBD_SERVIDOR, nombreTabla)

        Dim cantidadRecibidaOtraRecepcion_PDA As Double
        Dim cantidadRecibidaOtraRecepcion_SERVIDOR As Double

        If dataSet_PDA.Tables(0).Rows.Count = 0 Then
            cantidadRecibidaOtraRecepcion_PDA = 0
        Else
            cantidadRecibidaOtraRecepcion_PDA = CDbl(dataSet_PDA.Tables(0).Rows(0)("cantidadUnidades")) + (CDbl((dataSet_PDA.Tables(0).Rows(0)("cantidadEmpaques")) * UnidadesxEmpaque))
        End If

        If dataSet_Servidor.Tables(0).Rows.Count = 0 Then
            cantidadRecibidaOtraRecepcion_SERVIDOR = 0
        Else
            cantidadRecibidaOtraRecepcion_SERVIDOR = CDbl(dataSet_Servidor.Tables(0).Rows(0)("cantidadUnidades")) + (CDbl((dataSet_Servidor.Tables(0).Rows(0)("cantidadEmpaques")) * UnidadesxEmpaque))
        End If

        cantidadFaltanteFormularios = cantidadFaltanteFormularios - cantidadRecibidaOtraRecepcion_SERVIDOR - cantidadRecibidaOtraRecepcion_PDA

    End Sub

    Sub configurarFormulariosFormadeTrabajoSinODC()

        If dataset_RegladeNegocio.Tables(0).Rows.Count = 0 Then
            MsgBox("No existe la Regla de Negocio consultada", MsgBoxStyle.Information, "isMOBILE")
        Else
            valorRegladeNegocio = CInt(dataset_RegladeNegocio.Tables(0).Rows(0)("valor"))
            If valorRegladeNegocio = 0 Then 'Trabaja solo con unidades
                FormCantidadRecibida_Recepcion.txtEmpaquesRecibidos.Visible = False
                FormCantidadRecibida_Recepcion.lblEmpaquesRecibidos.Visible = False
                FormCantidadRecibida_Recepcion.lblUnidadesxEmpaque.Visible = False
                FormCantidadRecibida_Recepcion.lblEmpaquesSolicitados.Visible = False
                FormCantidadRecibida_Recepcion.txtUnidadesRecibidas.Focus()

                FormCantidadFacturada_Recepcion.txtEmpaquesFacturados.Visible = False
                FormCantidadFacturada_Recepcion.lblEmpaquesFacturados.Visible = False
                FormCantidadFacturada_Recepcion.lblUnidadesxEmpaque.Visible = False
                FormCantidadFacturada_Recepcion.lblEmpaquesSolicitados.Visible = False
                FormCantidadFacturada_Recepcion.txtUnidadesFacturadas.Focus()

            Else
                If valorRegladeNegocio = 1 Then 'Trabaja solo con empaques
                    FormCantidadRecibida_Recepcion.txtUnidadesRecibidas.Visible = False
                    FormCantidadRecibida_Recepcion.lblUnidadesRecibidas.Visible = False
                    FormCantidadRecibida_Recepcion.lblUnidadesSolicitadas.Visible = False
                    FormCantidadRecibida_Recepcion.txtEmpaquesRecibidos.Focus()

                    FormCantidadFacturada_Recepcion.txtUnidadesFacturadas.Visible = False
                    FormCantidadFacturada_Recepcion.lblUnidadesFacturadas.Visible = False
                    FormCantidadFacturada_Recepcion.lblUnidadesSolicitadas.Visible = False
                    FormCantidadFacturada_Recepcion.txtEmpaquesFacturados.Focus()

                Else 'Trabaja con empaques y unidades
                    FormCantidadRecibida_Recepcion.txtEmpaquesRecibidos.Focus()

                    FormCantidadFacturada_Recepcion.txtEmpaquesFacturados.Focus()
                End If
            End If
        End If
    End Sub

    Sub configurarFormulariosFormadeTrabajoconODC()
        RegladeNegocio = "Recepcion_FormadeTrabajo"
        dataset_RegladeNegocio = consultarRegladeNegocio(RegladeNegocio)

        If dataset_RegladeNegocio.Tables(0).Rows.Count = 0 Then
            MsgBox("No existe la Regla de Negocio consultada", MsgBoxStyle.Information, "isMOBILE")
        Else
            valorRegladeNegocio = CInt(dataset_RegladeNegocio.Tables(0).Rows(0)("valor"))
            If valorRegladeNegocio = 0 Then 'Trabaja solo con unidades
                FormCantidadRecibida_Recepcion.txtEmpaquesRecibidos.Visible = False
                FormCantidadRecibida_Recepcion.lblEmpaquesRecibidos.Visible = False
                FormCantidadRecibida_Recepcion.lblUnidadesxEmpaque.Visible = False
                FormCantidadRecibida_Recepcion.lblEmpaquesSolicitados.Visible = False
                FormCantidadRecibida_Recepcion.txtUnidadesRecibidas.Focus()
                UnidadesSolicitadas = cantidadSolicitada 'Ya está en unidades
                EmpaquesSolicitados = 0

                FormCantidadFacturada_Recepcion.txtEmpaquesFacturados.Visible = False
                FormCantidadFacturada_Recepcion.lblEmpaquesFacturados.Visible = False
                FormCantidadFacturada_Recepcion.lblUnidadesxEmpaque.Visible = False
                FormCantidadFacturada_Recepcion.lblEmpaquesSolicitados.Visible = False
                FormCantidadFacturada_Recepcion.txtUnidadesFacturadas.Focus()

            Else
                If valorRegladeNegocio = 1 Then 'Trabaja solo con empaques
                    FormCantidadRecibida_Recepcion.txtUnidadesRecibidas.Visible = False
                    FormCantidadRecibida_Recepcion.lblUnidadesRecibidas.Visible = False
                    FormCantidadRecibida_Recepcion.lblUnidadesSolicitadas.Visible = False
                    FormCantidadRecibida_Recepcion.txtEmpaquesRecibidos.Focus()
                    EmpaquesSolicitados = cantidadSolicitada / UnidadesxEmpaque 'Deberia dar siempre entero, le coloco la barra normal (/) por si acaso no da entero
                    UnidadesSolicitadas = 0

                    FormCantidadFacturada_Recepcion.txtUnidadesFacturadas.Visible = False
                    FormCantidadFacturada_Recepcion.lblUnidadesFacturadas.Visible = False
                    FormCantidadFacturada_Recepcion.lblUnidadesSolicitadas.Visible = False
                    FormCantidadFacturada_Recepcion.txtEmpaquesFacturados.Focus()

                Else 'Trabaja con empaques y unidades
                    EmpaquesSolicitados = cantidadSolicitada \ UnidadesxEmpaque ' \ devuelve la parte entera del resultado de la división
                    UnidadesSolicitadas = cantidadSolicitada Mod UnidadesxEmpaque ' Mod devuelve el resto de la división

                    FormCantidadRecibida_Recepcion.txtEmpaquesRecibidos.Focus()

                    FormCantidadFacturada_Recepcion.txtEmpaquesFacturados.Focus()
                End If
            End If
        End If
    End Sub

    Function verificaExistenTraslados(ByVal numeroTraslado_SERVIDOR)

        'Consulto cuantos registros tiene la tabla TR_INV_FISICO
        'nombreTabla = "TR_INVENTARIO"
        'comandoBD_SERVIDOR.CommandText = "Select * from TR_INVENTARIO where c_documento = @numeroTraslado_SERVIDOR AND c_concepto='TRS'"
        'comandoBD_SERVIDOR.Parameters.Add("@numeroTraslado_SERVIDOR", numeroTraslado_SERVIDOR)
        'dataSet_Servidor = ejecutaQuery_Servidor(comandoBD_SERVIDOR, nombreTabla)

        'If dataSet_Servidor.Tables(0).Rows.Count = 0 Then
        'comandoBD_PDA.CommandText = "select * from TR_TRASLADOS_PDA where c_documento = @numeroTraslado_SERVIDOR AND c_concepto='TRS'"
        'comandoBD_PDA.Parameters.Add("@numeroTraslado_SERVIDOR", numeroTraslado_SERVIDOR)

        comandoBD_PDA.CommandText = "SELECT * FROM TR_TRASLADOS_PDA"
        dataSet_PDA = consultaQuery_PDA(comandoBD_PDA)

        If dataSet_PDA.Tables(0).Rows.Count = 0 Then
            Return 0
        Else
            Return 1
        End If

        'Else
        'Return 1
        'End If

    End Function

    Function verificaProductosTraslados(ByVal numeroTraslado_SERVIDOR) As Integer

        Try
            'Consulto cuantos registros tiene la tabla TR_INV_FISICO
            nombreTabla = "TR_INVENTARIO"
            comandoBD_SERVIDOR.CommandText = "Select * from TR_INVENTARIO where c_documento = @numeroTraslado_SERVIDOR AND c_concepto='TRS'"
            comandoBD_SERVIDOR.Parameters.Add("@numeroTraslado_SERVIDOR", numeroTraslado_SERVIDOR)
            dataSet_Servidor = ejecutaQuery_Servidor(comandoBD_SERVIDOR, nombreTabla)

            comandoBD_PDA.CommandText = "select * from TR_TRASLADOS_PDA where c_documento = @numeroTraslado_SERVIDOR AND c_concepto='TRS'"
            comandoBD_PDA.Parameters.Add("@numeroTraslado_SERVIDOR", numeroTraslado_SERVIDOR)
            dataSet_PDA = consultaQuery_PDA(comandoBD_PDA)

            Dim num As Integer = CInt(((dataSet_Servidor.Tables(0).Rows.Count + dataSet_PDA.Tables(0).Rows.Count) / 2))

            Return CInt(IIf(num < 1, 1, num))

        Catch sqlEx As SqlCeException
            Return 1
        End Try

    End Function

    Sub MostrarFormCantidadFacturadaSinODC()

        Dim cantidadRecibidaNoProcesada_SERVIDOR As Double
        Dim cantidadRecibidaNoProcesada_PDA As Double

        verFormCantidadFacturadaSinODC = True

        'Verifico que no se haya recibido anteriormete el producto en la tabla del servidor

        If conectarDB_SERVIDOR(rutaDB_SERVIDOR) = 1 Then

            dataset_MA_RECEPCIONESMOVIL_SERVIDOR = obtenerRegistrosMA_RECEPCIONESMOVIL_SERVIDOR(FormCantidadRecibida_Recepcion.lblNºRec.Text) 'Es para un numero de recepción 
            'específico, no toma en cuenta el código de orden de compra. Por lo tanto si se recibe un producto con la misma orden de compra pero en otra recepcion, y la recepcion 
            'anterior no ha sido procesada en stellar, no se estaría restando la cantidad recibida anterior. 

            If dataset_MA_RECEPCIONESMOVIL_SERVIDOR.Tables(0).Rows.Count = 0 Then
                cantidadRecibidaNoProcesada_SERVIDOR = 0
                UnidadesFacturadasNoProcesadas_SERVIDOR = 0
                EmpaquesFacturadosNoProcesados_SERVIDOR = 0
            Else
                cantidadRecibidaNoProcesada_SERVIDOR = dataset_MA_RECEPCIONESMOVIL_SERVIDOR.Tables(0).Rows(0)("cantidadUnidades") + (dataset_MA_RECEPCIONESMOVIL_SERVIDOR.Tables(0).Rows(0)("cantidadEmpaques") * UnidadesxEmpaque)
                UnidadesFacturadasNoProcesadas_SERVIDOR = dataset_MA_RECEPCIONESMOVIL_SERVIDOR.Tables(0).Rows(0)("facturaUnidades")
                EmpaquesFacturadosNoProcesados_SERVIDOR = dataset_MA_RECEPCIONESMOVIL_SERVIDOR.Tables(0).Rows(0)("facturaEmpaques")
            End If

            'verifico que no se haya recibido anteriormente el producto en el pda
            dataset_MA_RECEPCIONESMOVIL_PDA = obtenerRegistrosMA_RECEPCIONESMOVIL_PDA(FormCantidadRecibida_Recepcion.lblNºRec.Text)

            If dataset_MA_RECEPCIONESMOVIL_PDA.Tables(0).Rows.Count = 0 Then
                cantidadRecibidaNoProcesada_PDA = 0
                UnidadesFacturadasNoProcesadas_PDA = 0
                EmpaquesFacturadosNoProcesados_PDA = 0
            Else
                cantidadRecibidaNoProcesada_PDA = dataset_MA_RECEPCIONESMOVIL_PDA.Tables(0).Rows(0)("cantidadUnidades") + (dataset_MA_RECEPCIONESMOVIL_PDA.Tables(0).Rows(0)("cantidadEmpaques") * UnidadesxEmpaque)
                UnidadesFacturadasNoProcesadas_PDA = dataset_MA_RECEPCIONESMOVIL_PDA.Tables(0).Rows(0)("facturaUnidades")
                EmpaquesFacturadosNoProcesados_PDA = dataset_MA_RECEPCIONESMOVIL_PDA.Tables(0).Rows(0)("facturaEmpaques")
            End If

            If cantidadRecibidaNoProcesada_SERVIDOR = 0 And cantidadRecibidaNoProcesada_PDA = 0 Then 'No se ha recibido el producto anteriormente para esa recepcion
                'No hacer nada porque se debe mostrar el formCantidadFacturada
            Else
                verFormCantidadFacturadaSinODC = False
            End If

        Else
            MsgBox("No hay conexión con el servidor, no se puede" & vbNewLine & "determinar si el producto fue recibido anteriormente.", MsgBoxStyle.Exclamation, "isMOBILE")
            'VerFormCantidadFacturada= true
        End If

        cerrarConexion_SERVIDOR()

    End Sub

    '********************** I N V E N T A R I O    F I S I C O ************************************

    Function añadirProductoInv(ByVal CantTotal As Double, ByVal CantEmpaque As Integer, ByVal Linea As Integer) As Integer

        Dim n_Costo As Integer
        Dim Costo, Subtotal As Double

        If CompatibilidadFueraDeLinea Then
            SentenciaSQL = "SELECT ESTIMACION_INV FROM MA_CONFIGURACION"
            n_Costo = returnInteger_PDA(SentenciaSQL)
        Else
            SentenciaSQL = "SELECT ESTIMACION_INV FROM REGLAS_COMERCIALES"
            n_Costo = returnInteger_SERVIDOR(SentenciaSQL)
        End If

        Select Case n_Costo
            Case 0
                Costo = CDbl(dataSet_PDA.Tables(0).Rows(0)("n_costoact"))
            Case 1
                Costo = CDbl(dataSet_PDA.Tables(0).Rows(0)("n_costoant"))
            Case 2
                Costo = CDbl(dataSet_PDA.Tables(0).Rows(0)("n_costopro"))
            Case 3
                Costo = CDbl(dataSet_PDA.Tables(0).Rows(0)("n_costorep"))
            Case Else
                Costo = CDbl(dataSet_PDA.Tables(0).Rows(0)("n_costoact"))
        End Select

        Subtotal = (CantTotal * Costo)

        Dim NoNecesitaConfirmacion As Boolean

        If Not FormElab_Inv_InventarioFisico Is Nothing Then
            If FormElab_Inv_InventarioFisico.Visible Then
                If FormElab_Inv_InventarioFisico.chkQuickScan.Checked Then
                    NoNecesitaConfirmacion = True
                End If
            End If
        End If

        If NoNecesitaConfirmacion Then
            respuesta = "6"
        Else
            respuesta = MsgBox("Cantidad Total: " & CantTotal & ". " + FormElab_Inv_InventarioFisico.lblDescripcion.Text + _
            ". ¿Esta seguro de proceder a registrar? ", MsgBoxStyle.YesNo, "Añadir producto al inventario")
        End If

        If respuesta = "6" Then

            comandoBD_PDA.CommandText = "INSERT INTO TR_INV_FISICO" + _
            vbNewLine + "(c_LINEA, c_DOCUMENTO, c_DEPOSITO, c_CODARTICULO, n_CANTIDAD, n_COSTO, N_SUBTOTAL, cs_codlocalidad, ns_CantidadEmpaque) VALUES" + _
            "(@c_LINEA, @c_DOCUMENTO, @c_DEPOSITO, @c_CODARTICULO, @n_CANTIDAD, @n_COSTO, @N_SUBTOTAL, @cs_codlocalidad, @ns_CantidadEmpaque)"

            comandoBD_PDA.Parameters.Add("@c_LINEA", Linea)
            comandoBD_PDA.Parameters.Add("@c_DOCUMENTO", FormElab_Inv_InventarioFisico.lblNumDoc.Text)
            comandoBD_PDA.Parameters.Add("@c_DEPOSITO", FormElab_Inv_InventarioFisico.lblNumDeposito.Text)
            comandoBD_PDA.Parameters.Add("@c_CODARTICULO", FormElab_Inv_InventarioFisico.txtCodigoProducto.Text)
            comandoBD_PDA.Parameters.Add("@n_CANTIDAD", CantTotal)
            comandoBD_PDA.Parameters.Add("@n_COSTO", Costo)
            comandoBD_PDA.Parameters.Add("@N_SUBTOTAL", Subtotal)
            comandoBD_PDA.Parameters.Add("@cs_codlocalidad", Localidad)
            comandoBD_PDA.Parameters.Add("@ns_CantidadEmpaque", CantEmpaque)

            ejecutaQuery_PDA(comandoBD_PDA)

            limpiarForm_Elab_Inv()

            FormElab_Inv_InventarioFisico.Linea = FormElab_Inv_InventarioFisico.Linea + 1

            Return 1

        Else
            limpiarForm_Elab_Inv()
            Return 0
        End If

    End Function

    Sub limpiarForm_Elab_Inv()
        FormElab_Inv_InventarioFisico.txtCodigoProducto.Text = ""
        FormElab_Inv_InventarioFisico.txtCodigoProducto.Focus()
        FormElab_Inv_InventarioFisico.txtUnidad.Text = ""
        FormElab_Inv_InventarioFisico.lblEmpaque.Text = "Empaques"
        FormElab_Inv_InventarioFisico.txtEmpaque.Text = ""
        FormElab_Inv_InventarioFisico.lblDescripcion.Text = "Descripción: "
        'FormElab_Inv_InventarioFisico.Linea = 1
    End Sub

    Function verifica_Inventario(ByVal numDoc As String) As Integer
        'Consulto cuantos registros tiene la tabla TR_INV_FISICO
        SentenciaSQL = "SELECT COUNT(*) FROM TR_INV_FISICO WHERE c_DOCUMENTO = '" + numDoc + "'"
        contador = returnInteger_PDA(SentenciaSQL)
        Return contador
    End Function

    Function actualizarMA_INV_FISICO(ByVal numDoc As String, _
    ByVal observacion As String, ByVal motivo As String) As Boolean

        Dim c_codmoneda, comprador As String
        Dim factorCambio As Double

        SentenciaSQL = "SELECT C_CODMONEDA FROM MA_MONEDAS WHERE b_Preferencia = 1"
        If CompatibilidadFueraDeLinea Then
            c_codmoneda = returnString_PDA(SentenciaSQL)
        Else
            c_codmoneda = returnString_SERVIDOR(SentenciaSQL)
        End If

        SentenciaSQL = "SELECT n_factor FROM MA_MONEDAS WHERE b_Preferencia = 1"
        If CompatibilidadFueraDeLinea Then
            factorCambio = returnInteger_PDA(SentenciaSQL)
        Else
            factorCambio = returnString_SERVIDOR(SentenciaSQL)
        End If

        If c_codmoneda = String.Empty _
        Or factorCambio = 0 Then
            MsgBox("No hay moneda predeterminada o no se pudo recuperar el correlativo. " + _
            "Intente de nuevo mas tarde", MsgBoxStyle.Information, "isMOBILE")
            Exit Function
        End If

        'SentenciaSQL = "SELECT codusuario FROM MA_USUARIOS"
        comprador = CStr(datasetUsuario.Tables(0).Rows(0)("codusuario")) 'returnString_PDA(SentenciaSQL)

        SentenciaSQL = "INSERT INTO MA_INV_FISICO" + _
        vbNewLine + "(c_DOCUMENTO, d_FECHA, c_STATUS, c_CODLOCALIDAD, c_codMONEDA, n_FACTORCAMBIO, c_OBSERVACION, c_CODCOMPRADOR, c_DEP_ORIG, c_MOTIVO, NO_ELAB, CU_CODDEPARTAMENTO, CU_CODGRUPO, CU_CODSUBGRUPO, CU_CODPROVEEDOR) VALUES" + _
        vbNewLine + "(@c_DOCUMENTO, GETDATE(), 'DPE', @c_CODLOCALIDAD, @c_codMONEDA, @n_FACTORCAMBIO, @c_OBSERVACION, @c_CODCOMPRADOR, @c_DEP_ORIG, @c_MOTIVO, @NO_ELAB, @CU_CODDEPARTAMENTO, @CU_CODGRUPO, @CU_CODSUBGRUPO, @CU_CODPROVEEDOR)"

        comandoBD_PDA.CommandText = SentenciaSQL

        comandoBD_PDA.Parameters.Clear()
        comandoBD_PDA.Parameters.Add("@c_DOCUMENTO", numDoc)
        comandoBD_PDA.Parameters.Add("@c_CODLOCALIDAD", Localidad)
        comandoBD_PDA.Parameters.Add("@c_codMONEDA", c_codmoneda)
        comandoBD_PDA.Parameters.Add("@n_FACTORCAMBIO", factorCambio)
        comandoBD_PDA.Parameters.Add("@c_OBSERVACION", observacion)
        comandoBD_PDA.Parameters.Add("@c_CODCOMPRADOR", comprador)
        comandoBD_PDA.Parameters.Add("@c_DEP_ORIG", FormInv_Activo_InventarioFisico.DataGridINVENTARIO_INV_ACTIVO.Item(filaDataGrid, 1))
        comandoBD_PDA.Parameters.Add("@c_MOTIVO", motivo)
        comandoBD_PDA.Parameters.Add("@NO_ELAB", FormInv_Activo_InventarioFisico.DataGridINVENTARIO_INV_ACTIVO.Item(filaDataGrid, 6))
        comandoBD_PDA.Parameters.Add("@CU_CODDEPARTAMENTO", FormInv_Activo_InventarioFisico.DataGridINVENTARIO_INV_ACTIVO.Item(filaDataGrid, 2))
        comandoBD_PDA.Parameters.Add("@CU_CODGRUPO", FormInv_Activo_InventarioFisico.DataGridINVENTARIO_INV_ACTIVO.Item(filaDataGrid, 3))
        comandoBD_PDA.Parameters.Add("@CU_CODSUBGRUPO", FormInv_Activo_InventarioFisico.DataGridINVENTARIO_INV_ACTIVO.Item(filaDataGrid, 4))
        comandoBD_PDA.Parameters.Add("@CU_CODPROVEEDOR", FormInv_Activo_InventarioFisico.DataGridINVENTARIO_INV_ACTIVO.Item(filaDataGrid, 5))

        If ejecutaQuery_PDA(comandoBD_PDA) Then
            actualizarMA_INV_FISICO = True
        End If

    End Function

    Sub finalizarInventario(ByVal numDoc As String, ByVal codDeposito As String)

        comandoBD_PDA.CommandText = "UPDATE MA_CONFIGURACION SET NUMultDOC = @NUMultDOC"
        comandoBD_PDA.Parameters.Add("@NUMultDOC", numDoc)

        ejecutaQuery_PDA(comandoBD_PDA)
        'Verifico que tenga conexión con el servidor

        If conectarDB_SERVIDOR(rutaDB_SERVIDOR) = 1 Then
            cargarInventario(numDoc, codDeposito)
            cerrarConexion_SERVIDOR()
        Else 'En caso de no tener conexión se guardara en pendientes por enviar
            guardarInventario(numDoc, codDeposito)
            MsgBox("Documento de inventario generado y pendiente por enviar, no hay conexión al servidor.", MsgBoxStyle.Information, "isMOBILE")
        End If

    End Sub

    Function cargarInventario(ByVal numDoc As String, ByVal codDeposito As String) As Boolean

        Dim dataSet_PDA2 As Data.DataSet
        Dim numDocumento As String

        'Construyendo el número de documento para el servidor
        SentenciaSQL = "SELECT RIGHT('000000" & numDoc & "', 6)"
        numDocumento = Id_PDA & returnString_SERVIDOR(SentenciaSQL)
        'Asegurar que aun esta abierto el inventario en el sistema
        SentenciaSQL = "SELECT COUNT(*) FROM MA_ELAB_INVENTARIO " + _
        "WHERE b_Finalizado = 0 " + _
        "AND c_CodDeposito = '" + codDeposito + "'"

        If returnInteger_SERVIDOR(SentenciaSQL) = 0 Then
            MsgBox("Inventario cerrado, se descartará la información.", MsgBoxStyle.Information, "isMOBILE")
            Return True
        Else

            comandoBD_PDA.CommandText = "SELECT * FROM MA_INV_FISICO WHERE C_DOCUMENTO = @C_DOCUMENTO"
            comandoBD_PDA.Parameters.Add("@C_DOCUMENTO", numDoc)
            dataSet_PDA = consultaQuery_PDA(comandoBD_PDA)
            comandoBD_PDA.CommandText = "SELECT * FROM TR_INV_FISICO WHERE C_DOCUMENTO = @C_DOCUMENTO"
            comandoBD_PDA.Parameters.Add("@C_DOCUMENTO", numDoc)
            dataSet_PDA2 = consultaQuery_PDA(comandoBD_PDA)

            If insertarRegistros_INV_FISICO_SERVIDOR(dataSet_PDA, numDocumento, dataSet_PDA2) = 1 Then
                MsgBox("Documento de inventario enviado.", MsgBoxStyle.Information, "isMOBILE")
                limpiar_INV_FISICO(numDoc)
                Return True
            Else
                guardarInventario(numDoc, codDeposito)
                MsgBox("Envio Fallido, documento de inventario guardado.", MsgBoxStyle.Information, "isMOBILE")
                Return False
            End If

        End If

    End Function


    Sub guardarInventario(ByVal numDoc As Integer, ByVal codDeposito As String)

        comandoBD_PDA.CommandText = "INSERT INTO MA_ENVIO_PENDIENTES (C_CONCEPTO, C_DOCUMENTO, C_DEPOSITO) VALUES ('INV', @c_DOCUMENTO, @c_DEPOSITO)"
        comandoBD_PDA.Parameters.Add("@c_DOCUMENTO", numDoc)
        comandoBD_PDA.Parameters.Add("@c_DEPOSITO", codDeposito)
        ejecutaQuery_PDA(comandoBD_PDA)

    End Sub

    Sub limpiar_INV_FISICO(ByVal numDoc As String)

        comandoBD_PDA.CommandText = "DELETE MA_INV_FISICO WHERE C_DOCUMENTO= @c_DOCUMENTO"
        comandoBD_PDA.Parameters.Add("@c_DOCUMENTO", numDoc)
        ejecutaQuery_PDA(comandoBD_PDA)
        comandoBD_PDA.CommandText = "DELETE TR_INV_FISICO WHERE C_DOCUMENTO= @c_DOCUMENTO"
        comandoBD_PDA.Parameters.Add("@c_DOCUMENTO", numDoc)
        ejecutaQuery_PDA(comandoBD_PDA)

    End Sub

    Sub iniciarInventario()

        'Obtengo la fila del DataGrid_INV_ACTIVO que el usuario selecciono para elaborar el inventario
        'Asi podre llenar la información basica del inventario
        filaDataGrid = FormInv_Activo_InventarioFisico.DataGridINVENTARIO_INV_ACTIVO.CurrentRowIndex

        If filaDataGrid = -1 Then
            MsgBox("No hay inventarios Activos", MsgBoxStyle.Information, "isMOBILE")
            formMenuPrincipal.Show()
            FormElab_Inv_InventarioFisico.Visible = False
        Else

            Dim TempDoc As Integer

            If CompatibilidadFueraDeLinea Then

                SentenciaSQL = "SELECT NUMultDOC FROM MA_CONFIGURACION"
                TempDoc = returnInteger_PDA(SentenciaSQL) + 1

            Else

                comandBD_SERVIDOR.Parameters.Clear()
                comandBD_SERVIDOR.CommandText = "SELECT isNULL(MAX(SUBSTRING(c_Documento, (" & _
                Len(Id_PDA).ToString & " + 1), 9999)), '0') AS NumUltimoConteoInv " + _
                "FROM MA_INV_FISICO WHERE 1 = 1 " & _
                "AND LEFT(c_Documento, " & Len(Id_PDA).ToString & ") = @ID_PDA " & _
                "AND c_CodLocalidad = @Loc "

                comandBD_SERVIDOR.Parameters.Add("@ID_PDA", Id_PDA)
                comandBD_SERVIDOR.Parameters.Add("@Loc", Localidad)

                TempDoc = Val(returnString_SERVIDOR(comandBD_SERVIDOR.CommandText)) + 1

            End If

            FormElab_Inv_InventarioFisico.lblNumDoc.Text = TempDoc
            FormElab_Inv_InventarioFisico.lblNumDeposito.Text = FormInv_Activo_InventarioFisico.DataGridINVENTARIO_INV_ACTIVO.Item(filaDataGrid, 1)
            FormElab_Inv_InventarioFisico.Linea = 1

            LimpiarDocumentoInventario(CStr(TempDoc))

        End If

    End Sub

    Private Sub LimpiarDocumentoInventario(ByVal numDoc As String)

        'Elimina datos de documentos incompletos.

        comandoBD_PDA.CommandText = "DELETE FROM MA_INV_FISICO WHERE C_DOCUMENTO = @c_DOCUMENTO"
        comandoBD_PDA.Parameters.Add("@c_DOCUMENTO", numDoc)
        ejecutaQuery_PDA(comandoBD_PDA)

        comandoBD_PDA.CommandText = "DELETE FROM TR_INV_FISICO WHERE C_DOCUMENTO = @c_DOCUMENTO"
        comandoBD_PDA.Parameters.Add("@c_DOCUMENTO", numDoc)
        ejecutaQuery_PDA(comandoBD_PDA)

    End Sub

    Sub llenaDataGrid_INV_ACTIVO()

        Dim Query As String = "SELECT F_INiCIO, INV.c_coddeposito, CU_CODDEPARTAMENTO, CU_CODGRUPO, CU_CODSUBGRUPO, CU_CODPROVEEDOR, no_elab " + _
        "FROM MA_ELAB_INVENTARIO AS INV INNER JOIN MA_DEPOSITO AS DEP ON DEP.c_coddeposito = INV.c_coddeposito"

        If CompatibilidadFueraDeLinea Then

            comandoBD_PDA.Parameters.Clear()
            comandoBD_PDA.CommandText = Query
            dataSet_PDA = consultaQuery_PDA(comandoBD_PDA)

        Else

            comandBD_SERVIDOR.Parameters.Clear()
            comandBD_SERVIDOR.CommandText = Query
            dataSet_PDA = executeQuery_Servidor(comandBD_SERVIDOR)

        End If

        'En la propiedad DataSource es donde puedo llenar el dataGrid con la informacion de la BD.
        FormInv_Activo_InventarioFisico.DataGridINVENTARIO_INV_ACTIVO.DataSource = dataSet_PDA.Tables(0)

        Dim Estilos As New DataGridTableStyle
        Dim columna1, columna2, columna3, columna4, columna5, columna6, columna7 As New DataGridTextBoxColumn
        Estilos.MappingName = dataSet_PDA.Tables(0).TableName

        With columna1
            .MappingName = "F_INiCIO"
            .HeaderText = "Fecha"
            .Width = 90
        End With
        With columna2
            .MappingName = "c_coddeposito"
            .HeaderText = "Depo"
            .Width = 50
        End With
        With columna3
            .MappingName = "CU_CODDEPARTAMENTO"
            .HeaderText = "Dept"
            .Width = 40
        End With
        With columna4
            .MappingName = "CU_CODGRUPO"
            .HeaderText = "Gru"
            .Width = 40
        End With
        With columna5
            .MappingName = "CU_CODSUBGRUPO"
            .HeaderText = "Sub"
            .Width = 30
        End With
        With columna6
            .MappingName = "CU_CODPROVEEDOR"
            .HeaderText = "Nº"
            .Width = 40
        End With
        With columna7
            .MappingName = "no_elab"
            .HeaderText = "Nº"
            .Width = 40
        End With

        Estilos.GridColumnStyles.Add(columna1)
        Estilos.GridColumnStyles.Add(columna2)
        Estilos.GridColumnStyles.Add(columna3)
        Estilos.GridColumnStyles.Add(columna4)
        Estilos.GridColumnStyles.Add(columna5)
        Estilos.GridColumnStyles.Add(columna6)
        Estilos.GridColumnStyles.Add(columna7)
        FormInv_Activo_InventarioFisico.DataGridINVENTARIO_INV_ACTIVO.TableStyles.Clear()
        FormInv_Activo_InventarioFisico.DataGridINVENTARIO_INV_ACTIVO.TableStyles.Add(Estilos)

    End Sub

    Sub CargarDataGridPickingUser(ByVal CodigoUsuario As String, ByRef pForm As FormPickingUserMain)

        Try

            'Verifico que tenga conexión con el servidor

            'CodigoUsuario = "PIC_" & CodigoUsuario ' Aqui esto no aplica.

            If conectarDB_SERVIDOR(rutaDB_SERVIDOR) = 0 Then
                MsgBox("No hay conexión con el Servidor. No se pueden actualizar los datos en este momento.", MsgBoxStyle.Information, "isMOBILE")
            Else

                Dim TmpConsulta As String

                Dim mCampoUbicaciones = "REPLACE(isNULL(REVERSE(SUBSTRING(REVERSE((SELECT UBC2.cu_Mascara + '    ' FROM MA_UBICACIONxPRODUCTO UBC2 WHERE UBC2.cu_Deposito + UBC2.cu_Producto = c_CodDeposito + CodProducto FOR XML PATH(''))), 1 + LEN('    '), 9999)), 'N/A'), '&#x20', '') AS Ubicacion"

                TmpConsulta = "SELECT MAX(CodEdi) AS CodEdi, MAX(Nombre) AS Nombre, MAX(Ubicacion) AS Ubicacion, CodProducto, SUM(CantSolicitada) AS CantSolicitada," & vbNewLine & _
                "SUM(CantRecolectada) as CantRecolectada, CantDecimales FROM (" & vbNewLine & _
                "SELECT CodEDI, Nombre, " & mCampoUbicaciones & ", CodProducto, CantSolicitada, CantRecolectada, CantDecimales FROM (" & vbNewLine & _
                "SELECT isNull(MA_Codigos.C_Codigo, MA_PRODUCTOS.C_Codigo) as CodEDI, " & vbNewLine & _
                "MA_PEDIDOS_RUTA_PICKING.CodProducto, MA_PRODUCTOS.C_DESCRI as Nombre, " & vbNewLine & _
                "MA_PEDIDOS_RUTA_PICKING.CodLote, SUM(MA_PEDIDOS_RUTA_PICKING.CantSolicitada) AS CantSolicitada, " & vbNewLine & _
                "SUM(MA_PEDIDOS_RUTA_PICKING.CantRecolectada) as CantRecolectada, 0 AS CantPedidos," & vbNewLine & _
                "MA_PRODUCTOS.Cant_Decimales AS CantDecimales, c_CodDeposito " & vbNewLine & _
                "FROM MA_PEDIDOS_RUTA_PICKING INNER JOIN MA_VENTAS " & vbNewLine & _
                "ON MA_PEDIDOS_RUTA_PICKING.CodPedido = MA_VENTAS.c_DOCUMENTO AND MA_VENTAS.c_CONCEPTO = 'PED'" & vbNewLine & _
                "inner join MA_PRODUCTOS on MA_PRODUCTOS.C_CODIGO " & vbNewLine & _
                "= MA_PEDIDOS_RUTA_PICKING.CodProducto " & vbNewLine & _
                "left join MA_Codigos on MA_Codigos.c_codnasa = MA_PRODUCTOS.C_Codigo and  " & vbNewLine & _
                "MA_Codigos.nu_intercambio = 1 where MA_PEDIDOS_RUTA_PICKING.CodRecolector = '" & CodigoUsuario & "'" & vbNewLine & _
                "AND MA_PEDIDOS_RUTA_PICKING.Picking = 0 " & vbNewLine & _
                "GROUP BY MA_PRODUCTOS.C_CODIGO, MA_Codigos.c_Codigo, MA_PEDIDOS_RUTA_PICKING.CodProducto, MA_PRODUCTOS.C_DESCRI, MA_PEDIDOS_RUTA_PICKING.CodLote, CodRecolector, Picking, MA_PRODUCTOS.Cant_Decimales, c_CodDeposito " & vbNewLine & _
                ") AS Todo GROUP BY Nombre, CodEDI, CantSolicitada, CantRecolectada, CodProducto, CodLote, CantDecimales, c_CodDeposito " & vbNewLine & _
                " UNION " & vbNewLine & _
                "(SELECT '' AS CodEdi, '' AS Nombre, '' AS Ubicacion, CodProducto, 0 AS CantSolicitada, 0 AS CantRecolectada " & vbNewLine & _
                ", P.Cant_Decimales AS CantDecimales FROM MA_PEDIDOS_RUTA_PICKING M INNER JOIN MA_PRODUCTOS P ON M.CodProducto = P.c_Codigo WHERE M.CodRecolector = '" & CodigoUsuario & "'" & vbNewLine & _
                "AND M.Picking = 0 GROUP BY CodProducto, P.Cant_Decimales) ) AS TB GROUP BY CodProducto, CantDecimales" & vbNewLine & _
                "ORDER BY isNull(MAX(Ubicacion), 'N/A'), isNull(MAX(Nombre), 'N/A')"

                comandoBD_SERVIDOR.CommandText = TmpConsulta
                nombreTabla = "PICKING_USER"
                dataSet_Servidor = ejecutaQuery_Servidor(comandoBD_SERVIDOR, nombreTabla)

                ' FormPickingUserMain

                pForm.DataGridProductos.PreferredRowHeight = _
                pForm.DataGridProductos.Height * 0.25

                pForm.DataGridProductos.DataSource = dataSet_Servidor.Tables(0)

                Dim Estilos As New DataGridTableStyle

                Dim Columna1, Columna2, Columna3, Columna4, Columna5, Columna6, Columna7 As New DataGridTextBoxColumn

                Estilos.MappingName = nombreTabla

                With Columna1
                    .MappingName = "CodEdi"
                    .HeaderText = "Cod."
                    .Width = ((110 * RuntimeVSDesignRatioX) * CurrentWidthFactor)
                End With

                With Columna2
                    .MappingName = "Nombre"
                    .HeaderText = "Desc."
                    .Width = ((60 * RuntimeVSDesignRatioX) * CurrentWidthFactor)
                End With

                With Columna3
                    .MappingName = "Ubicacion"
                    .HeaderText = "Ubc."
                    .Width = ((30 * RuntimeVSDesignRatioX) * CurrentWidthFactor)
                End With

                With Columna4
                    .MappingName = "CodProducto"
                    .HeaderText = "c_CodNasa"
                    .Width = -1
                End With

                With Columna5
                    .MappingName = "CantSolicitada"
                    .HeaderText = "Cant. Sol."
                    .Width = -1
                End With

                With Columna6
                    .MappingName = "CantRecolectada"
                    .HeaderText = "Cant. Rec."
                    .Width = -1
                End With

                With Columna7
                    .MappingName = "CantDecimales"
                    .HeaderText = "Cant. Dec."
                    .Width = -1
                End With

                Estilos.GridColumnStyles.Add(Columna1)
                Estilos.GridColumnStyles.Add(Columna2)
                Estilos.GridColumnStyles.Add(Columna3)
                Estilos.GridColumnStyles.Add(Columna4)
                Estilos.GridColumnStyles.Add(Columna5)
                Estilos.GridColumnStyles.Add(Columna6)
                Estilos.GridColumnStyles.Add(Columna7)

                pForm.DataGridProductos.TableStyles.Clear()
                pForm.DataGridProductos.TableStyles.Add(Estilos)

            End If

        Catch Ex As Exception

            MsgBox("Error cargando los productos del recolector." & vbNewLine & Ex.ToString, MsgBoxStyle.Information, "isMOBILE")

        End Try

    End Sub

    Function GuardarPickingUser(ByVal CodigoUsuario As String, _
    ByVal CodProducto As String, ByVal CantDecimalesProducto As Integer, _
    ByVal CantSolicitada As Double, ByVal CantDisponible As Double) As Boolean

        Dim SrvTrans As SqlTransaction = Nothing

        Try

            ' Verifico que tenga conexión con el servidor.

            If conectarDB_SERVIDOR(rutaDB_SERVIDOR) = 0 Then
                MsgBox("No hay conexión con el Servidor. No se puede guardar la cantidad en este momento.", MsgBoxStyle.Information, "isMOBILE")
            Else

                Dim TmpConsulta As String

                Dim Ds As DataSet, Rs As DataRow

                comandoBD_SERVIDOR.Parameters.Clear()
                comandoBD_SERVIDOR.CommandText = "SELECT * FROM MA_PEDIDOS_RUTA_PICKING WHERE CodProducto = '" & CodProducto & "'" & vbNewLine & _
                "AND CodRecolector = '" & CodigoUsuario & "' AND Picking = 0 ORDER BY FechaAsignacion ASC"

                Ds = ejecutaQuery_Servidor(comandoBD_SERVIDOR, "X")

                SrvTrans = conexionBD_SERVIDOR.BeginTransaction("PickingUser_" + CodigoUsuario)

                For Each Rs In Ds.Tables(0).Rows

                    If (CantDisponible >= Rs!CantSolicitada) Then

                        TmpConsulta = "UPDATE MA_PEDIDOS_RUTA_PICKING SET CantRecolectada = " & Replace(CStr(Rs("CantSolicitada")), ",", ".") & " " & vbNewLine & _
                        "WHERE CodLote = '" & CStr(Rs("CodLote")) & "' AND CodPedido = '" & CStr(Rs("CodPedido")) & "'" & vbNewLine & _
                        "AND CodProducto = '" & CodProducto & "'"

                        comandoBD_SERVIDOR.Parameters.Clear()
                        comandoBD_SERVIDOR.CommandText = TmpConsulta
                        comandoBD_SERVIDOR.Transaction = SrvTrans
                        comandoBD_SERVIDOR.ExecuteNonQuery()

                        CantDisponible = Math.Round((CantDisponible - CDbl(Rs("CantSolicitada"))), CantDecimalesProducto)

                    Else

                        TmpConsulta = "UPDATE MA_PEDIDOS_RUTA_PICKING SET CantRecolectada = " & Replace(CStr(CantDisponible), ",", ".") & " " & vbNewLine & _
                        "WHERE CodLote = '" & CStr(Rs("CodLote")) & "' AND CodPedido = '" & CStr(Rs("CodPedido")) & "'" & vbNewLine & _
                        "AND CodProducto = '" & CodProducto & "'"

                        comandoBD_SERVIDOR.Parameters.Clear()
                        comandoBD_SERVIDOR.CommandText = TmpConsulta
                        comandoBD_SERVIDOR.Transaction = SrvTrans
                        comandoBD_SERVIDOR.ExecuteNonQuery()

                        CantDisponible = 0

                    End If

                Next

                SrvTrans.Commit()

                GuardarPickingUser = True

            End If

            Exit Function

        Catch Ex As Exception

            If Not SrvTrans Is Nothing Then
                SrvTrans.Rollback()
            End If

            MsgBox("Error al guardar la cantidad recolectada." & vbNewLine & Ex.ToString, MsgBoxStyle.Information, "isMOBILE")

        End Try

    End Function

    Function FinalizarPickingUser(ByVal CodigoUsuario As String, _
    ByVal CodProducto As String, ByVal CantDecimalesProducto As Integer, _
    ByVal CantSolicitada As Double, ByVal CantRecolectada As Double, _
    ByVal FinalizarCantidadCero As Boolean) As Boolean

        Dim SrvTrans As SqlTransaction = Nothing

        Try

            ' Verifico que tenga conexión con el servidor.

            If conectarDB_SERVIDOR(rutaDB_SERVIDOR) = 0 Then
                MsgBox("No hay conexión con el Servidor. No se puede finalizar el picking del producto en este momento.", MsgBoxStyle.Information, "isMOBILE")
            Else

                Dim TmpConsulta As String

                Dim Ds As DataSet, Rs As DataRow

                comandoBD_SERVIDOR.Parameters.Clear()

                comandoBD_SERVIDOR.CommandText = "SELECT * FROM MA_PEDIDOS_RUTA_PICKING WHERE CodProducto = '" & CodProducto & "'" & vbNewLine & _
                "AND CodRecolector = '" & CodigoUsuario & "' AND Picking = 0 ORDER BY FechaAsignacion ASC"

                Ds = ejecutaQuery_Servidor(comandoBD_SERVIDOR, "X")

                SrvTrans = conexionBD_SERVIDOR.BeginTransaction("EndPickingUser_" + CodigoUsuario)

                For Each Rs In Ds.Tables(0).Rows

                    If (Not FinalizarCantidadCero And Rs("CantRecolectada") <> 0) Or FinalizarCantidadCero Then

                        If CantRecolectada = 0 Then

                            TmpConsulta = "UPDATE MA_PEDIDOS_RUTA_PICKING SET CantRecolectada = 0" & vbNewLine & _
                            "WHERE CodLote = '" & CStr(Rs("CodLote")) & "' AND CodPedido = '" & CStr(Rs("CodPedido")) & "'" & vbNewLine & _
                            "AND CodProducto = '" & CodProducto & "'"

                            comandoBD_SERVIDOR.Parameters.Clear()
                            comandoBD_SERVIDOR.CommandText = TmpConsulta
                            comandoBD_SERVIDOR.Transaction = SrvTrans
                            comandoBD_SERVIDOR.ExecuteNonQuery()

                        End If

                        TmpConsulta = "UPDATE MA_PEDIDOS_RUTA_PICKING SET Picking = 1 " & vbNewLine & _
                        "WHERE CodLote = '" & CStr(Rs("CodLote")) & "' AND CodPedido = '" & CStr(Rs("CodPedido")) & "'" & vbNewLine & _
                        "AND CodProducto = '" & CodProducto & "'"

                        comandoBD_SERVIDOR.Parameters.Clear()
                        comandoBD_SERVIDOR.CommandText = TmpConsulta
                        comandoBD_SERVIDOR.Transaction = SrvTrans
                        comandoBD_SERVIDOR.ExecuteNonQuery()

                    End If

                Next

                SrvTrans.Commit()

                FinalizarPickingUser = True

            End If

            Exit Function

        Catch Ex As Exception

            If Not SrvTrans Is Nothing Then
                SrvTrans.Rollback()
            End If

            MsgBox("Error al finalizar la recolección del producto." & vbNewLine & Ex.ToString, MsgBoxStyle.Information, "isMOBILE")

        End Try

    End Function

    Sub CargarDataGridPackingUser(ByVal CodigoUsuario As String, ByRef pForm As FormPackingUserMain)

        Try

            'Verifico que tenga conexión con el servidor

            'CodigoUsuario = "PAC_" & CodigoUsuario ' Aqui esto no aplica.

            If conectarDB_SERVIDOR(rutaDB_SERVIDOR) = 0 Then
                MsgBox("No hay conexión con el Servidor. No se pueden actualizar los datos en este momento.", MsgBoxStyle.Information, "isMOBILE")
            Else

                Dim TmpConsulta As String

                Dim OrderBy As String = "ORDER BY CodPedido"

                TmpConsulta = "SELECT CodPedido, Min(FechaAsignacion) as FechaAsignacion, " & vbNewLine & _
                "SUM(CantRecolectada) AS Articulos, CodLote FROM MA_PEDIDOS_RUTA_PICKING " & vbNewLine & _
                "WHERE CodEmpacador = '" & CodigoUsuario & "' AND Picking = 1 AND (PackingMobile_CantEmpaques = 0) AND Packing = 0" & vbNewLine & _
                "GROUP BY CodPedido, CodLote " & OrderBy

                comandoBD_SERVIDOR.CommandText = TmpConsulta
                nombreTabla = "PACKING_USER"
                dataSet_Servidor = ejecutaQuery_Servidor(comandoBD_SERVIDOR, nombreTabla)

                Dim Estilos As New DataGridTableStyle

                Dim Columna1, Columna2, Columna3, Columna4 As New DataGridTextBoxColumn

                ' FormPackingUserMain

                pForm.DataGridPedidos.PreferredRowHeight = _
                pForm.DataGridPedidos.Height * 0.25

                pForm.DataGridPedidos.DataSource = dataSet_Servidor.Tables(0)

                Estilos.MappingName = nombreTabla

                With Columna1
                    .MappingName = "CodPedido"
                    .HeaderText = "Pedido"
                    .Width = ((100 * RuntimeVSDesignRatioX) * CurrentWidthFactor)
                End With

                With Columna2
                    .MappingName = "FechaAsignacion"
                    .HeaderText = "Fecha"
                    .Width = ((70 * RuntimeVSDesignRatioX) * CurrentWidthFactor)
                End With

                With Columna3
                    .MappingName = "Articulos"
                    .HeaderText = "Items"
                    .Width = ((50 * RuntimeVSDesignRatioX) * CurrentWidthFactor)
                End With

                With Columna4
                    .MappingName = "CodLote"
                    .HeaderText = "Lote"
                    .Width = -1
                End With

                Estilos.GridColumnStyles.Add(Columna1)
                Estilos.GridColumnStyles.Add(Columna2)
                Estilos.GridColumnStyles.Add(Columna3)
                Estilos.GridColumnStyles.Add(Columna4)

                pForm.DataGridPedidos.TableStyles.Clear()
                pForm.DataGridPedidos.TableStyles.Add(Estilos)

            End If

        Catch Ex As Exception

            MsgBox("Error cargando los pedidos del empacador." & vbNewLine & Ex.ToString, MsgBoxStyle.Information, "isMOBILE")

        End Try

    End Sub

    Sub CargarDataGridPedidoPacking(ByVal CodigoUsuario As String, _
    ByVal CodLote As String, ByVal CodPedido As String)

        Try

            'Verifico que tenga conexión con el servidor

            'CodigoUsuario = "PAC_" & CodigoUsuario ' Aqui esto no aplica.

            If conectarDB_SERVIDOR(rutaDB_SERVIDOR) = 0 Then
                MsgBox("No hay conexión con el Servidor. No se pueden actualizar los datos en este momento.", MsgBoxStyle.Information, "isMOBILE")
            Else

                Dim TmpConsulta As String

                Dim OrderBy As String = "ORDER BY MA_PRODUCTOS.c_Descri, MA_PEDIDOS_RUTA_PICKING.CantSolicitada DESC"

                TmpConsulta = "SELECT isNull(MA_Codigos.C_Codigo, MA_PRODUCTOS.C_Codigo) AS CodEDI, MA_PRODUCTOS.C_DESCRI as Nombre, " & vbNewLine & _
                "MA_PEDIDOS_RUTA_PICKING.CantSolicitada, MA_PEDIDOS_RUTA_PICKING.CantRecolectada, " & vbNewLine & _
                "MA_PEDIDOS_RUTA_PICKING.CantEmpacada, MA_PEDIDOS_RUTA_PICKING.CodProducto " & vbNewLine & _
                ", MA_PRODUCTOS.Cant_Decimales AS CantDecimales FROM " & vbNewLine & _
                "MA_PEDIDOS_RUTA_PICKING INNER JOIN MA_PRODUCTOS ON MA_PRODUCTOS.C_CODIGO " & vbNewLine & _
                "= MA_PEDIDOS_RUTA_PICKING.CodProducto  " & vbNewLine & _
                "LEFT JOIN MA_Codigos on MA_Codigos.c_codnasa = MA_PRODUCTOS.C_Codigo and " & vbNewLine & _
                "MA_Codigos.nu_Intercambio = 1 WHERE MA_PEDIDOS_RUTA_PICKING.CodEmpacador = '" & CodigoUsuario & "' AND " & vbNewLine & _
                "MA_PEDIDOS_RUTA_PICKING.CodLote = '" & CodLote & "' AND MA_PEDIDOS_RUTA_PICKING.Picking = 1 " & vbNewLine & _
                "AND MA_PEDIDOS_RUTA_PICKING.CodPedido = '" & CodPedido & "'" & vbNewLine & _
                OrderBy

                ' Vamos a colocar una forma de Packing Movil para que le envíen los pedidos a un supervisor.

                comandoBD_SERVIDOR.CommandText = TmpConsulta
                nombreTabla = "PEDIDO_PACKING_USER"
                dataSet_Servidor = ejecutaQuery_Servidor(comandoBD_SERVIDOR, nombreTabla)

                FormDetallePedidoPacking.DataGridProductos.PreferredRowHeight = _
                FormDetallePedidoPacking.DataGridProductos.Height * 0.2

                FormDetallePedidoPacking.DataGridProductos.DataSource = dataSet_Servidor.Tables(0)

                Dim Estilos As New DataGridTableStyle

                Dim Columna1, Columna2, Columna3, Columna4, Columna5, Columna6, Columna7 As New DataGridTextBoxColumn

                Estilos.MappingName = nombreTabla

                With Columna1
                    .MappingName = "CodEDI"
                    .HeaderText = "Cod."
                    .Width = ((100 * RuntimeVSDesignRatioX) * CurrentWidthFactor)
                End With

                With Columna2
                    .MappingName = "Nombre"
                    .HeaderText = "Desc."
                    .Width = ((30 * RuntimeVSDesignRatioX) * CurrentWidthFactor)
                End With

                With Columna3
                    .MappingName = "CantSolicitada"
                    .HeaderText = "Sol."
                    .Width = -1
                End With

                With Columna4
                    .MappingName = "CantRecolectada"
                    .HeaderText = "Rec."
                    .Width = 42
                End With

                With Columna5
                    .MappingName = "CantEmpacada"
                    .HeaderText = "Emp."
                    .Width = 42
                End With

                With Columna6
                    .MappingName = "CodProducto"
                    .HeaderText = "c_CodNasa"
                    .Width = -1
                End With

                With Columna7
                    .MappingName = "CantDecimales"
                    .HeaderText = "Cant. Dec."
                    .Width = -1
                End With

                Estilos.GridColumnStyles.Add(Columna1)
                Estilos.GridColumnStyles.Add(Columna2)
                Estilos.GridColumnStyles.Add(Columna3)
                Estilos.GridColumnStyles.Add(Columna4)
                Estilos.GridColumnStyles.Add(Columna5)
                Estilos.GridColumnStyles.Add(Columna6)
                Estilos.GridColumnStyles.Add(Columna7)

                FormDetallePedidoPacking.DataGridProductos.TableStyles.Clear()
                FormDetallePedidoPacking.DataGridProductos.TableStyles.Add(Estilos)

                '

                OrderBy = "ORDER BY MA_PRODUCTOS.c_Descri, MA_PEDIDOS_RUTA_PICKING.CantSolicitada DESC"

                TmpConsulta = "SELECT MA_PEDIDOS_RUTA_PICKING.CantEmpacada, MA_PEDIDOS_RUTA_PICKING.CodProducto " & vbNewLine & _
                "FROM " & vbNewLine & _
                "MA_PEDIDOS_RUTA_PICKING INNER JOIN MA_PRODUCTOS ON MA_PRODUCTOS.C_CODIGO " & vbNewLine & _
                "= MA_PEDIDOS_RUTA_PICKING.CodProducto " & vbNewLine & _
                "WHERE MA_PEDIDOS_RUTA_PICKING.CodEmpacador = '" & CodigoUsuario & "' AND " & vbNewLine & _
                "MA_PEDIDOS_RUTA_PICKING.CodLote = '" & CodLote & "' AND MA_PEDIDOS_RUTA_PICKING.Picking = 1 " & vbNewLine & _
                "AND MA_PEDIDOS_RUTA_PICKING.CodPedido = '" & CodPedido & "'" & vbNewLine & _
                "AND (MA_PEDIDOS_RUTA_PICKING.CantEmpacada = MA_PEDIDOS_RUTA_PICKING.CantRecolectada)" & vbNewLine & _
                OrderBy

                comandoBD_SERVIDOR.CommandText = TmpConsulta
                nombreTabla = "PEDIDO_PACKING_USER"
                dataSet_Servidor = ejecutaQuery_Servidor(comandoBD_SERVIDOR, nombreTabla)

                If Not dataSet_Servidor Is Nothing Then
                    If dataSet_Servidor.Tables(0).Rows.Count > 0 Then
                        FormDetallePedidoPacking.CantEmpacados = dataSet_Servidor.Tables(0).Rows.Count
                    Else
                        FormDetallePedidoPacking.CantEmpacados = 0
                    End If
                End If

            End If

        Catch Ex As Exception

            MsgBox("Error cargando los productos del pedido." & vbNewLine & Ex.ToString, MsgBoxStyle.Information, "isMOBILE")

        End Try

    End Sub

    Function GuardarPackingUser(ByVal CodigoUsuario As String, _
    ByVal CodLote As String, ByVal CodPedido As String, _
    ByVal CodProducto As String, ByVal CantDecimalesProducto As Integer, _
    ByVal CantSolicitada As Double, ByVal CantRecolectada As Double, _
    ByVal CantEmpacada As Double) As Boolean

        Dim SrvTrans As SqlTransaction = Nothing

        Try

            ' Verifico que tenga conexión con el servidor.

            If conectarDB_SERVIDOR(rutaDB_SERVIDOR) = 0 Then
                MsgBox("No hay conexión con el Servidor. No se puede guardar la cantidad en este momento.", MsgBoxStyle.Information, "isMOBILE")
            Else

                Dim TmpConsulta As String

                SrvTrans = conexionBD_SERVIDOR.BeginTransaction("PackingUser_" + CodigoUsuario)

                comandoBD_SERVIDOR.Parameters.Clear()

                TmpConsulta = "UPDATE MA_PEDIDOS_RUTA_PICKING SET CantEmpacada = " & Replace(CantEmpacada.ToString, ",", ".") & " " & vbNewLine & _
                "WHERE CodLote = '" & CodLote & "' AND CodPedido = '" & CodPedido & "' AND " & vbNewLine & _
                "CodProducto = '" & CodProducto & "'"

                comandoBD_SERVIDOR.CommandText = TmpConsulta

                comandoBD_SERVIDOR.Transaction = SrvTrans

                comandoBD_SERVIDOR.ExecuteNonQuery()

                SrvTrans.Commit()

                GuardarPackingUser = True

            End If

            Exit Function

        Catch Ex As Exception

            If Not SrvTrans Is Nothing Then
                SrvTrans.Rollback()
            End If

            MsgBox("Error al guardar la cantidad empacada." & vbNewLine & Ex.ToString, MsgBoxStyle.Information, "isMOBILE")

        End Try

    End Function

    Function FinalizarPackingUser(ByVal CodigoUsuario As String, _
    ByVal CodLote As String, _
    ByVal CodPedido As String, _
    ByVal CantEmpaques As Long _
    ) As Boolean

        'Dim SrvTrans As SqlTransaction = Nothing

        Try

            ' Verifico que tenga conexión con el servidor.

            If conectarDB_SERVIDOR(rutaDB_SERVIDOR) = 0 Then
                MsgBox("No hay conexión con el Servidor. No se puede enviar el empacado en este momento.", MsgBoxStyle.Information, "isMOBILE")
            Else

                'Dim TmpConsulta As String

                'Dim Ds As DataSet, Rs As DataRow

                comandoBD_SERVIDOR.Parameters.Clear()

                comandoBD_SERVIDOR.CommandText = "UPDATE MA_PEDIDOS_RUTA_PICKING SET PackingMobile_CantEmpaques = @Cant WHERE CodLote = '" & CodLote & "' AND CodPedido = '" & CodPedido & "'" & vbNewLine & _
                "AND CodEmpacador = '" & CodigoUsuario & "' AND Packing = 0"

                comandoBD_SERVIDOR.Parameters.Add("@Cant", CantEmpaques)

                'SrvTrans = conexionBD_SERVIDOR.BeginTransaction("EndPackingUser_" + CodigoUsuario)

                'comandoBD_SERVIDOR.Transaction = SrvTrans

                comandoBD_SERVIDOR.ExecuteNonQuery()

                'SrvTrans.Commit()

                FinalizarPackingUser = True

            End If

            Exit Function

        Catch Ex As Exception

            'If Not SrvTrans Is Nothing Then
            'SrvTrans.Rollback()
            'End If

            MsgBox("Error al enviar el empacado." & vbNewLine & Ex.ToString, MsgBoxStyle.Information, "isMOBILE")

        End Try

    End Function

    Sub llenaDataGrid_DocPendientes()

        comandoBD_PDA.CommandText = "SELECT c_DOCUMENTO, c_DEPOSITO FROM MA_ENVIO_PENDIENTES"
        dataSet_PDA = consultaQuery_PDA(comandoBD_PDA)
        FormPendientes_InventarioFisico.DataDocPendientes.DataSource = dataSet_PDA.Tables(0)

        'Se crea una variable para dar el estilo al DataGrid
        Dim Estilos As New DataGridTableStyle
        'Se crea una variable para el estilo de cada columna
        Dim columna1, columna2 As New DataGridTextBoxColumn
        'Asigno la tabla a la variable de estilo
        Estilos.MappingName = dataSet_PDA.Tables(0).TableName
        'Asigno el estilo de cada columna

        With columna1
            .MappingName = "c_DOCUMENTO"    'nombre de columna a la cual queremos dar estilo
            .HeaderText = "Documento"       'texto de la columna
            .Width = 100                    'ancho de la columna
        End With
        With columna2
            .MappingName = "c_DEPOSITO"
            .HeaderText = "Deposito"
            .Width = 70
        End With

        'Se añade cada estilo de columno a la variable general que dara estilo al DataGrid
        Estilos.GridColumnStyles.Add(columna1)
        Estilos.GridColumnStyles.Add(columna2)
        'Limpio el estilo que tenga y asigno el nuevo
        FormPendientes_InventarioFisico.DataDocPendientes.TableStyles.Clear()
        FormPendientes_InventarioFisico.DataDocPendientes.TableStyles.Add(Estilos)

    End Sub

    Sub envio_Ind_Pendientes(ByVal fila As Integer)
        'Verifico que tenga conexión con el servidor
        If conectarDB_SERVIDOR(rutaDB_SERVIDOR) = 1 Then
            'Enviare el documento que eligio el usuario en el DataGrid
            If (cargarInventario(FormPendientes_InventarioFisico.DataDocPendientes.Item(fila, 0), FormPendientes_InventarioFisico.DataDocPendientes.Item(fila, 1))) Then
                actualizaPendientes(FormPendientes_InventarioFisico.DataDocPendientes.Item(fila, 0))
            End If
            llenaDataGrid_DocPendientes()
            cerrarConexion_SERVIDOR()
        Else
            MsgBox("El documento de inventario se mantiene pendiente por enviar, no hay conexión al servidor.", MsgBoxStyle.Information, "isMOBILE")
        End If

    End Sub

    Sub actualizaPendientes(ByVal numDoc As String)
        'Elimina el documento que se logro enviar al servidor
        comandoBD_PDA.CommandText = "DELETE MA_ENVIO_PENDIENTES WHERE C_DOCUMENTO = @c_DOCUMENTO"
        comandoBD_PDA.Parameters.Add("@c_DOCUMENTO", numDoc)
        ejecutaQuery_PDA(comandoBD_PDA)
    End Sub

    Sub envio_Todos_Pendientes()

        comandoBD_PDA.CommandText = "SELECT * FROM MA_ENVIO_PENDIENTES"
        dataSet_PDA = consultaQuery_PDA(comandoBD_PDA)
        'Verifico que tenga conexión con el servidor

        If conectarDB_SERVIDOR(rutaDB_SERVIDOR) = 1 Then

            'Enviara todos los documentos de la tabla pendientes por enviar
            For Each registro As Data.DataRow In dataSet_PDA.Tables(0).Rows
                If (cargarInventario(registro("c_DOCUMENTO"), registro("c_DEPOSITO"))) Then
                    actualizaPendientes(registro("c_DOCUMENTO"))
                End If
                llenaDataGrid_DocPendientes()
            Next

            cerrarConexion_SERVIDOR()

        Else
            MsgBox("Los documentos de inventario seguiran pendientes por enviar, no hay conexión.", MsgBoxStyle.Information, "isMOBILE")
        End If

    End Sub


    '**************************************** T R A S L A D O S ********************************************

    Function consultarProducto_MA_DEPOPROD(ByVal codigoProducto, ByVal codigoDeposito)

        nombreTabla = "MA_DEPOPROD"

        comandoBD_SERVIDOR.CommandText = ("SELECT * FROM MA_DEPOPROD WHERE c_codarticulo = @codigoProducto AND c_coddeposito = @codigoDeposito")
        comandoBD_SERVIDOR.Parameters.Add("@codigoProducto", codigoProducto)
        comandoBD_SERVIDOR.Parameters.Add("@codigoDeposito", codigoDeposito)
        dataSet_Servidor = ejecutaQuery_Servidor(comandoBD_SERVIDOR, nombreTabla)

        comandoBD_SERVIDOR.Parameters.Clear()

        Return dataSet_Servidor

    End Function

    Sub consultarNivelUsuario_PermiteTrasladarExistenciaNegativa(ByVal cantidadTraslado, ByVal existenciaProductoDepositoOrigen)
        'RegladeNegocio = "Trs_PermiteTrasladarExistenciaconNegativaNivel"
        'dataset_RegladeNegocio = consultarRegladeNegocio(RegladeNegocio)
        valorRegladeNegocio = obtenerRegladeNegocio_Servidor("Trs_PermiteTrasladarExistenciaconNegativaNivel", "9") 'CInt(dataset_RegladeNegocio.Tables(0).Rows(0)("valor"))

        'consulto el nivel del usuario ingresado en el formulario principal (formUsuario)
        dataset_NivelUsuario = consultarNivelUsuario_Servidor(formUsuario.txtUsuario.Text, formUsuario.txtContraseña.Text)
        nivelUsuario = CInt(dataset_NivelUsuario.Tables(0).Rows(0)("nivel"))

        If nivelUsuario < CInt(valorRegladeNegocio) Then
            MsgBox("El producto esta quedando en negativo en el inventario, debe solicitar una clave de autorización.", MsgBoxStyle.OkCancel, "Existencia Negativa")

            If MsgBoxResult.Ok Then
                FormConfirmarUsuarioyContraseña.ShowDialog()
                If FormConfirmarUsuarioyContraseña.DialogResult = Windows.Forms.DialogResult.OK Then
                    'consulto el nivel del usuario ingresado
                    dataset_NivelUsuario = consultarNivelUsuario_Servidor(FormConfirmarUsuarioyContraseña.txtUsuario.Text, FormConfirmarUsuarioyContraseña.txtContraseña.Text)
                    nivelUsuario = CInt(dataset_NivelUsuario.Tables(0).Rows(0)("nivel"))

                    If nivelUsuario < CInt(valorRegladeNegocio) Then
                        MsgBox("No posee el nivel necesario realizar el traslado de este producto.", MsgBoxStyle.Information, "isMOBILE")
                        FormLecturaProducto_Traslado.txtCodigoProducto.Text = ""
                        FormLecturaProducto_Traslado.lblDescripcion.Text = "Descripción:"
                        FormLecturaProducto_Traslado.txtCodigoProducto.Focus()
                        FormLecturaProducto_Traslado.txtEmpaques.Text = ""
                        FormLecturaProducto_Traslado.txtUnidades.Text = ""
                    Else
                        'MsgBox("Usted posee el nivel necesario para realizar el traslado de este producto", MsgBoxStyle.Information, "isMOBILE")
                        'FormLecturaProducto_Traslado.cmbAgregar.Focus()
                        'Agregar producto 
                        lineaTraslado = lineaTraslado + agregarProductoTR_INVENTARIO_SERVIDOR(cantidadTraslado, existenciaProductoDepositoOrigen)
                    End If
                Else 'Si el usuario no ingreso ningun usuario y contraseña (Le dio al botón Cancelar)
                    If FormConfirmarUsuarioyContraseña.DialogResult = Windows.Forms.DialogResult.Cancel Then
                        FormLecturaProducto_Traslado.txtCodigoProducto.Text = ""
                        FormLecturaProducto_Traslado.lblDescripcion.Text = "Descripción:"
                        FormLecturaProducto_Traslado.txtCodigoProducto.Focus()
                        FormLecturaProducto_Traslado.txtEmpaques.Text = ""
                        FormLecturaProducto_Traslado.txtUnidades.Text = ""
                    End If
                End If
            Else  'Si le da al botón cancelar, no desea ingresar una clave de autorización
                FormLecturaProducto_Traslado.txtCodigoProducto.Text = ""
                FormLecturaProducto_Traslado.lblDescripcion.Text = "Descripción:"
                FormLecturaProducto_Traslado.txtCodigoProducto.Focus()
                FormLecturaProducto_Traslado.txtEmpaques.Text = ""
                FormLecturaProducto_Traslado.txtUnidades.Text = ""
            End If

        Else 'El usuario ingresado incialmente si posee el nivel para recibir sin ODC
            MsgBox("El producto esta quedando en negativo en el inventario, ¿Desea continuar?", MsgBoxStyle.OkCancel, "Existencia Negativa")
            If MsgBoxResult.Ok Then
                'FormLecturaProducto_Traslado.cmbAgregar.Focus()
                'Agregar producto 
                lineaTraslado = lineaTraslado + agregarProductoTR_INVENTARIO_SERVIDOR(cantidadTraslado, existenciaProductoDepositoOrigen)
            Else
                FormLecturaProducto_Traslado.txtCodigoProducto.Text = ""
                FormLecturaProducto_Traslado.lblDescripcion.Text = "Descripción:"
                FormLecturaProducto_Traslado.txtCodigoProducto.Focus()
            End If

        End If
    End Sub

    Function determinarNumeroTraslado_SERVIDOR()

        Dim numeroTraslado_SERVIDORsinFormato As Double

        comandBD_SERVIDOR.CommandText = "SELECT nu_valor FROM MA_CORRELATIVOS WHERE cu_campo = 'Traslados'"
        dataSet_Servidor = executeQuery_Servidor(comandBD_SERVIDOR)

        numeroTraslado_SERVIDORsinFormato = (dataSet_Servidor.Tables(0).Rows(0)("nu_valor")) + 1

        Return numeroTraslado_SERVIDORsinFormato

    End Function

    Sub ReglasdeNegocioAgregarProducto_Traslados(ByVal unidades, ByVal empaques)

        Dim dataset_MA_DEPOPROD As New Data.DataSet
        Dim existenciaProductoDepositoOrigen As Double 'Es la cantidad que se va a colocar en MA_DEPOPROD para el deposito origen
        Dim cantidadTraslado As Double ' Es la cantidad que se va a colocar en TR_INVENTARIO

        cantidadTraslado = unidades + (empaques * UnidadesxEmpaque)

        respuesta = MsgBox("Cantidad Total: " & cantidadTraslado & ". " + FormLecturaProducto_Traslado.lblDescripcion.Text + ". ¿Esta seguro de proceder a registrar? ", MsgBoxStyle.YesNo, "Añadir producto al Traslado")

        If respuesta = "6" Then

            ' If conexionBD_SERVIDOR.State = ConnectionState.Open Then conexionBD_SERVIDOR.Close()

            If conectarDB_SERVIDOR(rutaDB_SERVIDOR) = 1 Then

                'Verifico si existen traslados pendientes por enviar y en caso de que hayan, los envio y actualizo el correlativo en el servidor
                'enviarTrasladosPendientes() 'Esto no es posible.

                dataset_MA_DEPOPROD.Reset()
                dataset_MA_DEPOPROD.Clear()
                dataset_MA_DEPOPROD = consultarProducto_MA_DEPOPROD(FormLecturaProducto_Traslado.txtCodigoProducto.Text, FormDepositoOrigenDestino_Traslado.txtDepositoOrigen.Text)

                If Not dataset_MA_DEPOPROD.Tables(0).Rows.Count = 0 Then

                    existenciaProductoDepositoOrigen = CDbl(dataset_MA_DEPOPROD.Tables(0).Rows(0)("n_cantidad")) - cantidadTraslado

                    If existenciaProductoDepositoOrigen < 0 Then

                        'RegladeNegocio = "Trs_PermiteTrasladarExistenciaconNegativa"
                        'dataset_RegladeNegocio = consultarRegladeNegocio(RegladeNegocio)
                        valorRegladeNegocio = obtenerRegladeNegocio_Servidor("Trs_PermiteTrasladarExistenciaconNegativa", "1") 'CInt(dataset_RegladeNegocio.Tables(0).Rows(0)("valor"))

                        If valorRegladeNegocio = "0" Then ' No permite trasladar si el producto queda con existencia negativa

                            'RegladeNegocio = "Trs_PermiteTrasladarExistenciaconNegativaMensaje"
                            'dataset_RegladeNegocio = consultarRegladeNegocio(RegladeNegocio)
                            valorRegladeNegocio = obtenerRegladeNegocio_Servidor("Trs_PermiteTrasladarExistenciaconNegativaMensaje", "1") 'CInt(dataset_RegladeNegocio.Tables(0).Rows(0)("valor"))

                            If valorRegladeNegocio = "1" Then
                                '********** consulto nivel y muestro mensaje ******************
                                consultarNivelUsuario_PermiteTrasladarExistenciaNegativa(cantidadTraslado, existenciaProductoDepositoOrigen)
                            Else

                                'No mostrar mensaje, por lo tanto no se consulta el nivel de usuario y no permite trasladar
                                FormLecturaProducto_Traslado.txtCodigoProducto.Text = ""
                                FormLecturaProducto_Traslado.lblCodigoProducto.Text = "Código:"
                                FormLecturaProducto_Traslado.txtCodigoProducto.Focus()
                                FormLecturaProducto_Traslado.txtEmpaques.Text = ""
                                FormLecturaProducto_Traslado.txtUnidades.Text = ""
                                MsgBox("El producto posee existencia negativa." & vbNewLine & "No se permite realizar el traslado.", MsgBoxStyle.Information, "isMOBILE")

                            End If
                        Else 'Si permite trasladar con existencia negativa
                            'RegladeNegocio = "Trs_PermiteTrasladarExistenciaconNegativaMensaje"
                            'dataset_RegladeNegocio = consultarRegladeNegocio(RegladeNegocio)
                            valorRegladeNegocio = obtenerRegladeNegocio_Servidor("Trs_PermiteTrasladarExistenciaconNegativaMensaje", "1") 'CInt(dataset_RegladeNegocio.Tables(0).Rows(0)("valor"))

                            If valorRegladeNegocio = "1" Then
                                '********** consulto nivel y muestro mensaje ******************
                                consultarNivelUsuario_PermiteTrasladarExistenciaNegativa(cantidadTraslado, existenciaProductoDepositoOrigen)
                            Else
                                'No mostrar mensaje, por lo tanto no se consulta el nivel de usuario y permite realizar  el traslado
                                'Insertar o actualizar datos en las tablas correspondientes
                                lineaTraslado = lineaTraslado + agregarProductoTR_INVENTARIO_SERVIDOR(cantidadTraslado, existenciaProductoDepositoOrigen)
                            End If
                        End If
                    Else 'la existencia no va a quedar negativa
                        'Insertar o actualizar datos en las tablas correspondientes
                        lineaTraslado = lineaTraslado + agregarProductoTR_INVENTARIO_SERVIDOR(cantidadTraslado, existenciaProductoDepositoOrigen)
                    End If
                Else 'El producto no se encuentra en MA_DEPOPROD
                    'Va a quedar con existencia negativa en la tabla (¿Se deben evaluar las reglas de negocio otra vez?) Yo creo que si!

                    'RegladeNegocio = "Trs_PermiteTrasladarExistenciaconNegativa"
                    'dataset_RegladeNegocio = consultarRegladeNegocio(RegladeNegocio)
                    valorRegladeNegocio = obtenerRegladeNegocio_Servidor("Trs_PermiteTrasladarExistenciaconNegativa", "1") 'CInt(dataset_RegladeNegocio.Tables(0).Rows(0)("valor"))

                    If valorRegladeNegocio = "0" Then ' No permite trasladar si el producto queda con existencia negativa
                        'RegladeNegocio = "Trs_PermiteTrasladarExistenciaconNegativa"
                        'dataset_RegladeNegocio = consultarRegladeNegocio(RegladeNegocio)
                        valorRegladeNegocio = obtenerRegladeNegocio_Servidor("Trs_PermiteTrasladarExistenciaconNegativa", "1") 'CInt(dataset_RegladeNegocio.Tables(0).Rows(0)("valor"))

                        If valorRegladeNegocio = "1" Then
                            '********** consulto nivel y muestro mensaje ******************
                            consultarNivelUsuario_PermiteTrasladarExistenciaNegativa(cantidadTraslado, existenciaProductoDepositoOrigen) 'Aqui tambien se debe añadir el producto cuando lo permita la funcion (agregar en la funcion)
                        Else
                            'No mostrar mensaje, por lo tanto no se consulta el nivel de usuario y no permite trasladar
                            FormLecturaProducto_Traslado.txtCodigoProducto.Text = ""
                            FormLecturaProducto_Traslado.lblCodigoProducto.Text = "Descripción:"
                            FormLecturaProducto_Traslado.txtCodigoProducto.Focus()
                            FormLecturaProducto_Traslado.txtEmpaques.Text = ""
                            FormLecturaProducto_Traslado.txtUnidades.Text = ""

                        End If
                    Else 'Si permite trasladar con existencia negativa
                        'RegladeNegocio = "Trs_PermiteTrasladarExistenciaconNegativaMensaje"
                        'dataset_RegladeNegocio = consultarRegladeNegocio(RegladeNegocio)
                        valorRegladeNegocio = obtenerRegladeNegocio_Servidor("Trs_PermiteTrasladarExistenciaconNegativaMensaje", "1") 'CInt(dataset_RegladeNegocio.Tables(0).Rows(0)("valor"))

                        If valorRegladeNegocio = "1" Then
                            '********** consulto nivel y muestro mensaje ******************
                            consultarNivelUsuario_PermiteTrasladarExistenciaNegativa(cantidadTraslado, existenciaProductoDepositoOrigen) 'Aqui tambien se debe añadir el producto cuando lo permita la funcion (agregar en la funcion)
                        Else
                            'No mostrar mensaje, por lo tanto no se consulta el nivel de usuario y permite realizar  el traslado
                            'Insertar o actualizar datos en las tablas correspondientes
                            existenciaProductoDepositoOrigen = cantidadTraslado * (-1) 'Porque el producto no existe en MA_DEPOPROD
                            lineaTraslado = lineaTraslado + agregarProductoTR_INVENTARIO_SERVIDOR(cantidadTraslado, existenciaProductoDepositoOrigen)
                        End If
                    End If
                End If
                cerrarConexion_SERVIDOR()
            Else
                MsgBox("No se puede consultar la existencia del producto, verifique conexión con el servidor.", MsgBoxStyle.Information, "Existencia Producto")
            End If

        End If

    End Sub

    Sub ActualizarNumUltimoTraslado_PDA()

        comandoBD_PDA.CommandText = "SELECT numUltimoTraslado FROM MA_CONFIGURACION"
        dataSet_PDA = consultaQuery_PDA(comandoBD_PDA)

        Dim numUltimoTrasladoProcesado As Double = dataSet_PDA.Tables(0).Rows(0)("numUltimoTraslado") + 1

        comandoBD_PDA.CommandText = "UPDATE MA_CONFIGURACION SET numUltimoTraslado = @numUltimoTrasladoProcesado"
        comandoBD_PDA.Parameters.Add("@numUltimoTrasladoProcesado", numUltimoTrasladoProcesado)

        ejecutaQuery_PDA(comandoBD_PDA)

    End Sub

    Sub limpiarFormLecturaProducto_Traslado()
        FormLecturaProducto_Traslado.txtCodigoProducto.Text = ""
        FormLecturaProducto_Traslado.lblDescripcion.Text = "Descripción:"
        FormLecturaProducto_Traslado.txtEmpaques.Text = ""
        FormLecturaProducto_Traslado.txtUnidades.Text = ""
        FormLecturaProducto_Traslado.lblUnidadesxEmpaque.Text = "Uni. x Emp.:"
        FormLecturaProducto_Traslado.txtCodigoProducto.Focus()

        'FormDepositoOrigenDestino_Traslado.Close()
        'FormEjecutorMotivo_Traslado.Close()
        'FormLecturaProducto_Traslado.Close()
        'FormObservacion_Traslado.Close()

        'Dim F1 As New FormDepositoOrigenDestino_Traslado
        'Dim F2 As New FormEjecutorMotivo_Traslado
        'Dim F3 As New FormLecturaProducto_Traslado
        'Dim F4 As New FormObservacion_Traslado

    End Sub

    Sub limpiarFormDepositoOrigenDestino()
        FormDepositoOrigenDestino_Traslado.txtDepositoOrigen.Text = ""
        FormDepositoOrigenDestino_Traslado.txtDepositoDestino.Text = ""
        FormDepositoOrigenDestino_Traslado.lblDescripcionDepositoOrigen.Text = "Descripción:"
        FormDepositoOrigenDestino_Traslado.lblDescripcionDepositoDestino.Text = "Descripcion:"
    End Sub

    Sub limpiarFormEjecutorMotivo()
        FormEjecutorMotivo_Traslado.txtEjecutor.Text = ""
    End Sub

    Sub enviarTrasladosPendientes()

        Dim dataset_TR_TRASLADOS_PDA As New Data.DataSet
        Dim dataset_MA_TRASLADOS_PDA As New Data.DataSet

        comandoBD_PDA.CommandText = "SELECT * FROM TR_TRASLADOS_PDA"
        dataset_TR_TRASLADOS_PDA = consultaQuery_PDA(comandoBD_PDA)

        If Not dataset_TR_TRASLADOS_PDA.Tables(0).Rows.Count = 0 Then
            agregarProductoPendienteTR_INVENTARIO_SERVIDOR(dataset_TR_TRASLADOS_PDA) 'Guarda en TR_INVENTARIO y en MA_DEPOPROD
        End If

        'Verifico que existen registros en MA_TRASLADOS_PDA pendientes por enviar

        'comandoBD_PDA.CommandText = "SELECT * FROM MA_TRASLADOS_PDA"
        'dataset_MA_TRASLADOS_PDA = consultaQuery_PDA(comandoBD_PDA)

        'If Not dataset_MA_TRASLADOS_PDA.Tables(0).Rows.Count = 0 Then
        'agregarProductoPendienteMA_INVENTARIO_SERVIDOR(dataset_MA_TRASLADOS_PDA) 'Guarda en MA_INVENTARIO y actualiza MA_CORRELATIVOS
        'End If

    End Sub

    Public Sub AjustarFormularios(ByVal TargetForm As Form)

        If Not (IsNumeric(FormDesignHeight) Or IsNumeric(FormDesignWidth) Or IsNumeric(CurrentScreenHeight) Or IsNumeric(CurrentScreenWidth)) Then
            CurrentHeightFactor = 1
            CurrentWidthFactor = 1
            CurrentFontFactor = 1
            Exit Sub
        Else
            AjustarPantalla(TargetForm, Val(CurrentScreenWidth), Val(CurrentScreenHeight))
        End If

    End Sub

    Public Sub AjustarFormularios(ByVal TargetForm As Panel)

        If Not (IsNumeric(FormDesignHeight) Or IsNumeric(FormDesignWidth) Or IsNumeric(CurrentScreenHeight) Or IsNumeric(CurrentScreenWidth)) Then
            CurrentHeightFactor = 1
            CurrentWidthFactor = 1
            CurrentFontFactor = 1
            Exit Sub
        Else
            AjustarPantalla(TargetForm, Val(CurrentScreenWidth), Val(CurrentScreenHeight))
        End If

    End Sub

    Private Sub AjustarPantalla(ByVal TargetForm As Form, ByVal XSize As Integer, ByVal YSize As Integer)

        On Error Resume Next

        TargetForm.Top = 0

        TargetForm.Left = 0

        If DebugMode Then MsgBox("Form Original Height: " & TargetForm.Height.ToString & vbNewLine & _
        "Form Original Width: " & TargetForm.Width.ToString & vbNewLine & _
        "Intended Height: " & YSize.ToString & vbNewLine & _
        "Intended Width: " & XSize.ToString & vbNewLine & _
        "Height Ratio: " & (YSize / Val(FormDesignHeight)).ToString & vbNewLine & _
        "Width Ratio: " & (XSize / Val(FormDesignWidth)).ToString)

        TargetForm.Height = YSize

        TargetForm.Width = XSize

        TargetForm.MinimizeBox = False
        TargetForm.MaximizeBox = False
        TargetForm.AutoScaleMode = AutoScaleMode.Dpi
        TargetForm.AutoScroll = True
        TargetForm.FormBorderStyle = FormBorderStyle.None

        Dim HeightRatio As Double
        Dim WidthRatio As Double
        Dim FontFactor As Double

        HeightRatio = (YSize / Val(FormDesignHeight))
        WidthRatio = (XSize / Val(FormDesignWidth))
        FontFactor = (HeightRatio + WidthRatio) / 2

        CurrentHeightFactor = HeightRatio
        CurrentWidthFactor = WidthRatio
        CurrentFontFactor = FontFactor

        For i = 0 To TargetForm.Controls.Count - 1

            TargetForm.Controls(i).Left = TargetForm.Controls(i).Left * WidthRatio

            TargetForm.Controls(i).Top = TargetForm.Controls(i).Top * HeightRatio

            TargetForm.Controls(i).Width = TargetForm.Controls(i).Width * WidthRatio

            TargetForm.Controls(i).Height = TargetForm.Controls(i).Height * HeightRatio

            AjustarFuente(TargetForm.Controls(i), FontFactor)

        Next i

    End Sub

    Private Sub AjustarPantalla(ByVal TargetForm As Panel, ByVal XSize As Integer, ByVal YSize As Integer)

        On Error Resume Next

        TargetForm.Top = 0

        TargetForm.Left = 0

        TargetForm.Height = YSize

        TargetForm.Width = XSize

        TargetForm.AutoScroll = True

        Dim HeightRatio As Double
        Dim WidthRatio As Double
        Dim FontFactor As Double

        WidthRatio = (XSize / Val(FormDesignWidth))
        HeightRatio = (YSize / Val(FormDesignHeight))
        FontFactor = (HeightRatio + WidthRatio) / 2

        For i = 0 To TargetForm.Controls.Count - 1

            TargetForm.Controls(i).Left = TargetForm.Controls(i).Left * WidthRatio

            TargetForm.Controls(i).Top = TargetForm.Controls(i).Top * HeightRatio

            TargetForm.Controls(i).Width = TargetForm.Controls(i).Width * WidthRatio

            TargetForm.Controls(i).Height = TargetForm.Controls(i).Height * HeightRatio

            AjustarFuente(TargetForm.Controls(i), FontFactor)

        Next i

    End Sub

    Private Sub AjustarFuente(ByRef Ctrl As Control, ByVal Factor As Double)

        On Error Resume Next

        Ctrl.Font = New Font(Ctrl.Font.Name, (Ctrl.Font.Size * Factor), Ctrl.Font.Style)

    End Sub

    Private Function ValorSetup(ByVal Setup As XmlNodeList, ByVal index As Integer, Optional ByVal ValorDefault As String = "") As String

        Dim Nodo As String

        Try
            Nodo = Setup(0).ChildNodes.Item(index).InnerText
        Catch ex As Exception
            Nodo = ValorDefault
        End Try

        Return Nodo

    End Function

    Public Sub SeleccionarTexto(ByVal pControl As TextBox)
        On Error Resume Next
        pControl.SelectionStart = 0
        pControl.SelectionLength = Len(pControl.Text)
    End Sub

    Public Function ConversionBolivarFuerte(ByVal pConvertir) As Double
        If IsNumeric(pConvertir) Then
            ConversionBolivarFuerte = CDbl(pConvertir) / 100000
            ' Redondeo Decimales. ' Actualizacion Reconversion Monetaria VE 2018-08
            Dim TmpValor = Math.Round(CDec(ConversionBolivarFuerte), 2)
            Dim TmpEntero = Fix(ConversionBolivarFuerte)
            Dim TmpRestoDecimal = Math.Round(CDec(TmpValor - TmpEntero), 2) * 100
            If TmpRestoDecimal < 25 Then
                ConversionBolivarFuerte = TmpEntero
            ElseIf TmpRestoDecimal < 50 Then
                TmpRestoDecimal = 0.5
                ConversionBolivarFuerte = TmpEntero + TmpRestoDecimal
            ElseIf TmpRestoDecimal < 75 Then
                TmpRestoDecimal = 0.5
                ConversionBolivarFuerte = TmpEntero + TmpRestoDecimal
            Else '(TmpRestoDecimal < 100)'
                TmpRestoDecimal = 1
                ConversionBolivarFuerte = TmpEntero + TmpRestoDecimal
            End If
        End If
    End Function

    Public Function MontoPREReconversion(ByVal pConvertir) As Double
        If IsNumeric(pConvertir) Then
            ' Por Normativa Reconversión Monetaria 2018 mostrar los montos como
            ' estaban antes mientras el público se familiariza con la
            ' reconversión, hasta que el BCV Disponga.
            MontoPREReconversion = CDbl(pConvertir) * 100000
        End If
    End Function

    Public Function FactorizarPrecio(ByVal pValor As Double, _
    Optional ByVal pFactor As Double = -1, _
    Optional ByRef pMonedaOrigen As String = "", _
    Optional ByVal pFactorizarMoneda As String = "") As Double

        If Not FactorizarPrecios Then
            FactorizarPrecio = pValor
            Return FactorizarPrecio
        End If

        Dim mFactorOrigen As Double, mFactorDestino As Double, mFactorTmp As Double
        Dim Query As String

        If pFactor > 0 And pFactorizarMoneda = String.Empty Then

            FactorizarPrecio = Math.Round(pValor * pFactor, 8)

        ElseIf Not String.IsNullOrEmpty(pMonedaOrigen) Then

            If pFactorizarMoneda <> String.Empty Then

                Query = "SELECT n_Factor FROM MA_MONEDAS " & _
                "WHERE c_CodMoneda = '" & pMonedaOrigen & "'"

                comandBD_SERVIDOR.Parameters.Clear()
                comandBD_SERVIDOR.CommandText = Query
                mFactorOrigen = GetScalar_SRV(comandBD_SERVIDOR, conexionBD_SERVIDOR, 1, 1)

                Query = "SELECT c_Simbolo FROM MA_MONEDAS " & _
                "WHERE c_CodMoneda = '" & pMonedaOrigen & "'"

                comandBD_SERVIDOR.CommandText = Query
                TmpMoneda_Sim = GetScalar_SRV(comandBD_SERVIDOR, conexionBD_SERVIDOR, 1, 1)

                If pFactor > 0 Then
                    mFactorDestino = pFactor
                Else

                    Query = "SELECT n_Factor FROM MA_MONEDAS " & _
                    "WHERE c_CodMoneda = '" & pFactorizarMoneda & "'"

                    comandBD_SERVIDOR.Parameters.Clear()
                    comandBD_SERVIDOR.CommandText = Query
                    mFactorDestino = GetScalar_SRV(comandBD_SERVIDOR, conexionBD_SERVIDOR, 1, 1)

                End If

                If mFactorOrigen > 0 And mFactorDestino > 0 Then
                    FactorizarPrecio = Math.Round(pValor * (mFactorOrigen / mFactorDestino), 8)
                Else
                    FactorizarPrecio = pValor
                End If

            ElseIf pMonedaOrigen <> Moneda_Cod _
            And pMonedaOrigen <> String.Empty Then

                Query = "SELECT n_Factor FROM MA_MONEDAS " & _
                "WHERE c_CodMoneda = '" & pMonedaOrigen & "'"

                comandBD_SERVIDOR.Parameters.Clear()
                comandBD_SERVIDOR.CommandText = Query
                mFactorTmp = GetScalar_SRV(comandBD_SERVIDOR, conexionBD_SERVIDOR, 1, 1)

                Query = "SELECT c_Simbolo FROM MA_MONEDAS " & _
                "WHERE c_CodMoneda = '" & pMonedaOrigen & "'"

                comandBD_SERVIDOR.CommandText = Query
                TmpMoneda_Sim = GetScalar_SRV(comandBD_SERVIDOR, conexionBD_SERVIDOR, 1, 1)

                If mFactorTmp > 0 Then
                    FactorizarPrecio = Math.Round(pValor * mFactorTmp, 8)
                Else
                    FactorizarPrecio = pValor
                End If

            Else
                FactorizarPrecio = pValor
            End If

        End If

        Return FactorizarPrecio

    End Function

End Module