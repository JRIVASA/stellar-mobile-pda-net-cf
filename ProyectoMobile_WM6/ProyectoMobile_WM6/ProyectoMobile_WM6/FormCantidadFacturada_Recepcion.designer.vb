﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class FormCantidadFacturada_Recepcion
    Inherits System.Windows.Forms.Form

    'Form invalida a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer
    Private mainMenu1 As System.Windows.Forms.MainMenu

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar con el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormCantidadFacturada_Recepcion))
        Me.mainMenu1 = New System.Windows.Forms.MainMenu
        Me.lblCantidadRecibida = New System.Windows.Forms.Label
        Me.lblUnidadesxEmpaque = New System.Windows.Forms.Label
        Me.txtEmpaquesFacturados = New System.Windows.Forms.TextBox
        Me.lblEmpaquesFacturados = New System.Windows.Forms.Label
        Me.lblUnidadesFacturadas = New System.Windows.Forms.Label
        Me.txtUnidadesFacturadas = New System.Windows.Forms.TextBox
        Me.PictureFondo = New System.Windows.Forms.PictureBox
        Me.lblFechaRecepcion = New System.Windows.Forms.Label
        Me.lblNºRec = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.lblRec = New System.Windows.Forms.Label
        Me.picBarraMorada = New System.Windows.Forms.PictureBox
        Me.cmbSalir = New System.Windows.Forms.PictureBox
        Me.cmbFinalizar = New System.Windows.Forms.Button
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.lblCantidadFaltante = New System.Windows.Forms.Label
        Me.lblUnidadesSolicitadas = New System.Windows.Forms.Label
        Me.lblEmpaquesSolicitados = New System.Windows.Forms.Label
        Me.lblUsuarioEnSesion = New System.Windows.Forms.Label
        Me.TimerReload = New System.Windows.Forms.Timer
        Me.SuspendLayout()
        '
        'lblCantidadRecibida
        '
        Me.lblCantidadRecibida.BackColor = System.Drawing.Color.Gainsboro
        Me.lblCantidadRecibida.Font = New System.Drawing.Font("Verdana", 14.0!, System.Drawing.FontStyle.Bold)
        Me.lblCantidadRecibida.Location = New System.Drawing.Point(10, 125)
        Me.lblCantidadRecibida.Name = "lblCantidadRecibida"
        Me.lblCantidadRecibida.Size = New System.Drawing.Size(218, 25)
        Me.lblCantidadRecibida.Text = "Cantidad Facturada"
        Me.lblCantidadRecibida.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblUnidadesxEmpaque
        '
        Me.lblUnidadesxEmpaque.BackColor = System.Drawing.Color.Gainsboro
        Me.lblUnidadesxEmpaque.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblUnidadesxEmpaque.Location = New System.Drawing.Point(52, 105)
        Me.lblUnidadesxEmpaque.Name = "lblUnidadesxEmpaque"
        Me.lblUnidadesxEmpaque.Size = New System.Drawing.Size(147, 23)
        Me.lblUnidadesxEmpaque.Text = "Uni. x Emp.:"
        '
        'txtEmpaquesFacturados
        '
        Me.txtEmpaquesFacturados.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold)
        Me.txtEmpaquesFacturados.Location = New System.Drawing.Point(126, 153)
        Me.txtEmpaquesFacturados.MaxLength = 10
        Me.txtEmpaquesFacturados.Name = "txtEmpaquesFacturados"
        Me.txtEmpaquesFacturados.Size = New System.Drawing.Size(75, 23)
        Me.txtEmpaquesFacturados.TabIndex = 91
        '
        'lblEmpaquesFacturados
        '
        Me.lblEmpaquesFacturados.BackColor = System.Drawing.Color.Gainsboro
        Me.lblEmpaquesFacturados.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblEmpaquesFacturados.Location = New System.Drawing.Point(23, 153)
        Me.lblEmpaquesFacturados.Name = "lblEmpaquesFacturados"
        Me.lblEmpaquesFacturados.Size = New System.Drawing.Size(97, 23)
        Me.lblEmpaquesFacturados.Text = "Empaques"
        Me.lblEmpaquesFacturados.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblUnidadesFacturadas
        '
        Me.lblUnidadesFacturadas.BackColor = System.Drawing.Color.Gainsboro
        Me.lblUnidadesFacturadas.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblUnidadesFacturadas.Location = New System.Drawing.Point(41, 181)
        Me.lblUnidadesFacturadas.Name = "lblUnidadesFacturadas"
        Me.lblUnidadesFacturadas.Size = New System.Drawing.Size(79, 24)
        Me.lblUnidadesFacturadas.Text = "Unidades"
        Me.lblUnidadesFacturadas.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtUnidadesFacturadas
        '
        Me.txtUnidadesFacturadas.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold)
        Me.txtUnidadesFacturadas.Location = New System.Drawing.Point(126, 182)
        Me.txtUnidadesFacturadas.MaxLength = 10
        Me.txtUnidadesFacturadas.Name = "txtUnidadesFacturadas"
        Me.txtUnidadesFacturadas.Size = New System.Drawing.Size(75, 23)
        Me.txtUnidadesFacturadas.TabIndex = 90
        '
        'PictureFondo
        '
        Me.PictureFondo.Image = CType(resources.GetObject("PictureFondo.Image"), System.Drawing.Image)
        Me.PictureFondo.Location = New System.Drawing.Point(42, 0)
        Me.PictureFondo.Name = "PictureFondo"
        Me.PictureFondo.Size = New System.Drawing.Size(198, 38)
        Me.PictureFondo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'lblFechaRecepcion
        '
        Me.lblFechaRecepcion.BackColor = System.Drawing.Color.Gainsboro
        Me.lblFechaRecepcion.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold)
        Me.lblFechaRecepcion.Location = New System.Drawing.Point(62, 75)
        Me.lblFechaRecepcion.Name = "lblFechaRecepcion"
        Me.lblFechaRecepcion.Size = New System.Drawing.Size(132, 15)
        '
        'lblNºRec
        '
        Me.lblNºRec.BackColor = System.Drawing.Color.Gainsboro
        Me.lblNºRec.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold)
        Me.lblNºRec.Location = New System.Drawing.Point(62, 61)
        Me.lblNºRec.Name = "lblNºRec"
        Me.lblNºRec.Size = New System.Drawing.Size(132, 15)
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Gainsboro
        Me.Label2.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold)
        Me.Label2.Location = New System.Drawing.Point(5, 75)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(61, 15)
        Me.Label2.Text = "Fecha:"
        '
        'lblRec
        '
        Me.lblRec.BackColor = System.Drawing.Color.Gainsboro
        Me.lblRec.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold)
        Me.lblRec.Location = New System.Drawing.Point(5, 61)
        Me.lblRec.Name = "lblRec"
        Me.lblRec.Size = New System.Drawing.Size(61, 15)
        Me.lblRec.Text = "Nº Rec.:"
        '
        'picBarraMorada
        '
        Me.picBarraMorada.Image = CType(resources.GetObject("picBarraMorada.Image"), System.Drawing.Image)
        Me.picBarraMorada.Location = New System.Drawing.Point(0, 260)
        Me.picBarraMorada.Name = "picBarraMorada"
        Me.picBarraMorada.Size = New System.Drawing.Size(240, 40)
        Me.picBarraMorada.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'cmbSalir
        '
        Me.cmbSalir.Image = CType(resources.GetObject("cmbSalir.Image"), System.Drawing.Image)
        Me.cmbSalir.Location = New System.Drawing.Point(0, 0)
        Me.cmbSalir.Name = "cmbSalir"
        Me.cmbSalir.Size = New System.Drawing.Size(41, 38)
        Me.cmbSalir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'cmbFinalizar
        '
        Me.cmbFinalizar.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.cmbFinalizar.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.cmbFinalizar.ForeColor = System.Drawing.Color.Black
        Me.cmbFinalizar.Location = New System.Drawing.Point(78, 265)
        Me.cmbFinalizar.Name = "cmbFinalizar"
        Me.cmbFinalizar.Size = New System.Drawing.Size(78, 25)
        Me.cmbFinalizar.TabIndex = 112
        Me.cmbFinalizar.Text = "Finalizar"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Gainsboro
        Me.PictureBox1.Location = New System.Drawing.Point(0, 54)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(240, 205)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'lblCantidadFaltante
        '
        Me.lblCantidadFaltante.BackColor = System.Drawing.Color.Gainsboro
        Me.lblCantidadFaltante.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold)
        Me.lblCantidadFaltante.Location = New System.Drawing.Point(25, 239)
        Me.lblCantidadFaltante.Name = "lblCantidadFaltante"
        Me.lblCantidadFaltante.Size = New System.Drawing.Size(163, 15)
        Me.lblCantidadFaltante.Text = "Cant. Faltante:"
        '
        'lblUnidadesSolicitadas
        '
        Me.lblUnidadesSolicitadas.BackColor = System.Drawing.Color.Gainsboro
        Me.lblUnidadesSolicitadas.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold)
        Me.lblUnidadesSolicitadas.Location = New System.Drawing.Point(137, 215)
        Me.lblUnidadesSolicitadas.Name = "lblUnidadesSolicitadas"
        Me.lblUnidadesSolicitadas.Size = New System.Drawing.Size(96, 15)
        Me.lblUnidadesSolicitadas.Text = "Uni. Sol.:"
        '
        'lblEmpaquesSolicitados
        '
        Me.lblEmpaquesSolicitados.BackColor = System.Drawing.Color.Gainsboro
        Me.lblEmpaquesSolicitados.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold)
        Me.lblEmpaquesSolicitados.Location = New System.Drawing.Point(25, 215)
        Me.lblEmpaquesSolicitados.Name = "lblEmpaquesSolicitados"
        Me.lblEmpaquesSolicitados.Size = New System.Drawing.Size(106, 15)
        Me.lblEmpaquesSolicitados.Text = "Emp. Sol.:"
        '
        'lblUsuarioEnSesion
        '
        Me.lblUsuarioEnSesion.BackColor = System.Drawing.Color.DarkGray
        Me.lblUsuarioEnSesion.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblUsuarioEnSesion.Location = New System.Drawing.Point(0, 39)
        Me.lblUsuarioEnSesion.Name = "lblUsuarioEnSesion"
        Me.lblUsuarioEnSesion.Size = New System.Drawing.Size(240, 15)
        Me.lblUsuarioEnSesion.Text = "Usuario:"
        '
        'TimerReload
        '
        Me.TimerReload.Interval = 1250
        '
        'FormCantidadFacturada_Recepcion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(240, 300)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblUsuarioEnSesion)
        Me.Controls.Add(Me.lblEmpaquesSolicitados)
        Me.Controls.Add(Me.txtEmpaquesFacturados)
        Me.Controls.Add(Me.txtUnidadesFacturadas)
        Me.Controls.Add(Me.lblCantidadFaltante)
        Me.Controls.Add(Me.lblFechaRecepcion)
        Me.Controls.Add(Me.lblNºRec)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblRec)
        Me.Controls.Add(Me.lblUnidadesSolicitadas)
        Me.Controls.Add(Me.lblCantidadRecibida)
        Me.Controls.Add(Me.lblUnidadesxEmpaque)
        Me.Controls.Add(Me.lblEmpaquesFacturados)
        Me.Controls.Add(Me.lblUnidadesFacturadas)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.cmbFinalizar)
        Me.Controls.Add(Me.cmbSalir)
        Me.Controls.Add(Me.picBarraMorada)
        Me.Controls.Add(Me.PictureFondo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormCantidadFacturada_Recepcion"
        Me.Text = "isMOBILE - Recepción"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblCantidadRecibida As System.Windows.Forms.Label
    Friend WithEvents lblUnidadesxEmpaque As System.Windows.Forms.Label
    Friend WithEvents txtEmpaquesFacturados As System.Windows.Forms.TextBox
    Friend WithEvents lblEmpaquesFacturados As System.Windows.Forms.Label
    Friend WithEvents lblUnidadesFacturadas As System.Windows.Forms.Label
    Friend WithEvents txtUnidadesFacturadas As System.Windows.Forms.TextBox
    Friend WithEvents PictureFondo As System.Windows.Forms.PictureBox
    Friend WithEvents lblFechaRecepcion As System.Windows.Forms.Label
    Friend WithEvents lblNºRec As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblRec As System.Windows.Forms.Label
    Friend WithEvents picBarraMorada As System.Windows.Forms.PictureBox
    Friend WithEvents cmbSalir As System.Windows.Forms.PictureBox
    Friend WithEvents cmbFinalizar As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents lblCantidadFaltante As System.Windows.Forms.Label
    Friend WithEvents lblUnidadesSolicitadas As System.Windows.Forms.Label
    Friend WithEvents lblEmpaquesSolicitados As System.Windows.Forms.Label
    Friend WithEvents lblUsuarioEnSesion As System.Windows.Forms.Label
    Friend WithEvents TimerReload As System.Windows.Forms.Timer
End Class
