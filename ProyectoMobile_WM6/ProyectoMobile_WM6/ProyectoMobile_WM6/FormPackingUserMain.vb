﻿Public Class FormPackingUserMain

    Private FormaCargada As Boolean

    Private Sub DataGridPedidos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridPedidos.Click

        If DataGridPedidos.VisibleRowCount <= 0 Then Exit Sub

        Dim Row As Integer : Row = DataGridPedidos.CurrentRowIndex
        Dim Col As Integer : Col = DataGridPedidos.CurrentCell.ColumnNumber

        If Col <> 0 Then
            ' No hacer nada para que se muestre el tooltip.
        Else

            ' Ir al pedido.

            FormaCargada = False
            TimerReload.Enabled = False
            TimerReload.Interval = 500

            FormDetallePedidoPacking.CodLote = CStr(DataGridPedidos.Item(Row, 3))
            FormDetallePedidoPacking.CodPedido = CStr(DataGridPedidos.Item(Row, 0))
            FormDetallePedidoPacking.Show()
            Me.Visible = False

        End If

    End Sub

    Private Sub FormPackingUserMain_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Activated

        If Not FormaCargada Then

            FormaCargada = True

            lblUsuarioEnSesion.Text = "Usuario: " + descripcionUsuario

            TimerReload.Enabled = True

        End If

    End Sub

    Private Sub FormPackingUserMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        AjustarFormularios(Me)
        AjustarFormularios(Me.PanelInventario_Inv_Activo)

    End Sub

    Private Sub cmbSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSalir.Click
        salirProcesoModLog(Me)
        If SalioDelProceso Then
            FormaCargada = False
            DataGridPedidos.DataSource = Nothing
            TimerReload.Interval = 500
            TimerReload.Enabled = False
        End If
    End Sub

    Private Sub TimerReload_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TimerReload.Tick

        TimerReload.Enabled = False

        If TimerReload.Interval = 500 Then ' Iniciando. Despues colocarlo cada 3 minutos.
            TimerReload.Interval = 180000
        End If

        lblGridTooltip.Text = "Cargando..."

        CargarDataGridPackingUser(CStr(datasetUsuario.Tables(0).Rows(0)("codusuario")), Me)

        'DataGridPedidos.TableStyles(0).GridColumnStyles(3).Width = -1
        'DataGridPedidos.Refresh()

        lblGridTooltip.Text = "Lista Actualizada."

        If DataGridPedidos.VisibleRowCount <= 0 Then _
        lblGridTooltip.Text = "El usuario no tiene productos pendientes por empacar."

        TimerReload.Enabled = True

    End Sub

    Private Sub DataGridPedidos_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles DataGridPedidos.MouseMove

        If DataGridPedidos.VisibleRowCount > 0 Then

            Dim HoverRow As Integer : HoverRow = DataGridPedidos.HitTest(e.X, e.Y).Row

            If HoverRow >= 0 Then

                lblGridTooltip.Text = CStr(DataGridPedidos.Item(HoverRow, 1)) & vbNewLine & _
                "Cant. Articulos: " & CStr(DataGridPedidos.Item(HoverRow, 2))

                'DataGridPedidos.TableStyles(0).GridColumnStyles(3).HeaderText = String.Empty
                'DataGridPedidos.TableStyles(0).GridColumnStyles(3).Width = 0
                'DataGridPedidos.TableStyles(0).GridColumnStyles.Remove(DataGridPedidos.TableStyles(0).GridColumnStyles(3))
                'DataGridPedidos.Refresh()

            End If

        End If

    End Sub

    Private Sub cmbActualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbActualizar.Click
        TimerReload_Tick(TimerReload, Nothing)
    End Sub

End Class