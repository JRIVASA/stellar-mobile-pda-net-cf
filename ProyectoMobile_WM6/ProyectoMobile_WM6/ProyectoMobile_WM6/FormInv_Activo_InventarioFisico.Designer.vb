﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class FormInv_Activo_InventarioFisico
    Inherits System.Windows.Forms.Form

    'Form invalida a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer
    Private mainMenu1 As System.Windows.Forms.MainMenu

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar con el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormInv_Activo_InventarioFisico))
        Me.mainMenu1 = New System.Windows.Forms.MainMenu
        Me.LblINV_ACTIVO = New System.Windows.Forms.Label
        Me.PanelInventario_Inv_Activo = New System.Windows.Forms.Panel
        Me.DataGridINVENTARIO_INV_ACTIVO = New System.Windows.Forms.DataGrid
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.picBarraMorada = New System.Windows.Forms.PictureBox
        Me.lblUsuarioEnSesion = New System.Windows.Forms.Label
        Me.cmbSalir = New System.Windows.Forms.PictureBox
        Me.PictureFondoINVENTARIO_INV_ACTIVO = New System.Windows.Forms.PictureBox
        Me.PanelInventario_Inv_Activo.SuspendLayout()
        Me.SuspendLayout()
        '
        'LblINV_ACTIVO
        '
        Me.LblINV_ACTIVO.BackColor = System.Drawing.Color.Gainsboro
        Me.LblINV_ACTIVO.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.LblINV_ACTIVO.Location = New System.Drawing.Point(28, 67)
        Me.LblINV_ACTIVO.Name = "LblINV_ACTIVO"
        Me.LblINV_ACTIVO.Size = New System.Drawing.Size(187, 26)
        Me.LblINV_ACTIVO.Text = "Inventarios Activos"
        Me.LblINV_ACTIVO.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'PanelInventario_Inv_Activo
        '
        Me.PanelInventario_Inv_Activo.BackColor = System.Drawing.Color.White
        Me.PanelInventario_Inv_Activo.Controls.Add(Me.LblINV_ACTIVO)
        Me.PanelInventario_Inv_Activo.Controls.Add(Me.DataGridINVENTARIO_INV_ACTIVO)
        Me.PanelInventario_Inv_Activo.Controls.Add(Me.PictureBox1)
        Me.PanelInventario_Inv_Activo.Controls.Add(Me.picBarraMorada)
        Me.PanelInventario_Inv_Activo.Controls.Add(Me.lblUsuarioEnSesion)
        Me.PanelInventario_Inv_Activo.Controls.Add(Me.cmbSalir)
        Me.PanelInventario_Inv_Activo.Controls.Add(Me.PictureFondoINVENTARIO_INV_ACTIVO)
        Me.PanelInventario_Inv_Activo.Location = New System.Drawing.Point(0, 0)
        Me.PanelInventario_Inv_Activo.Name = "PanelInventario_Inv_Activo"
        Me.PanelInventario_Inv_Activo.Size = New System.Drawing.Size(240, 300)
        '
        'DataGridINVENTARIO_INV_ACTIVO
        '
        Me.DataGridINVENTARIO_INV_ACTIVO.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.DataGridINVENTARIO_INV_ACTIVO.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold)
        Me.DataGridINVENTARIO_INV_ACTIVO.HeaderForeColor = System.Drawing.Color.Black
        Me.DataGridINVENTARIO_INV_ACTIVO.Location = New System.Drawing.Point(7, 102)
        Me.DataGridINVENTARIO_INV_ACTIVO.Name = "DataGridINVENTARIO_INV_ACTIVO"
        Me.DataGridINVENTARIO_INV_ACTIVO.RowHeadersVisible = False
        Me.DataGridINVENTARIO_INV_ACTIVO.Size = New System.Drawing.Size(225, 152)
        Me.DataGridINVENTARIO_INV_ACTIVO.TabIndex = 0
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Gainsboro
        Me.PictureBox1.Location = New System.Drawing.Point(0, 56)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(240, 205)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'picBarraMorada
        '
        Me.picBarraMorada.Image = CType(resources.GetObject("picBarraMorada.Image"), System.Drawing.Image)
        Me.picBarraMorada.Location = New System.Drawing.Point(0, 260)
        Me.picBarraMorada.Name = "picBarraMorada"
        Me.picBarraMorada.Size = New System.Drawing.Size(240, 40)
        Me.picBarraMorada.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'lblUsuarioEnSesion
        '
        Me.lblUsuarioEnSesion.BackColor = System.Drawing.Color.DarkGray
        Me.lblUsuarioEnSesion.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblUsuarioEnSesion.Location = New System.Drawing.Point(0, 39)
        Me.lblUsuarioEnSesion.Name = "lblUsuarioEnSesion"
        Me.lblUsuarioEnSesion.Size = New System.Drawing.Size(240, 16)
        Me.lblUsuarioEnSesion.Text = "Usuario:"
        '
        'cmbSalir
        '
        Me.cmbSalir.Image = CType(resources.GetObject("cmbSalir.Image"), System.Drawing.Image)
        Me.cmbSalir.Location = New System.Drawing.Point(0, 0)
        Me.cmbSalir.Name = "cmbSalir"
        Me.cmbSalir.Size = New System.Drawing.Size(41, 38)
        Me.cmbSalir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'PictureFondoINVENTARIO_INV_ACTIVO
        '
        Me.PictureFondoINVENTARIO_INV_ACTIVO.Image = CType(resources.GetObject("PictureFondoINVENTARIO_INV_ACTIVO.Image"), System.Drawing.Image)
        Me.PictureFondoINVENTARIO_INV_ACTIVO.Location = New System.Drawing.Point(42, 0)
        Me.PictureFondoINVENTARIO_INV_ACTIVO.Name = "PictureFondoINVENTARIO_INV_ACTIVO"
        Me.PictureFondoINVENTARIO_INV_ACTIVO.Size = New System.Drawing.Size(198, 38)
        Me.PictureFondoINVENTARIO_INV_ACTIVO.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'FormInv_Activo_InventarioFisico
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(240, 300)
        Me.ControlBox = False
        Me.Controls.Add(Me.PanelInventario_Inv_Activo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormInv_Activo_InventarioFisico"
        Me.Text = "isMOBILE - Inventarios Activos"
        Me.PanelInventario_Inv_Activo.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LblINV_ACTIVO As System.Windows.Forms.Label
    Friend WithEvents PanelInventario_Inv_Activo As System.Windows.Forms.Panel
    Friend WithEvents DataGridINVENTARIO_INV_ACTIVO As System.Windows.Forms.DataGrid
    Friend WithEvents PictureFondoINVENTARIO_INV_ACTIVO As System.Windows.Forms.PictureBox
    Friend WithEvents lblUsuarioEnSesion As System.Windows.Forms.Label
    Friend WithEvents cmbSalir As System.Windows.Forms.PictureBox
    Friend WithEvents picBarraMorada As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
End Class
