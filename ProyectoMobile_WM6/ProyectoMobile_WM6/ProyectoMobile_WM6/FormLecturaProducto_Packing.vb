﻿Public Class FormLecturaProducto_Packing

    Public FormaCargada As Boolean ' No se por que ocurre el evento Activate al salir... es ilógico. Pero bueno... lo resolveré desde afuera.

    Public CodLote As String
    Public CodPedido As String
    Public CantDecimalesProducto As Integer

    Private Sub FormLecturaProducto_Packing_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Activated

        'If DebugMode Then MsgBox("Ctl:" & Me.Controls.Count.ToString)
        'If DebugMode Then MsgBox("Timer:" & (TimerReload Is Nothing).ToString)

        If Me.Controls.Count > 0 Then

            TimerReload.Interval = 100
            TimerReload.Enabled = False
            TimerReload.Enabled = True

        End If

    End Sub

    Private Sub ColorAlertaFinalizar()

        If Not IsNumeric(txtCantSolicitada.Text) Then Exit Sub
        If Not IsNumeric(txtCantRecolectada.Text) Then Exit Sub
        If Not IsNumeric(txtCantEmpacada.Text) Then Exit Sub

        If CDbl(txtCantEmpacada.Text) < CDbl(txtCantRecolectada.Text) Then
            cmbAgregar.BackColor = Color.Chocolate
        Else
            cmbAgregar.BackColor = Color.LightGreen
        End If

    End Sub

    Private Sub FormLecturaProducto_Packing_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        AjustarFormularios(Me)
    End Sub

    Private Sub cmbAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbAgregar.Click

        If Not IsNumeric(txtCantSolicitada.Text) Then Exit Sub
        If Not IsNumeric(txtCantRecolectada.Text) Then Exit Sub

        If IsNumeric(txtCantEmpacada.Text) Then

            Dim NewCant As Double, CantSolicitada As Double, _
            CantRecolectada As Double, CantEmpacada As Double

            CantSolicitada = CDbl(txtCantSolicitada.Text)

            CantRecolectada = CDbl(txtCantRecolectada.Text)

            If Not IsNumeric(txtCantEmpacada.Tag) Then txtCantEmpacada.Tag = "0"

            CantEmpacada = CDbl(txtCantEmpacada.Tag)

            NewCant = CDbl(txtCantEmpacada.Text)

            NewCant = Math.Round(NewCant, CantDecimalesProducto)

            If (CantRecolectada = CantEmpacada) Then
                Application.DoEvents()
                Dim Resp As MsgBoxResult = MsgBox("Ha empacado anteriormente la cantidad recolectada para este producto ¿Está seguro de que desea cambiarla?", MsgBoxStyle.OkCancel)
                Application.DoEvents()
                If Resp = MsgBoxResult.Cancel Then Exit Sub
            End If

            If NewCant >= 0 Then

                If (CantEmpacada > NewCant) Then

                    Dim Resp As MsgBoxResult = MsgBoxResult.Ok

                    If Not (CantRecolectada = CantEmpacada) Then
                        Resp = MsgBox("La cantidad actual de productos empacados es de " & _
                        CantEmpacada & " ¿Seguro que desea disminuirla a " & NewCant & "?", MsgBoxStyle.OkCancel)
                    End If

                    If Resp = MsgBoxResult.Ok Then
                        If GuardarPackingUser(CStr(datasetUsuario.Tables(0).Rows(0)("codusuario")), _
                        CodLote, CodPedido, lblDescripcion.Tag, CantDecimalesProducto, _
                        CantSolicitada, CantRecolectada, NewCant) Then
                            txtCantEmpacada.Tag = txtCantEmpacada.Text
                            cmbSalir_Click(Nothing, Nothing)
                            Exit Sub
                        End If
                    End If

                ElseIf CantRecolectada = NewCant Then

                    If GuardarPackingUser(CStr(datasetUsuario.Tables(0).Rows(0)("codusuario")), _
                    CodLote, CodPedido, lblDescripcion.Tag, CantDecimalesProducto, _
                    CantSolicitada, CantRecolectada, NewCant) Then
                        txtCantEmpacada.Tag = txtCantEmpacada.Text
                        cmbSalir_Click(Nothing, Nothing)
                        Exit Sub
                    End If

                ElseIf CantEmpacada <= NewCant Then

                    If (NewCant > CantRecolectada) Then
                        MsgBox("La cantidad empacada no puede ser mayor a la cantidad recolectada.")
                    Else
                        If GuardarPackingUser(CStr(datasetUsuario.Tables(0).Rows(0)("codusuario")), _
                        CodLote, CodPedido, lblDescripcion.Tag, CantDecimalesProducto, _
                        CantSolicitada, CantRecolectada, NewCant) Then
                            cmbSalir_Click(Nothing, Nothing)
                            Exit Sub
                        End If
                    End If

                End If

            End If

        Else
            MsgBox("Entrada inválida en la Cantidad a Empacar. Por favor verifique que sea un número")
        End If

    End Sub

    Private Sub cmbSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSalir.Click
        FormaCargada = False
        FormDetallePedidoPacking.Show()
        Me.Visible = False
    End Sub

    Private Sub txtCantEmpacada_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCantEmpacada.GotFocus

        txtCantEmpacada.Enabled = False

        FormNumPadGeneral.ValorOriginal = txtCantEmpacada.Text
        Dim A = FormNumPadGeneral.ShowDialog()

        Dim NewCant = Math.Round(FormNumPadGeneral.ValorNumerico, CantDecimalesProducto)
        txtCantEmpacada.Text = FormatNumber(NewCant, CantDecimalesProducto)

        txtCantRecolectada.Focus()
        cmbAgregar.Focus()

        ColorAlertaFinalizar()

        txtCantEmpacada.Enabled = True

        Me.Activate()
        Me.BringToFront()

    End Sub

    Private Sub txtCantEmpacada_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCantEmpacada.KeyPress
        txtCantEmpacada_GotFocus(Nothing, Nothing)
    End Sub

    Private Sub CmbEmpTodo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmbEmpTodo.Click
        txtCantEmpacada.Text = txtCantRecolectada.Text
        ColorAlertaFinalizar()
    End Sub

    Private Sub Activation()

        If Not FormaCargada Then

            FormaCargada = True

            txtCantEmpacada.Tag = txtCantEmpacada.Text

            'If DebugMode Then MsgBox("Activando..." & vbNewLine & "txtCantRecolectada.Text:" & txtCantRecolectada.Text & vbNewLine & "txtCantRecolectada.Tag:" & txtCantRecolectada.Tag.ToString & vbNewLine & "txtCantSolicitada.Text:" & txtCantSolicitada.Text)

            ColorAlertaFinalizar()

        End If

    End Sub

    Private Sub TimerReload_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TimerReload.Tick
        TimerReload.Enabled = False
        Activation()
    End Sub

End Class