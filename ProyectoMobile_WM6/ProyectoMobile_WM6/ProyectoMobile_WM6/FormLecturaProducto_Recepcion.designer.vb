﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class FormLecturaProducto_Recepcion
    Inherits System.Windows.Forms.Form

    'Form invalida a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer
    Private mainMenu1 As System.Windows.Forms.MainMenu

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar con el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormLecturaProducto_Recepcion))
        Me.mainMenu1 = New System.Windows.Forms.MainMenu
        Me.PictureFondoBuscarProveedor = New System.Windows.Forms.PictureBox
        Me.txtCodigoProducto = New System.Windows.Forms.TextBox
        Me.lblCodigoProducto = New System.Windows.Forms.Label
        Me.lblDescripcion = New System.Windows.Forms.Label
        Me.lblRec = New System.Windows.Forms.Label
        Me.lblFecha = New System.Windows.Forms.Label
        Me.lblNºRec = New System.Windows.Forms.Label
        Me.lblFechaRecepcion = New System.Windows.Forms.Label
        Me.picBarraMorada = New System.Windows.Forms.PictureBox
        Me.cmbSiguiente = New System.Windows.Forms.Button
        Me.cmbLimpiar = New System.Windows.Forms.Button
        Me.cmbSalir = New System.Windows.Forms.PictureBox
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.lblUsuarioEnSesion = New System.Windows.Forms.Label
        Me.ProgressBar_FormLecturaProducto = New System.Windows.Forms.ProgressBar
        Me.lblProgressBar_FormLecturaProducto = New System.Windows.Forms.Label
        Me.cmbActualizar = New System.Windows.Forms.PictureBox
        Me.SuspendLayout()
        '
        'PictureFondoBuscarProveedor
        '
        Me.PictureFondoBuscarProveedor.Image = CType(resources.GetObject("PictureFondoBuscarProveedor.Image"), System.Drawing.Image)
        Me.PictureFondoBuscarProveedor.Location = New System.Drawing.Point(42, 0)
        Me.PictureFondoBuscarProveedor.Name = "PictureFondoBuscarProveedor"
        Me.PictureFondoBuscarProveedor.Size = New System.Drawing.Size(198, 38)
        Me.PictureFondoBuscarProveedor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'txtCodigoProducto
        '
        Me.txtCodigoProducto.BackColor = System.Drawing.Color.White
        Me.txtCodigoProducto.Font = New System.Drawing.Font("Verdana", 14.0!, System.Drawing.FontStyle.Bold)
        Me.txtCodigoProducto.Location = New System.Drawing.Point(27, 136)
        Me.txtCodigoProducto.MaxLength = 50
        Me.txtCodigoProducto.Name = "txtCodigoProducto"
        Me.txtCodigoProducto.Size = New System.Drawing.Size(186, 29)
        Me.txtCodigoProducto.TabIndex = 68
        '
        'lblCodigoProducto
        '
        Me.lblCodigoProducto.BackColor = System.Drawing.Color.Gainsboro
        Me.lblCodigoProducto.Font = New System.Drawing.Font("Verdana", 14.0!, System.Drawing.FontStyle.Bold)
        Me.lblCodigoProducto.Location = New System.Drawing.Point(27, 106)
        Me.lblCodigoProducto.Name = "lblCodigoProducto"
        Me.lblCodigoProducto.Size = New System.Drawing.Size(186, 25)
        Me.lblCodigoProducto.Text = "Codigo Producto"
        Me.lblCodigoProducto.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblDescripcion
        '
        Me.lblDescripcion.BackColor = System.Drawing.Color.Gainsboro
        Me.lblDescripcion.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Regular)
        Me.lblDescripcion.Location = New System.Drawing.Point(15, 168)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(209, 47)
        Me.lblDescripcion.Text = "Descripción:"
        '
        'lblRec
        '
        Me.lblRec.BackColor = System.Drawing.Color.Gainsboro
        Me.lblRec.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold)
        Me.lblRec.Location = New System.Drawing.Point(5, 61)
        Me.lblRec.Name = "lblRec"
        Me.lblRec.Size = New System.Drawing.Size(61, 23)
        Me.lblRec.Text = "Nº Rec.:"
        '
        'lblFecha
        '
        Me.lblFecha.BackColor = System.Drawing.Color.Gainsboro
        Me.lblFecha.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold)
        Me.lblFecha.Location = New System.Drawing.Point(5, 75)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(61, 23)
        Me.lblFecha.Text = "Fecha:"
        '
        'lblNºRec
        '
        Me.lblNºRec.BackColor = System.Drawing.Color.Gainsboro
        Me.lblNºRec.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold)
        Me.lblNºRec.Location = New System.Drawing.Point(61, 61)
        Me.lblNºRec.Name = "lblNºRec"
        Me.lblNºRec.Size = New System.Drawing.Size(132, 23)
        '
        'lblFechaRecepcion
        '
        Me.lblFechaRecepcion.BackColor = System.Drawing.Color.Gainsboro
        Me.lblFechaRecepcion.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold)
        Me.lblFechaRecepcion.Location = New System.Drawing.Point(61, 75)
        Me.lblFechaRecepcion.Name = "lblFechaRecepcion"
        Me.lblFechaRecepcion.Size = New System.Drawing.Size(132, 23)
        '
        'picBarraMorada
        '
        Me.picBarraMorada.Image = CType(resources.GetObject("picBarraMorada.Image"), System.Drawing.Image)
        Me.picBarraMorada.Location = New System.Drawing.Point(-2, 259)
        Me.picBarraMorada.Name = "picBarraMorada"
        Me.picBarraMorada.Size = New System.Drawing.Size(240, 40)
        Me.picBarraMorada.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'cmbSiguiente
        '
        Me.cmbSiguiente.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.cmbSiguiente.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.cmbSiguiente.ForeColor = System.Drawing.Color.Black
        Me.cmbSiguiente.Location = New System.Drawing.Point(146, 265)
        Me.cmbSiguiente.Name = "cmbSiguiente"
        Me.cmbSiguiente.Size = New System.Drawing.Size(78, 25)
        Me.cmbSiguiente.TabIndex = 84
        Me.cmbSiguiente.Text = "Siguiente"
        '
        'cmbLimpiar
        '
        Me.cmbLimpiar.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.cmbLimpiar.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.cmbLimpiar.ForeColor = System.Drawing.Color.Black
        Me.cmbLimpiar.Location = New System.Drawing.Point(15, 265)
        Me.cmbLimpiar.Name = "cmbLimpiar"
        Me.cmbLimpiar.Size = New System.Drawing.Size(78, 25)
        Me.cmbLimpiar.TabIndex = 85
        Me.cmbLimpiar.Text = "Limpiar"
        '
        'cmbSalir
        '
        Me.cmbSalir.Image = CType(resources.GetObject("cmbSalir.Image"), System.Drawing.Image)
        Me.cmbSalir.Location = New System.Drawing.Point(0, 0)
        Me.cmbSalir.Name = "cmbSalir"
        Me.cmbSalir.Size = New System.Drawing.Size(41, 38)
        Me.cmbSalir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Gainsboro
        Me.PictureBox1.Location = New System.Drawing.Point(-1, 55)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(240, 203)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'lblUsuarioEnSesion
        '
        Me.lblUsuarioEnSesion.BackColor = System.Drawing.Color.DarkGray
        Me.lblUsuarioEnSesion.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblUsuarioEnSesion.Location = New System.Drawing.Point(0, 39)
        Me.lblUsuarioEnSesion.Name = "lblUsuarioEnSesion"
        Me.lblUsuarioEnSesion.Size = New System.Drawing.Size(240, 15)
        Me.lblUsuarioEnSesion.Text = "Usuario:"
        '
        'ProgressBar_FormLecturaProducto
        '
        Me.ProgressBar_FormLecturaProducto.Location = New System.Drawing.Point(5, 235)
        Me.ProgressBar_FormLecturaProducto.Name = "ProgressBar_FormLecturaProducto"
        Me.ProgressBar_FormLecturaProducto.Size = New System.Drawing.Size(219, 15)
        Me.ProgressBar_FormLecturaProducto.Visible = False
        '
        'lblProgressBar_FormLecturaProducto
        '
        Me.lblProgressBar_FormLecturaProducto.BackColor = System.Drawing.Color.Gainsboro
        Me.lblProgressBar_FormLecturaProducto.Location = New System.Drawing.Point(5, 214)
        Me.lblProgressBar_FormLecturaProducto.Name = "lblProgressBar_FormLecturaProducto"
        Me.lblProgressBar_FormLecturaProducto.Size = New System.Drawing.Size(219, 19)
        Me.lblProgressBar_FormLecturaProducto.Text = "Buscando Información del Producto..."
        Me.lblProgressBar_FormLecturaProducto.Visible = False
        '
        'cmbActualizar
        '
        Me.cmbActualizar.Image = CType(resources.GetObject("cmbActualizar.Image"), System.Drawing.Image)
        Me.cmbActualizar.Location = New System.Drawing.Point(106, 265)
        Me.cmbActualizar.Name = "cmbActualizar"
        Me.cmbActualizar.Size = New System.Drawing.Size(24, 24)
        Me.cmbActualizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'FormLecturaProducto_Recepcion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(240, 300)
        Me.ControlBox = False
        Me.Controls.Add(Me.cmbActualizar)
        Me.Controls.Add(Me.ProgressBar_FormLecturaProducto)
        Me.Controls.Add(Me.lblProgressBar_FormLecturaProducto)
        Me.Controls.Add(Me.lblUsuarioEnSesion)
        Me.Controls.Add(Me.txtCodigoProducto)
        Me.Controls.Add(Me.lblCodigoProducto)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.lblFechaRecepcion)
        Me.Controls.Add(Me.lblNºRec)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.lblRec)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.cmbSalir)
        Me.Controls.Add(Me.cmbSiguiente)
        Me.Controls.Add(Me.cmbLimpiar)
        Me.Controls.Add(Me.picBarraMorada)
        Me.Controls.Add(Me.PictureFondoBuscarProveedor)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MinimizeBox = False
        Me.Name = "FormLecturaProducto_Recepcion"
        Me.Text = "isMOBILE - Recepción"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PictureFondoBuscarProveedor As System.Windows.Forms.PictureBox
    Friend WithEvents txtCodigoProducto As System.Windows.Forms.TextBox
    Friend WithEvents lblCodigoProducto As System.Windows.Forms.Label
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents lblRec As System.Windows.Forms.Label
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents lblNºRec As System.Windows.Forms.Label
    Friend WithEvents lblFechaRecepcion As System.Windows.Forms.Label
    Friend WithEvents picBarraMorada As System.Windows.Forms.PictureBox
    Friend WithEvents cmbSiguiente As System.Windows.Forms.Button
    Friend WithEvents cmbLimpiar As System.Windows.Forms.Button
    Friend WithEvents cmbSalir As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents lblUsuarioEnSesion As System.Windows.Forms.Label
    Friend WithEvents ProgressBar_FormLecturaProducto As System.Windows.Forms.ProgressBar
    Friend WithEvents lblProgressBar_FormLecturaProducto As System.Windows.Forms.Label
    Friend WithEvents cmbActualizar As System.Windows.Forms.PictureBox
End Class
