﻿Public Class Formbusqueda_productos

    Dim filaDataGrid As Integer
    Public Tipo_Impresion As String

    Private Sub cmbBuscarProducto_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbBuscarProducto.Click
        BusquedaProducto()
    End Sub

    Private Sub cmbSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSalir.Click

        txtproductos.Text = ""
        DataGridbusqueda_productos.DataSource = Nothing

        Select Case (Tipo_Impresion)
            Case Is = "Etiqueta"
                Form_Impresion_Etiquetas.Show()
            Case Is = "Hablador"
                Form_Impresion_Hablador.Show()
            Case Else
        End Select

        Me.Visible = False

    End Sub

    Private Sub txtproductos_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtproductos.KeyDown
        If e.KeyValue = 13 Then
            BusquedaProducto()
        End If
    End Sub

    Private Sub BusquedaProducto()
        If txtproductos.Text = "" Then
            MsgBox("Debe colocar al menos un carácter en Descripción", MsgBoxStyle.Information, "isMOBILE")
            txtproductos.Focus()
        Else
            llenaDataGridBuscarProductos(txtproductos.Text)
        End If
    End Sub

    Private Sub ImprimirDatosProductos()

        SeleccionarProductos_datagrid()

        Select Case (Tipo_Impresion)
            Case Is = "Etiqueta"
                If Form_Impresion_Etiquetas.Datagrid_impresion.VisibleRowCount > 0 Then
                    Form_Impresion_Etiquetas.cmbImprimir.Visible = True
                    Form_Impresion_Etiquetas.cmbImprimir.Enabled = True
                End If
            Case Is = "Hablador"
                If Form_Impresion_Hablador.Datagrid_Impresion.VisibleRowCount > 0 Then
                    Form_Impresion_Hablador.cmbImprimir_hablador.Visible = True
                    Form_Impresion_Hablador.cmbImprimir_hablador.Enabled = True
                End If
        End Select

    End Sub

    Private Sub cmbAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbAceptar.Click
        ImprimirDatosProductos()
        ImpHblEti_BusquedaActiva = False
    End Sub

    Private Sub DataGridbusqueda_productos_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridbusqueda_productos.DoubleClick
        ImprimirDatosProductos()
        ImpHblEti_BusquedaActiva = False
    End Sub

    Private Sub Formbusqueda_productos_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        lblUsuarioEnSesion.Text = "Usuario: " + descripcionUsuario

        Me.Refresh()
        Me.Update()

        AjustarFormularios(Me)

    End Sub

    Private Sub Formbusqueda_productos_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated

        TimerReload.Interval = 100
        TimerReload.Enabled = True

    End Sub

    Private Sub DataGridbusqueda_productos_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles DataGridbusqueda_productos.KeyDown
        If e.KeyValue = Keys.Enter Then
            ImprimirDatosProductos()
            ImpHblEti_BusquedaActiva = False
        End If
    End Sub

    Private Sub Activation()

        txtproductos.Enabled = True
        txtproductos.Focus()
        txtproductos.SelectionStart = 0
        txtproductos.SelectionLength = txtproductos.Text.Length

    End Sub

    Private Sub TimerReload_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TimerReload.Tick
        TimerReload.Enabled = False
        Activation()
    End Sub

    Private Sub txtproductos_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtproductos.TextChanged

    End Sub

End Class