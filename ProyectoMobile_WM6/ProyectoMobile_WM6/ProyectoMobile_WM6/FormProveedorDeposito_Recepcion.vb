﻿Public Class FormProveedorDeposito_Recepcion

    Dim dataset_Codigos As Data.DataSet
    Dim dataset_Deposito As Data.DataSet

    Private Sub formProveedorDeposito_Recepcion_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        lblUsuarioEnSesion.Text = "Usuario: " + descripcionUsuario
        Me.Refresh()
        Me.Update()
        AjustarFormularios(Me)

        txtProveedor.Enabled = True
        txtProveedor.Focus()
        txtDeposito.Enabled = False
        cmbBuscarDeposito.Enabled = False
        cmbVerificarDeposito.Enabled = False

    End Sub

    Private Sub cmbBuscarProveedor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbBuscarProveedor.Click
        If (txtProveedor.Text <> "" And txtProveedor.Enabled = False) Then
            MsgBox("Ya seleccionó un proveedor, presione Cancelar para ingresar un código de proveedor diferente o para realizar una nueva búsqueda", MsgBoxStyle.Information, "isMOBILE")
        Else
            FormBuscarProveedor_Recepcion.Show()
            Me.Visible = False
            FormBuscarProveedor_Recepcion.txtDescripcionProveedor.Focus()
        End If
    End Sub

    Private Sub cmbVerificarProveedor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbVerificarProveedor.Click
        If (txtProveedor.Text <> "" And txtProveedor.Enabled = False) Then
            MsgBox("Ya seleccionó un proveedor, presione Cancelar para ingresar un código de proveedor diferente o para realizar una nueva búsqueda", MsgBoxStyle.Information, "isMOBILE")
        Else
            buscarProveedorporCodigo(txtProveedor.Text)
        End If
    End Sub

    Private Sub cmbCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbCancelar.Click
        If Not (txtProveedor.Text = "") Then
            respuesta = MsgBox("¿Está seguro que desea cancelar?", MsgBoxStyle.YesNo, "Cancelar")
            If respuesta = "6" Then
                txtProveedor.Text = ""
                lblDescripcionProveedor.Text = ""
                txtProveedor.Enabled = True
                cmbBuscarProveedor.Enabled = True
                txtDeposito.Text = ""
                lblDescripcionDeposito.Text = ""
                txtDeposito.Enabled = False
                cmbBuscarDeposito.Enabled = False
                cmbVerificarDeposito.Enabled = False
                txtProveedor.Focus()
            End If
        Else
            If Not (txtDeposito.Text = "") Then
                respuesta = MsgBox("¿Está Seguro que desea cancelar?", MsgBoxStyle.YesNo, "Cancelar")
                If respuesta = "6" Then
                    txtProveedor.Text = ""
                    lblDescripcionProveedor.Text = ""
                    txtProveedor.Enabled = True
                    cmbBuscarProveedor.Enabled = True
                    txtDeposito.Text = ""
                    lblDescripcionDeposito.Text = ""
                    txtDeposito.Enabled = False
                    cmbBuscarDeposito.Enabled = False
                    cmbVerificarDeposito.Enabled = False
                    txtProveedor.Focus()
                End If
            Else
                txtProveedor.Text = ""
                lblDescripcionProveedor.Text = ""
                txtProveedor.Enabled = True
                cmbBuscarProveedor.Enabled = True
                txtDeposito.Text = ""
                lblDescripcionDeposito.Text = ""
                txtDeposito.Enabled = False
                cmbBuscarDeposito.Enabled = False
                cmbVerificarDeposito.Enabled = False
                txtProveedor.Focus()
            End If
        End If
    End Sub

    Private Sub txtProveedor_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtProveedor.KeyDown
        If e.KeyValue = 13 Then
            buscarProveedorporCodigo(txtProveedor.Text)
        End If
    End Sub

    Private Sub cmbVerificarDeposito_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbVerificarDeposito.Click
        VerificarDeposito()
    End Sub

    Private Sub txtDeposito_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDeposito.KeyDown
        If e.KeyValue = 13 Then
            VerificarDeposito()
        End If
    End Sub

    Private Sub VerificarDeposito()

        'Cuando el txtDeposito no esta vacio, se verifica que el deposito existe
        If Not (txtDeposito.Text = "") Then
            If txtDeposito.TextLength <= 15 Then
                dataset_Deposito = buscarDepositoporCodigo(txtDeposito.Text)
                'Se verifica que la consulta trajo resultados o no
                If dataset_Deposito.Tables(0).Rows.Count() = 0 Then
                    MsgBox("El código de depósito no existe", MsgBoxStyle.Information, "isMOBILE")
                    txtDeposito.Text = ""
                    lblDescripcionDeposito.Text = "Descripción: "
                    txtDeposito.Focus()
                Else
                    lblDescripcionDeposito.Text = "Descripción: " + dataset_Deposito.Tables(0).Rows(0)("c_descripcion")
                    cmbSiguiente.Focus()
                End If
            Else
                MsgBox("Código inválido.", MsgBoxStyle.Information, "isMOBILE")
                txtDeposito.Text = ""
                txtDeposito.Focus()
            End If
        Else
            MsgBox("Debe ingresar el código del depósito o presionar el botón signo de interrogación para realizar la búsqueda.", MsgBoxStyle.Information, "isMOBILE")
            txtDeposito.Focus()
        End If

    End Sub

    Private Sub cmbBuscarDeposito_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbBuscarDeposito.Click
        FormBuscarDeposito.Owner = Me
        FormBuscarDeposito.txtDescripcionDeposito.Focus()
        FormBuscarDeposito.ShowDialog()
    End Sub

    Private Sub cmbSiguiente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSiguiente.Click

        If txtProveedor.Text = "" Then
            MsgBox("Debe ingresar el Proveedor.", MsgBoxStyle.Information, "isMOBILE")
            txtProveedor.Focus()
        Else
            If txtDeposito.Text = "" Then
                MsgBox("Debe ingresar el Depósito.", MsgBoxStyle.Information, "isMOBILE")
                txtDeposito.Focus()
            Else
                If lblDescripcionDeposito.Text = "Descripción:" Then
                    MsgBox("Debe verificar que el Depósito existe.", MsgBoxStyle.Information, "isMOBILE")
                    cmbVerificarDeposito.Focus()
                Else
                    FormDocumentoTransportista_Recepcion.Show()
                    FormDocumentoTransportista_Recepcion.txtDocumento.Focus()
                    Me.Visible = False
                End If
            End If
        End If

    End Sub

    Private Sub cmbSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSalir.Click
        If Not (txtProveedor.Text = "") Then
            respuesta = MsgBox("¿Está seguro que desea salir?", MsgBoxStyle.YesNo, "Salir")
            If respuesta = "6" Then
                txtProveedor.Text = ""
                lblDescripcionProveedor.Text = ""
                txtProveedor.Enabled = True
                cmbBuscarProveedor.Enabled = True
                txtDeposito.Text = ""
                lblDescripcionDeposito.Text = ""
                txtDeposito.Enabled = False
                cmbBuscarDeposito.Enabled = False
                cmbVerificarDeposito.Enabled = False
                'FormMenuRecepcion.Show()
                'formMenuPrincipal.Show()
                FormMenuModLog_1.Show()
                Me.Visible = False
            End If
        Else
            If Not (txtDeposito.Text = "") Then
                respuesta = MsgBox("¿Está seguro que desea salir?", MsgBoxStyle.YesNo, "Salir")
                If respuesta = "6" Then
                    txtProveedor.Text = ""
                    lblDescripcionProveedor.Text = ""
                    txtProveedor.Enabled = True
                    cmbBuscarProveedor.Enabled = True
                    txtDeposito.Text = ""
                    lblDescripcionDeposito.Text = ""
                    txtDeposito.Enabled = False
                    cmbBuscarDeposito.Enabled = False
                    cmbVerificarDeposito.Enabled = False
                    'FormMenuRecepcion.Show()
                    'formMenuPrincipal.Show()
                    FormMenuModLog_1.Show()
                    Me.Visible = False
                End If
            Else
                txtProveedor.Text = ""
                lblDescripcionProveedor.Text = ""
                txtProveedor.Enabled = True
                cmbBuscarProveedor.Enabled = True
                txtDeposito.Text = ""
                lblDescripcionDeposito.Text = ""
                txtDeposito.Enabled = False
                cmbBuscarDeposito.Enabled = False
                cmbVerificarDeposito.Enabled = False
                'FormMenuRecepcion.Show()
                'formMenuPrincipal.Show()
                FormMenuModLog_1.Show()
                Me.Visible = False
            End If
        End If

    End Sub

    Private Sub txtDeposito_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDeposito.TextChanged

    End Sub

End Class

