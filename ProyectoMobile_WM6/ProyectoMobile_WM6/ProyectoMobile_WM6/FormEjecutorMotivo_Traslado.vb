﻿Public Class FormEjecutorMotivo_Traslado
    Dim dataset_ConceptosTraslado As Data.DataSet
    Dim codigoConcepto As String
    Dim motivo As String
    Dim stringArregloConceptos As String

    Private Sub FormEjecutorMotivo_Traslado_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        lblUsuarioEnSesion.Text = "Usuario: " + descripcionUsuario
        Me.Refresh()
        Me.Update()

        AjustarFormularios(Me)

        If conectarDB_SERVIDOR(rutaDB_SERVIDOR) = 1 Then

            dataset_ConceptosTraslado = buscarConceptos("TRASLADOS")

            If Not dataset_ConceptosTraslado.Tables(0).Rows.Count = 0 Then
                cboConceptosTraslados.DataSource = dataset_ConceptosTraslado.Tables(0)
                cboConceptosTraslados.DisplayMember = dataset_ConceptosTraslado.Tables(0).Columns(1).Caption.ToString()
                cboConceptosTraslados.ValueMember = dataset_ConceptosTraslado.Tables(0).Columns(0).Caption.ToString()

                codigoConcepto = cboConceptosTraslados.ValueMember
                motivo = cboConceptosTraslados.DisplayMember
            Else
                codigoConcepto = ""
                MsgBox("Verifique la configuración de los conceptos para el proceso de Traslados.", MsgBoxStyle.Information, "Conceptos de Traslados")
            End If

            cerrarConexion_SERVIDOR()

        Else

            If CompatibilidadFueraDeLinea Then

                'Voy a hacer la tabla MA_CONCEPTOS y MA_PROCESOS en el PDA, para que permita cargar los conceptos si no hay conexión con el servidor!
                'MsgBox("Verifique conexión con el servidor, no se pueden consultar los conceptos", MsgBoxStyle.Information, "Conceptos Traslados")

                dataset_ConceptosTraslado = buscarConceptos_PDA("TRASLADOS")

                If Not dataset_ConceptosTraslado.Tables(0).Rows.Count = 0 Then
                    cboConceptosTraslados.DataSource = dataset_ConceptosTraslado.Tables(0)
                    cboConceptosTraslados.DisplayMember = dataset_ConceptosTraslado.Tables(0).Columns(1).Caption.ToString()
                    cboConceptosTraslados.ValueMember = dataset_ConceptosTraslado.Tables(0).Columns(0).Caption.ToString()
                Else
                    codigoConcepto = ""
                    MsgBox("Verifique la configuración de los conceptos para el proceso de Traslados.", MsgBoxStyle.Information, "Conceptos de Traslados")
                End If

            Else

                MsgBox("No hay conexión al servidor. No se pudieron recuperar los conceptos de traslado. Por favor intente mas tarde.", MsgBoxStyle.Information, "isMOBILE")

            End If

        End If

    End Sub
    
    Private Sub cmbSiguiente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSiguiente.Click
        If txtEjecutor.Text = "" Then
            MsgBox("Debe especificar el responsable del traslado.", MsgBoxStyle.Information, "Ingresar Ejecutor")
            txtEjecutor.Focus()
        Else
            If cboConceptosTraslados.DisplayMember = "" Then
                MsgBox("Debe seleccionar el motivo del traslado.", MsgBoxStyle.Information, "Motivo de Traslado")
                cboConceptosTraslados.Focus()
            Else
                FormLecturaProducto_Traslado.Show()
                Me.Visible = False
            End If
        End If
    End Sub

    Private Sub cmbSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSalir.Click
        txtEjecutor.Text = ""
        FormDepositoOrigenDestino_Traslado.Show()
        Me.Visible = False
    End Sub

    Private Sub txtEjecutor_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtEjecutor.KeyDown
        If e.KeyCode = Keys.Enter Then
            cboConceptosTraslados.Focus()
        End If
    End Sub

    Private Sub cboConceptosTraslados_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cboConceptosTraslados.KeyDown
        If e.KeyCode = Keys.Enter Then
            cmbSiguiente.Focus()
        End If
    End Sub

End Class