﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class FormGuardarDatos_Recepcion
    Inherits System.Windows.Forms.Form

    'Form invalida a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer
    Private mainMenu1 As System.Windows.Forms.MainMenu

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar con el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormGuardarDatos_Recepcion))
        Me.mainMenu1 = New System.Windows.Forms.MainMenu
        Me.PictureFondo = New System.Windows.Forms.PictureBox
        Me.lblDatosaGuardar = New System.Windows.Forms.Label
        Me.lblUnidadesxEmpaque = New System.Windows.Forms.Label
        Me.lblDescripcionProducto = New System.Windows.Forms.Label
        Me.lblEmpaquesRecibidos = New System.Windows.Forms.Label
        Me.lblUnidadesRecibidas = New System.Windows.Forms.Label
        Me.lblEmpaquesFacturados = New System.Windows.Forms.Label
        Me.lblUnidadesFacturadas = New System.Windows.Forms.Label
        Me.lblCantidadEmpaquesRecibidos = New System.Windows.Forms.Label
        Me.lblCantidadUnidadesRecibidas = New System.Windows.Forms.Label
        Me.lblCantidadEmpaquesFacturados = New System.Windows.Forms.Label
        Me.lblCantidadUnidadesFacturadas = New System.Windows.Forms.Label
        Me.picBarraMorada = New System.Windows.Forms.PictureBox
        Me.cmbSalir = New System.Windows.Forms.PictureBox
        Me.cmbGuardar = New System.Windows.Forms.Button
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.lblUsuarioEnSesion = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'PictureFondo
        '
        Me.PictureFondo.Image = CType(resources.GetObject("PictureFondo.Image"), System.Drawing.Image)
        Me.PictureFondo.Location = New System.Drawing.Point(42, 0)
        Me.PictureFondo.Name = "PictureFondo"
        Me.PictureFondo.Size = New System.Drawing.Size(198, 38)
        Me.PictureFondo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'lblDatosaGuardar
        '
        Me.lblDatosaGuardar.BackColor = System.Drawing.Color.Gainsboro
        Me.lblDatosaGuardar.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.lblDatosaGuardar.Location = New System.Drawing.Point(32, 58)
        Me.lblDatosaGuardar.Name = "lblDatosaGuardar"
        Me.lblDatosaGuardar.Size = New System.Drawing.Size(177, 40)
        Me.lblDatosaGuardar.Text = "Confirme Datos a Guardar"
        Me.lblDatosaGuardar.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblUnidadesxEmpaque
        '
        Me.lblUnidadesxEmpaque.BackColor = System.Drawing.Color.Gainsboro
        Me.lblUnidadesxEmpaque.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblUnidadesxEmpaque.Location = New System.Drawing.Point(40, 139)
        Me.lblUnidadesxEmpaque.Name = "lblUnidadesxEmpaque"
        Me.lblUnidadesxEmpaque.Size = New System.Drawing.Size(169, 23)
        Me.lblUnidadesxEmpaque.Text = "Uni. x Emp.:"
        '
        'lblDescripcionProducto
        '
        Me.lblDescripcionProducto.BackColor = System.Drawing.Color.Gainsboro
        Me.lblDescripcionProducto.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblDescripcionProducto.Location = New System.Drawing.Point(14, 107)
        Me.lblDescripcionProducto.Name = "lblDescripcionProducto"
        Me.lblDescripcionProducto.Size = New System.Drawing.Size(209, 30)
        Me.lblDescripcionProducto.Text = "Producto:"
        '
        'lblEmpaquesRecibidos
        '
        Me.lblEmpaquesRecibidos.BackColor = System.Drawing.Color.Gainsboro
        Me.lblEmpaquesRecibidos.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblEmpaquesRecibidos.Location = New System.Drawing.Point(6, 162)
        Me.lblEmpaquesRecibidos.Name = "lblEmpaquesRecibidos"
        Me.lblEmpaquesRecibidos.Size = New System.Drawing.Size(167, 23)
        Me.lblEmpaquesRecibidos.Text = "Empaques Recibidos"
        '
        'lblUnidadesRecibidas
        '
        Me.lblUnidadesRecibidas.BackColor = System.Drawing.Color.Gainsboro
        Me.lblUnidadesRecibidas.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblUnidadesRecibidas.Location = New System.Drawing.Point(6, 184)
        Me.lblUnidadesRecibidas.Name = "lblUnidadesRecibidas"
        Me.lblUnidadesRecibidas.Size = New System.Drawing.Size(167, 24)
        Me.lblUnidadesRecibidas.Text = "Unidades Recibidas"
        '
        'lblEmpaquesFacturados
        '
        Me.lblEmpaquesFacturados.BackColor = System.Drawing.Color.Gainsboro
        Me.lblEmpaquesFacturados.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblEmpaquesFacturados.Location = New System.Drawing.Point(6, 206)
        Me.lblEmpaquesFacturados.Name = "lblEmpaquesFacturados"
        Me.lblEmpaquesFacturados.Size = New System.Drawing.Size(179, 23)
        Me.lblEmpaquesFacturados.Text = "Empaques Facturados"
        '
        'lblUnidadesFacturadas
        '
        Me.lblUnidadesFacturadas.BackColor = System.Drawing.Color.Gainsboro
        Me.lblUnidadesFacturadas.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblUnidadesFacturadas.Location = New System.Drawing.Point(6, 229)
        Me.lblUnidadesFacturadas.Name = "lblUnidadesFacturadas"
        Me.lblUnidadesFacturadas.Size = New System.Drawing.Size(179, 22)
        Me.lblUnidadesFacturadas.Text = "Unidades Facturadas"
        '
        'lblCantidadEmpaquesRecibidos
        '
        Me.lblCantidadEmpaquesRecibidos.BackColor = System.Drawing.Color.Gainsboro
        Me.lblCantidadEmpaquesRecibidos.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblCantidadEmpaquesRecibidos.Location = New System.Drawing.Point(179, 162)
        Me.lblCantidadEmpaquesRecibidos.Name = "lblCantidadEmpaquesRecibidos"
        Me.lblCantidadEmpaquesRecibidos.Size = New System.Drawing.Size(54, 23)
        Me.lblCantidadEmpaquesRecibidos.Text = "0"
        Me.lblCantidadEmpaquesRecibidos.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblCantidadUnidadesRecibidas
        '
        Me.lblCantidadUnidadesRecibidas.BackColor = System.Drawing.Color.Gainsboro
        Me.lblCantidadUnidadesRecibidas.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblCantidadUnidadesRecibidas.Location = New System.Drawing.Point(179, 185)
        Me.lblCantidadUnidadesRecibidas.Name = "lblCantidadUnidadesRecibidas"
        Me.lblCantidadUnidadesRecibidas.Size = New System.Drawing.Size(54, 23)
        Me.lblCantidadUnidadesRecibidas.Text = "0"
        Me.lblCantidadUnidadesRecibidas.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblCantidadEmpaquesFacturados
        '
        Me.lblCantidadEmpaquesFacturados.BackColor = System.Drawing.Color.Gainsboro
        Me.lblCantidadEmpaquesFacturados.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblCantidadEmpaquesFacturados.Location = New System.Drawing.Point(179, 206)
        Me.lblCantidadEmpaquesFacturados.Name = "lblCantidadEmpaquesFacturados"
        Me.lblCantidadEmpaquesFacturados.Size = New System.Drawing.Size(54, 23)
        Me.lblCantidadEmpaquesFacturados.Text = "0"
        Me.lblCantidadEmpaquesFacturados.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblCantidadUnidadesFacturadas
        '
        Me.lblCantidadUnidadesFacturadas.BackColor = System.Drawing.Color.Gainsboro
        Me.lblCantidadUnidadesFacturadas.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblCantidadUnidadesFacturadas.Location = New System.Drawing.Point(179, 229)
        Me.lblCantidadUnidadesFacturadas.Name = "lblCantidadUnidadesFacturadas"
        Me.lblCantidadUnidadesFacturadas.Size = New System.Drawing.Size(54, 22)
        Me.lblCantidadUnidadesFacturadas.Text = "0"
        Me.lblCantidadUnidadesFacturadas.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'picBarraMorada
        '
        Me.picBarraMorada.Image = CType(resources.GetObject("picBarraMorada.Image"), System.Drawing.Image)
        Me.picBarraMorada.Location = New System.Drawing.Point(-1, 259)
        Me.picBarraMorada.Name = "picBarraMorada"
        Me.picBarraMorada.Size = New System.Drawing.Size(240, 40)
        Me.picBarraMorada.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'cmbSalir
        '
        Me.cmbSalir.Image = CType(resources.GetObject("cmbSalir.Image"), System.Drawing.Image)
        Me.cmbSalir.Location = New System.Drawing.Point(0, 0)
        Me.cmbSalir.Name = "cmbSalir"
        Me.cmbSalir.Size = New System.Drawing.Size(41, 38)
        Me.cmbSalir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'cmbGuardar
        '
        Me.cmbGuardar.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.cmbGuardar.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.cmbGuardar.ForeColor = System.Drawing.Color.Black
        Me.cmbGuardar.Location = New System.Drawing.Point(78, 265)
        Me.cmbGuardar.Name = "cmbGuardar"
        Me.cmbGuardar.Size = New System.Drawing.Size(78, 25)
        Me.cmbGuardar.TabIndex = 113
        Me.cmbGuardar.Text = "Guardar"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Gainsboro
        Me.PictureBox1.Location = New System.Drawing.Point(0, 55)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(240, 205)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'lblUsuarioEnSesion
        '
        Me.lblUsuarioEnSesion.BackColor = System.Drawing.Color.DarkGray
        Me.lblUsuarioEnSesion.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblUsuarioEnSesion.Location = New System.Drawing.Point(0, 38)
        Me.lblUsuarioEnSesion.Name = "lblUsuarioEnSesion"
        Me.lblUsuarioEnSesion.Size = New System.Drawing.Size(240, 15)
        Me.lblUsuarioEnSesion.Text = "Usuario:"
        '
        'FormGuardarDatos_Recepcion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(240, 300)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblUsuarioEnSesion)
        Me.Controls.Add(Me.lblDescripcionProducto)
        Me.Controls.Add(Me.lblCantidadUnidadesFacturadas)
        Me.Controls.Add(Me.lblCantidadEmpaquesFacturados)
        Me.Controls.Add(Me.lblCantidadUnidadesRecibidas)
        Me.Controls.Add(Me.lblCantidadEmpaquesRecibidos)
        Me.Controls.Add(Me.lblEmpaquesFacturados)
        Me.Controls.Add(Me.lblUnidadesFacturadas)
        Me.Controls.Add(Me.lblEmpaquesRecibidos)
        Me.Controls.Add(Me.lblUnidadesRecibidas)
        Me.Controls.Add(Me.lblUnidadesxEmpaque)
        Me.Controls.Add(Me.lblDatosaGuardar)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.cmbGuardar)
        Me.Controls.Add(Me.cmbSalir)
        Me.Controls.Add(Me.picBarraMorada)
        Me.Controls.Add(Me.PictureFondo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormGuardarDatos_Recepcion"
        Me.Text = "isMOBILE - Recepción"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PictureFondo As System.Windows.Forms.PictureBox
    Friend WithEvents lblDatosaGuardar As System.Windows.Forms.Label
    Friend WithEvents lblUnidadesxEmpaque As System.Windows.Forms.Label
    Friend WithEvents lblDescripcionProducto As System.Windows.Forms.Label
    Friend WithEvents lblEmpaquesRecibidos As System.Windows.Forms.Label
    Friend WithEvents lblUnidadesRecibidas As System.Windows.Forms.Label
    Friend WithEvents lblEmpaquesFacturados As System.Windows.Forms.Label
    Friend WithEvents lblUnidadesFacturadas As System.Windows.Forms.Label
    Friend WithEvents lblCantidadEmpaquesRecibidos As System.Windows.Forms.Label
    Friend WithEvents lblCantidadUnidadesRecibidas As System.Windows.Forms.Label
    Friend WithEvents lblCantidadEmpaquesFacturados As System.Windows.Forms.Label
    Friend WithEvents lblCantidadUnidadesFacturadas As System.Windows.Forms.Label
    Friend WithEvents picBarraMorada As System.Windows.Forms.PictureBox
    Friend WithEvents cmbSalir As System.Windows.Forms.PictureBox
    Friend WithEvents cmbGuardar As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents lblUsuarioEnSesion As System.Windows.Forms.Label
End Class
