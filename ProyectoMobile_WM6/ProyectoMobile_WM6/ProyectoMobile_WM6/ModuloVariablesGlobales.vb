﻿Imports System.Data.SqlServerCe

Module ModuloVariablesGlobales

    Public ApplicationExit As Boolean

    Public comandoBD_PDA As New SqlCeCommand

    Public dataset_ODC As New Data.DataSet
    'Public dataset_Codigos As New Data.DataSet
    Public dataset_Productos As New Data.DataSet
    Public dataset_MA_RECEPCIONESMOVIL_SERVIDOR As New Data.DataSet
    Public dataset_MA_RECEPCIONESMOVIL_PDA As New Data.DataSet
    Public dataset_RegladeNegocio As New Data.DataSet
    Public dataset_NivelUsuario As New Data.DataSet
    Public datasetUsuario As New Data.DataSet

    Public descripcionUsuario As String

    'Public rutaArchivoExe As String

    Public cantidadSolicitada As Double
    Public empaquesRecibidos As Integer
    Public cantidadEmpaquesPermitidos As Integer
    Public unidadesRecibidas As Double
    Public cantidadUnidadesPermitidas As Integer
    Public codigoODC As String
    Public Localidad As String
    Public respuesta As String
    Public conceptoRecepcion As String = "REC"
    Public factorCambio As Double
    Public numeroRecepcion_SERVIDOR As String
    Public SentenciaSQL As String
    Public Id_PDA As String
    'Public numeroRecepcion_PDA As String
    Public documento As String
    Public rutaDB_SERVIDOR As String
    Public descripcionProducto As String
    Public UnidadesxEmpaque As Integer
    Public cantidadFaltanteFormularios As Double
    Public cantidadRecibida As Double
    Public cantidadRecibidaNoProcesada As Double
    Public valorRegladeNegocio As String
    Public RegladeNegocio As String
    Public nivelUsuario As String
    Public verFormCantidadFacturada As Boolean
    Public verFormCantidadFacturadaSinODC As Boolean
    Public UnidadesFacturadasNoProcesadas_SERVIDOR As Double
    Public UnidadesFacturadasNoProcesadas_PDA As Double
    Public EmpaquesFacturadosNoProcesados_SERVIDOR As Double
    Public EmpaquesFacturadosNoProcesados_PDA As Double
    Public Deposito As String
    Public numeroTraslado_SERVIDOR As String
    Public numeroTraslado_SERVIDORsinFormato As Integer
    Public lineaTraslado As Integer

    Public Moneda_Cod As String
    Public Moneda_Des As String
    Public Moneda_Sim As String
    Public Moneda_Fac As Double
    Public Moneda_Dec As Integer

    Public TmpMoneda_Sim As String

    Public Hablador_MonedaAlternaParaPrecios As String
    Public Hablador_FactorizarPreciosMultiMoneda As Boolean

    Public mFueraLineaSetup As String
    Public CompatibilidadFueraDeLinea As Boolean
    Public Inicio_Conectado As Boolean
    Public TmpBDCheck As String

    'Recordar ajustes de pantalla

    Public Const MsjSinConexionGenerico = "No hay conexión con el servidor. " + _
    "La acción iniciada no pudo ser ejecutada. " + _
    "Intente de nuevo mas tarde."

    Public PantallaAjustada_FrmBuscarDeposito As Boolean

    Public Enum ConexionImpresora As Integer
        Impresora_WLAN
        Impresora_Bluetooth
    End Enum

    Public ImpresoraSeleccionada As ConexionImpresora = ConexionImpresora.Impresora_WLAN

    Public SalioDelProceso As Boolean

End Module
