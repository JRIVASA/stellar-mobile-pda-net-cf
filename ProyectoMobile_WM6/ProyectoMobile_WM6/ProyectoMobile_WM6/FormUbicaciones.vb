﻿Public Class FormUbicaciones

    Public CodigoProducto As String
    Private TmpCol As Integer, TmpRow As Integer

    Private Sub hideButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hideButton.Click
        FormConsultaProductos.Enabled = True
        Me.Hide()
    End Sub

    Private Sub FormUbicaciones_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Activated

        TimerReload.Interval = 100
        TimerReload.Enabled = True

    End Sub

    Private Sub FormUbicaciones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        AjustarFormularios(Me)
        TxtEdit.Visible = False
        TxtEdit.Text = vbNullString
        TxtEdit.BackColor = Me.GridUbicaciones.SelectionBackColor
        'TxtEdit.Font = Me.GridUbicaciones.Font
    End Sub

    Private Sub salirButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles salirButton.Click
        FormConsultaProductos.Enabled = True
        Me.Hide()
    End Sub

    Private Function GetRowLocation() As System.Drawing.Point

        Return Me.GridUbicaciones.GetCellBounds(Me.GridUbicaciones.CurrentCell.RowNumber, Me.GridUbicaciones.CurrentCell.ColumnNumber).Location()

        'With Me.GridUbicaciones.TableStyles(0).GridColumnStyles()

        '    Dim TmpX As Long, TmpY As Long

        '    For i As Integer = 0 To Me.GridUbicaciones.CurrentCell.ColumnNumber
        '        TmpX = TmpX + .Item(i).Width
        '    Next



        '    For i As Integer = 0 To Me.GridUbicaciones.CurrentCell.RowNumber
        '        TmpY = TmpY + .Item(i).Width
        '    Next

        '    Tmp = Tmp - .Item(Me.GridUbicaciones.CurrentCell.ColumnNumber).Width

        'End With

    End Function

    Private Sub PlaceEdit()

        With Me.GridUbicaciones.TableStyles(0).GridColumnStyles()

            TxtEdit.Location = GetRowLocation()
            TxtEdit.Location = New System.Drawing.Point(TxtEdit.Location.X, (TxtEdit.Location.Y + Me.GridUbicaciones.Location.Y))
            TxtEdit.Size = New System.Drawing.Size(.Item(Me.GridUbicaciones.CurrentCell.ColumnNumber).Width, Me.GridUbicaciones.PreferredRowHeight)

        End With

        TxtEdit.Text = Me.GridUbicaciones.Item(Me.GridUbicaciones.CurrentCell.RowNumber, Me.GridUbicaciones.CurrentCell.ColumnNumber).ToString
        TxtEdit.SelectionStart = 0
        TxtEdit.SelectionLength = Len(TxtEdit.Text)
        TxtEdit.Visible = True

    End Sub

    Private Sub GridUbicaciones_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridUbicaciones.DoubleClick

        'Dim t As String

        't = "ColWidth:"

        'For Each Itm As DataGridColumnStyle In Me.GridUbicaciones.TableStyles(0).GridColumnStyles
        '    t = t & vbNewLine & "(" & Itm.Width & ")"
        'Next

        'MsgBox(t)

        Select GridUbicaciones.CurrentCell.ColumnNumber
            Case 3
                PlaceEdit()
                If TxtEdit.Enabled And TxtEdit.Visible Then TxtEdit.Focus()
        End Select

    End Sub

    Private Sub GridUbicaciones_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridUbicaciones.GotFocus

        'Me.GridUbicaciones.TableStyles(0).GridColumnStyles(0).Width = 0
        'Me.GridUbicaciones.TableStyles(0).GridColumnStyles(1).Width = 0
        'Me.GridUbicaciones.TableStyles(0).GridColumnStyles(2).Width = 105
        'Me.GridUbicaciones.TableStyles(0).GridColumnStyles(3).Width = 51
        'Me.GridUbicaciones.TableStyles(0).GridColumnStyles(4).Width = 55

    End Sub

    Private Sub GridUbicaciones_CurrentCellChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GridUbicaciones.CurrentCellChanged
        TmpRow = GridUbicaciones.CurrentCell.RowNumber
        TmpCol = GridUbicaciones.CurrentCell.ColumnNumber

        TxtEdit.Visible = False
    End Sub

    Private Function Grabar() As Boolean
        Grabar = GrabarUbicacionProducto(GridUbicaciones, Me.CodigoProducto)
    End Function

    Private Function ValidarDatosGrabar() As Boolean

        For i As Integer = 1 To GridUbicaciones.BindingContext(GridUbicaciones.DataSource).Count
            If ValidarUbicacion(GridUbicaciones.Item(i - 1, 3), GridUbicaciones.Item(i - 1, 4)) Then
                ValidarDatosGrabar = True
            Else
                MsgBox("El valor para la fila Número [" & i & "][" & GridUbicaciones.Item(i - 1, 2) & "] no coincide con la máscara definida. Por favor verifique los datos.")
                Return False
            End If
        Next

    End Function

    Private Function ValidarUbicacion(ByVal pTexto As String, ByVal pMascara As String) As Boolean

        If pTexto = vbNullString Then Return True

        If Len(pTexto) <> Len(pMascara) Then Return False

        For i As Integer = 1 To Len(pMascara)

            If ValidarCaracter(pTexto, pMascara, Mid(pTexto, i, 1), i) Then
                ValidarUbicacion = True
            Else
                Return False
            End If

        Next

    End Function

    Private Function ValidarCaracter(ByVal pTexto As String, ByVal pMascara As String, ByVal pKeyChar As String, ByVal pPos As Integer) As Boolean

        pKeyChar = UCase(pKeyChar)

        Select Case pKeyChar

            Case "A" To "Z"
                ValidarCaracter = Mid(pMascara, pPos, 1) = "A"
            Case 0 To 9
                ValidarCaracter = Mid(pMascara, pPos, 1) = "#" Or Mid(pMascara, pPos, 1) = "A"
            Case "/", ".", ",", "\", "-", "|"
                ValidarCaracter = Mid(pMascara, pPos, 1) = pKeyChar

        End Select

    End Function

    Private Sub TxtEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtEdit.KeyPress

        If (Char.IsControl(e.KeyChar)) Then Return

        If TxtEdit.SelectionLength = Len(TxtEdit.Text) Then TxtEdit.Text = vbNullString

        If (Len(Me.GridUbicaciones.Item(TmpRow, 4).ToString) > Len(TxtEdit.Text)) Then
            If Not ValidarCaracter(TxtEdit.Text, Me.GridUbicaciones.Item(TmpRow, 4).ToString, e.KeyChar.ToString, Len(TxtEdit.Text) + 1) Then
                e.Handled = True
            Else
                TxtEdit.Text = UCase(TxtEdit.Text)
            End If
        Else
            e.Handled = True
        End If

    End Sub

    Private Sub TxtEdit_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TxtEdit.TextChanged
        If TxtEdit.Visible And Me.Visible Then
            Me.GridUbicaciones.Item(TmpRow, TmpCol) = TxtEdit.Text
        End If
    End Sub

    Private Sub TxtEdit_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtEdit.GotFocus
        PlaceEdit()
    End Sub

    Private Sub TxtEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TxtEdit.KeyDown
        Select Case e.KeyCode
            Case Keys.Escape, Keys.Enter
                TxtEdit.Visible = False
        End Select
    End Sub

    Private Sub GridUbicaciones_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles GridUbicaciones.KeyDown
        Select Case e.KeyCode
            Case Keys.Enter
                GridUbicaciones_DoubleClick(Nothing, Nothing)
        End Select
    End Sub

    Private Sub guardarButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles guardarButton.Click

        If ValidarDatosGrabar() Then
            If Grabar() Then
                MsgBox("Datos grabados con exito.")
                hideButton_Click(Nothing, Nothing)
            End If
        End If

    End Sub

    Private Sub Activation()

    End Sub

    Private Sub TimerReload_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TimerReload.Tick
        TimerReload.Enabled = False
        Activation()
    End Sub

End Class