﻿Public Class FormBuscarProveedor_Recepcion

    Dim filaDataGrid As Integer

    Private Sub FormBuscarProveedor_Recepcion_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Refresh()
        Me.Update()

        AjustarFormularios(Me)

    End Sub

    Private Sub cmbBuscarProveedor_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbBuscarProveedor.Click
        If txtDescripcionProveedor.Text = "" Then
            MsgBox("Debe colocar al menos un carácter en Descripción", MsgBoxStyle.Information, "isMOBILE")
            txtDescripcionProveedor.Focus()
        Else
            llenaDataGridBuscarProveedor(txtDescripcionProveedor.Text)
        End If
    End Sub

    Private Sub cmbAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbAceptar.Click
        seleccionarProveedorDataGrid()
    End Sub

    Private Sub txtDescripcionProveedor_KeyDown1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDescripcionProveedor.KeyDown
        If e.KeyValue = 13 Then
            If txtDescripcionProveedor.Text = "" Then
                MsgBox("Debe colocar en Descripción al menos un carácter", MsgBoxStyle.Information, "isMOBILE")
                txtDescripcionProveedor.Focus()
            Else
                llenaDataGridBuscarProveedor(txtDescripcionProveedor.Text)
            End If
        End If
    End Sub

    Private Sub DataGridBuscarProveedor_DoubleClick1(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridBuscarProveedor.DoubleClick
        seleccionarProveedorDataGrid()
    End Sub

    Private Sub cmbSalir_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSalir.Click
        txtDescripcionProveedor.Text = ""
        DataGridBuscarProveedor.DataSource = Nothing
        FormProveedorDeposito_Recepcion.Show()
        Me.Visible = False
    End Sub

    Private Sub FormBuscarProveedor_Recepcion_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated

        TimerReload.Interval = 100
        TimerReload.Enabled = True

    End Sub

    Private Sub Activation()

        txtDescripcionProveedor.Enabled = True
        txtDescripcionProveedor.Focus()
        txtDescripcionProveedor.SelectionStart = 0
        txtDescripcionProveedor.SelectionLength = txtDescripcionProveedor.Text.Length

    End Sub

    Private Sub TimerReload_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TimerReload.Tick
        TimerReload.Enabled = False
        Activation()
    End Sub

End Class