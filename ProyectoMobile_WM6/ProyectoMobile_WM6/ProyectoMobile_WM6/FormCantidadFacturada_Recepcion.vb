﻿Public Class FormCantidadFacturada_Recepcion

    Dim dataset_RegladeNegocio, dataset_DetallesProductoenODC, dataset_DetallesProductoenInventario As Data.DataSet
    Dim RegladeNegocio, valorRegladeNegocio As String
    Dim decimales As Integer
    Dim cantidadEmpaques As Integer
    Dim tipoPeso As Integer
    Dim FormaCargada As Boolean

    Private Sub FormCantidadFacturada_Recepcion_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Activated

        If Not FormaCargada Then

            FormaCargada = True

            TimerReload.Enabled = False
            TimerReload.Interval = 100
            TimerReload.Enabled = True

        End If

    End Sub

    Private Sub FormCantidadFacturada_Recepcion_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        lblUsuarioEnSesion.Text = "Usuario: " + descripcionUsuario
        Me.Refresh()
        Me.Update()

        AjustarFormularios(Me)

    End Sub

    Private Sub cmbFinalizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbFinalizar.Click

        If valorRegladeNegocio = "0" Then 'Trabaja solo con unidades
            If txtUnidadesFacturadas.Text = "" Then
                MsgBox("Debe ingresar la cantidad de unidades facturadas.", MsgBoxStyle.Information, "isMOBILE")
                txtUnidadesFacturadas.Focus()
            Else
                If Not IsNumeric(txtUnidadesFacturadas.Text) Then
                    MsgBox("Los datos deben ser numéricos.", MsgBoxStyle.Information, "isMOBILE")
                    txtUnidadesFacturadas.Text = ""
                    txtUnidadesFacturadas.Focus()
                Else
                    If Not IsNumeric(txtUnidadesFacturadas.Text) Then
                        MsgBox("Los datos deben ser numéricos.", MsgBoxStyle.Information, "isMOBILE")
                        txtEmpaquesFacturados.Text = ""
                        txtEmpaquesFacturados.Focus()
                    Else
                        txtEmpaquesFacturados.Text = 0 'No esta visible el textbox por lo que le coloco 0 para que lo guarde de esta forma en la base de datos
                        'unidadesRecibidas = CDbl(txtUnidadesFacturadas.Text)
                        FormGuardarDatos_Recepcion.lblUnidadesxEmpaque.Text = "Uni. x Emp.: " + CStr(UnidadesxEmpaque) 'Trae la informacion del FormGuardarDatos
                        dataset_Productos = detallesProducto(FormLecturaProducto_Recepcion.txtCodigoProducto.Text)
                        FormGuardarDatos_Recepcion.lblDescripcionProducto.Text = "Producto: " + dataset_Productos.Tables(0).Rows(0)("c_descri")
                        FormGuardarDatos_Recepcion.lblCantidadEmpaquesRecibidos.Text = FormCantidadRecibida_Recepcion.txtEmpaquesRecibidos.Text
                        FormGuardarDatos_Recepcion.lblCantidadUnidadesRecibidas.Text = FormCantidadRecibida_Recepcion.txtUnidadesRecibidas.Text
                        FormGuardarDatos_Recepcion.lblCantidadEmpaquesFacturados.Text = txtEmpaquesFacturados.Text
                        FormGuardarDatos_Recepcion.lblCantidadUnidadesFacturadas.Text = txtUnidadesFacturadas.Text
                        FormGuardarDatos_Recepcion.Show()
                        Me.Visible = False
                    End If
                End If
            End If
        Else
            If valorRegladeNegocio = "1" Then 'Trabaja solo con empaques
                If txtEmpaquesFacturados.Text = "" Then
                    MsgBox("Debe ingresar la cantidad de empaques facturados.", MsgBoxStyle.Information, "isMOBILE")
                    txtEmpaquesFacturados.Focus()
                Else
                    If Not IsNumeric(txtEmpaquesFacturados.Text) Then
                        MsgBox("Los datos deben ser numéricos.", MsgBoxStyle.Information, "isMOBILE")
                        txtEmpaquesFacturados.Text = ""
                        txtEmpaquesFacturados.Focus()
                    Else
                        txtUnidadesFacturadas.Text = 0 'No esta visible el textbox por lo que le coloco 0 para que lo guarde de esta forma en la base de datos
                        'empaquesRecibidos = CDbl(txtEmpaquesFacturados.Text)
                        FormGuardarDatos_Recepcion.lblUnidadesxEmpaque.Text = "Uni. x Emp.: " + CStr(UnidadesxEmpaque)
                        dataset_Productos = detallesProducto(FormLecturaProducto_Recepcion.txtCodigoProducto.Text)
                        FormGuardarDatos_Recepcion.lblDescripcionProducto.Text = "Producto: " + dataset_Productos.Tables(0).Rows(0)("c_descri")
                        FormGuardarDatos_Recepcion.lblCantidadEmpaquesRecibidos.Text = FormCantidadRecibida_Recepcion.txtEmpaquesRecibidos.Text
                        FormGuardarDatos_Recepcion.lblCantidadUnidadesRecibidas.Text = FormCantidadRecibida_Recepcion.txtUnidadesRecibidas.Text
                        FormGuardarDatos_Recepcion.lblCantidadEmpaquesFacturados.Text = txtEmpaquesFacturados.Text
                        FormGuardarDatos_Recepcion.lblCantidadUnidadesFacturadas.Text = txtUnidadesFacturadas.Text
                        FormGuardarDatos_Recepcion.Show()
                        Me.Visible = False
                    End If
                End If

            Else 'valorRegladeNegocio = 2 Trabaja con unidades y empaques
                If txtEmpaquesFacturados.Text = "" And txtUnidadesFacturadas.Text = "" Then
                    MsgBox("Debe ingresar la cantidad de empaques y/o unidades facturadas.", MsgBoxStyle.Information, "isMOBILE")
                    txtEmpaquesFacturados.Focus()
                Else
                    If txtEmpaquesFacturados.Text = "" And txtUnidadesFacturadas.Text <> "" Then
                        If Not IsNumeric(txtUnidadesFacturadas.Text) Then
                            MsgBox("Los datos deben ser numéricos.", MsgBoxStyle.Information, "isMOBILE")
                            txtUnidadesFacturadas.Text = ""
                            txtUnidadesFacturadas.Focus()
                        Else
                            txtEmpaquesFacturados.Text = 0
                            'empaquesRecibidos = CDbl(txtEmpaquesFacturados.Text)
                            'unidadesRecibidas = CDbl(txtUnidadesFacturadas.Text)
                            lblUnidadesxEmpaque.Text = FormCantidadRecibida_Recepcion.lblUnidadesxEmpaque.Text
                            FormGuardarDatos_Recepcion.lblUnidadesxEmpaque.Text = "Uni. x Emp.: " + CStr(UnidadesxEmpaque)
                            dataset_Productos = detallesProducto(FormLecturaProducto_Recepcion.txtCodigoProducto.Text)
                            FormGuardarDatos_Recepcion.lblDescripcionProducto.Text = "Producto: " + dataset_Productos.Tables(0).Rows(0)("c_descri")
                            FormGuardarDatos_Recepcion.lblCantidadEmpaquesRecibidos.Text = FormCantidadRecibida_Recepcion.txtEmpaquesRecibidos.Text
                            FormGuardarDatos_Recepcion.lblCantidadUnidadesRecibidas.Text = FormCantidadRecibida_Recepcion.txtUnidadesRecibidas.Text
                            FormGuardarDatos_Recepcion.lblCantidadEmpaquesFacturados.Text = txtEmpaquesFacturados.Text
                            FormGuardarDatos_Recepcion.lblCantidadUnidadesFacturadas.Text = txtUnidadesFacturadas.Text
                            FormGuardarDatos_Recepcion.Show()
                            Me.Visible = False
                        End If
                    Else
                        If txtEmpaquesFacturados.Text <> "" And txtUnidadesFacturadas.Text = "" Then
                            If Not IsNumeric(txtEmpaquesFacturados.Text) Then
                                MsgBox("Los datos deben ser numéricos.", MsgBoxStyle.Information, "isMOBILE")
                                txtEmpaquesFacturados.Text = ""
                                txtEmpaquesFacturados.Focus()
                            Else
                                txtUnidadesFacturadas.Text = 0
                                'unidadesRecibidas = CDbl(txtUnidadesFacturadas.Text)
                                'empaquesRecibidos = CDbl(txtEmpaquesFacturados.Text)
                                FormGuardarDatos_Recepcion.lblUnidadesxEmpaque.Text = "Uni. x Emp.: " + CStr(UnidadesxEmpaque)
                                dataset_Productos = detallesProducto(FormLecturaProducto_Recepcion.txtCodigoProducto.Text)
                                FormGuardarDatos_Recepcion.lblDescripcionProducto.Text = "Producto: " + dataset_Productos.Tables(0).Rows(0)("c_descri")
                                FormGuardarDatos_Recepcion.lblCantidadEmpaquesRecibidos.Text = FormCantidadRecibida_Recepcion.txtEmpaquesRecibidos.Text
                                FormGuardarDatos_Recepcion.lblCantidadUnidadesRecibidas.Text = FormCantidadRecibida_Recepcion.txtUnidadesRecibidas.Text
                                FormGuardarDatos_Recepcion.lblCantidadEmpaquesFacturados.Text = txtEmpaquesFacturados.Text
                                FormGuardarDatos_Recepcion.lblCantidadUnidadesFacturadas.Text = txtUnidadesFacturadas.Text
                                FormGuardarDatos_Recepcion.Show()
                                Me.Visible = False
                            End If
                        Else
                            If txtEmpaquesFacturados.Text <> "" And txtUnidadesFacturadas.Text <> "" Then
                                If IsNumeric(txtUnidadesFacturadas.Text) Then
                                    If IsNumeric(txtEmpaquesFacturados.Text) Then
                                        'unidadesRecibidas = CDbl(txtUnidadesFacturadas.Text)
                                        'empaquesRecibidos = CDbl(txtEmpaquesFacturados.Text)
                                        FormGuardarDatos_Recepcion.lblUnidadesxEmpaque.Text = "Uni. x Emp.: " + CStr(UnidadesxEmpaque)
                                        dataset_Productos = detallesProducto(FormLecturaProducto_Recepcion.txtCodigoProducto.Text)
                                        FormGuardarDatos_Recepcion.lblDescripcionProducto.Text = "Producto: " + dataset_Productos.Tables(0).Rows(0)("c_descri")
                                        FormGuardarDatos_Recepcion.lblCantidadEmpaquesRecibidos.Text = FormCantidadRecibida_Recepcion.txtEmpaquesRecibidos.Text
                                        FormGuardarDatos_Recepcion.lblCantidadUnidadesRecibidas.Text = FormCantidadRecibida_Recepcion.txtUnidadesRecibidas.Text
                                        FormGuardarDatos_Recepcion.lblCantidadEmpaquesFacturados.Text = txtEmpaquesFacturados.Text
                                        FormGuardarDatos_Recepcion.lblCantidadUnidadesFacturadas.Text = txtUnidadesFacturadas.Text
                                        FormGuardarDatos_Recepcion.Show()
                                        Me.Visible = False
                                    Else
                                        MsgBox("Los datos deben ser numéricos.", MsgBoxStyle.Information, "isMOBILE")
                                        txtEmpaquesFacturados.Text = ""
                                        txtEmpaquesFacturados.Focus()
                                    End If
                                Else
                                    If IsNumeric(txtEmpaquesFacturados.Text) Then
                                        MsgBox("Los datos deben ser numéricos.", MsgBoxStyle.Information, "isMOBILE")
                                        txtUnidadesFacturadas.Text = ""
                                        txtUnidadesFacturadas.Focus()
                                    Else
                                        MsgBox("Los datos deben ser numéricos.", MsgBoxStyle.Information, "isMOBILE")
                                        txtEmpaquesFacturados.Text = ""
                                        txtUnidadesFacturadas.Text = ""
                                        txtEmpaquesFacturados.Focus()
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub cmbSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSalir.Click
        FormCantidadRecibida_Recepcion.Show()
        Me.Visible = False
    End Sub

    Private Sub txtEmpaquesFacturados_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtEmpaquesFacturados.KeyPress
        'If Char.IsNumber(e.KeyChar) Then 'Comentado, no permitía ingresar . para valores decimales
        'MsgBox("-!" & txtEmpaquesFacturados.Text & e.KeyChar.ToString & "!-") Nueva forma de Validar Aprobada.
        If IsNumeric(txtEmpaquesFacturados.Text & e.KeyChar.ToString) Then
            e.Handled = False
        Else
            If Char.IsControl(e.KeyChar) Then
                e.Handled = False
            Else
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txtEmpaquesFacturados_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEmpaquesFacturados.LostFocus
        Dim TmpCantDec As Short
        Dim TmpText As String
        If IsNumeric(txtEmpaquesFacturados.Text) And Not (dataset_Productos Is Nothing) Then
            If dataset_Productos.Tables(0).Rows.Count <> 0 Then
                TmpText = txtEmpaquesFacturados.Text
                TmpCantDec = CShort(Val(dataset_Productos.Tables(0).Rows(0)("CANT_DECIMALES")))
                txtEmpaquesFacturados.Text = FormatNumber(txtEmpaquesFacturados.Text, TmpCantDec, TriState.True, TriState.False, TriState.False)
                If TmpText <> txtEmpaquesFacturados.Text Then txtEmpaquesFacturados.Focus()
            End If
        End If
    End Sub

    Private Sub txtUnidadesFacturadas_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtUnidadesFacturadas.KeyPress
        'If Char.IsNumber(e.KeyChar) Then 'Comentado, no permitía ingresar . para valores decimales
        'MsgBox("-!" & txtUnidadesFacturadas.Text & e.KeyChar.ToString & "!-") Nueva forma de Validar Aprobada.
        If IsNumeric(txtUnidadesFacturadas.Text & e.KeyChar.ToString) Then
            e.Handled = False
        Else
            If Char.IsControl(e.KeyChar) Then
                e.Handled = False
            Else
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txtUnidadesFacturadas_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUnidadesFacturadas.LostFocus
        Dim TmpCantDec As Short
        Dim TmpText As String
        If IsNumeric(txtUnidadesFacturadas.Text) And Not (dataset_Productos Is Nothing) Then
            If dataset_Productos.Tables(0).Rows.Count <> 0 Then
                TmpText = txtUnidadesFacturadas.Text
                TmpCantDec = CShort(Val(dataset_Productos.Tables(0).Rows(0)("CANT_DECIMALES")))
                txtUnidadesFacturadas.Text = FormatNumber(txtUnidadesFacturadas.Text, TmpCantDec, TriState.True, TriState.False, TriState.False)
                If TmpText <> txtUnidadesFacturadas.Text Then txtUnidadesFacturadas.Focus()
            End If
        End If
    End Sub

    Private Sub Activation()

        lblFechaRecepcion.Text = Date.Now.ToString
        RegladeNegocio = "Recepcion_FormadeTrabajo"

        dataset_RegladeNegocio = consultarRegladeNegocio(RegladeNegocio)

        If dataset_RegladeNegocio.Tables(0).Rows.Count = 0 Then
            MsgBox("Ha ocurrido un error al cargar las reglas de negocio.", MsgBoxStyle.Information, "isMOBILE")
        Else
            valorRegladeNegocio = dataset_RegladeNegocio.Tables(0).Rows(0)("valor")
            If valorRegladeNegocio = "0" Then
                txtEmpaquesFacturados.Visible = False
                lblEmpaquesFacturados.Visible = False
                lblUnidadesxEmpaque.Visible = False
                lblEmpaquesSolicitados.Visible = False
                txtUnidadesFacturadas.Focus()
            Else
                If valorRegladeNegocio = "1" Then
                    txtUnidadesFacturadas.Visible = False
                    lblUnidadesFacturadas.Visible = False
                    lblUnidadesSolicitadas.Visible = False
                    txtEmpaquesFacturados.Focus()
                Else
                    txtEmpaquesFacturados.Focus()
                End If
            End If
        End If

        dataset_Productos = detallesProducto(FormLecturaProducto_Recepcion.txtCodigoProducto.Text)

    End Sub

    Private Sub TimerReload_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TimerReload.Tick
        TimerReload.Enabled = False
        Activation()
    End Sub

End Class