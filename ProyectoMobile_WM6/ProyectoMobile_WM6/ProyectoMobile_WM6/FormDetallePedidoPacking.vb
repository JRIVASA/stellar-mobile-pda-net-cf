﻿Public Class FormDetallePedidoPacking

    Private FormaCargada As Boolean

    Public CodLote As String
    Public CodPedido As String
    Public CantEmpacados As Long
    Public CantProductos As Long

    Private Sub DataGridProductos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridProductos.Click

        If DataGridProductos.VisibleRowCount <= 0 Then Exit Sub

        Dim Row As Integer : Row = DataGridProductos.CurrentRowIndex
        Dim Col As Integer : Col = DataGridProductos.CurrentCell.ColumnNumber

        If Col <> 0 Then
            ' Descripcion. No hacer nada para que se muestre el tooltip.
        Else

            ' Ir al producto.

            FormLecturaProducto_Packing.lblDescripcion.Text = CStr(DataGridProductos.Item(Row, 1))
            FormLecturaProducto_Packing.lblDescripcion.Tag = CStr(DataGridProductos.Item(Row, 5)) ' Codigo

            '

            FormLecturaProducto_Packing.CantDecimalesProducto = DataGridProductos.Item(Row, 6)

            FormLecturaProducto_Packing.txtCantSolicitada.Text = FormatNumber(DataGridProductos.Item(Row, 2), _
            FormLecturaProducto_Packing.CantDecimalesProducto, TriState.True, TriState.False, TriState.False)

            FormLecturaProducto_Packing.txtCantSolicitada.ReadOnly = True

            FormLecturaProducto_Packing.txtCantRecolectada.Text = FormatNumber(DataGridProductos.Item(Row, 3), _
            FormLecturaProducto_Packing.CantDecimalesProducto, TriState.True, TriState.False, TriState.False)

            FormLecturaProducto_Packing.txtCantRecolectada.ReadOnly = True

            '

            FormLecturaProducto_Packing.txtCantEmpacada.Text = FormatNumber(DataGridProductos.Item(Row, 4), _
            FormLecturaProducto_Packing.CantDecimalesProducto, TriState.True, TriState.False, TriState.False)

            FormLecturaProducto_Packing.txtCantEmpacada.Tag = FormLecturaProducto_Packing.txtCantEmpacada.Text

            FormLecturaProducto_Packing.txtCantEmpacada.ReadOnly = False

            '

            FormLecturaProducto_Packing.CodLote = Me.CodLote
            FormLecturaProducto_Packing.CodPedido = Me.CodPedido

            FormaCargada = False
            TimerReload.Enabled = False
            TimerReload.Interval = 500

            FormLecturaProducto_Packing.FormaCargada = False
            FormLecturaProducto_Packing.Show()

            Me.Visible = False

        End If

    End Sub

    Private Sub FormPickingUserMain_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Activated

        If Not FormaCargada Then

            FormaCargada = True

            lblUsuarioEnSesion.Text = "Usuario: " + descripcionUsuario

            'TimerReload_Tick(TimerReload, Nothing)

            txtCodigoProducto.Focus()

            TimerReload.Enabled = False
            TimerReload.Enabled = True

        End If

    End Sub

    Private Sub FormPickingUserMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Me.Refresh()
        'Me.Update()

        AjustarFormularios(Me)
        AjustarFormularios(Me.PanelInventario_Inv_Activo)

    End Sub

    Private Sub cmbSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSalir.Click
        FormaCargada = False
        txtCodigoProducto.Text = String.Empty
        DataGridProductos.DataSource = Nothing
        TimerReload.Enabled = False
        TimerReload.Interval = 500
        FormPackingUserMain.Visible = True
        Me.Visible = False
    End Sub

    Private Sub TimerReload_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TimerReload.Tick

        TimerReload.Enabled = False

        If TimerReload.Interval = 500 Then ' Iniciando. Despues colocarlo cada 3 minutos.
            TimerReload.Interval = 180000
        End If

        'lblGridTooltip.Visible = True
        lblGridTooltip.Text = "Cargando..."

        CargarDataGridPedidoPacking(CStr(datasetUsuario.Tables(0).Rows(0)("codusuario")), _
        CodLote, CodPedido)
        DataGridProductos.Focus()

        'lblGridTooltip.Text = "Lista Actualizada."

        If DataGridProductos.DataSource Is Nothing Then
            CantProductos = 0
        Else
            CantProductos = CType(DataGridProductos.DataSource, DataTable).Rows.Count
        End If

        If CantProductos = 0 Then
            lblGridTooltip.Text = "No se encontraron detalles del pedido."
            CmbFinalizar.Visible = False
        ElseIf (CantProductos - CantEmpacados) > 0 Then
            CmbFinalizar.Visible = True
            CmbFinalizar.BackColor = Color.Chocolate
            lblGridTooltip.Text = "Ha empacado en su totalidad " & CantEmpacados.ToString & " de " & CantProductos.ToString & ". Falta(n) " & (CantProductos - CantEmpacados).ToString
            'lblGridTooltip.Visible = False
        Else
            CmbFinalizar.Visible = True
            CmbFinalizar.BackColor = Color.LightGreen
            lblGridTooltip.Text = "Todos los productos han sido empacados. (" & CantProductos.ToString & ")"
            'lblGridTooltip.Visible = False
        End If

        If DataGridProductos.VisibleRowCount <= 0 Then ProcesarBusqueda()

        TimerReload.Enabled = True

    End Sub

    Private Sub DataGridProductos_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles DataGridProductos.KeyPress
        txtCodigoProducto.Focus()
        txtCodigoProducto.Text = e.KeyChar.ToString
        txtCodigoProducto.SelectionStart = Len(txtCodigoProducto.Text)
    End Sub

    Private Sub DataGridProductos_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles DataGridProductos.MouseMove

        If DataGridProductos.VisibleRowCount > 0 Then

            Dim HoverRow As Integer : HoverRow = DataGridProductos.HitTest(e.X, e.Y).Row

            If HoverRow >= 0 Then

                lblGridTooltip.Text = CStr(DataGridProductos.Item(HoverRow, 1)) & vbNewLine & _
                "Rec.: " & CStr(DataGridProductos.Item(HoverRow, 3)) & " | " & "Emp.: " & CStr(DataGridProductos.Item(HoverRow, 4))

            End If

        End If

    End Sub

    Private Sub txtCodigoProducto_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCodigoProducto.GotFocus
        SeleccionarTexto(txtCodigoProducto)
    End Sub

    Private Sub txtCodigoProducto_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCodigoProducto.KeyDown
        If e.KeyCode = Keys.Return Then
            TimerBuscarProducto_Tick(Nothing, Nothing)
        End If
    End Sub

    Private Sub txtCodigoProducto_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCodigoProducto.TextChanged

        TimerBuscarProducto.Enabled = False ' Reset Timer...
        TimerBuscarProducto.Enabled = True

    End Sub

    Private Sub ProcesarBusqueda()

        If DataGridProductos.VisibleRowCount > 0 Then

            'lblGridTooltip.Visible = True
            lblGridTooltip.Text = "Buscando Producto..."
            lblGridTooltip.Refresh()

            Cursor.Current = Cursors.WaitCursor

            Me.Enabled = False

            'Call ActualizacionParcial("Productos") ' Mejor no... potea la aplicación y despues se quejan...

            Cursor.Current = Cursors.Default

            Dim CodProducto As String, DSCodigo As DataSet

            DSCodigo = buscarCodigoProducto(txtCodigoProducto.Text)

            If DSCodigo.Tables(0).Rows.Count = 0 Then
                lblGridTooltip.Text = "Código de producto ingresado no existe."
            Else

                Dim Rows() As DataRow, Row As Integer, DsProductos As DataTable

                CodProducto = DSCodigo.Tables(0).Rows(0).Item("c_CodNasa")

                DsProductos = CType(DataGridProductos.DataSource, DataTable)

                Rows = DsProductos _
                .Select("CodProducto = '" & CodProducto & "'")

                If Rows.Length > 0 Then ' Check

                    Row = DsProductos.Rows.IndexOf(Rows(0))

                    DataGridProductos.Focus()
                    DataGridProductos.Select(Row)
                    DataGridProductos.CurrentCell = New DataGridCell(Row, 0)
                    DataGridProductos_Click(DataGridProductos, Nothing)

                Else
                    lblGridTooltip.Text = "Producto no encontrado."
                End If

            End If

            Me.Enabled = True

            txtCodigoProducto.Focus()

            Cursor.Current = Cursors.Default

        Else
            lblGridTooltip.Text = "No hay productos pendientes por empacar."
        End If

    End Sub

    Private Sub TimerBuscarProducto_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TimerBuscarProducto.Tick
        TimerBuscarProducto.Enabled = False
        ProcesarBusqueda()
    End Sub

    Private Sub cmbActualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbActualizar.Click
        TimerReload_Tick(TimerReload, Nothing)
    End Sub

    Private Sub cmbFinalizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmbFinalizar.Click

        ' Este botón está deshabilitado, no visible, debido a que no tiene aún funcionalidad.
        ' Este código no se está habilitando debido a que el PDA no va a generar / imprimir una nota de entrega
        ' Ni tampoco va a hacer la llamada para finalizar los pagos. Este proceso de terminar el empacado 
        ' se debe hacer desde BUSINESS, donde todos esos aspectos y otros más fueron considerados para el ciclo
        ' de gestión de los pedidos. Este botón se mantiene oculto

        ' Actualización: Ahora se le está colocando funcionalidad.

        If (CantProductos - CantEmpacados) > 0 Then ' Faltan
            Dim Resp As MsgBoxResult = MsgBox("¿Está seguro que desea finalizar el empacado con productos faltantes?", MsgBoxStyle.YesNo)
            If Resp = MsgBoxResult.No Then Exit Sub
        End If

        MessageBox.Show("Ingrese la cantidad de empaques usados para este pedido.", "isMOBILE")

        FormNumPadGeneral.ValorOriginal = "1"

        Dim A = FormNumPadGeneral.ShowDialog()

        Dim NewCant = Math.Round(Val(FormNumPadGeneral.ValorNumerico), 0)

        Me.Activate()
        Me.BringToFront()

        Dim CantEmpaques As Long = IIf(NewCant >= 1, NewCant, 1)

        If FinalizarPackingUser(CStr(datasetUsuario.Tables(0).Rows(0)("codusuario")), CodLote, CodPedido, CantEmpaques) Then
            cmbSalir_Click(Nothing, Nothing)
        End If

    End Sub

End Class