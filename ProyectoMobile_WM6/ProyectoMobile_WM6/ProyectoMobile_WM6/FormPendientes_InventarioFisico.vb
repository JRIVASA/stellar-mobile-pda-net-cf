﻿Public Class FormPendientes_InventarioFisico

    Dim filaDataGrid As Integer

    Private Sub DataDocPendientes_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataDocPendientes.GotFocus
        'Busca la información que esta pendiente
        llenaDataGrid_DocPendientes()
    End Sub

    Private Sub cmbEnviarPendientes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbEnviarPendientes.Click

        'Determina la fila que el usuario selecciono
        filaDataGrid = DataDocPendientes.CurrentRowIndex

        If filaDataGrid = -1 Then
            MsgBox("No hay documentos de inventario pendientes por enviar.", MsgBoxStyle.Information, "isMOBILE")
        Else
            envio_Ind_Pendientes(filaDataGrid)
        End If

    End Sub

    Private Sub cmbTodosPendientes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbTodosPendientes.Click

        filaDataGrid = DataDocPendientes.CurrentRowIndex

        If filaDataGrid = -1 Then
            MsgBox("No hay documentos de inventario pendientes por enviar", MsgBoxStyle.Information, "isMOBILE")
        Else
            envio_Todos_Pendientes()
        End If

    End Sub

    Private Sub FormPENDIENTES_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        lblUsuarioEnSesion.Text = "Usuario: " + descripcionUsuario

        Me.Refresh()
        Me.Update()

        AjustarFormularios(Me)
        AjustarFormularios(Me.PanelPendientes)

        Me.Focus()

    End Sub


    Private Sub cmbSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSalir.Click
        FormMenuInventarioFisico.Show()
        Me.Visible = False
    End Sub

End Class