﻿Public Class FormInv_Activo_InventarioFisico

    Private Sub DataGridINVENTARIO_INV_ACTIVO_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridINVENTARIO_INV_ACTIVO.DoubleClick

        FormElab_Inv_InventarioFisico.Show()
        Me.Visible = False
        iniciarInventario()

    End Sub

    Private Sub DataGridINVENTARIO_INV_ACTIVO_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridINVENTARIO_INV_ACTIVO.GotFocus
        llenaDataGrid_INV_ACTIVO()
    End Sub

    Private Sub FormInv_Activo_InventarioFisico_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        lblUsuarioEnSesion.Text = "Usuario: " + descripcionUsuario
        Me.Refresh()
        Me.Update()

        AjustarFormularios(Me)
        AjustarFormularios(Me.PanelInventario_Inv_Activo)

    End Sub

    Private Sub cmbSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSalir.Click
        If Not CompatibilidadFueraDeLinea Then
            cerrarConexion_SERVIDOR()
        End If
        FormMenuInventarioFisico.Show()
        Me.Visible = False
    End Sub

End Class