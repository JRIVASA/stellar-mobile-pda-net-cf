﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class FormBuscarODC_Recepcion
    Inherits System.Windows.Forms.Form

    'Form invalida a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer
    Private mainMenu1 As System.Windows.Forms.MainMenu

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar con el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormBuscarODC_Recepcion))
        Me.mainMenu1 = New System.Windows.Forms.MainMenu
        Me.DataGridBuscarODC = New System.Windows.Forms.DataGrid
        Me.txtCodigoProveedor = New System.Windows.Forms.TextBox
        Me.lblCodigoProveedor = New System.Windows.Forms.Label
        Me.cmbSalir = New System.Windows.Forms.PictureBox
        Me.cmbBuscarODC = New System.Windows.Forms.Button
        Me.cmbAceptar = New System.Windows.Forms.Button
        Me.picBarraMorada = New System.Windows.Forms.PictureBox
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.lblUsuarioEnSesion = New System.Windows.Forms.Label
        Me.pictureFondoBuscarProveedor = New System.Windows.Forms.PictureBox
        Me.SuspendLayout()
        '
        'DataGridBuscarODC
        '
        Me.DataGridBuscarODC.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.DataGridBuscarODC.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold)
        Me.DataGridBuscarODC.Location = New System.Drawing.Point(9, 119)
        Me.DataGridBuscarODC.Name = "DataGridBuscarODC"
        Me.DataGridBuscarODC.Size = New System.Drawing.Size(220, 134)
        Me.DataGridBuscarODC.TabIndex = 33
        '
        'txtCodigoProveedor
        '
        Me.txtCodigoProveedor.Font = New System.Drawing.Font("Verdana", 14.0!, System.Drawing.FontStyle.Bold)
        Me.txtCodigoProveedor.Location = New System.Drawing.Point(9, 84)
        Me.txtCodigoProveedor.MaxLength = 50
        Me.txtCodigoProveedor.Name = "txtCodigoProveedor"
        Me.txtCodigoProveedor.Size = New System.Drawing.Size(135, 29)
        Me.txtCodigoProveedor.TabIndex = 31
        '
        'lblCodigoProveedor
        '
        Me.lblCodigoProveedor.BackColor = System.Drawing.Color.Gainsboro
        Me.lblCodigoProveedor.Font = New System.Drawing.Font("Verdana", 14.0!, System.Drawing.FontStyle.Bold)
        Me.lblCodigoProveedor.Location = New System.Drawing.Point(33, 58)
        Me.lblCodigoProveedor.Name = "lblCodigoProveedor"
        Me.lblCodigoProveedor.Size = New System.Drawing.Size(172, 25)
        Me.lblCodigoProveedor.Text = "Cod. Proveedor"
        Me.lblCodigoProveedor.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'cmbSalir
        '
        Me.cmbSalir.Image = CType(resources.GetObject("cmbSalir.Image"), System.Drawing.Image)
        Me.cmbSalir.Location = New System.Drawing.Point(0, 0)
        Me.cmbSalir.Name = "cmbSalir"
        Me.cmbSalir.Size = New System.Drawing.Size(41, 38)
        Me.cmbSalir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'cmbBuscarODC
        '
        Me.cmbBuscarODC.BackColor = System.Drawing.Color.White
        Me.cmbBuscarODC.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.cmbBuscarODC.ForeColor = System.Drawing.Color.Black
        Me.cmbBuscarODC.Location = New System.Drawing.Point(151, 84)
        Me.cmbBuscarODC.Name = "cmbBuscarODC"
        Me.cmbBuscarODC.Size = New System.Drawing.Size(78, 29)
        Me.cmbBuscarODC.TabIndex = 40
        Me.cmbBuscarODC.Text = "Buscar"
        '
        'cmbAceptar
        '
        Me.cmbAceptar.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.cmbAceptar.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.cmbAceptar.ForeColor = System.Drawing.Color.Black
        Me.cmbAceptar.Location = New System.Drawing.Point(78, 265)
        Me.cmbAceptar.Name = "cmbAceptar"
        Me.cmbAceptar.Size = New System.Drawing.Size(78, 25)
        Me.cmbAceptar.TabIndex = 116
        Me.cmbAceptar.Text = "Aceptar"
        '
        'picBarraMorada
        '
        Me.picBarraMorada.Image = CType(resources.GetObject("picBarraMorada.Image"), System.Drawing.Image)
        Me.picBarraMorada.Location = New System.Drawing.Point(0, 260)
        Me.picBarraMorada.Name = "picBarraMorada"
        Me.picBarraMorada.Size = New System.Drawing.Size(240, 40)
        Me.picBarraMorada.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Gainsboro
        Me.PictureBox1.Location = New System.Drawing.Point(0, 54)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(240, 205)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'lblUsuarioEnSesion
        '
        Me.lblUsuarioEnSesion.BackColor = System.Drawing.Color.DarkGray
        Me.lblUsuarioEnSesion.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblUsuarioEnSesion.Location = New System.Drawing.Point(0, 39)
        Me.lblUsuarioEnSesion.Name = "lblUsuarioEnSesion"
        Me.lblUsuarioEnSesion.Size = New System.Drawing.Size(240, 15)
        Me.lblUsuarioEnSesion.Text = "Usuario:"
        '
        'pictureFondoBuscarProveedor
        '
        Me.pictureFondoBuscarProveedor.Image = CType(resources.GetObject("pictureFondoBuscarProveedor.Image"), System.Drawing.Image)
        Me.pictureFondoBuscarProveedor.Location = New System.Drawing.Point(42, 0)
        Me.pictureFondoBuscarProveedor.Name = "pictureFondoBuscarProveedor"
        Me.pictureFondoBuscarProveedor.Size = New System.Drawing.Size(198, 38)
        Me.pictureFondoBuscarProveedor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'FormBuscarODC_Recepcion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(240, 300)
        Me.ControlBox = False
        Me.Controls.Add(Me.pictureFondoBuscarProveedor)
        Me.Controls.Add(Me.lblUsuarioEnSesion)
        Me.Controls.Add(Me.cmbBuscarODC)
        Me.Controls.Add(Me.DataGridBuscarODC)
        Me.Controls.Add(Me.txtCodigoProveedor)
        Me.Controls.Add(Me.lblCodigoProveedor)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.cmbAceptar)
        Me.Controls.Add(Me.picBarraMorada)
        Me.Controls.Add(Me.cmbSalir)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormBuscarODC_Recepcion"
        Me.Text = "isMOBILE - Recepción"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridBuscarODC As System.Windows.Forms.DataGrid
    Friend WithEvents txtCodigoProveedor As System.Windows.Forms.TextBox
    Friend WithEvents lblCodigoProveedor As System.Windows.Forms.Label
    Friend WithEvents cmbSalir As System.Windows.Forms.PictureBox
    Friend WithEvents cmbBuscarODC As System.Windows.Forms.Button
    Friend WithEvents cmbAceptar As System.Windows.Forms.Button
    Friend WithEvents picBarraMorada As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents lblUsuarioEnSesion As System.Windows.Forms.Label
    Friend WithEvents pictureFondoBuscarProveedor As System.Windows.Forms.PictureBox
End Class
