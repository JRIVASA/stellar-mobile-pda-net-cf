﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class FormDocumentoTransportista_Recepcion
    Inherits System.Windows.Forms.Form

    'Form invalida a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer
    Private mainMenu1 As System.Windows.Forms.MainMenu

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar con el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormDocumentoTransportista_Recepcion))
        Me.mainMenu1 = New System.Windows.Forms.MainMenu
        Me.txtTransportista = New System.Windows.Forms.TextBox
        Me.lblTransportista = New System.Windows.Forms.Label
        Me.txtDocumento = New System.Windows.Forms.TextBox
        Me.lblDocumento = New System.Windows.Forms.Label
        Me.PictureFondoProveedorDeposito_Recepcion = New System.Windows.Forms.PictureBox
        Me.ProgressBar_FormDocumentoTransportista = New System.Windows.Forms.ProgressBar
        Me.lblProgressBar_FormDocumentoTransportista = New System.Windows.Forms.Label
        Me.cmbSalir = New System.Windows.Forms.PictureBox
        Me.picBarraMorada = New System.Windows.Forms.PictureBox
        Me.cmbSiguiente = New System.Windows.Forms.Button
        Me.cmbCancelar = New System.Windows.Forms.Button
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.lblUsuarioEnSesion = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'txtTransportista
        '
        Me.txtTransportista.Font = New System.Drawing.Font("Verdana", 14.0!, System.Drawing.FontStyle.Bold)
        Me.txtTransportista.Location = New System.Drawing.Point(37, 211)
        Me.txtTransportista.MaxLength = 50
        Me.txtTransportista.Name = "txtTransportista"
        Me.txtTransportista.Size = New System.Drawing.Size(164, 29)
        Me.txtTransportista.TabIndex = 40
        '
        'lblTransportista
        '
        Me.lblTransportista.BackColor = System.Drawing.Color.Gainsboro
        Me.lblTransportista.Font = New System.Drawing.Font("Verdana", 14.0!, System.Drawing.FontStyle.Bold)
        Me.lblTransportista.Location = New System.Drawing.Point(37, 183)
        Me.lblTransportista.Name = "lblTransportista"
        Me.lblTransportista.Size = New System.Drawing.Size(161, 25)
        Me.lblTransportista.Text = "Transportista"
        Me.lblTransportista.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txtDocumento
        '
        Me.txtDocumento.Font = New System.Drawing.Font("Verdana", 14.0!, System.Drawing.FontStyle.Bold)
        Me.txtDocumento.Location = New System.Drawing.Point(37, 101)
        Me.txtDocumento.MaxLength = 50
        Me.txtDocumento.Name = "txtDocumento"
        Me.txtDocumento.Size = New System.Drawing.Size(164, 29)
        Me.txtDocumento.TabIndex = 36
        '
        'lblDocumento
        '
        Me.lblDocumento.BackColor = System.Drawing.Color.Gainsboro
        Me.lblDocumento.Font = New System.Drawing.Font("Verdana", 14.0!, System.Drawing.FontStyle.Bold)
        Me.lblDocumento.Location = New System.Drawing.Point(52, 73)
        Me.lblDocumento.Name = "lblDocumento"
        Me.lblDocumento.Size = New System.Drawing.Size(139, 25)
        Me.lblDocumento.Text = "Documento"
        Me.lblDocumento.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'PictureFondoProveedorDeposito_Recepcion
        '
        Me.PictureFondoProveedorDeposito_Recepcion.Image = CType(resources.GetObject("PictureFondoProveedorDeposito_Recepcion.Image"), System.Drawing.Image)
        Me.PictureFondoProveedorDeposito_Recepcion.Location = New System.Drawing.Point(42, 0)
        Me.PictureFondoProveedorDeposito_Recepcion.Name = "PictureFondoProveedorDeposito_Recepcion"
        Me.PictureFondoProveedorDeposito_Recepcion.Size = New System.Drawing.Size(198, 38)
        Me.PictureFondoProveedorDeposito_Recepcion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'ProgressBar_FormDocumentoTransportista
        '
        Me.ProgressBar_FormDocumentoTransportista.Location = New System.Drawing.Point(37, 153)
        Me.ProgressBar_FormDocumentoTransportista.Name = "ProgressBar_FormDocumentoTransportista"
        Me.ProgressBar_FormDocumentoTransportista.Size = New System.Drawing.Size(164, 15)
        Me.ProgressBar_FormDocumentoTransportista.Visible = False
        '
        'lblProgressBar_FormDocumentoTransportista
        '
        Me.lblProgressBar_FormDocumentoTransportista.BackColor = System.Drawing.Color.Gainsboro
        Me.lblProgressBar_FormDocumentoTransportista.Location = New System.Drawing.Point(37, 131)
        Me.lblProgressBar_FormDocumentoTransportista.Name = "lblProgressBar_FormDocumentoTransportista"
        Me.lblProgressBar_FormDocumentoTransportista.Size = New System.Drawing.Size(164, 19)
        Me.lblProgressBar_FormDocumentoTransportista.Text = "Verificando Documento..."
        Me.lblProgressBar_FormDocumentoTransportista.Visible = False
        '
        'cmbSalir
        '
        Me.cmbSalir.Image = CType(resources.GetObject("cmbSalir.Image"), System.Drawing.Image)
        Me.cmbSalir.Location = New System.Drawing.Point(0, 0)
        Me.cmbSalir.Name = "cmbSalir"
        Me.cmbSalir.Size = New System.Drawing.Size(41, 38)
        Me.cmbSalir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'picBarraMorada
        '
        Me.picBarraMorada.Image = CType(resources.GetObject("picBarraMorada.Image"), System.Drawing.Image)
        Me.picBarraMorada.Location = New System.Drawing.Point(0, 259)
        Me.picBarraMorada.Name = "picBarraMorada"
        Me.picBarraMorada.Size = New System.Drawing.Size(240, 40)
        Me.picBarraMorada.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'cmbSiguiente
        '
        Me.cmbSiguiente.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.cmbSiguiente.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.cmbSiguiente.ForeColor = System.Drawing.Color.Black
        Me.cmbSiguiente.Location = New System.Drawing.Point(128, 265)
        Me.cmbSiguiente.Name = "cmbSiguiente"
        Me.cmbSiguiente.Size = New System.Drawing.Size(78, 25)
        Me.cmbSiguiente.TabIndex = 48
        Me.cmbSiguiente.Text = "Siguiente"
        '
        'cmbCancelar
        '
        Me.cmbCancelar.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.cmbCancelar.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.cmbCancelar.ForeColor = System.Drawing.Color.Black
        Me.cmbCancelar.Location = New System.Drawing.Point(29, 265)
        Me.cmbCancelar.Name = "cmbCancelar"
        Me.cmbCancelar.Size = New System.Drawing.Size(78, 25)
        Me.cmbCancelar.TabIndex = 49
        Me.cmbCancelar.Text = "Cancelar"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Gainsboro
        Me.PictureBox1.Location = New System.Drawing.Point(0, 56)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(240, 203)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'lblUsuarioEnSesion
        '
        Me.lblUsuarioEnSesion.BackColor = System.Drawing.Color.DarkGray
        Me.lblUsuarioEnSesion.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblUsuarioEnSesion.Location = New System.Drawing.Point(0, 39)
        Me.lblUsuarioEnSesion.Name = "lblUsuarioEnSesion"
        Me.lblUsuarioEnSesion.Size = New System.Drawing.Size(240, 15)
        Me.lblUsuarioEnSesion.Text = "Usuario:"
        '
        'FormDocumentoTransportista_Recepcion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(240, 300)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblUsuarioEnSesion)
        Me.Controls.Add(Me.txtTransportista)
        Me.Controls.Add(Me.lblTransportista)
        Me.Controls.Add(Me.ProgressBar_FormDocumentoTransportista)
        Me.Controls.Add(Me.lblProgressBar_FormDocumentoTransportista)
        Me.Controls.Add(Me.txtDocumento)
        Me.Controls.Add(Me.lblDocumento)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.cmbSiguiente)
        Me.Controls.Add(Me.cmbCancelar)
        Me.Controls.Add(Me.picBarraMorada)
        Me.Controls.Add(Me.cmbSalir)
        Me.Controls.Add(Me.PictureFondoProveedorDeposito_Recepcion)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormDocumentoTransportista_Recepcion"
        Me.Text = "isMOBILE - Recepción"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtTransportista As System.Windows.Forms.TextBox
    Friend WithEvents lblTransportista As System.Windows.Forms.Label
    Friend WithEvents txtDocumento As System.Windows.Forms.TextBox
    Friend WithEvents lblDocumento As System.Windows.Forms.Label
    Friend WithEvents PictureFondoProveedorDeposito_Recepcion As System.Windows.Forms.PictureBox
    Friend WithEvents ProgressBar_FormDocumentoTransportista As System.Windows.Forms.ProgressBar
    Friend WithEvents lblProgressBar_FormDocumentoTransportista As System.Windows.Forms.Label
    Friend WithEvents cmbSalir As System.Windows.Forms.PictureBox
    Friend WithEvents picBarraMorada As System.Windows.Forms.PictureBox
    Friend WithEvents cmbSiguiente As System.Windows.Forms.Button
    Friend WithEvents cmbCancelar As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents lblUsuarioEnSesion As System.Windows.Forms.Label
End Class
