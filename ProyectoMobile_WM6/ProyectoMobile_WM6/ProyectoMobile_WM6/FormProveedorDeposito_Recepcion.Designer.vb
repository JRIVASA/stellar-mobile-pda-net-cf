﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class FormProveedorDeposito_Recepcion
    Inherits System.Windows.Forms.Form

    'Form invalida a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer
    Private mainMenu1 As System.Windows.Forms.MainMenu

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar con el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormProveedorDeposito_Recepcion))
        Me.mainMenu1 = New System.Windows.Forms.MainMenu
        Me.PictureFondoProveedorDeposito_Recepcion = New System.Windows.Forms.PictureBox
        Me.txtProveedor = New System.Windows.Forms.TextBox
        Me.lblProveedor = New System.Windows.Forms.Label
        Me.lblDescripcionProveedor = New System.Windows.Forms.Label
        Me.cmbBuscarProveedor = New System.Windows.Forms.PictureBox
        Me.cmbVerificarProveedor = New System.Windows.Forms.PictureBox
        Me.cmbSiguiente = New System.Windows.Forms.Button
        Me.cmbCancelar = New System.Windows.Forms.Button
        Me.txtDeposito = New System.Windows.Forms.TextBox
        Me.lblDeposito = New System.Windows.Forms.Label
        Me.lblDescripcionDeposito = New System.Windows.Forms.Label
        Me.cmbVerificarDeposito = New System.Windows.Forms.PictureBox
        Me.cmbBuscarDeposito = New System.Windows.Forms.PictureBox
        Me.cmbSalir = New System.Windows.Forms.PictureBox
        Me.picBarraMorada = New System.Windows.Forms.PictureBox
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.lblUsuarioEnSesion = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'PictureFondoProveedorDeposito_Recepcion
        '
        Me.PictureFondoProveedorDeposito_Recepcion.Image = CType(resources.GetObject("PictureFondoProveedorDeposito_Recepcion.Image"), System.Drawing.Image)
        Me.PictureFondoProveedorDeposito_Recepcion.Location = New System.Drawing.Point(42, 0)
        Me.PictureFondoProveedorDeposito_Recepcion.Name = "PictureFondoProveedorDeposito_Recepcion"
        Me.PictureFondoProveedorDeposito_Recepcion.Size = New System.Drawing.Size(198, 38)
        Me.PictureFondoProveedorDeposito_Recepcion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'txtProveedor
        '
        Me.txtProveedor.Font = New System.Drawing.Font("Verdana", 14.0!, System.Drawing.FontStyle.Bold)
        Me.txtProveedor.Location = New System.Drawing.Point(29, 95)
        Me.txtProveedor.MaxLength = 50
        Me.txtProveedor.Name = "txtProveedor"
        Me.txtProveedor.Size = New System.Drawing.Size(122, 29)
        Me.txtProveedor.TabIndex = 11
        '
        'lblProveedor
        '
        Me.lblProveedor.BackColor = System.Drawing.Color.Gainsboro
        Me.lblProveedor.Font = New System.Drawing.Font("Tahoma", 14.0!, System.Drawing.FontStyle.Bold)
        Me.lblProveedor.Location = New System.Drawing.Point(29, 70)
        Me.lblProveedor.Name = "lblProveedor"
        Me.lblProveedor.Size = New System.Drawing.Size(125, 25)
        Me.lblProveedor.Text = "Proveedor"
        '
        'lblDescripcionProveedor
        '
        Me.lblDescripcionProveedor.BackColor = System.Drawing.Color.Gainsboro
        Me.lblDescripcionProveedor.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Regular)
        Me.lblDescripcionProveedor.Location = New System.Drawing.Point(27, 128)
        Me.lblDescripcionProveedor.Name = "lblDescripcionProveedor"
        Me.lblDescripcionProveedor.Size = New System.Drawing.Size(186, 31)
        Me.lblDescripcionProveedor.Text = "Descripción:"
        '
        'cmbBuscarProveedor
        '
        Me.cmbBuscarProveedor.Image = CType(resources.GetObject("cmbBuscarProveedor.Image"), System.Drawing.Image)
        Me.cmbBuscarProveedor.Location = New System.Drawing.Point(191, 95)
        Me.cmbBuscarProveedor.Name = "cmbBuscarProveedor"
        Me.cmbBuscarProveedor.Size = New System.Drawing.Size(28, 29)
        Me.cmbBuscarProveedor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'cmbVerificarProveedor
        '
        Me.cmbVerificarProveedor.Image = CType(resources.GetObject("cmbVerificarProveedor.Image"), System.Drawing.Image)
        Me.cmbVerificarProveedor.Location = New System.Drawing.Point(157, 95)
        Me.cmbVerificarProveedor.Name = "cmbVerificarProveedor"
        Me.cmbVerificarProveedor.Size = New System.Drawing.Size(28, 29)
        Me.cmbVerificarProveedor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'cmbSiguiente
        '
        Me.cmbSiguiente.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.cmbSiguiente.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.cmbSiguiente.ForeColor = System.Drawing.Color.Black
        Me.cmbSiguiente.Location = New System.Drawing.Point(128, 265)
        Me.cmbSiguiente.Name = "cmbSiguiente"
        Me.cmbSiguiente.Size = New System.Drawing.Size(78, 25)
        Me.cmbSiguiente.TabIndex = 13
        Me.cmbSiguiente.Text = "Siguiente"
        '
        'cmbCancelar
        '
        Me.cmbCancelar.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.cmbCancelar.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.cmbCancelar.ForeColor = System.Drawing.Color.Black
        Me.cmbCancelar.Location = New System.Drawing.Point(29, 265)
        Me.cmbCancelar.Name = "cmbCancelar"
        Me.cmbCancelar.Size = New System.Drawing.Size(78, 25)
        Me.cmbCancelar.TabIndex = 23
        Me.cmbCancelar.Text = "Cancelar"
        '
        'txtDeposito
        '
        Me.txtDeposito.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.txtDeposito.Font = New System.Drawing.Font("Verdana", 14.0!, System.Drawing.FontStyle.Bold)
        Me.txtDeposito.Location = New System.Drawing.Point(29, 183)
        Me.txtDeposito.MaxLength = 50
        Me.txtDeposito.Name = "txtDeposito"
        Me.txtDeposito.Size = New System.Drawing.Size(122, 29)
        Me.txtDeposito.TabIndex = 31
        '
        'lblDeposito
        '
        Me.lblDeposito.BackColor = System.Drawing.Color.Gainsboro
        Me.lblDeposito.Font = New System.Drawing.Font("Tahoma", 14.0!, System.Drawing.FontStyle.Bold)
        Me.lblDeposito.Location = New System.Drawing.Point(29, 159)
        Me.lblDeposito.Name = "lblDeposito"
        Me.lblDeposito.Size = New System.Drawing.Size(125, 25)
        Me.lblDeposito.Text = "Depósito"
        '
        'lblDescripcionDeposito
        '
        Me.lblDescripcionDeposito.BackColor = System.Drawing.Color.Gainsboro
        Me.lblDescripcionDeposito.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Regular)
        Me.lblDescripcionDeposito.Location = New System.Drawing.Point(27, 214)
        Me.lblDescripcionDeposito.Name = "lblDescripcionDeposito"
        Me.lblDescripcionDeposito.Size = New System.Drawing.Size(186, 30)
        Me.lblDescripcionDeposito.Text = "Descripción:"
        '
        'cmbVerificarDeposito
        '
        Me.cmbVerificarDeposito.Image = CType(resources.GetObject("cmbVerificarDeposito.Image"), System.Drawing.Image)
        Me.cmbVerificarDeposito.Location = New System.Drawing.Point(157, 183)
        Me.cmbVerificarDeposito.Name = "cmbVerificarDeposito"
        Me.cmbVerificarDeposito.Size = New System.Drawing.Size(28, 29)
        Me.cmbVerificarDeposito.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'cmbBuscarDeposito
        '
        Me.cmbBuscarDeposito.Image = CType(resources.GetObject("cmbBuscarDeposito.Image"), System.Drawing.Image)
        Me.cmbBuscarDeposito.Location = New System.Drawing.Point(191, 183)
        Me.cmbBuscarDeposito.Name = "cmbBuscarDeposito"
        Me.cmbBuscarDeposito.Size = New System.Drawing.Size(28, 29)
        Me.cmbBuscarDeposito.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'cmbSalir
        '
        Me.cmbSalir.Image = CType(resources.GetObject("cmbSalir.Image"), System.Drawing.Image)
        Me.cmbSalir.Location = New System.Drawing.Point(0, 0)
        Me.cmbSalir.Name = "cmbSalir"
        Me.cmbSalir.Size = New System.Drawing.Size(41, 38)
        Me.cmbSalir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'picBarraMorada
        '
        Me.picBarraMorada.Image = CType(resources.GetObject("picBarraMorada.Image"), System.Drawing.Image)
        Me.picBarraMorada.Location = New System.Drawing.Point(0, 258)
        Me.picBarraMorada.Name = "picBarraMorada"
        Me.picBarraMorada.Size = New System.Drawing.Size(240, 42)
        Me.picBarraMorada.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Gainsboro
        Me.PictureBox1.Location = New System.Drawing.Point(0, 56)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(240, 203)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'lblUsuarioEnSesion
        '
        Me.lblUsuarioEnSesion.BackColor = System.Drawing.Color.DarkGray
        Me.lblUsuarioEnSesion.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblUsuarioEnSesion.Location = New System.Drawing.Point(0, 39)
        Me.lblUsuarioEnSesion.Name = "lblUsuarioEnSesion"
        Me.lblUsuarioEnSesion.Size = New System.Drawing.Size(240, 15)
        Me.lblUsuarioEnSesion.Text = "Usuario:"
        '
        'FormProveedorDeposito_Recepcion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(240, 300)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblUsuarioEnSesion)
        Me.Controls.Add(Me.cmbVerificarDeposito)
        Me.Controls.Add(Me.cmbBuscarDeposito)
        Me.Controls.Add(Me.txtDeposito)
        Me.Controls.Add(Me.lblDeposito)
        Me.Controls.Add(Me.lblDescripcionDeposito)
        Me.Controls.Add(Me.cmbVerificarProveedor)
        Me.Controls.Add(Me.cmbBuscarProveedor)
        Me.Controls.Add(Me.txtProveedor)
        Me.Controls.Add(Me.lblProveedor)
        Me.Controls.Add(Me.lblDescripcionProveedor)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.cmbSiguiente)
        Me.Controls.Add(Me.cmbCancelar)
        Me.Controls.Add(Me.picBarraMorada)
        Me.Controls.Add(Me.cmbSalir)
        Me.Controls.Add(Me.PictureFondoProveedorDeposito_Recepcion)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormProveedorDeposito_Recepcion"
        Me.Text = "isMOBILE - Recepción"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PictureFondoProveedorDeposito_Recepcion As System.Windows.Forms.PictureBox
    Friend WithEvents txtProveedor As System.Windows.Forms.TextBox
    Friend WithEvents lblProveedor As System.Windows.Forms.Label
    Friend WithEvents lblDescripcionProveedor As System.Windows.Forms.Label
    Friend WithEvents cmbBuscarProveedor As System.Windows.Forms.PictureBox
    Friend WithEvents cmbVerificarProveedor As System.Windows.Forms.PictureBox
    Friend WithEvents cmbSiguiente As System.Windows.Forms.Button
    Friend WithEvents cmbCancelar As System.Windows.Forms.Button
    Friend WithEvents txtDeposito As System.Windows.Forms.TextBox
    Friend WithEvents lblDeposito As System.Windows.Forms.Label
    Friend WithEvents lblDescripcionDeposito As System.Windows.Forms.Label
    Friend WithEvents cmbVerificarDeposito As System.Windows.Forms.PictureBox
    Friend WithEvents cmbBuscarDeposito As System.Windows.Forms.PictureBox
    Friend WithEvents cmbSalir As System.Windows.Forms.PictureBox
    Friend WithEvents picBarraMorada As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents lblUsuarioEnSesion As System.Windows.Forms.Label
End Class
