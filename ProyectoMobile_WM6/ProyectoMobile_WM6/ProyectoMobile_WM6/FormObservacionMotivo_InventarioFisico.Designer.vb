﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class FormObservacionMotivo_InventarioFisico
    Inherits System.Windows.Forms.Form

    'Form invalida a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer
    Private mainMenu1 As System.Windows.Forms.MainMenu

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar con el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormObservacionMotivo_InventarioFisico))
        Me.mainMenu1 = New System.Windows.Forms.MainMenu
        Me.lblMotivo = New System.Windows.Forms.Label
        Me.cmbAceptar = New System.Windows.Forms.Button
        Me.picBarraMorada = New System.Windows.Forms.PictureBox
        Me.txtObservacion = New System.Windows.Forms.TextBox
        Me.lblObservacion = New System.Windows.Forms.Label
        Me.lblUsuarioEnSesion = New System.Windows.Forms.Label
        Me.cmbSalir = New System.Windows.Forms.PictureBox
        Me.PictureFondoProveedorDeposito_Recepcion = New System.Windows.Forms.PictureBox
        Me.txtMotivo = New System.Windows.Forms.TextBox
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.SuspendLayout()
        '
        'lblMotivo
        '
        Me.lblMotivo.BackColor = System.Drawing.Color.Gainsboro
        Me.lblMotivo.Font = New System.Drawing.Font("Tahoma", 14.0!, System.Drawing.FontStyle.Bold)
        Me.lblMotivo.Location = New System.Drawing.Point(28, 149)
        Me.lblMotivo.Name = "lblMotivo"
        Me.lblMotivo.Size = New System.Drawing.Size(178, 24)
        Me.lblMotivo.Text = "Motivo"
        Me.lblMotivo.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'cmbAceptar
        '
        Me.cmbAceptar.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.cmbAceptar.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.cmbAceptar.ForeColor = System.Drawing.Color.Black
        Me.cmbAceptar.Location = New System.Drawing.Point(78, 265)
        Me.cmbAceptar.Name = "cmbAceptar"
        Me.cmbAceptar.Size = New System.Drawing.Size(78, 25)
        Me.cmbAceptar.TabIndex = 69
        Me.cmbAceptar.Text = "Aceptar"
        '
        'picBarraMorada
        '
        Me.picBarraMorada.Image = CType(resources.GetObject("picBarraMorada.Image"), System.Drawing.Image)
        Me.picBarraMorada.Location = New System.Drawing.Point(0, 259)
        Me.picBarraMorada.Name = "picBarraMorada"
        Me.picBarraMorada.Size = New System.Drawing.Size(240, 40)
        Me.picBarraMorada.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'txtObservacion
        '
        Me.txtObservacion.Font = New System.Drawing.Font("Verdana", 14.0!, System.Drawing.FontStyle.Bold)
        Me.txtObservacion.Location = New System.Drawing.Point(17, 98)
        Me.txtObservacion.MaxLength = 50
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(208, 29)
        Me.txtObservacion.TabIndex = 68
        '
        'lblObservacion
        '
        Me.lblObservacion.BackColor = System.Drawing.Color.Gainsboro
        Me.lblObservacion.Font = New System.Drawing.Font("Tahoma", 14.0!, System.Drawing.FontStyle.Bold)
        Me.lblObservacion.Location = New System.Drawing.Point(28, 65)
        Me.lblObservacion.Name = "lblObservacion"
        Me.lblObservacion.Size = New System.Drawing.Size(178, 24)
        Me.lblObservacion.Text = "Observación"
        Me.lblObservacion.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblUsuarioEnSesion
        '
        Me.lblUsuarioEnSesion.BackColor = System.Drawing.Color.DarkGray
        Me.lblUsuarioEnSesion.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblUsuarioEnSesion.Location = New System.Drawing.Point(0, 39)
        Me.lblUsuarioEnSesion.Name = "lblUsuarioEnSesion"
        Me.lblUsuarioEnSesion.Size = New System.Drawing.Size(240, 15)
        Me.lblUsuarioEnSesion.Text = "Usuario:"
        '
        'cmbSalir
        '
        Me.cmbSalir.Image = CType(resources.GetObject("cmbSalir.Image"), System.Drawing.Image)
        Me.cmbSalir.Location = New System.Drawing.Point(0, 0)
        Me.cmbSalir.Name = "cmbSalir"
        Me.cmbSalir.Size = New System.Drawing.Size(41, 38)
        Me.cmbSalir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'PictureFondoProveedorDeposito_Recepcion
        '
        Me.PictureFondoProveedorDeposito_Recepcion.Image = CType(resources.GetObject("PictureFondoProveedorDeposito_Recepcion.Image"), System.Drawing.Image)
        Me.PictureFondoProveedorDeposito_Recepcion.Location = New System.Drawing.Point(42, 0)
        Me.PictureFondoProveedorDeposito_Recepcion.Name = "PictureFondoProveedorDeposito_Recepcion"
        Me.PictureFondoProveedorDeposito_Recepcion.Size = New System.Drawing.Size(198, 38)
        Me.PictureFondoProveedorDeposito_Recepcion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'txtMotivo
        '
        Me.txtMotivo.Font = New System.Drawing.Font("Verdana", 14.0!, System.Drawing.FontStyle.Bold)
        Me.txtMotivo.Location = New System.Drawing.Point(17, 201)
        Me.txtMotivo.MaxLength = 50
        Me.txtMotivo.Name = "txtMotivo"
        Me.txtMotivo.Size = New System.Drawing.Size(208, 29)
        Me.txtMotivo.TabIndex = 77
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Gainsboro
        Me.PictureBox1.Location = New System.Drawing.Point(0, 56)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(240, 203)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'FormObservacionMotivo_InventarioFisico
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(240, 300)
        Me.ControlBox = False
        Me.Controls.Add(Me.txtMotivo)
        Me.Controls.Add(Me.lblMotivo)
        Me.Controls.Add(Me.txtObservacion)
        Me.Controls.Add(Me.lblObservacion)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.cmbAceptar)
        Me.Controls.Add(Me.picBarraMorada)
        Me.Controls.Add(Me.lblUsuarioEnSesion)
        Me.Controls.Add(Me.cmbSalir)
        Me.Controls.Add(Me.PictureFondoProveedorDeposito_Recepcion)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormObservacionMotivo_InventarioFisico"
        Me.Text = "isMOBILE - Inventario Físico"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblMotivo As System.Windows.Forms.Label
    Friend WithEvents cmbAceptar As System.Windows.Forms.Button
    Friend WithEvents picBarraMorada As System.Windows.Forms.PictureBox
    Friend WithEvents txtObservacion As System.Windows.Forms.TextBox
    Friend WithEvents lblObservacion As System.Windows.Forms.Label
    Friend WithEvents lblUsuarioEnSesion As System.Windows.Forms.Label
    Friend WithEvents cmbSalir As System.Windows.Forms.PictureBox
    Friend WithEvents PictureFondoProveedorDeposito_Recepcion As System.Windows.Forms.PictureBox
    Friend WithEvents txtMotivo As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
End Class
