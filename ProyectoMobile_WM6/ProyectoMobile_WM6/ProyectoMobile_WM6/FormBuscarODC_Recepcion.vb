﻿Public Class FormBuscarODC_Recepcion

    Private Sub DataGridBuscarODC_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridBuscarODC.DoubleClick
        seleccionarODCDataGrid()
    End Sub

    Private Sub cmbAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbAceptar.Click
        seleccionarODCDataGrid()
    End Sub

    Private Sub FormBuscarODC_Recepcion_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblUsuarioEnSesion.Text = "Usuario: " + descripcionUsuario
        Me.Refresh()
        Me.Update()
        AjustarFormularios(Me)
    End Sub

End Class