﻿Imports System.Data.SqlServerCe
Imports System.Net
Imports System.Data.SqlClient
Imports System.Data
Imports System.Data.Common.DbConnection
Imports System.Data.Common.DbCommand
Imports System.Data.Common.DbDataAdapter

Public Class FormConsultaProductos

    Dim FormaCargada As Boolean
    Dim dsCodigos, dsProductos, dsDepoProd, dsPrecios As Data.DataSet
    Dim CodigoProducto As String

    Private Sub FormConsultaProductos_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Activated

        If Not FormaCargada Then

            FormaCargada = True

            'If Not CompatibilidadFueraDeLinea Then
            If conectarDB_SERVIDOR(rutaDB_SERVIDOR) = 0 Then
                MessageBox.Show(MsjSinConexionGenerico)
                Exit Sub
            End If
            'End If

            TmpBDCheck = obtenerRegladeNegocio_Servidor("PDA_Habladores_BD_Consultar", "1")

            If Val(TmpBDCheck) = 1 Then
                'If Not CompatibilidadFueraDeLinea Then
                If conectarDB_SERVIDORPOS(rutaDB_SERVIDOR) = 0 Then
                    MessageBox.Show(MsjSinConexionGenerico)
                    Exit Sub
                End If
                'End If
            End If

        End If

    End Sub

    Private Sub FormConsultaProductos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        CodigoProducto = Nothing

        If CompatibilidadFueraDeLinea Then
            cmbActualizar.Visible = True
        Else
            cmbActualizar.Visible = False
        End If

        lblUsuarioEnSesion.Text = "Usuario: " + descripcionUsuario
        Me.Refresh()
        Me.Update()
        AjustarFormularios(Me)

    End Sub

    Private Sub cmbSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSalir.Click

        LimpiarDatos()

        If Not CompatibilidadFueraDeLinea Then
            cerrarConexion_SERVIDOR()
            cerrarConexion_SERVIDORPOS()
        End If

        formMenuPrincipal.Show()
        Me.Visible = False

    End Sub

    Private Sub codigoBox_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles codigoBox.KeyDown

        If e.KeyValue = Keys.Enter Then

            If codigoBox.Text = "" Then
                MsgBox("Debe introducir un código para realizar la consulta.", MsgBoxStyle.Critical, "isMOBILE")
            Else

                Try

                    CodigoProducto = codigoBox.Text.Trim
                    comandBD_SERVIDOR.CommandText = "SELECT p.C_CODIGO AS codigo, p.C_DESCRI AS descripcion, " + _
                    "d.C_DESCRIPCIO AS departamento, CASE WHEN g.C_DESCRIPCIO IS NULL THEN '' ELSE g.C_DESCRIPCIO END AS grupo, " + _
                    "CASE WHEN s.c_DESCRIPCIO IS NULL THEN '' ELSE s.c_DESCRIPCIO END AS subgrupo, " + _
                    "ROUND(CASE WHEN p.n_impuesto1 = 0 THEN p.n_precio1 ELSE p.n_precio1 * (1 + (p.n_impuesto1 / 100)) END, 2) AS precio " + _
                    "FROM MA_PRODUCTOS p LEFT JOIN MA_CODIGOS c " + _
                    "ON p.C_CODIGO = c.c_codnasa " + _
                    "LEFT JOIN MA_DEPARTAMENTOS d " + _
                    "ON p.c_departamento = d.C_CODIGO " + _
                    "LEFT JOIN MA_GRUPOS g " + _
                    "ON p.c_grupo = g.c_CODIGO AND p.c_departamento = g.c_departamento " + _
                    "LEFT JOIN MA_SUBGRUPOS s " + _
                    "ON p.c_subgrupo = s.c_CODIGO AND p.c_grupo = s.c_in_grupo AND p.c_departamento = s.c_in_departamento " + _
                    "WHERE c.c_codigo = @codigo "
                    comandBD_SERVIDOR.Parameters.AddWithValue("@codigo", codigoBox.Text.Trim)
                    dsProductos = executeQuery_Servidor(comandBD_SERVIDOR)

                    If dsProductos.Tables(0).Rows.Count = 0 Then
                        MsgBox("El producto no existe.", MsgBoxStyle.Critical, "isMOBILE")
                    Else
                        cdescriLabel.Text = dsProductos.Tables(0).Rows(0)("descripcion")
                        cdepartamentoLabel.Text = dsProductos.Tables(0).Rows(0)("departamento")
                        cgrupoLabel.Text = dsProductos.Tables(0).Rows(0)("grupo")
                        csubgrupoLabel.Text = dsProductos.Tables(0).Rows(0)("subgrupo")
                        precioLabel.Text = dsProductos.Tables(0).Rows(0)("precio")
                    End If

                Catch ex As Exception
                    MsgBox(ex.Message)
                Finally
                    codigoBox.Text = ""
                End Try

            End If

        End If

    End Sub

    Private Sub existenciaButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles existenciaButton.Click

        Me.Enabled = False

        If IsNothing(CodigoProducto) Then
            MsgBox("Debe seleccionar un código y presionar la tecla enter", MsgBoxStyle.Information)
        Else
            buscarProductoDepoProd()
            FormExistencias.Show()
        End If

        Me.Enabled = True

    End Sub

    Private Sub buscarProductoUbicaciones()

        Try

            comandBD_SERVIDOR.CommandText = "SELECT MAX(Linea) AS Linea, MAX(CodDeposito) AS CodDeposito, MAX(Deposito) AS Deposito, MAX(Ubicacion) AS Ubicacion, MAX(Mascara) AS Mascara FROM(" & vbNewLine & _
            "SELECT 0 AS Linea, DEP.c_CodDeposito AS CodDeposito, DEP.c_Descripcion AS Deposito, '' AS Ubicacion, DEP.cu_Mascara AS Mascara FROM MA_DEPOSITO DEP" & vbNewLine & _
            "WHERE DEP.c_CodLocalidad = @Localidad AND LTRIM(DEP.cu_Mascara) <> ''" & vbNewLine & _
            "UNION" & vbNewLine & _
            "SELECT n_Linea AS Linea, cu_Deposito AS CodDeposito, '' AS Deposito, cu_Mascara AS Ubicacion, '' AS Mascara FROM MA_UBICACIONxPRODUCTO UBC" & vbNewLine & _
            "WHERE UBC.cu_Producto = @Codigo" & vbNewLine & _
            ") TB GROUP BY CodDeposito"

            comandBD_SERVIDOR.Parameters.AddWithValue("@Codigo", codigoProducto)
            comandBD_SERVIDOR.Parameters.AddWithValue("@Localidad", Localidad)

            dsDepoProd = executeQuery_Servidor(comandBD_SERVIDOR)

            FormUbicaciones.CodigoProducto = Me.CodigoProducto

            Dim Tmp As DataGrid
            Tmp = FormUbicaciones.GridUbicaciones
            Tmp.DataSource = dsDepoProd.Tables(0)
            Tmp.PreferredRowHeight = Fix(35 * CurrentHeightFactor)

            If dsDepoProd.Tables(0).Rows.Count = 0 Then
                MsgBox("Verifique que existan Depósitos en la Localidad seleccionada.Estos deben poseer una Máscara de Ubicación definida.", MsgBoxStyle.Critical, "isMOBILE")
            Else

                Tmp.TableStyles.Clear()

                Dim TableStyle As DataGridTableStyle = New DataGridTableStyle()

                TableStyle.MappingName = dsDepoProd.Tables(0).TableName

                For Each Item As DataColumn In dsDepoProd.Tables(0).Columns

                    Dim TbcName As DataGridTextBoxColumn = New DataGridTextBoxColumn()
                    TbcName.MappingName = Item.ColumnName
                    TbcName.HeaderText = Item.ColumnName
                    TableStyle.GridColumnStyles.Add(TbcName)

                Next

                Tmp.TableStyles.Add(TableStyle)

            End If
        Catch ex As Exception
            MsgBox("Error al cargar la información: " & ex.Message, MsgBoxStyle.Critical, "isMOBILE")
        End Try

    End Sub

    Private Sub buscarProductoDepoProd()
        Try
            comandBD_SERVIDOR.CommandText = "SELECT d.c_coddeposito AS Deposito, dep.c_descripcion AS Descripcion, d.n_cantidad AS Disponible, d.n_cant_ordenada AS Ordenado " _
            + " FROM MA_DEPOPROD d LEFT JOIN MA_CODIGOS c ON d.c_codarticulo = c.c_codnasa LEFT JOIN MA_DEPOSITO dep ON d.c_coddeposito = dep.c_coddeposito WHERE c.c_codigo = @codigo"
            comandBD_SERVIDOR.Parameters.AddWithValue("@codigo", codigoProducto)
            dsDepoProd = executeQuery_Servidor(comandBD_SERVIDOR)

            If dsDepoProd.Tables(0).Rows.Count = 0 Then
                MsgBox("El producto no se encuentra registrado en ningún almacén.", MsgBoxStyle.Critical, "isMOBILE")
            Else
                FormExistencias.gridExistencia.DataSource = dsDepoProd.Tables(0)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "isMOBILE")
        End Try
    End Sub

    Private Sub buscarCodigosAlternos()
        Try
            comandBD_SERVIDOR.CommandText = "SELECT c_codigo AS Codigo, c_descripcion AS Descripcion, n_cantidad AS Cant, " + _
            "nu_intercambio AS EDI, nu_tipoprecio AS Precio FROM MA_CODIGOS " + _
            "WHERE c_codnasa IN (" + _
            "SELECT c_codnasa FROM MA_CODIGOS WHERE c_codigo = @codigo " + _
            ")"
            comandBD_SERVIDOR.Parameters.AddWithValue("@codigo", codigoProducto)
            dsCodigos = executeQuery_Servidor(comandBD_SERVIDOR)

            If dsCodigos.Tables(0).Rows.Count = 0 Then
                MsgBox("No se encontraron registros asociados a ese código.", MsgBoxStyle.Critical, "isMOBILE")
            Else
                formCodigosAlternos.gridCodigos.DataSource = dsCodigos.Tables(0)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "isMOBILE")
        End Try
    End Sub

    Private Sub buscarPrecios()

        Try

            comandBD_SERVIDOR.CommandText = "SELECT p.n_precio1 AS Precio1, p.n_precio2 AS Precio2, p.n_precio3 AS Precio3, " + _
            "p.n_precioO AS PrecioOferta, p.n_impuesto1 AS Impuesto " + _
            "FROM MA_PRODUCTOS p INNER JOIN MA_CODIGOS c " + _
            "ON p.C_CODIGO = c.c_codnasa WHERE c.C_CODIGO = @codigo "
            comandBD_SERVIDOR.Parameters.AddWithValue("@codigo", CodigoProducto)

            If Val(TmpBDCheck) = 1 Then
                dsPrecios = executeQuery_Servidor(comandBD_SERVIDOR, conexionBD_SERVIDORPOS)
            Else
                dsPrecios = executeQuery_Servidor(comandBD_SERVIDOR)
            End If

            If dsPrecios.Tables(0).Rows.Count = 0 Then
                MsgBox("No se encontraron registros asociados a ese código.", MsgBoxStyle.Information, "isMOBILE")
            Else
                formPrecios.gridPrecios.DataSource = dsPrecios.Tables(0)
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "isMOBILE")
        End Try

    End Sub

    Private Sub codigosButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles codigosButton.Click
        Me.Enabled = False
        If IsNothing(codigoProducto) Then
            MsgBox("Debe seleccionar un código y presionar la tecla enter", MsgBoxStyle.Information, "isMOBILE")
        Else
            buscarCodigosAlternos()
            formCodigosAlternos.Show()
        End If
        Me.Enabled = True
    End Sub

    Private Sub preciosButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles preciosButton.Click
        Me.Enabled = False
        If IsNothing(codigoProducto) Then
            MsgBox("Debe seleccionar un código y presionar la tecla enter", MsgBoxStyle.Information, "isMOBILE")
        Else
            buscarPrecios()
            formPrecios.Show()
        End If
        Me.Enabled = True
    End Sub

    Private Sub LimpiarDatos()

        codigoBox.Text = ""
        cdescriLabel.Text = ""
        cdepartamentoLabel.Text = ""
        cgrupoLabel.Text = ""
        csubgrupoLabel.Text = ""
        precioLabel.Text = ""

        formPrecios.gridPrecios.DataSource = Nothing
        formCodigosAlternos.gridCodigos.DataSource = Nothing
        FormExistencias.gridExistencia.DataSource = Nothing

    End Sub

    Private Sub cmbActualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbActualizar.Click
        If ActualizacionParcial("Productos") Then
            MsgBox("Actualización parcial de Productos completada.", MsgBoxStyle.Information, "isMOBILE")
        Else
            MsgBox("No se pudo realizar la Actualización parcial de Productos.", MsgBoxStyle.Information, "isMOBILE")
        End If
    End Sub

    Private Sub cdescriLabel_ParentChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cdescriLabel.ParentChanged

    End Sub

    Private Sub lugarButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lugarButton.Click

        Me.Enabled = False

        If IsNothing(CodigoProducto) Then
            MsgBox("Debe seleccionar un código y presionar la tecla enter", MsgBoxStyle.Information)
        Else

            buscarProductoUbicaciones()

            Dim CantRows As Integer = FormUbicaciones.GridUbicaciones.BindingContext(FormUbicaciones.GridUbicaciones.DataSource).Count

            If CantRows > 0 Then

                FormUbicaciones.GridUbicaciones.TableStyles(0).GridColumnStyles(0).Width = 0
                FormUbicaciones.GridUbicaciones.TableStyles(0).GridColumnStyles(1).Width = 0

                If CantRows > 3 Then
                    FormUbicaciones.GridUbicaciones.TableStyles(0).GridColumnStyles(2).Width = Fix(85 * CurrentWidthFactor)
                    FormUbicaciones.GridUbicaciones.TableStyles(0).GridColumnStyles(3).Width = Fix(57 * CurrentWidthFactor)
                    FormUbicaciones.GridUbicaciones.TableStyles(0).GridColumnStyles(4).Width = Fix(57 * CurrentWidthFactor)
                Else
                    FormUbicaciones.GridUbicaciones.TableStyles(0).GridColumnStyles(2).Width = Fix(95 * CurrentWidthFactor)
                    FormUbicaciones.GridUbicaciones.TableStyles(0).GridColumnStyles(3).Width = Fix(58 * CurrentWidthFactor)
                    FormUbicaciones.GridUbicaciones.TableStyles(0).GridColumnStyles(4).Width = Fix(58 * CurrentWidthFactor)
                End If

                FormUbicaciones.Show()

            End If

        End If

        Me.Enabled = True

    End Sub

    Private Sub codigoBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles codigoBox.TextChanged

    End Sub

End Class