﻿Public Class FormLecturaProducto_Picking

    Public FormaCargada As Boolean ' No se por que ocurre el evento Activate al salir... es ilógico. Pero bueno... lo resolveré desde afuera.
    Private CambioCantidad As Boolean

    Public CantDecimalesProducto As Integer

    Private Sub FormLecturaProducto_Picking_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Activated

        'If DebugMode Then MsgBox("Ctl:" & Me.Controls.Count.ToString)
        'If DebugMode Then MsgBox("Timer:" & (TimerReload Is Nothing).ToString)

        If Me.Controls.Count > 0 Then

            TimerReload.Interval = 100
            TimerReload.Enabled = False
            TimerReload.Enabled = True

        End If

    End Sub

    Private Sub ColorAlertaFinalizar()

        If Not IsNumeric(txtCantRecolectada.Text) Then Exit Sub
        If Not IsNumeric(txtCantRecolectada.Tag) Then Exit Sub

        If CDbl(txtCantRecolectada.Tag) < CDbl(txtCantSolicitada.Text) Then
            cmbFinalizar.BackColor = Color.Chocolate
        Else
            cmbFinalizar.BackColor = Color.LightGreen
        End If

    End Sub

    Private Sub FormLecturaProducto_Picking_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        AjustarFormularios(Me)
    End Sub

    Private Sub cmbAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbAgregar.Click

        If IsNumeric(txtCantRecolectada.Text) Then

            Dim NewCant As Double, CantSolicitada As Double, CantRecolectada As Double

            CantSolicitada = CDbl(txtCantSolicitada.Text)

            If Not IsNumeric(txtCantRecolectada.Tag) Then txtCantRecolectada.Tag = "0"

            CantRecolectada = CDbl(txtCantRecolectada.Tag)

            NewCant = CDbl(txtCantRecolectada.Text)

            NewCant = Math.Round(NewCant, CantDecimalesProducto)

            If NewCant > CantSolicitada Then
                MsgBox("La cantidad recolectada no puede ser mayor a la cantidad solicitada.")
                Exit Sub
            End If

            If CantRecolectada > NewCant Then

                Dim Resp As MsgBoxResult = MsgBox("La cantidad actual de productos recolectados es de " & _
                CantRecolectada & " ¿Seguro que desea disminuirla a " & NewCant & "?", MsgBoxStyle.OkCancel)

                If Resp = MsgBoxResult.Cancel Then Exit Sub

            End If

            If GuardarPickingUser(CStr(datasetUsuario.Tables(0).Rows(0)("codusuario")), lblDescripcion.Tag, CantDecimalesProducto, CantSolicitada, NewCant) Then
                txtCantRecolectada.Tag = txtCantRecolectada.Text
                MsgBox("Cantidad guardada")
                CambioCantidad = False
                ColorAlertaFinalizar()
            End If

        Else
            MsgBox("Entrada inválida en la Cantidad a Recolectar. Por favor verifique que sea un número")
        End If

    End Sub

    Private Sub cmbSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSalir.Click
        CambioCantidad = False
        FormaCargada = False
        FormPickingUserMain.Show()
        Me.Visible = False
    End Sub

    Private Sub cmbFinalizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbFinalizar.Click

        If CambioCantidad Then
            MsgBox("No es posible finalizar debido a que cambió la cantidad pero aún no ha guardado el cambio. pulse Guardar Cantidad primero o si desea desechar el cambio salga y seleccione el producto nuevamente.")
            Exit Sub
        End If

        If Not IsNumeric(txtCantSolicitada.Text) Then txtCantSolicitada.Text = 0
        If Not IsNumeric(txtCantRecolectada.Tag) Then txtCantRecolectada.Tag = 0

        'If CDbl(txtCantRecolectada.Tag) < CDbl(txtCantSolicitada.Text) Then
        'Dim Resp As MsgBoxResult = MsgBox("No ha recolectado la totalidad de la cantidad solicitada, ¿Está seguro de finalizar este producto con cantidad faltante?", MsgBoxStyle.OkCancel)
        'If Resp = MsgBoxResult.Cancel Then Exit Sub
        ''Else
        ''cmbFinalizar.BackColor = Color.LightGreen
        'End If
        ' Comentado.... se estaba preguntando 2 veces. La idea es que sea rápido.

        Dim CantSolicitada As Double, CantRecolectada As Double, _
        CantRestante As Double, FinalizarCantidadCero As Boolean

        CantSolicitada = CDbl(txtCantSolicitada.Text)

        CantRecolectada = CDbl(txtCantRecolectada.Tag)

        CantRecolectada = Math.Round(CantRecolectada, CantDecimalesProducto)

        CantRestante = Math.Round((CantSolicitada - CantRecolectada), CantDecimalesProducto)

        If CantRecolectada = 0 Then
            Application.DoEvents()
            Dim Resp As MsgBoxResult = MsgBox("¿Está seguro de finalizar la recolección de este producto con una cantidad igual a cero (0)?", MsgBoxStyle.YesNo)
            Application.DoEvents()
            If Resp = MsgBoxResult.No Then Exit Sub
            FinalizarCantidadCero = True
        End If

        If (CantRecolectada < CantSolicitada) And Not FinalizarCantidadCero Then
            Application.DoEvents()
            Dim Resp As MsgBoxResult = MsgBox("¿Está seguro de finalizar la recolección de este producto con una cantidad faltante de productos? (Falta(n) " & CantRestante & ")", MsgBoxStyle.YesNo)
            Application.DoEvents()
            If Resp = MsgBoxResult.No Then Exit Sub
        End If

        If FinalizarPickingUser(CStr(datasetUsuario.Tables(0).Rows(0)("codusuario")), lblDescripcion.Tag, CantDecimalesProducto, CantSolicitada, CantRecolectada, FinalizarCantidadCero) Then
            cmbSalir_Click(Nothing, Nothing)
        End If

    End Sub

    Private Sub txtCantRecolectada_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCantRecolectada.GotFocus

        txtCantRecolectada.Enabled = False

        FormNumPadGeneral.ValorOriginal = txtCantRecolectada.Text
        Dim A = FormNumPadGeneral.ShowDialog()

        Dim NewCant = Math.Round(FormNumPadGeneral.ValorNumerico, CantDecimalesProducto)
        txtCantRecolectada.Text = FormatNumber(NewCant, CantDecimalesProducto)

        CambioCantidad = True

        'ColorAlertaFinalizar()

        txtCantSolicitada.Focus()
        cmbAgregar.Focus()

        txtCantRecolectada.Enabled = True

        Me.Activate()
        Me.BringToFront()

    End Sub

    Private Sub txtCantRecolectada_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCantRecolectada.KeyPress
        txtCantRecolectada_GotFocus(Nothing, Nothing)
    End Sub

    Private Sub CmbRecTodo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmbRecTodo.Click
        txtCantRecolectada.Text = txtCantSolicitada.Text
        CambioCantidad = True
    End Sub

    Private Sub Activation()

        If Not FormaCargada Then

            FormaCargada = True

            txtCantRecolectada.Tag = txtCantRecolectada.Text

            'If DebugMode Then MsgBox("Activando..." & vbNewLine & "txtCantRecolectada.Text:" & txtCantRecolectada.Text & vbNewLine & "txtCantRecolectada.Tag:" & txtCantRecolectada.Tag.ToString & vbNewLine & "txtCantSolicitada.Text:" & txtCantSolicitada.Text)

            ColorAlertaFinalizar()

        End If

    End Sub

    Private Sub TimerReload_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TimerReload.Tick
        TimerReload.Enabled = False
        Activation()
    End Sub

End Class