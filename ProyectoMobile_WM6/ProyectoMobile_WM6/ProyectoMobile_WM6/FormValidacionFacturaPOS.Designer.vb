﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class FormValidacionFacturaPOS
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormValidacionFacturaPOS))
        Me.picBarraMorada = New System.Windows.Forms.PictureBox
        Me.pictureFondo = New System.Windows.Forms.PictureBox
        Me.cmbSalir = New System.Windows.Forms.PictureBox
        Me.lblUsuarioEnSesion = New System.Windows.Forms.Label
        Me.txtDocumento = New System.Windows.Forms.TextBox
        Me.lblDocumento = New System.Windows.Forms.Label
        Me.PicValidacion = New System.Windows.Forms.PictureBox
        Me.lblResultInfoL1 = New System.Windows.Forms.Label
        Me.lblResultInfoL2 = New System.Windows.Forms.Label
        Me.cmbValidar = New System.Windows.Forms.PictureBox
        Me.ResultShowTime = New System.Windows.Forms.Timer
        Me.TimerReload = New System.Windows.Forms.Timer
        Me.SuspendLayout()
        '
        'picBarraMorada
        '
        Me.picBarraMorada.Image = CType(resources.GetObject("picBarraMorada.Image"), System.Drawing.Image)
        Me.picBarraMorada.Location = New System.Drawing.Point(0, 260)
        Me.picBarraMorada.Name = "picBarraMorada"
        Me.picBarraMorada.Size = New System.Drawing.Size(240, 40)
        Me.picBarraMorada.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'pictureFondo
        '
        Me.pictureFondo.Image = CType(resources.GetObject("pictureFondo.Image"), System.Drawing.Image)
        Me.pictureFondo.Location = New System.Drawing.Point(42, 0)
        Me.pictureFondo.Name = "pictureFondo"
        Me.pictureFondo.Size = New System.Drawing.Size(198, 38)
        Me.pictureFondo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'cmbSalir
        '
        Me.cmbSalir.Image = CType(resources.GetObject("cmbSalir.Image"), System.Drawing.Image)
        Me.cmbSalir.Location = New System.Drawing.Point(0, 0)
        Me.cmbSalir.Name = "cmbSalir"
        Me.cmbSalir.Size = New System.Drawing.Size(41, 38)
        Me.cmbSalir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'lblUsuarioEnSesion
        '
        Me.lblUsuarioEnSesion.BackColor = System.Drawing.Color.DarkGray
        Me.lblUsuarioEnSesion.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblUsuarioEnSesion.Location = New System.Drawing.Point(0, 39)
        Me.lblUsuarioEnSesion.Name = "lblUsuarioEnSesion"
        Me.lblUsuarioEnSesion.Size = New System.Drawing.Size(240, 15)
        Me.lblUsuarioEnSesion.Text = "Usuario:"
        '
        'txtDocumento
        '
        Me.txtDocumento.BackColor = System.Drawing.Color.White
        Me.txtDocumento.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.txtDocumento.Location = New System.Drawing.Point(97, 59)
        Me.txtDocumento.MaxLength = 20
        Me.txtDocumento.Name = "txtDocumento"
        Me.txtDocumento.Size = New System.Drawing.Size(111, 23)
        Me.txtDocumento.TabIndex = 73
        '
        'lblDocumento
        '
        Me.lblDocumento.BackColor = System.Drawing.Color.Gainsboro
        Me.lblDocumento.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.lblDocumento.Location = New System.Drawing.Point(6, 63)
        Me.lblDocumento.Name = "lblDocumento"
        Me.lblDocumento.Size = New System.Drawing.Size(91, 19)
        Me.lblDocumento.Text = "Documento:"
        '
        'PicValidacion
        '
        Me.PicValidacion.Image = CType(resources.GetObject("PicValidacion.Image"), System.Drawing.Image)
        Me.PicValidacion.Location = New System.Drawing.Point(97, 98)
        Me.PicValidacion.Name = "PicValidacion"
        Me.PicValidacion.Size = New System.Drawing.Size(45, 40)
        Me.PicValidacion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'lblResultInfoL1
        '
        Me.lblResultInfoL1.BackColor = System.Drawing.Color.Gainsboro
        Me.lblResultInfoL1.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblResultInfoL1.Location = New System.Drawing.Point(25, 155)
        Me.lblResultInfoL1.Name = "lblResultInfoL1"
        Me.lblResultInfoL1.Size = New System.Drawing.Size(192, 15)
        Me.lblResultInfoL1.Text = "El documento"
        Me.lblResultInfoL1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblResultInfoL2
        '
        Me.lblResultInfoL2.BackColor = System.Drawing.Color.Gainsboro
        Me.lblResultInfoL2.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblResultInfoL2.Location = New System.Drawing.Point(25, 175)
        Me.lblResultInfoL2.Name = "lblResultInfoL2"
        Me.lblResultInfoL2.Size = New System.Drawing.Size(192, 15)
        Me.lblResultInfoL2.Text = "es válido | ya existe"
        Me.lblResultInfoL2.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'cmbValidar
        '
        Me.cmbValidar.Image = CType(resources.GetObject("cmbValidar.Image"), System.Drawing.Image)
        Me.cmbValidar.Location = New System.Drawing.Point(211, 59)
        Me.cmbValidar.Name = "cmbValidar"
        Me.cmbValidar.Size = New System.Drawing.Size(23, 23)
        Me.cmbValidar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'ResultShowTime
        '
        Me.ResultShowTime.Interval = 1500
        '
        'TimerReload
        '
        Me.TimerReload.Interval = 1250
        '
        'FormValidacionFacturaPOS
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.Gainsboro
        Me.ClientSize = New System.Drawing.Size(240, 300)
        Me.ControlBox = False
        Me.Controls.Add(Me.cmbValidar)
        Me.Controls.Add(Me.lblResultInfoL2)
        Me.Controls.Add(Me.lblResultInfoL1)
        Me.Controls.Add(Me.PicValidacion)
        Me.Controls.Add(Me.txtDocumento)
        Me.Controls.Add(Me.lblDocumento)
        Me.Controls.Add(Me.lblUsuarioEnSesion)
        Me.Controls.Add(Me.cmbSalir)
        Me.Controls.Add(Me.picBarraMorada)
        Me.Controls.Add(Me.pictureFondo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormValidacionFacturaPOS"
        Me.Text = "isMOBILE - Consulta"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents picBarraMorada As System.Windows.Forms.PictureBox
    Friend WithEvents pictureFondo As System.Windows.Forms.PictureBox
    Friend WithEvents cmbSalir As System.Windows.Forms.PictureBox
    Friend WithEvents lblUsuarioEnSesion As System.Windows.Forms.Label
    Friend WithEvents txtDocumento As System.Windows.Forms.TextBox
    Friend WithEvents lblDocumento As System.Windows.Forms.Label
    Friend WithEvents PicValidacion As System.Windows.Forms.PictureBox
    Friend WithEvents lblResultInfoL1 As System.Windows.Forms.Label
    Friend WithEvents lblResultInfoL2 As System.Windows.Forms.Label
    Friend WithEvents cmbValidar As System.Windows.Forms.PictureBox
    Friend WithEvents ResultShowTime As System.Windows.Forms.Timer
    Friend WithEvents TimerReload As System.Windows.Forms.Timer
End Class
