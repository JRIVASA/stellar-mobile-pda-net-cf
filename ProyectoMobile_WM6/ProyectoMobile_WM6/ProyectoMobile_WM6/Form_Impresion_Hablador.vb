﻿Imports System.Data
Imports System.Data.SqlServerCe
Imports System.Data.SqlClient

Imports InTheHand.Net
Imports InTheHand.Net.Bluetooth
Imports InTheHand.Net.Ports

Public Class Form_Impresion_Hablador

    Dim FormaCargada As Boolean

    Private dsMonedaPredeterminada As DataSet

    Private Sub Form_Impresion_Hablador_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Activated

        If Not FormaCargada Then

            FormaCargada = True

            'If Not CompatibilidadFueraDeLinea Then
            If conectarDB_SERVIDOR(rutaDB_SERVIDOR) = 0 Then
                MessageBox.Show(MsjSinConexionGenerico)
                Exit Sub
            End If
            'End If

            TmpBDCheck = obtenerRegladeNegocio_Servidor("PDA_Habladores_BD_Consultar", "1")

            'If Val(TmpBDCheck) = 1 Then
            If Not CompatibilidadFueraDeLinea Then
                If conectarDB_SERVIDORPOS(rutaDB_SERVIDOR) = 0 Then
                    MessageBox.Show(MsjSinConexionGenerico)
                    Exit Sub
                End If
            End If
            'End If

            Dim dsMonedaPredeterminada As DataSet, dsRDN As DataSet

            comandBD_SERVIDOR.CommandText = "SELECT * FROM MA_MONEDAS WHERE b_Preferencia = 1"
            dsMonedaPredeterminada = executeQuery_Servidor(comandBD_SERVIDOR)

            If dsMonedaPredeterminada.Tables(0).Rows.Count > 0 Then
                Moneda_Cod = dsMonedaPredeterminada.Tables(0).Rows(0)("c_CodMoneda")
                Moneda_Des = dsMonedaPredeterminada.Tables(0).Rows(0)("c_Descripcion")
                Moneda_Sim = dsMonedaPredeterminada.Tables(0).Rows(0)("c_Simbolo")
                Moneda_Fac = dsMonedaPredeterminada.Tables(0).Rows(0)("n_Factor")
                Moneda_Dec = dsMonedaPredeterminada.Tables(0).Rows(0)("n_Decimales")
            End If

            dsRDN = consultarRegladeNegocio("Hbl_MonedaAlternaPrecios")

            If dsRDN.Tables(0).Rows.Count > 0 Then
                Hablador_MonedaAlternaParaPrecios = dsRDN.Tables(0).Rows(0)("Valor")
            End If

        End If

    End Sub

    Private Sub Form_Impresion_Hablador_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Nombre de usuario
        lblUsuarioEnSesion.Text = "Usuario: " + descripcionUsuario

        Me.Refresh()
        Me.Update()

        AjustarFormularios(Me)

        If Me.Datagrid_Impresion.VisibleRowCount = 0 Then
            cmbImprimir_hablador.Visible = False
        End If

    End Sub

    Private Sub MostrarSeleccionImpresora()

        If (Not Impresora_WLAN_IP Is Nothing And Not Impresora_Bluetooth_Port = "") Then
            Me.CmbSeleccionarImpresora.Visible = True
        Else
            Me.CmbSeleccionarImpresora.Visible = False
        End If

    End Sub

    Private Sub cmbSalir_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSalir_hablador.Click

        If Not CompatibilidadFueraDeLinea Then
            cerrarConexion_SERVIDOR()
            cerrarConexion_SERVIDORPOS()
        End If

        FormaCargada = False
        formMenuPrincipal.Show()
        Datagrid_Impresion.DataSource = Nothing
        cmbImprimir_hablador.Visible = False
        Me.Visible = False

        Formbusqueda_productos.txtproductos.Text = ""
        Formbusqueda_productos.DataGridbusqueda_productos.DataSource = Nothing

    End Sub

    Private Sub cmbImprimir_hablador_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbImprimir_hablador.Click

        'Dim TestIni As Date = Now
        'Dim TestEnd As Date = Now

        If ImpHblEti_BusquedaActiva Then
            ImpHblEti_BusquedaActiva = False
            Exit Sub
        End If

        Me.Enabled = False

        Try

            lblStatus.Text = "Por favor espere..."
            lblStatus.Refresh()

            'Datos sobre la ip, puerto de la impresora
            'Y carpeta raiz de la etiqueta o hablador

            'Dim IpAddress As String = moduloFunciones.ip_impresora 
            'Ya no usamos el String sino System.Net.IPAddress para evitar lentitud.

            Dim Num_Impresiones As Integer = numero_impresiones.Value
            Dim N As Integer
            Dim CodigoEntrada As String
            Dim Codigo As String
            Dim Datos As New Collection

            CodigoEntrada = CStr(Me.Datagrid_Impresion.Item(0, 0))

            'TestIni = Now

            Codigo = obtenerCodigoPrincipal(CodigoEntrada)

            'TestEnd = Now

            'MsgBox(DateDiff(DateInterval.Second, TestIni, TestEnd, FirstDayOfWeek.Monday, FirstWeekOfYear.System), MsgBoxStyle.Information)

            Dim Lector_Consulta As SqlDataReader

            'TestIni = Now

            Dim nPrecioItem As String = NPrecio_Contado()

            comandBD_SERVIDOR.Parameters.Clear()
            comandBD_SERVIDOR.CommandText = "SELECT C_CODIGO, C_DESCRI, CU_DESCRIPCION_CORTA, N_PRECIOO, " & _
            "" & NPrecio_Contado() & ", N_IMPUESTO1, N_IMPUESTO2, N_IMPUESTO3, N_CANTIBUL, c_Presenta, c_CodMoneda, " & _
            "Text1, Text2, Text3, Text4, Text5, Text6, Text7, Text8, " & _
            "f_Inicial, f_Final, h_Inicial, h_Final " & _
            "FROM MA_PRODUCTOS WHERE c_Codigo = @Cod"

            comandBD_SERVIDOR.Parameters.Add("@Cod", Codigo)

            If Val(TmpBDCheck) = 1 Then
                conectarDB_SERVIDORPOS(rutaDB_SERVIDOR)
                comandBD_SERVIDOR.Connection = conexionBD_SERVIDORPOS
            Else
                conectarDB_SERVIDOR(rutaDB_SERVIDOR)
                comandBD_SERVIDOR.Connection = conexionBD_SERVIDOR
            End If

            Lector_Consulta = comandBD_SERVIDOR.ExecuteReader

            If Lector_Consulta.Read Then

                Dim A As Object

                A = Lector_Consulta.Item("C_CODIGO")
                Datos.Add(A, "Codigo")
                A = Lector_Consulta.Item("C_DESCRI")
                Datos.Add(A, "Descripcion")
                A = Lector_Consulta.Item("CU_DESCRIPCION_CORTA")

                If CStr(A) <> "" Then
                    Datos.Add(A, "DesCorta")
                Else
                    Datos.Add(Datos.Item("Descripcion"), "DesCorta")
                End If

                A = Lector_Consulta.Item(nPrecioItem)
                Datos.Add(A, "Precio")
                A = Lector_Consulta.Item("N_PRECIOO")
                Datos.Add(A, "PrecioOferta")
                A = Lector_Consulta.Item("N_IMPUESTO1")
                Datos.Add(A, "Imp1")
                A = Lector_Consulta.Item("N_IMPUESTO2")
                Datos.Add(A, "Imp2")
                A = Lector_Consulta.Item("N_IMPUESTO3")
                Datos.Add(A, "Imp3")
                A = Lector_Consulta.Item("N_CANTIBUL")
                Datos.Add(A, "CantEmp")
                A = Lector_Consulta.Item("c_Presenta")
                Datos.Add(A, "Presentacion")

                A = Lector_Consulta.Item("Text1")
                Datos.Add(A, "Text1")
                A = Lector_Consulta.Item("Text2")
                Datos.Add(A, "Text2")
                A = Lector_Consulta.Item("Text3")
                Datos.Add(A, "Text3")
                A = Lector_Consulta.Item("Text4")
                Datos.Add(A, "Text4")
                A = Lector_Consulta.Item("Text5")
                Datos.Add(A, "Text5")
                A = Lector_Consulta.Item("Text6")
                Datos.Add(A, "Text6")
                A = Lector_Consulta.Item("Text7")
                Datos.Add(A, "Text7")
                A = Lector_Consulta.Item("Text8")
                Datos.Add(A, "Text8")

                A = Lector_Consulta.Item("c_CodMoneda")
                Datos.Add(A, "c_CodMoneda")

                A = Lector_Consulta.Item("f_Inicial")
                Datos.Add(A, "f_Inicial")
                A = Lector_Consulta.Item("h_Inicial")
                Datos.Add(A, "h_Inicial")
                A = Lector_Consulta.Item("f_Final")
                Datos.Add(A, "f_Final")
                A = Lector_Consulta.Item("h_Final")
                Datos.Add(A, "h_Final")

                'MsgBox(Datos.Item(1) & " - " & Datos.Item("Codigo") & " - " & CStr(Datos.Item("Codigo")))
                'MsgBox(Datos.Item(2) & " - " & Datos.Item("Descripcion") & " - " & CStr(Datos.Item("Descripcion")))
                'MsgBox(Datos.Item(3) & " - " & Datos.Item("DesCorta") & " - " & CStr(Datos.Item("DesCorta")))
                'MsgBox(Datos.Item(4) & " - " & Datos.Item("Precio") & " - " & CDbl(Datos.Item("Precio")))
                'MsgBox(Datos.Item(5) & " - " & Datos.Item("PrecioOferta") & " - " & CDbl(Datos.Item("PrecioOferta")))
                'MsgBox(Datos.Item(6) & " - " & Datos.Item("Imp1") & " - " & CDbl(Datos.Item("Imp1")))
                'MsgBox(Datos.Item(7) & " - " & Datos.Item("Imp2") & " - " & CDbl(Datos.Item("Imp2")))
                'MsgBox(Datos.Item(8) & " - " & Datos.Item("Imp3") & " - " & CDbl(Datos.Item("Imp3")))
                'MsgBox(Datos.Item(9) & " - " & Datos.Item("CantEmp") & " - " & CDbl(Datos.Item("CantEmp")))

            End If

            Lector_Consulta.Close()
            'Conexion_BD.Close()
            comandBD_SERVIDOR.Parameters.Clear()

            'TestEnd = Now

            'MsgBox(DateDiff(DateInterval.Second, TestIni, TestEnd, FirstDayOfWeek.Monday, FirstWeekOfYear.System), MsgBoxStyle.Information)

            Dim mArchivo As String = Hablador_Setup

            If (Date.Today >= Convert.ToDateTime(Datos.Item("f_Inicial")) And Date.Today <= Convert.ToDateTime(Datos.Item("f_Final")) _
            And (Convert.ToDateTime(Datos.Item("h_Inicial")).TimeOfDay <= Date.Now.TimeOfDay And Convert.ToDateTime(Datos.Item("h_Final")).TimeOfDay >= Date.Now.TimeOfDay)) Then
                mArchivo = Hablador_Oferta_Setup
            End If

            Try

                'TestIni = Now

                Dim Lector As System.IO.StreamReader = Nothing
                Dim LeerLineas As String = ""

                Dim ClientWifi As System.Net.Sockets.TcpClient = Nothing
                Dim ClientBluetooth As System.IO.Ports.SerialPort = Nothing
                Dim ServicePort As BluetoothSerialPort = Nothing

                ' Verifica que haya conexion con la impresora
                Dim Conexion As Boolean

                If (ImpresoraSeleccionada = ConexionImpresora.Impresora_Bluetooth) Then

                    Try

                        If Impresora_Bluetooth_NeedPortRegister Then

                            Dim Tmp As String() = SerialPort.GetPortNames()
                            Dim Ports As ArrayList = New ArrayList(Tmp)
                            Dim Existe As Boolean = Ports.Contains(Impresora_Bluetooth_Port)

                            If Not Existe Then

                                ServicePort = BluetoothSerialPort.CreateClient( _
                                Impresora_Bluetooth_Port, New BluetoothEndPoint( _
                                    BluetoothAddress.Parse(Impresora_Bluetooth_Address), _
                                    BluetoothService.SerialPort _
                                    ) _
                                )

                                'Tmp = SerialPort.GetPortNames()

                            End If

                        End If

                        Lector = New System.IO.StreamReader(mArchivo)
                        LeerLineas = Lector.ReadToEnd()
                        Lector.Close()

                        ClientBluetooth = New System.IO.Ports.SerialPort(moduloFunciones.Impresora_Bluetooth_Port) 'New System.IO.Ports.SerialPort(moduloFunciones.Impresora_Puerto, BaudRate)
                        ClientBluetooth.Open()

                        Conexion = True

                    Catch ConnEx As Exception

                        Try
                            If Not (ServicePort Is Nothing) Then ServicePort.Close()
                        Catch SPEx As Exception
                            'MsgBox("TRYCATCHEXCEPTION CAPTURED (SERVICEPORT)" & vbNewLine & SPEx.Message)
                        End Try

                        Try
                            If (Not ClientBluetooth Is Nothing) Then If (ClientBluetooth.IsOpen) Then ClientBluetooth.Close() : ClientBluetooth.Dispose()
                        Catch CBEx As Exception
                            'MsgBox("TRYCATCHEXCEPTION CAPTURED (CLIENTBLUETOOTH)" & vbNewLine & CBEx.Message)
                        End Try

                        Conexion = False
                        MsgBox("No hay conexión con la impresora.", MsgBoxStyle.Information, "Setup isMOBILE")
                        lblStatus.Text = "Habladores"
                        lblStatus.Refresh()
                        Me.Enabled = True
                        comandBD_SERVIDOR.Parameters.Clear()
                        comandBD_SERVIDOR.Connection = conexionBD_SERVIDOR

                        Exit Sub

                    End Try

                ElseIf (ImpresoraSeleccionada = ConexionImpresora.Impresora_WLAN) Then

                    Try
                        Lector = New System.IO.StreamReader(mArchivo)
                        LeerLineas = Lector.ReadToEnd()
                        Lector.Close()

                        ClientWifi = New System.Net.Sockets.TcpClient()
                        ClientWifi.Connect(Impresora_WLAN_IP, Impresora_WLAN_Port)

                        Conexion = True
                    Catch ConnEx As Exception

                        ClientWifi.Close()
                        Conexion = False
                        MsgBox("No hay conexión con la impresora.", MsgBoxStyle.Information, "Setup isMOBILE")
                        lblStatus.Text = "Habladores"
                        lblStatus.Refresh()
                        Me.Enabled = True
                        comandBD_SERVIDOR.Parameters.Clear()
                        comandBD_SERVIDOR.Connection = conexionBD_SERVIDOR

                        Exit Sub

                    End Try

                End If

                'TestEnd = Now

                'MsgBox(DateDiff(DateInterval.Second, TestIni, TestEnd, FirstDayOfWeek.Monday, FirstWeekOfYear.System), MsgBoxStyle.Information)

                'Crea la variable que permitira imprimir

                'TestIni = Now

                Dim Writer As System.IO.StreamWriter = Nothing
                'Reemplaza las variables de los .txt

                LeerLineas = Replace(LeerLineas, "|1001|", Codigo) 'CODIGO PRINCIPAL
                LeerLineas = Replace(LeerLineas, "|1010|", CodigoEntrada)   'CODIGO INGRESADO
                LeerLineas = Replace(LeerLineas, "|1002|", CStr(Datos.Item(2))) ' DESCRIPCION COMPLETA
                LeerLineas = Replace(LeerLineas, "|1008|", obtenerCodigoEDI(Codigo, True, True)) 'CODIGO EDI SINO CODIGO PRINCIPAL

                Dim CodBarra As String
                CodBarra = obtenerCodigoBarra(Codigo, False)

                LeerLineas = Replace(LeerLineas, "|1014|", CodBarra) 'CODIGO BARRA
                LeerLineas = Replace(LeerLineas, "|1015|", IIf(CodBarra = "", Codigo, CodBarra)) 'CODIGO BARRA SINO CODIGO PRINCIPAL
                LeerLineas = Replace(LeerLineas, "|1007|", CStr(Datos.Item(2))) ' DESCRIPCION COMPLETA
                LeerLineas = Replace(LeerLineas, "|1013|", CStr(Datos.Item(2))) ' DESCRIPCION COMPLETA

                Dim CantCaracteres As Integer = CInt(Val(obtenerRegladeNegocio_Servidor("PDA_Habladores_CaracteresDescripcionParte", "40")))

                LeerLineas = Replace(LeerLineas, "|1017|", Microsoft.VisualBasic.Left(CStr(Datos.Item(2)), CantCaracteres)) ' DESCRIPCION COMPLETA PARTE 1

                Dim Desc2 As String

                If Len(CStr(Datos.Item(2))) > CantCaracteres Then
                    Desc2 = Microsoft.VisualBasic.Right(CStr(Datos.Item(2)), Len(CStr(Datos.Item(2))) - CantCaracteres)
                Else
                    Desc2 = ""
                End If

                LeerLineas = Replace(LeerLineas, "|1018|", Desc2) ' DESCRIPCION COMPLETA PARTE 2
                LeerLineas = Replace(LeerLineas, "|1009|", CStr(Datos.Item(3))) ' DESCRIPCION CORTA

                'Format(text1.text,"#.##0,00")
                'MessageBox.Show(FormatNumber(CDbl(Datos.Item(4)), , TriState.True, TriState.False, TriState.True))
                'MessageBox.Show(FormatNumber(Redondear_Precio(CDbl(Datos.Item(4)), CDbl(Datos.Item(6)), CDbl(Datos.Item(7)), CDbl(Datos.Item(8))), 2, TriState.True, TriState.False, TriState.True))

                'Dim Precio As Double = FormatNumber(Redondear_Precio(CDbl(Datos.Item(4)), CDbl(Datos.Item(6)), CDbl(Datos.Item(7)), CDbl(Datos.Item(8))), 2, TriState.True, TriState.False, TriState.True)
                Dim PrecioFicha As Double = CDbl(Datos.Item(4))
                'Dim PrecioOferta As Double = FormatNumber(Redondear_Precio(CDbl(Datos.Item(5)), CDbl(Datos.Item(6)), CDbl(Datos.Item(7)), CDbl(Datos.Item(8))), 2, TriState.True, TriState.False, TriState.True)
                Dim PrecioOfertaFicha As Double = CDbl(Datos.Item(5))

                Dim Precio As Double = PrecioFicha
                Dim PrecioOferta As Double = PrecioOfertaFicha

                If Hablador_FactorizarPreciosMultiMoneda Then
                    Precio = FormatNumber(FactorizarPrecio(Precio, , Datos.Item("c_CodMoneda")), , TriState.True, TriState.False, TriState.True)
                    PrecioOferta = FormatNumber(FactorizarPrecio(PrecioOferta, , Datos.Item("c_CodMoneda")), , TriState.True, TriState.False, TriState.True)
                Else
                    TmpMoneda_Sim = Moneda_Sim
                End If

                Dim TmpPrc_MonPred As Double, TmpPrcO_MonPred As Double, TmpPrc_MonAlt As Double, TmpPrcO_MonAlt As Double

                TmpPrc_MonPred = FormatNumber(FactorizarPrecio(PrecioFicha, Moneda_Fac, Datos.Item("c_CodMoneda"), Moneda_Cod))
                TmpPrcO_MonPred = FormatNumber(FactorizarPrecio(PrecioOfertaFicha, Moneda_Fac, Datos.Item("c_CodMoneda"), Moneda_Cod))

                TmpPrc_MonAlt = FormatNumber(FactorizarPrecio(PrecioFicha, , Datos.Item("c_CodMoneda"), Hablador_MonedaAlternaParaPrecios))
                TmpPrcO_MonAlt = FormatNumber(FactorizarPrecio(PrecioOfertaFicha, , Datos.Item("c_CodMoneda"), Hablador_MonedaAlternaParaPrecios))

                'LeerLineas = Replace(LeerLineas, "|1003|", CStr(Precio)) ' PRECIO
                LeerLineas = Replace(LeerLineas, "|1003|", CStr(FormatNumber(CDbl(Precio), , TriState.True, TriState.False, TriState.True))) ' PRECIO
                'LeerLineas = Replace(LeerLineas, "|1006|", CStr(PrecioOferta)) ' PRECIO OFERTA
                LeerLineas = Replace(LeerLineas, "|1006|", CStr(FormatNumber(CDbl(PrecioOferta), , TriState.True, TriState.False, TriState.True))) ' PRECIO OFERTA

                Dim pImp1 As Double = CDbl(Datos.Item(6))
                Dim pImp2 As Double = CDbl(Datos.Item(7))
                Dim pImp3 As Double = CDbl(Datos.Item(8))

                Dim Imp1 As Double = FormatNumber(Precio * pImp1 / 100, 2)
                Dim Imp2 As Double = FormatNumber(Precio * pImp2 / 100, 2)
                Dim Imp3 As Double = FormatNumber(Precio * pImp3 / 100, 2)

                Dim ImpTotal As Double = FormatNumber(Imp1 + Imp2 + Imp3, 2, TriState.True, TriState.False, TriState.True)

                Dim ImpO1 As Double = FormatNumber(PrecioOferta * pImp1 / 100, 2)
                Dim ImpO2 As Double = FormatNumber(PrecioOferta * pImp2 / 100, 2)
                Dim ImpO3 As Double = FormatNumber(PrecioOferta * pImp3 / 100, 2)

                Dim ImpTotalOferta As Double = FormatNumber(ImpO1 + ImpO2 + ImpO3, 2, TriState.True, TriState.False, TriState.True)

                Dim ImpN1_MonPred As Double = FormatNumber(TmpPrc_MonPred * pImp1 / 100, 2)
                Dim ImpN2_MonPred As Double = FormatNumber(TmpPrc_MonPred * pImp2 / 100, 2)
                Dim ImpN3_MonPred As Double = FormatNumber(TmpPrc_MonPred * pImp3 / 100, 2)

                Dim Imp_Nor_MonPred As Double = FormatNumber(ImpN1_MonPred + ImpN2_MonPred + ImpN3_MonPred, 2, TriState.True, TriState.False, TriState.True)

                Dim ImpN1_MonAlt As Double = FormatNumber(TmpPrc_MonAlt * pImp1 / 100, 2)
                Dim ImpN2_MonAlt As Double = FormatNumber(TmpPrc_MonAlt * pImp2 / 100, 2)
                Dim ImpN3_MonAlt As Double = FormatNumber(TmpPrc_MonAlt * pImp3 / 100, 2)

                Dim Imp_Nor_MonAlt As Double = FormatNumber(ImpN1_MonAlt + ImpN2_MonAlt + ImpN3_MonAlt, 2, TriState.True, TriState.False, TriState.True)

                Dim Total_Nor_MonPred As Double = FormatNumber(TmpPrc_MonPred + Imp_Nor_MonPred, 2, TriState.True, TriState.False, TriState.True)
                Dim Total_Nor_MonAlt As Double = FormatNumber(TmpPrc_MonAlt + Imp_Nor_MonAlt, 2, TriState.True, TriState.False, TriState.True)

                Dim ImpO1_MonPred As Double = FormatNumber(TmpPrcO_MonPred * pImp1 / 100, 2)
                Dim ImpO2_MonPred As Double = FormatNumber(TmpPrcO_MonPred * pImp2 / 100, 2)
                Dim ImpO3_MonPred As Double = FormatNumber(TmpPrcO_MonPred * pImp3 / 100, 2)

                Dim Imp_Ofe_MonPred As Double = FormatNumber(ImpO1_MonPred + ImpO2_MonPred + ImpO3_MonPred, 2, TriState.True, TriState.False, TriState.True)

                Dim ImpO1_MonAlt As Double = FormatNumber(TmpPrcO_MonAlt * pImp1 / 100, 2)
                Dim ImpO2_MonAlt As Double = FormatNumber(TmpPrcO_MonAlt * pImp2 / 100, 2)
                Dim ImpO3_MonAlt As Double = FormatNumber(TmpPrcO_MonAlt * pImp3 / 100, 2)

                Dim Imp_Ofe_MonAlt As Double = FormatNumber(ImpO1_MonAlt + ImpO2_MonAlt + ImpO3_MonAlt, 2, TriState.True, TriState.False, TriState.True)

                Dim Total_Ofe_MonPred As Double = FormatNumber(TmpPrcO_MonPred + Imp_Ofe_MonPred, 2, TriState.True, TriState.False, TriState.True)
                Dim Total_Ofe_MonAlt As Double = FormatNumber(TmpPrcO_MonAlt + Imp_Ofe_MonAlt, 2, TriState.True, TriState.False, TriState.True)

                LeerLineas = Replace(LeerLineas, "|1004|", CStr(FormatNumber(CDbl(ImpTotal), , TriState.True, TriState.False, TriState.True))) ' IMPUESTO
                LeerLineas = Replace(LeerLineas, "|1005|", CStr(FormatNumber(CDbl(Precio + ImpTotal), , TriState.True, TriState.False, TriState.True))) ' TOTAL
                LeerLineas = Replace(LeerLineas, "|1016|", CStr(Datos.Item(9))) ' Cantidad por Empaque
                LeerLineas = Replace(LeerLineas, "|1019|", CStr(FormatNumber(ConversionBolivarFuerte(Precio + ImpTotal), , TriState.True, TriState.False, TriState.True))) ' TOTAL POST RECONVERSION
                LeerLineas = Replace(LeerLineas, "|1020|", CStr(FormatNumber(MontoPREReconversion(Precio + ImpTotal), , TriState.True, TriState.False, TriState.True))) ' TOTAL PRE RECONVERSION
                LeerLineas = Replace(LeerLineas, "|1021|", CStr(Datos.Item("Presentacion"))) ' Campo Presentacion
                LeerLineas = Replace(LeerLineas, "|1022|", CStr(FormatNumber(pImp1 + pImp2 + pImp3, , TriState.True, TriState.False, TriState.True))) ' Porc Imp
                LeerLineas = Replace(LeerLineas, "|1023|", CStr(FormatNumber(CDbl(ImpTotalOferta), , TriState.True, TriState.False, TriState.True))) ' IMPUESTO OF
                LeerLineas = Replace(LeerLineas, "|1024|", CStr(FormatNumber(CDbl(PrecioOferta + ImpTotalOferta), , TriState.True, TriState.False, TriState.True))) ' TOTAL OF

                LeerLineas = Replace(LeerLineas, "|1027|", CStr(Datos.Item("Text1")))
                LeerLineas = Replace(LeerLineas, "|1028|", CStr(Datos.Item("Text2")))
                LeerLineas = Replace(LeerLineas, "|1029|", CStr(Datos.Item("Text3")))
                LeerLineas = Replace(LeerLineas, "|1030|", CStr(Datos.Item("Text4")))
                LeerLineas = Replace(LeerLineas, "|1031|", CStr(Datos.Item("Text5")))
                LeerLineas = Replace(LeerLineas, "|1032|", CStr(Datos.Item("Text6")))
                LeerLineas = Replace(LeerLineas, "|1033|", CStr(Datos.Item("Text7")))
                LeerLineas = Replace(LeerLineas, "|1034|", CStr(Datos.Item("Text8")))

                LeerLineas = Replace(LeerLineas, "|1035|", CStr(FormatNumber(TmpPrc_MonPred, , TriState.True, TriState.False, TriState.True)))
                LeerLineas = Replace(LeerLineas, "|1036|", CStr(FormatNumber(Imp_Nor_MonPred, , TriState.True, TriState.False, TriState.True)))
                LeerLineas = Replace(LeerLineas, "|1037|", CStr(FormatNumber(Total_Nor_MonPred, , TriState.True, TriState.False, TriState.True)))
                LeerLineas = Replace(LeerLineas, "|1038|", CStr(FormatNumber(TmpPrc_MonAlt, , TriState.True, TriState.False, TriState.True)))
                LeerLineas = Replace(LeerLineas, "|1039|", CStr(FormatNumber(Imp_Nor_MonAlt, , TriState.True, TriState.False, TriState.True)))
                LeerLineas = Replace(LeerLineas, "|1040|", CStr(FormatNumber(Total_Nor_MonAlt, , TriState.True, TriState.False, TriState.True)))

                LeerLineas = Replace(LeerLineas, "|1041|", CStr(FormatNumber(TmpPrcO_MonPred, , TriState.True, TriState.False, TriState.True)))
                LeerLineas = Replace(LeerLineas, "|1042|", CStr(FormatNumber(Imp_Ofe_MonPred, , TriState.True, TriState.False, TriState.True)))
                LeerLineas = Replace(LeerLineas, "|1043|", CStr(FormatNumber(Total_Ofe_MonPred, , TriState.True, TriState.False, TriState.True)))
                LeerLineas = Replace(LeerLineas, "|1044|", CStr(FormatNumber(TmpPrcO_MonAlt, , TriState.True, TriState.False, TriState.True)))
                LeerLineas = Replace(LeerLineas, "|1045|", CStr(FormatNumber(Imp_Ofe_MonAlt, , TriState.True, TriState.False, TriState.True)))
                LeerLineas = Replace(LeerLineas, "|1046|", CStr(FormatNumber(Total_Ofe_MonAlt, , TriState.True, TriState.False, TriState.True)))

                LeerLineas = Replace(LeerLineas, "|1047|", TmpMoneda_Sim)

                LeerLineas = Replace(LeerLineas, "|1095|", Format(Now, "yyyy/dd/MM"))  'Fecha Actual
                LeerLineas = Replace(LeerLineas, "|1096|", Format(Now, "hh:mm:ss tt"))  'Horario Actual
                LeerLineas = Replace(LeerLineas, "|1097|", Format(Now, "yyyy/MM/dd"))  'Fecha Actual
                LeerLineas = Replace(LeerLineas, "|1098|", Format(Now, "HH:mm:ss"))  'Horario Actual
                LeerLineas = Replace(LeerLineas, "|1099|", Format(Now, "dd/MM/yyyy"))  'Fecha Actual
                LeerLineas = Replace(LeerLineas, "|1100|", Format(Now, "dd-MM-yyyy"))  'Fecha Actual

                'TestEnd = Now

                'MsgBox(DateDiff(DateInterval.Second, TestIni, TestEnd, FirstDayOfWeek.Monday, FirstWeekOfYear.System), MsgBoxStyle.Information)

                'TestIni = Now

                'Imprime todas las lineas leidas

                If (ImpresoraSeleccionada = ConexionImpresora.Impresora_Bluetooth) Then

                    Try

                        For N = 1 To Num_Impresiones
                            ClientBluetooth.WriteLine(LeerLineas)
                        Next

                        ClientBluetooth.Close()
                        If Not (ServicePort Is Nothing) Then ServicePort.Close()

                    Catch AnyBTEx As Exception
                        MsgBox("Ha ocurrido un error en la comunicación con la impresora. Intente de nuevo en algunos momentos.", MsgBoxStyle.Information, "isMOBILE")
                    End Try

                ElseIf (ImpresoraSeleccionada = ConexionImpresora.Impresora_WLAN) Then

                    Writer = New System.IO.StreamWriter(ClientWifi.GetStream())

                    For N = 1 To Num_Impresiones
                        Writer.WriteLine(LeerLineas)
                    Next

                    'Comienza Impresion

                    Writer.Flush()

                    'Close Connections

                    Writer.Close()

                    ClientWifi.Close()

                End If

                'TestEnd = Now

                'MsgBox(DateDiff(DateInterval.Second, TestIni, TestEnd, FirstDayOfWeek.Monday, FirstWeekOfYear.System), MsgBoxStyle.Information)

            Catch AnyInner As Exception
                'Catch Exception Here
                MsgBox("No se encuentra el archivo de configuracion de los habladores o ha ocurrido un error en la funcionalidad de imprimir.", MsgBoxStyle.Information, "Setup")
                lblStatus.Text = "Habladores"
                lblStatus.Refresh()
                Me.Enabled = True

                comandBD_SERVIDOR.Parameters.Clear()
                comandBD_SERVIDOR.Connection = conexionBD_SERVIDOR

                Exit Sub
            End Try

            lblStatus.Text = "Habladores"
            lblStatus.Refresh()
            Me.Enabled = True

        Catch Any As Exception
            MsgBox("Ha ocurrido un error en la funcion de imprimir." & vbNewLine & vbNewLine & Any.Message, MsgBoxStyle.Information, "isMOBILE")
            lblStatus.Text = "Habladores"
            lblStatus.Refresh()
            Me.Enabled = True
        End Try

        comandBD_SERVIDOR.Parameters.Clear()
        comandBD_SERVIDOR.Connection = conexionBD_SERVIDOR

    End Sub

    Private Sub cmbBuscar_producto_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbBuscar_producto.Click

        Formbusqueda_productos.Tipo_Impresion = "Hablador"

        Formbusqueda_productos.Show()
        Me.cmbImprimir_hablador.Enabled = False
        ImpHblEti_BusquedaActiva = True
        Me.Visible = False

        Formbusqueda_productos.txtproductos.Focus()

    End Sub

    Private Sub cmbVerificarCodigo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbVerificarCodigo.Click
        VerificaryCargarProducto(txtCod.Text)
    End Sub

    Private Sub VerificaryCargarProducto(ByVal codigoEntrada As String)

        Try

            Me.Enabled = False

            Dim dataSet_Servidor As New Data.DataSet
            Dim nuevoAncho As Integer

            Dim Query As String = "SELECT C.C_CODIGO, P.C_DESCRI " + _
            "FROM MA_PRODUCTOS P INNER JOIN MA_CODIGOS C " + _
            "ON P.C_CODIGO = C.C_CODNASA " + _
            "WHERE C.C_CODIGO = @codigoProducto"

            comandBD_SERVIDOR.CommandText = Query
            comandBD_SERVIDOR.Parameters.Add("@codigoProducto", codigoEntrada)

            If Val(TmpBDCheck) = 1 Then
                dataSet_Servidor = executeQuery_Servidor(comandBD_SERVIDOR, conexionBD_SERVIDORPOS)
            Else
                dataSet_Servidor = executeQuery_Servidor(comandBD_SERVIDOR)
            End If

            If dataSet_Servidor.Tables(0).Rows.Count() = 0 Then
                MsgBox("Búsqueda sin éxito.", MsgBoxStyle.Information, "isMOBILE")
                Me.Enabled = True
                txtCod.Text = ""
                txtCod.Focus()
                Exit Sub
            End If

            'En la propiedad DataSource es donde puedo llenar el dataGrid con la informacion de la BD.
            Me.Datagrid_Impresion.DataSource = dataSet_Servidor.Tables(0)

            'se cambia el nombre de las columnas del dataSet al nombre que se desea que aparezcan en el dataGrid,
            'para después poder utilizarlos en la funcion de obtener el campo mas largo

            dataSet_Servidor.Tables(0).Columns(0).ColumnName = "CÓDIGO"
            dataSet_Servidor.Tables(0).Columns(1).ColumnName = "DESCRIPCIÓN"

            Dim Estilos As New DataGridTableStyle
            Dim columna1, columna2 As New DataGridTextBoxColumn
            Estilos.MappingName = dataSet_Servidor.Tables(0).TableName

            With columna1
                .MappingName = "CÓDIGO"
                .HeaderText = dataSet_Servidor.Tables(0).Columns(0).ColumnName
            End With

            With columna2
                .MappingName = "DESCRIPCIÓN"
                .HeaderText = dataSet_Servidor.Tables(0).Columns(1).ColumnName
            End With

            Estilos.GridColumnStyles.Add(columna1)
            Estilos.GridColumnStyles.Add(columna2)

            Me.Datagrid_Impresion.TableStyles.Clear()
            Me.Datagrid_Impresion.TableStyles.Add(Estilos)

            ' For para ajustar el tamaño de las columnas contenidas en el dataSet_PDA al campo mas largo
            Dim numeroColumnas As Integer
            For numeroColumnas = 0 To (dataSet_Servidor.Tables(0).Columns.Count - 1)
                nuevoAncho = obtenerCampoMasLargo(dataSet_Servidor, 0, numeroColumnas)
                Me.Datagrid_Impresion.TableStyles(dataSet_Servidor.Tables(0).TableName).GridColumnStyles(numeroColumnas).Width = nuevoAncho
            Next

            Me.cmbImprimir_hablador.Visible = True
            Me.cmbImprimir_hablador.Enabled = True
            ImpHblEti_BusquedaActiva = False
            Me.Enabled = True

        Catch ex As Exception
            Me.Enabled = True
            MsgBox("Ha ocurrido un error al verificar el codigo.", MsgBoxStyle.Information, "isMOBILE")
        End Try

    End Sub

    Private Sub txtCod_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCod.GotFocus
        txtCod.Text = ""
    End Sub

    Private Sub txtCod_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCod.KeyDown
        If e.KeyCode = Keys.Enter Then
            VerificaryCargarProducto(txtCod.Text)
        End If
    End Sub

    Private Sub CmbSeleccionarImpresora_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmbSeleccionarImpresora.Click
        FormSeleccionImpresora.Tipo_Impresion = "Hablador"
        FormSeleccionImpresora.Show()
        Me.Visible = False
    End Sub

    Private Sub txtCod_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCod.TextChanged

    End Sub

End Class