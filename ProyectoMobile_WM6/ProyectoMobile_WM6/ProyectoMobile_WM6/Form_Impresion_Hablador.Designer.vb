﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class Form_Impresion_Hablador
    Inherits System.Windows.Forms.Form

    'Form invalida a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer
    Private mainMenu1 As System.Windows.Forms.MainMenu

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar con el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form_Impresion_Hablador))
        Me.mainMenu1 = New System.Windows.Forms.MainMenu
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.cmbSalir_hablador = New System.Windows.Forms.PictureBox
        Me.lblUsuarioEnSesion = New System.Windows.Forms.Label
        Me.picBarraMorada = New System.Windows.Forms.PictureBox
        Me.cmbImprimir_hablador = New System.Windows.Forms.Button
        Me.PictureBox2 = New System.Windows.Forms.PictureBox
        Me.Datagrid_Impresion = New System.Windows.Forms.DataGrid
        Me.cmbBuscar_producto = New System.Windows.Forms.Button
        Me.numero_impresiones = New System.Windows.Forms.NumericUpDown
        Me.lblStatus = New System.Windows.Forms.Label
        Me.txtCod = New System.Windows.Forms.TextBox
        Me.cmbVerificarCodigo = New System.Windows.Forms.PictureBox
        Me.CmbSeleccionarImpresora = New System.Windows.Forms.PictureBox
        Me.SuspendLayout()
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(42, 0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(198, 38)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'cmbSalir_hablador
        '
        Me.cmbSalir_hablador.Image = CType(resources.GetObject("cmbSalir_hablador.Image"), System.Drawing.Image)
        Me.cmbSalir_hablador.Location = New System.Drawing.Point(0, 0)
        Me.cmbSalir_hablador.Name = "cmbSalir_hablador"
        Me.cmbSalir_hablador.Size = New System.Drawing.Size(41, 38)
        Me.cmbSalir_hablador.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'lblUsuarioEnSesion
        '
        Me.lblUsuarioEnSesion.BackColor = System.Drawing.Color.DarkGray
        Me.lblUsuarioEnSesion.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblUsuarioEnSesion.Location = New System.Drawing.Point(0, 39)
        Me.lblUsuarioEnSesion.Name = "lblUsuarioEnSesion"
        Me.lblUsuarioEnSesion.Size = New System.Drawing.Size(240, 15)
        Me.lblUsuarioEnSesion.Text = "Usuario:"
        '
        'picBarraMorada
        '
        Me.picBarraMorada.Image = CType(resources.GetObject("picBarraMorada.Image"), System.Drawing.Image)
        Me.picBarraMorada.Location = New System.Drawing.Point(0, 260)
        Me.picBarraMorada.Name = "picBarraMorada"
        Me.picBarraMorada.Size = New System.Drawing.Size(240, 40)
        Me.picBarraMorada.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'cmbImprimir_hablador
        '
        Me.cmbImprimir_hablador.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.cmbImprimir_hablador.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.cmbImprimir_hablador.ForeColor = System.Drawing.Color.Black
        Me.cmbImprimir_hablador.Location = New System.Drawing.Point(72, 268)
        Me.cmbImprimir_hablador.Name = "cmbImprimir_hablador"
        Me.cmbImprimir_hablador.Size = New System.Drawing.Size(87, 24)
        Me.cmbImprimir_hablador.TabIndex = 116
        Me.cmbImprimir_hablador.Text = "Imprimir"
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.Gainsboro
        Me.PictureBox2.Location = New System.Drawing.Point(0, 54)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(240, 205)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'Datagrid_Impresion
        '
        Me.Datagrid_Impresion.BackgroundColor = System.Drawing.Color.Gainsboro
        Me.Datagrid_Impresion.GridLineColor = System.Drawing.Color.Gainsboro
        Me.Datagrid_Impresion.HeaderBackColor = System.Drawing.Color.Gainsboro
        Me.Datagrid_Impresion.Location = New System.Drawing.Point(19, 154)
        Me.Datagrid_Impresion.Name = "Datagrid_Impresion"
        Me.Datagrid_Impresion.Size = New System.Drawing.Size(202, 84)
        Me.Datagrid_Impresion.TabIndex = 119
        '
        'cmbBuscar_producto
        '
        Me.cmbBuscar_producto.BackColor = System.Drawing.Color.White
        Me.cmbBuscar_producto.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.cmbBuscar_producto.ForeColor = System.Drawing.Color.Black
        Me.cmbBuscar_producto.Location = New System.Drawing.Point(19, 92)
        Me.cmbBuscar_producto.Name = "cmbBuscar_producto"
        Me.cmbBuscar_producto.Size = New System.Drawing.Size(108, 24)
        Me.cmbBuscar_producto.TabIndex = 120
        Me.cmbBuscar_producto.Text = "Buscar Producto"
        '
        'numero_impresiones
        '
        Me.numero_impresiones.Location = New System.Drawing.Point(133, 92)
        Me.numero_impresiones.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.numero_impresiones.Name = "numero_impresiones"
        Me.numero_impresiones.Size = New System.Drawing.Size(77, 24)
        Me.numero_impresiones.TabIndex = 201
        Me.numero_impresiones.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'lblStatus
        '
        Me.lblStatus.BackColor = System.Drawing.Color.Gainsboro
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.lblStatus.Location = New System.Drawing.Point(19, 65)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(202, 14)
        Me.lblStatus.Text = "Habladores"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txtCod
        '
        Me.txtCod.Location = New System.Drawing.Point(19, 125)
        Me.txtCod.Name = "txtCod"
        Me.txtCod.Size = New System.Drawing.Size(156, 23)
        Me.txtCod.TabIndex = 207
        '
        'cmbVerificarCodigo
        '
        Me.cmbVerificarCodigo.Image = CType(resources.GetObject("cmbVerificarCodigo.Image"), System.Drawing.Image)
        Me.cmbVerificarCodigo.Location = New System.Drawing.Point(181, 125)
        Me.cmbVerificarCodigo.Name = "cmbVerificarCodigo"
        Me.cmbVerificarCodigo.Size = New System.Drawing.Size(29, 23)
        Me.cmbVerificarCodigo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'CmbSeleccionarImpresora
        '
        Me.CmbSeleccionarImpresora.Image = CType(resources.GetObject("CmbSeleccionarImpresora.Image"), System.Drawing.Image)
        Me.CmbSeleccionarImpresora.Location = New System.Drawing.Point(22, 268)
        Me.CmbSeleccionarImpresora.Name = "CmbSeleccionarImpresora"
        Me.CmbSeleccionarImpresora.Size = New System.Drawing.Size(29, 23)
        Me.CmbSeleccionarImpresora.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'Form_Impresion_Hablador
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.ClientSize = New System.Drawing.Size(240, 300)
        Me.ControlBox = False
        Me.Controls.Add(Me.CmbSeleccionarImpresora)
        Me.Controls.Add(Me.cmbVerificarCodigo)
        Me.Controls.Add(Me.txtCod)
        Me.Controls.Add(Me.lblStatus)
        Me.Controls.Add(Me.numero_impresiones)
        Me.Controls.Add(Me.cmbBuscar_producto)
        Me.Controls.Add(Me.Datagrid_Impresion)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.cmbImprimir_hablador)
        Me.Controls.Add(Me.picBarraMorada)
        Me.Controls.Add(Me.lblUsuarioEnSesion)
        Me.Controls.Add(Me.cmbSalir_hablador)
        Me.Controls.Add(Me.PictureBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Form_Impresion_Hablador"
        Me.Text = "isMOBILE - Habladores"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents cmbSalir_hablador As System.Windows.Forms.PictureBox
    Friend WithEvents lblUsuarioEnSesion As System.Windows.Forms.Label
    Friend WithEvents picBarraMorada As System.Windows.Forms.PictureBox
    Friend WithEvents cmbImprimir_hablador As System.Windows.Forms.Button
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents Datagrid_Impresion As System.Windows.Forms.DataGrid
    Friend WithEvents cmbBuscar_producto As System.Windows.Forms.Button
    Friend WithEvents numero_impresiones As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents txtCod As System.Windows.Forms.TextBox
    Friend WithEvents cmbVerificarCodigo As System.Windows.Forms.PictureBox
    Friend WithEvents CmbSeleccionarImpresora As System.Windows.Forms.PictureBox
End Class
