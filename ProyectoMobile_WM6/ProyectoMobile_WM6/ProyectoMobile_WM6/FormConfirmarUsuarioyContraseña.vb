﻿Public Class FormConfirmarUsuarioyContraseña

    Private Sub FormConfirmarUsuarioyContraseña_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblUsuarioEnSesion.Text = "Usuario: " + descripcionUsuario
        Me.Refresh()
        Me.Update()
        txtUsuario.Text = ""
        txtContraseña.Text = ""
        txtUsuario.Focus()
    End Sub

    Private Sub cmbAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbAceptar.Click
        Dim dataset_confirmarUsuario As New Data.DataSet
        'Dim ExisteUsuarioyContraseña As String

        'Evaluo que el formulario no este vacio
        If (txtUsuario.Text = "") Then

            MsgBox("Debe ingresar un usuario", MsgBoxStyle.Information, "isMOBILE")
            txtUsuario.Focus()
        Else

            If (txtContraseña.Text = "") Then
                MsgBox("Debe ingresar una contraseña", MsgBoxStyle.Information, "isMOBILE")
                txtContraseña.Focus()
            Else
                'Verificación de la información suministrada
                dataset_confirmarUsuario = validarUsuarioContraseña(txtUsuario.Text, txtContraseña.Text)
                If Not dataset_confirmarUsuario.Tables(0).Rows.Count = 0 Then
                    Me.DialogResult = Windows.Forms.DialogResult.OK
                    Me.Close()
                Else
                    MsgBox("Usuario y/o Contraseñas incorrectos", MsgBoxStyle.Information, "isMOBILE")
                    txtUsuario.Text = ""
                    txtContraseña.Text = ""
                    txtUsuario.Focus()
                End If
            End If
        End If
    End Sub

    Private Sub cmbCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbCancelar.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub
End Class