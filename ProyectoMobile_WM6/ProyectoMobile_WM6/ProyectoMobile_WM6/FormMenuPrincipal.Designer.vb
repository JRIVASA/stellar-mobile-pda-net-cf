﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class formMenuPrincipal
    Inherits System.Windows.Forms.Form

    'Form invalida a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar con el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(formMenuPrincipal))
        Me.lblInventario = New System.Windows.Forms.Label
        Me.lblStatus_MenuPrincipal = New System.Windows.Forms.Label
        Me.lblEtiquetas = New System.Windows.Forms.Label
        Me.lblHabladores = New System.Windows.Forms.Label
        Me.lblConsulta = New System.Windows.Forms.Label
        Me.lblValidacionFacturaPOS1 = New System.Windows.Forms.Label
        Me.lblModLog = New System.Windows.Forms.Label
        Me.CmdModLogisticaAlmacenaje = New System.Windows.Forms.PictureBox
        Me.cmbValidacionFacturaPOS = New System.Windows.Forms.PictureBox
        Me.cmbConsulta = New System.Windows.Forms.PictureBox
        Me.cmbHabladores = New System.Windows.Forms.PictureBox
        Me.cmbEtiquetas = New System.Windows.Forms.PictureBox
        Me.cmbInventario = New System.Windows.Forms.PictureBox
        Me.CmbSalir = New System.Windows.Forms.PictureBox
        Me.PicTopBar = New System.Windows.Forms.PictureBox
        Me.lblActualizar = New System.Windows.Forms.Label
        Me.CmdActualizar = New System.Windows.Forms.PictureBox
        Me.TimerReload = New System.Windows.Forms.Timer
        Me.SuspendLayout()
        '
        'lblInventario
        '
        Me.lblInventario.BackColor = System.Drawing.Color.Gainsboro
        Me.lblInventario.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular)
        Me.lblInventario.Location = New System.Drawing.Point(87, 114)
        Me.lblInventario.Name = "lblInventario"
        Me.lblInventario.Size = New System.Drawing.Size(72, 26)
        Me.lblInventario.Text = "Inventario"
        Me.lblInventario.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblInventario.Visible = False
        '
        'lblStatus_MenuPrincipal
        '
        Me.lblStatus_MenuPrincipal.BackColor = System.Drawing.Color.DarkGray
        Me.lblStatus_MenuPrincipal.Location = New System.Drawing.Point(0, 39)
        Me.lblStatus_MenuPrincipal.Name = "lblStatus_MenuPrincipal"
        Me.lblStatus_MenuPrincipal.Size = New System.Drawing.Size(240, 15)
        Me.lblStatus_MenuPrincipal.Visible = False
        '
        'lblEtiquetas
        '
        Me.lblEtiquetas.BackColor = System.Drawing.Color.Gainsboro
        Me.lblEtiquetas.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular)
        Me.lblEtiquetas.Location = New System.Drawing.Point(16, 188)
        Me.lblEtiquetas.Name = "lblEtiquetas"
        Me.lblEtiquetas.Size = New System.Drawing.Size(72, 26)
        Me.lblEtiquetas.Text = "Etiquetas"
        Me.lblEtiquetas.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblEtiquetas.Visible = False
        '
        'lblHabladores
        '
        Me.lblHabladores.BackColor = System.Drawing.Color.Gainsboro
        Me.lblHabladores.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular)
        Me.lblHabladores.Location = New System.Drawing.Point(87, 188)
        Me.lblHabladores.Name = "lblHabladores"
        Me.lblHabladores.Size = New System.Drawing.Size(72, 26)
        Me.lblHabladores.Text = "Habladores"
        Me.lblHabladores.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblHabladores.Visible = False
        '
        'lblConsulta
        '
        Me.lblConsulta.BackColor = System.Drawing.Color.Gainsboro
        Me.lblConsulta.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular)
        Me.lblConsulta.Location = New System.Drawing.Point(158, 114)
        Me.lblConsulta.Name = "lblConsulta"
        Me.lblConsulta.Size = New System.Drawing.Size(72, 26)
        Me.lblConsulta.Text = "Consulta"
        Me.lblConsulta.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblConsulta.Visible = False
        '
        'lblValidacionFacturaPOS1
        '
        Me.lblValidacionFacturaPOS1.BackColor = System.Drawing.Color.Gainsboro
        Me.lblValidacionFacturaPOS1.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular)
        Me.lblValidacionFacturaPOS1.Location = New System.Drawing.Point(16, 263)
        Me.lblValidacionFacturaPOS1.Name = "lblValidacionFacturaPOS1"
        Me.lblValidacionFacturaPOS1.Size = New System.Drawing.Size(72, 26)
        Me.lblValidacionFacturaPOS1.Text = "Validación Factura POS"
        Me.lblValidacionFacturaPOS1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblValidacionFacturaPOS1.Visible = False
        '
        'lblModLog
        '
        Me.lblModLog.BackColor = System.Drawing.Color.Gainsboro
        Me.lblModLog.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular)
        Me.lblModLog.Location = New System.Drawing.Point(87, 263)
        Me.lblModLog.Name = "lblModLog"
        Me.lblModLog.Size = New System.Drawing.Size(72, 26)
        Me.lblModLog.Text = "Logística Almacenaje"
        Me.lblModLog.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblModLog.Visible = False
        '
        'CmdModLogisticaAlmacenaje
        '
        Me.CmdModLogisticaAlmacenaje.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.CmdModLogisticaAlmacenaje.Image = CType(resources.GetObject("CmdModLogisticaAlmacenaje.Image"), System.Drawing.Image)
        Me.CmdModLogisticaAlmacenaje.Location = New System.Drawing.Point(99, 213)
        Me.CmdModLogisticaAlmacenaje.Name = "CmdModLogisticaAlmacenaje"
        Me.CmdModLogisticaAlmacenaje.Size = New System.Drawing.Size(48, 47)
        Me.CmdModLogisticaAlmacenaje.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.CmdModLogisticaAlmacenaje.Visible = False
        '
        'cmbValidacionFacturaPOS
        '
        Me.cmbValidacionFacturaPOS.Image = CType(resources.GetObject("cmbValidacionFacturaPOS.Image"), System.Drawing.Image)
        Me.cmbValidacionFacturaPOS.Location = New System.Drawing.Point(28, 213)
        Me.cmbValidacionFacturaPOS.Name = "cmbValidacionFacturaPOS"
        Me.cmbValidacionFacturaPOS.Size = New System.Drawing.Size(48, 47)
        Me.cmbValidacionFacturaPOS.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.cmbValidacionFacturaPOS.Visible = False
        '
        'cmbConsulta
        '
        Me.cmbConsulta.Image = CType(resources.GetObject("cmbConsulta.Image"), System.Drawing.Image)
        Me.cmbConsulta.Location = New System.Drawing.Point(170, 63)
        Me.cmbConsulta.Name = "cmbConsulta"
        Me.cmbConsulta.Size = New System.Drawing.Size(48, 47)
        Me.cmbConsulta.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.cmbConsulta.Visible = False
        '
        'cmbHabladores
        '
        Me.cmbHabladores.Image = CType(resources.GetObject("cmbHabladores.Image"), System.Drawing.Image)
        Me.cmbHabladores.Location = New System.Drawing.Point(99, 138)
        Me.cmbHabladores.Name = "cmbHabladores"
        Me.cmbHabladores.Size = New System.Drawing.Size(48, 47)
        Me.cmbHabladores.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.cmbHabladores.Visible = False
        '
        'cmbEtiquetas
        '
        Me.cmbEtiquetas.Image = CType(resources.GetObject("cmbEtiquetas.Image"), System.Drawing.Image)
        Me.cmbEtiquetas.Location = New System.Drawing.Point(28, 138)
        Me.cmbEtiquetas.Name = "cmbEtiquetas"
        Me.cmbEtiquetas.Size = New System.Drawing.Size(48, 47)
        Me.cmbEtiquetas.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.cmbEtiquetas.Visible = False
        '
        'cmbInventario
        '
        Me.cmbInventario.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.cmbInventario.Image = CType(resources.GetObject("cmbInventario.Image"), System.Drawing.Image)
        Me.cmbInventario.Location = New System.Drawing.Point(99, 63)
        Me.cmbInventario.Name = "cmbInventario"
        Me.cmbInventario.Size = New System.Drawing.Size(48, 47)
        Me.cmbInventario.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.cmbInventario.Visible = False
        '
        'CmbSalir
        '
        Me.CmbSalir.Image = CType(resources.GetObject("CmbSalir.Image"), System.Drawing.Image)
        Me.CmbSalir.Location = New System.Drawing.Point(0, 0)
        Me.CmbSalir.Name = "CmbSalir"
        Me.CmbSalir.Size = New System.Drawing.Size(41, 38)
        Me.CmbSalir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'PicTopBar
        '
        Me.PicTopBar.Image = CType(resources.GetObject("PicTopBar.Image"), System.Drawing.Image)
        Me.PicTopBar.Location = New System.Drawing.Point(42, 0)
        Me.PicTopBar.Name = "PicTopBar"
        Me.PicTopBar.Size = New System.Drawing.Size(198, 38)
        Me.PicTopBar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'lblActualizar
        '
        Me.lblActualizar.BackColor = System.Drawing.Color.Gainsboro
        Me.lblActualizar.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Regular)
        Me.lblActualizar.Location = New System.Drawing.Point(16, 114)
        Me.lblActualizar.Name = "lblActualizar"
        Me.lblActualizar.Size = New System.Drawing.Size(72, 26)
        Me.lblActualizar.Text = "Actualizar"
        Me.lblActualizar.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblActualizar.Visible = False
        '
        'CmdActualizar
        '
        Me.CmdActualizar.BackColor = System.Drawing.Color.Gainsboro
        Me.CmdActualizar.Image = CType(resources.GetObject("CmdActualizar.Image"), System.Drawing.Image)
        Me.CmdActualizar.Location = New System.Drawing.Point(28, 63)
        Me.CmdActualizar.Name = "CmdActualizar"
        Me.CmdActualizar.Size = New System.Drawing.Size(48, 47)
        Me.CmdActualizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.CmdActualizar.Visible = False
        '
        'TimerReload
        '
        Me.TimerReload.Interval = 1250
        '
        'formMenuPrincipal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.Gainsboro
        Me.ClientSize = New System.Drawing.Size(240, 300)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblActualizar)
        Me.Controls.Add(Me.CmdActualizar)
        Me.Controls.Add(Me.lblModLog)
        Me.Controls.Add(Me.CmdModLogisticaAlmacenaje)
        Me.Controls.Add(Me.lblValidacionFacturaPOS1)
        Me.Controls.Add(Me.cmbValidacionFacturaPOS)
        Me.Controls.Add(Me.lblConsulta)
        Me.Controls.Add(Me.cmbConsulta)
        Me.Controls.Add(Me.lblHabladores)
        Me.Controls.Add(Me.cmbHabladores)
        Me.Controls.Add(Me.lblEtiquetas)
        Me.Controls.Add(Me.cmbEtiquetas)
        Me.Controls.Add(Me.lblStatus_MenuPrincipal)
        Me.Controls.Add(Me.lblInventario)
        Me.Controls.Add(Me.cmbInventario)
        Me.Controls.Add(Me.CmbSalir)
        Me.Controls.Add(Me.PicTopBar)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "formMenuPrincipal"
        Me.Text = "isMOBILE - Menú Principal"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PicTopBar As System.Windows.Forms.PictureBox
    Friend WithEvents CmbSalir As System.Windows.Forms.PictureBox
    Friend WithEvents lblInventario As System.Windows.Forms.Label
    Friend WithEvents cmbInventario As System.Windows.Forms.PictureBox
    Friend WithEvents lblStatus_MenuPrincipal As System.Windows.Forms.Label
    Friend WithEvents cmbEtiquetas As System.Windows.Forms.PictureBox
    Friend WithEvents lblEtiquetas As System.Windows.Forms.Label
    Friend WithEvents cmbHabladores As System.Windows.Forms.PictureBox
    Friend WithEvents lblHabladores As System.Windows.Forms.Label
    Friend WithEvents cmbConsulta As System.Windows.Forms.PictureBox
    Friend WithEvents lblConsulta As System.Windows.Forms.Label
    Friend WithEvents cmbValidacionFacturaPOS As System.Windows.Forms.PictureBox
    Friend WithEvents lblValidacionFacturaPOS1 As System.Windows.Forms.Label
    Friend WithEvents lblModLog As System.Windows.Forms.Label
    Friend WithEvents CmdModLogisticaAlmacenaje As System.Windows.Forms.PictureBox
    Friend WithEvents lblActualizar As System.Windows.Forms.Label
    Friend WithEvents CmdActualizar As System.Windows.Forms.PictureBox
    Friend WithEvents TimerReload As System.Windows.Forms.Timer
End Class
