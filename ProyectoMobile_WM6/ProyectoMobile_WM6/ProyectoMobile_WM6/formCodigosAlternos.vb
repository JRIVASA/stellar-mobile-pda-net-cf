﻿Public Class formCodigosAlternos

    Private Sub hideButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hideButton.Click
        FormConsultaProductos.Enabled = True
        Me.Hide()
    End Sub

    Private Sub salirButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles salirButton.Click
        FormConsultaProductos.Enabled = True
        Me.Hide()
    End Sub

    Private Sub formCodigosAlternos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        AjustarFormularios(Me)
    End Sub

End Class