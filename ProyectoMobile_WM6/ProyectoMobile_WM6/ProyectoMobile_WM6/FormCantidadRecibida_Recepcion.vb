﻿Public Class FormCantidadRecibida_Recepcion

    Dim tipoPeso As Integer
    Dim FormaCargada As Boolean

    Private Sub FormCantidadRecibida_Recepcion_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Activated

        If Not FormaCargada Then

            FormaCargada = True

            TimerReload.Enabled = False
            TimerReload.Interval = 100
            TimerReload.Enabled = True

        End If

    End Sub

    Private Sub FormCantidadRecibida_Recepcion_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        lblUsuarioEnSesion.Text = "Usuario: " + descripcionUsuario
        Me.Refresh()
        Me.Update()

        AjustarFormularios(Me)

    End Sub

    Private Sub cmbSiguiente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSiguiente.Click

        RegladeNegocio = "Recepcion_FormadeTrabajo"
        dataset_RegladeNegocio = consultarRegladeNegocio(RegladeNegocio)

        If (dataset_RegladeNegocio Is Nothing) Then
            MsgBox("Ha ocurrido un error al cargar las reglas de negocio.", MsgBoxStyle.Information, "isMOBILE")
            cmbSalir_Click(Nothing, Nothing)
        ElseIf (dataset_RegladeNegocio.Tables(0).Rows.Count <= 0) Then
            MsgBox("No existe la Regla de Negocio Recepcion_FormadeTrabajo.", MsgBoxStyle.Information, "isMOBILE")
            cmbSalir_Click(Nothing, Nothing)
        End If

        valorRegladeNegocio = dataset_RegladeNegocio.Tables(0).Rows(0)("valor")

        If valorRegladeNegocio = "0" Then 'Trabaja solo con unidades

            If txtUnidadesRecibidas.Text = "" Then
                MsgBox("Debe ingresar la cantidad de unidades recibidas.", MsgBoxStyle.Information, "isMOBILE")
                txtUnidadesRecibidas.Focus()
            Else

                If Not IsNumeric(txtUnidadesRecibidas.Text) Then
                    MsgBox("Los datos deben ser numéricos.", MsgBoxStyle.Information, "isMOBILE")
                    txtUnidadesRecibidas.Text = ""
                    txtUnidadesRecibidas.Focus()
                Else

                    txtEmpaquesRecibidos.Text = 0 'A pesar de que no está visible, esto es para usar esta variable en el form de GuardarDatos
                    unidadesRecibidas = CDbl(txtUnidadesRecibidas.Text)

                    If cantidadSolicitada = 0 Then 'quiere decir que no se tiene ODC o que el producto no se encuentra en la ODC

                        cantidadUnidadesPermitidas = unidadesRecibidas 'Esto es para que NO entre en el IF de unidadesRecibidas > cantidadSolicitada
                        MostrarFormCantidadFacturadaSinODC()

                        If verFormCantidadFacturadaSinODC = False Then
                            MsgBox("Este producto ya fue recibido anteriormente, no es necesario colocar la cantidad facturada.", MsgBoxStyle.Information, "Cantidad Facturada Procesada")
                            FormGuardarDatos_Recepcion.lblUnidadesxEmpaque.Text = "Uni. x Emp.: " + CStr(UnidadesxEmpaque) 'trae la informacion de el FormLecturaProducto
                            dataset_Productos = detallesProducto(FormLecturaProducto_Recepcion.txtCodigoProducto.Text)
                            FormGuardarDatos_Recepcion.lblDescripcionProducto.Text = "Producto: " + dataset_Productos.Tables(0).Rows(0)("c_descri")
                            FormGuardarDatos_Recepcion.lblCantidadEmpaquesRecibidos.Text = txtEmpaquesRecibidos.Text
                            FormGuardarDatos_Recepcion.lblCantidadUnidadesRecibidas.Text = txtUnidadesRecibidas.Text
                            'Voy a mostrar la cantidad facturada ingresada anteriormente, tanto empaques como unidades
                            FormGuardarDatos_Recepcion.lblCantidadEmpaquesFacturados.Text = EmpaquesFacturadosNoProcesados_PDA + EmpaquesFacturadosNoProcesados_SERVIDOR
                            FormGuardarDatos_Recepcion.lblCantidadUnidadesFacturadas.Text = UnidadesFacturadasNoProcesadas_PDA + UnidadesFacturadasNoProcesadas_SERVIDOR
                            'FormGuardarDatos_Recepcion.Show()
                            FormUnidadesxEmpaque_Recepcion.Show()
                            Me.Visible = False
                        Else
                            FormUnidadesxEmpaque_Recepcion.Show()
                            Me.Visible = False
                        End If

                    Else
                        cantidadEmpaquesPermitidos = cantidadFaltanteFormularios / UnidadesxEmpaque
                        cantidadUnidadesPermitidas = ((cantidadEmpaquesPermitidos - empaquesRecibidos) * UnidadesxEmpaque) + (cantidadFaltanteFormularios Mod UnidadesxEmpaque)
                    End If

                    If unidadesRecibidas > cantidadUnidadesPermitidas Then
                        permiteRecibirporEncimadeODC()
                    Else 'si las unidades recibidas no son mayores que la cantidad solicitada
                        'Verificar aqui si hay una regla de negocio que diga algo de si permite recibir por debajo de la
                        'cantidad solicitada

                        If verFormCantidadFacturada = False Then
                            MsgBox("Este producto ya fue recibido anteriormente, no es necesario colocar la cantidad facturada.", MsgBoxStyle.Information, "Cantidad Facturada Procesada")
                            FormGuardarDatos_Recepcion.lblUnidadesxEmpaque.Text = "Uni. x Emp.: " + CStr(UnidadesxEmpaque) 'trae la informacion de el FormLecturaProducto
                            dataset_Productos = detallesProducto(FormLecturaProducto_Recepcion.txtCodigoProducto.Text)
                            FormGuardarDatos_Recepcion.lblDescripcionProducto.Text = "Producto: " + dataset_Productos.Tables(0).Rows(0)("c_descri")
                            FormGuardarDatos_Recepcion.lblCantidadEmpaquesRecibidos.Text = txtEmpaquesRecibidos.Text
                            FormGuardarDatos_Recepcion.lblCantidadUnidadesRecibidas.Text = txtUnidadesRecibidas.Text
                            'Voy a mostrar la cantidad facturada ingresada anteriormente, tanto empaques como unidades
                            FormGuardarDatos_Recepcion.lblCantidadEmpaquesFacturados.Text = EmpaquesFacturadosNoProcesados_PDA + EmpaquesFacturadosNoProcesados_SERVIDOR
                            FormGuardarDatos_Recepcion.lblCantidadUnidadesFacturadas.Text = UnidadesFacturadasNoProcesadas_PDA + UnidadesFacturadasNoProcesadas_SERVIDOR

                            FormGuardarDatos_Recepcion.Show()
                            'FormUnidadesxEmpaque_Recepcion.Show
                            Me.Visible = False
                        Else
                            FormCantidadFacturada_Recepcion.Show()
                            'FormUnidadesxEmpaque_Recepcion.Show() No muestro el formulario de confirmar unidades x empaque porque no recibe empaques
                            Me.Visible = False
                        End If

                    End If
                End If
            End If
        Else 'Si el valor de la regla de negocio no es cero (0)
            If valorRegladeNegocio = "1" Then 'Trabaja solo con Empaques
                If txtEmpaquesRecibidos.Text = "" Then
                    MsgBox("Debe ingresar la cantidad de empaques recibidos.", MsgBoxStyle.Information, "isMOBILE")
                    txtEmpaquesRecibidos.Focus()
                Else
                    If Not IsNumeric(txtEmpaquesRecibidos.Text) Then
                        MsgBox("Los datos deben ser numéricos.", MsgBoxStyle.Information, "isMOBILE")
                        txtEmpaquesRecibidos.Text = ""
                        txtEmpaquesRecibidos.Focus()
                    Else
                        txtUnidadesRecibidas.Text = 0
                        empaquesRecibidos = CDbl(txtEmpaquesRecibidos.Text)

                        If cantidadSolicitada = 0 Then 'quiere decir que no se tiene ODC
                            cantidadEmpaquesPermitidos = empaquesRecibidos 'Para que no entre en el if de empaquesRecibidos> cantidadEmpaquesPermitidos

                            MostrarFormCantidadFacturadaSinODC()

                            If verFormCantidadFacturadaSinODC = False Then
                                MsgBox("Este producto ya fue recibido anteriormente, no es necesario colocar la cantidad facturada.", MsgBoxStyle.Information, "Cantidad Facturada Procesada")
                                FormGuardarDatos_Recepcion.lblUnidadesxEmpaque.Text = "Uni. x Emp.: " + CStr(UnidadesxEmpaque) 'trae la informacion de el FormLecturaProducto
                                dataset_Productos = detallesProducto(FormLecturaProducto_Recepcion.txtCodigoProducto.Text)
                                FormGuardarDatos_Recepcion.lblDescripcionProducto.Text = "Producto: " + dataset_Productos.Tables(0).Rows(0)("c_descri")
                                FormGuardarDatos_Recepcion.lblCantidadEmpaquesRecibidos.Text = txtEmpaquesRecibidos.Text
                                FormGuardarDatos_Recepcion.lblCantidadUnidadesRecibidas.Text = txtUnidadesRecibidas.Text
                                'Voy a mostrar la cantidad facturada ingresada anteriormente, tanto empaques como unidades
                                FormGuardarDatos_Recepcion.lblCantidadEmpaquesFacturados.Text = EmpaquesFacturadosNoProcesados_PDA + EmpaquesFacturadosNoProcesados_SERVIDOR
                                FormGuardarDatos_Recepcion.lblCantidadUnidadesFacturadas.Text = UnidadesFacturadasNoProcesadas_PDA + UnidadesFacturadasNoProcesadas_SERVIDOR
                                'FormGuardarDatos_Recepcion.Show()
                                FormUnidadesxEmpaque_Recepcion.Show()
                                Me.Visible = False
                            Else
                                FormUnidadesxEmpaque_Recepcion.Show()
                                Me.Visible = False
                            End If

                        Else
                            cantidadEmpaquesPermitidos = cantidadFaltanteFormularios / UnidadesxEmpaque 'la variable UnidadesxEmpaques tiene la informacion que viene 
                            'desde el formulario lecturaProducto, igualmente con cantidadSolicitadaFormulario
                        End If

                        If empaquesRecibidos > cantidadEmpaquesPermitidos Then
                            permiteRecibirporEncimadeODC()
                        Else 'si los empaques recibidos no son mayores que los empaques permitidos
                            'Verificar aqui si hay una regla de negocio que diga algo de si permite recibir por debajo de la
                            'cantidad solicitada
                            If verFormCantidadFacturada = False Then
                                MsgBox("Este producto ya fue recibido anteriormente, no es necesario colocar la cantidad facturada.", MsgBoxStyle.Information, "Cantidad Facturada Procesada")
                                FormGuardarDatos_Recepcion.lblUnidadesxEmpaque.Text = "Uni. x Emp.: " + CStr(UnidadesxEmpaque) 'trae la informacion de el FormLecturaProducto
                                dataset_Productos = detallesProducto(FormLecturaProducto_Recepcion.txtCodigoProducto.Text)
                                FormGuardarDatos_Recepcion.lblDescripcionProducto.Text = "Producto: " + dataset_Productos.Tables(0).Rows(0)("c_descri")
                                FormGuardarDatos_Recepcion.lblCantidadEmpaquesRecibidos.Text = txtEmpaquesRecibidos.Text
                                FormGuardarDatos_Recepcion.lblCantidadUnidadesRecibidas.Text = txtUnidadesRecibidas.Text
                                'Voy a mostrar la cantidad facturada ingresada anteriormente, tanto empaques como unidades
                                FormGuardarDatos_Recepcion.lblCantidadEmpaquesFacturados.Text = EmpaquesFacturadosNoProcesados_PDA + EmpaquesFacturadosNoProcesados_SERVIDOR
                                FormGuardarDatos_Recepcion.lblCantidadUnidadesFacturadas.Text = UnidadesFacturadasNoProcesadas_PDA + UnidadesFacturadasNoProcesadas_SERVIDOR
                                'FormGuardarDatos_Recepcion.Show()
                                FormUnidadesxEmpaque_Recepcion.Show()
                                Me.Visible = False
                            Else
                                'FormCantidadFacturada_Recepcion.Show()
                                FormUnidadesxEmpaque_Recepcion.Show()
                                Me.Visible = False
                            End If

                        End If
                    End If
                End If
            Else 'si el valor de la regla de negocio es igual a 2 (Trabaja con unidades y/o empaques)
                'hay 4 opciones:
                '1.- Que tanto empaques como unidades estén vacíos
                If txtEmpaquesRecibidos.Text = "" And txtUnidadesRecibidas.Text = "" Then
                    MsgBox("Debe ingresar la cantidad de empaques y/o unidades recibidas.", MsgBoxStyle.Information, "isMOBILE")
                    txtEmpaquesRecibidos.Focus()
                Else
                    '2.- Que EmpaquesRecibidos esté vacío y UnidadesRecibidas esté lleno
                    If (txtEmpaquesRecibidos.Text = "" And txtUnidadesRecibidas.Text <> "") Then

                        If Not IsNumeric(txtUnidadesRecibidas.Text) Then
                            MsgBox("Los datos deben ser numéricos.", MsgBoxStyle.Information, "isMOBILE")
                            txtUnidadesRecibidas.Text = ""
                            txtUnidadesRecibidas.Focus()
                        Else
                            txtEmpaquesRecibidos.Text = 0

                            empaquesRecibidos = CDbl(txtEmpaquesRecibidos.Text)

                            unidadesRecibidas = CDbl(txtUnidadesRecibidas.Text)

                            If cantidadSolicitada = 0 Then 'quiere decir que no se tiene ODC
                                cantidadUnidadesPermitidas = unidadesRecibidas

                                MostrarFormCantidadFacturadaSinODC()

                                If verFormCantidadFacturadaSinODC = False Then
                                    MsgBox("Este producto ya fue recibido anteriormente, no es necesario colocar la cantidad facturada", MsgBoxStyle.Information, "Cantidad Facturada Procesada")
                                    FormGuardarDatos_Recepcion.lblUnidadesxEmpaque.Text = "Uni. x Emp.: " + CStr(UnidadesxEmpaque) 'trae la informacion de el FormLecturaProducto
                                    dataset_Productos = detallesProducto(FormLecturaProducto_Recepcion.txtCodigoProducto.Text)
                                    FormGuardarDatos_Recepcion.lblDescripcionProducto.Text = "Producto: " + dataset_Productos.Tables(0).Rows(0)("c_descri")
                                    FormGuardarDatos_Recepcion.lblCantidadEmpaquesRecibidos.Text = txtEmpaquesRecibidos.Text
                                    FormGuardarDatos_Recepcion.lblCantidadUnidadesRecibidas.Text = txtUnidadesRecibidas.Text
                                    'Voy a mostrar la cantidad facturada ingresada anteriormente, tanto empaques como unidades
                                    FormGuardarDatos_Recepcion.lblCantidadEmpaquesFacturados.Text = EmpaquesFacturadosNoProcesados_PDA + EmpaquesFacturadosNoProcesados_SERVIDOR
                                    FormGuardarDatos_Recepcion.lblCantidadUnidadesFacturadas.Text = UnidadesFacturadasNoProcesadas_PDA + UnidadesFacturadasNoProcesadas_SERVIDOR
                                    'FormGuardarDatos_Recepcion.Show()
                                    FormUnidadesxEmpaque_Recepcion.Show()
                                    Me.Visible = False
                                Else
                                    FormUnidadesxEmpaque_Recepcion.Show()
                                    Me.Visible = False
                                End If

                            Else
                                cantidadUnidadesPermitidas = ((cantidadEmpaquesPermitidos - empaquesRecibidos) * UnidadesxEmpaque) + (cantidadFaltanteFormularios Mod UnidadesxEmpaque)
                            End If

                            If unidadesRecibidas > cantidadUnidadesPermitidas Then
                                permiteRecibirporEncimadeODC()
                            Else 'si las unidades recibidas no son mayores que la cantidad solicitada (Son menores o iguales)
                                'Verificar aqui si hay una regla de negocio que diga algo de si permite recibir por debajo de la
                                'cantidad solicitada
                                If verFormCantidadFacturada = False Then
                                    MsgBox("Este producto ya fue recibido anteriormente, no es necesario colocar la cantidad facturada.", MsgBoxStyle.Information, "Cantidad Facturada Procesada")
                                    FormGuardarDatos_Recepcion.lblUnidadesxEmpaque.Text = "Uni. x Emp.: " + CStr(UnidadesxEmpaque) 'trae la informacion de el FormLecturaProducto
                                    dataset_Productos = detallesProducto(FormLecturaProducto_Recepcion.txtCodigoProducto.Text)
                                    FormGuardarDatos_Recepcion.lblDescripcionProducto.Text = "Producto: " + dataset_Productos.Tables(0).Rows(0)("c_descri")
                                    FormGuardarDatos_Recepcion.lblCantidadEmpaquesRecibidos.Text = txtEmpaquesRecibidos.Text
                                    FormGuardarDatos_Recepcion.lblCantidadUnidadesRecibidas.Text = txtUnidadesRecibidas.Text
                                    'Voy a mostrar la cantidad facturada ingresada anteriormente, tanto empaques como unidades
                                    FormGuardarDatos_Recepcion.lblCantidadEmpaquesFacturados.Text = EmpaquesFacturadosNoProcesados_PDA + EmpaquesFacturadosNoProcesados_SERVIDOR
                                    FormGuardarDatos_Recepcion.lblCantidadUnidadesFacturadas.Text = UnidadesFacturadasNoProcesadas_PDA + UnidadesFacturadasNoProcesadas_SERVIDOR
                                    'FormGuardarDatos_Recepcion.Show()
                                    FormUnidadesxEmpaque_Recepcion.Show()
                                    Me.Visible = False
                                Else
                                    'FormCantidadFacturada_Recepcion.Show()
                                    FormUnidadesxEmpaque_Recepcion.Show()
                                    Me.Visible = False
                                End If

                            End If
                        End If
                    Else
                        '3.- Que EmpaquesRecibidos esté lleno y UnidadesRecibidas esté vacío 
                        If (txtEmpaquesRecibidos.Text <> "" And txtUnidadesRecibidas.Text = "") Then

                            If Not IsNumeric(txtEmpaquesRecibidos.Text) Then
                                MsgBox("Los datos deben ser numéricos.", MsgBoxStyle.Information, "isMOBILE")
                                txtEmpaquesRecibidos.Text = ""
                                txtEmpaquesRecibidos.Focus()
                            Else
                                txtUnidadesRecibidas.Text = 0

                                empaquesRecibidos = CDbl(txtEmpaquesRecibidos.Text)

                                unidadesRecibidas = CDbl(txtUnidadesRecibidas.Text)

                                If cantidadSolicitada = 0 Then 'quiere decir que no se tiene ODC
                                    cantidadEmpaquesPermitidos = empaquesRecibidos

                                    MostrarFormCantidadFacturadaSinODC()

                                    If verFormCantidadFacturadaSinODC = False Then
                                        MsgBox("Este producto ya fue recibido anteriormente, no es necesario colocar la cantidad facturada.", MsgBoxStyle.Information, "Cantidad Facturada Procesada")
                                        FormGuardarDatos_Recepcion.lblUnidadesxEmpaque.Text = "Uni. x Emp.: " + CStr(UnidadesxEmpaque) 'trae la informacion de el FormLecturaProducto
                                        dataset_Productos = detallesProducto(FormLecturaProducto_Recepcion.txtCodigoProducto.Text)
                                        FormGuardarDatos_Recepcion.lblDescripcionProducto.Text = "Producto: " + dataset_Productos.Tables(0).Rows(0)("c_descri")
                                        FormGuardarDatos_Recepcion.lblCantidadEmpaquesRecibidos.Text = txtEmpaquesRecibidos.Text
                                        FormGuardarDatos_Recepcion.lblCantidadUnidadesRecibidas.Text = txtUnidadesRecibidas.Text
                                        'Voy a mostrar la cantidad facturada ingresada anteriormente, tanto empaques como unidades
                                        FormGuardarDatos_Recepcion.lblCantidadEmpaquesFacturados.Text = EmpaquesFacturadosNoProcesados_PDA + EmpaquesFacturadosNoProcesados_SERVIDOR
                                        FormGuardarDatos_Recepcion.lblCantidadUnidadesFacturadas.Text = UnidadesFacturadasNoProcesadas_PDA + UnidadesFacturadasNoProcesadas_SERVIDOR
                                        'FormGuardarDatos_Recepcion.Show()
                                        FormUnidadesxEmpaque_Recepcion.Show()
                                        Me.Visible = False
                                    Else
                                        FormUnidadesxEmpaque_Recepcion.Show()
                                        Me.Visible = False
                                    End If

                                Else
                                    cantidadEmpaquesPermitidos = cantidadFaltanteFormularios / UnidadesxEmpaque
                                End If

                                If empaquesRecibidos > cantidadEmpaquesPermitidos Then
                                    permiteRecibirporEncimadeODC()
                                Else 'si los empaques recibidos no son mayores que los empaques permitidos(Son menores o iguales)
                                    'Verificar aqui si hay una regla de negocio que diga algo de si permite recibir por debajo de la
                                    'cantidad solicitada 
                                    If verFormCantidadFacturada = False Then
                                        MsgBox("Este producto ya fue recibido anteriormente, no es necesario colocar la cantidad facturada.", MsgBoxStyle.Information, "Cantidad Facturada Procesada")
                                        FormGuardarDatos_Recepcion.lblUnidadesxEmpaque.Text = "Uni. x Emp.: " + CStr(UnidadesxEmpaque) 'trae la informacion de el FormLecturaProducto
                                        dataset_Productos = detallesProducto(FormLecturaProducto_Recepcion.txtCodigoProducto.Text)
                                        FormGuardarDatos_Recepcion.lblDescripcionProducto.Text = "Producto: " + dataset_Productos.Tables(0).Rows(0)("c_descri")
                                        FormGuardarDatos_Recepcion.lblCantidadEmpaquesRecibidos.Text = txtEmpaquesRecibidos.Text
                                        FormGuardarDatos_Recepcion.lblCantidadUnidadesRecibidas.Text = txtUnidadesRecibidas.Text
                                        'Voy a mostrar la cantidad facturada ingresada anteriormente, tanto empaques como unidades
                                        FormGuardarDatos_Recepcion.lblCantidadEmpaquesFacturados.Text = EmpaquesFacturadosNoProcesados_PDA + EmpaquesFacturadosNoProcesados_SERVIDOR
                                        FormGuardarDatos_Recepcion.lblCantidadUnidadesFacturadas.Text = UnidadesFacturadasNoProcesadas_PDA + UnidadesFacturadasNoProcesadas_SERVIDOR
                                        'FormGuardarDatos_Recepcion.Show()
                                        FormUnidadesxEmpaque_Recepcion.Show()
                                        Me.Visible = False
                                    Else
                                        'FormCantidadFacturada_Recepcion.Show()
                                        FormUnidadesxEmpaque_Recepcion.Show()
                                        Me.Visible = False
                                    End If

                                End If
                            End If
                        Else
                            '4.- Que tanto EmpaquesRecibidos como UnidadesRecibidas estén llenos 

                            If (txtEmpaquesRecibidos.Text <> "" And txtUnidadesRecibidas.Text <> "") Then

                                If IsNumeric(txtUnidadesRecibidas.Text) Then
                                    If IsNumeric(txtEmpaquesRecibidos.Text) Then

                                        empaquesRecibidos = CDbl(txtEmpaquesRecibidos.Text)
                                        unidadesRecibidas = CDbl(txtUnidadesRecibidas.Text)

                                        If cantidadSolicitada = 0 Then 'quiere decir que no se tiene ODC
                                            cantidadEmpaquesPermitidos = empaquesRecibidos 'Para que no entre en el if de empaques recibidos > cantidadEmpaquesPermitidos

                                            MostrarFormCantidadFacturadaSinODC()

                                            If verFormCantidadFacturadaSinODC = False Then
                                                MsgBox("Este producto ya fue recibido anteriormente, no es necesario colocar la cantidad facturada.", MsgBoxStyle.Information, "Cantidad Facturada Procesada")
                                                FormGuardarDatos_Recepcion.lblUnidadesxEmpaque.Text = "Uni. x Emp.: " + CStr(UnidadesxEmpaque) 'trae la informacion de el FormLecturaProducto
                                                dataset_Productos = detallesProducto(FormLecturaProducto_Recepcion.txtCodigoProducto.Text)
                                                FormGuardarDatos_Recepcion.lblDescripcionProducto.Text = "Producto: " + dataset_Productos.Tables(0).Rows(0)("c_descri")
                                                FormGuardarDatos_Recepcion.lblCantidadEmpaquesRecibidos.Text = txtEmpaquesRecibidos.Text
                                                FormGuardarDatos_Recepcion.lblCantidadUnidadesRecibidas.Text = txtUnidadesRecibidas.Text
                                                'Voy a mostrar la cantidad facturada ingresada anteriormente, tanto empaques como unidades
                                                FormGuardarDatos_Recepcion.lblCantidadEmpaquesFacturados.Text = EmpaquesFacturadosNoProcesados_PDA + EmpaquesFacturadosNoProcesados_SERVIDOR
                                                FormGuardarDatos_Recepcion.lblCantidadUnidadesFacturadas.Text = UnidadesFacturadasNoProcesadas_PDA + UnidadesFacturadasNoProcesadas_SERVIDOR
                                                'FormGuardarDatos_Recepcion.Show()
                                                FormUnidadesxEmpaque_Recepcion.Show()
                                                Me.Visible = False
                                            Else
                                                FormUnidadesxEmpaque_Recepcion.Show()
                                                Me.Visible = False
                                            End If

                                        Else
                                            cantidadEmpaquesPermitidos = cantidadFaltanteFormularios / UnidadesxEmpaque
                                        End If

                                        If empaquesRecibidos < cantidadEmpaquesPermitidos Then 'Puede recibir unidades adicionales
                                            cantidadUnidadesPermitidas = ((cantidadEmpaquesPermitidos - empaquesRecibidos) * UnidadesxEmpaque) + (cantidadFaltanteFormularios Mod UnidadesxEmpaque)
                                            If unidadesRecibidas > cantidadUnidadesPermitidas Then
                                                permiteRecibirporEncimadeODC()
                                            Else 'si las unidades recibidas no son mayores que la cantidad solicitada (Son menores o iguales)
                                                'Verificar aqui si hay una regla de negocio que diga algo de si permite recibir por debajo de la
                                                'cantidad solicitada
                                                If verFormCantidadFacturada = False Then
                                                    MsgBox("Este producto ya fue recibido anteriormente, no es necesario colocar la cantidad facturada.", MsgBoxStyle.Information, "Cantidad Facturada Procesada")
                                                    FormGuardarDatos_Recepcion.lblUnidadesxEmpaque.Text = "Uni. x Emp.: " + CStr(UnidadesxEmpaque) 'trae la informacion de el FormLecturaProducto
                                                    dataset_Productos = detallesProducto(FormLecturaProducto_Recepcion.txtCodigoProducto.Text)
                                                    FormGuardarDatos_Recepcion.lblDescripcionProducto.Text = "Producto: " + dataset_Productos.Tables(0).Rows(0)("c_descri")
                                                    FormGuardarDatos_Recepcion.lblCantidadEmpaquesRecibidos.Text = txtEmpaquesRecibidos.Text
                                                    FormGuardarDatos_Recepcion.lblCantidadUnidadesRecibidas.Text = txtUnidadesRecibidas.Text
                                                    'Voy a mostrar la cantidad facturada ingresada anteriormente, tanto empaques como unidades
                                                    FormGuardarDatos_Recepcion.lblCantidadEmpaquesFacturados.Text = EmpaquesFacturadosNoProcesados_PDA + EmpaquesFacturadosNoProcesados_SERVIDOR
                                                    FormGuardarDatos_Recepcion.lblCantidadUnidadesFacturadas.Text = UnidadesFacturadasNoProcesadas_PDA + UnidadesFacturadasNoProcesadas_SERVIDOR
                                                    'FormGuardarDatos_Recepcion.Show()
                                                    FormUnidadesxEmpaque_Recepcion.Show()
                                                    Me.Visible = False
                                                Else
                                                    'FormCantidadFacturada_Recepcion.Show()
                                                    FormUnidadesxEmpaque_Recepcion.Show()
                                                    Me.Visible = False
                                                End If

                                            End If
                                        Else 'Si empaques recibidos no es menor a cantidadEmpaquesPermitidos
                                            If empaquesRecibidos > cantidadEmpaquesPermitidos Then
                                                cantidadUnidadesPermitidas = ((cantidadEmpaquesPermitidos - empaquesRecibidos) * UnidadesxEmpaque) + (cantidadFaltanteFormularios Mod UnidadesxEmpaque)
                                                If unidadesRecibidas > cantidadUnidadesPermitidas Then
                                                    permiteRecibirporEncimadeODC()
                                                Else 'si las unidades recibidas no son mayores que la cantidad solicitada (Son menores o iguales)
                                                    'Verificar aqui si hay una regla de negocio que diga algo de si permite recibir por debajo de la
                                                    'cantidad solicitada
                                                    If verFormCantidadFacturada = False Then
                                                        MsgBox("Este producto ya fue recibido anteriormente, no es necesario colocar la cantidad facturada.", MsgBoxStyle.Information, "Cantidad Facturada Procesada")
                                                        FormGuardarDatos_Recepcion.lblUnidadesxEmpaque.Text = "Uni. x Emp.: " + CStr(UnidadesxEmpaque) 'trae la informacion de el FormLecturaProducto
                                                        FormGuardarDatos_Recepcion.lblDescripcionProducto.Text = dataset_Productos.Tables(0).Rows(0)("c_descri")
                                                        dataset_Productos = detallesProducto(FormLecturaProducto_Recepcion.txtCodigoProducto.Text)
                                                        FormGuardarDatos_Recepcion.lblDescripcionProducto.Text = "Producto: " + dataset_Productos.Tables(0).Rows(0)("c_descri")
                                                        FormGuardarDatos_Recepcion.lblCantidadUnidadesRecibidas.Text = txtUnidadesRecibidas.Text
                                                        'Voy a mostrar la cantidad facturada ingresada anteriormente, tanto empaques como unidades
                                                        FormGuardarDatos_Recepcion.lblCantidadEmpaquesFacturados.Text = EmpaquesFacturadosNoProcesados_PDA + EmpaquesFacturadosNoProcesados_SERVIDOR
                                                        FormGuardarDatos_Recepcion.lblCantidadUnidadesFacturadas.Text = UnidadesFacturadasNoProcesadas_PDA + UnidadesFacturadasNoProcesadas_SERVIDOR
                                                        'FormGuardarDatos_Recepcion.Show()
                                                        FormUnidadesxEmpaque_Recepcion.Show()
                                                        Me.Visible = False
                                                    Else
                                                        'FormCantidadFacturada_Recepcion.Show()
                                                        FormUnidadesxEmpaque_Recepcion.Show()
                                                        Me.Visible = False
                                                    End If

                                                End If
                                            Else ' empaques recibidos = empaques permitidos
                                                cantidadUnidadesPermitidas = ((cantidadEmpaquesPermitidos - empaquesRecibidos) * UnidadesxEmpaque) + (cantidadFaltanteFormularios Mod UnidadesxEmpaque)
                                                If unidadesRecibidas > cantidadUnidadesPermitidas Then
                                                    permiteRecibirporEncimadeODC()
                                                Else 'si las unidades recibidas no son mayores que la cantidad solicitada (Son menores o iguales)
                                                    'Verificar aqui si hay una regla de negocio que diga algo de si permite recibir por debajo de la
                                                    'cantidad solicitada
                                                    If verFormCantidadFacturada = False Then
                                                        MsgBox("Este producto ya fue recibido anteriormente, no es necesario colocar la cantidad facturada.", MsgBoxStyle.Information, "Cantidad Facturada Procesada")
                                                        FormGuardarDatos_Recepcion.lblUnidadesxEmpaque.Text = "Uni. x Emp.: " + CStr(UnidadesxEmpaque) 'trae la informacion de el FormLecturaProducto
                                                        dataset_Productos = detallesProducto(FormLecturaProducto_Recepcion.txtCodigoProducto.Text)
                                                        FormGuardarDatos_Recepcion.lblDescripcionProducto.Text = "Producto: " + dataset_Productos.Tables(0).Rows(0)("c_descri")
                                                        FormGuardarDatos_Recepcion.lblCantidadEmpaquesRecibidos.Text = txtEmpaquesRecibidos.Text
                                                        FormGuardarDatos_Recepcion.lblCantidadUnidadesRecibidas.Text = txtUnidadesRecibidas.Text
                                                        'Voy a mostrar la cantidad facturada ingresada anteriormente, tanto empaques como unidades
                                                        FormGuardarDatos_Recepcion.lblCantidadEmpaquesFacturados.Text = EmpaquesFacturadosNoProcesados_PDA + EmpaquesFacturadosNoProcesados_SERVIDOR
                                                        FormGuardarDatos_Recepcion.lblCantidadUnidadesFacturadas.Text = UnidadesFacturadasNoProcesadas_PDA + UnidadesFacturadasNoProcesadas_SERVIDOR
                                                        'FormGuardarDatos_Recepcion.Show()
                                                        FormUnidadesxEmpaque_Recepcion.Show()
                                                        Me.Visible = False
                                                    Else
                                                        'FormCantidadFacturada_Recepcion.Show()
                                                        FormUnidadesxEmpaque_Recepcion.Show()
                                                        Me.Visible = False
                                                    End If

                                                End If
                                            End If
                                        End If
                                    Else
                                        MsgBox("Los datos deben ser numéricos", MsgBoxStyle.Information, "isMOBILE")
                                        txtEmpaquesRecibidos.Text = ""
                                        txtEmpaquesRecibidos.Focus()
                                    End If
                                Else
                                    MsgBox("Los datos deben ser numéricos", MsgBoxStyle.Information, "isMOBILE")
                                    txtUnidadesRecibidas.Text = ""
                                    txtUnidadesRecibidas.Focus()
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub cmbSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSalir.Click
        FormaCargada = False
        FormLecturaProducto_Recepcion.Show()
        Me.Visible = False
    End Sub

    Private Sub txtEmpaquesRecibidos_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtEmpaquesRecibidos.KeyPress
        'If Char.IsNumber(e.KeyChar) Then 'Comentado, no permitía ingresar . para valores decimales
        'MsgBox("-!" & txtEmpaquesRecibidos.Text & e.KeyChar.ToString & "!-") Nueva forma de Validar Aprobada.
        If IsNumeric(txtEmpaquesRecibidos.Text & e.KeyChar.ToString) Then
            e.Handled = False
        Else
            If Char.IsControl(e.KeyChar) Then
                e.Handled = False
            Else
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txtEmpaquesRecibidos_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEmpaquesRecibidos.LostFocus
        Dim TmpCantDec As Short
        Dim TmpText As String
        If IsNumeric(txtEmpaquesRecibidos.Text) And Not (dataset_Productos Is Nothing) Then
            If dataset_Productos.Tables(0).Rows.Count <> 0 Then
                TmpText = txtEmpaquesRecibidos.Text
                TmpCantDec = CShort(Val(dataset_Productos.Tables(0).Rows(0)("CANT_DECIMALES")))
                txtEmpaquesRecibidos.Text = FormatNumber(txtEmpaquesRecibidos.Text, TmpCantDec, TriState.True, TriState.False, TriState.False)
                If TmpText <> txtEmpaquesRecibidos.Text Then txtEmpaquesRecibidos.Focus()
            End If
        End If
    End Sub

    Private Sub txtUnidadesRecibidas_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtUnidadesRecibidas.KeyPress
        'If Char.IsNumber(e.KeyChar) Then 'Comentado, no permitía ingresar . para valores decimales
        'MsgBox("-!" & txtUnidadesRecibidas.Text & e.KeyChar.ToString & "!-") Nueva forma de Validar Aprobada.
        If IsNumeric(txtUnidadesRecibidas.Text & e.KeyChar.ToString) Then
            e.Handled = False
        Else
            If Char.IsControl(e.KeyChar) Then
                e.Handled = False
            Else
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txtUnidadesRecibidas_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUnidadesRecibidas.LostFocus
        Dim TmpCantDec As Short
        Dim TmpText As String
        If IsNumeric(txtUnidadesRecibidas.Text) And Not (dataset_Productos Is Nothing) Then
            If dataset_Productos.Tables(0).Rows.Count <> 0 Then
                TmpText = txtUnidadesRecibidas.Text
                TmpCantDec = CShort(Val(dataset_Productos.Tables(0).Rows(0)("CANT_DECIMALES")))
                txtUnidadesRecibidas.Text = FormatNumber(txtUnidadesRecibidas.Text, TmpCantDec, TriState.True, TriState.False, TriState.False)
                If TmpText <> txtUnidadesRecibidas.Text Then txtUnidadesRecibidas.Focus()
            End If
        End If
    End Sub

    Private Sub Activation()

        lblFechaRecepcion.Text = Date.Now.ToString
        RegladeNegocio = "Recepcion_FormadeTrabajo"

        dataset_RegladeNegocio = consultarRegladeNegocio(RegladeNegocio)

        If dataset_RegladeNegocio.Tables(0).Rows.Count = 0 Then
            MsgBox("No existe la Regla de Negocio Recepcion_FormadeTrabajo.", MsgBoxStyle.Information, "isMOBILE")
        Else
            valorRegladeNegocio = dataset_RegladeNegocio.Tables(0).Rows(0)("valor")
            If valorRegladeNegocio = "0" Then 'Trabaja solo con unidades
                txtEmpaquesRecibidos.Visible = False
                lblEmpaquesRecibidos.Visible = False
                lblUnidadesxEmpaque.Visible = False
                lblEmpaquesSolicitados.Visible = False
                txtUnidadesRecibidas.Focus()
            Else
                If valorRegladeNegocio = "1" Then 'Trabaja solo con empaques
                    txtUnidadesRecibidas.Visible = False
                    lblUnidadesRecibidas.Visible = False
                    lblUnidadesSolicitadas.Visible = False
                    txtEmpaquesRecibidos.Focus()
                Else 'Trabaja con empaques y unidades
                    txtEmpaquesRecibidos.Focus()
                End If
            End If
        End If

        dataset_Productos = detallesProducto(FormLecturaProducto_Recepcion.txtCodigoProducto.Text)

    End Sub

    Private Sub TimerReload_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TimerReload.Tick
        TimerReload.Enabled = False
        Activation()
    End Sub

End Class