﻿Public Class FormUnidadesxEmpaque_Recepcion

    Private Sub cmbAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbAceptar.Click

        FormCantidadFacturada_Recepcion.lblUnidadesxEmpaque.Text = "Uni.x Emp.: " + txtUnidadesxEmpaque.Text
        UnidadesxEmpaque = txtUnidadesxEmpaque.Text

        If verFormCantidadFacturada = False Or verFormCantidadFacturadaSinODC = False Then
            FormGuardarDatos_Recepcion.Show()
            Me.Visible = False
        Else
            FormCantidadFacturada_Recepcion.Show()
            Me.Visible = False
        End If

        txtUnidadesxEmpaque.Enabled = False

    End Sub

    Private Sub cmbModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbModificar.Click
        txtUnidadesxEmpaque.Enabled = True
        txtUnidadesxEmpaque.Text = ""
        txtUnidadesxEmpaque.Focus()
    End Sub

    Private Sub FormUnidadesxEmpaque_Recepcion_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblUsuarioEnSesion.Text = "Usuario: " + descripcionUsuario
        Me.Refresh()
        Me.Update()
        AjustarFormularios(Me)
    End Sub

    Private Sub cmbSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSalir.Click
        FormCantidadRecibida_Recepcion.Show()
        Me.Visible = False
    End Sub
End Class