﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class FormLecturaProducto_Traslado
    Inherits System.Windows.Forms.Form

    'Form invalida a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer
    Private mainMenu1 As System.Windows.Forms.MainMenu

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar con el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormLecturaProducto_Traslado))
        Me.mainMenu1 = New System.Windows.Forms.MainMenu
        Me.picBarraMorada = New System.Windows.Forms.PictureBox
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.lblUsuarioEnSesion = New System.Windows.Forms.Label
        Me.cmbSalir = New System.Windows.Forms.PictureBox
        Me.PictureFondoProveedorDeposito_Recepcion = New System.Windows.Forms.PictureBox
        Me.txtEmpaques = New System.Windows.Forms.TextBox
        Me.lblEmpaques = New System.Windows.Forms.Label
        Me.lblUnidades = New System.Windows.Forms.Label
        Me.txtUnidades = New System.Windows.Forms.TextBox
        Me.lblCantidadProducto = New System.Windows.Forms.Label
        Me.txtCodigoProducto = New System.Windows.Forms.TextBox
        Me.lblCodigoProducto = New System.Windows.Forms.Label
        Me.lblDescripcion = New System.Windows.Forms.Label
        Me.cmbAgregar = New System.Windows.Forms.Button
        Me.cmbFinalizar = New System.Windows.Forms.Button
        Me.lblUnidadesxEmpaque = New System.Windows.Forms.Label
        Me.lblNºTrs = New System.Windows.Forms.Label
        Me.lblTrs = New System.Windows.Forms.Label
        Me.ProgressBar_FormLecturaProducto_Traslado = New System.Windows.Forms.ProgressBar
        Me.cmbActualizar = New System.Windows.Forms.PictureBox
        Me.TimerReload = New System.Windows.Forms.Timer
        Me.SuspendLayout()
        '
        'picBarraMorada
        '
        Me.picBarraMorada.Image = CType(resources.GetObject("picBarraMorada.Image"), System.Drawing.Image)
        Me.picBarraMorada.Location = New System.Drawing.Point(0, 259)
        Me.picBarraMorada.Name = "picBarraMorada"
        Me.picBarraMorada.Size = New System.Drawing.Size(240, 40)
        Me.picBarraMorada.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Gainsboro
        Me.PictureBox1.Location = New System.Drawing.Point(0, 57)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(240, 203)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'lblUsuarioEnSesion
        '
        Me.lblUsuarioEnSesion.BackColor = System.Drawing.Color.DarkGray
        Me.lblUsuarioEnSesion.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblUsuarioEnSesion.Location = New System.Drawing.Point(0, 39)
        Me.lblUsuarioEnSesion.Name = "lblUsuarioEnSesion"
        Me.lblUsuarioEnSesion.Size = New System.Drawing.Size(240, 15)
        Me.lblUsuarioEnSesion.Text = "Usuario:"
        '
        'cmbSalir
        '
        Me.cmbSalir.Image = CType(resources.GetObject("cmbSalir.Image"), System.Drawing.Image)
        Me.cmbSalir.Location = New System.Drawing.Point(0, 0)
        Me.cmbSalir.Name = "cmbSalir"
        Me.cmbSalir.Size = New System.Drawing.Size(41, 38)
        Me.cmbSalir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'PictureFondoProveedorDeposito_Recepcion
        '
        Me.PictureFondoProveedorDeposito_Recepcion.Image = CType(resources.GetObject("PictureFondoProveedorDeposito_Recepcion.Image"), System.Drawing.Image)
        Me.PictureFondoProveedorDeposito_Recepcion.Location = New System.Drawing.Point(42, 0)
        Me.PictureFondoProveedorDeposito_Recepcion.Name = "PictureFondoProveedorDeposito_Recepcion"
        Me.PictureFondoProveedorDeposito_Recepcion.Size = New System.Drawing.Size(198, 38)
        Me.PictureFondoProveedorDeposito_Recepcion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'txtEmpaques
        '
        Me.txtEmpaques.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtEmpaques.Location = New System.Drawing.Point(128, 208)
        Me.txtEmpaques.MaxLength = 10
        Me.txtEmpaques.Name = "txtEmpaques"
        Me.txtEmpaques.Size = New System.Drawing.Size(63, 21)
        Me.txtEmpaques.TabIndex = 23
        '
        'lblEmpaques
        '
        Me.lblEmpaques.BackColor = System.Drawing.Color.Gainsboro
        Me.lblEmpaques.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.lblEmpaques.Location = New System.Drawing.Point(38, 208)
        Me.lblEmpaques.Name = "lblEmpaques"
        Me.lblEmpaques.Size = New System.Drawing.Size(84, 23)
        Me.lblEmpaques.Text = "Empaques"
        Me.lblEmpaques.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblUnidades
        '
        Me.lblUnidades.BackColor = System.Drawing.Color.Gainsboro
        Me.lblUnidades.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.lblUnidades.Location = New System.Drawing.Point(50, 235)
        Me.lblUnidades.Name = "lblUnidades"
        Me.lblUnidades.Size = New System.Drawing.Size(70, 24)
        Me.lblUnidades.Text = "Unidades"
        Me.lblUnidades.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtUnidades
        '
        Me.txtUnidades.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.txtUnidades.Location = New System.Drawing.Point(128, 235)
        Me.txtUnidades.MaxLength = 10
        Me.txtUnidades.Name = "txtUnidades"
        Me.txtUnidades.Size = New System.Drawing.Size(63, 21)
        Me.txtUnidades.TabIndex = 22
        '
        'lblCantidadProducto
        '
        Me.lblCantidadProducto.BackColor = System.Drawing.Color.Gainsboro
        Me.lblCantidadProducto.Font = New System.Drawing.Font("Verdana", 11.0!, System.Drawing.FontStyle.Bold)
        Me.lblCantidadProducto.Location = New System.Drawing.Point(27, 186)
        Me.lblCantidadProducto.Name = "lblCantidadProducto"
        Me.lblCantidadProducto.Size = New System.Drawing.Size(186, 20)
        Me.lblCantidadProducto.Text = "Cantidad Producto"
        Me.lblCantidadProducto.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txtCodigoProducto
        '
        Me.txtCodigoProducto.BackColor = System.Drawing.Color.White
        Me.txtCodigoProducto.Font = New System.Drawing.Font("Verdana", 11.0!, System.Drawing.FontStyle.Bold)
        Me.txtCodigoProducto.Location = New System.Drawing.Point(27, 103)
        Me.txtCodigoProducto.MaxLength = 50
        Me.txtCodigoProducto.Name = "txtCodigoProducto"
        Me.txtCodigoProducto.Size = New System.Drawing.Size(186, 24)
        Me.txtCodigoProducto.TabIndex = 71
        '
        'lblCodigoProducto
        '
        Me.lblCodigoProducto.BackColor = System.Drawing.Color.Gainsboro
        Me.lblCodigoProducto.Font = New System.Drawing.Font("Verdana", 11.0!, System.Drawing.FontStyle.Bold)
        Me.lblCodigoProducto.Location = New System.Drawing.Point(27, 82)
        Me.lblCodigoProducto.Name = "lblCodigoProducto"
        Me.lblCodigoProducto.Size = New System.Drawing.Size(186, 18)
        Me.lblCodigoProducto.Text = "Codigo Producto"
        Me.lblCodigoProducto.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblDescripcion
        '
        Me.lblDescripcion.BackColor = System.Drawing.Color.Gainsboro
        Me.lblDescripcion.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Regular)
        Me.lblDescripcion.Location = New System.Drawing.Point(15, 128)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(209, 39)
        Me.lblDescripcion.Text = "Descripción:"
        '
        'cmbAgregar
        '
        Me.cmbAgregar.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.cmbAgregar.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.cmbAgregar.ForeColor = System.Drawing.Color.Black
        Me.cmbAgregar.Location = New System.Drawing.Point(146, 265)
        Me.cmbAgregar.Name = "cmbAgregar"
        Me.cmbAgregar.Size = New System.Drawing.Size(78, 25)
        Me.cmbAgregar.TabIndex = 74
        Me.cmbAgregar.Text = "Agregar"
        '
        'cmbFinalizar
        '
        Me.cmbFinalizar.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.cmbFinalizar.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.cmbFinalizar.ForeColor = System.Drawing.Color.Black
        Me.cmbFinalizar.Location = New System.Drawing.Point(15, 265)
        Me.cmbFinalizar.Name = "cmbFinalizar"
        Me.cmbFinalizar.Size = New System.Drawing.Size(78, 25)
        Me.cmbFinalizar.TabIndex = 75
        Me.cmbFinalizar.Text = "Finalizar"
        '
        'lblUnidadesxEmpaque
        '
        Me.lblUnidadesxEmpaque.BackColor = System.Drawing.Color.Gainsboro
        Me.lblUnidadesxEmpaque.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular)
        Me.lblUnidadesxEmpaque.Location = New System.Drawing.Point(15, 167)
        Me.lblUnidadesxEmpaque.Name = "lblUnidadesxEmpaque"
        Me.lblUnidadesxEmpaque.Size = New System.Drawing.Size(198, 15)
        Me.lblUnidadesxEmpaque.Text = "Uni. x Emp.:"
        '
        'lblNºTrs
        '
        Me.lblNºTrs.BackColor = System.Drawing.Color.Gainsboro
        Me.lblNºTrs.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold)
        Me.lblNºTrs.Location = New System.Drawing.Point(67, 60)
        Me.lblNºTrs.Name = "lblNºTrs"
        Me.lblNºTrs.Size = New System.Drawing.Size(132, 15)
        '
        'lblTrs
        '
        Me.lblTrs.BackColor = System.Drawing.Color.Gainsboro
        Me.lblTrs.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold)
        Me.lblTrs.Location = New System.Drawing.Point(11, 60)
        Me.lblTrs.Name = "lblTrs"
        Me.lblTrs.Size = New System.Drawing.Size(61, 15)
        Me.lblTrs.Text = "Nº Trs.:"
        '
        'ProgressBar_FormLecturaProducto_Traslado
        '
        Me.ProgressBar_FormLecturaProducto_Traslado.Location = New System.Drawing.Point(27, 149)
        Me.ProgressBar_FormLecturaProducto_Traslado.Name = "ProgressBar_FormLecturaProducto_Traslado"
        Me.ProgressBar_FormLecturaProducto_Traslado.Size = New System.Drawing.Size(186, 10)
        Me.ProgressBar_FormLecturaProducto_Traslado.Visible = False
        '
        'cmbActualizar
        '
        Me.cmbActualizar.Image = CType(resources.GetObject("cmbActualizar.Image"), System.Drawing.Image)
        Me.cmbActualizar.Location = New System.Drawing.Point(106, 265)
        Me.cmbActualizar.Name = "cmbActualizar"
        Me.cmbActualizar.Size = New System.Drawing.Size(24, 24)
        Me.cmbActualizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'TimerReload
        '
        Me.TimerReload.Interval = 1250
        '
        'FormLecturaProducto_Traslado
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(240, 300)
        Me.ControlBox = False
        Me.Controls.Add(Me.cmbActualizar)
        Me.Controls.Add(Me.ProgressBar_FormLecturaProducto_Traslado)
        Me.Controls.Add(Me.lblNºTrs)
        Me.Controls.Add(Me.lblTrs)
        Me.Controls.Add(Me.lblUnidadesxEmpaque)
        Me.Controls.Add(Me.cmbAgregar)
        Me.Controls.Add(Me.cmbFinalizar)
        Me.Controls.Add(Me.lblCantidadProducto)
        Me.Controls.Add(Me.txtCodigoProducto)
        Me.Controls.Add(Me.lblCodigoProducto)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.txtEmpaques)
        Me.Controls.Add(Me.lblEmpaques)
        Me.Controls.Add(Me.lblUnidades)
        Me.Controls.Add(Me.txtUnidades)
        Me.Controls.Add(Me.picBarraMorada)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.lblUsuarioEnSesion)
        Me.Controls.Add(Me.cmbSalir)
        Me.Controls.Add(Me.PictureFondoProveedorDeposito_Recepcion)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.Name = "FormLecturaProducto_Traslado"
        Me.Text = "isMOBILE - Traslados"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents picBarraMorada As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents lblUsuarioEnSesion As System.Windows.Forms.Label
    Friend WithEvents cmbSalir As System.Windows.Forms.PictureBox
    Friend WithEvents PictureFondoProveedorDeposito_Recepcion As System.Windows.Forms.PictureBox
    Friend WithEvents txtEmpaques As System.Windows.Forms.TextBox
    Friend WithEvents lblEmpaques As System.Windows.Forms.Label
    Friend WithEvents lblUnidades As System.Windows.Forms.Label
    Friend WithEvents txtUnidades As System.Windows.Forms.TextBox
    Friend WithEvents lblCantidadProducto As System.Windows.Forms.Label
    Friend WithEvents txtCodigoProducto As System.Windows.Forms.TextBox
    Friend WithEvents lblCodigoProducto As System.Windows.Forms.Label
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents cmbAgregar As System.Windows.Forms.Button
    Friend WithEvents cmbFinalizar As System.Windows.Forms.Button
    Friend WithEvents lblUnidadesxEmpaque As System.Windows.Forms.Label
    Friend WithEvents lblNºTrs As System.Windows.Forms.Label
    Friend WithEvents lblTrs As System.Windows.Forms.Label
    Friend WithEvents cmbTeclado As System.Windows.Forms.PictureBox
    Friend WithEvents ProgressBar_FormLecturaProducto_Traslado As System.Windows.Forms.ProgressBar
    Friend WithEvents cmbActualizar As System.Windows.Forms.PictureBox
    Friend WithEvents TimerReload As System.Windows.Forms.Timer
End Class
