﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class FormPendientes_InventarioFisico
    Inherits System.Windows.Forms.Form

    'Form invalida a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer
    Private mainMenu1 As System.Windows.Forms.MainMenu

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar con el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormPendientes_InventarioFisico))
        Me.mainMenu1 = New System.Windows.Forms.MainMenu
        Me.PictureFondoPENDIENTES = New System.Windows.Forms.PictureBox
        Me.cmbSalir = New System.Windows.Forms.PictureBox
        Me.lblUsuarioEnSesion = New System.Windows.Forms.Label
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.picBarraMorada = New System.Windows.Forms.PictureBox
        Me.cmbEnviarPendientes = New System.Windows.Forms.PictureBox
        Me.lblDocPendientes = New System.Windows.Forms.Label
        Me.cmbTodosPendientes = New System.Windows.Forms.PictureBox
        Me.DataDocPendientes = New System.Windows.Forms.DataGrid
        Me.PanelPendientes = New System.Windows.Forms.Panel
        Me.PanelPendientes.SuspendLayout()
        Me.SuspendLayout()
        '
        'PictureFondoPENDIENTES
        '
        Me.PictureFondoPENDIENTES.Image = CType(resources.GetObject("PictureFondoPENDIENTES.Image"), System.Drawing.Image)
        Me.PictureFondoPENDIENTES.Location = New System.Drawing.Point(42, 0)
        Me.PictureFondoPENDIENTES.Name = "PictureFondoPENDIENTES"
        Me.PictureFondoPENDIENTES.Size = New System.Drawing.Size(198, 39)
        Me.PictureFondoPENDIENTES.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'cmbSalir
        '
        Me.cmbSalir.Image = CType(resources.GetObject("cmbSalir.Image"), System.Drawing.Image)
        Me.cmbSalir.Location = New System.Drawing.Point(0, 0)
        Me.cmbSalir.Name = "cmbSalir"
        Me.cmbSalir.Size = New System.Drawing.Size(41, 39)
        Me.cmbSalir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'lblUsuarioEnSesion
        '
        Me.lblUsuarioEnSesion.BackColor = System.Drawing.Color.DarkGray
        Me.lblUsuarioEnSesion.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblUsuarioEnSesion.Location = New System.Drawing.Point(0, 40)
        Me.lblUsuarioEnSesion.Name = "lblUsuarioEnSesion"
        Me.lblUsuarioEnSesion.Size = New System.Drawing.Size(240, 15)
        Me.lblUsuarioEnSesion.Text = "Usuario:"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Gainsboro
        Me.PictureBox1.Location = New System.Drawing.Point(0, 56)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(240, 203)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'picBarraMorada
        '
        Me.picBarraMorada.Image = CType(resources.GetObject("picBarraMorada.Image"), System.Drawing.Image)
        Me.picBarraMorada.Location = New System.Drawing.Point(0, 260)
        Me.picBarraMorada.Name = "picBarraMorada"
        Me.picBarraMorada.Size = New System.Drawing.Size(240, 40)
        Me.picBarraMorada.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'cmbEnviarPendientes
        '
        Me.cmbEnviarPendientes.Image = CType(resources.GetObject("cmbEnviarPendientes.Image"), System.Drawing.Image)
        Me.cmbEnviarPendientes.Location = New System.Drawing.Point(19, 172)
        Me.cmbEnviarPendientes.Name = "cmbEnviarPendientes"
        Me.cmbEnviarPendientes.Size = New System.Drawing.Size(55, 55)
        Me.cmbEnviarPendientes.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'lblDocPendientes
        '
        Me.lblDocPendientes.BackColor = System.Drawing.Color.Gainsboro
        Me.lblDocPendientes.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.lblDocPendientes.Location = New System.Drawing.Point(36, 64)
        Me.lblDocPendientes.Name = "lblDocPendientes"
        Me.lblDocPendientes.Size = New System.Drawing.Size(167, 22)
        Me.lblDocPendientes.Text = "Doc. Pendientes"
        Me.lblDocPendientes.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'cmbTodosPendientes
        '
        Me.cmbTodosPendientes.Image = CType(resources.GetObject("cmbTodosPendientes.Image"), System.Drawing.Image)
        Me.cmbTodosPendientes.Location = New System.Drawing.Point(19, 105)
        Me.cmbTodosPendientes.Name = "cmbTodosPendientes"
        Me.cmbTodosPendientes.Size = New System.Drawing.Size(55, 55)
        Me.cmbTodosPendientes.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'DataDocPendientes
        '
        Me.DataDocPendientes.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.DataDocPendientes.Font = New System.Drawing.Font("Verdana", 8.0!, System.Drawing.FontStyle.Bold)
        Me.DataDocPendientes.Location = New System.Drawing.Point(80, 89)
        Me.DataDocPendientes.Name = "DataDocPendientes"
        Me.DataDocPendientes.RowHeadersVisible = False
        Me.DataDocPendientes.Size = New System.Drawing.Size(147, 160)
        Me.DataDocPendientes.TabIndex = 0
        '
        'PanelPendientes
        '
        Me.PanelPendientes.BackColor = System.Drawing.Color.White
        Me.PanelPendientes.Controls.Add(Me.DataDocPendientes)
        Me.PanelPendientes.Controls.Add(Me.cmbTodosPendientes)
        Me.PanelPendientes.Controls.Add(Me.lblDocPendientes)
        Me.PanelPendientes.Controls.Add(Me.cmbEnviarPendientes)
        Me.PanelPendientes.Controls.Add(Me.picBarraMorada)
        Me.PanelPendientes.Controls.Add(Me.PictureBox1)
        Me.PanelPendientes.Controls.Add(Me.lblUsuarioEnSesion)
        Me.PanelPendientes.Controls.Add(Me.cmbSalir)
        Me.PanelPendientes.Controls.Add(Me.PictureFondoPENDIENTES)
        Me.PanelPendientes.Location = New System.Drawing.Point(0, 0)
        Me.PanelPendientes.Name = "PanelPendientes"
        Me.PanelPendientes.Size = New System.Drawing.Size(240, 300)
        '
        'FormPendientes_InventarioFisico
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(240, 300)
        Me.ControlBox = False
        Me.Controls.Add(Me.PanelPendientes)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormPendientes_InventarioFisico"
        Me.Text = "isMOBILE - Documentos Pendientes"
        Me.PanelPendientes.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PictureFondoPENDIENTES As System.Windows.Forms.PictureBox
    Friend WithEvents cmbSalir As System.Windows.Forms.PictureBox
    Friend WithEvents lblUsuarioEnSesion As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents picBarraMorada As System.Windows.Forms.PictureBox
    Friend WithEvents cmbEnviarPendientes As System.Windows.Forms.PictureBox
    Friend WithEvents lblDocPendientes As System.Windows.Forms.Label
    Friend WithEvents cmbTodosPendientes As System.Windows.Forms.PictureBox
    Friend WithEvents DataDocPendientes As System.Windows.Forms.DataGrid
    Friend WithEvents PanelPendientes As System.Windows.Forms.Panel
End Class
