﻿Public Class FormPickingUserMain

    Private FormaCargada As Boolean

    Private TmpFrm As FormLecturaProducto_Picking

    Private Sub DataGridProductos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridProductos.Click

        If DataGridProductos.VisibleRowCount <= 0 Then Exit Sub

        Dim Row As Integer : Row = DataGridProductos.CurrentRowIndex
        Dim Col As Integer : Col = DataGridProductos.CurrentCell.ColumnNumber

        If Col <> 0 Then
            ' Descripcion. No hacer nada para que se muestre el tooltip.
        Else

            ' Ir al producto.

            'If DebugMode Then
            'MsgBox(DataGridProductos.Item(Row, 0).ToString)
            'MsgBox(DataGridProductos.Item(Row, 1).ToString)
            'MsgBox(DataGridProductos.Item(Row, 2).ToString)
            'MsgBox(DataGridProductos.Item(Row, 3).ToString)
            'MsgBox(DataGridProductos.Item(Row, 4).ToString)
            'MsgBox(DataGridProductos.Item(Row, 5).ToString)
            'MsgBox(DataGridProductos.Item(Row, 6).ToString)
            'End If

            If TmpFrm Is Nothing Then TmpFrm = New FormLecturaProducto_Picking()

            'TmpFrm = FormLecturaProducto_Picking

            'If DebugMode Then MsgBox("Asignando Descripcion")
            TmpFrm.lblDescripcion.Text = CStr(DataGridProductos.Item(Row, 1))
            'If DebugMode Then MsgBox("Asignando Codigo")
            TmpFrm.lblDescripcion.Tag = CStr(DataGridProductos.Item(Row, 3)) ' Codigo
            'If DebugMode Then MsgBox("Asignando Decimales")
            TmpFrm.CantDecimalesProducto = DataGridProductos.Item(Row, 6)

            '
            'If DebugMode Then MsgBox("Asignando Ubicacion")
            TmpFrm.txtUbicaciones.Text = CStr(DataGridProductos.Item(Row, 2))

            '
            'If DebugMode Then MsgBox("Asignando Cant. Sol.")
            TmpFrm.txtCantSolicitada.Text = FormatNumber(DataGridProductos.Item(Row, 4), _
            TmpFrm.CantDecimalesProducto, TriState.True, TriState.False, TriState.False)

            TmpFrm.txtCantSolicitada.ReadOnly = True

            '
            'If DebugMode Then MsgBox("Asignando Cant. Rec. Editable: " & DataGridProductos.Item(Row, 5).ToString & " (" & FormatNumber(DataGridProductos.Item(Row, 5), TmpFrm.CantDecimalesProducto, TriState.True, TriState.False, TriState.False) & ")")
            TmpFrm.txtCantRecolectada.Text = FormatNumber(DataGridProductos.Item(Row, 5), _
            TmpFrm.CantDecimalesProducto, TriState.True, TriState.False, TriState.False)
            'If DebugMode Then MsgBox("Asignando Cant. Rec. Consolidada")
            TmpFrm.txtCantRecolectada.Tag = TmpFrm.txtCantRecolectada.Text

            TmpFrm.txtCantRecolectada.ReadOnly = False

            FormaCargada = False
            TimerReload.Enabled = False
            TimerReload.Interval = 500

            'If DebugMode Then MsgBox("Cargando Lectura Producto")
            TmpFrm.FormaCargada = False
            TmpFrm.Show()

            Me.Visible = False

        End If

    End Sub

    Private Sub FormPickingUserMain_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Activated

        If Not FormaCargada Then

            FormaCargada = True

            lblUsuarioEnSesion.Text = "Usuario: " + descripcionUsuario

            txtCodigoProducto.Focus()

            TimerReload.Enabled = False
            TimerReload.Enabled = True

        End If

    End Sub

    Private Sub FormPickingUserMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        AjustarFormularios(Me)
        AjustarFormularios(Me.PanelInventario_Inv_Activo)

    End Sub

    Private Sub cmbSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSalir.Click
        salirProcesoModLog(Me)
        If SalioDelProceso Then
            FormaCargada = False
            txtCodigoProducto.Text = String.Empty
            DataGridProductos.DataSource = Nothing
            TimerReload.Interval = 500
            TimerReload.Enabled = False
        End If
    End Sub

    Private Sub TimerReload_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TimerReload.Tick
        TimerReload.Enabled = False
        If TimerReload.Interval = 500 Then ' Iniciando. Despues colocarlo cada 3 minutos.
            TimerReload.Interval = 180000
        End If
        lblGridTooltip.Text = "Cargando..."
        CargarDataGridPickingUser(CStr(datasetUsuario.Tables(0).Rows(0)("codusuario")), Me)
        DataGridProductos.Focus()
        lblGridTooltip.Text = "Lista Actualizada."
        If DataGridProductos.VisibleRowCount <= 0 Then ProcesarBusqueda()
        TimerReload.Enabled = True
    End Sub

    Private Sub DataGridProductos_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles DataGridProductos.KeyPress
        txtCodigoProducto.Focus()
        txtCodigoProducto.Text = e.KeyChar.ToString
        txtCodigoProducto.SelectionStart = Len(txtCodigoProducto.Text)
    End Sub

    Private Sub DataGridProductos_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles DataGridProductos.MouseMove

        If DataGridProductos.VisibleRowCount > 0 Then

            Dim HoverRow As Integer : HoverRow = DataGridProductos.HitTest(e.X, e.Y).Row

            If HoverRow >= 0 Then
                lblGridTooltip.Text = CStr(DataGridProductos.Item(HoverRow, 1)) & vbNewLine & _
                CStr(DataGridProductos.Item(HoverRow, 2))
            End If

        End If

    End Sub

    Private Sub txtCodigoProducto_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCodigoProducto.GotFocus
        SeleccionarTexto(txtCodigoProducto)
    End Sub

    Private Sub txtCodigoProducto_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCodigoProducto.KeyDown
        If e.KeyCode = Keys.Return Then
            TimerBuscarProducto_Tick(Nothing, Nothing)
        End If
    End Sub

    Private Sub txtCodigoProducto_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCodigoProducto.TextChanged

        TimerBuscarProducto.Enabled = False ' Reset Timer while Typing...
        TimerBuscarProducto.Enabled = True

    End Sub

    Private Sub ProcesarBusqueda()

        If DataGridProductos.VisibleRowCount > 0 Then

            lblGridTooltip.Text = "Buscando Producto..."
            lblGridTooltip.Refresh()

            Cursor.Current = Cursors.WaitCursor

            Me.Enabled = False

            'Call ActualizacionParcial("Productos") ' Mejor no... potea la aplicación y despues se quejan...

            Cursor.Current = Cursors.Default

            Dim CodProducto As String, DSCodigo As DataSet

            DSCodigo = buscarCodigoProducto(txtCodigoProducto.Text)

            If DSCodigo.Tables(0).Rows.Count = 0 Then
                lblGridTooltip.Text = "Código de producto ingresado no existe."
            Else

                Dim Rows() As DataRow, Row As Integer, DsProductos As DataTable

                CodProducto = DSCodigo.Tables(0).Rows(0).Item("c_CodNasa")

                DsProductos = CType(DataGridProductos.DataSource, DataTable)

                Rows = DsProductos _
                .Select("CodProducto = '" & CodProducto & "'")

                If Rows.Length > 0 Then ' Check

                    Row = DsProductos.Rows.IndexOf(Rows(0))

                    DataGridProductos.Focus()
                    DataGridProductos.Select(Row)
                    DataGridProductos.CurrentCell = New DataGridCell(Row, 0)
                    DataGridProductos_Click(DataGridProductos, Nothing)

                Else
                    lblGridTooltip.Text = "Producto no encontrado."
                End If

            End If

            Me.Enabled = True

            txtCodigoProducto.Focus()

            Cursor.Current = Cursors.Default

        Else
            lblGridTooltip.Text = "El usuario no tiene productos pendientes por recolectar."
        End If

    End Sub

    Private Sub TimerBuscarProducto_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TimerBuscarProducto.Tick
        TimerBuscarProducto.Enabled = False
        ProcesarBusqueda()
    End Sub

    Private Sub cmbActualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbActualizar.Click
        TimerReload_Tick(TimerReload, Nothing)
    End Sub

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

End Class