﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class FormPickingUserMain
    Inherits System.Windows.Forms.Form

    'Form invalida a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer
    Private mainMenu1 As System.Windows.Forms.MainMenu

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar con el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormPickingUserMain))
        Me.mainMenu1 = New System.Windows.Forms.MainMenu
        Me.PanelInventario_Inv_Activo = New System.Windows.Forms.Panel
        Me.cmbActualizar = New System.Windows.Forms.PictureBox
        Me.lblBGActualizar = New System.Windows.Forms.Label
        Me.lblGridTooltip = New System.Windows.Forms.Label
        Me.txtCodigoProducto = New System.Windows.Forms.TextBox
        Me.lblBuscarCodigo = New System.Windows.Forms.Label
        Me.DataGridProductos = New System.Windows.Forms.DataGrid
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.picBarraMorada = New System.Windows.Forms.PictureBox
        Me.lblUsuarioEnSesion = New System.Windows.Forms.Label
        Me.cmbSalir = New System.Windows.Forms.PictureBox
        Me.PictureFondoINVENTARIO_INV_ACTIVO = New System.Windows.Forms.PictureBox
        Me.TimerReload = New System.Windows.Forms.Timer
        Me.TimerBuscarProducto = New System.Windows.Forms.Timer
        Me.PanelInventario_Inv_Activo.SuspendLayout()
        Me.SuspendLayout()
        '
        'PanelInventario_Inv_Activo
        '
        Me.PanelInventario_Inv_Activo.BackColor = System.Drawing.Color.White
        Me.PanelInventario_Inv_Activo.Controls.Add(Me.cmbActualizar)
        Me.PanelInventario_Inv_Activo.Controls.Add(Me.lblBGActualizar)
        Me.PanelInventario_Inv_Activo.Controls.Add(Me.lblGridTooltip)
        Me.PanelInventario_Inv_Activo.Controls.Add(Me.txtCodigoProducto)
        Me.PanelInventario_Inv_Activo.Controls.Add(Me.lblBuscarCodigo)
        Me.PanelInventario_Inv_Activo.Controls.Add(Me.DataGridProductos)
        Me.PanelInventario_Inv_Activo.Controls.Add(Me.PictureBox1)
        Me.PanelInventario_Inv_Activo.Controls.Add(Me.picBarraMorada)
        Me.PanelInventario_Inv_Activo.Controls.Add(Me.lblUsuarioEnSesion)
        Me.PanelInventario_Inv_Activo.Controls.Add(Me.cmbSalir)
        Me.PanelInventario_Inv_Activo.Controls.Add(Me.PictureFondoINVENTARIO_INV_ACTIVO)
        Me.PanelInventario_Inv_Activo.Location = New System.Drawing.Point(0, 0)
        Me.PanelInventario_Inv_Activo.Name = "PanelInventario_Inv_Activo"
        Me.PanelInventario_Inv_Activo.Size = New System.Drawing.Size(240, 300)
        '
        'cmbActualizar
        '
        Me.cmbActualizar.Image = CType(resources.GetObject("cmbActualizar.Image"), System.Drawing.Image)
        Me.cmbActualizar.Location = New System.Drawing.Point(207, 267)
        Me.cmbActualizar.Name = "cmbActualizar"
        Me.cmbActualizar.Size = New System.Drawing.Size(24, 24)
        Me.cmbActualizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'lblBGActualizar
        '
        Me.lblBGActualizar.BackColor = System.Drawing.Color.FromArgb(CType(CType(95, Byte), Integer), CType(CType(66, Byte), Integer), CType(CType(134, Byte), Integer))
        Me.lblBGActualizar.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.lblBGActualizar.Location = New System.Drawing.Point(197, 260)
        Me.lblBGActualizar.Name = "lblBGActualizar"
        Me.lblBGActualizar.Size = New System.Drawing.Size(43, 40)
        Me.lblBGActualizar.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblGridTooltip
        '
        Me.lblGridTooltip.BackColor = System.Drawing.Color.FromArgb(CType(CType(95, Byte), Integer), CType(CType(66, Byte), Integer), CType(CType(134, Byte), Integer))
        Me.lblGridTooltip.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.lblGridTooltip.Location = New System.Drawing.Point(0, 260)
        Me.lblGridTooltip.Name = "lblGridTooltip"
        Me.lblGridTooltip.Size = New System.Drawing.Size(201, 40)
        Me.lblGridTooltip.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txtCodigoProducto
        '
        Me.txtCodigoProducto.BackColor = System.Drawing.Color.White
        Me.txtCodigoProducto.Font = New System.Drawing.Font("Verdana", 11.0!, System.Drawing.FontStyle.Bold)
        Me.txtCodigoProducto.HideSelection = False
        Me.txtCodigoProducto.Location = New System.Drawing.Point(27, 87)
        Me.txtCodigoProducto.MaxLength = 50
        Me.txtCodigoProducto.Name = "txtCodigoProducto"
        Me.txtCodigoProducto.Size = New System.Drawing.Size(186, 24)
        Me.txtCodigoProducto.TabIndex = 73
        '
        'lblBuscarCodigo
        '
        Me.lblBuscarCodigo.BackColor = System.Drawing.Color.Gainsboro
        Me.lblBuscarCodigo.Font = New System.Drawing.Font("Verdana", 11.0!, System.Drawing.FontStyle.Bold)
        Me.lblBuscarCodigo.Location = New System.Drawing.Point(27, 66)
        Me.lblBuscarCodigo.Name = "lblBuscarCodigo"
        Me.lblBuscarCodigo.Size = New System.Drawing.Size(186, 18)
        Me.lblBuscarCodigo.Text = "Buscar Código"
        Me.lblBuscarCodigo.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'DataGridProductos
        '
        Me.DataGridProductos.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.DataGridProductos.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold)
        Me.DataGridProductos.HeaderForeColor = System.Drawing.Color.Black
        Me.DataGridProductos.Location = New System.Drawing.Point(7, 117)
        Me.DataGridProductos.Name = "DataGridProductos"
        Me.DataGridProductos.RowHeadersVisible = False
        Me.DataGridProductos.Size = New System.Drawing.Size(225, 137)
        Me.DataGridProductos.TabIndex = 0
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Gainsboro
        Me.PictureBox1.Location = New System.Drawing.Point(0, 56)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(240, 205)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'picBarraMorada
        '
        Me.picBarraMorada.Image = CType(resources.GetObject("picBarraMorada.Image"), System.Drawing.Image)
        Me.picBarraMorada.Location = New System.Drawing.Point(0, 260)
        Me.picBarraMorada.Name = "picBarraMorada"
        Me.picBarraMorada.Size = New System.Drawing.Size(240, 40)
        Me.picBarraMorada.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'lblUsuarioEnSesion
        '
        Me.lblUsuarioEnSesion.BackColor = System.Drawing.Color.DarkGray
        Me.lblUsuarioEnSesion.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblUsuarioEnSesion.Location = New System.Drawing.Point(0, 39)
        Me.lblUsuarioEnSesion.Name = "lblUsuarioEnSesion"
        Me.lblUsuarioEnSesion.Size = New System.Drawing.Size(240, 16)
        Me.lblUsuarioEnSesion.Text = "Usuario:"
        '
        'cmbSalir
        '
        Me.cmbSalir.Image = CType(resources.GetObject("cmbSalir.Image"), System.Drawing.Image)
        Me.cmbSalir.Location = New System.Drawing.Point(0, 0)
        Me.cmbSalir.Name = "cmbSalir"
        Me.cmbSalir.Size = New System.Drawing.Size(41, 38)
        Me.cmbSalir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'PictureFondoINVENTARIO_INV_ACTIVO
        '
        Me.PictureFondoINVENTARIO_INV_ACTIVO.Image = CType(resources.GetObject("PictureFondoINVENTARIO_INV_ACTIVO.Image"), System.Drawing.Image)
        Me.PictureFondoINVENTARIO_INV_ACTIVO.Location = New System.Drawing.Point(42, 0)
        Me.PictureFondoINVENTARIO_INV_ACTIVO.Name = "PictureFondoINVENTARIO_INV_ACTIVO"
        Me.PictureFondoINVENTARIO_INV_ACTIVO.Size = New System.Drawing.Size(198, 38)
        Me.PictureFondoINVENTARIO_INV_ACTIVO.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'TimerReload
        '
        Me.TimerReload.Enabled = True
        Me.TimerReload.Interval = 500
        '
        'TimerBuscarProducto
        '
        Me.TimerBuscarProducto.Interval = 2250
        '
        'FormPickingUserMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(240, 300)
        Me.ControlBox = False
        Me.Controls.Add(Me.PanelInventario_Inv_Activo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormPickingUserMain"
        Me.Text = "isMOBILE - Inventarios Activos"
        Me.PanelInventario_Inv_Activo.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelInventario_Inv_Activo As System.Windows.Forms.Panel
    Friend WithEvents DataGridProductos As System.Windows.Forms.DataGrid
    Friend WithEvents PictureFondoINVENTARIO_INV_ACTIVO As System.Windows.Forms.PictureBox
    Friend WithEvents lblUsuarioEnSesion As System.Windows.Forms.Label
    Friend WithEvents cmbSalir As System.Windows.Forms.PictureBox
    Friend WithEvents picBarraMorada As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents txtCodigoProducto As System.Windows.Forms.TextBox
    Friend WithEvents lblBuscarCodigo As System.Windows.Forms.Label
    Friend WithEvents TimerReload As System.Windows.Forms.Timer
    Friend WithEvents lblGridTooltip As System.Windows.Forms.Label
    Friend WithEvents cmbActualizar As System.Windows.Forms.PictureBox
    Friend WithEvents lblBGActualizar As System.Windows.Forms.Label
    Friend WithEvents TimerBuscarProducto As System.Windows.Forms.Timer
End Class
