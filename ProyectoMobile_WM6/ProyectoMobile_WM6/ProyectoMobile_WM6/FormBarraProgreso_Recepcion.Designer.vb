﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class FormBarraProgreso_Recepcion
    Inherits System.Windows.Forms.Form

    'Form invalida a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer
    Private mainMenu1 As System.Windows.Forms.MainMenu

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar con el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormBarraProgreso_Recepcion))
        Me.mainMenu1 = New System.Windows.Forms.MainMenu
        Me.ProgressBar_FormBarraProgreso = New System.Windows.Forms.ProgressBar
        Me.PictureFondo_FormBarraProgreso = New System.Windows.Forms.PictureBox
        Me.cmbSalir = New System.Windows.Forms.PictureBox
        Me.lblBarraProgreso = New System.Windows.Forms.Label
        Me.PictureFondoGrisClaro = New System.Windows.Forms.PictureBox
        Me.SuspendLayout()
        '
        'ProgressBar_FormBarraProgreso
        '
        Me.ProgressBar_FormBarraProgreso.Location = New System.Drawing.Point(5, 85)
        Me.ProgressBar_FormBarraProgreso.Name = "ProgressBar_FormBarraProgreso"
        Me.ProgressBar_FormBarraProgreso.Size = New System.Drawing.Size(228, 20)
        '
        'PictureFondo_FormBarraProgreso
        '
        Me.PictureFondo_FormBarraProgreso.BackColor = System.Drawing.Color.Gainsboro
        Me.PictureFondo_FormBarraProgreso.Image = CType(resources.GetObject("PictureFondo_FormBarraProgreso.Image"), System.Drawing.Image)
        Me.PictureFondo_FormBarraProgreso.Location = New System.Drawing.Point(42, 0)
        Me.PictureFondo_FormBarraProgreso.Name = "PictureFondo_FormBarraProgreso"
        Me.PictureFondo_FormBarraProgreso.Size = New System.Drawing.Size(198, 38)
        Me.PictureFondo_FormBarraProgreso.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'cmbSalir
        '
        Me.cmbSalir.Enabled = False
        Me.cmbSalir.Image = CType(resources.GetObject("cmbSalir.Image"), System.Drawing.Image)
        Me.cmbSalir.Location = New System.Drawing.Point(0, 0)
        Me.cmbSalir.Name = "cmbSalir"
        Me.cmbSalir.Size = New System.Drawing.Size(41, 38)
        Me.cmbSalir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'lblBarraProgreso
        '
        Me.lblBarraProgreso.BackColor = System.Drawing.Color.Gainsboro
        Me.lblBarraProgreso.Location = New System.Drawing.Point(6, 45)
        Me.lblBarraProgreso.Name = "lblBarraProgreso"
        Me.lblBarraProgreso.Size = New System.Drawing.Size(228, 32)
        '
        'PictureFondoGrisClaro
        '
        Me.PictureFondoGrisClaro.BackColor = System.Drawing.Color.Gainsboro
        Me.PictureFondoGrisClaro.Location = New System.Drawing.Point(0, 39)
        Me.PictureFondoGrisClaro.Name = "PictureFondoGrisClaro"
        Me.PictureFondoGrisClaro.Size = New System.Drawing.Size(240, 79)
        '
        'FormBarraProgreso_Recepcion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.Gainsboro
        Me.ClientSize = New System.Drawing.Size(240, 300)
        Me.ControlBox = False
        Me.Controls.Add(Me.ProgressBar_FormBarraProgreso)
        Me.Controls.Add(Me.lblBarraProgreso)
        Me.Controls.Add(Me.PictureFondoGrisClaro)
        Me.Controls.Add(Me.cmbSalir)
        Me.Controls.Add(Me.PictureFondo_FormBarraProgreso)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormBarraProgreso_Recepcion"
        Me.Text = "Iniciando..."
        Me.TopMost = True
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ProgressBar_FormBarraProgreso As System.Windows.Forms.ProgressBar
    Friend WithEvents PictureFondo_FormBarraProgreso As System.Windows.Forms.PictureBox
    Friend WithEvents cmbSalir As System.Windows.Forms.PictureBox
    Friend WithEvents lblBarraProgreso As System.Windows.Forms.Label
    Friend WithEvents PictureFondoGrisClaro As System.Windows.Forms.PictureBox
End Class
