﻿Imports System
Imports System.Data
'Para poder hacer uso de la función que verifica si un archivo existe o no en el sistema de archivos de Windows Mobile
Imports System.IO
'Para obtener la ruta en la cual se encuentra el ejecutable de la aplciación y verificar que el archivo .sdf se encuentre en esa misma ruta.
Imports System.Configuration
Imports System.Reflection

Public Class formUsuario

    Public nombreArchivoExe As String
    Public rutaArchivoExe As String

    Private Sub TimerInicio_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TimerInicio.Tick

        Static btecontador As Byte

        If btecontador = 1 Then
            TimerInicio.Enabled = False
        Else

            ' txtUsuario.Enabled = False
            ' txtContraseña.Enabled = False
            btecontador = btecontador + 1

            Me.Enabled = False

            ConfiguracionInicial()

            If ApplicationExit Then Exit Sub

            If Not VerificarPDA() Then
                ApplicationExit = True : Me.Close() : Exit Sub 'Global.System.Windows.Forms.Application.Exit()
            Else

                FormBarraProgreso_Recepcion.Show()
                FormBarraProgreso_Recepcion.Refresh()
                FormBarraProgreso_Recepcion.Update()

                ActualizarRecursos()

                If ApplicationExit Then Exit Sub

                Me.Enabled = True

                AjustarFormularios(Me)

                ' txtUsuario.Enabled = True
                ' txtContraseña.Enabled = True

                txtUsuario.Focus()

            End If

        End If
    End Sub

    Private Sub formUsuario_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Enabled = False
        Me.versionLabel.Text = "V" + System.Reflection.Assembly.GetExecutingAssembly.GetName().Version.ToString()

    End Sub

    Sub ConfiguracionInicial()

        'Obtengo por Reflexion la ruta completa del archivo .exe
        nombreArchivoExe = System.Reflection.Assembly.GetExecutingAssembly.GetName.CodeBase
        rutaArchivoExe = String.Empty
        'Obtengo la ruta del archivo .exe             
        rutaArchivoExe = nombreArchivoExe.Substring(0, nombreArchivoExe.LastIndexOf("\") + 1)

        If File.Exists(rutaArchivoExe + "DebugMode.Active") Or Directory.Exists(rutaArchivoExe + "DebugMode.Active") Then DebugMode = True

        If Not Leer_Setup(rutaArchivoExe) Then ApplicationExit = True : Me.Close() 'Global.System.Windows.Forms.Application.Exit()

    End Sub

    Sub ActualizarRecursos()

        If (Not File.Exists(rutaArchivoExe + "isMOBILE.sdf")) Then

            'Si no existe la BD se evalua que es la primera vez que la aplicación se ejecuta y hay que inicializar

            'Me.SendToBack()
            'Me.Enabled = False
            'FormBarraProgreso_Recepcion.Show()
            'FormBarraProgreso_Recepcion.Refresh()
            'FormBarraProgreso_Recepcion.Update()
            inicializarPDA(rutaArchivoExe)
            FormBarraProgreso_Recepcion.Close()
            Me.Enabled = True
            Me.Visible = True

        Else

            'Si existe la BD automaticamente se actualizan ciertas tablas
            'Me.SendToBack()
            'Me.Enabled = False
            'FormBarraProgreso_Recepcion.Show()
            'FormBarraProgreso_Recepcion.Refresh()
            'FormBarraProgreso_Recepcion.Update()
            actualizacionAutomatica(rutaArchivoExe)
            FormBarraProgreso_Recepcion.Close()
            Me.Enabled = True
            Me.Visible = True

            If Not Inicio_Conectado Then
                ApplicationExit = True
                Me.Close()
            End If

        End If

    End Sub

    Private Sub txtUsuario_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtUsuario.KeyDown

        If e.KeyValue = 13 Then
            txtContraseña.Focus()
        End If

    End Sub

    Private Sub txtContraseña_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtContraseña.KeyDown
        If e.KeyValue = 13 Then
            cmbEntrar.Focus()
            Login()
        End If
    End Sub

    Private Sub cmbEntrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbEntrar.Click
        Login()
    End Sub

    Private Sub cmbBorrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbBorrar.Click
        txtUsuario.Text = ""
        txtContraseña.Text = ""
        txtUsuario.Focus()
    End Sub

    Private Sub cmbSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSalir.Click
        Salir()
    End Sub

    Private Sub Login()

        'Evaluo que el formulario no este vacio

        If (txtUsuario.Text = "") Then
            MsgBox("Debe ingresar el usuario.", MsgBoxStyle.Information, "isMOBILE")
            txtUsuario.Focus()
        Else

            If (txtContraseña.Text = "") Then
                MsgBox("Debe ingresar la contraseña.", MsgBoxStyle.Information, "isMOBILE")
                txtContraseña.Focus()
            Else

                ProgressBar_FormUsuario.Visible = True
                ProgressBar_FormUsuario.Value = 10

                If Not CompatibilidadFueraDeLinea Then
                    If conectarDB_SERVIDOR(rutaDB_SERVIDOR) = 0 Then
                        MessageBox.Show(MsjSinConexionGenerico)
                        Exit Sub
                    End If
                End If

                datasetUsuario = validarUsuarioContraseña(txtUsuario.Text, txtContraseña.Text)
                ProgressBar_FormUsuario.Value = 35

                If datasetUsuario.Tables(0).Rows.Count <> 0 Then

                    ProgressBar_FormUsuario.Value = 50

                    descripcionUsuario = CStr(datasetUsuario.Tables(0).Rows(0)("descripcion"))

                    ProgressBar_FormUsuario.Value = 75

                    'Try
                    'System.Diagnostics.Process.Start(rutaArchivoExe + "isMOBILE Bluetooth Serial Port Creator.exe", "")
                    'Catch ex As Exception

                    'End Try

                    Dim TmpUserMenuConfig As DataSet = RecuperarCONF_MENU_USER(txtUsuario.Text)

                    ProgressBar_FormUsuario.Value = 100
                    ProgressBar_FormUsuario.Visible = False

                    UserMenuConfig = TmpUserMenuConfig

                    If DebugMode Then
                        If Not UserMenuConfig Is Nothing Then
                            MsgBox("Cant. Opciones: " & UserMenuConfig.Tables(0).Rows.Count.ToString)
                        End If
                    End If

                    If Not CompatibilidadFueraDeLinea Then
                        cerrarConexion_SERVIDOR()
                    End If

                    formMenuPrincipal.Show()

                    Me.Visible = False

                Else

                    ProgressBar_FormUsuario.Value = 100

                    ProgressBar_FormUsuario.Visible = False

                    MsgBox("Usuario o contraseña incorrectos.", MsgBoxStyle.Information, "isMOBILE")
                    txtUsuario.Text = ""
                    txtContraseña.Text = ""
                    txtUsuario.Focus()

                End If

                If Not CompatibilidadFueraDeLinea Then
                    cerrarConexion_SERVIDOR()
                End If

            End If

        End If

    End Sub

    Private Sub cmbEntrar_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cmbEntrar.KeyDown
        If e.KeyValue = 13 Then
            Login()
        End If
    End Sub

    'Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

    '    Dim ipAddress As String = moduloFunciones.ip_impresora
    '    Dim port As Integer = moduloFunciones.puerto_impresora
    '    Dim habladortxt As String = moduloFunciones.hablador_setup

    '    Dim client As New System.Net.Sockets.TcpClient
    '    ' Verifica que haya conexion con la impresora
    '    Dim conexion As Boolean
    '    Try
    '        client.Connect(ipAddress, port)
    '        conexion = True
    '    Catch
    '        conexion = False
    '        MsgBox("No hay conexión con la impresora.", MsgBoxStyle.Information, "Setup")
    '        Exit Sub
    '    End Try

    '    'Crea la variable que permitira imprimir



    '    Dim writer As New System.IO.StreamWriter(client.GetStream())
    '    'Reemplaza las variables de los .txt
    '    'leerlineas = Replace(leerlineas, "|D1998|", CDate(Now))
    '    ''leerlineas = Replace(leerlineas, "|D1002|", CStr(codi))
    '    'leerlineas = Replace(leerlineas, "|D1003|", CStr(descri))
    '    ' leerlineas = Replace(leerlineas, "|D1006|", CDbl(totalprecio1))

    '    'Imprime todas las lineas leidas
    '    'For n = 1 To num_impresiones
    '    Dim STR As String = "N" & vbNewLine & "A130,10,0,3,1,2,R,""KENZIA, C.A""" & vbNewLine & "P1"

    '    writer.WriteLine(STR)
    '    'Next
    '    'Comienza Impresion
    '    writer.Flush()

    '    writer.Close()

    '    'Close Connection

    '    client.Close()


    'End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        'ActualizarTR_ODC()
        'comandoBD_PDA.CommandText = "DELETE FROM Tr_ODC WHERE C_DOCUMENTO = '000000001'"
        'ejecutaQuery_PDA(comandoBD_PDA)
        'ActualizacionParcial("ODC")
        'ActualizacionParcial("ODC", "000000001")
        'ActualizacionParcial("Productos")

    End Sub

End Class
