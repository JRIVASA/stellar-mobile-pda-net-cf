﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class FormActualizar_InventarioFisico
    Inherits System.Windows.Forms.Form

    'Form invalida a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer
    Private mainMenu1 As System.Windows.Forms.MainMenu

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar con el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormActualizar_InventarioFisico))
        Me.mainMenu1 = New System.Windows.Forms.MainMenu
        Me.lblActualización = New System.Windows.Forms.Label
        Me.cmbActualizar = New System.Windows.Forms.PictureBox
        Me.cmbSelectTodos = New System.Windows.Forms.PictureBox
        Me.PictureFondoACTUALIZAR = New System.Windows.Forms.PictureBox
        Me.CheckInventarios = New System.Windows.Forms.CheckBox
        Me.CheckDepositos = New System.Windows.Forms.CheckBox
        Me.PanelActualizar = New System.Windows.Forms.Panel
        Me.ProgressBar_FormActualizarInventarioFisico = New System.Windows.Forms.ProgressBar
        Me.lblUsuarioEnSesion = New System.Windows.Forms.Label
        Me.PanelOpcionesActualizar = New System.Windows.Forms.Panel
        Me.CheckODC = New System.Windows.Forms.CheckBox
        Me.CheckProductos = New System.Windows.Forms.CheckBox
        Me.CheckUsuarios = New System.Windows.Forms.CheckBox
        Me.CheckConfiguración = New System.Windows.Forms.CheckBox
        Me.picBarraMorada = New System.Windows.Forms.PictureBox
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.cmbSalir = New System.Windows.Forms.PictureBox
        Me.PanelActualizar.SuspendLayout()
        Me.PanelOpcionesActualizar.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblActualización
        '
        Me.lblActualización.BackColor = System.Drawing.Color.Gainsboro
        Me.lblActualización.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold)
        Me.lblActualización.Location = New System.Drawing.Point(8, 77)
        Me.lblActualización.Name = "lblActualización"
        Me.lblActualización.Size = New System.Drawing.Size(80, 20)
        Me.lblActualización.Text = "Actualización"
        '
        'cmbActualizar
        '
        Me.cmbActualizar.BackColor = System.Drawing.Color.White
        Me.cmbActualizar.Image = CType(resources.GetObject("cmbActualizar.Image"), System.Drawing.Image)
        Me.cmbActualizar.Location = New System.Drawing.Point(17, 176)
        Me.cmbActualizar.Name = "cmbActualizar"
        Me.cmbActualizar.Size = New System.Drawing.Size(55, 55)
        Me.cmbActualizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'cmbSelectTodos
        '
        Me.cmbSelectTodos.Image = CType(resources.GetObject("cmbSelectTodos.Image"), System.Drawing.Image)
        Me.cmbSelectTodos.Location = New System.Drawing.Point(17, 109)
        Me.cmbSelectTodos.Name = "cmbSelectTodos"
        Me.cmbSelectTodos.Size = New System.Drawing.Size(55, 55)
        Me.cmbSelectTodos.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'PictureFondoACTUALIZAR
        '
        Me.PictureFondoACTUALIZAR.Image = CType(resources.GetObject("PictureFondoACTUALIZAR.Image"), System.Drawing.Image)
        Me.PictureFondoACTUALIZAR.Location = New System.Drawing.Point(42, 0)
        Me.PictureFondoACTUALIZAR.Name = "PictureFondoACTUALIZAR"
        Me.PictureFondoACTUALIZAR.Size = New System.Drawing.Size(198, 38)
        Me.PictureFondoACTUALIZAR.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'CheckInventarios
        '
        Me.CheckInventarios.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.CheckInventarios.Location = New System.Drawing.Point(3, 108)
        Me.CheckInventarios.Name = "CheckInventarios"
        Me.CheckInventarios.Size = New System.Drawing.Size(126, 18)
        Me.CheckInventarios.TabIndex = 4
        Me.CheckInventarios.Text = "Inventarios"
        '
        'CheckDepositos
        '
        Me.CheckDepositos.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.CheckDepositos.Location = New System.Drawing.Point(3, 82)
        Me.CheckDepositos.Name = "CheckDepositos"
        Me.CheckDepositos.Size = New System.Drawing.Size(126, 18)
        Me.CheckDepositos.TabIndex = 3
        Me.CheckDepositos.Text = "Depositos"
        '
        'PanelActualizar
        '
        Me.PanelActualizar.BackColor = System.Drawing.Color.White
        Me.PanelActualizar.Controls.Add(Me.ProgressBar_FormActualizarInventarioFisico)
        Me.PanelActualizar.Controls.Add(Me.lblUsuarioEnSesion)
        Me.PanelActualizar.Controls.Add(Me.PanelOpcionesActualizar)
        Me.PanelActualizar.Controls.Add(Me.picBarraMorada)
        Me.PanelActualizar.Controls.Add(Me.cmbActualizar)
        Me.PanelActualizar.Controls.Add(Me.cmbSelectTodos)
        Me.PanelActualizar.Controls.Add(Me.lblActualización)
        Me.PanelActualizar.Controls.Add(Me.PictureBox1)
        Me.PanelActualizar.Controls.Add(Me.cmbSalir)
        Me.PanelActualizar.Controls.Add(Me.PictureFondoACTUALIZAR)
        Me.PanelActualizar.Location = New System.Drawing.Point(0, 0)
        Me.PanelActualizar.Name = "PanelActualizar"
        Me.PanelActualizar.Size = New System.Drawing.Size(240, 300)
        '
        'ProgressBar_FormActualizarInventarioFisico
        '
        Me.ProgressBar_FormActualizarInventarioFisico.Location = New System.Drawing.Point(21, 270)
        Me.ProgressBar_FormActualizarInventarioFisico.Name = "ProgressBar_FormActualizarInventarioFisico"
        Me.ProgressBar_FormActualizarInventarioFisico.Size = New System.Drawing.Size(199, 16)
        '
        'lblUsuarioEnSesion
        '
        Me.lblUsuarioEnSesion.BackColor = System.Drawing.Color.DarkGray
        Me.lblUsuarioEnSesion.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblUsuarioEnSesion.Location = New System.Drawing.Point(0, 39)
        Me.lblUsuarioEnSesion.Name = "lblUsuarioEnSesion"
        Me.lblUsuarioEnSesion.Size = New System.Drawing.Size(240, 15)
        Me.lblUsuarioEnSesion.Text = "Usuario:"
        '
        'PanelOpcionesActualizar
        '
        Me.PanelOpcionesActualizar.AutoScroll = True
        Me.PanelOpcionesActualizar.AutoScrollMargin = New System.Drawing.Size(1, 1)
        Me.PanelOpcionesActualizar.BackColor = System.Drawing.Color.RoyalBlue
        Me.PanelOpcionesActualizar.Controls.Add(Me.CheckODC)
        Me.PanelOpcionesActualizar.Controls.Add(Me.CheckInventarios)
        Me.PanelOpcionesActualizar.Controls.Add(Me.CheckProductos)
        Me.PanelOpcionesActualizar.Controls.Add(Me.CheckDepositos)
        Me.PanelOpcionesActualizar.Controls.Add(Me.CheckUsuarios)
        Me.PanelOpcionesActualizar.Controls.Add(Me.CheckConfiguración)
        Me.PanelOpcionesActualizar.Location = New System.Drawing.Point(90, 68)
        Me.PanelOpcionesActualizar.Name = "PanelOpcionesActualizar"
        Me.PanelOpcionesActualizar.Size = New System.Drawing.Size(137, 175)
        '
        'CheckODC
        '
        Me.CheckODC.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.CheckODC.Location = New System.Drawing.Point(3, 133)
        Me.CheckODC.Name = "CheckODC"
        Me.CheckODC.Size = New System.Drawing.Size(126, 18)
        Me.CheckODC.TabIndex = 5
        Me.CheckODC.Text = "ODC"
        '
        'CheckProductos
        '
        Me.CheckProductos.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.CheckProductos.Location = New System.Drawing.Point(3, 6)
        Me.CheckProductos.Name = "CheckProductos"
        Me.CheckProductos.Size = New System.Drawing.Size(126, 18)
        Me.CheckProductos.TabIndex = 0
        Me.CheckProductos.Text = "Productos"
        '
        'CheckUsuarios
        '
        Me.CheckUsuarios.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.CheckUsuarios.Location = New System.Drawing.Point(3, 56)
        Me.CheckUsuarios.Name = "CheckUsuarios"
        Me.CheckUsuarios.Size = New System.Drawing.Size(126, 18)
        Me.CheckUsuarios.TabIndex = 2
        Me.CheckUsuarios.Text = "Usuarios"
        '
        'CheckConfiguración
        '
        Me.CheckConfiguración.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.CheckConfiguración.Location = New System.Drawing.Point(3, 30)
        Me.CheckConfiguración.Name = "CheckConfiguración"
        Me.CheckConfiguración.Size = New System.Drawing.Size(126, 18)
        Me.CheckConfiguración.TabIndex = 1
        Me.CheckConfiguración.Text = "Configuración"
        '
        'picBarraMorada
        '
        Me.picBarraMorada.Image = CType(resources.GetObject("picBarraMorada.Image"), System.Drawing.Image)
        Me.picBarraMorada.Location = New System.Drawing.Point(0, 260)
        Me.picBarraMorada.Name = "picBarraMorada"
        Me.picBarraMorada.Size = New System.Drawing.Size(240, 40)
        Me.picBarraMorada.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Gainsboro
        Me.PictureBox1.Location = New System.Drawing.Point(0, 54)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(240, 205)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'cmbSalir
        '
        Me.cmbSalir.Image = CType(resources.GetObject("cmbSalir.Image"), System.Drawing.Image)
        Me.cmbSalir.Location = New System.Drawing.Point(0, 0)
        Me.cmbSalir.Name = "cmbSalir"
        Me.cmbSalir.Size = New System.Drawing.Size(41, 38)
        Me.cmbSalir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'FormActualizar_InventarioFisico
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(240, 300)
        Me.ControlBox = False
        Me.Controls.Add(Me.PanelActualizar)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormActualizar_InventarioFisico"
        Me.Text = "isMOBILE - Actualizar"
        Me.PanelActualizar.ResumeLayout(False)
        Me.PanelOpcionesActualizar.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblActualización As System.Windows.Forms.Label
    Friend WithEvents cmbActualizar As System.Windows.Forms.PictureBox
    Friend WithEvents cmbSelectTodos As System.Windows.Forms.PictureBox
    Friend WithEvents PictureFondoACTUALIZAR As System.Windows.Forms.PictureBox
    Friend WithEvents CheckInventarios As System.Windows.Forms.CheckBox
    Friend WithEvents CheckDepositos As System.Windows.Forms.CheckBox
    Friend WithEvents PanelActualizar As System.Windows.Forms.Panel
    Friend WithEvents PanelOpcionesActualizar As System.Windows.Forms.Panel
    Friend WithEvents CheckProductos As System.Windows.Forms.CheckBox
    Friend WithEvents CheckUsuarios As System.Windows.Forms.CheckBox
    Friend WithEvents CheckConfiguración As System.Windows.Forms.CheckBox
    Friend WithEvents cmbSalir As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents picBarraMorada As System.Windows.Forms.PictureBox
    Friend WithEvents lblUsuarioEnSesion As System.Windows.Forms.Label
    Friend WithEvents ProgressBar_FormActualizarInventarioFisico As System.Windows.Forms.ProgressBar
    Friend WithEvents CheckODC As System.Windows.Forms.CheckBox

    'Friend WithEvents ProgressBar_FormActualizar_InventarioFisico As System.Windows.Forms.ProgressBar
End Class
