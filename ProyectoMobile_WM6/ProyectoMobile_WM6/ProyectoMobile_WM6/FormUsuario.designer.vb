﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class formUsuario
    Inherits System.Windows.Forms.Form

    'Form invalida a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer
    Private mainMenu1 As System.Windows.Forms.MainMenu

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar con el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(formUsuario))
        Me.mainMenu1 = New System.Windows.Forms.MainMenu
        Me.txtUsuario = New System.Windows.Forms.TextBox
        Me.txtContraseña = New System.Windows.Forms.TextBox
        Me.pictureFondoUsuario = New System.Windows.Forms.PictureBox
        Me.TimerInicio = New System.Windows.Forms.Timer
        Me.ProgressBar_FormUsuario = New System.Windows.Forms.ProgressBar
        Me.lblUsuario = New System.Windows.Forms.Label
        Me.lblContraseña = New System.Windows.Forms.Label
        Me.cmbEntrar = New System.Windows.Forms.Button
        Me.cmbBorrar = New System.Windows.Forms.Button
        Me.cmbSalir = New System.Windows.Forms.Button
        Me.versionLabel = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'txtUsuario
        '
        Me.txtUsuario.Font = New System.Drawing.Font("Verdana", 14.0!, System.Drawing.FontStyle.Bold)
        Me.txtUsuario.Location = New System.Drawing.Point(31, 123)
        Me.txtUsuario.MaxLength = 50
        Me.txtUsuario.Name = "txtUsuario"
        Me.txtUsuario.Size = New System.Drawing.Size(177, 29)
        Me.txtUsuario.TabIndex = 1
        '
        'txtContraseña
        '
        Me.txtContraseña.Font = New System.Drawing.Font("Verdana", 14.0!, System.Drawing.FontStyle.Bold)
        Me.txtContraseña.Location = New System.Drawing.Point(31, 178)
        Me.txtContraseña.MaxLength = 50
        Me.txtContraseña.Name = "txtContraseña"
        Me.txtContraseña.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtContraseña.Size = New System.Drawing.Size(177, 29)
        Me.txtContraseña.TabIndex = 2
        '
        'pictureFondoUsuario
        '
        Me.pictureFondoUsuario.Image = CType(resources.GetObject("pictureFondoUsuario.Image"), System.Drawing.Image)
        Me.pictureFondoUsuario.Location = New System.Drawing.Point(0, 0)
        Me.pictureFondoUsuario.Name = "pictureFondoUsuario"
        Me.pictureFondoUsuario.Size = New System.Drawing.Size(240, 300)
        Me.pictureFondoUsuario.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'TimerInicio
        '
        Me.TimerInicio.Enabled = True
        Me.TimerInicio.Interval = 10
        '
        'ProgressBar_FormUsuario
        '
        Me.ProgressBar_FormUsuario.Location = New System.Drawing.Point(35, 274)
        Me.ProgressBar_FormUsuario.Name = "ProgressBar_FormUsuario"
        Me.ProgressBar_FormUsuario.Size = New System.Drawing.Size(164, 15)
        Me.ProgressBar_FormUsuario.Visible = False
        '
        'lblUsuario
        '
        Me.lblUsuario.BackColor = System.Drawing.Color.LightGray
        Me.lblUsuario.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblUsuario.Location = New System.Drawing.Point(86, 100)
        Me.lblUsuario.Name = "lblUsuario"
        Me.lblUsuario.Size = New System.Drawing.Size(71, 20)
        Me.lblUsuario.Text = "Usuario"
        Me.lblUsuario.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblContraseña
        '
        Me.lblContraseña.BackColor = System.Drawing.Color.Silver
        Me.lblContraseña.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblContraseña.Location = New System.Drawing.Point(76, 157)
        Me.lblContraseña.Name = "lblContraseña"
        Me.lblContraseña.Size = New System.Drawing.Size(94, 20)
        Me.lblContraseña.Text = "Contraseña"
        Me.lblContraseña.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'cmbEntrar
        '
        Me.cmbEntrar.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.cmbEntrar.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.cmbEntrar.ForeColor = System.Drawing.Color.Black
        Me.cmbEntrar.Location = New System.Drawing.Point(12, 235)
        Me.cmbEntrar.Name = "cmbEntrar"
        Me.cmbEntrar.Size = New System.Drawing.Size(65, 30)
        Me.cmbEntrar.TabIndex = 3
        Me.cmbEntrar.Text = "Entrar"
        '
        'cmbBorrar
        '
        Me.cmbBorrar.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.cmbBorrar.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.cmbBorrar.ForeColor = System.Drawing.Color.Black
        Me.cmbBorrar.Location = New System.Drawing.Point(88, 235)
        Me.cmbBorrar.Name = "cmbBorrar"
        Me.cmbBorrar.Size = New System.Drawing.Size(65, 30)
        Me.cmbBorrar.TabIndex = 4
        Me.cmbBorrar.Text = "Borrar"
        '
        'cmbSalir
        '
        Me.cmbSalir.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.cmbSalir.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.cmbSalir.ForeColor = System.Drawing.Color.Black
        Me.cmbSalir.Location = New System.Drawing.Point(164, 235)
        Me.cmbSalir.Name = "cmbSalir"
        Me.cmbSalir.Size = New System.Drawing.Size(65, 30)
        Me.cmbSalir.TabIndex = 5
        Me.cmbSalir.Text = "Salir"
        '
        'versionLabel
        '
        Me.versionLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(230, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(230, Byte), Integer))
        Me.versionLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular)
        Me.versionLabel.Location = New System.Drawing.Point(107, 0)
        Me.versionLabel.Name = "versionLabel"
        Me.versionLabel.Size = New System.Drawing.Size(130, 21)
        Me.versionLabel.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'formUsuario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(240, 300)
        Me.ControlBox = False
        Me.Controls.Add(Me.versionLabel)
        Me.Controls.Add(Me.cmbSalir)
        Me.Controls.Add(Me.cmbBorrar)
        Me.Controls.Add(Me.cmbEntrar)
        Me.Controls.Add(Me.lblContraseña)
        Me.Controls.Add(Me.lblUsuario)
        Me.Controls.Add(Me.ProgressBar_FormUsuario)
        Me.Controls.Add(Me.txtContraseña)
        Me.Controls.Add(Me.txtUsuario)
        Me.Controls.Add(Me.pictureFondoUsuario)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "formUsuario"
        Me.Text = "isMOBILE - Login"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtUsuario As System.Windows.Forms.TextBox
    Friend WithEvents txtContraseña As System.Windows.Forms.TextBox
    Friend WithEvents pictureFondoUsuario As System.Windows.Forms.PictureBox
    Friend WithEvents TimerInicio As System.Windows.Forms.Timer
    Friend WithEvents ProgressBar_FormUsuario As System.Windows.Forms.ProgressBar
    Friend WithEvents lblUsuario As System.Windows.Forms.Label
    Friend WithEvents lblContraseña As System.Windows.Forms.Label
    Friend WithEvents cmbEntrar As System.Windows.Forms.Button
    Friend WithEvents cmbBorrar As System.Windows.Forms.Button
    Friend WithEvents cmbSalir As System.Windows.Forms.Button
    Friend WithEvents versionLabel As System.Windows.Forms.Label
End Class
