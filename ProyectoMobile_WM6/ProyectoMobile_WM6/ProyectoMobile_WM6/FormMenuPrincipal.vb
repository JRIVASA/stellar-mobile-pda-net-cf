﻿Imports System
Imports System.Data
Imports System.IO

Public Class formMenuPrincipal

    Private FormaCargada As Boolean

    Private Sub formMenuPrincipal_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Activated

        If Not FormaCargada Then

            FormaCargada = True

            TimerReload.Interval = 100
            TimerReload.Enabled = True

        End If

    End Sub

    Public Sub HideOptions(ByRef MenuForm As Form)

        Dim Cont As Integer = 0

        ' > 57.5 = Controls Over lblStatus.... Which represent Menu Options.

        For Each TmpButton As Control In MenuForm.Controls()
            If (TypeOf TmpButton Is PictureBox) Then
                'If DebugMode Then MsgBox(TmpButton.Name & ": ¿" & TmpButton.Top.ToString & " > " & (((57.5 * RuntimeVSDesignRatioY) * CurrentHeightFactor)).ToString & "?")
                If (TmpButton.Top > ((57.5 * RuntimeVSDesignRatioY) * CurrentHeightFactor)) Then ' Menu Options
                    TmpButton.Visible = False
                End If
            End If
        Next

        For Each TmpLabel As Control In MenuForm.Controls()
            If (TypeOf TmpLabel Is Label) Then
                If (TmpLabel.Top > ((57.5 * RuntimeVSDesignRatioY) * CurrentHeightFactor)) Then ' Menu Options
                    TmpLabel.Visible = False
                End If
            End If
        Next

    End Sub

    Public Function NextVisibleMenu(ByRef MenuForm As Form) As Integer

        Dim Cont As Integer = 0

        For Each TmpButton As Control In MenuForm.Controls()
            If (TypeOf TmpButton Is PictureBox) Then
                If Not ( _
                String.Equals(TmpButton.Name, "CmbSalir", StringComparison.OrdinalIgnoreCase) _
                Or String.Equals(TmpButton.Name, "PicTopBar", StringComparison.OrdinalIgnoreCase) _
                ) Then
                    If TmpButton.Visible Then
                        Cont += 1
                    End If
                End If
            End If
        Next

        Cont += 1 ' Next Free Position

        Return Cont

    End Function

    Public Sub ShowMenuOption(ByRef MenuForm As Form, ByVal pButton As PictureBox, _
    ByVal pLabel As Label, ByVal Position As Integer)

        ' Valores medidos para visualización y colocación optima
        ' en tiempo de diseño de los formularios de la aplicación.
        ' No tocar los valores constantes. Se multiplican por
        ' los valores proporcionales para colocar los valores finales.

        'Console.WriteLine(pButton.Name)
        'Console.WriteLine(pLabel.Name)

        pLabel.Width = ((72 * RuntimeVSDesignRatioX) * CurrentWidthFactor)
        pLabel.Height = ((26 * RuntimeVSDesignRatioY) * CurrentHeightFactor)

        pButton.Width = ((48 * RuntimeVSDesignRatioX) * CurrentWidthFactor)
        pButton.Height = ((47 * RuntimeVSDesignRatioY) * CurrentHeightFactor)

        Select Case Position

            Case 1

                pButton.Left = ((28 * RuntimeVSDesignRatioX) * CurrentWidthFactor)
                pButton.Top = ((63 * RuntimeVSDesignRatioY) * CurrentHeightFactor)

                pLabel.Left = ((16 * RuntimeVSDesignRatioX) * CurrentWidthFactor)
                pLabel.Top = ((114 * RuntimeVSDesignRatioY) * CurrentHeightFactor)

            Case 2

                pButton.Left = ((99 * RuntimeVSDesignRatioX) * CurrentWidthFactor)
                pButton.Top = ((63 * RuntimeVSDesignRatioY) * CurrentHeightFactor)

                pLabel.Left = ((87 * RuntimeVSDesignRatioX) * CurrentWidthFactor)
                pLabel.Top = ((114 * RuntimeVSDesignRatioY) * CurrentHeightFactor)

            Case 3

                pButton.Left = ((170 * RuntimeVSDesignRatioX) * CurrentWidthFactor)
                pButton.Top = ((63 * RuntimeVSDesignRatioY) * CurrentHeightFactor)

                pLabel.Left = ((158 * RuntimeVSDesignRatioX) * CurrentWidthFactor)
                pLabel.Top = ((114 * RuntimeVSDesignRatioY) * CurrentHeightFactor)

            Case 4

                pButton.Left = ((28 * RuntimeVSDesignRatioX) * CurrentWidthFactor)
                pButton.Top = ((138 * RuntimeVSDesignRatioY) * CurrentHeightFactor)

                pLabel.Left = ((16 * RuntimeVSDesignRatioX) * CurrentWidthFactor)
                pLabel.Top = ((188 * RuntimeVSDesignRatioY) * CurrentHeightFactor)

            Case 5

                pButton.Left = ((99 * RuntimeVSDesignRatioX) * CurrentWidthFactor)
                pButton.Top = ((138 * RuntimeVSDesignRatioY) * CurrentHeightFactor)

                pLabel.Left = ((87 * RuntimeVSDesignRatioX) * CurrentWidthFactor)
                pLabel.Top = ((188 * RuntimeVSDesignRatioY) * CurrentHeightFactor)

            Case 6

                pButton.Left = ((170 * RuntimeVSDesignRatioX) * CurrentWidthFactor)
                pButton.Top = ((138 * RuntimeVSDesignRatioY) * CurrentHeightFactor)

                pLabel.Left = ((158 * RuntimeVSDesignRatioX) * CurrentWidthFactor)
                pLabel.Top = ((188 * RuntimeVSDesignRatioY) * CurrentHeightFactor)

            Case 7

                pButton.Left = ((28 * RuntimeVSDesignRatioX) * CurrentWidthFactor)
                pButton.Top = ((213 * RuntimeVSDesignRatioY) * CurrentHeightFactor)

                pLabel.Left = ((16 * RuntimeVSDesignRatioX) * CurrentWidthFactor)
                pLabel.Top = ((263 * RuntimeVSDesignRatioY) * CurrentHeightFactor)

            Case 8

                pButton.Left = ((99 * RuntimeVSDesignRatioX) * CurrentWidthFactor)
                pButton.Top = ((213 * RuntimeVSDesignRatioY) * CurrentHeightFactor)

                pLabel.Left = ((87 * RuntimeVSDesignRatioX) * CurrentWidthFactor)
                pLabel.Top = ((263 * RuntimeVSDesignRatioY) * CurrentHeightFactor)

            Case 9

                pButton.Left = ((170 * RuntimeVSDesignRatioX) * CurrentWidthFactor)
                pButton.Top = ((213 * RuntimeVSDesignRatioY) * CurrentHeightFactor)

                pLabel.Left = ((158 * RuntimeVSDesignRatioX) * CurrentWidthFactor)
                pLabel.Top = ((263 * RuntimeVSDesignRatioY) * CurrentHeightFactor)

        End Select

        pButton.Visible = True
        pLabel.Visible = True

    End Sub

    Private Sub CmbSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmbSalir.Click
        'Salir()

        respuesta = MsgBox("¿Desea cerrar sesión?", MsgBoxStyle.YesNo, "isMOBILE")
        If respuesta = "6" Then
            FormaCargada = False
            limpiarFormUsuario()
            UserMenuConfig = Nothing
            formUsuario.Show()
            formUsuario.txtUsuario.Focus()
            Me.Visible = False
        End If

    End Sub

    Private Sub cmbInventario_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbInventario.Click
        FormMenuInventarioFisico.Show()
        Me.Visible = False
    End Sub

    Private Sub cmbRecepcion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        lblStatus_MenuPrincipal.Visible = True
        lblStatus_MenuPrincipal.Text = "Actualizando ODC."

        Me.Refresh()
        Me.Update()
        'Actualizo MA_ODC para que no permita recibir productos de una recepcion que ya haya sido completada 

        If conectarDB_SERVIDOR(rutaDB_SERVIDOR) = 1 Then
            'comandoBD_PDA.CommandText = "DELETE MA_ODC"
            'ejecutaQuery_PDA(comandoBD_PDA)
            'actualizarMA_ODC()
            ActualizacionParcial("ODC")
            cerrarConexion_SERVIDOR()
        Else
            MsgBox("No se puede actualizar la informacion de ordenes de compra," & vbNewLine & "no hay conexión con el servidor, comuniquese con el departamento de sistemas." _
            & vbNewLine & "No se recomienda realizar una nueva recepción sin antes" & vbNewLine & "haber establecido la conexión con el servidor.", MsgBoxStyle.Information, "isMOBILE")
        End If

        lblStatus_MenuPrincipal.Visible = False
        verFormCantidadFacturadaSinODC = True
        FormProveedorDeposito_Recepcion.Show()
        FormProveedorDeposito_Recepcion.txtProveedor.Enabled = True
        FormProveedorDeposito_Recepcion.txtProveedor.Focus()
        FormProveedorDeposito_Recepcion.txtDeposito.Enabled = False
        FormProveedorDeposito_Recepcion.cmbBuscarDeposito.Enabled = False
        FormProveedorDeposito_Recepcion.cmbVerificarDeposito.Enabled = False

        Me.Visible = False
    End Sub

    Private Sub cmbTraslado_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        FormDepositoOrigenDestino_Traslado.Show()
        Me.Visible = False
        FormDepositoOrigenDestino_Traslado.txtDepositoOrigen.Focus()
    End Sub

    Private Sub cmbEtiquetas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbEtiquetas.Click
        Form_Impresion_Etiquetas.Show()
        Me.Visible = False
    End Sub

    Private Sub cmbHabladores_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbHabladores.Click
        Form_Impresion_Hablador.Show()
        Me.Visible = False
    End Sub

    Private Sub cmbConsulta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbConsulta.Click
        FormConsultaProductos.Show()
        Me.Visible = False
        FormConsultaProductos.codigoBox.Focus()
    End Sub

    Public Function OpcionUsuarioActualHabilitada(ByVal Tag As String) As Boolean

        'Dim TmpDS As DataSet = UserMenuConfig.Copy

        If UserMenuConfig Is Nothing Then
            If DebugMode Then MsgBox("Error al cargar menú del usuario. No hay opciones")
            Return True
        End If

        If UserMenuConfig.Tables(0).Rows.Count <= 0 Then
            If DebugMode Then MsgBox("CantOpc: " & UserMenuConfig.Tables(0).Rows.Count.ToString)
            Return True
        End If

        UserMenuConfig.CaseSensitive = False

        Dim Rows() As DataRow = UserMenuConfig.Tables(0).Select("Forma = '" & Tag & "'")

        If Rows.Length > 0 Then
            With Rows(0)
                'If DebugMode Then MsgBox("Tag -> " & .Item("Forma") & " -> " & .Item("Activado").ToString)
                Return (.Item("Activado") >= 1)
            End With
        End If

    End Function

    Private Sub cmbValidacionFacturaPOS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbValidacionFacturaPOS.Click
        FormValidacionFacturaPOS.Show()
        Me.Visible = False
    End Sub

    Private Sub CmdActualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdActualizar.Click
        FormActualizar_InventarioFisico.CallerForm = Me
        FormActualizar_InventarioFisico.Show()
        Me.Visible = False
    End Sub

    Private Sub CmdModLogisticaAlmacenaje_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdModLogisticaAlmacenaje.Click
        FormMenuModLog_1.Show()
        Me.Visible = False
    End Sub

    Private Sub formMenuPrincipal_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        AjustarFormularios(Me)
    End Sub

    Private Sub Activation()

        HideOptions(Me)

        'If OpcionUsuarioActualHabilitada("ActualizarDatos_PDA") Then ' Por los momentos True Fijo.
        If CompatibilidadFueraDeLinea Then
            ShowMenuOption(Me, CmdActualizar, lblActualizar, NextVisibleMenu(Me))
        End If
        'End If

        If (OpcionUsuarioActualHabilitada("modinv")) Then
            ShowMenuOption(Me, cmbInventario, lblInventario, NextVisibleMenu(Me))
        End If

        If (OpcionUsuarioActualHabilitada("modinv")) Then
            If (OpcionUsuarioActualHabilitada("Ficha_ProductosLite")) _
            Or (OpcionUsuarioActualHabilitada("Ficha_Productos")) _
            Or (OpcionUsuarioActualHabilitada("Frm_ConsultaProductos")) _
            Then
                ShowMenuOption(Me, cmbConsulta, lblConsulta, NextVisibleMenu(Me))
            End If
        End If

        If (OpcionUsuarioActualHabilitada("Frm_Etiquetas_codebar") Or _
        (OpcionUsuarioActualHabilitada("modinv") And _
            (OpcionUsuarioActualHabilitada("Ficha_ProductosLite") _
            Or OpcionUsuarioActualHabilitada("Ficha_Productos") _
            Or OpcionUsuarioActualHabilitada("Frm_ConsultaProductos")))) _
            Then
            ShowMenuOption(Me, cmbEtiquetas, lblEtiquetas, NextVisibleMenu(Me))
        End If

        If (OpcionUsuarioActualHabilitada("habladores") Or _
        (OpcionUsuarioActualHabilitada("modinv") And _
            (OpcionUsuarioActualHabilitada("Ficha_ProductosLite") _
            Or OpcionUsuarioActualHabilitada("Ficha_Productos") _
            Or OpcionUsuarioActualHabilitada("Frm_ConsultaProductos")))) _
            Then
            ShowMenuOption(Me, cmbHabladores, lblHabladores, NextVisibleMenu(Me))
        End If

        If (OpcionUsuarioActualHabilitada("modpos")) Then
            If (OpcionUsuarioActualHabilitada("ValidarFacPos_Mobile")) Then
                ShowMenuOption(Me, cmbValidacionFacturaPOS, lblValidacionFacturaPOS1, NextVisibleMenu(Me))
            End If
        End If

        If (OpcionUsuarioActualHabilitada("modlog")) Then
            ShowMenuOption(Me, CmdModLogisticaAlmacenaje, lblModLog, NextVisibleMenu(Me))
        End If

    End Sub

    Private Sub TimerReload_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TimerReload.Tick
        TimerReload.Enabled = False
        Activation()
    End Sub

End Class