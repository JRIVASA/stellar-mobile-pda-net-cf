﻿Imports System.Collections.Generic

Public Class FormElab_Inv_InventarioFisico

    Dim TipoPeso As Integer
    Public Linea As Integer
    Dim Dataset_Codigos, Dataset_Productos As Data.DataSet

    Private Sub FormINVENTARIO_ELAB_INV_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        lblUsuarioEnSesion.Text = "Usuario: " + descripcionUsuario

        If CompatibilidadFueraDeLinea Then
            cmbActualizar.Visible = True
        Else
            cmbActualizar.Visible = False
        End If

        Me.Refresh()
        Me.Update()
        Linea = 1

        AjustarFormularios(Me)
        'AjustarFormularios(Me.PanelInventario_Elab_Inv)

        txtCodigoProducto.Focus()

    End Sub

    Private Sub cmbFinalizar_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbFinalizar.Click

        Dim Respuesta As String

        Respuesta = MsgBox("¿Está seguro que desea finalizar el inventario?", MsgBoxStyle.YesNo, "isMOBILE")

        If Respuesta = "6" Then

            'Si no hay registros en TR_INV_FISICO
            If verifica_Inventario(lblNumDoc.Text) = 0 Then
                'Limpia el formulario y no genera documento de inventario
                lblNumDoc.Text = ""
                lblNumDeposito.Text = ""
                limpiarForm_Elab_Inv()
                formMenuPrincipal.Show()
                Linea = 1
                MsgBox("No se generó documento de inventario.", MsgBoxStyle.Information, "isMOBILE")
            Else
                FormObservacionMotivo_InventarioFisico.Show()
                Me.Visible = False
            End If

        Else
            txtCodigoProducto.Focus()
        End If

    End Sub

    Private Sub txtCodigoProducto_KeyDown1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCodigoProducto.KeyDown
        If e.KeyValue = 13 Then
            txtEmpaque.Focus()
        End If
    End Sub

    Private Sub txtCodigoProducto_LostFocus1(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCodigoProducto.LostFocus

        'Cuando el txtCodigoProducto pierde el foco y no esta vacio; se verifica que el codigo existe
        If Not (txtCodigoProducto.Text = String.Empty) Then

            If txtCodigoProducto.TextLength <= 15 Then

                lblEmpaque.Text = "Empaques"
                lblUnidad.Text = "Unidades"

                Dataset_Codigos = buscarCodigoProducto(txtCodigoProducto.Text)

                If (Dataset_Codigos Is Nothing) Then
                    MsgBox("La información del producto no se ha cargado. Ingrese el producto nuevamente.", MsgBoxStyle.Information, "isMOBILE")
                    txtCodigoProducto.SelectAll()
                    txtCodigoProducto.Focus()
                    Return
                End If

                'Se verifica que la consulta trajo resultados o no
                If Dataset_Codigos.Tables(0).Rows.Count() <= 0 Then

                    MsgBox("El código de producto no existe.", MsgBoxStyle.Information, "isMOBILE")
                    txtCodigoProducto.Text = ""
                    lblDescripcion.Text = "Descripción: "
                    txtCodigoProducto.Focus()
                    Return

                Else

                    txtCodigoProducto.Text = Dataset_Codigos.Tables(0).Rows(0)("C_CODNASA")
                    Dataset_Productos = detallesProducto(txtCodigoProducto.Text)

                    If (Dataset_Productos Is Nothing) Then
                        MsgBox("La información del producto no se ha cargado. Ingrese el producto nuevamente.", MsgBoxStyle.Information, "isMOBILE")
                        txtCodigoProducto.Focus()
                        Return
                    ElseIf (Dataset_Productos.Tables(0).Rows.Count <= 0) Then
                        MsgBox("La información del producto no se ha cargado. Ingrese el producto nuevamente.", MsgBoxStyle.Information, "isMOBILE")
                        txtCodigoProducto.Focus()
                        Return
                    End If

                    lblDescripcion.Text = "Descripción: " + Dataset_Productos.Tables(0).Rows(0)("C_DESCRI")
                    TipoPeso = Dataset_Productos.Tables(0).Rows(0)("n_tipopeso")

                    If ChkRevisar.Checked Then

                        comandoBD_PDA.CommandText = "SELECT c_Documento, c_CodArticulo, " & _
                        "SUM(n_Cantidad) AS CantTotal, AVG(ns_CantidadEmpaque) AS CantEmp " & _
                        "FROM TR_INV_FISICO " & _
                        "WHERE c_Documento = @Doc " & _
                        "AND c_CodArticulo = @CodProducto " & _
                        "GROUP BY c_Documento, c_CodArticulo " & _
                        "ORDER BY c_Documento, c_CodArticulo"

                        comandoBD_PDA.Parameters.Clear()
                        comandoBD_PDA.Parameters.Add("@Doc", lblNumDoc.Text)
                        comandoBD_PDA.Parameters.Add("@CodProducto", txtCodigoProducto.Text)

                        Dim DSConsulta As DataSet = consultaQuery_PDA(comandoBD_PDA)
                        Dim EstaEnDoc As Boolean

                        If Not DSConsulta Is Nothing Then
                            If DSConsulta.Tables(0).Rows.Count > 0 Then
                                If Not IsDBNull(DSConsulta.Tables(0).Rows(0)("CantTotal")) Then
                                    EstaEnDoc = True
                                End If
                            End If
                        End If

                        If EstaEnDoc Then

                            Dim TempDec, Decimales, CantEmp As Double, Emp, Und

                            TempDec = Dataset_Productos.Tables(0).Rows(0)("cant_DECIMALES")
                            Decimales = IIf(IsNumeric(TempDec), Val(TempDec), 0)

                            CantEmp = Dataset_Productos.Tables(0).Rows(0)("n_Cantibul")
                            If CantEmp <= 0 Then CantEmp = 1

                            If CantEmp <> 1 Then
                                Emp = Math.Round(Fix(CDbl(DSConsulta.Tables(0).Rows(0)("CantTotal")) / CantEmp), 8)
                                Und = Math.Round(CDbl(DSConsulta.Tables(0).Rows(0)("CantTotal")) Mod CantEmp, 8)
                            Else
                                Emp = 0
                                Und = Math.Round(CDbl(DSConsulta.Tables(0).Rows(0)("CantTotal")), 8)
                            End If

                            txtUnidad.Text = Und
                            txtEmpaque.Text = Emp

                            lblEmpaque.Text = "Empaques" & " " & "(x" & CantEmp & ")"

                        Else
                            lblEmpaque.Text = "Empaques"
                            txtUnidad.Text = "N/A"
                            txtEmpaque.Text = "N/A"
                        End If

                        txtCodigoProducto.SelectAll()
                        ChkRevisar.Focus()

                    Else

                        'Se evalua el tipo de peso 
                        'If tipoPeso = 1 Or tipoPeso = 2 Then

                        'lblEmpaque.Text = "Kg"
                        'lblUnidad.Text = "g"
                        'Else WTF

                        'lblUnidad.Text = "Unidades"

                        'Se evalua la cantidad establecida para el producto y saber si la presentacion es por unidad y/o empaque
                        If Dataset_Codigos.Tables(0).Rows(0)("N_CANTIDAD") > 1 Then
                            lblEmpaque.Text = "Empaques" & " " & "(x" & Dataset_Codigos.Tables(0).Rows(0)("N_CANTIDAD") & ")"
                            txtUnidad.Enabled = False
                            If chkQuickScan.Checked Then
                                'txtUnidad.Text = Math.Round(CDbl(Dataset_Codigos.Tables(0).Rows(0)("N_CANTIDAD")), 8)
                                txtUnidad.Text = "0"
                                txtEmpaque.Text = "1"
                                cmbAgregar_Click_1(cmbAgregar, Nothing)
                                Exit Sub
                            End If
                        Else
                            lblEmpaque.Text = "Empaques" & " " & "(x" & Dataset_Productos.Tables(0).Rows(0)("n_cantibul") & ")"
                            txtUnidad.Enabled = True
                            If chkQuickScan.Checked Then
                                txtEmpaque.Text = "0"
                                txtUnidad.Text = "1"
                                cmbAgregar_Click_1(cmbAgregar, Nothing)
                                Exit Sub
                            End If
                        End If

                    End If

                End If

            Else

                MsgBox("Código inválido.", MsgBoxStyle.Information, "isMOBILE")
                txtCodigoProducto.Text = ""
                txtCodigoProducto.Focus()

            End If

        End If

    End Sub

    Private Sub cmbSalir_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSalir.Click
        FormInv_Activo_InventarioFisico.Show()
        Me.Visible = False
    End Sub

    Private Sub cmbAgregar_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbAgregar.Click

        Dim Decimales As Integer
        Dim TempDec
        Dim CantEmpaque, CantTotal As Double

        'Verifica que el formulario no este vacio
        If (txtCodigoProducto.Text = "") Then
            MsgBox("Debe ingresar el código del producto.", MsgBoxStyle.Information, "isMOBILE")
            txtCodigoProducto.Focus()
        Else

            If (Dataset_Productos Is Nothing Or Dataset_Codigos Is Nothing) Then
                MsgBox("La información del producto no se ha cargado. Ingrese el producto nuevamente.", MsgBoxStyle.Information, "isMOBILE")
                txtCodigoProducto.Focus()
                Return
            ElseIf (Dataset_Productos.Tables(0).Rows.Count <= 0 Or Dataset_Codigos.Tables(0).Rows.Count <= 0) Then
                MsgBox("El producto no existe en la base de datos." & vbNewLine _
                & "Es recomendable que actualice los productos antes de continuar" & vbNewLine _
                & "con el proceso, a través del modulo de Inventario.", MsgBoxStyle.Information, "isMOBILE")
                txtCodigoProducto.Focus()
                Return
            End If

            If txtUnidad.Text = "" And txtEmpaque.Text = "" Then
                MsgBox("Debe ingresar la cantidad de unidades y/o empaques.", MsgBoxStyle.Information, "isMOBILE")
                txtEmpaque.Focus()
            Else

                TempDec = Dataset_Productos.Tables(0).Rows(0)("cant_DECIMALES")
                Decimales = IIf(IsNumeric(TempDec), Val(TempDec), 0)

                'Determinando el parametro n_cantidad de TR_INV_FISICO segun la cantidad de MA_CODIGOS
                If Dataset_Codigos.Tables(0).Rows(0)("N_CANTIDAD") > 1 Then

                    CantEmpaque = Dataset_Codigos.Tables(0).Rows(0)("N_CANTIDAD")
                    'cantEmpaque = dataset_Productos.Tables(0).Rows(0)("N_CANTIBUL")
                    CantTotal = IIf(IsNumeric(txtEmpaque.Text), Val(txtEmpaque.Text), 0) * CantEmpaque

                Else

                    If Not IsNumeric(txtEmpaque.Text) Then
                        txtEmpaque.Text = 0
                    End If

                    If Not IsNumeric(txtUnidad.Text) Then
                        txtUnidad.Text = 0
                    End If

                    CantEmpaque = Dataset_Productos.Tables(0).Rows(0)("n_cantibul")
                    'Determinando el parametro n_cantidad segun el tipo de peso
                    'If tipoPeso = 0 Or tipoPeso = 5 Then
                    CantTotal = txtUnidad.Text + (CDbl(txtEmpaque.Text) * CantEmpaque)
                    'Else    'tipoPeso = 1 Or tipoPeso = 2

                    'cantTotal = FormatNumber(txtEmpaque.Text & "." & txtUnidad.Text, decimales)
                    'End If  --------------WTF

                End If

                'Si anexo producto al inventario la linea sumara 1
                Linea = Linea + añadirProductoInv(CantTotal, CantEmpaque, Linea)

            End If

        End If

    End Sub

    Private Sub txtEmpaque_KeyDown1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtEmpaque.KeyDown
        If txtUnidad.Enabled = False Then
            If e.KeyValue = 13 Then
                cmbAgregar.Focus()
            End If
        Else
            If e.KeyValue = 13 Then
                txtUnidad.Focus()
            End If
        End If
    End Sub

    Private Sub txtEmpaque_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtEmpaque.KeyPress
        'If Char.IsNumber(e.KeyChar) Then 'Comentado, no permitía ingresar . para valores decimales
        'MsgBox("-!" & txtEmpaque.Text & e.KeyChar.ToString & "!-") Nueva forma de Validar Aprobada.
        If IsNumeric(txtEmpaque.Text & e.KeyChar.ToString) Then
            e.Handled = False
        Else
            If Char.IsControl(e.KeyChar) Then
                e.Handled = False
            Else
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txtUnidad_KeyDown1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtUnidad.KeyDown
        If e.KeyValue = 13 Then
            cmbAgregar.Focus()
        End If
    End Sub

    Private Sub txtUnidad_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtUnidad.KeyPress
        'If Char.IsNumber(e.KeyChar) Then 'Comentado, no permitía ingresar . para valores decimales
        'MsgBox("-!" & txtUnidad.Text & e.KeyChar.ToString & "!-") Nueva forma de Validar Aprobada.
        If IsNumeric(txtUnidad.Text & e.KeyChar.ToString) Then
            e.Handled = False
        Else
            If Char.IsControl(e.KeyChar) Then
                e.Handled = False
            Else
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txtEmpaque_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEmpaque.LostFocus
        Dim TmpCantDec As Short
        Dim TmpText As String
        If IsNumeric(txtEmpaque.Text) And Not (Dataset_Productos Is Nothing) Then
            If Dataset_Productos.Tables(0).Rows.Count <> 0 Then
                TmpText = txtEmpaque.Text
                TmpCantDec = CShort(Val(Dataset_Productos.Tables(0).Rows(0)("CANT_DECIMALES")))
                txtEmpaque.Text = FormatNumber(txtEmpaque.Text, TmpCantDec, TriState.True, TriState.False, TriState.False)
                If TmpText <> txtEmpaque.Text Then txtEmpaque.Focus()
            End If
        End If
    End Sub

    Private Sub txtUnidad_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUnidad.LostFocus
        Dim TmpCantDec As Short
        Dim TmpText As String
        If IsNumeric(txtUnidad.Text) And Not (Dataset_Productos Is Nothing) Then
            If Dataset_Productos.Tables(0).Rows.Count <> 0 Then
                TmpText = txtUnidad.Text
                TmpCantDec = CShort(Val(Dataset_Productos.Tables(0).Rows(0)("CANT_DECIMALES")))
                txtUnidad.Text = FormatNumber(txtUnidad.Text, TmpCantDec, TriState.True, TriState.False, TriState.False)
                If TmpText <> txtUnidad.Text Then txtUnidad.Focus()
            End If
        End If
    End Sub

    Private Sub cmbActualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbActualizar.Click
        If ActualizacionParcial("Productos") Then
            MsgBox("Actualización parcial de Productos completada.", MsgBoxStyle.Information, "isMOBILE")
        Else
            MsgBox("No se pudo realizar la Actualización parcial de Productos.", MsgBoxStyle.Information, "isMOBILE")
        End If
    End Sub

    Private Sub ChkRevisar_CheckStateChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChkRevisar.CheckStateChanged
        limpiarForm_Elab_Inv()
        If ChkRevisar.Checked Then
            lblCantidadProducto.Text = "Cantidad Guardada"
            cmbAgregar.Enabled = False
        Else
            lblCantidadProducto.Text = "Cantidad a Registrar"
            cmbAgregar.Enabled = True
        End If
    End Sub

    Private Sub chkQuickScan_CheckStateChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkQuickScan.CheckStateChanged
        limpiarForm_Elab_Inv()
    End Sub

    Private Sub txtCodigoProducto_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCodigoProducto.TextChanged

    End Sub

End Class
