﻿'Para poder tener acceso al proveedor de datos y realizar las operaciones necesarias
'sobre la base de datos.
Imports System.Data.SqlServerCe
Imports System.Net
Imports System.Data.SqlClient
Imports System.Data
Imports System.Data.Common.DbConnection
Imports System.Data.Common.DbCommand
Imports System.Data.Common.DbDataAdapter

Module ModuloBasedeDatos
    'Construcción del objeto SqlCeConnection que abre la conexión con la base de datos 
    'SQL PDA.
    Public conexionBD_PDA As New SqlCeConnection

    'Construcción del objeto SqlConnection que abre la conexión con la base de datos 
    'SQL Servidor.
    Public conexionBD_SERVIDOR As New SqlConnection()
    Public conexionBD_SERVIDORPOS As New SqlConnection()

    'Creación del objeto que posee las sentencia SQL a ejecutar contra la base de datos 
    Public comandBD_SERVIDOR As New SqlCommand()
    Public comandBD_PDA As New SqlCeCommand

    Dim sqlTran As SqlTransaction
    Dim dataSet_PDA, dataSet_Servidor As DataSet

    Dim numeroTraslado_SERVIDORsinFormato As Integer
    Dim dataset_Monedas As New Data.DataSet

    '***********************FUNCIONES DE CONEXIÓN LOCAL*******************************

    Function crearDB_PDA(ByVal ruta As String) As Integer

        'Para crear la base de datos: se necesita el objeto SqlCeEngine de ADO.NET para SQL Mobile.
        Dim crearBD_PDA As New SqlCeEngine
        Try
            crearBD_PDA.LocalConnectionString = ruta
            crearBD_PDA.CreateDatabase()
            crearBD_PDA.Dispose()
            Return 1
        Catch sqlEx As SqlCeException
            MessageBox.Show("Ha ocurrido una excepción:" + sqlEx.Message + "Creando Base de Datos")
            Return 0
        End Try

    End Function

    Sub conectarDB_PDA(ByVal ruta As String)

        Try
            If conexionBD_PDA.State <> ConnectionState.Open Then
                conexionBD_PDA.ConnectionString = ruta
                conexionBD_PDA.Open()
            End If
        Catch sqlEx As SqlCeException
            MessageBox.Show("Ha ocurrido una excepción:" + sqlEx.Message + "Conectando la Base de Datos Local")
        End Try

    End Sub

    Function ejecutaQuery_PDA(ByVal comandoBD_PDA As SqlCeCommand, Optional ByVal ShowErrMessage As Boolean = True) As Boolean

        Try
            'Asigno la conexion al objeto SqlCeCommand            
            comandoBD_PDA.Connection = conexionBD_PDA
            'Ejecuto la instruccion             
            comandoBD_PDA.ExecuteNonQuery()
            ejecutaQuery_PDA = True
        Catch sqlEx As SqlCeException
            If (ShowErrMessage) Then MessageBox.Show("Ha ocurrido una excepción:" + sqlEx.Message + "Actualizando la Tabla")
        Catch AnyEx As Exception
            If (ShowErrMessage) Then MessageBox.Show("Ha ocurrido una excepción:" + AnyEx.Message + "Actualizando la Tabla")
        Finally
            comandoBD_PDA.Parameters.Clear()
        End Try

    End Function

    Function consultaQuery_PDA(ByVal comandoBD_PDA As SqlCeCommand) As DataSet

        Dim Adapter_PDA As New SqlCeDataAdapter
        Dim DataSet_PDA As New System.Data.DataSet
        Try
            comandoBD_PDA.Connection = conexionBD_PDA
            'el metodo SelectCommand de un dataAdapter contiene los datos o información del origen de datos
            Adapter_PDA.SelectCommand = comandoBD_PDA
            'el metodo fill se usa para rellenar un objeto DataSet con los resultados del elemento SelectCommand
            Adapter_PDA.Fill(DataSet_PDA)
            Adapter_PDA.Dispose()
            Adapter_PDA = Nothing

        Catch sqlEx As SqlCeException
            MessageBox.Show("Ha ocurrido una excepción:" + sqlEx.Message + "Consultando BD del PDA")
            DataSet_PDA = Nothing
        Catch AnyEx As Exception
            MessageBox.Show("Ha ocurrido una excepción:" + AnyEx.Message + "Consultando BD del PDA")
            DataSet_PDA = Nothing
        Finally
            comandoBD_PDA.Parameters.Clear()
        End Try

        Return DataSet_PDA

    End Function

    Function GetScalar_PDA(ByVal CommandWithQueryAndParams As SqlCeCommand, ByVal DefaultValue As Object, _
    ByVal DefaultValueForDBNull As Object) As Object

        Try
            CommandWithQueryAndParams.Connection = conexionBD_PDA
            Dim Value As Object = CommandWithQueryAndParams.ExecuteScalar()
            Return IIf(Value.Equals(DBNull.Value), DefaultValueForDBNull, Value)
        Catch Any As Exception
            'Console.WriteLine(Any.Message)
            Return DefaultValue
        End Try

    End Function

    Function GetScalar_SRV(ByVal CommandWithQueryAndParams As SqlCommand, ByVal Connection As SqlConnection, _
    ByVal DefaultValue As Object, ByVal DefaultValueForDBNull As Object) As Object

        Try
            CommandWithQueryAndParams.Connection = Connection
            Dim Value As Object = CommandWithQueryAndParams.ExecuteScalar
            Return IIf(Value.Equals(DBNull.Value), DefaultValueForDBNull, Value)
        Catch Any As Exception
            'Console.WriteLine(Any.Message)
            Return DefaultValue
        End Try

    End Function

    Function returnString_PDA(ByVal sentenciaSQL As String) As String

        Try
            comandBD_PDA.CommandText = sentenciaSQL
            comandBD_PDA.Connection = conexionBD_PDA
            Return comandBD_PDA.ExecuteScalar
        Catch sqlEx As SqlCeException
            MessageBox.Show("Ha ocurrido una excepción:" + sqlEx.Message + " retornando scalar string")
        Catch AnyEx As Exception
            MessageBox.Show("Ha ocurrido una excepción:" + AnyEx.Message + " retornando scalar string.")
        End Try

        Return String.Empty

    End Function

    Function returnInteger_PDA(ByVal sentenciaSQL As String) As Integer

        Try
            comandBD_PDA.CommandText = sentenciaSQL
            comandBD_PDA.Connection = conexionBD_PDA
            Return comandBD_PDA.ExecuteScalar
        Catch sqlEx As SqlCeException
            MessageBox.Show("Ha ocurrido una excepción:" + sqlEx.Message + " Consultando scalar integer")
        Catch AnyEx As Exception
            MessageBox.Show("Ha ocurrido una excepción:" + AnyEx.Message + " Consultando scalar integer")
        End Try

        Return 0

    End Function

    Function returnDouble_PDA(ByVal sentenciaSQL As String) As Double

        Try
            comandBD_PDA.CommandText = sentenciaSQL
            comandBD_PDA.Connection = conexionBD_PDA
            Return Convert.ToDouble(comandBD_PDA.ExecuteScalar)
        Catch sqlEx As SqlCeException
            MessageBox.Show("Ha ocurrido una excepción:" + sqlEx.Message + " Consultando scalar double")
        Catch AnyEx As Exception
            MessageBox.Show("Ha ocurrido una excepción:" + AnyEx.Message + " Consultando scalar double")
        End Try

        Return 0

    End Function

    Sub insertarRegistros_MA_ELAB_INVENTARIO_PDA(ByVal dataSet_Origen As Data.DataSet)

        Dim adapter_MA_ELAB_INVENTARIO As New SqlCeDataAdapter
        Try
            adapter_MA_ELAB_INVENTARIO.InsertCommand = New SqlCeCommand("INSERT INTO MA_ELAB_INVENTARIO VALUES (@F_INiCIO, @c_coddeposito, @CU_CODDEPARTAMENTO, @CU_CODGRUPO, @CU_CODSUBGRUPO, @CU_CODPROVEEDOR, @no_elab)", conexionBD_PDA)
            'Se da un for por cada registro dentro del DataSet
            For Each registro As Data.DataRow In dataSet_Origen.Tables("MA_ELAB_INVENTARIO").Rows
                'Añado los valores a los parametros
                adapter_MA_ELAB_INVENTARIO.InsertCommand.Parameters.Add("@F_INiCIO", registro("F_INiCIO"))
                adapter_MA_ELAB_INVENTARIO.InsertCommand.Parameters.Add("@c_coddeposito", registro("c_coddeposito"))
                adapter_MA_ELAB_INVENTARIO.InsertCommand.Parameters.Add("@CU_CODDEPARTAMENTO", registro("CU_CODDEPARTAMENTO"))
                adapter_MA_ELAB_INVENTARIO.InsertCommand.Parameters.Add("@CU_CODGRUPO", registro("CU_CODGRUPO"))
                adapter_MA_ELAB_INVENTARIO.InsertCommand.Parameters.Add("@CU_CODSUBGRUPO", registro("CU_CODSUBGRUPO"))
                adapter_MA_ELAB_INVENTARIO.InsertCommand.Parameters.Add("@CU_CODPROVEEDOR", registro("CU_CODPROVEEDOR"))
                adapter_MA_ELAB_INVENTARIO.InsertCommand.Parameters.Add("@no_elab", registro("no_elab"))
                adapter_MA_ELAB_INVENTARIO.InsertCommand.ExecuteNonQuery()
                'Limpio los parametros para leer los del registro siguiente
                adapter_MA_ELAB_INVENTARIO.InsertCommand.Parameters.Clear()
            Next
        Catch sqlEx As SqlCeException
            MessageBox.Show("Ha ocurrido una excepción:" + sqlEx.Message + "Al insertar en la tabla MA_ELAB_INVENTARIO")
        End Try

    End Sub

    Sub insertarRegistros_MA_DEPOSITO_PDA(ByVal dataSet_Origen As Data.DataSet)

        Dim adapter_MA_DEPOSITO As New SqlCeDataAdapter
        Try
            adapter_MA_DEPOSITO.InsertCommand = New SqlCeCommand("INSERT INTO MA_DEPOSITO VALUES (@c_coddeposito, @c_descripcion, @c_codlocalidad)", conexionBD_PDA)
            For Each registro As Data.DataRow In dataSet_Origen.Tables("MA_DEPOSITO").Rows
                adapter_MA_DEPOSITO.InsertCommand.Parameters.Add("@c_coddeposito", registro("c_coddeposito"))
                adapter_MA_DEPOSITO.InsertCommand.Parameters.Add("@c_descripcion", registro("c_descripcion"))
                adapter_MA_DEPOSITO.InsertCommand.Parameters.Add("@c_codlocalidad", registro("c_codlocalidad"))
                adapter_MA_DEPOSITO.InsertCommand.ExecuteNonQuery()
                adapter_MA_DEPOSITO.InsertCommand.Parameters.Clear()
            Next
        Catch sqlEx As SqlCeException
            MessageBox.Show("Ha ocurrido una excepción:" + sqlEx.Message + "Al insertar en la tabla MA_DEPOSITO")
        End Try

    End Sub

    Sub insertarRegistros_MA_USUARIOS_PDA(ByVal dataSet_Origen As Data.DataSet)

        Dim adapter_MA_USUARIOS As New SqlCeDataAdapter
        Try
            adapter_MA_USUARIOS.InsertCommand = New SqlCeCommand("INSERT INTO MA_USUARIOS  VALUES (@codusuario, @password, @login_name, @nivel, @clave, @descripcion)", conexionBD_PDA)
            For Each registro As Data.DataRow In dataSet_Origen.Tables("MA_USUARIOS").Rows
                adapter_MA_USUARIOS.InsertCommand.Parameters.Add("@codusuario", registro("codusuario"))
                adapter_MA_USUARIOS.InsertCommand.Parameters.Add("@password", registro("password"))
                adapter_MA_USUARIOS.InsertCommand.Parameters.Add("@login_name", registro("login_name"))
                adapter_MA_USUARIOS.InsertCommand.Parameters.Add("@nivel", registro("nivel"))
                adapter_MA_USUARIOS.InsertCommand.Parameters.Add("@clave", registro("clave"))
                adapter_MA_USUARIOS.InsertCommand.Parameters.Add("@descripcion", registro("descripcion"))
                adapter_MA_USUARIOS.InsertCommand.ExecuteNonQuery()
                adapter_MA_USUARIOS.InsertCommand.Parameters.Clear()
            Next
        Catch sqlEx As SqlCeException
            MessageBox.Show("Ha ocurrido una excepción:" + sqlEx.Message + "Al insertar en la tabla MA_USUARIOS")
        End Try

    End Sub

    Sub insertarRegistros_CONF_MENU_USER_PDA(ByVal dataSet_Origen As Data.DataSet, _
    Optional ByVal pClave_User As String = "")

        Dim Trans As SqlCeTransaction = Nothing
        Dim adapter_CONF_MENU_USER As New SqlCeDataAdapter

        Try

            Trans = conexionBD_PDA.BeginTransaction

            If Not String.IsNullOrEmpty(pClave_User) Then

                comandoBD_PDA.Parameters.Clear()
                comandoBD_PDA.CommandText = "DELETE FROM CONF_MENU_USER WHERE Clave_User = @Clave_User"
                comandoBD_PDA.Parameters.Add("@Clave_User", pClave_User)
                comandoBD_PDA.Transaction = Trans
                comandoBD_PDA.Connection = conexionBD_PDA

                'Ejecuto la instruccion             
                comandoBD_PDA.ExecuteNonQuery()

            Else

                comandoBD_PDA.Parameters.Clear()
                comandoBD_PDA.CommandText = "DELETE FROM CONF_MENU_USER"
                comandoBD_PDA.Transaction = Trans
                comandoBD_PDA.Connection = conexionBD_PDA

                'Ejecuto la instruccion             
                comandoBD_PDA.ExecuteNonQuery()

            End If

            adapter_CONF_MENU_USER.InsertCommand = New SqlCeCommand("INSERT INTO CONF_MENU_USER (Clave_User, Clave_Menu, Texto, Activado, Icono, Forma, Relacion, ResourceId) VALUES " + _
            "(@Clave_User, @Clave_Menu, @Texto, @Activado, @Icono, @Forma, @Relacion, @ResourceId)", conexionBD_PDA)

            For Each registro As Data.DataRow In dataSet_Origen.Tables("CONF_MENU_USER").Rows
                adapter_CONF_MENU_USER.InsertCommand.Transaction = Trans
                adapter_CONF_MENU_USER.InsertCommand.Parameters.Add("@Clave_User", registro("Clave_User"))
                adapter_CONF_MENU_USER.InsertCommand.Parameters.Add("@Clave_Menu", registro("Clave_Menu"))
                adapter_CONF_MENU_USER.InsertCommand.Parameters.Add("@Texto", registro("Texto"))
                adapter_CONF_MENU_USER.InsertCommand.Parameters.Add("@Activado", registro("Activado"))
                adapter_CONF_MENU_USER.InsertCommand.Parameters.Add("@Icono", registro("Icono"))
                adapter_CONF_MENU_USER.InsertCommand.Parameters.Add("@Forma", registro("Forma"))
                adapter_CONF_MENU_USER.InsertCommand.Parameters.Add("@Relacion", registro("Relacion"))
                adapter_CONF_MENU_USER.InsertCommand.Parameters.Add("@ResourceId", registro("ResourceId"))
                adapter_CONF_MENU_USER.InsertCommand.ExecuteNonQuery()
                adapter_CONF_MENU_USER.InsertCommand.Parameters.Clear()
            Next

            Trans.Commit()

            'If DebugMode Then MsgBox("CONF_MENU_USER actualizado.")

        Catch sqlEx As SqlCeException

            If Not Trans Is Nothing Then Trans.Rollback()
            MessageBox.Show("Ha ocurrido una excepción:" + sqlEx.Message + "Al insertar en la tabla CONF_MENU_USER")

        Catch AnyOtherEx As Exception

            If Not Trans Is Nothing Then Trans.Rollback()
            MessageBox.Show("Ha ocurrido una excepción:" + AnyOtherEx.Message + "Al insertar en la tabla CONF_MENU_USER")

        Finally

            comandoBD_PDA.Transaction = Nothing
            comandoBD_PDA.Parameters.Clear()

        End Try

    End Sub

    Sub insertarRegistros_MA_CODIGOS_PDA(ByVal dataSet_Origen As Data.DataSet)

        Dim adapter_MA_CODIGOS As New SqlCeDataAdapter
        Try
            adapter_MA_CODIGOS.InsertCommand = New SqlCeCommand("INSERT INTO MA_CODIGOS VALUES  (@c_codnasa, @c_codigo, @c_descripcion, @n_cantidad)", conexionBD_PDA)
            For Each registro As Data.DataRow In dataSet_Origen.Tables("MA_CODIGOS").Rows

                adapter_MA_CODIGOS.InsertCommand.Parameters.Add("@c_codnasa", registro("c_codnasa"))
                adapter_MA_CODIGOS.InsertCommand.Parameters.Add("@c_codigo", registro("c_codigo"))
                adapter_MA_CODIGOS.InsertCommand.Parameters.Add("@c_descripcion", registro("c_descripcion"))
                adapter_MA_CODIGOS.InsertCommand.Parameters.Add("@n_cantidad", registro("n_cantidad"))
                adapter_MA_CODIGOS.InsertCommand.ExecuteNonQuery()
                adapter_MA_CODIGOS.InsertCommand.Parameters.Clear()
            Next
        Catch sqlEx As SqlCeException
            MessageBox.Show("Ha ocurrido una excepción:" + sqlEx.Message + "Al insertar en la tabla MA_CODIGOS")
        End Try

    End Sub

    Sub insertarRegistros_MA_PRODUCTOS_PDA(ByVal dataSet_Origen As Data.DataSet)

        Dim adapter_MA_PRODUCTOS As New SqlCeDataAdapter
        Try
            adapter_MA_PRODUCTOS.InsertCommand = New SqlCeCommand("INSERT INTO MA_PRODUCTOS VALUES (@C_CODIGO, @C_DESCRI, @c_departamento, @c_grupo, @c_subgrupo, @n_costoact, @n_costoant, @n_costopro, @n_cantibul, @n_tipopeso, @cant_DECIMALES)", conexionBD_PDA)
            For Each registro As Data.DataRow In dataSet_Origen.Tables("MA_PRODUCTOS").Rows
                adapter_MA_PRODUCTOS.InsertCommand.Parameters.Add("@C_CODIGO", registro("C_CODIGO"))
                adapter_MA_PRODUCTOS.InsertCommand.Parameters.Add("@C_DESCRI", registro("C_DESCRI"))
                adapter_MA_PRODUCTOS.InsertCommand.Parameters.Add("@c_departamento", registro("c_departamento"))
                adapter_MA_PRODUCTOS.InsertCommand.Parameters.Add("@c_grupo", registro("c_grupo"))
                adapter_MA_PRODUCTOS.InsertCommand.Parameters.Add("@c_subgrupo", registro("c_subgrupo"))
                adapter_MA_PRODUCTOS.InsertCommand.Parameters.Add("@n_costoact", registro("n_costoact"))
                adapter_MA_PRODUCTOS.InsertCommand.Parameters.Add("@n_costoant", registro("n_costoant"))
                adapter_MA_PRODUCTOS.InsertCommand.Parameters.Add("@n_costopro", registro("n_costopro"))
                adapter_MA_PRODUCTOS.InsertCommand.Parameters.Add("@n_cantibul", registro("n_cantibul"))
                adapter_MA_PRODUCTOS.InsertCommand.Parameters.Add("@n_tipopeso", registro("n_tipopeso"))
                adapter_MA_PRODUCTOS.InsertCommand.Parameters.Add("@cant_DECIMALES", registro("cant_DECIMALES"))
                adapter_MA_PRODUCTOS.InsertCommand.ExecuteNonQuery()
                adapter_MA_PRODUCTOS.InsertCommand.Parameters.Clear()
            Next
        Catch sqlEx As SqlCeException
            MessageBox.Show("Ha ocurrido una excepción:" + sqlEx.Message + "Al insertar en la tabla MA_PRODUCTOS")
        End Try

    End Sub

    Sub insertarRegistros_MA_MONEDAS_PDA(ByVal dataSet_Origen As Data.DataSet)

        Dim adapter_MA_MONEDAS As New SqlCeDataAdapter
        Try
            adapter_MA_MONEDAS.InsertCommand = New SqlCeCommand("INSERT INTO MA_MONEDAS  VALUES (@c_codmoneda, @n_factor, @n_decimales)", conexionBD_PDA)
            For Each registro As Data.DataRow In dataSet_Origen.Tables("MA_MONEDAS").Rows
                adapter_MA_MONEDAS.InsertCommand.Parameters.Add("@c_codmoneda", registro("c_codmoneda"))
                adapter_MA_MONEDAS.InsertCommand.Parameters.Add("@n_factor", registro("n_factor"))
                adapter_MA_MONEDAS.InsertCommand.Parameters.Add("@n_decimales", registro("n_decimales"))
                adapter_MA_MONEDAS.InsertCommand.ExecuteNonQuery()
                adapter_MA_MONEDAS.InsertCommand.Parameters.Clear()
            Next
        Catch sqlEx As SqlCeException
            MessageBox.Show("Ha ocurrido una excepción:" + sqlEx.Message + "Al insertar en la tabla MA_MONEDAS")
        End Try

    End Sub

    Sub insertarRegistros_MA_PROVEEDORES_PDA(ByVal dataSet_Origen As Data.DataSet)

        Dim adapter_MA_PROVEEDORES As New SqlCeDataAdapter
        Try
            adapter_MA_PROVEEDORES.InsertCommand = New SqlCeCommand("INSERT INTO MA_PROVEEDORES  VALUES (@c_codproveed, @c_descripcio, @c_razon)", conexionBD_PDA)
            For Each registro As Data.DataRow In dataSet_Origen.Tables("MA_PROVEEDORES").Rows
                adapter_MA_PROVEEDORES.InsertCommand.Parameters.Add("@c_codproveed", registro("c_codproveed"))
                adapter_MA_PROVEEDORES.InsertCommand.Parameters.Add("@c_descripcio", registro("c_descripcio"))
                adapter_MA_PROVEEDORES.InsertCommand.Parameters.Add("@c_razon", registro("c_razon"))
                adapter_MA_PROVEEDORES.InsertCommand.ExecuteNonQuery()
                adapter_MA_PROVEEDORES.InsertCommand.Parameters.Clear()
            Next
        Catch sqlEx As SqlCeException
            MessageBox.Show("Ha ocurrido una excepción:" + sqlEx.Message + "Al insertar en la tabla MA_PROVEEDORES")
        End Try


    End Sub

    Sub insertarRegistros_MA_ODC_PDA(ByVal dataSet_Origen As Data.DataSet)

        Dim adapter_MA_ODC As New SqlCeDataAdapter
        Try
            adapter_MA_ODC.InsertCommand = New SqlCeCommand("INSERT INTO MA_ODC  VALUES (@c_documento, @d_fecha, @d_fecha_recepcion, @c_status, @c_codproveedor, @c_codlocalidad, @c_despachar, @c_codmoneda, @n_factorcambio)", conexionBD_PDA)
            For Each registro As Data.DataRow In dataSet_Origen.Tables("MA_ODC").Rows
                adapter_MA_ODC.InsertCommand.Parameters.Add("@c_documento", registro("c_documento"))
                adapter_MA_ODC.InsertCommand.Parameters.Add("@d_fecha", registro("d_fecha"))
                adapter_MA_ODC.InsertCommand.Parameters.Add("@d_fecha_recepcion", registro("d_fecha_recepcion"))
                adapter_MA_ODC.InsertCommand.Parameters.Add("@c_status", registro("c_status"))
                adapter_MA_ODC.InsertCommand.Parameters.Add("@c_codproveedor", registro("c_codproveedor"))
                adapter_MA_ODC.InsertCommand.Parameters.Add("@c_codlocalidad", registro("c_codlocalidad"))
                adapter_MA_ODC.InsertCommand.Parameters.Add("@c_despachar", registro("c_despachar"))
                adapter_MA_ODC.InsertCommand.Parameters.Add("@c_codmoneda", registro("c_codmoneda"))
                adapter_MA_ODC.InsertCommand.Parameters.Add("@n_factorcambio", registro("n_factorcambio"))
                adapter_MA_ODC.InsertCommand.ExecuteNonQuery()
                adapter_MA_ODC.InsertCommand.Parameters.Clear()
            Next
        Catch sqlEx As SqlCeException
            MessageBox.Show("Ha ocurrido una excepción:" + sqlEx.Message + "Al insertar en la tabla MA_ODC")
        End Try


    End Sub


    Sub insertarRegistros_MA_CORRELATIVOS_PDA(ByVal dataSet_Origen As Data.DataSet)

        Dim adapter_MA_CORRELATIVOS As New SqlCeDataAdapter
        Try
            adapter_MA_CORRELATIVOS.InsertCommand = New SqlCeCommand("INSERT INTO MA_CORRELATIVOS VALUES (@cu_campo, @nu_valor)", conexionBD_PDA)
            For Each registro As Data.DataRow In dataSet_Origen.Tables("MA_CORRELATIVOS").Rows
                adapter_MA_CORRELATIVOS.InsertCommand.Parameters.Add("@cu_campo", registro("cu_campo"))
                adapter_MA_CORRELATIVOS.InsertCommand.Parameters.Add("@nu_valor", registro("nu_valor"))
                adapter_MA_CORRELATIVOS.InsertCommand.ExecuteNonQuery()
                adapter_MA_CORRELATIVOS.InsertCommand.Parameters.Clear()
            Next
        Catch sqlEx As SqlCeException
            MessageBox.Show("Ha ocurrido una excepción:" + sqlEx.Message + "Al insertar en la tabla MA_CORRELATIVOS")
        End Try

    End Sub

    Sub insertarRegistros_MA_INVENTARIO_PDA(ByVal dataSet_Origen As Data.DataSet)

        Dim adapter_MA_INVENTARIO As New SqlCeDataAdapter
        Try
            adapter_MA_INVENTARIO.InsertCommand = New SqlCeCommand("INSERT INTO MA_INVENTARIO  VALUES (@c_documento, @c_codproveedor, @c_factura, @c_concepto, @c_status, @c_codlocalidad)", conexionBD_PDA)
            For Each registro As Data.DataRow In dataSet_Origen.Tables("MA_INVENTARIO").Rows
                adapter_MA_INVENTARIO.InsertCommand.Parameters.Add("@c_documento", registro("c_documento"))
                adapter_MA_INVENTARIO.InsertCommand.Parameters.Add("@c_codproveedor", registro("c_codproveedor"))
                adapter_MA_INVENTARIO.InsertCommand.Parameters.Add("@c_factura", registro("c_factura"))
                adapter_MA_INVENTARIO.InsertCommand.Parameters.Add("@c_concepto", registro("c_concepto"))
                adapter_MA_INVENTARIO.InsertCommand.Parameters.Add("@c_status", registro("c_status"))
                adapter_MA_INVENTARIO.InsertCommand.Parameters.Add("@c_codlocalidad", registro("c_codlocalidad"))
                adapter_MA_INVENTARIO.InsertCommand.ExecuteNonQuery()
                adapter_MA_INVENTARIO.InsertCommand.Parameters.Clear()
            Next
        Catch sqlEx As SqlCeException
            MessageBox.Show("Ha ocurrido una excepción:" + sqlEx.Message + "Al insertar en la tabla MA_INVENTARIO")
        End Try

    End Sub

    Sub insertarRegistros_MA_CONCEPTOS(ByVal dataSet_Origen As Data.DataSet)

        Dim adapter_MA_CONCEPTOS As New SqlCeDataAdapter
        Try
            adapter_MA_CONCEPTOS.InsertCommand = New SqlCeCommand("INSERT INTO MA_CONCEPTOS  VALUES (@idconcepto, @descripcion, @idproceso)", conexionBD_PDA)
            For Each registro As Data.DataRow In dataSet_Origen.Tables("MA_CONCEPTOS").Rows
                adapter_MA_CONCEPTOS.InsertCommand.Parameters.Add("@idconcepto", registro("idconcepto"))
                adapter_MA_CONCEPTOS.InsertCommand.Parameters.Add("@descripcion", registro("descripcion"))
                adapter_MA_CONCEPTOS.InsertCommand.Parameters.Add("@idproceso", registro("idproceso"))
                adapter_MA_CONCEPTOS.InsertCommand.ExecuteNonQuery()
                adapter_MA_CONCEPTOS.InsertCommand.Parameters.Clear()
            Next
        Catch sqlEx As SqlCeException
            MessageBox.Show("Ha ocurrido una excepción:" + sqlEx.Message + "Al insertar en la tabla MA_CONCEPTOS")
        End Try


    End Sub

    Sub insertarRegistros_MA_PROCESOS(ByVal dataSet_Origen As Data.DataSet)

        Dim adapter_MA_PROCESOS As New SqlCeDataAdapter
        Try
            adapter_MA_PROCESOS.InsertCommand = New SqlCeCommand("INSERT INTO MA_PROCESOS  VALUES (@idproceso, @proceso)", conexionBD_PDA)
            For Each registro As Data.DataRow In dataSet_Origen.Tables("MA_PROCESOS").Rows
                adapter_MA_PROCESOS.InsertCommand.Parameters.Add("@idproceso", registro("idproceso"))
                adapter_MA_PROCESOS.InsertCommand.Parameters.Add("@proceso", registro("proceso"))
                adapter_MA_PROCESOS.InsertCommand.ExecuteNonQuery()
                adapter_MA_PROCESOS.InsertCommand.Parameters.Clear()
            Next
        Catch sqlEx As SqlCeException
            MessageBox.Show("Ha ocurrido una excepción:" + sqlEx.Message + "Al insertar en la tabla MA_PROCESOS")
        End Try


    End Sub

    Sub insertarRegistros_TR_ODC(ByVal dataSet_Origen As Data.DataSet)

        Dim adapter_TR_ODC As New SqlCeDataAdapter
        Try
            adapter_TR_ODC.InsertCommand = New SqlCeCommand("INSERT INTO TR_ODC  VALUES (@c_documento, @c_codarticulo, @c_linea,@n_cantidad, @ns_cantidadEmpaque, @cs_codlocalidad, @n_cant_recibida, @n_costo, @n_subtotal, @n_impuesto, @n_total)", conexionBD_PDA)
            For Each registro As Data.DataRow In dataSet_Origen.Tables(0).Rows
                adapter_TR_ODC.InsertCommand.Parameters.Add("@c_documento", registro("c_documento"))
                adapter_TR_ODC.InsertCommand.Parameters.Add("@c_codarticulo", registro("c_codarticulo"))
                adapter_TR_ODC.InsertCommand.Parameters.Add("@c_linea", registro("c_linea"))
                adapter_TR_ODC.InsertCommand.Parameters.Add("@n_cantidad", registro("n_cantidad"))
                adapter_TR_ODC.InsertCommand.Parameters.Add("@ns_cantidadEmpaque", registro("ns_cantidadEmpaque"))
                adapter_TR_ODC.InsertCommand.Parameters.Add("@cs_codlocalidad", registro("cs_codlocalidad"))
                adapter_TR_ODC.InsertCommand.Parameters.Add("@n_cant_recibida", registro("n_cant_recibida"))
                adapter_TR_ODC.InsertCommand.Parameters.Add("@n_costo", registro("n_costo"))
                adapter_TR_ODC.InsertCommand.Parameters.Add("@n_subtotal", registro("n_subtotal"))
                adapter_TR_ODC.InsertCommand.Parameters.Add("@n_impuesto", registro("n_impuesto"))
                adapter_TR_ODC.InsertCommand.Parameters.Add("@n_total", registro("n_total"))
                adapter_TR_ODC.InsertCommand.ExecuteNonQuery()
                adapter_TR_ODC.InsertCommand.Parameters.Clear()
            Next
        Catch sqlEx As SqlCeException
            MessageBox.Show("Ha ocurrido una excepción:" + sqlEx.Message + "Al insertar en la tabla TR_ODC")
        End Try


    End Sub

    Sub insertarRegistros_MA_REGLASDENEGOCIO_PDA(ByVal dataSet_Origen As Data.DataSet)

        Dim adapter_MA_REGLASDENEGOCIO As New SqlCeDataAdapter

        Try
            adapter_MA_REGLASDENEGOCIO.InsertCommand = New SqlCeCommand("INSERT INTO MA_REGLASDENEGOCIO  VALUES (@campo, @valor)", conexionBD_PDA)
            For Each registro As Data.DataRow In dataSet_Origen.Tables("MA_REGLASDENEGOCIO").Rows
                adapter_MA_REGLASDENEGOCIO.InsertCommand.Parameters.Add("@campo", registro("campo"))
                adapter_MA_REGLASDENEGOCIO.InsertCommand.Parameters.Add("@valor", registro("valor"))
                adapter_MA_REGLASDENEGOCIO.InsertCommand.ExecuteNonQuery()
                adapter_MA_REGLASDENEGOCIO.InsertCommand.Parameters.Clear()
            Next
        Catch sqlEx As SqlCeException
            MessageBox.Show("Ha ocurrido una excepción:" + sqlEx.Message + "Al insertar en la tabla MA_REGLASDENEGOCIO")
        End Try

    End Sub

    '***********************FUNCIONES DE CONEXION REMOTA******************************

    Function conectarDB_SERVIDOR(ByVal ruta As String) As Integer

        Try
            If conexionBD_SERVIDOR.State <> ConnectionState.Open Then
                conexionBD_SERVIDOR.ConnectionString = ruta
                conexionBD_SERVIDOR.Open()
            End If
            Return 1
            'En caso que se produzca un error en las líneas de código del Try, 
            'el Catch contiene el código para controlar cualquier error que tenga lugar
            'y se produce una execpción'
        Catch sqlEx As SqlException
            Return 0
        End Try

    End Function

    Function conectarDB_SERVIDORPOS(ByVal ruta As String) As Integer

        Try
            If conexionBD_SERVIDORPOS.State <> ConnectionState.Open Then
                conexionBD_SERVIDORPOS.ConnectionString = Replace(UCase(ruta), "VAD10", "VAD20")
                conexionBD_SERVIDORPOS.Open()
            End If
            Return 1
            'En caso que se produzca un error en las líneas de código del Try, 
            'el Catch contiene el código para controlar cualquier error que tenga lugar
            'y se produce una execpción'
        Catch sqlEx As SqlException
            Return 0

        End Try

    End Function

    Public Function executeQuery_Servidor(ByVal comandoBD_SERVIDOR As SqlCommand) As Data.DataSet

        Dim Adapter_Servidor As New SqlDataAdapter
        Dim DataSet_Servidor As New System.Data.DataSet

        Try
            comandoBD_SERVIDOR.Connection = conexionBD_SERVIDOR
            'el metodo SelectCommand de un dataAdapter contiene los datos o información del origen de datos
            Adapter_Servidor.SelectCommand = (comandoBD_SERVIDOR)
            Adapter_Servidor.SelectCommand.Transaction = sqlTran
            Adapter_Servidor.Fill(DataSet_Servidor)
            Adapter_Servidor.Dispose()
        Catch sqlEx As SqlException
            MessageBox.Show("Ha ocurrido una excepción:" + sqlEx.Message + " Consulta Servidor")
        Catch AnyEx As Exception
            MessageBox.Show("Ha ocurrido una excepción general:" + AnyEx.Message + "al realizar una consulta al servidor.")
        Finally
            comandoBD_SERVIDOR.Parameters.Clear()
        End Try

        Return DataSet_Servidor

    End Function

    Public Function executeQuery_Servidor(ByVal comandoBD_SERVIDOR As SqlCommand, ByVal pConexion As SqlConnection) As Data.DataSet

        Dim Adapter_Servidor As New SqlDataAdapter
        Dim DataSet_Servidor As New System.Data.DataSet

        Try
            comandoBD_SERVIDOR.Connection = pConexion
            'el metodo SelectCommand de un dataAdapter contiene los datos o información del origen de datos
            Adapter_Servidor.SelectCommand = (comandoBD_SERVIDOR)
            Adapter_Servidor.SelectCommand.Transaction = sqlTran
            Adapter_Servidor.Fill(DataSet_Servidor)
            Adapter_Servidor.Dispose()
        Catch sqlEx As SqlException
            MessageBox.Show("Ha ocurrido una excepción:" + sqlEx.Message + " Consulta Servidor")
        Catch AnyEx As Exception
            MessageBox.Show("Ha ocurrido una excepción general:" + AnyEx.Message + "al realizar una consulta al servidor.")
        Finally
            comandoBD_SERVIDOR.Parameters.Clear()
        End Try

        Return DataSet_Servidor

    End Function

    Function ejecutaQuery_Servidor(ByVal comandoBD_SERVIDOR As SqlCommand, ByVal nombreTabla As String) As Data.DataSet

        Dim Adapter_Servidor As New SqlDataAdapter
        Dim DataSet_Servidor As New System.Data.DataSet

        Try
            comandoBD_SERVIDOR.Connection = conexionBD_SERVIDOR
            'el metodo SelectCommand de un dataAdapter contiene los datos o información del origen de datos
            Adapter_Servidor.SelectCommand = (comandoBD_SERVIDOR)
            Adapter_Servidor.SelectCommand.Transaction = sqlTran
            Adapter_Servidor.Fill(DataSet_Servidor, nombreTabla)
            Adapter_Servidor.Dispose()
        Catch sqlEx As SqlException
            MessageBox.Show("Ha ocurrido una excepción:" + sqlEx.Message + " al realizar una consulta al servidor.")
        Catch InvOpEx As InvalidOperationException
            MessageBox.Show("Ha ocurrido una excepción:" + InvOpEx.Message + " al realizar una consulta al servidor.")
        Catch AnyEx As Exception
            MessageBox.Show("Ha ocurrido una excepción general:" + AnyEx.Message + "al realizar una consulta al servidor.")
        Finally
            comandoBD_SERVIDOR.Parameters.Clear()
        End Try

        Return DataSet_Servidor

    End Function

    Function returnInteger_SERVIDOR(ByVal sentenciaSQL As String) As Integer

        Try
            comandBD_SERVIDOR.CommandText = sentenciaSQL
            comandBD_SERVIDOR.Connection = conexionBD_SERVIDOR
        Catch sqlEx As SqlException
            MessageBox.Show("Ha ocurrido una excepción SQL:" + sqlEx.Message + "al realizar una consulta al servidor.")
        Catch AnyEx As Exception
            MessageBox.Show("Ha ocurrido una excepción general:" + AnyEx.Message + "al realizar una consulta al servidor.")
        End Try
        Return comandBD_SERVIDOR.ExecuteScalar

    End Function

    Function returnString_SERVIDOR(ByVal sentenciaSQL As String) As String

        Dim ResultadoConsultaServidor As String

        Try
            comandBD_SERVIDOR.CommandText = sentenciaSQL
            comandBD_SERVIDOR.Connection = conexionBD_SERVIDOR
        Catch sqlEx As SqlException
            MessageBox.Show("Ha ocurrido una excepción:" + sqlEx.Message + "en una consulta Especifica")
        Catch AnyEx As Exception
            MessageBox.Show("Ha ocurrido una excepción general:" + AnyEx.Message + "al realizar una consulta al servidor.")
        End Try

        If conexionBD_SERVIDOR.State = ConnectionState.Closed Then
            If conectarDB_SERVIDOR(rutaDB_SERVIDOR) = 1 Then
                ResultadoConsultaServidor = comandBD_SERVIDOR.ExecuteScalar()
                cerrarConexion_SERVIDOR()
                Return ResultadoConsultaServidor

            Else
                MsgBox("No hay conexión con el servidor", MsgBoxStyle.Exclamation, "Consulta Servidor")
                Return 0
            End If
        Else
            ResultadoConsultaServidor = comandBD_SERVIDOR.ExecuteScalar()
            Return ResultadoConsultaServidor
        End If

    End Function

    Sub AccionesTablaTemporal(ByVal sentenciaSQL As String)

        Try
            comandBD_SERVIDOR.CommandText = sentenciaSQL
            comandBD_SERVIDOR.Connection = conexionBD_SERVIDOR
            comandBD_SERVIDOR.ExecuteNonQuery()
        Catch sqlEx As SqlException
            MessageBox.Show("Ha ocurrido una excepción:" + sqlEx.Message + "en una Accion de Tabla Temporal")
        Finally
            comandBD_SERVIDOR.Dispose()
        End Try

    End Sub

    Function obtenerRegistrosMA_RECEPCIONESMOVIL_SERVIDOR(ByVal numeroRecepcion_PDA)

        Dim nombreTabla As String = "MA_RECEPCIONESMOVIL"

        comandBD_SERVIDOR.CommandText = "SELECT * FROM MA_RECEPCIONESMOVIL WHERE numeroRecepcion_PDA= @numeroRecepcion_PDA AND codigoProveedor = @codigoProveedor AND  codigoProducto = @codigoProducto"
        comandBD_SERVIDOR.Parameters.Add("@numeroRecepcion_PDA", numeroRecepcion_PDA) 'Porque es el numero de factura que tiene el id del PDA
        comandBD_SERVIDOR.Parameters.Add("@codigoProveedor", FormProveedorDeposito_Recepcion.txtProveedor.Text)
        comandBD_SERVIDOR.Parameters.Add("@codigoProducto", FormLecturaProducto_Recepcion.txtCodigoProducto.Text)
        dataSet_Servidor = ejecutaQuery_Servidor(comandBD_SERVIDOR, nombreTabla)
        comandBD_SERVIDOR.Parameters.Clear()
        Return dataSet_Servidor

    End Function

    Sub actualizarMA_RECEPCIONESMOVIL_SERVIDOR(ByVal cantidadTotalUnidadesRecibidas, ByVal cantidadTotalEmpaquesRecibidos, ByVal cantidadUnidadesRecibidasAnterior, ByVal cantidadEmpaquesRecibidosAnterior, ByVal numeroRecepcion_PDA)

        sqlTran = conexionBD_SERVIDOR.BeginTransaction()
        Try
            comandBD_SERVIDOR.CommandText = "UPDATE MA_RECEPCIONESMOVIL SET cantidadUnidades = @cantidadUnidades, cantidadEmpaques = @cantidadEmpaques WHERE codigoProveedor = @codigoProveedor AND numeroRecepcion_PDA = @numeroRecepcion_PDA AND codigoProducto = @codigoProducto AND cantidadUnidades = @cantidadUnidadesRecibidasAnterior AND cantidadEmpaques = @cantidadEmpaquesRecibidosAnterior"
            comandBD_SERVIDOR.Transaction = sqlTran
            comandBD_SERVIDOR.Parameters.Add("@cantidadUnidades", cantidadTotalUnidadesRecibidas)
            comandBD_SERVIDOR.Parameters.Add("@cantidadEmpaques", cantidadTotalEmpaquesRecibidos)
            comandBD_SERVIDOR.Parameters.Add("@codigoProveedor", FormProveedorDeposito_Recepcion.txtProveedor.Text)
            comandBD_SERVIDOR.Parameters.Add("@codigoProducto", FormLecturaProducto_Recepcion.txtCodigoProducto.Text)
            comandBD_SERVIDOR.Parameters.Add("@cantidadUnidadesRecibidasAnterior", cantidadUnidadesRecibidasAnterior)
            comandBD_SERVIDOR.Parameters.Add("@cantidadEmpaquesRecibidosAnterior", cantidadEmpaquesRecibidosAnterior)
            comandBD_SERVIDOR.Parameters.Add("@numeroRecepcion_PDA", numeroRecepcion_PDA)
            comandBD_SERVIDOR.ExecuteNonQuery()
            comandBD_SERVIDOR.Parameters.Clear()
            sqlTran.Commit()

        Catch sqlEx As SqlException
            MessageBox.Show("Ha ocurrido una excepción:" + sqlEx.Message + ". Al actualizar informacion en  MA_RECEPCIONESMOVIL")
            sqlTran.Rollback()
        End Try

    End Sub

    Sub insertarRegistrosMA_RECEPCIONESMOVIL_SERVIDOR(ByVal numeroRecepcion_PDA)

        sqlTran = conexionBD_SERVIDOR.BeginTransaction()

        Try
            comandBD_SERVIDOR.CommandText = "INSERT INTO MA_RECEPCIONESMOVIL (codigoProveedor, codigoODC, codigoProducto, cantidadUnidades, cantidadEmpaques, facturaUnidades, facturaEmpaques, transportista, codigoLocalidad, codigoDeposito, documento, numeroRecepcion_PDA, UnidadesxEmpaque, numeroFactura) VALUES (@codigoProveedor, @codigoODC, @codigoProducto, @cantidadUnidades, @cantidadEmpaques, @facturaUnidades, @facturaEmpaques, @transportista, @codigoLocalidad, @codigoDeposito, @documento, @numeroRecepcion_PDA, @UnidadesxEmpaque, @NumeroFactura)"
            comandBD_SERVIDOR.Transaction = sqlTran
            comandBD_SERVIDOR.Parameters.Add("@codigoProveedor", FormProveedorDeposito_Recepcion.txtProveedor.Text)
            comandBD_SERVIDOR.Parameters.Add("@codigoODC", codigoODC)
            comandBD_SERVIDOR.Parameters.Add("@codigoProducto", FormLecturaProducto_Recepcion.txtCodigoProducto.Text)
            comandBD_SERVIDOR.Parameters.Add("@cantidadUnidades", FormGuardarDatos_Recepcion.lblCantidadUnidadesRecibidas.Text)
            comandBD_SERVIDOR.Parameters.Add("@cantidadEmpaques", FormGuardarDatos_Recepcion.lblCantidadEmpaquesRecibidos.Text)
            comandBD_SERVIDOR.Parameters.Add("@facturaUnidades", FormGuardarDatos_Recepcion.lblCantidadUnidadesFacturadas.Text)
            comandBD_SERVIDOR.Parameters.Add("@facturaEmpaques", FormGuardarDatos_Recepcion.lblCantidadEmpaquesFacturados.Text)
            comandBD_SERVIDOR.Parameters.Add("@transportista", FormDocumentoTransportista_Recepcion.txtTransportista.Text)
            comandBD_SERVIDOR.Parameters.Add("@codigoLocalidad", Localidad)
            comandBD_SERVIDOR.Parameters.Add("@codigoDeposito", FormProveedorDeposito_Recepcion.txtDeposito.Text)
            comandBD_SERVIDOR.Parameters.Add("@documento", FormDocumentoTransportista_Recepcion.txtDocumento.Text)
            comandBD_SERVIDOR.Parameters.Add("@numeroRecepcion_PDA", numeroRecepcion_PDA)
            comandBD_SERVIDOR.Parameters.Add("@UnidadesxEmpaque", UnidadesxEmpaque)
            comandBD_SERVIDOR.Parameters.Add("@NumeroFactura", FormDocumentoTransportista_Recepcion.txtDocumento.Text)

            comandBD_SERVIDOR.ExecuteNonQuery()
            comandBD_SERVIDOR.Parameters.Clear()
            sqlTran.Commit()

        Catch sqlEx As SqlException
            MessageBox.Show("Ha ocurrido una excepción:" + sqlEx.Message + ". Al insertar informacion en  MA_RECEPCIONESMOVIL")
            sqlTran.Rollback()
        End Try

    End Sub

    Function construirNumeroRecepcion_PDA()

        Dim numeroRecepcion_PDA As String
        Dim numUltimaRecepcionProcesada As String

        If CompatibilidadFueraDeLinea Then

            comandBD_PDA.Parameters.Clear()
            comandBD_PDA.CommandText = "SELECT numUltimaRecepcion FROM MA_CONFIGURACION"
            dataSet_PDA = consultaQuery_PDA(comandBD_PDA)

            numUltimaRecepcionProcesada = Val(dataSet_PDA.Tables(0).Rows(0)("numUltimaRecepcion")) + 1

            'Construyendo el número de documento para el servidor que será usado en la observacion de MA_INVENTARIO
            'SentenciaSQL = "SELECT RIGHT('000000" + numUltimaRecepcionProcesada + "',6)"
            'numeroRecepcion_PDA = Id_PDA + returnString_SERVIDOR(SentenciaSQL)

        Else

            comandBD_SERVIDOR.Parameters.Clear()
            comandBD_SERVIDOR.CommandText = "SELECT isNULL(MAX(SUBSTRING(c_Documento, (" & _
            Len(Id_PDA).ToString & " + 1), 9999)), '1') AS NumUltimaRecepcion " + _
            "FROM MA_INVENTARIO WHERE c_Concepto = 'REC' " & _
            "AND LEFT(c_Documento, " & Len(Id_PDA).ToString & ") = @ID_PDA " & _
            "AND c_CodLocalidad = @Loc "

            comandBD_SERVIDOR.Parameters.Add("@ID_PDA", Id_PDA)
            comandBD_SERVIDOR.Parameters.Add("@Loc", Localidad)

            dataSet_PDA = executeQuery_Servidor(comandBD_SERVIDOR)

            numUltimaRecepcionProcesada = Val(dataSet_PDA.Tables(0).Rows(0)("NumUltimaRecepcion")) + 1

        End If

        numeroRecepcion_PDA = Id_PDA + Format(numUltimaRecepcionProcesada, "000000")

        Return numeroRecepcion_PDA

    End Function

    Sub cerrarConexion_SERVIDOR()
        If (conexionBD_SERVIDOR.State = ConnectionState.Open) Then conexionBD_SERVIDOR.Close()
    End Sub

    Sub cerrarConexion_SERVIDORPOS()
        If (conexionBD_SERVIDORPOS.State = ConnectionState.Open) Then conexionBD_SERVIDORPOS.Close()
    End Sub

    Sub cerrarConexionSQL(ByVal pConexion As SqlConnection)
        If (pConexion.State = ConnectionState.Open) Then pConexion.Close()
    End Sub

    '******************************** T R A S L A D O S **************************************************************
    Function buscarConceptos(ByVal Proceso As String)

        Dim nombreTabla As String = "MA_CONCEPTOS"

        comandBD_SERVIDOR.CommandText = " SELECT MA_CONCEPTOS.IDCONCEPTO, MA_CONCEPTOS.DESCRIPCION " + _
        "FROM MA_CONCEPTOS INNER JOIN MA_PROCESOS " + _
        "ON MA_CONCEPTOS.IDPROCESO = MA_PROCESOS.IDProceso " + _
        "WHERE (MA_PROCESOS.Proceso = @Proceso)"

        comandBD_SERVIDOR.Parameters.Add("@Proceso", Proceso)
        dataSet_Servidor = ejecutaQuery_Servidor(comandBD_SERVIDOR, nombreTabla)

        Return dataSet_Servidor

    End Function

    Function buscarConceptos_PDA(ByVal Proceso As String)

        comandoBD_PDA.CommandText = " SELECT MA_CONCEPTOS.IDCONCEPTO, MA_CONCEPTOS.DESCRIPCION FROM MA_CONCEPTOS INNER JOIN MA_PROCESOS ON MA_CONCEPTOS.IDPROCESO = MA_PROCESOS.IDProceso WHERE (MA_PROCESOS.Proceso = @Proceso)"
        comandoBD_PDA.Parameters.Add("@Proceso", Proceso)
        dataSet_PDA = consultaQuery_PDA(comandoBD_PDA)

        Return dataSet_PDA

    End Function

    Public Function NPrecio_Contado(Optional ByVal pPrecioBarra As Integer = 0) As String
        Try
            Dim PrecioContado As Integer = CInt(returnString_SERVIDOR("SELECT N_PRECIO FROM MA_CLIENTES WHERE C_CODCLIENTE = '9999999999'"))

            Dim miPrecio As Integer

            miPrecio = IIf(pPrecioBarra > 0 And pPrecioBarra <= 3, pPrecioBarra, PrecioContado)

            Select Case miPrecio
                Case 1
                    NPrecio_Contado = "N_PRECIO1"
                Case 2
                    NPrecio_Contado = "N_PRECIO2"
                Case 3
                    NPrecio_Contado = "N_PRECIO3"
                Case Else
                    NPrecio_Contado = "N_PRECIO1"
            End Select

        Catch EX As Exception
            NPrecio_Contado = "N_PRECIO1"
        End Try
    End Function

    Function agregarProductoTR_INVENTARIO_SERVIDOR(ByVal cantidadTraslado, ByVal existenciaProductoDepositoOrigen)

        'Dim adapter_TR_INVENTARIO As New SqlDataAdapter
        'Dim adapter_MA_CORRELATIVOS As New SqlDataAdapter
        'Dim adapter_MA_DEPOPROD As New SqlDataAdapter
        'Dim dataset_MA_DEPOPROD_ORIGEN, dataset_MA_DEPOPROD_DESTINO As New Data.DataSet
        Dim n_costo, costo, subtotal, total As Double

        'sentenciaSQL = "SELECT ESTIMACION_INV FROM MA_CONFIGURACION"
        'n_costo = returnInteger_PDA(sentenciaSQL)
        SentenciaSQL = "SELECT ESTIMACION_INV FROM REGLAS_COMERCIALES"
        n_costo = returnInteger_SERVIDOR(SentenciaSQL)

        dataset_Productos = detallesProducto_Servidor(FormLecturaProducto_Traslado.txtCodigoProducto.Text)

        If Not dataset_Productos.Tables(0).Rows.Count = 0 Then
            If n_costo = 0 Then
                costo = CDbl(dataset_Productos.Tables(0).Rows(0)("n_costoact"))
            ElseIf n_costo = 1 Then
                costo = CDbl(dataset_Productos.Tables(0).Rows(0)("n_costoant"))
            ElseIf n_costo = 2 Then
                costo = CDbl(dataset_Productos.Tables(0).Rows(0)("n_costopro"))
            Else
                costo = CDbl(dataset_Productos.Tables(0).Rows(0)("n_costoact"))
            End If
        Else
            Return 0
            MsgBox("No se puede consultar la información del producto, no se agrego el traslado.", MsgBoxStyle.Information, "isMOBILE")
            Exit Function
        End If

        subtotal = CDbl(cantidadTraslado) * costo
        total = subtotal

        'If conexionBD_SERVIDOR.State = ConnectionState.Open Then

        'Actualizo los correlativos antes de que cree el numero del traslado del servidor
        'Esto lo hago para que en caso de que se guarde un traslado pendiente, el correlativo a usar sea el correcto

        'numeroTraslado_SERVIDORsinFormato = determinarNumeroTraslado_SERVIDOR()
        'numeroTraslado_SERVIDOR = Format(numeroTraslado_SERVIDORsinFormato, "00000000#")

        'comandoBD_PDA.CommandText = "DELETE MA_CORRELATIVOS"
        'ejecutaQuery_PDA(comandoBD_PDA)
        'actualizarMA_CORRELATIVOS()

        'dataset_MA_DEPOPROD_ORIGEN = consultarProducto_MA_DEPOPROD(FormLecturaProducto_Traslado.txtCodigoProducto.Text, FormDepositoOrigenDestino_Traslado.txtDepositoOrigen.Text)
        'dataset_MA_DEPOPROD_DESTINO = consultarProducto_MA_DEPOPROD(FormLecturaProducto_Traslado.txtCodigoProducto.Text, FormDepositoOrigenDestino_Traslado.txtDepositoDestino.Text)

        'If conexionBD_SERVIDOR.State = ConnectionState.Closed Then 'Prueba

        'sqlTran = conexionBD_SERVIDOR.BeginTransaction()

        'Try

        'Crea el detalle del inventario como descargo del deposito origen
        'adapter_TR_INVENTARIO.InsertCommand = New SqlCommand("INSERT INTO TR_INVENTARIO VALUES(@c_linea, 'TRS', @c_documento, @c_deposito, @c_codarticulo, @n_cantidad, @n_costo, @n_subtotal, 0, @n_total, 'Descargo', 0, 0, 0, 0, SYSDATETIME (), @c_codlocalidad, 1, '', '', 0, 0, 0, '', '', 0, 0, '', @cs_codlocalidad, @ns_cantidadEmpaque, 0)", conexionBD_SERVIDOR)
        'adapter_TR_INVENTARIO.InsertCommand.Transaction = sqlTran

        'adapter_TR_INVENTARIO.InsertCommand.Parameters.Add("@c_linea", lineaTraslado)
        'adapter_TR_INVENTARIO.InsertCommand.Parameters.Add("@c_documento", numeroTraslado_SERVIDOR)
        'adapter_TR_INVENTARIO.InsertCommand.Parameters.Add("@c_deposito", FormDepositoOrigenDestino_Traslado.txtDepositoOrigen.Text)
        'adapter_TR_INVENTARIO.InsertCommand.Parameters.Add("@c_codarticulo", FormLecturaProducto_Traslado.txtCodigoProducto.Text)
        'adapter_TR_INVENTARIO.InsertCommand.Parameters.Add("@n_cantidad", CDbl(cantidadTraslado))
        'adapter_TR_INVENTARIO.InsertCommand.Parameters.Add("@n_costo", costo)
        'adapter_TR_INVENTARIO.InsertCommand.Parameters.Add("@n_subtotal", subtotal)
        'adapter_TR_INVENTARIO.InsertCommand.Parameters.Add("@n_total", total)
        'adapter_TR_INVENTARIO.InsertCommand.Parameters.Add("@c_codlocalidad", localidad)
        'adapter_TR_INVENTARIO.InsertCommand.Parameters.Add("@c_descripcion", CStr(dataset_Productos.Tables(0).Rows(0)("c_descri")))
        'adapter_TR_INVENTARIO.InsertCommand.Parameters.Add("@cs_codlocalidad", localidad)
        'adapter_TR_INVENTARIO.InsertCommand.Parameters.Add("@ns_cantidadEmpaque", UnidadesxEmpaque)
        'adapter_TR_INVENTARIO.InsertCommand.ExecuteNonQuery()
        'adapter_TR_INVENTARIO.InsertCommand.Parameters.Clear()

        ''Busca el producto en el deposito origen

        'If dataset_MA_DEPOPROD_ORIGEN.Tables(0).Rows.Count = 0 Then 'No existe el producto en la tabla, entonces lo crea con existencia negativa!

        '    adapter_MA_DEPOPROD.UpdateCommand = New SqlCommand("INSERT INTO MA_DEPOPROD VALUES (@codigodeposito, @codigoProducto, @descripcion, @cantidad_MA_DEPOPROD, 0, 0)", conexionBD_SERVIDOR)
        '    adapter_MA_DEPOPROD.UpdateCommand.Transaction = sqlTran

        '    adapter_MA_DEPOPROD.UpdateCommand.Parameters.Add("@codigoDeposito", FormDepositoOrigenDestino_Traslado.txtDepositoOrigen.Text)
        '    adapter_MA_DEPOPROD.UpdateCommand.Parameters.Add("@codigoProducto", FormLecturaProducto_Traslado.txtCodigoProducto.Text)
        '    adapter_MA_DEPOPROD.UpdateCommand.Parameters.Add("@descripcion", CStr(dataset_Productos.Tables(0).Rows(0)("c_descri")))
        '    adapter_MA_DEPOPROD.UpdateCommand.Parameters.Add("@cantidad_MA_DEPOPROD", existenciaProductoDepositoOrigen) 'Ya está negativo en caso de que entre en esta parte del código
        '    adapter_MA_DEPOPROD.UpdateCommand.ExecuteNonQuery()
        '    adapter_MA_DEPOPROD.UpdateCommand.Parameters.Clear()

        'Else 'Si existe el producto en la tabla
        '    adapter_MA_DEPOPROD.InsertCommand = New SqlCommand("UPDATE MA_DEPOPROD SET n_cantidad = @cantidad_MA_DEPOPROD WHERE c_coddeposito = @codigodeposito AND c_codarticulo = @codigoProducto", conexionBD_SERVIDOR)
        '    adapter_MA_DEPOPROD.InsertCommand.Transaction = sqlTran

        '    adapter_MA_DEPOPROD.InsertCommand.Parameters.Add("@cantidad_MA_DEPOPROD", existenciaProductoDepositoOrigen) 'Ya toma en cuenta la cantidad que estaba anteriormente en la tabla
        '    adapter_MA_DEPOPROD.InsertCommand.Parameters.Add("@codigoDeposito", FormDepositoOrigenDestino_Traslado.txtDepositoOrigen.Text)
        '    adapter_MA_DEPOPROD.InsertCommand.Parameters.Add("@codigoProducto", FormLecturaProducto_Traslado.txtCodigoProducto.Text)
        '    adapter_MA_DEPOPROD.InsertCommand.ExecuteNonQuery()
        '    adapter_MA_DEPOPROD.InsertCommand.Parameters.Clear()

        'End If

        ''Crea el detalle del inventario como cargo del deposito destino

        'adapter_TR_INVENTARIO.InsertCommand = New SqlCommand("INSERT INTO TR_INVENTARIO VALUES(@c_linea, 'TRS', @c_documento, @c_deposito, @c_codarticulo, @n_cantidad, @n_costo, @n_subtotal, 0, @n_total, 'Cargo', 0, 0, 0, 0, SYSDATETIME (), @c_codlocalidad, 1, '', '', 0, 0, 0, '', '', 0, 0, '', @cs_codlocalidad, @ns_cantidadEmpaque, 0)", conexionBD_SERVIDOR)
        'adapter_TR_INVENTARIO.InsertCommand.Transaction = sqlTran
        'adapter_TR_INVENTARIO.InsertCommand.Parameters.Add("@c_linea", lineaTraslado)
        'adapter_TR_INVENTARIO.InsertCommand.Parameters.Add("@c_documento", numeroTraslado_SERVIDOR)
        'adapter_TR_INVENTARIO.InsertCommand.Parameters.Add("@c_deposito", FormDepositoOrigenDestino_Traslado.txtDepositoDestino.Text)
        'adapter_TR_INVENTARIO.InsertCommand.Parameters.Add("@c_codarticulo", FormLecturaProducto_Traslado.txtCodigoProducto.Text)
        'adapter_TR_INVENTARIO.InsertCommand.Parameters.Add("@n_cantidad", CDbl(cantidadTraslado))
        'adapter_TR_INVENTARIO.InsertCommand.Parameters.Add("@n_costo", costo)
        'adapter_TR_INVENTARIO.InsertCommand.Parameters.Add("@n_subtotal", subtotal)
        'adapter_TR_INVENTARIO.InsertCommand.Parameters.Add("@n_total", total)
        'adapter_TR_INVENTARIO.InsertCommand.Parameters.Add("@c_codlocalidad", localidad)
        'adapter_TR_INVENTARIO.InsertCommand.Parameters.Add("@c_descripcion", CStr(dataset_Productos.Tables(0).Rows(0)("c_descri")))
        'adapter_TR_INVENTARIO.InsertCommand.Parameters.Add("@cs_codlocalidad", localidad)
        'adapter_TR_INVENTARIO.InsertCommand.Parameters.Add("@ns_cantidadEmpaque", UnidadesxEmpaque)
        'adapter_TR_INVENTARIO.InsertCommand.ExecuteNonQuery()
        'adapter_TR_INVENTARIO.InsertCommand.Parameters.Clear()

        ''Busca el producto en el deposito destino

        'If dataset_MA_DEPOPROD_DESTINO.Tables(0).Rows.Count = 0 Then 'No existe el producto en la tabla, entonces lo crea con existencia negativa!

        '    adapter_MA_DEPOPROD.UpdateCommand = New SqlCommand("INSERT INTO MA_DEPOPROD VALUES (@codigodeposito, @codigoProducto, @descripcion, @cantidad_MA_DEPOPROD, 0 , 0)", conexionBD_SERVIDOR)
        '    adapter_MA_DEPOPROD.UpdateCommand.Transaction = sqlTran

        '    adapter_MA_DEPOPROD.UpdateCommand.Parameters.Add("@codigoDeposito", FormDepositoOrigenDestino_Traslado.txtDepositoDestino.Text)
        '    adapter_MA_DEPOPROD.UpdateCommand.Parameters.Add("@codigoProducto", FormLecturaProducto_Traslado.txtCodigoProducto.Text)
        '    adapter_MA_DEPOPROD.UpdateCommand.Parameters.Add("@descripcion", CStr(dataset_Productos.Tables(0).Rows(0)("c_descri")))
        '    adapter_MA_DEPOPROD.UpdateCommand.Parameters.Add("@cantidad_MA_DEPOPROD", CDbl(cantidadTraslado)) 'Positivo porque es el depósito destino
        '    adapter_MA_DEPOPROD.UpdateCommand.ExecuteNonQuery()
        '    adapter_MA_DEPOPROD.UpdateCommand.Parameters.Clear()

        'Else 'Si existe el producto en el deposito destino

        '    Dim cantidadDepositoDestino As Double = CDbl(dataset_MA_DEPOPROD_DESTINO.Tables(0).Rows(0)("n_cantidad")) + CDbl(cantidadTraslado)

        '    adapter_MA_DEPOPROD.InsertCommand = New SqlCommand("UPDATE MA_DEPOPROD SET n_cantidad = @cantidad_MA_DEPOPROD WHERE c_coddeposito = @codigodeposito AND c_codarticulo = @codigoProducto", conexionBD_SERVIDOR)
        '    adapter_MA_DEPOPROD.InsertCommand.Transaction = sqlTran
        '    adapter_MA_DEPOPROD.InsertCommand.Parameters.Add("@cantidad_MA_DEPOPROD", cantidadDepositoDestino)
        '    adapter_MA_DEPOPROD.InsertCommand.Parameters.Add("@codigoDeposito", FormDepositoOrigenDestino_Traslado.txtDepositoDestino.Text)
        '    adapter_MA_DEPOPROD.InsertCommand.Parameters.Add("@codigoProducto", FormLecturaProducto_Traslado.txtCodigoProducto.Text)
        '    adapter_MA_DEPOPROD.InsertCommand.ExecuteNonQuery()
        '    adapter_MA_DEPOPROD.InsertCommand.Parameters.Clear()
        'End If
        'sqlTran.Commit()

        'MsgBox("El traslado de este producto ha sido guardado en la base de datos del servidor", MsgBoxStyle.Exclamation, "isMOBILE")

        'limpiarFormLecturaProducto_Traslado()

        'Return 1

        'Catch sqlEx As SqlException
        '    MessageBox.Show("Ha ocurrido una excepción:" + sqlEx.Message + " Al agregar producto al inventario")
        '    sqlTran.Rollback()

        '    MsgBox("No se guardó el traslado de este producto", MsgBoxStyle.Exclamation, "Error al agregar el traslado del producto")

        '    limpiarFormLecturaProducto_Traslado()

        '    Return 0
        'End Try
        'Else 'No hay conexión con el servidor

        'Guardo el producto en la base de datos local para el desposito origen

        'Dim Fecha As String = Replace(Format(DateTime.Now, "dd/MM/yyyy HH:MM:ss"), ".", "") 'Replace(DateTime.Now.ToString, ".", "")

        comandoBD_PDA.CommandText = "INSERT INTO TR_TRASLADOS_PDA VALUES(@c_linea, 'TRS', @c_documento, @c_deposito, @c_codarticulo, @n_cantidad, @n_costo, @n_subtotal, @n_total, 'Descargo', getdate(), @c_codlocalidad, @c_descripcion, @cs_codlocalidad, @ns_cantidadEmpaque)"
        comandoBD_PDA.Parameters.Add("@c_linea", lineaTraslado)
        comandoBD_PDA.Parameters.Add("@c_documento", numeroTraslado_SERVIDOR)
        comandoBD_PDA.Parameters.Add("@c_deposito", FormDepositoOrigenDestino_Traslado.txtDepositoOrigen.Text)
        comandoBD_PDA.Parameters.Add("@c_codarticulo", FormLecturaProducto_Traslado.txtCodigoProducto.Text)
        comandoBD_PDA.Parameters.Add("@n_cantidad", CDbl(cantidadTraslado))
        comandoBD_PDA.Parameters.Add("@n_costo", costo)
        comandoBD_PDA.Parameters.Add("@n_subtotal", subtotal)
        comandoBD_PDA.Parameters.Add("@n_total", total)
        'comandoBD_PDA.Parameters.Add("@fecha", Fecha)
        comandoBD_PDA.Parameters.Add("@c_codlocalidad", Localidad)
        comandoBD_PDA.Parameters.Add("@c_descripcion", CStr(dataset_Productos.Tables(0).Rows(0)("c_descri")))
        comandoBD_PDA.Parameters.Add("@cs_codlocalidad", Localidad)
        comandoBD_PDA.Parameters.Add("@ns_CantidadEmpaque", UnidadesxEmpaque)
        ejecutaQuery_PDA(comandoBD_PDA)
        comandBD_PDA.Parameters.Clear()

        'Guardo el producto en la base de datos local para el deposito destino

        comandoBD_PDA.CommandText = "INSERT INTO TR_TRASLADOS_PDA VALUES(@c_linea, 'TRS', @c_documento, @c_deposito, @c_codarticulo, @n_cantidad, @n_costo, @n_subtotal, @n_total, 'Cargo', getdate(), @c_codlocalidad, @c_descripcion, @cs_codlocalidad, @ns_cantidadEmpaque)"
        comandoBD_PDA.Parameters.Add("@c_linea", lineaTraslado)
        comandoBD_PDA.Parameters.Add("@c_documento", numeroTraslado_SERVIDOR)
        comandoBD_PDA.Parameters.Add("@c_deposito", FormDepositoOrigenDestino_Traslado.txtDepositoDestino.Text)
        comandoBD_PDA.Parameters.Add("@c_codarticulo", FormLecturaProducto_Traslado.txtCodigoProducto.Text)
        comandoBD_PDA.Parameters.Add("@n_cantidad", CDbl(cantidadTraslado))
        comandoBD_PDA.Parameters.Add("@n_costo", costo)
        comandoBD_PDA.Parameters.Add("@n_subtotal", subtotal)
        comandoBD_PDA.Parameters.Add("@n_total", total)
        'comandoBD_PDA.Parameters.Add("@fecha", Fecha)
        comandoBD_PDA.Parameters.Add("@c_codlocalidad", Localidad)
        comandoBD_PDA.Parameters.Add("@c_descripcion", CStr(dataset_Productos.Tables(0).Rows(0)("c_descri")))
        comandoBD_PDA.Parameters.Add("@cs_codlocalidad", Localidad)
        comandoBD_PDA.Parameters.Add("@ns_CantidadEmpaque", UnidadesxEmpaque)
        ejecutaQuery_PDA(comandoBD_PDA)
        comandBD_PDA.Parameters.Clear()

        MsgBox("El traslado de este producto ha sido guardado en la base de datos local temporalmente.", MsgBoxStyle.Exclamation, "isMOBILE")

        limpiarFormLecturaProducto_Traslado()

        Return 1

        'End If

    End Function


    Function finalizarTraslado() As Boolean

        Dim adapter_MA_INVENTARIO As New SqlDataAdapter
        Dim adapter_MA_CORRELATIVOS As New SqlDataAdapter

        Dim codigoMoneda As String

        SentenciaSQL = "SELECT c_CodMoneda FROM MA_MONEDAS WHERE b_Preferencia = 1"
        If CompatibilidadFueraDeLinea Then
            codigoMoneda = returnString_PDA(SentenciaSQL)
        Else
            codigoMoneda = returnString_SERVIDOR(SentenciaSQL)
        End If

        SentenciaSQL = "SELECT n_Factor FROM MA_MONEDAS WHERE b_Preferencia = 1"
        If CompatibilidadFueraDeLinea Then
            factorCambio = returnInteger_PDA(SentenciaSQL)
        Else
            factorCambio = returnString_SERVIDOR(SentenciaSQL)
        End If

        numeroTraslado_SERVIDORsinFormato = determinarNumeroTraslado_SERVIDOR()

        If codigoMoneda = String.Empty _
        Or factorCambio = 0 _
        Or numeroTraslado_SERVIDORsinFormato = 0 Then
            MsgBox("No hay moneda predeterminada o no se pudo recuperar el correlativo. " + _
            "Intente de nuevo mas tarde", MsgBoxStyle.Information, "isMOBILE")
            Exit Function
        End If

        numeroTraslado_SERVIDOR = Format(numeroTraslado_SERVIDORsinFormato, "00000000#")

        sqlTran = conexionBD_SERVIDOR.BeginTransaction()

        Try

            adapter_MA_INVENTARIO.InsertCommand = New SqlCommand( _
            "INSERT INTO MA_INVENTARIO (C_CONCEPTO, C_DOCUMENTO, D_FECHA, C_DESCRIPCION, C_STATUS, " _
            & "C_CODPROVEEDOR, C_CODLOCALIDAD, C_CODMONEDA, N_FACTORCAMBIO, N_DESCUENTO, C_OBSERVACION, C_RELACION, C_CODCOMPRADOR, C_DEP_ORIG, C_DEP_DEST, " _
            & "C_MOTIVO, C_CODTRANSPORTE, C_EJECUTOR, C_FACTURA, C_TRANSPORTISTA, N_CANTIDAD_COMPRA, CODCONCEPTO, CS_COMPROBANTECONTABLE, DS_HORAINICIO, " _
            & "DS_HORAFINAL, CS_CODLOCALIDAD, CS_CODUNIDAD) VALUES ('TRS', @C_DOCUMENTO, GETDATE(), '', 'DCO', '', @C_CODLOCALIDAD, @C_CODMONEDA, " _
            & "@N_FACTORCAMBIO, 0, @C_OBSERVACION, '', @C_CODCOMPRADOR, @C_DEP_ORIG, @C_DEP_DEST, @C_MOTIVO, '', @C_EJECUTOR, '', '', 0, @CODCONCEPTO, " _
            & "'', GETDATE (), GETDATE (), '', '')", conexionBD_SERVIDOR)

            adapter_MA_INVENTARIO.InsertCommand.Transaction = sqlTran
            adapter_MA_INVENTARIO.InsertCommand.Parameters.Add("@C_DOCUMENTO", CStr(numeroTraslado_SERVIDOR))
            adapter_MA_INVENTARIO.InsertCommand.Parameters.Add("@C_CODLOCALIDAD", Localidad)
            adapter_MA_INVENTARIO.InsertCommand.Parameters.Add("@C_CODMONEDA", CStr(codigoMoneda))
            adapter_MA_INVENTARIO.InsertCommand.Parameters.Add("@N_FACTORCAMBIO", factorCambio)
            adapter_MA_INVENTARIO.InsertCommand.Parameters.Add("@C_OBSERVACION", CStr(FormObservacion_Traslado.txtObservacion.Text))
            adapter_MA_INVENTARIO.InsertCommand.Parameters.Add("@C_CODCOMPRADOR", CStr(datasetUsuario.Tables(0).Rows(0)("codusuario")))
            adapter_MA_INVENTARIO.InsertCommand.Parameters.Add("@C_DEP_ORIG", CStr(FormDepositoOrigenDestino_Traslado.txtDepositoOrigen.Text))
            adapter_MA_INVENTARIO.InsertCommand.Parameters.Add("@C_DEP_DEST", CStr(FormDepositoOrigenDestino_Traslado.txtDepositoDestino.Text))
            adapter_MA_INVENTARIO.InsertCommand.Parameters.Add("@C_MOTIVO", CStr(FormEjecutorMotivo_Traslado.cboConceptosTraslados.Text))
            adapter_MA_INVENTARIO.InsertCommand.Parameters.Add("@C_EJECUTOR", CStr(FormEjecutorMotivo_Traslado.txtEjecutor.Text))
            adapter_MA_INVENTARIO.InsertCommand.Parameters.Add("@CODCONCEPTO", CDbl(FormEjecutorMotivo_Traslado.cboConceptosTraslados.SelectedValue))
            adapter_MA_INVENTARIO.InsertCommand.ExecuteNonQuery()
            adapter_MA_INVENTARIO.InsertCommand.Parameters.Clear()

            enviarTrasladosPendientes()

            'Actualizo el correlativo de traslados
            adapter_MA_INVENTARIO.InsertCommand = New SqlCommand("UPDATE MA_CORRELATIVOS SET nu_valor = (nu_valor + 1) WHERE cu_campo = 'Traslados'", conexionBD_SERVIDOR)
            adapter_MA_INVENTARIO.InsertCommand.Transaction = sqlTran
            'adapter_MA_INVENTARIO.InsertCommand.Parameters.Add("@numeroTraslado_SERVIDORsinFormato", numeroTraslado_SERVIDORsinFormato) 'Ultimo traslado que se ha hecho en con el PDA sin formato (que es el mismo que en el servidor)
            adapter_MA_INVENTARIO.InsertCommand.ExecuteNonQuery()
            adapter_MA_INVENTARIO.InsertCommand.Parameters.Clear()

            sqlTran.Commit()

            ActualizarNumUltimoTraslado_PDA()

            finalizarTraslado = True

            MsgBox("Traslado Nº " & CStr(numeroTraslado_SERVIDOR) & " realizado con éxito.", MsgBoxStyle.Exclamation, "isMOBILE")

        Catch sqlEx As SqlException
            MessageBox.Show("Ha ocurrido una excepción:" + sqlEx.Message + ". Al finalizar traslado")
            sqlTran.Rollback()
            finalizarTraslado = False
        End Try

        limpiarFormLecturaProducto_Traslado()
        FormObservacion_Traslado.txtObservacion.Text = ""
        FormObservacion_Traslado.lblNºTrs.Text = ""
        formMenuPrincipal.Show()
        FormObservacion_Traslado.Visible = False
        FormLecturaProducto_Traslado.Cancelando_O_Entrando = True

    End Function

    Sub guardarTraslado() 'Si entra aqui es porque no hay conexión con el servidor
        'Guardo el producto en la base de datos local para el desposito origen

        Dim codigoMoneda As String
        Dim fecha As String = DateTime.Now.ToString

        SentenciaSQL = "SELECT C_CODMONEDA FROM MA_MONEDAS"
        codigoMoneda = returnString_PDA(SentenciaSQL)
        SentenciaSQL = "SELECT n_factor FROM MA_MONEDAS"
        factorCambio = returnInteger_PDA(SentenciaSQL)

        comandBD_PDA.CommandText = "SELECT * FROM MA_MONEDAS WHERE c_codmoneda = @codigoMoneda"
        comandBD_PDA.Parameters.Add("@codigoMoneda", codigoMoneda)
        dataset_Monedas = consultaQuery_PDA(comandBD_PDA)

        comandoBD_PDA.CommandText = "INSERT INTO MA_TRASLADOS_PDA VALUES ('TRS', @c_documento, @fecha, 'DCO', @c_codlocalidad, @c_codmoneda, @n_factorcambio, @c_observacion, @c_codcomprador, @c_dep_orig, @c_dep_dest, @c_motivo, @c_ejecutor, @codconcepto)"
        comandoBD_PDA.Parameters.Add("@c_documento", numeroTraslado_SERVIDOR)
        comandoBD_PDA.Parameters.Add("@fecha", fecha)
        comandoBD_PDA.Parameters.Add("@c_codlocalidad", Localidad)
        comandoBD_PDA.Parameters.Add("@c_codmoneda", CStr(codigoMoneda))
        comandoBD_PDA.Parameters.Add("@n_factorcambio", factorCambio)
        comandoBD_PDA.Parameters.Add("@c_observacion", FormObservacion_Traslado.txtObservacion.Text)
        comandoBD_PDA.Parameters.Add("@c_codcomprador", datasetUsuario.Tables(0).Rows(0)("codusuario"))
        comandoBD_PDA.Parameters.Add("@c_dep_orig", FormDepositoOrigenDestino_Traslado.txtDepositoOrigen.Text)
        comandoBD_PDA.Parameters.Add("@c_dep_dest", FormDepositoOrigenDestino_Traslado.txtDepositoDestino.Text)
        comandoBD_PDA.Parameters.Add("@c_motivo", FormEjecutorMotivo_Traslado.cboConceptosTraslados.Text)
        comandoBD_PDA.Parameters.Add("@c_ejecutor", FormEjecutorMotivo_Traslado.txtEjecutor.Text)
        comandoBD_PDA.Parameters.Add("@codconcepto", CDbl(FormEjecutorMotivo_Traslado.cboConceptosTraslados.SelectedValue))
        ejecutaQuery_PDA(comandoBD_PDA)

        MsgBox("El traslado ha sido guardado en la base de datos local temporalmente, no hay conexión con el servidor.", MsgBoxStyle.Exclamation, "isMOBILE")

        limpiarFormLecturaProducto_Traslado()
        FormObservacion_Traslado.txtObservacion.Text = ""
        FormObservacion_Traslado.lblNºTrs.Text = ""
        formMenuPrincipal.Show()
        FormObservacion_Traslado.Visible = False

        ActualizarNumUltimoTraslado_PDA()

    End Sub

    Public Function GrabarUbicacionProducto(ByVal GridUbicaciones As DataGrid, ByVal CodigoProducto As String) As Boolean

        Try

            If conectarDB_SERVIDOR(rutaDB_SERVIDOR) = 0 Then
                MsgBox("En estos momentos no hay conexión al servidor, no se ha podido grabar la información.", "isMOBILE")
                Return False
            End If

            sqlTran = conexionBD_SERVIDOR.BeginTransaction()

            comandBD_SERVIDOR.Parameters.Clear()
            comandBD_SERVIDOR.CommandText = "DELETE FROM MA_UBICACIONxPRODUCTO WHERE cu_Producto = @Codigo"
            comandBD_SERVIDOR.Parameters.AddWithValue("@Codigo", CodigoProducto)

            executeQuery_Servidor(comandBD_SERVIDOR)

            For i As Integer = 1 To GridUbicaciones.BindingContext(GridUbicaciones.DataSource).Count

                Dim SQL As String

                SQL = "INSERT INTO MA_UBICACIONxPRODUCTO (cu_Deposito, cu_Producto, cu_Mascara, n_Linea)" & vbNewLine & _
                "VALUES (@Deposito, @Producto, @Ubicacion, @Linea)"

                comandBD_SERVIDOR.CommandText = SQL

                comandBD_SERVIDOR.Parameters.Clear()
                comandBD_SERVIDOR.Parameters.AddWithValue("@Deposito", GridUbicaciones.Item(i - 1, 1).ToString)
                comandBD_SERVIDOR.Parameters.AddWithValue("@Producto", CodigoProducto)
                comandBD_SERVIDOR.Parameters.AddWithValue("@Ubicacion", GridUbicaciones.Item(i - 1, 3).ToString)
                comandBD_SERVIDOR.Parameters.AddWithValue("@Linea", i)

                executeQuery_Servidor(comandBD_SERVIDOR)

            Next

            sqlTran.Commit()

            Return True

        Catch ex As Exception

            If Not sqlTran Is Nothing Then sqlTran.Rollback()
            MsgBox("No se pudo grabar. Detalles: " & ex.Message, MsgBoxStyle.Critical, "isMOBILE")
        Finally

        End Try

    End Function

    Sub agregarProductoPendienteTR_INVENTARIO_SERVIDOR(ByVal dataset_TR_TRASLADOS_PDA As DataSet)

        Dim adapter_TR_INVENTARIO As New SqlDataAdapter
        Dim adapter_MA_DEPOPROD As New SqlDataAdapter

        'sqlTran = conexionBD_SERVIDOR.BeginTransaction()

        Try

            adapter_TR_INVENTARIO.InsertCommand = New SqlCommand("INSERT INTO TR_INVENTARIO " + _
            "(c_Linea, c_Concepto, c_Documento, c_Deposito, c_CodArticulo, n_Cantidad, " + _
            "n_Costo, n_Subtotal, n_Impuesto, n_Total, c_TipoMov, n_Cant_Teorica, n_Cant_Diferencia, " + _
            "n_Precio, n_Precio_Original, f_Fecha, c_CodLocalidad, n_FactorCambio, c_Descripcion, " + _
            "c_Compuesto, CodConcepto, n_DescuentoGeneral, n_DescuentoEspecifico, " + _
            "c_Documento_Origen, c_TipoDoc_Origen, n_CantidadFac, ns_Descuento, " + _
            "cs_ComprobanteContable, cs_CodLocalidad, ns_CantidadEmpaque, Impuesto) " + _
            "VALUES (@c_linea, 'TRS', @c_documento, @c_deposito, @c_codarticulo, @n_cantidad, " + _
            "@n_costo, @n_subtotal, 0, @n_total, @c_tipomov, 0, 0, 0, 0, GetDate(), " + _
            "@c_codlocalidad, 1, '', '', 0, 0, 0, '', '', 0, 0, '', " + _
            "@cs_codlocalidad, @ns_cantidadEmpaque, 0)", conexionBD_SERVIDOR)

            adapter_TR_INVENTARIO.InsertCommand.Transaction = sqlTran

            For Each registro As Data.DataRow In dataset_TR_TRASLADOS_PDA.Tables(0).Rows

                adapter_TR_INVENTARIO.InsertCommand.Parameters.Add("@c_linea", registro("c_linea"))
                adapter_TR_INVENTARIO.InsertCommand.Parameters.Add("@c_documento", numeroTraslado_SERVIDOR)
                adapter_TR_INVENTARIO.InsertCommand.Parameters.Add("@c_deposito", registro("c_deposito"))
                adapter_TR_INVENTARIO.InsertCommand.Parameters.Add("@c_codarticulo", registro("c_codarticulo"))
                adapter_TR_INVENTARIO.InsertCommand.Parameters.Add("@n_cantidad", registro("n_cantidad"))
                adapter_TR_INVENTARIO.InsertCommand.Parameters.Add("@n_costo", registro("n_costo"))
                adapter_TR_INVENTARIO.InsertCommand.Parameters.Add("@n_subtotal", registro("n_subtotal"))
                adapter_TR_INVENTARIO.InsertCommand.Parameters.Add("@n_total", registro("n_total"))
                adapter_TR_INVENTARIO.InsertCommand.Parameters.Add("@c_tipomov", registro("c_tipomov"))
                adapter_TR_INVENTARIO.InsertCommand.Parameters.Add("@c_codlocalidad", registro("c_codlocalidad"))
                adapter_TR_INVENTARIO.InsertCommand.Parameters.Add("@c_descripcion", registro("c_descripcion"))
                adapter_TR_INVENTARIO.InsertCommand.Parameters.Add("@cs_codlocalidad", registro("cs_codlocalidad"))
                adapter_TR_INVENTARIO.InsertCommand.Parameters.Add("@ns_cantidadEmpaque", registro("ns_cantidadEmpaque"))

                adapter_TR_INVENTARIO.InsertCommand.ExecuteNonQuery()
                adapter_TR_INVENTARIO.InsertCommand.Parameters.Clear()

                'Debo verificar la cantidad en este instante en MA_DEPOPROD en el servidor de cada producto que se encuentre en TR_TRASLADOS_PDA
                'para luego sumar o restar dependiendo del deposito, la cantidad del traslado.

                Dim dataset_MA_DEPOPROD As New Data.DataSet
                Dim codigoProducto As String = CStr(registro("c_codarticulo"))
                Dim codigoDeposito As String = CStr(registro("c_deposito"))
                Dim descripcionProducto As String = CStr(registro("c_descripcion"))
                Dim cantidadTraslado As Double = CDbl(registro("n_cantidad"))
                Dim tipomov As String = CStr(registro("c_tipomov"))
                Dim cantidadProducto As Double

                dataset_MA_DEPOPROD = consultarProducto_MA_DEPOPROD(codigoProducto, codigoDeposito)

                If dataset_MA_DEPOPROD.Tables(0).Rows.Count = 0 Then 'No existe el producto en la tabla

                    If tipomov = "Cargo" Then
                        cantidadProducto = cantidadTraslado
                    Else
                        cantidadProducto = cantidadTraslado * (-1)
                    End If

                    adapter_MA_DEPOPROD.UpdateCommand = New SqlCommand("INSERT INTO MA_DEPOPROD " + _
                    "(c_CodDeposito, c_CodArticulo, c_Descripcion, n_Cantidad, n_Cant_Ordenada, n_Cant_Comprometida) " + _
                    "VALUES (@codigodeposito, @codigoProducto, @descripcion, @cantidad_MA_DEPOPROD, 0 ,0)", _
                    conexionBD_SERVIDOR)

                    adapter_MA_DEPOPROD.UpdateCommand.Transaction = sqlTran

                    adapter_MA_DEPOPROD.UpdateCommand.Parameters.Add("@codigoDeposito", codigoDeposito)
                    adapter_MA_DEPOPROD.UpdateCommand.Parameters.Add("@codigoProducto", codigoProducto)
                    adapter_MA_DEPOPROD.UpdateCommand.Parameters.Add("@descripcion", descripcionProducto)
                    adapter_MA_DEPOPROD.UpdateCommand.Parameters.Add("@cantidad_MA_DEPOPROD", cantidadProducto)
                    adapter_MA_DEPOPROD.UpdateCommand.ExecuteNonQuery()
                    adapter_MA_DEPOPROD.UpdateCommand.Parameters.Clear()

                Else 'Si existe el producto en la tabla

                    If tipomov = "Cargo" Then
                        cantidadProducto = dataset_MA_DEPOPROD.Tables(0).Rows(0)("n_cantidad") + cantidadTraslado
                    Else
                        cantidadProducto = dataset_MA_DEPOPROD.Tables(0).Rows(0)("n_cantidad") - cantidadTraslado
                    End If

                    adapter_MA_DEPOPROD.InsertCommand = New SqlCommand("UPDATE MA_DEPOPROD SET " + _
                    "n_cantidad = @cantidad_MA_DEPOPROD " + _
                    "WHERE c_coddeposito = @codigodeposito " + _
                    "AND c_codarticulo = @codigoProducto", conexionBD_SERVIDOR)

                    adapter_MA_DEPOPROD.InsertCommand.Transaction = sqlTran

                    adapter_MA_DEPOPROD.InsertCommand.Parameters.Add("@cantidad_MA_DEPOPROD", cantidadProducto) 'Ya toma en cuenta la cantidad que estaba anteriormente en la tabla
                    adapter_MA_DEPOPROD.InsertCommand.Parameters.Add("@codigoDeposito", codigoDeposito)
                    adapter_MA_DEPOPROD.InsertCommand.Parameters.Add("@codigoProducto", codigoProducto)

                    adapter_MA_DEPOPROD.InsertCommand.ExecuteNonQuery()
                    adapter_MA_DEPOPROD.InsertCommand.Parameters.Clear()

                End If

            Next

            'sqlTran.Commit()

            'Borro la tabla en el PDA
            comandBD_PDA.CommandText = "DELETE TR_TRASLADOS_PDA"
            ejecutaQuery_PDA(comandBD_PDA)

        Catch sqlEx As SqlException

            MessageBox.Show("Ha ocurrido una excepción:" + sqlEx.Message + ".  Al enviar Traslado al servidor.")
            'sqlTran.Rollback()

        End Try

    End Sub


    Sub agregarProductoPendienteMA_INVENTARIO_SERVIDOR(ByVal dataset_MA_TRASLADOS_PDA As Data.DataSet)
        Dim adapter_MA_INVENTARIO As New SqlDataAdapter

        sqlTran = conexionBD_SERVIDOR.BeginTransaction()

        Try
            adapter_MA_INVENTARIO.InsertCommand = New SqlCommand("INSERT INTO MA_INVENTARIO VALUES ('TRS', @c_documento, SYSDATETIME (), '', 'DCO', '', @c_codlocalidad, @c_codmoneda, 1, 0, @c_observacion, '', @c_codcomprador, @c_dep_orig, @c_dep_dest, @c_motivo, '', @c_ejecutor, '', '', 0, @codconcepto, '', getdate (), getdate (), '', '', '')", conexionBD_SERVIDOR)
            adapter_MA_INVENTARIO.InsertCommand.Transaction = sqlTran
            For Each registro As Data.DataRow In dataset_MA_TRASLADOS_PDA.Tables(0).Rows
                adapter_MA_INVENTARIO.InsertCommand.Parameters.Add("@c_documento", registro("c_documento"))
                adapter_MA_INVENTARIO.InsertCommand.Parameters.Add("@c_codlocalidad", registro("c_codlocalidad"))
                adapter_MA_INVENTARIO.InsertCommand.Parameters.Add("@c_codmoneda", registro("c_codmoneda"))
                'adapter_MA_INVENTARIO.InsertCommand.Parameters.Add("@n_factorcambio", registro("n_factocambio")) ME DICE QUE NO EXISTE EL CAMPO EN LA TABLA :S
                adapter_MA_INVENTARIO.InsertCommand.Parameters.Add("@c_observacion", registro("c_observacion"))
                adapter_MA_INVENTARIO.InsertCommand.Parameters.Add("@c_codcomprador", registro("c_codcomprador"))
                adapter_MA_INVENTARIO.InsertCommand.Parameters.Add("@c_dep_orig", registro("c_dep_orig"))
                adapter_MA_INVENTARIO.InsertCommand.Parameters.Add("@c_dep_dest", registro("c_dep_dest"))
                adapter_MA_INVENTARIO.InsertCommand.Parameters.Add("@c_motivo", registro("c_motivo"))
                adapter_MA_INVENTARIO.InsertCommand.Parameters.Add("@c_ejecutor", registro("c_ejecutor"))
                adapter_MA_INVENTARIO.InsertCommand.Parameters.Add("@codconcepto", registro("codconcepto"))
                adapter_MA_INVENTARIO.InsertCommand.ExecuteNonQuery()
                adapter_MA_INVENTARIO.InsertCommand.Parameters.Clear()

                Dim numeroTraslado As String = CStr(registro("c_documento"))

                'Actualizo el correlativo de traslados
                adapter_MA_INVENTARIO.InsertCommand = New SqlCommand("UPDATE MA_CORRELATIVOS SET nu_valor = @numeroTraslado_SERVIDORsinFormato WHERE cu_campo = 'Traslados'", conexionBD_SERVIDOR)
                adapter_MA_INVENTARIO.InsertCommand.Transaction = sqlTran
                adapter_MA_INVENTARIO.InsertCommand.Parameters.Add("@numeroTraslado_SERVIDORsinFormato", numeroTraslado)
                adapter_MA_INVENTARIO.InsertCommand.ExecuteNonQuery()
                adapter_MA_INVENTARIO.InsertCommand.Parameters.Clear()

            Next

            sqlTran.Commit()

            'Borro la tabla en el PDA
            comandBD_PDA.CommandText = "DELETE MA_TRASLADOS_PDA"
            ejecutaQuery_PDA(comandBD_PDA)

        Catch sqlEx As SqlException

            MessageBox.Show("Ha ocurrido una excepción:" + sqlEx.Message + ". Al enviar MA_TRASLADOS al servidor")
            sqlTran.Rollback()
        End Try
    End Sub

    '******************************** I N V E N T A R I O   F I S I C O ************************************************


    Function insertarRegistros_INV_FISICO_SERVIDOR(ByVal dataSet_PDA As DataSet, ByVal numDocumento As String, ByVal dataSet_PDA2 As DataSet) As Integer

        Dim adapter_MA_INV_FISICO As New SqlDataAdapter
        Dim adapter_TR_INV_FISICO As New SqlDataAdapter

        sqlTran = conexionBD_SERVIDOR.BeginTransaction()

        Try
            adapter_MA_INV_FISICO.InsertCommand = New SqlCommand("INSERT INTO MA_INV_FISICO" + _
            vbNewLine + "(c_DOCUMENTO, d_FECHA, c_STATUS, c_CODLOCALIDAD, c_codMONEDA, n_FACTORCAMBIO, c_OBSERVACION, c_CODCOMPRADOR," + _
            vbNewLine + "c_DEP_ORIG, c_MOTIVO, NO_ELAB, CU_CODDEPARTAMENTO, CU_CODGRUPO, CU_CODSUBGRUPO, CU_CODPROVEEDOR) VALUES" + _
            vbNewLine + "(@c_DOCUMENTO, @d_FECHA, @c_STATUS, @c_CODLOCALIDAD, @c_codMONEDA, @n_FACTORCAMBIO, @c_OBSERVACION, @c_CODCOMPRADOR," + _
            vbNewLine + "@c_DEP_ORIG, @c_MOTIVO, @NO_ELAB, @CU_CODDEPARTAMENTO, @CU_CODGRUPO, @CU_CODSUBGRUPO, @CU_CODPROVEEDOR)", conexionBD_SERVIDOR)

            adapter_MA_INV_FISICO.InsertCommand.Transaction = sqlTran

            For Each registro As Data.DataRow In dataSet_PDA.Tables(0).Rows
                adapter_MA_INV_FISICO.InsertCommand.Parameters.Add("@c_DOCUMENTO", numDocumento)
                adapter_MA_INV_FISICO.InsertCommand.Parameters.Add("@d_FECHA", registro("d_FECHA"))
                adapter_MA_INV_FISICO.InsertCommand.Parameters.Add("@c_STATUS", registro("c_STATUS"))
                adapter_MA_INV_FISICO.InsertCommand.Parameters.Add("@c_CODLOCALIDAD", registro("c_CODLOCALIDAD"))
                adapter_MA_INV_FISICO.InsertCommand.Parameters.Add("@c_codMONEDA", registro("c_codMONEDA"))
                adapter_MA_INV_FISICO.InsertCommand.Parameters.Add("@n_FACTORCAMBIO", registro("n_FACTORCAMBIO"))
                adapter_MA_INV_FISICO.InsertCommand.Parameters.Add("@c_OBSERVACION", registro("c_OBSERVACION"))
                adapter_MA_INV_FISICO.InsertCommand.Parameters.Add("@c_CODCOMPRADOR", registro("c_CODCOMPRADOR"))
                adapter_MA_INV_FISICO.InsertCommand.Parameters.Add("@c_DEP_ORIG", registro("c_DEP_ORIG"))
                adapter_MA_INV_FISICO.InsertCommand.Parameters.Add("@c_MOTIVO", registro("c_MOTIVO"))
                adapter_MA_INV_FISICO.InsertCommand.Parameters.Add("@NO_ELAB", registro("NO_ELAB"))
                adapter_MA_INV_FISICO.InsertCommand.Parameters.Add("@CU_CODDEPARTAMENTO", registro("CU_CODDEPARTAMENTO"))
                adapter_MA_INV_FISICO.InsertCommand.Parameters.Add("@CU_CODGRUPO", registro("CU_CODGRUPO"))
                adapter_MA_INV_FISICO.InsertCommand.Parameters.Add("@CU_CODSUBGRUPO", registro("CU_CODSUBGRUPO"))
                adapter_MA_INV_FISICO.InsertCommand.Parameters.Add("@CU_CODPROVEEDOR", registro("CU_CODPROVEEDOR"))
                adapter_MA_INV_FISICO.InsertCommand.ExecuteNonQuery()
                adapter_MA_INV_FISICO.InsertCommand.Parameters.Clear()
            Next

            adapter_TR_INV_FISICO.InsertCommand = New SqlCommand("INSERT INTO TR_INV_FISICO" + _
            vbNewLine + "(c_LINEA, c_DOCUMENTO, c_DEPOSITO, c_CODARTICULO, n_CANTIDAD, n_COSTO, N_SUBTOTAL, cs_codlocalidad, ns_CantidadEmpaque) VALUES" + _
            vbNewLine + "(@c_LINEA, @c_DOCUMENTO, @c_DEPOSITO, @c_CODARTICULO, @n_CANTIDAD, @n_COSTO, @N_SUBTOTAL, @cs_codlocalidad, @ns_CantidadEmpaque)", conexionBD_SERVIDOR)

            adapter_TR_INV_FISICO.InsertCommand.Transaction = sqlTran

            For Each registro2 As Data.DataRow In dataSet_PDA2.Tables(0).Rows
                adapter_TR_INV_FISICO.InsertCommand.Parameters.Add("@c_LINEA", registro2("c_LINEA"))
                adapter_TR_INV_FISICO.InsertCommand.Parameters.Add("@c_DOCUMENTO", numDocumento)
                adapter_TR_INV_FISICO.InsertCommand.Parameters.Add("@c_DEPOSITO", registro2("c_DEPOSITO"))
                adapter_TR_INV_FISICO.InsertCommand.Parameters.Add("@c_CODARTICULO", registro2("c_CODARTICULO"))
                adapter_TR_INV_FISICO.InsertCommand.Parameters.Add("@n_CANTIDAD", registro2("n_CANTIDAD"))
                adapter_TR_INV_FISICO.InsertCommand.Parameters.Add("@n_COSTO", registro2("n_COSTO"))
                adapter_TR_INV_FISICO.InsertCommand.Parameters.Add("@N_SUBTOTAL", registro2("N_SUBTOTAL"))
                adapter_TR_INV_FISICO.InsertCommand.Parameters.Add("@cs_codlocalidad", registro2("cs_codlocalidad"))
                adapter_TR_INV_FISICO.InsertCommand.Parameters.Add("@ns_CantidadEmpaque", registro2("ns_CantidadEmpaque"))
                adapter_TR_INV_FISICO.InsertCommand.ExecuteNonQuery()
                adapter_TR_INV_FISICO.InsertCommand.Parameters.Clear()
            Next

            sqlTran.Commit()
            Return 1
        Catch sqlEx As SqlException
            MessageBox.Show("Ha ocurrido una excepción:" + sqlEx.Message + ". Al enviar documento de inventario.")
            sqlTran.Rollback()
            Return 0
        End Try

    End Function

    Function construirNumeroTraslado_PDA()
        Dim numeroTraslado_PDA As String
        comandBD_PDA.CommandText = "SELECT numUltimoTraslado FROM MA_CONFIGURACION"
        dataSet_PDA = consultaQuery_PDA(comandBD_PDA)

        Dim numUltimoTrasladoProcesado As Integer = CInt(dataSet_PDA.Tables(0).Rows(0)("numUltimoTraslado")) + 1

        'Construyendo el número de documento para el servidor que será usado en la observacion de MA_INVENTARIO
        'sentenciaSQL = "SELECT RIGHT('000000" + numUltimoTrasladoProcesado + "',6)"

        'If Not returnString_SERVIDOR(sentenciaSQL) = 0 Then
        Dim FormatoNumeroTraslado As String = Format(numUltimoTrasladoProcesado, "00000#")
        numeroTraslado_PDA = Id_PDA + FormatoNumeroTraslado
        Return numeroTraslado_PDA
        'Else
        'Return 0
        'End If

    End Function

    Function debeActualizar(ByVal tablaActualizar As String) As Boolean

        Select Case tablaActualizar
            Case Is = "MA_INVENTARIO"
                comandBD_PDA.CommandText = "SELECT COUNT(*) AS cant FROM " + tablaActualizar + " WHERE c_codlocalidad = @localidad AND c_STATUS <> 'ANU' AND c_CONCEPTO NOT IN ('AJU', 'PRD')"
                comandBD_PDA.Parameters.Add("@localidad", Localidad)
                dataSet_PDA = consultaQuery_PDA(comandBD_PDA)

                comandBD_SERVIDOR.CommandText = "SELECT COUNT(*) AS cant FROM " + tablaActualizar + " WHERE c_codlocalidad = @localidad AND c_STATUS <> 'ANU' AND c_CONCEPTO NOT IN ('AJU', 'PRD')"
                comandBD_SERVIDOR.Parameters.Add("@localidad", Localidad)
                dataSet_Servidor = ejecutaQuery_Servidor(comandBD_SERVIDOR, tablaActualizar)

                If dataSet_PDA.Tables(0).Rows(0)("cant") = dataSet_Servidor.Tables(0).Rows(0)("cant") Then
                    Return False
                Else
                    Return True
                End If

            Case Is = "MA_PRODUCTOS"
                comandBD_PDA.CommandText = "SELECT COUNT(*) AS cant FROM " + tablaActualizar + " WHERE n_activo = '1'"
                dataSet_PDA = consultaQuery_PDA(comandBD_PDA)

                comandBD_SERVIDOR.CommandText = "SELECT COUNT(*) AS cant FROM " + tablaActualizar + " WHERE n_activo = '1'"
                dataSet_Servidor = ejecutaQuery_Servidor(comandBD_SERVIDOR, tablaActualizar)

                If dataSet_PDA.Tables(0).Rows(0)("cant") = dataSet_Servidor.Tables(0).Rows(0)("cant") Then
                    Return False
                Else
                    Return True
                End If

            Case Is = "MA_CODIGOS"
                comandBD_PDA.CommandText = "SELECT COUNT(*) AS cant FROM " + tablaActualizar
                dataSet_PDA = consultaQuery_PDA(comandBD_PDA)

                comandBD_SERVIDOR.CommandText = "SELECT COUNT(*) AS cant FROM " + tablaActualizar
                dataSet_Servidor = ejecutaQuery_Servidor(comandBD_SERVIDOR, tablaActualizar)

                If dataSet_PDA.Tables(0).Rows(0)("cant") = dataSet_Servidor.Tables(0).Rows(0)("cant") Then
                    Return False
                Else
                    Return True
                End If

            Case Is = "MA_ODC"
                comandBD_PDA.CommandText = "SELECT COUNT(*) AS cant FROM " + tablaActualizar + ""
                'comandBD_PDA.Parameters.Add("@localidad", localidad)
                dataSet_PDA = consultaQuery_PDA(comandBD_PDA)

                'comandBD_SERVIDOR.CommandText = "SELECT COUNT(*) AS cant FROM " + tablaActualizar + " WHERE c_codlocalidad = @localidad AND c_status = 'DPE'"
                'comandBD_SERVIDOR.Parameters.Add("@localidad", localidad)
                'dataSet_Servidor = ejecutaQuery_Servidor(comandBD_SERVIDOR, tablaActualizar)

                If dataSet_PDA.Tables(0).Rows(0)("cant") <> 0 Then 'dataSet_Servidor.Tables(0).Rows(0)("cant") Then
                    Return False
                Else
                    Return True
                End If

            Case Is = "TR_ODC"
                comandBD_PDA.CommandText = "SELECT COUNT(*) AS cant FROM " + tablaActualizar + ""
                'comandBD_PDA.Parameters.Add("@localidad", localidad)
                dataSet_PDA = consultaQuery_PDA(comandBD_PDA)

                'comandBD_SERVIDOR.CommandText = "SELECT COUNT(*) AS cant FROM " + tablaActualizar + " t INNER JOIN MA_ODC m ON t.c_DOCUMENTO = m.c_DOCUMENTO WHERE t.cs_codlocalidad = @localidad AND m.c_status = 'DPE'"
                'comandBD_SERVIDOR.Parameters.Add("@localidad", localidad)
                'dataSet_Servidor = ejecutaQuery_Servidor(comandBD_SERVIDOR, tablaActualizar)

                If dataSet_PDA.Tables(0).Rows(0)("cant") <> 0 Then ' = dataSet_Servidor.Tables(0).Rows(0)("cant") Then
                    Return False
                Else
                    Return True
                End If

            Case Else
                Return False

        End Select


    End Function

    Public Function obtenerCodigoPrincipal(ByVal Cod As String) As String

        comandBD_SERVIDOR.CommandText = "SELECT C_CODNASA FROM MA_CODIGOS WHERE C_CODIGO = @COD"
        comandBD_SERVIDOR.Parameters.Add("@COD", Cod)

        If Val(TmpBDCheck) = 1 Then
            dataSet_Servidor = executeQuery_Servidor(comandBD_SERVIDOR, conexionBD_SERVIDORPOS)
        Else
            dataSet_Servidor = executeQuery_Servidor(comandBD_SERVIDOR)
        End If

        If Not dataSet_Servidor.Tables(0).Rows.Count = 0 Then
            obtenerCodigoPrincipal = dataSet_Servidor.Tables(0).Rows(0)("C_CODNASA")
            Exit Function
        End If

        obtenerCodigoPrincipal = ""

    End Function

    Public Function obtenerCodigoEDI(ByVal Cod As String, Optional ByVal esCodPrincipal As Boolean = True, Optional ByVal regresarPrincipalporDefault As Boolean = False) As String

        Dim CodPrincipal As String

        If esCodPrincipal Then
            CodPrincipal = Cod
        Else
            CodPrincipal = obtenerCodigoPrincipal(Cod)
        End If

        Try
            comandBD_SERVIDOR.CommandText = "SELECT C_CODIGO FROM MA_CODIGOS WHERE C_CODNASA = @COD AND NU_INTERCAMBIO = 1"
            comandBD_SERVIDOR.Parameters.Add("@COD", CodPrincipal)

            dataSet_Servidor = executeQuery_Servidor(comandBD_SERVIDOR)

            If Not dataSet_Servidor.Tables(0).Rows.Count = 0 Then
                obtenerCodigoEDI = dataSet_Servidor.Tables(0).Rows(0)("C_CODIGO")
                Exit Function
            End If

            obtenerCodigoEDI = IIf(regresarPrincipalporDefault, CodPrincipal, "")
        Catch EX As Exception
            obtenerCodigoEDI = IIf(regresarPrincipalporDefault, CodPrincipal, "")
        End Try

    End Function

    Public Function obtenerRegladeNegocio(ByVal Texto As String, ByVal DefaultValor As String) As String

        comandoBD_PDA.CommandText = "SELECT valor FROM MA_REGLASDENEGOCIO WHERE campo = @RegladeNegocio"
        comandoBD_PDA.Parameters.Add("@RegladeNegocio", Texto)
        dataSet_PDA = consultaQuery_PDA(comandoBD_PDA)

        If Not dataSet_PDA.Tables(0).Rows.Count = 0 Then
            obtenerRegladeNegocio = dataSet_PDA.Tables(0).Rows(0)("Valor")
        Else
            obtenerRegladeNegocio = DefaultValor
        End If

    End Function

    Public Function obtenerRegladeNegocio_Servidor(ByVal Texto As String, ByVal DefaultValor As String) As String

        comandBD_SERVIDOR.CommandText = "SELECT valor FROM MA_REGLASDENEGOCIO WHERE campo = @RegladeNegocio"
        comandBD_SERVIDOR.Parameters.Add("@RegladeNegocio", Texto)
        dataSet_Servidor = executeQuery_Servidor(comandBD_SERVIDOR)

        If Not dataSet_Servidor.Tables(0).Rows.Count = 0 Then
            obtenerRegladeNegocio_Servidor = dataSet_Servidor.Tables(0).Rows(0)("Valor")
        Else
            obtenerRegladeNegocio_Servidor = DefaultValor
        End If

    End Function

    Public Function obtenerCodigoBarra(ByVal Cod As String, Optional ByVal regresarPrincipalporDefecto As Boolean = False) As String

        Dim numChars As Integer = Val(obtenerRegladeNegocio_Servidor("Hbl_CantidadCodigoBarra", "12"))

        If obtenerRegladeNegocio_Servidor("Hbl_CodigoBarraNumerico", "0") = "1" Then
            ' comandBD_PDA.CommandText = "SELECT c_codigo From MA_CODIGOS WHERE ((c_codnasa = @CP)" _
            '& " AND (LEN(c_codigo) >= @CC))" _
            '& " AND (ISNUMERIC(c_codigo)=1) and (c_codigo <> C_codnasa)" 'isNUMERIC al parecer no existe en SQL SERVER COMPACT.

            comandBD_SERVIDOR.CommandText = "SELECT c_codigo From MA_CODIGOS WHERE ((c_codnasa = @CP)" _
            & " AND (LEN(c_codigo) >= @CC))" _
            & " AND C_CODIGO LIKE '%[^0-9]%' and (c_codigo <> C_codnasa)" 'Esta expresión Like no reemplaza a isNUMERIC, pero en este caso sirve para filtrar
            'debido a que solo necesitamos saber si el codigo es de solo números, sin incluir otros signos ni decimales, por ejemplo, el Codigo Stellar y Codigos de Barra.

        Else
            comandBD_SERVIDOR.CommandText = "SELECT c_codigo From MA_CODIGOS WHERE ((c_codnasa = @CP) AND (LEN(c_codigo) >= @CC))"
        End If

        comandBD_SERVIDOR.Parameters.Add("@CP", Cod)
        comandBD_SERVIDOR.Parameters.Add("@CC", numChars)

        dataSet_Servidor = executeQuery_Servidor(comandBD_SERVIDOR)

        If Not dataSet_Servidor.Tables(0).Rows.Count = 0 Then
            obtenerCodigoBarra = dataSet_Servidor.Tables(0).Rows(0)("C_CODIGO")
            Exit Function
        End If

        obtenerCodigoBarra = IIf(regresarPrincipalporDefecto, Cod, "")

    End Function

    Public Function ObtenerCorrelativoInterno_PDA(ByVal Campo As String, _
    Optional ByVal Trans_Avanzar As SqlCeTransaction = Nothing) As String

        Dim Existe As Boolean

        comandoBD_PDA.Parameters.Clear()
        comandoBD_PDA.CommandText = "SELECT nu_Valor FROM MA_CORRELATIVOS_PDA WHERE cu_Campo = @Campo"
        comandoBD_PDA.Parameters.Add("@Campo", Campo)
        If Not Trans_Avanzar Is Nothing Then _
        comandoBD_PDA.Transaction = Trans_Avanzar

        dataSet_PDA = consultaQuery_PDA(comandoBD_PDA)

        If dataSet_PDA.Tables(0).Rows.Count = 0 Then
            comandoBD_PDA.Parameters.Clear()
            comandoBD_PDA.CommandText = "INSERT INTO MA_CORRELATIVOS_PDA (cu_Campo, nu_Valor) VALUES (@Campo, @Valor)"
            comandoBD_PDA.Parameters.Add("@Campo", Campo)
            comandoBD_PDA.Parameters.Add("@Valor", 0)
            If Not Trans_Avanzar Is Nothing Then _
            comandoBD_PDA.Transaction = Trans_Avanzar
            comandoBD_PDA.ExecuteNonQuery()
        Else
            Existe = True
        End If

        If Not Existe Then

            comandoBD_PDA.Parameters.Clear()
            comandoBD_PDA.CommandText = "SELECT nu_Valor FROM MA_CORRELATIVOS_PDA WHERE cu_Campo = @Campo"
            comandoBD_PDA.Parameters.Add("@Campo", Campo)
            If Not Trans_Avanzar Is Nothing Then _
            comandoBD_PDA.Transaction = Trans_Avanzar

            dataSet_PDA = consultaQuery_PDA(comandoBD_PDA)

        End If

        Dim Valor As Double = dataSet_PDA.Tables(0).Rows(0)("nu_Valor")
        Dim Correlativo As String = Format(Valor + 1, "0000000000")

        If Not Trans_Avanzar Is Nothing Then

            comandoBD_PDA.Parameters.Clear()
            comandoBD_PDA.CommandText = "UPDATE MA_CORRELATIVOS_PDA SET nu_Valor = (nu_Valor + 1) " & _
            "WHERE cu_Campo = @Campo"
            comandoBD_PDA.Parameters.Add("@Campo", Campo)
            comandoBD_PDA.Transaction = Trans_Avanzar
            comandoBD_PDA.ExecuteNonQuery()

        End If

        ObtenerCorrelativoInterno_PDA = Correlativo

    End Function

    Public Function ObtenerCorrelativoServidor(ByVal Campo As String, _
    Optional ByVal Trans_Avanzar As SqlTransaction = Nothing, _
    Optional ByVal CrearAutomaticamente As Boolean = False, _
    Optional ByVal DescripcionCrearAuto As String = "N/A", _
    Optional ByVal FormatoCrearAuto As String = "0000000000" _
    ) As String

        Dim Existe As Boolean

        comandBD_SERVIDOR.Parameters.Clear()
        comandBD_SERVIDOR.CommandText = "SELECT nu_Valor FROM MA_CORRELATIVOS WHERE cu_Campo = @Campo"
        comandBD_SERVIDOR.Parameters.Add("@Campo", Campo)
        If Not Trans_Avanzar Is Nothing Then _
        comandBD_SERVIDOR.Transaction = Trans_Avanzar

        dataSet_Servidor = ejecutaQuery_Servidor(comandBD_SERVIDOR, "Correlativo")

        If dataSet_Servidor.Tables(0).Rows.Count = 0 Then

            If CrearAutomaticamente Then

                ' Ojo, no usar con procesos críticos centralizables a los cuales generalmente se les modifica
                ' El correlativo o su formato para evitar colisión con otras localidades. Solo se debe poder 
                ' crear desde código si es una funcionalidad que solo aplica para la misma localidad o instancia.

                comandBD_SERVIDOR.Parameters.Clear()
                comandBD_SERVIDOR.CommandText = "INSERT INTO MA_CORRELATIVOS (cu_Campo, cu_Descripcion, nu_Valor, cu_Formato) VALUES (@Campo, @Desc, @Valor, @Formato)"
                comandBD_SERVIDOR.Parameters.Add("@Campo", Campo)
                comandBD_SERVIDOR.Parameters.Add("@Valor", 0)
                comandBD_SERVIDOR.Parameters.Add("@Desc", DescripcionCrearAuto)
                comandBD_SERVIDOR.Parameters.Add("@Formato", FormatoCrearAuto)

                If Not Trans_Avanzar Is Nothing Then _
                comandBD_SERVIDOR.Transaction = Trans_Avanzar
                comandBD_SERVIDOR.ExecuteNonQuery()

            Else
                Return String.Empty ' Validar y ante este caso hacer rollback de la transacción.
            End If

        Else
            Existe = True
        End If

        If Not Existe Then

            comandBD_SERVIDOR.Parameters.Clear()
            comandBD_SERVIDOR.CommandText = "SELECT nu_Valor FROM MA_CORRELATIVOS WHERE cu_Campo = @Campo"
            comandBD_SERVIDOR.Parameters.Add("@Campo", Campo)
            If Not Trans_Avanzar Is Nothing Then _
            comandBD_SERVIDOR.Transaction = Trans_Avanzar

            dataSet_Servidor = ejecutaQuery_Servidor(comandBD_SERVIDOR, "Correlativo")

        End If

        Dim Valor As Double = dataSet_Servidor.Tables(0).Rows(0)("nu_Valor")
        Dim Correlativo As String = Format(Valor + 1, dataSet_Servidor.Tables(0).Rows(0)("cu_Formato").ToString)

        If Not Trans_Avanzar Is Nothing Then

            comandBD_SERVIDOR.Parameters.Clear()
            comandBD_SERVIDOR.CommandText = "UPDATE MA_CORRELATIVOS SET nu_Valor = (nu_Valor + 1) " & _
            "WHERE cu_Campo = @Campo"
            comandBD_SERVIDOR.Parameters.Add("@Campo", Campo)
            comandBD_SERVIDOR.Transaction = Trans_Avanzar
            comandBD_SERVIDOR.ExecuteNonQuery()

        End If

        ObtenerCorrelativoServidor = Correlativo

    End Function

End Module

