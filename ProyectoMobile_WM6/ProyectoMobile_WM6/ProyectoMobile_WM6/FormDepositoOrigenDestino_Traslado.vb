﻿Public Class FormDepositoOrigenDestino_Traslado

    Dim dataset_Deposito As Data.DataSet

    Private Sub cmbVerificarDepositoOrigen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbVerificarDepositoOrigen.Click
        buscarDepositoOrigen()
    End Sub

    Private Sub buscarDepositoOrigen()
        'Cuando el txtDeposito no esta vacio; se verifica que el deposito existe
        If Not (txtDepositoOrigen.Text = "") Then
            If txtDepositoDestino.Text.Trim = txtDepositoOrigen.Text.Trim Then
                MsgBox("El depósito origen no puede ser igual al depósito de destino.", MsgBoxStyle.Information, "isMOBILE")
            Else
                If txtDepositoOrigen.TextLength <= 15 Then
                    dataset_Deposito = buscarDepositoporCodigo(txtDepositoOrigen.Text)
                    'Se verifica que la consulta trajo resultados o no
                    If dataset_Deposito.Tables(0).Rows.Count() = 0 Then
                        MsgBox("El código de deposito no existe.", MsgBoxStyle.Information, "isMOBILE")
                        txtDepositoOrigen.Text = ""
                        lblDescripcionDepositoOrigen.Text = "Descripción: "
                        txtDepositoOrigen.Focus()
                    Else
                        lblDescripcionDepositoOrigen.Text = "Descripción: " + dataset_Deposito.Tables(0).Rows(0)("c_descripcion")
                        txtDepositoDestino.Focus()
                    End If
                Else
                    MsgBox("Código inválido.", MsgBoxStyle.Information, "isMOBILE")
                    txtDepositoOrigen.Text = ""
                    txtDepositoOrigen.Focus()
                End If
            End If
        Else
            MsgBox("Debe ingresar el código del depósito o presionar el botón signo de interrogación para realizar la búsqueda.", MsgBoxStyle.Information, "isMOBILE")
            txtDepositoOrigen.Focus()
        End If
    End Sub

    Private Sub cmbBuscarDepositoOrigen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbBuscarDepositoOrigen.Click
        Deposito = "origen"
        FormBuscarDeposito.Owner = Me
        FormBuscarDeposito.txtDescripcionDeposito.Focus()
        FormBuscarDeposito.ShowDialog()
    End Sub

    Private Sub cmbVerificarDepositoDestino_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbVerificarDepositoDestino.Click
        buscarDepositoDestino()
    End Sub

    Private Sub buscarDepositoDestino()

        If Not (txtDepositoDestino.Text = "") Then
            If txtDepositoDestino.Text.Trim = txtDepositoOrigen.Text.Trim Then
                MsgBox("El depósito destino no puede ser igual al depósito de origen.", MsgBoxStyle.Information, "isMOBILE")
            Else
                If txtDepositoDestino.TextLength <= 15 Then
                    dataset_Deposito = buscarDepositoporCodigo(txtDepositoDestino.Text.Trim)
                    'Se verifica que la consulta trajo resultados o no
                    If dataset_Deposito.Tables(0).Rows.Count() = 0 Then
                        MsgBox("El código de depósito no existe.", MsgBoxStyle.Information, "isMOBILE")
                        txtDepositoDestino.Text = ""
                        lblDescripcionDepositoDestino.Text = "Descripción: "
                        txtDepositoDestino.Focus()
                    Else
                        lblDescripcionDepositoDestino.Text = "Descripción: " + dataset_Deposito.Tables(0).Rows(0)("c_descripcion")
                        cmbSiguiente.Focus()
                    End If
                Else
                    MsgBox("Código inválido.", MsgBoxStyle.Information, "isMOBILE")
                    txtDepositoOrigen.Text = ""
                    txtDepositoOrigen.Focus()
                End If
            End If
        Else
            MsgBox("Debe ingresar el código del depósito o presionar el botón signo de interrogación para realizar la búsqueda.", MsgBoxStyle.Information, "isMOBILE")
            txtDepositoOrigen.Focus()
        End If

    End Sub

    Private Sub cmbBuscarDepositoDestino_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbBuscarDepositoDestino.Click
        Deposito = "destino"
        FormBuscarDeposito.Owner = Me
        FormBuscarDeposito.txtDescripcionDeposito.Focus()
        FormBuscarDeposito.ShowDialog()
    End Sub

    Private Sub cmbSiguiente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSiguiente.Click
        If txtDepositoOrigen.Text = "" Then
            MsgBox("Debe ingresar el Depósito origen.", MsgBoxStyle.Information, "isMOBILE")
            txtDepositoOrigen.Focus()
        Else
            If txtDepositoDestino.Text = "" Then
                MsgBox("Debe ingresar el Depósito destino.", MsgBoxStyle.Information, "isMOBILE")
                txtDepositoDestino.Focus()
            Else
                If lblDescripcionDepositoOrigen.Text = "Descripción:" Or lblDescripcionDepositoDestino.Text = "Descripción:" Then
                    MsgBox("Debe verificar que los Depósitos existen.", MsgBoxStyle.Information, "isMOBILE")
                Else
                    If txtDepositoDestino.Text = txtDepositoOrigen.Text Then
                        MsgBox("Los depósitos Origen y Destino no pueden ser iguales.")
                    Else
                        FormEjecutorMotivo_Traslado.Show()
                        FormEjecutorMotivo_Traslado.txtEjecutor.Focus()
                        Me.Visible = False
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub cmbCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbCancelar.Click
        If Not (txtDepositoOrigen.Text = "") Then
            respuesta = MsgBox("¿Está seguro que desea cancelar?", MsgBoxStyle.YesNo, "Cancelar")
            If respuesta = "6" Then
                txtDepositoOrigen.Text = ""
                lblDescripcionDepositoOrigen.Text = ""
                txtDepositoDestino.Text = ""
                lblDescripcionDepositoDestino.Text = ""
                txtDepositoOrigen.Focus()
            End If
        Else
            If Not (txtDepositoDestino.Text = "") Then
                respuesta = MsgBox("¿Está seguro que desea cancelar?", MsgBoxStyle.YesNo, "Cancelar")
                If respuesta = "6" Then
                    txtDepositoOrigen.Text = ""
                    lblDescripcionDepositoOrigen.Text = ""
                    txtDepositoDestino.Text = ""
                    lblDescripcionDepositoDestino.Text = ""
                    txtDepositoOrigen.Focus()
                End If
            Else
                txtDepositoOrigen.Text = ""
                lblDescripcionDepositoOrigen.Text = ""
                txtDepositoDestino.Text = ""
                lblDescripcionDepositoDestino.Text = ""
                txtDepositoOrigen.Focus()
            End If
        End If
    End Sub

    Private Sub FormDepositoOrigenDestino_Traslado_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        lblUsuarioEnSesion.Text = "Usuario: " + descripcionUsuario

        Me.txtDepositoDestino.Text = ""
        Me.txtDepositoOrigen.Text = ""
        Me.lblDescripcionDepositoDestino.Text = "Descripcion:"
        Me.lblDescripcionDepositoOrigen.Text = "Descripcion:"

        limpiarFormLecturaProducto_Traslado()
        FormLecturaProducto_Traslado.lblNºTrs.Text = ""

        Me.Refresh()
        Me.Update()

        AjustarFormularios(Me)

        txtDepositoOrigen.Focus()

    End Sub

    Private Sub cmbSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSalir.Click
        If Not (txtDepositoOrigen.Text = "") Then
            respuesta = MsgBox("¿Está seguro que desea salir?", MsgBoxStyle.YesNo, "Salir")
            If respuesta = "6" Then
                txtDepositoOrigen.Text = ""
                lblDescripcionDepositoOrigen.Text = ""
                txtDepositoDestino.Text = ""
                lblDescripcionDepositoDestino.Text = ""
                limpiarFormLecturaProducto_Traslado()
                FormLecturaProducto_Traslado.lblNºTrs.Text = ""
                FormMenuModLog_1.Show()
                Me.Visible = False
            End If
        Else
            If Not (txtDepositoDestino.Text = "") Then
                respuesta = MsgBox("¿Está seguro que desea salir?", MsgBoxStyle.YesNo, "Salir")
                If respuesta = "6" Then
                    txtDepositoOrigen.Text = ""
                    lblDescripcionDepositoOrigen.Text = ""
                    txtDepositoDestino.Text = ""
                    lblDescripcionDepositoDestino.Text = ""
                    limpiarFormLecturaProducto_Traslado()
                    FormLecturaProducto_Traslado.lblNºTrs.Text = ""
                    FormMenuModLog_1.Show()
                    Me.Visible = False
                End If
            Else
                txtDepositoOrigen.Text = ""
                lblDescripcionDepositoOrigen.Text = ""
                txtDepositoDestino.Text = ""
                lblDescripcionDepositoDestino.Text = ""
                limpiarFormLecturaProducto_Traslado()
                FormLecturaProducto_Traslado.lblNºTrs.Text = ""
                FormMenuModLog_1.Show()
                Me.Visible = False
            End If
        End If
    End Sub

    Private Sub txtDepositoOrigen_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDepositoOrigen.KeyDown
        If e.KeyValue = 13 Then
            buscarDepositoOrigen()
        End If
    End Sub

    Private Sub txtDepositoDestino_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDepositoDestino.KeyDown
        If e.KeyValue = 13 Then
            buscarDepositoDestino()
        End If
    End Sub
End Class