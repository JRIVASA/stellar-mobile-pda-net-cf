﻿Public Class FormObservacionMotivo_InventarioFisico

    Private Sub cmbAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbAceptar.Click

        Dim observacion, motivo As String
        observacion = txtObservacion.Text
        motivo = txtMotivo.Text

        If actualizarMA_INV_FISICO(FormElab_Inv_InventarioFisico.lblNumDoc.Text, observacion, motivo) Then
            finalizarInventario(FormElab_Inv_InventarioFisico.lblNumDoc.Text, FormElab_Inv_InventarioFisico.lblNumDeposito.Text)
        Else
            Exit Sub
        End If

        FormElab_Inv_InventarioFisico.lblNumDoc.Text = ""
        FormElab_Inv_InventarioFisico.lblNumDeposito.Text = ""
        limpiarForm_Elab_Inv()
        FormElab_Inv_InventarioFisico.Linea = 1

        formMenuPrincipal.Show()

        Me.Visible = False
    End Sub

    Private Sub txtMotivo_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyValue = 13 Then
            txtObservacion.Focus()
        End If
    End Sub

    Private Sub txtObservacion_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyValue = 13 Then
            cmbAceptar_Click(Me, e)
        End If
    End Sub

    Private Sub FormObservacionMotivo_InventarioFisico_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        lblUsuarioEnSesion.Text = "Usuario: " + descripcionUsuario

        Me.Refresh()
        Me.Update()

        AjustarFormularios(Me)

    End Sub

    Private Sub cmbSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSalir.Click
        FormElab_Inv_InventarioFisico.Show()
        Me.Visible = False
    End Sub

End Class