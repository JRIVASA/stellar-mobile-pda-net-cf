﻿Imports System
Imports System.Data
Imports System.IO

Public Class FormMenuModLog_1

    Private FormaCargada As Boolean

    Private Sub CmbSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmbSalir.Click
        'salirProceso(Me)
        FormaCargada = False
        formMenuPrincipal.Visible = True
        Me.Visible = False
    End Sub

    Private Sub cmbRecepcion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmbRecepcion.Click

        If CompatibilidadFueraDeLinea Then
            lblStatus_MenuPrincipal.Visible = True
            lblStatus_MenuPrincipal.Text = "Actualizando ODC."
        Else
            If conectarDB_SERVIDOR(rutaDB_SERVIDOR) = 0 Then
                MessageBox.Show(MsjSinConexionGenerico)
                Exit Sub
            End If
        End If

        Me.Refresh()
        Me.Update()

        'Actualizo MA_ODC para que no permita recibir productos de una recepcion que ya haya sido completada 

        If conectarDB_SERVIDOR(rutaDB_SERVIDOR) = 1 Then

            If CompatibilidadFueraDeLinea Then
                'comandoBD_PDA.CommandText = "DELETE MA_ODC"
                'ejecutaQuery_PDA(comandoBD_PDA)
                'actualizarMA_ODC()
                ActualizacionParcial("ODC")
            End If

        Else
            MsgBox("No se puede actualizar la informacion de ordenes de compra," & vbNewLine & "no hay conexión con el servidor, comuniquese con el departamento de sistemas." _
            & vbNewLine & "No se recomienda realizar una nueva recepción sin antes" & vbNewLine & "haber establecido la conexión con el servidor.", MsgBoxStyle.Information, "isMOBILE")
        End If

        lblStatus_MenuPrincipal.Visible = False
        verFormCantidadFacturadaSinODC = True
        FormProveedorDeposito_Recepcion.Show()
        FormProveedorDeposito_Recepcion.txtProveedor.Enabled = True
        FormProveedorDeposito_Recepcion.txtProveedor.Focus()
        FormProveedorDeposito_Recepcion.txtDeposito.Enabled = False
        FormProveedorDeposito_Recepcion.cmbBuscarDeposito.Enabled = False
        FormProveedorDeposito_Recepcion.cmbVerificarDeposito.Enabled = False

        Me.Visible = False

    End Sub

    Private Sub cmbTraslado_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbTraslado.Click
        FormDepositoOrigenDestino_Traslado.Show()
        Me.Visible = False
        FormDepositoOrigenDestino_Traslado.txtDepositoOrigen.Focus()
    End Sub

    Private Sub FormMenuModLog_1_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Activated

        If Not FormaCargada Then

            FormaCargada = True

            TimerReload.Interval = 100
            TimerReload.Enabled = True

        End If

    End Sub

    Private Sub CmdPicking_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdPicking.Click
        FormPickingUserMain.Show()
        Me.Visible = False
    End Sub

    Private Sub CmdPacking_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdPacking.Click
        FormPackingUserMain.Show()
        Me.Visible = False
    End Sub

    Private Sub FormMenuModLog_1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        AjustarFormularios(Me)
    End Sub

    Private Sub Activation()

        formMenuPrincipal.HideOptions(Me)

        If (formMenuPrincipal.OpcionUsuarioActualHabilitada("frm_recepcion")) Then
            formMenuPrincipal.ShowMenuOption(Me, CmbRecepcion, lblRecepcion, formMenuPrincipal.NextVisibleMenu(Me))
        End If

        If (formMenuPrincipal.OpcionUsuarioActualHabilitada("frm_traslado")) Then
            formMenuPrincipal.ShowMenuOption(Me, cmbTraslado, lblTraslado, formMenuPrincipal.NextVisibleMenu(Me))
        End If

        If (formMenuPrincipal.OpcionUsuarioActualHabilitada("PickingUser")) Then
            formMenuPrincipal.ShowMenuOption(Me, CmdPicking, lblPicking, formMenuPrincipal.NextVisibleMenu(Me))
        End If

        If (formMenuPrincipal.OpcionUsuarioActualHabilitada("PackingUser")) Then
            formMenuPrincipal.ShowMenuOption(Me, CmdPacking, lblPacking, formMenuPrincipal.NextVisibleMenu(Me))
        End If

    End Sub

    Private Sub TimerReload_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TimerReload.Tick
        TimerReload.Enabled = False
        Activation()
    End Sub

End Class