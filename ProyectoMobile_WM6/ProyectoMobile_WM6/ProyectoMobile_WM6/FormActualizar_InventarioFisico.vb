﻿Public Class FormActualizar_InventarioFisico

    Public CallerForm As Form

    Private Sub cmbSelectTodos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSelectTodos.Click

        CheckConfiguración.CheckState = 1
        CheckUsuarios.CheckState = 1
        CheckDepositos.CheckState = 1
        CheckInventarios.CheckState = 1
        CheckProductos.CheckState = 1
        CheckODC.CheckState = 1

    End Sub

    Private Sub cmbActualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbActualizar.Click

        If CheckConfiguración.CheckState = 0 And CheckUsuarios.CheckState = 0 And CheckDepositos.CheckState = 0 _
        And CheckInventarios.CheckState = 0 And CheckProductos.CheckState = 0 And CheckODC.CheckState = 0 Then

            MsgBox("Debe seleccionar al menos una opción.", MsgBoxStyle.Information, "isMOBILE")

        Else

            ProgressBar_FormActualizarInventarioFisico.Visible = True

            Me.Refresh()
            Me.Update()

            actualizacionManual()

            CheckConfiguración.CheckState = 0
            CheckUsuarios.CheckState = 0
            CheckDepositos.CheckState = 0
            CheckInventarios.CheckState = 0
            CheckProductos.CheckState = 0
            CheckODC.CheckState = 0

        End If


    End Sub

    Private Sub FormActualizar_InventarioFisico_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        lblUsuarioEnSesion.Text = "Usuario: " + descripcionUsuario
        Me.Refresh()
        Me.Update()

        AjustarFormularios(Me)
        AjustarFormularios(Me.PanelActualizar)
        'AjustarFormularios(Me.PanelOpcionesActualizar)

        Me.Focus()

    End Sub

    Private Sub cmbSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSalir.Click
        CheckConfiguración.CheckState = 0
        CheckUsuarios.CheckState = 0
        CheckDepositos.CheckState = 0
        CheckInventarios.CheckState = 0
        CheckProductos.CheckState = 0
        CheckODC.CheckState = 0
        CallerForm.Show()
        Me.Visible = False
    End Sub
    
End Class