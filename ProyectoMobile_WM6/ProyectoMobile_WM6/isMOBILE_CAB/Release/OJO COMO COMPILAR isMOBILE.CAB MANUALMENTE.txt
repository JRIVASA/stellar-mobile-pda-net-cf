Es importante que creen el .cab como les dir� a continuaci�n:

Abran el Solution del Proyecto (tiene 2 projects el .exe y el .cab)
Compilen haciendo primero un Clean Output y luego un Rebuild Output
Esto va a regenerar autom�ticamente isMOBILE.CAB y al lado va a colocar un isMOBILE.inf

En el VS 2008 van a ver en la consola que emite la siguiente instrucci�n en la cual genera ambos archivos:

Building file 'D:\Fuentes Mobile\ProyectoMobile_WM6\ProyectoMobile_WM6\isMOBILE_CAB\Release\isMOBILE.CAB'...

"C:\Program Files (x86)\Microsoft Visual Studio 9.0\smartdevices\sdk\sdktools\cabwiz.exe" "D:\Fuentes Mobile\ProyectoMobile_WM6\ProyectoMobile_WM6\isMOBILE_CAB\Release\isMOBILE.inf" /dest "D:\Fuentes Mobile\ProyectoMobile_WM6\ProyectoMobile_WM6\isMOBILE_CAB\Release\" /err CabWiz.log

------------------------

Luego ese .inf lo vamos a editar y le vamos a cambiar la parte que dice

InstallDir= ..... blablabla

por

InstallDir=\Application\Program Files\%AppName%

Y guardamos el .inf

ESTO CON EL FIN DE QUE CUANDO SE VAYA A INSTALAR isMOBILE en el PDA por defecto tenga esa carpeta que generalmente en los PDA
es la carpeta que es PERSISTENTE y no se vuela su contenido ante un Cold Boot. Entonces Ante un Cold Boot hay que reinstalar
isMOBILE, con el Software StartupCtl Podr�amos mandar a instalar isMOBILE automaticamente agregando la instrucci�n en uno de los
archivos OnRestore.txt:

\windows\wceload.exe /noaskdest /delete 0 "\DondeEsteElInstalador\isMOBILE.CAB"

La ventaja es que si le colocamos /noaskdest el instalar� de manera silenciosa y sin preguntar ubicaci�n, va a tomar por defecto
la ubicaci�n que tiene el .inf que editamos, y no la ubicaci�n original que b�sicamente es \Program Files\isMOBILE, sin embargo dicha carpeta
no es persistente por eso necesitamos instalar dentro de \Application\...

LO MENCIONADO ANTERIORMENTE ES SOLO EL OBJETIVO A�N NO HEMOS TERMINADO. LUEGO DE QUE HAYAN EDITADO EL .INF CON LO MENCIONADO ANTERIORMENTE, 
VAN A TOMAR LA SEGUNDA INSTRUCCI�N QUE GENER� EL VISUAL STUDIO, LA LARGA Y LA VAN A EJECUTAR ABRIENDO UN CMD.EXE, PEGANDO EL COMANDO:

"C:\Program Files (x86)\Microsoft Visual Studio 9.0\smartdevices\sdk\sdktools\cabwiz.exe" "D:\Fuentes Mobile\ProyectoMobile_WM6\ProyectoMobile_WM6\isMOBILE_CAB\Release\isMOBILE.inf" /dest "D:\Fuentes Mobile\ProyectoMobile_WM6\ProyectoMobile_WM6\isMOBILE_CAB\Release\" /err CabWiz.log

Y PULSANDO ENTER.

EL RESULTADO ES QUE SE GENERAR� EL .CAB QUE TIENE EL CAMBIO BASADO EN EL .INF.

NO NOS FUNCIONA GENERAR EL isMOBILE.CAB DESDE VISUAL STUDIO SOLAMENTE POR QUE EL VA A REEMPLAZAR EL .INF con el valor viejo
cada vez que lo compilen. Hay que tomar en cuenta todo el procedimiento descrito para cada nueva versi�n que se compile a partir de ahora,
de manera que podamos automatizar la instalaci�n y reinstalaci�n del software ante un Cold Boot de los PDA.
